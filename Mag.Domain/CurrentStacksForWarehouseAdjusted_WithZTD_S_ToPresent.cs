﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain
{
    public class CurrentStacksForWarehouseAdjusted_WithZTD_S_ToPresent
    {
        public string IndexSIMPLE { get; set; }
        public string MaterialName { get; set; }
        public double? Quantity { get; set; }
        public string UnitShortName { get; set; }
        public int? WarehouseId { get; set; }
        public int MaterialId { get; set; }
    }
}
