﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain
{
    public class MaterialAvailabilityInfo
    {
        public int MaterialId { get; set; }
        public string MaterialName { get; set; }

        public List<Tuple<int, string, double>> ZoneQuantities { get; set; }
    }
}
