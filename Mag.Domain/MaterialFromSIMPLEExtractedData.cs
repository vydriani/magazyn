﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;

namespace Mag.Domain
{
	public class MaterialFromSIMPLEExtractedData
	{
		public int? IdSimple;
		public string IndexSimple;
		public string NameSimple;


		public C_ImportSimple_MaterialFromSimple TheMaterialFromSimple;

	}
}
