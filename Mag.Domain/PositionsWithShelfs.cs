﻿using System;
using System.Collections.Generic;
using Mag.Domain.Model;
using Mag.Domain.Data;

namespace Mag.Domain
{
  [Serializable]
  public class AdditionalPositionInfo
  {
    public int SupplySourceType { get; set; }

    public decimal TotalOrderQuantity { get; set; }
  }

  [Serializable]
  public class ChildParentPositionQuantity
  {
    public int LocationId { get; set; }

    public int PositionParentID { get; set; }

    public decimal Quantity { get; set; }
  }

  [Serializable]
  public class PositionWithQuantityAndShelfs
  {
    public bool AutoParentCollection = true;

    public int KCDocumentId;

    public int KCPositionId;

    public int KCStateId;

    public int KCSupplierId;

    public int MaterialId;

    public List<WarehouseDocumentPositionPackage> Packages;

    public int ProductionOrderId;

    public decimal Quantity;

    public bool RefetchParentOrderedMaterial = false;

    public DateTime? StartDate;

    public int SupplyOrderPositionId;

    public int TargetContractingPartySimpleId;

    public int? TargetDepartmentId;

    public string TargetSimpleDepartmentName;

    public decimal UnitPrice;

    public int? WarehouseSourceId;

    public string WarehouseSourceName;

    public AdditionalPositionInfo additionalPositionInfo { get; set; }

    public List<int> documentParentIds { get; set; }

    public bool IsReal { get; set; }

    public Order order { get; set; }

    public WarehouseDocumentPosition position { get; set; }

    public List<ChildParentPositionQuantity> positionParentIds { get; set; }

    public List<WarehousePositionZone> WarehousePositionZones { get; set; }

    public List<WarehouseDocumentPositionExtension> WarehouseDocumentPositionExtensions { get; set; }

    public int GridRow { get; set; }
  }


  [Serializable]
  public class PositionWithShelf
  {
    public Order order { get; set; }

    public WarehouseDocumentPosition position { get; set; }

    public Zone shelf { get; set; }
  }

  [Serializable]
  public class PositionWithShelfAndParentID
  {
    public int? documentParentId { get; set; }

    public WarehouseDocumentPosition position { get; set; }

    public int positionParentId { get; set; }

    public Zone shelf { get; set; }
  }

  [Serializable]
  public class PositionWithShelfAndQuantity
  {
      public Order order { get; set; }

      public WarehouseDocumentPosition position { get; set; }

      public decimal quantity { get; set; }

      public Zone shelf { get; set; }

      public string TargetSimpleDepartmentName { get; set; }

      public string Zones { get; set; }

      public bool IsKJ { get; set; }
  }
  [Serializable]
  public class PositionWithShelfAndQuantityOrdered
  {
      public WarehouseDocumentPositionLayer position { get; set; }
      public Zone shelf { get; set; }
      public double quantity { get; set; }
      public double ActualQuantity { get; set; }
      public double GivenQuantity { get; set; }
      public Order order { get; set; }
      public string Zones { get; set; }
      public string TargetSimpleDepartmentName { get; set; }
  }
}