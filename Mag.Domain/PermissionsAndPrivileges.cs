﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;

namespace Mag.Domain
{
    public class WarehousesAndSecurityPrivileges
    {
        public int? IdPrivilege { get; set; }
        public int IdWarehouse { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
    }

    public class WarehousePermissionsAndSecurityPriviledges
    {
        public int? FromWarehouseId { get; set; }
        public int? ToWarehouseId { get; set; }
        public string FromWarehouseShortName { get; set; }
        public string ToWarehouseShortName { get; set; }
        public int OperationId { get; set; }
        public int IdPermission { get; set; }
        public int IdType { get; set; }
        public string Type { get; set; }
        public string ShortType { get; set; }
        public int SystemUserId { get; set; }
        public int? IdPrivilege { get; set; }

    }

    public class WarehousePermissionsAndSecurityGroupPriviledges
    {
        public int? FromWarehouseId { get; set; }
        public int? ToWarehouseId { get; set; }
        public string FromWarehouseShortName { get; set; }
        public string ToWarehouseShortName { get; set; }
        public int OperationId { get; set; }
        public int IdPermission { get; set; }
        public int IdType { get; set; }
        public string Type { get; set; }
        public string ShortType { get; set; }
        public int SystemGroupId { get; set; }
        public int? IdPrivilege { get; set; }

    }

    public class GroupAndMembership
    {
        public int? MembershipID { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public bool IsMember { get; set; }
    }

    [Serializable]
    public class WarehousePermissionVirtual
    {
        public Warehouse Warehouse { get; set; } // src
        public Warehouse Warehouse1 { get; set; } // trg
        public WarehouseDocumentType WarehouseDocumentType { get; set; }

        public int? WarehouseId { get; set; }
        public int? Warehouse1Id { get; set; }

        public bool MaterialIsPackage { get; set; }
        public bool PositionIsGarbage { get; set; }
        public bool PositionIsZZ { get; set; }
        public bool PositionIsPartlyDamaged { get; set; }
        public bool PositionIsDamaged { get; set; }
    }
}

