﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.SecurityData
{
	[Serializable]
	public class ShelfExtended
	{
		public int ShelfId;
		public string ShelfExtendedName;
	}
}
