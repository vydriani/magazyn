using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Client.Domain;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Mag.Domain.Model;
using System.Diagnostics.Contracts;

namespace Mag.Domain.SecurityData
{
  [Serializable]
  public class ClientSecurityData
  {
    public List<JoinedPermission> JoinedWarehousePermissionsLst;
    public List<WarehouseSecurityGroupPrivilegeForExecutionOpt> WarehouseSecurityGroupPrivilegeForExecutionLst;
    public UserSecurityConfigMatrix CurrentUserSecurityConfigMatrix;
    public List<SystemUserSecurityPrivilege> SystemUserSecurityPrivileges;

    public static long GetObjectSizeInBytes(object obj, BinaryFormatter bf)
    {
      Contract.Requires(obj != null);
      Contract.Requires(bf != null);

      using (var m = new MemoryStream())
      {
        bf.Serialize(m, obj);
        return m.Length;
      }
    }
  }
}
