﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace Mag.Domain.SecurityData
{
  [Serializable]
  public class UserSecurityConfigMatrix
  {
    public UserSecurityConfigMatrix()
    {
    }

    public UserSecurityConfigMatrix(IEnumerable<SecurityConfigElement> permissions, IEnumerable<SecurityConfigElement> privileges)
      : this()
    {
      if (permissions != null)
      {
        var validPermissions = permissions
              .Where(x => x.IsGranted) // permissions should never have IsGranted=false but many do. After removing this inefficiency, please delete this line and add Contract check.
              .Select(x => new PermissionHash(x.FromWarehouseId, x.ToWarehouseId, x.OperationId));// GenerateStringHash(GetPrehashIntValue(x.FromWarehouseId), GetPrehashIntValue(x.ToWarehouseId), x.OperationId));
        warehousePermissions = new HashSet<PermissionHash>(validPermissions);
      }
      if (privileges != null)
      {
        var validPrivileges = privileges
          .Where(x => x.IsGranted) // privileges should never have IsGranted=false but many do. After removing this inefficiency, please delete this line and add Contract check.
          .Select(x => new PermissionHash(x.FromWarehouseId, x.ToWarehouseId, x.OperationId));// GenerateStringHash(GetPrehashIntValue(x.FromWarehouseId), GetPrehashIntValue(x.ToWarehouseId), x.OperationId));
        userPrivileges = new HashSet<PermissionHash>(validPrivileges);
      }
    }

    private HashSet<PermissionHash> userPrivileges = new HashSet<PermissionHash>();
    private HashSet<PermissionHash> warehousePermissions = new HashSet<PermissionHash>();

    private string GenerateStringHash(int fromWarehouseId, int toWarehouseId, int operationId)
    {
      return string.Format("{0:X8}{0:X8}{0:X8}", fromWarehouseId, toWarehouseId, operationId);
    }

    public bool CheckOperationAvailability(int? fromWarehouseId, int? toWarehouseId, int operationId)
    {
      Contract.Requires(!fromWarehouseId.HasValue || fromWarehouseId.Value > 0, "fromWarehouseId DB column cannot be less than 1");
      Contract.Requires(!toWarehouseId.HasValue || toWarehouseId.Value > 0, "toWarehouseId DB column cannot be less than 1");
      Contract.Requires(operationId > 0, "operationId DB column cannot be less than 1");

      return true; //NOTICE: on request we're disabling all permissions because fo not work

      //var fromWarehouseIdValue = GetPrehashIntValue(fromWarehouseId);
      //var toWarehouseIdValue = GetPrehashIntValue(toWarehouseId);
      //var operationAvailabilityHash = new PermissionHash(fromWarehouseId, toWarehouseId, operationId);// GenerateStringHash(fromWarehouseIdValue, toWarehouseIdValue, operationId);

      //return warehousePermissions.Contains(operationAvailabilityHash, operationAvailabilityHash) && 
      //  (!userPrivileges.Any() ? true : userPrivileges.Contains(operationAvailabilityHash, operationAvailabilityHash));
    }

    struct PermissionHash : IEqualityComparer<PermissionHash>
    {
      int? fromWarehouseId;
      int? toWarehouseId;
      int? operationId;

      public PermissionHash(int? fromWarehouseId, int? toWarehouseId, int? operationId)
      {
        this.fromWarehouseId = fromWarehouseId;
        this.toWarehouseId = toWarehouseId;
        this.operationId = operationId;
      }
      //public int? FromWarehouseId { get; private set; }
      //public int? ToWarehouseId { get; private set; }
      //public int OperationId { get; private set; }

      // override object.Equals
      public override bool Equals(object obj)
      {
        //       
        // See the full list of guidelines at
        //   http://go.microsoft.com/fwlink/?LinkID=85237  
        // and also the guidance for operator== at
        //   http://go.microsoft.com/fwlink/?LinkId=85238
        //

        if (obj == null || GetType() != obj.GetType())
        {
          return false;
        }

        // TODO: write your implementation of Equals() here
        var objY = (PermissionHash)obj;

        return operationId == objY.operationId &&
          ((!fromWarehouseId.HasValue || !objY.fromWarehouseId.HasValue) || fromWarehouseId == objY.fromWarehouseId) &&
          ((!toWarehouseId.HasValue || !objY.toWarehouseId.HasValue) || toWarehouseId == objY.toWarehouseId);
      }

      // override object.GetHashCode
      public override int GetHashCode()
      {
        // TODO: write your implementation of GetHashCode() here
        unchecked // Overflow is fine, just wrap
        {
          int hash = 17;
          hash = hash * 23 + fromWarehouseId.GetHashCode();
          hash = hash * 23 + toWarehouseId.GetHashCode();
          hash = hash * 23 + operationId.GetHashCode();

          return hash;
        }
      }

      public bool Equals(PermissionHash permissionHash)
      {
        // Return true if the fields match:
        return operationId == permissionHash.operationId &&
          ((!fromWarehouseId.HasValue || !permissionHash.fromWarehouseId.HasValue) || fromWarehouseId == permissionHash.fromWarehouseId) &&
          ((!toWarehouseId.HasValue || !permissionHash.toWarehouseId.HasValue) || toWarehouseId == permissionHash.toWarehouseId);
      }

      public static bool operator ==(PermissionHash x, PermissionHash y)
      {
        // If both are null, or both are same instance, return true.
        if (System.Object.ReferenceEquals(x, y))
        {
          return true;
        }

        // If one is null, but not both, return false.
        if (((object)x == null) || ((object)y == null))
        {
          return false;
        }

        // Return true if the fields match:
        return x.Equals(y);
      }

      public static bool operator !=(PermissionHash x, PermissionHash y)
      {
        return !(x == y);
      }

      public override string ToString()
      {
        return string.Format("{0}->{1}:{2}", fromWarehouseId, toWarehouseId, operationId);
      }

      public bool Equals(PermissionHash x, PermissionHash y)
      {
        return x == y;
      }

      public int GetHashCode(PermissionHash obj)
      {
        return obj.GetHashCode();
      }
    }
  }
}
