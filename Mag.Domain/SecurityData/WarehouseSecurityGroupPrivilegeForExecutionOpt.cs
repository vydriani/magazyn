﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.SecurityData
{
	[Serializable]
	public class WarehouseSecurityGroupPrivilegeForExecutionOpt
	{
		public WarehouseSecurityGroupPrivilegeForExecutionOpt(int? fromWarehouseId, int? toWarehouseId, int? operationId, int systemGroupId)
		{
			this.FromWarehouseId = fromWarehouseId;
			this.ToWarehouseId = toWarehouseId;
			this.OperationId = operationId;
			this.SystemGroupId = systemGroupId;
			//int? fromWarehouseId, int? toWarehouseId, int operationId, int systemGroupId
		}

		public int? FromWarehouseId;
		public int? ToWarehouseId;
		public int? OperationId;
		public int SystemGroupId;
	}
}
