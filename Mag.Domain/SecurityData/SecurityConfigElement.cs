﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.SecurityData
{
	[Serializable]
	public class SecurityConfigElement
	{
		public SecurityConfigElement(int? fromWarehouseId, int? toWarehouseId, int? operationId, bool isGranted)
		{
			this.FromWarehouseId = fromWarehouseId;
			this.ToWarehouseId = toWarehouseId;
			this.OperationId = operationId;
			this.IsGranted = isGranted;
		}
		public int? FromWarehouseId;
		public int? ToWarehouseId;
		public int? OperationId;
		public bool IsGranted;
	}
}
