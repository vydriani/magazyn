﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.SecurityData
{
	[Serializable]
	public class JoinedPermission
	{
		public JoinedPermission(int? fromWarehouseId, int? toWarehouseId, List<WarehousePermissionVirtual> permissionsList)
		{
			this.FromWarehouseId = fromWarehouseId;
			this.ToWarehouseId = toWarehouseId;
			this.JoinedWarehousePermissions = permissionsList;
		}
		public int? FromWarehouseId;
		public int? ToWarehouseId;
		public List<WarehousePermissionVirtual> JoinedWarehousePermissions;
	}
}
