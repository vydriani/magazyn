﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;
using System.Diagnostics.Contracts;

namespace Mag.Domain
{
  public class PositionBunchCache
  {
    private class PositionBunchCacheElement
    {
      private int _PositionId;
      private List<List<PositionNode>> _PathBunch;

      public PositionBunchCacheElement(List<List<PositionNode>> bunch, int position_id)
      {
        this._PathBunch = bunch;
        this._PositionId = position_id;
      }

      public int PositionId
      {
        get
        {
          return this._PositionId;
        }
        set
        {
          this._PositionId = value;
        }
      }

      public List<List<PositionNode>> PathBunch
      {
        get
        {
          return this._PathBunch;
        }
        set
        {
          this._PathBunch = value;
        }
      }
    }

    public PositionBunchCache()
    {
      this._elements = new List<PositionBunchCacheElement>();
    }

    private List<PositionBunchCacheElement> _elements;

    public void AddBunch(List<List<PositionNode>> bunch, WarehouseDocumentPosition position)
    {
      Contract.Requires(position != null);

      this._elements.Add(new PositionBunchCacheElement(bunch, position.Id));
    }

    public List<List<PositionNode>> GetBunch(WarehouseDocumentPosition position)
    {
      for (int i = 0; i < this._elements.Count; i++)
      {
        if (_elements[i].PositionId == position.Id)
        {
          return _elements[i].PathBunch;
        }
      }
      return null;
    }

    public void FlushCache()
    {
      this._elements.Clear();
    }
  }
}
