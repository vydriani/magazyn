﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Transactions;
using Mag.Domain.Model;
using System.Diagnostics.Contracts;

namespace Mag.Domain
{
  internal class Operations
  {
    private WarehouseEntities warehouseEntities;

    public Operations(WarehouseEntities warehouseEntitiesEx)
    {
      warehouseEntities = warehouseEntitiesEx;
    }

    public WarehouseDocumentPosition CreateWarehouseDocumentPosition(Material material, float quantity, decimal unitPrice, WarehouseDocument warehouseDocument)
    {
      if (unitPrice < 0) throw new ArgumentException("UnitPrice cannot be null");

      WarehouseDocumentPosition warehouseDocumentPosition = new WarehouseDocumentPosition();

      warehouseDocumentPosition.Material = material;
      warehouseDocumentPosition.Quantity = quantity;
      warehouseDocumentPosition.UnitPrice = unitPrice;
      warehouseDocumentPosition.WarehouseDocument = warehouseDocument;

      return warehouseDocumentPosition;
    }


    /// <summary>
    /// Marek Zakrzewski ; 2010-03-16
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool ExistsWarehouseWithTheName(string name)
    {
      return warehouseEntities.Warehouses.FirstOrDefault(w => w.Name == name) != null;
    }

    public bool ExistsWarehouseGroupWithTheName(string name)
    {
      return warehouseEntities.WarehouseGroups.FirstOrDefault(w => w.Name == name) != null;
    }

    /// <summary>
    /// Marek Zakrzewski ; 2010-03-17
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool ExistsWarehouseWithTheShortName(string shortName)
    {
      return warehouseEntities.Warehouses.FirstOrDefault(w => w.ShortName == shortName) != null;
    }

    public bool ExistsWarehouseGroupWithTheShortName(string shortName)
    {
      return warehouseEntities.WarehouseGroups.FirstOrDefault(w => w.ShortName == shortName) != null;
    }

    //private WarehouseDocument CreateWarehouseDocument(int? contractingPartyId, int warehouseSourceId, int warehouseTargetId, int authorId, WarehouseDocumentTypeEnum warehouseDocumentType)
    //{
    //    Warehouse warehouseSource = warehouseEntities.Warehouse.First(p => p.Id == warehouseSourceId);
    //    Warehouse warehouseTarget = warehouseEntities.Warehouse.First(p => p.Id == warehouseTargetId);
    //    Person person = warehouseEntities.Person.First(p => p.Id == authorId);

    //    return CreateWarehouseDocument(contractingPartyId, warehouseSource, warehouseTarget, person,(int) warehouseDocumentType);
    //}

    //public WarehouseDocument CreateWarehouseDocument(Warehouse source, Warehouse target, Person author, WarehouseDocumentTypeEnum warehouseDocumentType, int? targetDeptSIMPLEId = null)
    //{
    //    return CreateWarehouseDocument(source, target, author, warehouseDocumentType, DateTime.Now);
    //}

    public WarehouseDocument CreateWarehouseDocument
    (
      int? sourceWarehouseId,
      int? targetWarehouseId,
      int authorId,
      WarehouseDocumentTypeEnum warehouseDocumentType,
      DateTime date,
      bool exportDocToSIMPLE = true,
      int? targetDeptSIMPLEId = null,
      int? targetOrderId = null,
      int? docSubtypeSIMPLE = null)
    {
        Contract.Assume(!targetOrderId.HasValue || this.warehouseEntities.Orders != null);

        var warehouseDocument = new WarehouseDocument();
        warehouseDocument.WarehouseSourceId = sourceWarehouseId;//.Warehouse=source;
        warehouseDocument.WarehouseTargetId = targetWarehouseId;//.Warehouse1=target;
        warehouseDocument.AuthorId = authorId;//.Person = author;
        warehouseDocument.TimeStamp = DateTime.Now;
        warehouseDocument.Date = date;
        warehouseDocument.IsFromDataExchange = false;
        warehouseDocument.TargetResponsibleDeptSIMPLEId = targetDeptSIMPLEId;
        warehouseDocument.TargetOrderId = targetOrderId;//.Order = targetOrderId.HasValue ? warehouseEntities.Orders.Where(p => p.Id == targetOrderId).FirstOrDefault() : null;
        warehouseDocument.DocSubtypeSIMPLE = docSubtypeSIMPLE ?? 1;
        warehouseDocument.ExportToSIMPLE = exportDocToSIMPLE;
        warehouseDocument.IsVisible = true;

        int warehouseDocumentTypeInt = (int)warehouseDocumentType;
        warehouseDocument.WarehouseDocumentType = warehouseEntities.WarehouseDocumentTypes.First(p => p.Id == warehouseDocumentTypeInt);

        return warehouseDocument;
    }


    public int CreateWarehouse(string name, string shortName, int companyId, int warehouseTypeId, int userId, int? departmentId)
    {
      int newWarehouseId = -1;

      shortName = shortName.ToUpper();

      // ShortName has to be char(4)
      if (shortName.Length > 0 && shortName.Length <= 7)
      {
        if (warehouseEntities.Warehouses.Any(p => p.Name == name)) return newWarehouseId;

        using (TransactionScope transactionScope = new TransactionScope())
        {
          //Warehouse warehouseWithTheSameName =  warehouseEntities.Warehouse.FirstOrDefault(w => w.Name == name);

          if (!ExistsWarehouseWithTheName(name))
          {
            if (!ExistsWarehouseWithTheShortName(shortName))
            {
              Warehouse warehouse = new Warehouse();
              WarehouseType warehouseType = warehouseEntities.WarehouseTypes.First(p => p.Id == warehouseTypeId);
              //Company company = warehouseEntities.Companies.First(p => p.Id == companyId);

              warehouse.Name = name;
              warehouse.ShortName = shortName;
              warehouse.WarehouseType = warehouseType;
              warehouse.CompanyId = companyId;// .Company = company;
              warehouse.NumLabel = shortName;
              #region Arbitrary defaults - can not be changed in UI yet
              warehouse.Active = true;
              warehouse.ExportDocumentsToSIMPLE = true;
              warehouse.LogicalLocationId = 2;
              warehouse.IsInput = true;
              warehouse.IsOutput = true;
              warehouse.IsProduction = false; //  
              #endregion


              if (departmentId.HasValue)
              {
                WarehouseDepartment wd = new WarehouseDepartment();
                wd.Warehouse = warehouse;
                wd.DepartmentId = departmentId.Value;//.Department = warehouseEntities.Department.First(p => p.Id == department);
                warehouse.WarehouseDepartments.Add(wd);
              }
              warehouseEntities.AddToWarehouses(warehouse);

              warehouseEntities.SaveChanges();

              LogNewWarehouse(warehouse, userId);

              warehouseEntities.SaveChanges();
              transactionScope.Complete();
              warehouseEntities.AcceptAllChanges();

              newWarehouseId = warehouse.Id;
            }
            else
            {
              newWarehouseId = -2;
            }

          }
          else
          {
            newWarehouseId = -1;
          }
        }
      }
      else
      {
        newWarehouseId = -3;
      }

      return newWarehouseId;
    }

    public int CreateWarehouseGroup(string name, string shortName, int userId)
    {
      int newWarehouseGroupId = -1;

      shortName = shortName.ToUpper();

      // ShortName has to be char(4)
      if (shortName.Length > 0 && shortName.Length <= 7)
      {
        if (warehouseEntities.WarehouseGroups.Any(p => p.Name == name))
          return newWarehouseGroupId;

        using (TransactionScope transactionScope = new TransactionScope())
        {
          //Warehouse warehouseWithTheSameName =  warehouseEntities.Warehouse.FirstOrDefault(w => w.Name == name);

          if (!ExistsWarehouseGroupWithTheName(name))
          {
            if (!ExistsWarehouseGroupWithTheShortName(shortName))
            {
              WarehouseGroup warehouseGroup = new WarehouseGroup();
              //WarehouseType warehouseType = warehouseEntities.WarehouseType.First(p => p.Id == warehouseTypeId);
              //Company company = warehouseEntities.Company.First(p => p.Id == companyId);

              warehouseGroup.Name = name;
              warehouseGroup.ShortName = shortName;
              //warehouse.WarehouseType = warehouseType;
              //warehouse.Company = company;

              //if (department != null)
              //{
              //    WarehouseDepartment wd = new WarehouseDepartment();
              //    wd.Warehouse = warehouse;
              //    wd.Department = warehouseEntities.Department.First(p => p.Id == department);
              //    warehouse.WarehouseDepartment.Add(wd);
              //}
              warehouseEntities.AddToWarehouseGroups(warehouseGroup);

              warehouseEntities.SaveChanges();

              //LogNewWarehouse(warehouse, userId);

              warehouseEntities.SaveChanges();
              transactionScope.Complete();
              warehouseEntities.AcceptAllChanges();

              newWarehouseGroupId = warehouseGroup.Id;
            }
            else
            {
              newWarehouseGroupId = -2;
            }

          }
          else
          {
            newWarehouseGroupId = -1;
          }
        }
      }
      else
      {
        newWarehouseGroupId = -3;
      }

      return newWarehouseGroupId;
    }

    public void UpdateCurrentStack(WarehouseDocument warehouseDocument, List<WarehouseDocumentPosition> warehouseDocumentPositions)
    {


      if ((warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.MMPlus).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.POPlus).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Pz).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Pw).Id ||
        //warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.BO).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.PP).Id)
          && warehouseDocumentPositions.Any(p => p.Quantity <= 0)
          )
      {
        throw new ArgumentException("For MMPlus, POPlus, Pz, Pw, BO quantity must be greater than 0");
      }

      //if ((warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.MMMinus).Id) ||
      //    (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.POMinus).Id) ||
      //     (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Wz).Id) ||
      //        (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Rw).Id))
      //    && warehouseDocumentPositions.Any(p => p.Quantity <= 0)
      //    )
      //{
      //    throw new ArgumentException("For MMPlus, POPlus, Pz, Pw, BO quantity must be greater than 0");
      //}



      if (
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.BO).Id
          ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.INW).Id
          )
      {

      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.MMPlus).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.POPlus).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Pz).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Pw).Id ||
        //warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.BO).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.PP).Id)
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions) //.Where(p => p.Quantity > 0))
        {
          if (warehouseDocumentPosition.Quantity <= 0) throw new ArgumentException("For MMPlus, POPlus, Pz, Pw, BO quantity must be greater than 0");
          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p =>
          //        p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse1.Id &&
          //        p.UnitPrice == warehouseDocumentPosition.UnitPrice);

          //if (warehouseCurrentStack == null)
          //{
          //    warehouseCurrentStack = new WarehouseCurrentStack();
          //    warehouseCurrentStack.Warehouse = warehouseDocument.Warehouse1;
          //    warehouseCurrentStack.Material = warehouseDocumentPosition.Material;
          //    warehouseCurrentStack.Quantity = warehouseDocumentPosition.Quantity;
          //    warehouseCurrentStack.UnitPrice = warehouseDocumentPosition.UnitPrice;
          //    warehouseCurrentStack.LastUpdateTime = DateTime.Now;

          //}
          //else
          //{
          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.PzKC).Id)
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions.Where(p => p.Quantity > 0))
        {
          if (warehouseDocumentPosition.Quantity <= 0) throw new ArgumentException("For PzKC quantity must be greater than 0");
          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p =>
          //        p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse1.Id &&
          //        p.UnitPrice == warehouseDocumentPosition.UnitPrice);

          //if (warehouseCurrentStack == null)
          //{
          //    warehouseCurrentStack = new WarehouseCurrentStack();
          //    warehouseCurrentStack.Warehouse = warehouseDocument.Warehouse1;
          //    warehouseCurrentStack.Material = warehouseDocumentPosition.Material;
          //    warehouseCurrentStack.Quantity = warehouseDocumentPosition.Quantity;
          //    warehouseCurrentStack.UnitPrice = warehouseDocumentPosition.UnitPrice;
          //}
          //else
          //{
          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else if ((warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.MMMinus).Id) ||
          (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.POMinus).Id) ||
           (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Wz).Id) ||
          (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.ZTD).Id) ||
              (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Rw).Id))
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions.Where(p => p.Quantity < 0))
        {
          if (warehouseDocumentPosition.Quantity >= 0) throw new ArgumentException("For MMMinus, POMinus, Wz, Rw quantity must be with minus");
          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p =>
          //        p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse.Id); //&&
          ////p.UnitPrice == warehouseDocumentPosition.UnitPrice && p.WarehouseDocumentPosition.Id == warehouseDocumentPosition.WarehouseDocumentPosition2.Id);

          //if (warehouseCurrentStack == null)
          //{
          //    throw new DataException("There is no such material");
          //}
          //else
          //{
          //    //////if (warehouseCurrentStack.Quantity - warehouseDocumentPosition.Quantity < 0) throw new DataException("You cannot give more than you have");

          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.Za).Id)
      {
        //do nothing
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.ZwPlus).Id)
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions)
        {
          if (warehouseDocumentPosition.Quantity <= 0) throw new ArgumentException("For ZwPlus quantity must be greater than 0");
          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p =>
          //        p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse1.Id &&
          //        p.UnitPrice == warehouseDocumentPosition.UnitPrice);

          //if (warehouseCurrentStack == null)
          //{
          //    warehouseCurrentStack = new WarehouseCurrentStack();
          //    warehouseCurrentStack.Warehouse = warehouseDocument.Warehouse1;
          //    warehouseCurrentStack.Material = warehouseDocumentPosition.Material;
          //    warehouseCurrentStack.Quantity = warehouseDocumentPosition.Quantity;
          //    warehouseCurrentStack.UnitPrice = warehouseDocumentPosition.UnitPrice;
          //}
          //else
          //{
          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.ZwMinus).Id)
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions)
        {
          if (warehouseDocumentPosition.Quantity >= 0) throw new ArgumentException("For ZwMinus quantity must be with minus");
          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p =>
          //        p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse.Id); //&&
          ////p.UnitPrice == warehouseDocumentPosition.UnitPrice && p.WarehouseDocumentPosition.Id == warehouseDocumentPosition.WarehouseDocumentPosition2.Id);

          //if (warehouseCurrentStack == null)
          //{
          //    throw new DataException("There is no such material");
          //}
          //else
          //{
          //    //////if (warehouseCurrentStack.Quantity - warehouseDocumentPosition.Quantity < 0) throw new DataException("You cannot give more than you have");

          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.LM).Id)
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions)
        {
          if (warehouseDocumentPosition.Quantity >= 0) throw new ArgumentException("For LM quantity must be with minus");
          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p =>
          //        p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse.Id); //&&
          ////p.UnitPrice == warehouseDocumentPosition.UnitPrice && p.WarehouseDocumentPosition.Id == warehouseDocumentPosition.WarehouseDocumentPosition2.Id);

          //if (warehouseCurrentStack == null)
          //{
          //    throw new DataException("There is no such material");
          //}
          //else
          //{
          //    //////if (warehouseCurrentStack.Quantity - warehouseDocumentPosition.Quantity < 0) throw new DataException("You cannot give more than you have");

          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.PM).Id)
      {
      }
      else if (warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.KPw).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.KRw).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.KMMMinus).Id ||
          warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.KMMPlus).Id)
      {
        foreach (WarehouseDocumentPosition warehouseDocumentPosition in warehouseDocumentPositions)
        {
          ///*if ((warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.KRw).Id ||
          //    warehouseDocument.WarehouseDocumentType.Id == WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.KMMMinus).Id))
          //{
          //    if (warehouseDocumentPosition.Quantity + warehouseDocumentPosition.WarehouseDocumentPositionRelation1.First().WarehouseDocumentPosition.Quantity >= 0)
          //    {
          //        throw new ArgumentException("For MMMinus, Rw quantity must be with minus");
          //    }
          //}
          //else
          //{
          //    if (warehouseDocumentPosition.Quantity + warehouseDocumentPosition.WarehouseDocumentPositionRelation1.First().WarehouseDocumentPosition.Quantity <= 0)
          //    {
          //        throw new ArgumentException("For MMPlus, Pw quantity must be greater than 0");
          //    }
          //}*/

          //WarehouseCurrentStack warehouseCurrentStack =
          //    warehouseEntities.WarehouseCurrentStack.FirstOrDefault(
          //        p => p.Material.Id == warehouseDocumentPosition.Material.Id && p.Warehouse.Id == warehouseDocument.Warehouse.Id);

          //if (warehouseCurrentStack == null)
          //{
          //    throw new DataException("There is no such material");
          //}
          //else
          //{
          //    //////if (warehouseCurrentStack.Quantity - warehouseDocumentPosition.Quantity < 0) throw new DataException("You cannot give more than you have");

          //    warehouseCurrentStack.Quantity += warehouseDocumentPosition.Quantity;
          //}

          //warehouseEntities.SaveChanges();
        }
      }
      else
      {
        throw new NotImplementedException("This operation is not implemented id=" + warehouseDocument.WarehouseDocumentType.Id);
      }
    }

    public void LogNewWarehouse(Warehouse warehouse, int userId)
    {
      Log log = new Log();
      log.Table = warehouseEntities.Tables.First(p => p.Id == 2);
      log.RecordId = warehouse.Id;
      log.TableOperation = warehouseEntities.TableOperations.First(p => p.Id == 1);
      log.Description = "Nowy magazyn został dodany";

      log.Person = warehouseEntities.People.First(p => p.Id == userId);

      //log.UserId = userId;
      log.TimeStamp = DateTime.Now;
    }
  }
}
