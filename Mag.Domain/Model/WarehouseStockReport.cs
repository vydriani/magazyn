﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Model
{
	public partial class WarehouseStockReport
	{
		private bool isTemporary = false;
		public bool IsTemporary
		{
			get { return isTemporary; }
			set
			{
				if (isTemporary != value)
				{
					isTemporary = value;
					OnPropertyChanged("IsTemporary");
				}
			}
		}

		private int? number = null;
		public int? Number
		{
			get { return number; }
			set
			{
				if (number != value)
				{
					number = value;
					OnPropertyChanged("Number");
				}
			}
		}

		private decimal stockQuantity = 0;
		public decimal StockQuantity
		{
			get { return stockQuantity; }
			set
			{
				if (stockQuantity != value)
				{
					stockQuantity = value;
					OnPropertyChanged("StockQuantity");
				}
			}
		}

		public decimal ActualStockQuantity
		{
			get
			{
				if (!IsTemporary && MaterialId > 0 && (WarehouseTargetId.HasValue || WarehouseSourceId.HasValue))
				{
					using (WarehouseEntities warehouseEntities = new WarehouseEntities())
					{
                        var stockForMoment = warehouseEntities.GetMaterialStockForMoment(
                            true,
                            this.WarehouseTargetId.HasValue ? this.WarehouseTargetId.Value : this.WarehouseSourceId.Value,
                            this.MaterialId,
                            this.TimeStamp);

						var stockMaterialized = stockForMoment.ToArray();

						var stock = stockMaterialized.Sum(p => p.Quantity.Value);
						//var test = stockForMoment.ToArray();
						return (decimal)stock; //TODO: Change double to decimal in SPROC
					}
				}
				else
				{
					return 0;
				}
			}
		}

		private decimal? quantityForSupply = null;
		public decimal? QuantityForSupply
		{
			get { return quantityForSupply; }
			set
			{
				if (quantityForSupply != value)
				{
					quantityForSupply = value;
					OnPropertyChanged("QuantityForSupply");
				}
			}
		}

		public string FutureDeliveriesContractingParty
		{
			get
			{
				using (WarehouseEntities warehouseEntities = new WarehouseEntities())
				{
					//var stockForMoment = warehouseEntities.GetMaterialFutureDeliveries(true, this.MaterialId, this.TimeStamp);
					var stockForMoment = warehouseEntities.GetMaterialFutureDeliveries(true, this.MaterialId, DateTime.Now);
					if (stockForMoment != null)
					{
						var firstStockForMoment = stockForMoment.FirstOrDefault();
						if (firstStockForMoment != null)
						{
							return firstStockForMoment.ContractingPartyName;
						}
					}
				}
				return null;
			}
		}

		public decimal? QuantityWaitingForSupply
		{
			get
			{
				if (!IsTemporary)
				{
					using (WarehouseEntities warehouseEntities = new WarehouseEntities())
					{
						//var stockForMoment = warehouseEntities.GetMaterialFutureDeliveries(true, this.MaterialId, this.TimeStamp);
						var stockForMoment = warehouseEntities.GetMaterialFutureDeliveries(true, this.MaterialId, DateTime.Now);

						return stockForMoment.Sum(p => p.IloscZamNetto.Value);
					}
				}
				else
				{
					return null;
				}
			}
		}

		public static IEnumerable<GetMaterialFutureDeliveries_Result> GetFutureDeliveries(int materialId)
		{
			using (WarehouseEntities warehouseEntities = new WarehouseEntities())
			{
				var stockForMoment = warehouseEntities.GetMaterialFutureDeliveries(true, materialId, DateTime.Now);
				if (stockForMoment != null)
				{
					return stockForMoment.ToList();
				}
			}
			return null;
		}

		public DateTime? TimeStamp2
		{
			get
			{
				if (!IsTemporary)
				{
					return TimeStamp;
				}
				else
				{
					return null;
				}
			}
		}

		public decimal? UnitPrice2
		{
			get
			{
				if (!IsTemporary)
				{
					return UnitPrice;
				}
				else
				{
					return null;
				}
			}
		}
	}
}
