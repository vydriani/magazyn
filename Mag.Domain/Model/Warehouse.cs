﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Diagnostics.Contracts;

namespace Mag.Domain.Model
{
  public partial class Warehouse
  {
    public int TempSeqId { get; set; }

    public static PositionWithQuantityAndShelfs CollectWarehouseDocPositionsToBeTaken
    (
      int warehouseId,
      int materialId,
      IEnumerable<string> ordersWithQuantities,
      string targetOrderNumber,
      bool quantitySortOrderAscending = true//,
      //bool checkFullQuantityCollection = false,
      //bool takeValidPositions = true,
      //bool takeInvalidPositions = false,
      //IEnumerable<int> docsToExcludeCollection = null,
      //int? processIdpl = null,
      //bool collectFromParents = true
      )
    {
      bool takeValidPositions = true;
      bool takeInvalidPositions = false;
      //IEnumerable<int> docsToExcludeCollection = null;
      int? processIdpl = null;
      bool collectFromParents = true;

      using (var warehouseEntities = new WarehouseEntities())
      {
        string[] fromOrders = (from p in ordersWithQuantities
                               select p.Split(';')[0]).ToArray();

        var matchingPositions = GetWarehouseStacks(warehouseId, materialId, fromOrders, quantitySortOrderAscending, takeValidPositions, takeInvalidPositions, processIdpl);

        //if (processIdpl.HasValue)
        //{
        //    matchingPositions = matchingPositions.Where(
        //        s => s.ProcessIdpl == processIdpl.Value);
        //}

        //.Where(s => !processIdpl.HasValue || s.ProcessIdpl == processIdpl.Value);

        //if (docsToExcludeCollection != null)
        //{
        //  matchingPositions = matchingPositions.Where(p => !docsToExcludeCollection.Contains(p.WarehouseDocumentId));
        //}

        var positionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();

        positionWithQuantityAndShelfs.documentParentIds = new List<int>();
        positionWithQuantityAndShelfs.positionParentIds = new List<ChildParentPositionQuantity>();
        positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();
        positionWithQuantityAndShelfs.WarehouseDocumentPositionExtensions = new List<WarehouseDocumentPositionExtension>();

        var totalCollectedQuantityForCurrentOrder = 0M;
        var totalCollectedQuantitForAllOrders = 0M;
        var unitPriceSum = 0M;

        var positionExtensions = new List<string>();

        foreach (var orderWithQuantity in ordersWithQuantities)
        {
          var tabs = orderWithQuantity.Split(';');
          string orderNumber = tabs[0];
          var quantity = decimal.Parse(tabs[1], NumberStyles.Any, CultureInfo.InvariantCulture);

          if (quantity > 0)
          {
            //totalQuantity += quantity;
            IQueryable<CurrentStackInDocument> matchingDocumentsForOrder =
              from p in matchingPositions
              where
                  (p.OrderId.HasValue && orderNumber.Length > 0 && p.OrderNumber == orderNumber) ||
                  (!p.OrderId.HasValue && orderNumber.Length == 0)
              select p;

            totalCollectedQuantityForCurrentOrder = 0;
            foreach (var currentStackInDocument in matchingDocumentsForOrder)
            {
              var activeQuantityOfCurrentPos = (decimal)currentStackInDocument.Quantity.Value;
              var missingQuantity = quantity - totalCollectedQuantityForCurrentOrder;

              if (missingQuantity > 0)
              {
                var quantityToCollectFromCurrentPos = missingQuantity > activeQuantityOfCurrentPos ? activeQuantityOfCurrentPos : missingQuantity;

                if (quantityToCollectFromCurrentPos > 0)
                {
                  // Adding document relation:

                  if (collectFromParents)
                  {
                    if (!positionWithQuantityAndShelfs.documentParentIds.Any(p => p == currentStackInDocument.WarehouseDocumentId))
                    {
                      positionWithQuantityAndShelfs.documentParentIds.Add(currentStackInDocument.WarehouseDocumentId);
                    }

                    // Adding position relation:
                    positionWithQuantityAndShelfs.positionParentIds.Add(
                        new ChildParentPositionQuantity()
                        {
                          PositionParentID = currentStackInDocument.PositionId,
                          Quantity = quantityToCollectFromCurrentPos,
                          LocationId = currentStackInDocument.ShelfId
                        });

                    var warehousePositionZone = new WarehousePositionZone();
                    warehousePositionZone.ZoneId = currentStackInDocument.ShelfId;
                    warehousePositionZone.Quantity = (double)quantityToCollectFromCurrentPos;

                    positionWithQuantityAndShelfs.WarehousePositionZones.Add(warehousePositionZone);
                  }

                  totalCollectedQuantitForAllOrders += quantityToCollectFromCurrentPos;
                  unitPriceSum += currentStackInDocument.UnitPrice * (decimal)quantityToCollectFromCurrentPos;
                }
              }
              else
              {
                break;
              }

              var lastPos = positionWithQuantityAndShelfs.positionParentIds.LastOrDefault();
              if (lastPos != null)
                totalCollectedQuantityForCurrentOrder += lastPos.Quantity;

            }
            if (collectFromParents && totalCollectedQuantityForCurrentOrder == decimal.Zero && totalCollectedQuantityForCurrentOrder < quantity)
            {
              // Add not related position
              positionExtensions.Add("<position><collectionError>Total quantity unavailable for OWQ: " + orderWithQuantity + "</collectionError></position>");
            }
          }
          else
          {
            throw new Exception("Warehouse: Quantities must be greater than zero!");
          }
        }

        Order order = null;
        if (!string.IsNullOrEmpty(targetOrderNumber))
        {
          order = warehouseEntities.Orders.Where(p => p.OrderNumber == targetOrderNumber).FirstOrDefault();
        }

        positionWithQuantityAndShelfs.position = new WarehouseDocumentPosition();
        positionWithQuantityAndShelfs.order = order; // clientEntities.Orders.Where(p => p.Id == targetOrderId);
        positionWithQuantityAndShelfs.position.Order = order;
        positionWithQuantityAndShelfs.position.IsDamaged = false;
        positionWithQuantityAndShelfs.position.IsGarbage = false;
        positionWithQuantityAndShelfs.position.IsPartlyDamaged = false;
        positionWithQuantityAndShelfs.position.IsZZ = false;
        positionWithQuantityAndShelfs.position.Material = warehouseEntities.Materials.Where(p => p.Id == materialId).FirstOrDefault();
        positionWithQuantityAndShelfs.position.Quantity = (double)-totalCollectedQuantitForAllOrders;
        positionWithQuantityAndShelfs.position.UnitPrice = totalCollectedQuantitForAllOrders > 0 
          ? unitPriceSum / totalCollectedQuantitForAllOrders
          : 0; // TODO: Check if weighted average calculated correctly

        foreach (string additionalPosInfo in positionExtensions)
        {
          var warehouseDocumentPositionExtension = new WarehouseDocumentPositionExtension();
          //warehouseDocumentPositionExtension.WarehouseDocumentPosition = positionWithQuantityAndShelfs.position;
          warehouseDocumentPositionExtension.Info = additionalPosInfo;
          positionWithQuantityAndShelfs.WarehouseDocumentPositionExtensions.Add(warehouseDocumentPositionExtension);
        }

        return positionWithQuantityAndShelfs;
      }
    }

    private static IQueryable<CurrentStackInDocument> GetWarehouseStacks(
      int warehouseId,
      int materialId,
      string[] fromOrders,
      bool quantitySortOrderAscending = true,
      bool takeValidPositions = true,
      bool takeInvalidPositions = false,
      int? processIdpl = null)
    {
      Contract.Requires(fromOrders != null);
      //if (stacks == null)
      //{
      //  var clientEntities = (ClientEntities)entityManager;
      //  stacks = clientEntities.CurrentStackInDocuments;
      //}
      var fromOrdersList = fromOrders.ToList();

      bool careOfOrders = fromOrdersList.Count > 0;

      using (var warehouseEntities = new WarehouseEntities())
      {
        var matchingStacks = warehouseEntities.CurrentStackInDocuments
          .Where
          (              p =>
            ((takeValidPositions && !p.IsDamaged) || (takeInvalidPositions && p.IsDamaged)) &&
            p.WarehouseTargetId == warehouseId &&
            p.MaterialId == materialId &&
            (
                !careOfOrders
                ||
                (
                    (!p.OrderId.HasValue && fromOrdersList.Contains(""))
                    ||
                    (
                        p.OrderId.HasValue && fromOrdersList.Contains(p.OrderNumber)
                    )
                )
            ) &&
            !p.IsReserved && !p.IsKC
          );

        if (processIdpl.HasValue)
        {
          matchingStacks =
            matchingStacks.Where(p => !p.ProcessIdpl.HasValue || p.ProcessIdpl == processIdpl.Value);
        }

        if (quantitySortOrderAscending)
        {
          return matchingStacks.OrderBy(p => p.Quantity); // In order to take from smallest positions first
        }
        return matchingStacks.OrderByDescending(p => p.Quantity); // In order to take from biggest positions first 
      }
    }
  }
}