﻿namespace Mag.Domain.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum WarehouseDocumentTypeEnum
    {
        Pz = 1,
        Pw = 2,
        Wz = 3,
        Rw = 4,
        MMMinus = 5,
        MMPlus = 6,
        //PrMinus=7, // not used anywhere in code
        //PrPlus=8, // not used anywhere in code
        BO = 9,
        Za = 10,
        ZwMinus = 11,
        ZwPlus = 12,
        PzKC = 13,
        LM = 14,    //  Likwidacja materiału
        PM = 15,    //  Przypisanie materiału
        KPw = 16,   //  Korekta - przychód wewnętrzny
        KRw = 17,   //  Korekta - rozchód wewnętrzny
        KMMMinus = 18,  //  Korekta - przesunięcie międzymagazynowe–rozchód
        KMMPlus = 19,   //  Korekta - przesunięcie międzymagazynowe–przychód
        POMinus = 20, // Przyjęcie odpadów-rozchód
        POPlus = 21, // Przyjęcie odpadów-przychód
        PMinus = 22, // Pożyczka-rozchód 
        PPlus = 23, //Pożyczka-przychód
        //ZPMinus=24, //Zwrot pożyczki-rozchód // not used anywhere in code
        //ZPPlus=25, // Zwrot pożyczki-przychód // not used anywhere in code
        //ZM=26, // Zmiana materiału // not used anywhere in code
        PP = 27, // Przyjęcie do produkcji,
        //PMZ=28, // not used anywhere in code
        UMZ = 29,
        //PMMZ=30, // not used anywhere in code
        ZTD = 31,
        ZL = 32, // 32 Zmiana lokalizacji // not used anywhere in code
        //RC = 33, // 33 (typ nieobsługiwany) // not used anywhere in code
        //URC =34 , // 34 (typ nieobsługiwany) // not used anywhere in code
        INW = 35// INWENTARYZACJA
        //MMMM = 36 //Przesunięcie międzymagazynowe - rozchód
        //MMMP = 37 //Przesunięcie międzymagazynowe - przychód
    }

    public partial class WarehouseDocumentType
    {
        public WarehouseDocumentType()
        { }

        public static WarehouseDocumentType GetEnumValue(WarehouseDocumentTypeEnum warehouseDocumentTypeEnum)
        {
            using (WarehouseEntities warehouseEntities = new WarehouseEntities())
            {
                int warehouseDocumentTypeId = (int)warehouseDocumentTypeEnum;
                return warehouseEntities.WarehouseDocumentTypes.First(p => p.Id == warehouseDocumentTypeId);
            }
        }
    }
}
