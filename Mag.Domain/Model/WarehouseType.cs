﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Model
{
    public enum WarehouseTypeEnum
    {
        WarehouseStandardWithoutRequirements = 1,
        WarehouseStandard,
        WarehouseStandardWithoutOrdering,
        WarehouseDepartmentWithoutOrdering,
        WarehouseDepartmentWithOrdering
    }
}
