﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Model
{
	public partial class WarehouseCustomsChamberPosition
	{
		public bool IsFinished
		{
			get { return this.KCSolvingDocumentTimeStamp.HasValue; }
		}
	}
}
