﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Model
{
	public partial class WarehouseDocumentPosition
	{
		// OrderId and DepartmentId not needed after moder regeneration
		//public int? OrderId
		//{
		//    get
		//    {
		//        return this.Order != null ? this.Order.Id : (int?)null;
		//    }
		//}
		//public int? DepartmentId
		//{
		//    get
		//    {
		//        return this.Department != null ? this.Department.Id : (int?)null;
		//    }
		//}
		private WarehouseDocumentPositionQuantityChange MyQuantityChange
		{
			get { return WarehouseDocumentPositionQuantityChanges.FirstOrDefault(p => !p.TimeCancelled.HasValue && (p.QuestionAnswer == null || !p.QuestionAnswer.TimePlaced.HasValue)); }
		}
		public double ActualQuantity
		{
			get
			{
				if (MyQuantityChange != null)
				{
					return Math.Abs(Math.Abs(Quantity) + MyQuantityChange.QuantityDiff);
				}
				return Math.Abs(Quantity);
			}
		}
		public bool IsLockedByQuantityChange
		{
			get { return MyQuantityChange != null ? MyQuantityChange.QuestionAnswer1 != null && !MyQuantityChange.QuestionAnswer1.TimePlaced.HasValue : false; }
		}
	}
}
