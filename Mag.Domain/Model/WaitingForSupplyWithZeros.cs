﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Model
{
	public partial class WaitingForSupplyWithZeros1
	{
		public int CorrectKontrahentId;

		public int CorrectDostawcaId
		{
			get { return this.kontrachent_id; }
		}

		public string CorrectOrderNumber;
	}
}
