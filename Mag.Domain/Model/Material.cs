﻿using System;
using Mag.Domain;
using Mag.Domain.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;

namespace Mag.Domain.Model
{
    public partial class Material
    {
        public static Material GetOrCreateWarehouseMaterial(string name, int idSimple, int companyId)
        {
            if (name != null)
            {
                name = name.Trim();
            }
            
            using (var warehouseEntities = new WarehouseEntities())
            {
                var materials = warehouseEntities.Materials
                    .Where(m => (m.IdSIMPLE == idSimple && m.CompanyId == companyId));

                var material = materials.FirstOrDefault();

                if (material == null)
                {
                    const int unitId = 21;
                    const int maxNameLength = 80;

                    var materialName = name;

                    material = new Material();
                    //material = warehouseEntities.CreateObject<Material>();
                    material.Name = (materialName != null && materialName.Length > maxNameLength) ? materialName.Substring(0, maxNameLength) : materialName;
                    material.UnitId = unitId;
                    material.IsPrefabricatedElement = false;
                    material.IsPackage = false;
                    material.TimeCreated = DateTime.Now;
                    material.InternalIndex = Guid.NewGuid();
                    material.CompanyId = companyId;

                    var materialFromSimple = warehouseEntities.C_ImportSimple_MaterialFromSimple
                            .FirstOrDefault(m => m.IdSimple == idSimple && m.CompanyId == companyId);
                                        
                    if (materialFromSimple != null && !string.IsNullOrWhiteSpace(materialFromSimple.IndexSIMPLE))
                    {
                        if (!string.IsNullOrEmpty(materialFromSimple.NameSimple) && materialFromSimple.UnitIdSIMPLE.HasValue)
                        {
                            const int maxIndexSIMPLELength = 50;
                            const int maxNameSIMPLELength = 300;

                            int simpleUnitId = decimal.ToInt32(decimal.Round(materialFromSimple.UnitIdSIMPLE.Value, MidpointRounding.AwayFromZero));

                            var indexSimple = materialFromSimple.IndexSIMPLE;
                            indexSimple = indexSimple.Trim();

                            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(indexSimple) &&
                                !name.StartsWith(string.Format("{0} - ", indexSimple)))
                            {
                                materialName = string.Format("{0} - {1}", indexSimple, name);
                            }

                            material.IndexSIMPLE = (indexSimple.Length > maxIndexSIMPLELength)
                                                             ? indexSimple.Substring(0, maxIndexSIMPLELength)
                                                             : indexSimple;
                            string nameSimple = (materialFromSimple.NameSimple.Length > maxNameSIMPLELength)
                                                    ? materialFromSimple.NameSimple.Substring(0, maxNameSIMPLELength)
                                                    : materialFromSimple.NameSimple;
                            if (nameSimple != null)
                            {
                                nameSimple = nameSimple.Trim();
                            }
                            material.ExtendedName = nameSimple;
                            material.IdSIMPLE = idSimple;
                            material.CompanyId = companyId;
                            material.SimpleUnitId = simpleUnitId;

                            var materialClassesFromSimple = warehouseEntities.MaterialClassesFromSIMPLEs
                                .FirstOrDefault(m => m.wytwor_id == idSimple && m.CompanyId == companyId);
                            if (materialClassesFromSimple != null && materialClassesFromSimple.klaswytw_id.HasValue)
                            {
                                material.SIMPLEClassId = decimal.ToInt32(materialClassesFromSimple.klaswytw_id.Value);
                            }

                            var unitWarehouseToSimple = warehouseEntities.UnitWarehouseToSimples
                                .FirstOrDefault(p => p.SIMPLEUnitId == simpleUnitId && p.CompanyId == companyId);
                            if (unitWarehouseToSimple != null)
                            {
                                material.UnitId = unitWarehouseToSimple.WarehouseUnitId;
                            }
                            material.Unit = warehouseEntities.Units.Where(x => x.Id == unitId).SingleOrDefault();

                            //// adding new material from MaterialFromSIMPLEs
                            //MaterialSIMPLE newMaterialSimple = new MaterialSIMPLE()
                            //{
                            //    IndexSIMPLE = material.IndexSIMPLE,
                            //    NameSIMPLE = material.Name,
                            //    IdSIMPLE = idSimple,
                            //    CompanyId = (int)material.CompanyId,
                            //    SimpleUnitId = unitWarehouseToSimple.Id,
                            //    Material = material
                            //};

                            //warehouseEntities.MaterialSIMPLEs.AddObject(newMaterialSimple);
                            warehouseEntities.Materials.AddObject(material);

                            //newMaterialSimple.MaterialReference.a.Attach(material);
                            //material.MaterialSIMPLEs.Add(newMaterialSimple);

                            
                            
                            //material.MaterialSIMPLEs.Add(newMaterialSimple);
                            //material.IdSIMPLE = materialFromSimple.IdSimple;
                            
                            
                            
                            //material.MaterialSIMPLEs.Attach(newMaterialSimple);

                            //warehouseEntities.SaveChanges();

                         
                            
                            //warehouseEntities.AddToMaterials(material);
                            //var entities = warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToList();

                            //foreach (var entry in entities)
                            //{
                            //    if (entry.Entity.GetType() == typeof(Material))
                            //    {
                            //        entry.ChangeState(EntityState.Detached);
                            //    }
                            //}
                            try
                            {
                                warehouseEntities.SaveChanges();
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                //((IObjectContextAdapter)warehouseEntities).ObjectContext.Refresh(RefreshMode.ClientWins, warehouseEntities.Materials);
                                //warehouseEntities.Refresh(RefreshMode.ClientWins, material);
                                //var entities = warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
                                //foreach (var item in entities)
                                //{
                                //    item.EntityKey = new EntityKey();
                                //}
                                //warehouseEntities.Refresh(RefreshMode.ClientWins, warehouseEntities.ObjectStateManager.GetObjectStateEntries(System.Data.EntityState.Added));
                                warehouseEntities.Refresh(RefreshMode.StoreWins, warehouseEntities.Materials);
                                //warehouseEntities.Refresh(RefreshMode.StoreWins, material);
                                //warehouseEntities.Refresh(RefreshMode.ClientWins, warehouseEntities.MaterialSIMPLEs);
                                warehouseEntities.SaveChanges();
                            }
                            
                            
                            //warehouseEntities.SaveChanges();


                            //try
                            //{
                            //    warehouseEntities.SaveChanges();
                            //}
                            //catch (System.Data.OptimisticConcurrencyException e)
                            //{
                            //    warehouseEntities.Refresh(
                            //        System.Data.Objects.RefreshMode.StoreWins,
                            //        warehouseEntities.ObjectStateManager.GetObjectStateEntries(System.Data.EntityState.Modified));
                            //    warehouseEntities.SaveChanges();
                            //}
                            
                        }
                    }
                }
                return material;
            }
        }
    }
}
