﻿using System.Linq;
using System.Diagnostics.Contracts;

namespace Mag.Domain.Model
{
  public partial class WarehouseDocument
  {
    public enum WarehouseDocumentEditStateEnum
    {
      Unknown,
      New,
      ReadyToSave, // includes WaitingForData
      Saved
    }

    public WarehouseDocument()
    { }

    public WarehouseDocumentType DisplayType
    {
      get
      {
        bool isSourceInput = Warehouse != null && Warehouse.IsInput;
        bool isTargetOutput = Warehouse1 != null && Warehouse1.IsOutput;

        var typeAfter = this.WarehouseDocumentType.WarehouseDataExchangeDocTypeMapSIMPLEOverrides1
          .FirstOrDefault(p => p.IsSourceWarehouseInput == isSourceInput && p.IsTargetWarehouseOutput == isTargetOutput);

        if (WarehouseDocumentType.Id == 5)
        {
        }

        if (typeAfter != null)
        {
          return typeAfter.WarehouseDocumentType;
        }
        return this.WarehouseDocumentType;
      }
    }

    public int DocTypeAndWarehouseSerialNumber { get; set; }

    public int DocTypeSerialNumber { get; set; }

    public void AttachWarehouseDocumentPosition(WarehouseDocumentPosition warehouseDocumentPosition)
    {
      Contract.Requires(warehouseDocumentPosition!=null);

      warehouseDocumentPosition.WarehouseDocument = this;
    }
  }
}