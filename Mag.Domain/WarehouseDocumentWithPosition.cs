﻿using Mag.Domain.Model;

namespace Mag.Domain
{
    class WarehouseDocumentWithPosition
    {
        public WarehouseDocument WarehouseDocument { set; get; }
        public WarehouseDocumentPosition WarehouseDocumentPosition { set; get; }
    }
}
