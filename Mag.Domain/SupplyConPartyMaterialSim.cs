﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;

namespace Mag.Domain
{
    public class SupplyConPartyMaterialSim
    {
        public WaitingForSupply waitingForSupply{ get; set;}
        public C_ImportSimple_ContractingPartyFromSimple2 contractingPartyFromSIMPLE { get; set; }
        public C_ImportSimple_MaterialFromSimple materialFromSIMPLE { get; set; }
    }
}
