﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;

namespace Mag.Domain
{
    public class GivingOrderAndHourHeader
    {
        public GivingOrderAndHourHeader(Order order, DateTime dataAndHour, int numberOfPositions)
        {
            Order = order;
            DateAndHour = dataAndHour;
            NumberOfPositions = numberOfPositions;
        }
        public Order Order { get; set; }
        public DateTime DateAndHour { get; set; }
        public int NumberOfPositions { get; set; }
    }
}
