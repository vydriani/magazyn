namespace Mag.Domain
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Objects.DataClasses;
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Transactions;
    using System.Web;
    using System.Xml;
    //using IdeaBlade.EntityModel;
    using Mag.Domain.Data;
    using Mag.Domain.Model;
    using Mag.Domain.SecurityData;
    using System.Data.Objects.SqlClient;
    using MoreLinq;

    public class WarehouseDS : IDisposable
    {
        public enum PositionFeaturedStatus
        {
            Good, // "dobra"
            Damaged, // "zła, Z"
            DamagedFromDamaged, // "zła zła, ZZ"
            Package, //
            Garbage, //
            PartlyDamaged,
        }

        public PositionBunchCache positionBunchCache;
        private ClientSecurityData _clientData;
        private Operations operations;
        private WarehouseEntities warehouseEntities;

        public WarehouseDS()
        {
            warehouseEntities = new WarehouseEntities();
            warehouseEntities.CommandTimeout = 120;
            operations = new Operations(warehouseEntities);
        }

        public WarehouseDS(ClientSecurityData clientData)
            : this()
        {
            warehouseEntities.CommandTimeout = 120;
            this._clientData = clientData;
        }

        public WarehouseEntities WarehouseEntitiesTest
        {
            get { return warehouseEntities; }
        }

        public static PositionWithQuantityAndShelfs CollectPositionsFromStacks(IEnumerable<CurrentStackInDocument> stacks, decimal quantityToCollect)
        {
#if DEBUG
            Contract.Requires(stacks != null);
            Contract.Requires<ArgumentException>(quantityToCollect > decimal.Zero, "Cannot collect zero quantity");
            //Contract.Requires<ArgumentException>(stacks.Sum(p => (decimal?)p.Quantity) <= quantityToCollect, "Could not collect total quantity from given stacks");
#endif
            //if (quantityToCollect <= 0)
            //{
            //  throw new Exception("Cannot collect zero quantity");
            //}
            //if (stacks.Sum(p => p.Quantity) < quantityToCollect)
            //{
            //  throw new Exception("Could not collect total quantity from given stacks");
            //}

            var positionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
            positionWithQuantityAndShelfs.documentParentIds = new List<int>();
            positionWithQuantityAndShelfs.positionParentIds = new List<ChildParentPositionQuantity>();
            positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();

            var totalCollectedQuantity = 0M;

            decimal missingQuantity;
            foreach (var stack in stacks)
            {
                missingQuantity = quantityToCollect - totalCollectedQuantity;
                if (missingQuantity > 0)
                {
                    var quantityCollectedFromCurrentPosition = (decimal?)stack.Quantity > missingQuantity ? missingQuantity : (decimal)stack.Quantity.Value;

                    if (!positionWithQuantityAndShelfs.documentParentIds.Contains(stack.WarehouseDocumentId))
                    {
                        positionWithQuantityAndShelfs.documentParentIds.Add(stack.WarehouseDocumentId);
                    }

                    var childParentPositionQuantity = new ChildParentPositionQuantity();
                    childParentPositionQuantity.PositionParentID = stack.PositionId;
                    childParentPositionQuantity.LocationId = stack.ShelfId;
                    childParentPositionQuantity.Quantity = quantityCollectedFromCurrentPosition;
                    positionWithQuantityAndShelfs.positionParentIds.Add(childParentPositionQuantity);

                    //WarehousePositionZone warehousePositionZone = new WarehousePositionZone();
                    //// TODO: Fill fields
                    //positionWithQuantityAndShelfs.WarehousePositionZones.Add(warehousePositionZone);

                    totalCollectedQuantity += quantityCollectedFromCurrentPosition;
                }
                else
                {
                    break;
                }
            }

            return positionWithQuantityAndShelfs;
        }

        public static PositionWithQuantityAndShelfs CollectPositionsFromStacks(List<WarehouseDocumentPosition> stacks, decimal quantityToCollect)
        {
            //if (quantityToCollect <= 0)
            //{
            //  throw new Exception("Cannot collect zero quantity");
            //}
            //if (stacks.Sum(p => p.Quantity) < quantityToCollect)
            //{
            //  throw new Exception("Could not collect total quantity from given stacks");
            //}

            var positionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
            positionWithQuantityAndShelfs.documentParentIds = new List<int>();
            positionWithQuantityAndShelfs.positionParentIds = new List<ChildParentPositionQuantity>();
            positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();

            var totalCollectedQuantity = 0M;

            decimal missingQuantity;
            foreach (var stack in stacks)
            {
                if (!positionWithQuantityAndShelfs.documentParentIds.Contains(stack.WarehouseDocumentId))
                {
                    positionWithQuantityAndShelfs.documentParentIds.Add(stack.WarehouseDocumentId);
                }

                var childParentPositionQuantity = new ChildParentPositionQuantity();
                childParentPositionQuantity.PositionParentID = stack.Id;
                childParentPositionQuantity.LocationId = 0;
                childParentPositionQuantity.Quantity = (decimal)stack.Quantity;
                positionWithQuantityAndShelfs.positionParentIds.Add(childParentPositionQuantity);
            }

            return positionWithQuantityAndShelfs;
        }

        public static bool IsNullEntity(object entity)
        {
            bool isNullEntity = true;
            if (entity != null)
            {
                Type t = entity.GetType();
                PropertyInfo p = t.GetProperty("Id");
                if (p != null)
                {
                    if (!p.Equals(0))
                    {
                        isNullEntity = false;
                    }
                }
            }
            return isNullEntity;
        }

        public string AddNewCustomsChamberDispositionState(int stateTypeId, int warehouseDocumentPositionId, int operatorId, double quantity, DateTime timeRequired, int? orderId, string comment)
        {
#if DEBUG
            Contract.Requires((stateTypeId == 3 && orderId.HasValue) || (stateTypeId != 3 && orderId == null), "Podano nieprawidłowy numer zlecenia");
#endif
            string message = "";
            bool dbpossibleerr = false;
            try
            {
                //moved to Contract.Requires clause
                //if (stateTypeId == 3)
                //{
                //  if (!orderId.HasValue)
                //    throw new Exception("Brak numeru zlecenia");
                //}
                //else
                //{
                //  orderId = null;
                //}

                var newState = new WarehouseDocumentPositionKCState();
                newState.StateTypeId = stateTypeId;
                newState.WarehouseDocumentPosition = GetPositionById(warehouseDocumentPositionId);
                newState.Quantity = quantity;
                newState.TimeRequired = timeRequired;
                newState.OperatorId = operatorId;
                newState.OrderId = orderId;
                newState.Comment = comment;
                newState.TimeSet = DateTime.Now;
                newState.TimeInvalidated = null;
                newState.IsFinal = true;

                warehouseEntities.WarehouseDocumentPositionKCStates.AddObject(newState);
                dbpossibleerr = true;
                warehouseEntities.SaveChanges();

                message = "Materiały z komory celnej zostały ";
                switch (stateTypeId)
                {
                    case 1:
                        {
                            message = message + "przekazane do zwrotu (oczekują na wydanie do dostawcy)";
                        }
                        break;

                    case 2:
                        {
                            message = message + "wpisane na stan magazynowy";
                        }
                        break;

                    case 3:
                        {
                            var orderNumber = warehouseEntities.Orders.Single(o => o.Id == orderId).OrderNumber;
                            message = message + "przypisane do zlecenia " + orderNumber;
                        }
                        break;

                    case 4:
                        {
                            message = message + "przypisane do działu ???";
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                message = dbpossibleerr ? ex.InnerException.Message : ex.Message;
            }
            return message;
        }

        public WarehouseSupplyMaterialsByHandTmp AddNewSupplyByHand
        (
          int simpleId,
          decimal quantity,
          DateTime supplyDate,
          int firma,
          int ID,
          int IDpl,
          int IDdpl,
          int kontrachent_id,
          string nazwSK,
          bool forDept,
          int flaga,
          decimal valuePerUnit,
          string orderId)
        {
            var newSupply = new WarehouseSupplyMaterialsByHandTmp();
            newSupply.IDSim = simpleId;
            newSupply.IloscZam = Convert.ToDecimal(quantity);
            newSupply.DataKL = supplyDate;
            newSupply.Firma = firma;
            newSupply.ID = ID;
            newSupply.IDpl = IDpl;
            newSupply.IDdpl = IDdpl;
            newSupply.kontrachent_id = kontrachent_id;
            newSupply.NazwSK = nazwSK;
            newSupply.Dzial = 0; // forDept
            newSupply.Flaga = flaga;

            newSupply.NRp = orderId;

            newSupply.UsedQuantity = 0;

            newSupply.ContractingPartyName = warehouseEntities.C_ImportSimple_ContractingPartyFromSimple2
                .Where(x => x.ContractingPartyId == kontrachent_id
                    && x.CompanyId == firma)
                .Select(x => x.ContractingPartyName)
                .FirstOrDefault();

            var material = warehouseEntities.C_ImportSimple_MaterialFromSimple
                .Where(x => x.IdSimple == simpleId
                    && x.CompanyId == firma)
                .FirstOrDefault();

            newSupply.MaterialName = material.NameSimple;

            newSupply.UnitName = material.UnitName;
            newSupply.UnitIdSIMPLE = material.UnitIdSIMPLE;

            newSupply.Cena = valuePerUnit;

            warehouseEntities.AddToWarehouseSupplyMaterialsByHandTmps(newSupply);

            warehouseEntities.SaveChanges();
            return newSupply;
        }

        public void AddSystemUserGroupMembership(int userId, int groupID)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var memberships = warehouseEntities.SystemUserGroupMemberships
                    .Where(x => x.SystemUser.Id == userId
                        && x.SystemGroup.Id == groupID);

                if (memberships.Count() == 0)
                {
                    SystemUserGroupMembership sgm = new SystemUserGroupMembership();

                    sgm.SystemUser = GetSystemUserById(userId);
                    sgm.SystemGroup = GetSystemGroupById(groupID);

                    sgm.CreateTime = DateTime.Now;
                    sgm.SystemUser1 = null; // JUST FOR TESTING
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void AddWarehouseGroupMembership(int warehouseId, int groupID)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var memberships = warehouseEntities.WarehouseGroupMemberships
                    .Where(x => x.Warehouse.Id == warehouseId
                        && x.WarehouseGroup.Id == groupID);

                if (memberships.Count() == 0)
                {
                    WarehouseGroupMembership wgm = new WarehouseGroupMembership();

                    wgm.Warehouse = GetWarehouseById(warehouseId);
                    wgm.WarehouseGroup = GetWarehouseGroupById(groupID);
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();

            }
        }

        public void AddWarehouseGroupPermission(int? fromWarehouseGroupId, int? toWarehouseGroupId, int operationId, bool AllowedMaterialIsPackage, bool AllowedPositionIsZZ, bool AllowedPositionIsGarbage, bool AllowedPositionIsDamaged, bool AllowedPositionIsPartlyDamaged)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                // Check if the connection already exists
                if (CheckWarehouseGroupPermission(fromWarehouseGroupId, toWarehouseGroupId, operationId) == null)
                {
                    // If not, create a connection
                    var wp = new WarehouseGroupPermission();
                    wp.WarehouseGroup = fromWarehouseGroupId != null ? GetWarehouseGroupById(fromWarehouseGroupId.Value) : null;
                    wp.WarehouseGroup1 = toWarehouseGroupId != null ? GetWarehouseGroupById(toWarehouseGroupId.Value) : null;
                    wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);

                    wp.GrantTime = DateTime.Now;

                    wp.MaterialIsPackage = AllowedMaterialIsPackage;
                    wp.PositionIsZZ = AllowedPositionIsZZ;
                    wp.PositionIsGarbage = AllowedPositionIsGarbage;
                    wp.PositionIsDamaged = AllowedPositionIsDamaged;
                    wp.PositionIsPartlyDamaged = AllowedPositionIsPartlyDamaged;
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void AddWarehousePermission(int? fromWarehouseId, int? toWarehouseId, int operationId, bool AllowedMaterialIsPackage, bool AllowedPositionIsZZ, bool AllowedPositionIsGarbage, bool AllowedPositionIsDamaged, bool AllowedPositionIsPartlyDamaged)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                object permission = null;
                using (TransactionScope transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    permission = CheckWarehousePermission(fromWarehouseId, toWarehouseId, operationId).FirstOrDefault();

                    transactionScope2.Complete();
                }

                // Check if the connection already exists
                if (permission == null)
                {
                    // If not, create a connection
                    var wp = new WarehousePermission();
                    wp.Warehouse = fromWarehouseId != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    wp.Warehouse1 = toWarehouseId != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);

                    wp.MaterialIsPackage = AllowedMaterialIsPackage;
                    wp.PositionIsZZ = AllowedPositionIsZZ;
                    wp.PositionIsGarbage = AllowedPositionIsGarbage;
                    wp.PositionIsDamaged = AllowedPositionIsDamaged;
                    wp.PositionIsPartlyDamaged = AllowedPositionIsPartlyDamaged;
                    wp.GrantTime = DateTime.Now;
                    wp.RevokeTime = null;
                }
                else
                {
                    throw new Exception("Podane uprawnienie już istnieje. Sprawdź także uprawnienia grupowe.");
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public WarehouseDocument AssignMaterialToOrder(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            return AssignMaterialToOrder(warehouseSourceId, authorId, positionWithQuantityAndShelfses, DateTime.Now);
        }

        public WarehouseDocument AssignMaterialToOrder(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, DateTime date)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource = null;
            //Warehouse warehouseTarget = null;
            //Person person = null;

            //TranslateParameters(warehouseSourceId, warehouseSourceId, authorId, out warehouseSource, out warehouseTarget, out person);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseSourceId, authorId, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.PM, date);
                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        public void AssignMaterialToOrder(int materialId, int orderId, int warehouseId, float quantity)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void Bind(List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            foreach (var positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                Bind(positionWithQuantityAndShelf);
            }

            warehouseEntities.AcceptAllChanges();
        }

        public void Bind(PositionWithQuantityAndShelfs positionWithQuantityAndShelfs)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfs != null);
#endif
            if (positionWithQuantityAndShelfs.WarehousePositionZones != null)
            {
                foreach (WarehousePositionZone warehousePositionZone in positionWithQuantityAndShelfs.WarehousePositionZones)
                {
                    if (warehousePositionZone.Zone != null && warehousePositionZone.Zone.Id > 0)
                    {
                        WarehousePositionZone newWarehousePositionZone = new WarehousePositionZone();
                        newWarehousePositionZone.WarehouseDocumentPosition =
                            warehouseEntities.WarehouseDocumentPositions.First(p => p.Id == warehousePositionZone.WarehouseDocumentPosition.Id);
                        newWarehousePositionZone.Zone = warehouseEntities.Zones.First(z => z.Id == warehousePositionZone.Zone.Id);
                        newWarehousePositionZone.Quantity = warehousePositionZone.Quantity;
                        warehouseEntities.SaveChanges();
                    }
                }
            }
        }

        public void CalculateCostOfUsedMaterials()
        {
            throw new NotImplementedException();
        }

        public void CallToCollect()
        {
            throw new NotImplementedException();
        }

        public string CancelKCDisposition(int KCStateId)
        {
            string result = "";

            var state =
                warehouseEntities.WarehouseDocumentPositionKCStates.Where(s => s.Id == KCStateId).First();

            state.TimeInvalidated = DateTime.Now;

            warehouseEntities.SaveChanges();
            //using (TransactionScope transactionScope = new TransactionScope())
            //{
            //    transactionScope.Complete();
            //}
            result = "Dyspozycja została anulowana!";

            return result;
        }

        public string ChangeKCRequiredTime(int KCStateId, DateTime newTime)
        {
            string result = "";

            if (newTime > DateTime.Now.AddMinutes(5))
            {
                var state =
                    warehouseEntities.WarehouseDocumentPositionKCStates.Where(s => s.Id == KCStateId).First();
                state.TimeRequired = newTime;

                warehouseEntities.SaveChanges();
                //using (TransactionScope transactionScope = new TransactionScope())
                //{
                //    transactionScope.Complete();
                //}
                result = "Zmieniono czas realizacji dyspozycji!";
            }
            else
            {
                result = "Błędny czas!";
            }

            return result;
        }

        public void ChangeShelf(int warehouseDocumentPositionId, int newZoneId)
        {
            using (var transactionScope = new TransactionScope())
            {
                var warehousePositionZone = warehouseEntities.WarehousePositionZones.First(p => p.WarehouseDocumentPosition.Id == warehouseDocumentPositionId);
                warehousePositionZone.Zone = warehouseEntities.Zones.First(p => p.Id == newZoneId);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void ChangeSIMPLEIndex(Material oldMaterial, Material newMaterial)
        {
            throw new NotImplementedException();
        }

        public void ChangeWarehouseDocumentPositionQuantity(int positionId, double newQuantity, int authorId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                WarehouseDocumentPosition warehouseDocumentPosition = GetWarehouseDocumentPositionById(positionId);
                if (warehouseDocumentPosition.IsLockedByQuantityChange)
                {
                    throw new Exception(
                        "Nie można ponownie zmienić ilości dla pozycji, która oczekuje na akceptację poprzedniej zmiany przez kierownika.");
                }
                if (newQuantity == warehouseDocumentPosition.ActualQuantity)
                {
                    throw new Exception("Nowa ilość musi być różna od poprzedniej.");
                }

                double quantityDiff = newQuantity - Math.Abs(warehouseDocumentPosition.Quantity);

                if (quantityDiff > 0)
                {
                    // Check stacks
                    var stacks =
                        warehouseEntities.CurrentStackInDocuments.Where(
                            s =>
                            s.WarehouseTargetId == warehouseDocumentPosition.WarehouseDocument.Warehouse.Id &&
                            s.OrderId == warehouseDocumentPosition.OrderId &&
                            s.ProcessIdpl == warehouseDocumentPosition.ProcessIdpl);

                    var stacksSum = stacks.Sum(p => p.Quantity);

                    double stacksQuantity = stacksSum != null ? stacksSum.Value : 0;

                    if (stacksQuantity < quantityDiff)
                    {
                        throw new Exception("Ilość materiału na stanach magazynowych jest niewystarczająca do wydania dodatkowej ilości.<br/>Dostępna ilość: "+stacksQuantity.ToString());
                    }
                }

                // Applying change:
                WarehouseDocumentPositionQuantityChange warehouseDocumentPositionQuantityChange =
                    new WarehouseDocumentPositionQuantityChange();
                warehouseDocumentPositionQuantityChange.AuthorId = authorId;
                warehouseDocumentPositionQuantityChange.WarehouseDocumentPositionId = positionId;
                warehouseDocumentPositionQuantityChange.TimeRequested = DateTime.Now;
                warehouseDocumentPositionQuantityChange.QuantityDiff = quantityDiff;
                warehouseEntities.AddToWarehouseDocumentPositionQuantityChanges(warehouseDocumentPositionQuantityChange);
                warehouseEntities.SaveChanges();

                transactionScope.Complete();
            }
        }

        public bool CheckGiveWasNotAlreadyReceived(int warehouseDocumentId)
        {
            int zwEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            int mmMinusEnumInt = (int)WarehouseDocumentTypeEnum.MMMinus;
            int mmPlusEnumInt = (int)WarehouseDocumentTypeEnum.MMPlus;

            WarehouseDocumentRelation OrderWithMMMinus = warehouseEntities.WarehouseDocumentRelations.Where(
                    order => order.WarehouseDocument.WarehouseDocumentType.Id == zwEnumInt &&
                            order.WarehouseDocument1.Id == warehouseDocumentId &&
                            order.WarehouseDocument1.WarehouseDocumentType.Id == mmMinusEnumInt).FirstOrDefault();

            if (OrderWithMMMinus == null)
                throw new DataException("Database lost its consistency or this document has been not ordered idDocument=" +
                                        warehouseDocumentId);

            WarehouseDocumentRelation MMMinusWithMMPlus = warehouseEntities.WarehouseDocumentRelations.Where(
                    order => order.WarehouseDocument.WarehouseDocumentType.Id == mmMinusEnumInt &&
                            order.WarehouseDocument.Id == warehouseDocumentId &&
                            order.WarehouseDocument1.WarehouseDocumentType.Id == mmPlusEnumInt).FirstOrDefault();

            if (MMMinusWithMMPlus == null) return true;

            return false;
        }

        public bool CheckIfPackageTypeExists(int unitId, int quantity, string name)
        {
            return
                warehouseEntities.PackageTypes.Include("Unit").Where(p => p.Unit.Id == unitId && p.Quantity == quantity && p.Name.Trim().ToLower() == name.Trim().ToLower()).
                    Count() == 1;
        }

        public bool CheckIfWarehouseInGroup(Warehouse warehouse, WarehouseGroup warehouseGroup)
        {
            if (warehouse != null && warehouseGroup != null)
            {
                var memberships = GetAllGroupMembershipsForWarehouse(warehouse.Id);
                var groups = memberships.Where(x => x.WarehouseGroup.Id == warehouseGroup.Id);
                return groups.Count() > 0;
            }
            else
                return false;
        }

        public string CheckOperationAvailability(int? fromWarehouseId, int? toWarehouseId, WarehouseDocumentTypeEnum operation, int? systemUserId)
        {
#if DEBUG
            Contract.Requires(!fromWarehouseId.HasValue || fromWarehouseId.Value > 0, "fromWarehouseId DB column cannot be less than 1");
            Contract.Requires(!toWarehouseId.HasValue || toWarehouseId.Value > 0, "toWarehouseId DB column cannot be less than 1");
            Contract.Requires(!systemUserId.HasValue || systemUserId.Value > 0, "systemUserId DB column cannot be less than 1");
#endif
            return string.Empty; //NOTICE: on request we're disabling all permissions because fo not work

            if (this._clientData != null && this._clientData.CurrentUserSecurityConfigMatrix != null)
            {
                int operationId = Convert.ToInt32(operation);

                bool permittedAndPrivileged = this._clientData.CurrentUserSecurityConfigMatrix.CheckOperationAvailability(fromWarehouseId, toWarehouseId, operationId);

                if (permittedAndPrivileged)
                {
                    return "";
                }
                else
                {
                    return string.Format("OPERACJA (fromId:{0},toId:{1},opId:{2}) NIEDOZWOLONA dla userId:{3}", fromWarehouseId, toWarehouseId, operation, systemUserId);
                }
            }
            else
            {
                return "BŁĄD";
            }
        }

        public bool CheckOrderWasNotAlreadyGiven(int warehouseDocumentId)
        {

            return true;  //allow already given orders

            //int zwEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            //int mmMinusEnumInt = (int)WarehouseDocumentTypeEnum.MMMinus;

            //WarehouseDocumentRelation orderWithMMMinus = warehouseEntities.WarehouseDocumentRelations.Where(
            //        order => order.WarehouseDocument.WarehouseDocumentType.Id == zwEnumInt &&
            //                order.WarehouseDocument.Id == warehouseDocumentId &&
            //                order.WarehouseDocument1.WarehouseDocumentType.Id == mmMinusEnumInt).FirstOrDefault();

            //if (orderWithMMMinus == null) return true;

            //return false;
        }

        public bool CheckSimpleExportPossibility(int warehouseId, int operatorId, int documentTypeId = 0)
        {
            var warehouse = GetWarehouseById(warehouseId);

            return
                IsWarehouseMappedToSIMPLE(warehouseId)
                &&
                //(warehouseTargetId.HasValue ? IsWarehouseMappedToSIMPLE(warehouse2Id.Value) : true)
                //&&
                IsOperatorMappedToSIMPLEAccount(operatorId, warehouse.Company.Id)
                ;
        }

        public bool CheckUserPrivilegeForOperation(int? fromWarehouseId, int? toWarehouseId, WarehouseDocumentTypeEnum operation, int? systemUserId)
        {
            int operationId = Convert.ToInt32(operation);

            if (systemUserId.HasValue)
            {
                var userGroupMemberships = GetAllSystemUserGroupMemberships(systemUserId.Value);

                // Check if user in has admin privileges for appropriate dept
                bool userHasAdminPrivilegeForDept = false;
                var warehouseDocumentType =
                    warehouseEntities.WarehouseDocumentTypes.Where(p => p.Id == (int)operation).FirstOrDefault();

                if (warehouseDocumentType != null)
                {
                    int? warehouseToCheckId = null;
                    if (warehouseDocumentType.AllowIssueForAdmOfWarehouse == 1)
                    {
                        // Check admin priv for source warehouse
                        warehouseToCheckId = fromWarehouseId;
                    }
                    else if (warehouseDocumentType.AllowIssueForAdmOfWarehouse == 2)
                    {
                        // Check admin priv for target warehouse
                        warehouseToCheckId = toWarehouseId;
                    }

                    var userPrivileges = GetSecurityPrivilegesForUser(systemUserId.Value);

                    if (userPrivileges.Any(p => p.SecurityPrivilegeId == 24)) // Privilege 24 = WarehouseMasterOperationAdmin
                    {
                        userHasAdminPrivilegeForDept = true;
                    }

                    if (!userHasAdminPrivilegeForDept && warehouseToCheckId.HasValue)
                    {
                        if (userPrivileges.Any(p => p.SecurityPrivilegeId == 23 && int.Parse(p.Value) == warehouseToCheckId.Value)) // Privilege 23 = WarehouseOperationAdminForWarehouse
                        {
                            userHasAdminPrivilegeForDept = true;
                        }
                    }
                }

                if (!userHasAdminPrivilegeForDept)
                {
                    if (fromWarehouseId.HasValue && fromWarehouseId.Value > 0)
                    {
                        // Checking user privileges for read
                        bool hasUserPrivilegeForRead = SecurityCheckUserPriviledgeForRead(fromWarehouseId.Value, systemUserId.Value);

                        // Checking group privileges for read
                        bool hasGroupPrivilegeForRead = false;

                        foreach (var membership in userGroupMemberships)
                        {
                            if (SecurityCheckGroupPriviledgeForRead(fromWarehouseId.Value, membership.SystemGroup.Id))
                            {
                                hasGroupPrivilegeForRead = true;
                                break;
                            }
                        }

                        if (!hasUserPrivilegeForRead && !hasGroupPrivilegeForRead)
                        {
                            //message = message + " Brak uprawnień użytkownika do odczytu magazynu źródłowego (" + fromWarehouseId.ToString() + ")";
                            return false;
                        }
                    }

                    if (toWarehouseId.HasValue && toWarehouseId.Value > 0)
                    {
                        // Checking user privileges for read
                        bool hasUserPrivilegeForRead = SecurityCheckUserPriviledgeForRead(toWarehouseId.Value, systemUserId.Value);

                        // Checking group privileges for read
                        bool hasGroupPrivilegeForRead = false;

                        foreach (var membership in userGroupMemberships)
                        {
                            if (SecurityCheckGroupPriviledgeForRead(toWarehouseId.Value, membership.SystemGroup.Id))
                            {
                                hasGroupPrivilegeForRead = true;
                                break;
                            }
                        }

                        if (!hasUserPrivilegeForRead && !hasGroupPrivilegeForRead)
                        {
                            //message = message + " Brak uprawnień użytkownika do odczytu magazynu docelowego (" + toWarehouseId.ToString() + ")";
                            return false;
                        }
                    }

                    // Checking group privileges
                    bool hasGroupPrivilegeForExec = false;
                    bool hasUserPrivilegeForExec = false;

                    foreach (var membership in userGroupMemberships)
                    {
                        if (SecurityCheckGroupPriviledgeForExecution(fromWarehouseId, toWarehouseId, operationId, membership.SystemGroup.Id))
                        {
                            hasGroupPrivilegeForExec = true;
                            break;
                        }
                    }

                    if (!hasGroupPrivilegeForExec)
                    {
                        // Checking user privileges
                        hasUserPrivilegeForExec = SecurityCheckUserPriviledgeForExecution(fromWarehouseId, toWarehouseId, operationId, systemUserId.Value);
                    }

                    if (!hasUserPrivilegeForExec && !hasGroupPrivilegeForExec)
                    {
                        //message = message + " Brak uprawnień użytkownika do wykonywania operacji " + operation.ToString() + " na tych magazynach (" + (fromWarehouseId != null ? fromWarehouseId.ToString() : "?") + "," + (toWarehouseId != null ? toWarehouseId.ToString() : "?") + ")";
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public WarehouseGroupPermission CheckWarehouseGroupPermission(int? fromWarehouseGroupId, int? toWarehouseGroupId, WarehouseDocumentTypeEnum operation)
        {
            int operationId = Convert.ToInt32(operation);
            return CheckWarehouseGroupPermission(fromWarehouseGroupId, toWarehouseGroupId, operationId);
        }

        public WarehouseGroupPermission CheckWarehouseGroupPermission(int? fromWarehouseGroupId, int? toWarehouseGroupId, int operationId)
        {
            var permissions = warehouseEntities.WarehouseGroupPermissions
                .Include(x => x.WarehouseGroup)
                .Where(x => !x.RevokeTime.HasValue
                    && (x.WarehouseGroup == null) || (x.WarehouseGroup != null && fromWarehouseGroupId != null && x.WarehouseGroup.Id == fromWarehouseGroupId)
                    && (x.WarehouseGroup1 == null) || (x.WarehouseGroup1.Id != null && toWarehouseGroupId != null && x.WarehouseGroup1.Id == toWarehouseGroupId)
                    && (x.WarehouseDocumentType.Id == operationId));

            return permissions.FirstOrDefault();
        }

        public string CheckWarehouseGroupPermissionForStatuses(int? fromWarehouseGroupId, int? toWarehouseGroupId, int operationId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses)
        {
            StringBuilder sb = new StringBuilder();

            var permission = CheckWarehouseGroupPermission(fromWarehouseGroupId, toWarehouseGroupId, operationId);

            if (permission != null)
            {
                if (positionWithQuantityAndShelfses != null && positionWithQuantityAndShelfses.Count > 0)
                {
                    // Checking availability of operation for all position and material statuses
                    foreach (PositionWithQuantityAndShelfs positionWQAS in positionWithQuantityAndShelfses)
                    {
                        // ???????????????? ADD THE PosWQAS ???? OR ONLY PARENTS - CHECK IT!

                        foreach (var position in GetParentPositionsOfPositionWithQuantityAndShelfs(positionWQAS))
                        {
                            var statuses = GetPositionFeaturedStatusList(position);

                            //WarehouseDocumentPosition _position = positionWQAS.position;
                            var material = position.Material;

                            //bool MaterialIsPackage = material.IsPackage;
                            //bool PositionIsGarbage = position.IsGarbage;
                            //bool PositionIsZZ = position.IsZZ;
                            //bool PositionIsDamaged = position.IsDamaged;
                            //bool PositionIsPartlyDamaged = position.IsPartlyDamaged;

                            if (statuses.Contains(PositionFeaturedStatus.Package) && !permission.MaterialIsPackage)
                            {
                                sb.Append(" Materiał '" + material.Name + "' jest opakowaniem.");
                            }
                            if (statuses.Contains(PositionFeaturedStatus.Garbage) && !permission.PositionIsGarbage)
                            {
                                sb.Append(" Pozycja '" + position.Material.Name + "' jest odpadem.");
                            }
                            if (statuses.Contains(PositionFeaturedStatus.DamagedFromDamaged) && !permission.PositionIsZZ)
                            {
                                sb.Append(" Pozycja '" + position.Material.Name + "' jest ZZ.");
                            }
                            if (statuses.Contains(PositionFeaturedStatus.Damaged) && !permission.PositionIsDamaged)
                            {
                                sb.Append(" Pozycja '" + position.Material.Name + "' jest uszkodzona.");
                            }
                            if (statuses.Contains(PositionFeaturedStatus.PartlyDamaged) && !permission.PositionIsPartlyDamaged)
                            {
                                sb.Append(" Pozycja '" + position.Material.Name + "' jest częściowo uszkodzona.");
                            }
                        }
                    }
                }
            }

            return sb.ToString();
        }

        public IQueryable<WarehousePermissionVirtual> CheckWarehousePermission(int? fromWarehouseId, int? toWarehouseId, WarehouseDocumentTypeEnum operation)
        {
            int operationId = Convert.ToInt32(operation);
            return CheckWarehousePermission(fromWarehouseId, toWarehouseId, operationId);
        }

        public IQueryable<WarehousePermissionVirtual> CheckWarehousePermission(int? fromWarehouseId, int? toWarehouseId, int operationId)
        {
            IEnumerable<WarehousePermissionVirtual> joinedPermissions = null;

            if (_clientData != null && _clientData.JoinedWarehousePermissionsLst != null && _clientData.JoinedWarehousePermissionsLst.Any(p => p.FromWarehouseId == fromWarehouseId && p.ToWarehouseId == toWarehouseId))
            {
                joinedPermissions = (from q in
                                         _clientData.JoinedWarehousePermissionsLst.Where(p => p.FromWarehouseId == fromWarehouseId && p.ToWarehouseId == toWarehouseId)
                                     select q.JoinedWarehousePermissions).FirstOrDefault();
            }
            else
            {
                joinedPermissions = GetJoinedWarehousePermissions(fromWarehouseId, toWarehouseId);

                if (_clientData != null)
                {
                    _clientData.JoinedWarehousePermissionsLst = new List<JoinedPermission>();
                    _clientData.JoinedWarehousePermissionsLst.Add(new JoinedPermission(fromWarehouseId, toWarehouseId, joinedPermissions.ToList()));
                }
            }

            return joinedPermissions.Where(p => p.WarehouseDocumentType.Id == operationId).AsQueryable();
        }

        public string CheckWarehousePermissionForStatuses(int? fromWarehouseId, int? toWarehouseId, int operationId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses)
        {
            StringBuilder sb = new StringBuilder();

            IQueryable<WarehousePermissionVirtual> permissions = CheckWarehousePermission(fromWarehouseId, toWarehouseId, operationId);

            foreach (WarehousePermissionVirtual permission in permissions)
            {
                if (permission != null)
                {
                    if (positionWithQuantityAndShelfses != null && positionWithQuantityAndShelfses.Count > 0)
                    {
                        // Checking availability of operation for all position and material statuses
                        foreach (PositionWithQuantityAndShelfs positionWQAS in positionWithQuantityAndShelfses)
                        {
                            // ???????????????? ADD THE PosWQAS ???? OR ONLY PARENTS - CHECK IT!

                            foreach (WarehouseDocumentPosition position in GetParentPositionsOfPositionWithQuantityAndShelfs(positionWQAS))
                            {
                                var statuses = GetPositionFeaturedStatusList(position);

                                //WarehouseDocumentPosition _position = positionWQAS.position;
                                Material material = position.Material;

                                //bool MaterialIsPackage = material.IsPackage;
                                //bool PositionIsGarbage = position.IsGarbage;
                                //bool PositionIsZZ = position.IsZZ;
                                //bool PositionIsDamaged = position.IsDamaged;
                                //bool PositionIsPartlyDamaged = position.IsPartlyDamaged;

                                if (statuses.Contains(PositionFeaturedStatus.Package) && !permission.MaterialIsPackage)
                                {
                                    sb.Append(" Materiał '" + material.Name + "' jest opakowaniem.");
                                }
                                if (statuses.Contains(PositionFeaturedStatus.Garbage) && !permission.PositionIsGarbage)
                                {
                                    sb.Append(" Pozycja '" + position.Material.Name + "' jest odpadem.");
                                }
                                if (statuses.Contains(PositionFeaturedStatus.DamagedFromDamaged) && !permission.PositionIsZZ)
                                {
                                    sb.Append(" Pozycja '" + position.Material.Name + "' jest ZZ.");
                                }
                                if (statuses.Contains(PositionFeaturedStatus.Damaged) && !permission.PositionIsDamaged)
                                {
                                    sb.Append(" Pozycja '" + position.Material.Name + "' jest uszkodzona.");
                                }
                                if (statuses.Contains(PositionFeaturedStatus.PartlyDamaged) && !permission.PositionIsPartlyDamaged)
                                {
                                    sb.Append(" Pozycja '" + position.Material.Name + "' jest częściowo uszkodzona.");
                                }
                            }
                        }
                    }
                }
            }

            //WarehousePermissionVirtual permission = CheckWarehousePermission(fromWarehouseId, toWarehouseId, operationId).FirstOrDefault();

            return sb.ToString();
        }

        public bool CheckWarehousePermissionYesOrNoOnly(int? sourceWarehouseId, int? targetWarehouseId, int operationId)
        {
            var groupPermissions = GetAllGroupPermissions(operationId).ToArray();
            var warehousePermissionsForGrid = new List<WarehousePermissionVirtual>();

            foreach (var permission in groupPermissions)
            {
                int perm_id = permission.Id;
                var wg1 = permission.WarehouseGroup;
                var wg2 = permission.WarehouseGroup1;

                // Check if the group contains warehouse
                bool sourceWarehouseInGroup = false;
                bool targetWarehouseInGroup = false;

                if (sourceWarehouseId.HasValue && sourceWarehouseId > 0)
                {
                    sourceWarehouseInGroup = CheckIfWarehouseInGroup(GetWarehouseByIdIB(sourceWarehouseId.Value), permission.WarehouseGroup);
                }

                if (targetWarehouseId.HasValue && targetWarehouseId > 0)
                {
                    targetWarehouseInGroup = CheckIfWarehouseInGroup(GetWarehouseByIdIB(targetWarehouseId.Value), permission.WarehouseGroup1);
                }

                if (
                    (
                        sourceWarehouseId == 0
                        ||
                        sourceWarehouseInGroup
                        ||
                        (
                            (
                                permission.WarehouseGroup == null || permission.WarehouseGroup.Id == 0
                            )
                            && sourceWarehouseId == null
                        )
                    )
                    &&
                    (
                        targetWarehouseId == 0
                        ||
                        targetWarehouseInGroup
                        ||
                        (
                            (
                                permission.WarehouseGroup1 == null || permission.WarehouseGroup1.Id == 0
                            )
                            && targetWarehouseId == null
                        )
                    )
                    )
                {
                    WarehousePermissionVirtual wpfg = new WarehousePermissionVirtual();

                    wpfg.WarehouseId = null;
                    wpfg.Warehouse1Id = null;

                    wpfg.WarehouseId = sourceWarehouseId;
                    wpfg.Warehouse1Id = targetWarehouseId;

                    if (wpfg.WarehouseId != null)
                    {
                        wpfg.Warehouse = GetWarehouseById(Convert.ToInt32(wpfg.WarehouseId));
                    }
                    if (wpfg.Warehouse1Id != null)
                    {
                        wpfg.Warehouse1 = GetWarehouseById(Convert.ToInt32(wpfg.Warehouse1Id));
                    }

                    wpfg.WarehouseDocumentType = GetDocumentTypeList("").Where(p => p.Id == permission.WarehouseDocumentType.Id).FirstOrDefault();

                    wpfg.MaterialIsPackage = permission.MaterialIsPackage;
                    wpfg.PositionIsGarbage = permission.PositionIsGarbage;
                    wpfg.PositionIsZZ = permission.PositionIsZZ;
                    wpfg.PositionIsPartlyDamaged = permission.PositionIsPartlyDamaged;
                    wpfg.PositionIsDamaged = permission.PositionIsDamaged;

                    //warehousePermissionsForGrid.Add(wpfg);
                    if (!warehousePermissionsForGrid.Any(
                        p =>
                            p.WarehouseDocumentType.Id == wpfg.WarehouseDocumentType.Id
                            &&
                            p.WarehouseId == wpfg.WarehouseId
                            &&
                            p.Warehouse1Id == wpfg.Warehouse1Id
                            ))
                    {
                        //warehousePermissionsForGrid.Add(wpfg);
                        return true;
                    }
                }
            }

            if (targetWarehouseId > 1)
            {
                //int y = 0;
            }

            // Adding warehouse permissions:
            var warehousePermissions = from p in GetAllWarehousePermissions(operationId)
                                       where
                                       (sourceWarehouseId == 0 || ((p.Warehouse == null || p.Warehouse.Id == 0) && sourceWarehouseId == null) || ((p.Warehouse != null && p.Warehouse.Id > 0) && p.Warehouse.Id == sourceWarehouseId))
                                       &&
                                       (targetWarehouseId == 0 || ((p.Warehouse1 == null || p.Warehouse1.Id == 0) && targetWarehouseId == null) || ((p.Warehouse1 != null && p.Warehouse1.Id > 0) && p.Warehouse1.Id == targetWarehouseId))
                                       select p;

            foreach (var permission in warehousePermissions)
            {
                WarehousePermissionVirtual wpfg = new WarehousePermissionVirtual();

                if (permission.Warehouse != null)
                {
                    wpfg.WarehouseId = permission.Warehouse.Id;
                    wpfg.Warehouse = GetWarehouseById(Convert.ToInt32(wpfg.WarehouseId));
                }

                if (permission.Warehouse1 != null)
                {
                    wpfg.Warehouse1Id = permission.Warehouse1.Id;
                    wpfg.Warehouse1 = GetWarehouseById(Convert.ToInt32(wpfg.Warehouse1Id));
                }

                wpfg.WarehouseDocumentType = GetDocumentTypeList("").Where(p => p.Id == permission.WarehouseDocumentType.Id).FirstOrDefault();

                wpfg.MaterialIsPackage = permission.MaterialIsPackage;
                wpfg.PositionIsGarbage = permission.PositionIsGarbage;
                wpfg.PositionIsZZ = permission.PositionIsZZ;
                wpfg.PositionIsPartlyDamaged = permission.PositionIsPartlyDamaged;
                wpfg.PositionIsDamaged = permission.PositionIsDamaged;

                if (!warehousePermissionsForGrid.Any(
                    p =>
                        p.WarehouseDocumentType.Id == wpfg.WarehouseDocumentType.Id
                        &&
                        p.WarehouseId == wpfg.WarehouseId
                        &&
                        p.Warehouse1Id == wpfg.Warehouse1Id
                        ))
                {
                    //warehousePermissionsForGrid.Add(wpfg);
                    return true;
                }
            }

            //var x = warehousePermissionsForGrid.ToArray();

            return false;
        }

        public IEnumerable<WarehousePositionZone> ChoosePosShelfsByPositionIdDataOnly(int documentPositionId)
        {
            var zones = warehouseEntities.WarehousePositionZones
                .Where(x => x.WarehouseDocumentPosition.Id == documentPositionId);

            return zones;
        }

        //ToDo: intensive unit testing must be done!!!
        public List<Zone> ChooseShelfsByPositionIdDataOnly(int documentPositionId)
        {
            List<Zone> zonesData = new List<Zone>();
            var zones = warehouseEntities.WarehousePositionZones
                .Where(x => x.WarehouseDocumentPosition.Id == documentPositionId)
                .Select(x => x.Zone);

            foreach (Zone zone in zones)
            {
                zonesData.Add(new Zone() { Id = zone.Id, Name = zone.Name });
            }

            return zonesData;
        }

        /// <summary>
        /// Converts position list to markup string
        /// </summary>
        /// <param name="positionPath"></param>
        /// <returns></returns>
        public string ConvertPositionGraphPathToMarkupString(List<PositionNode> positionPath)
        {
#if DEBUG
            Contract.Requires(positionPath != null);
#endif
            StringBuilder markupString = new StringBuilder();

            for (var i = 0; i < positionPath.Count(); i++)
            {
                markupString.Append(positionPath[i].TextCode);
            }
            return markupString.ToString();
        }

        [Obsolete]
        public WarehouseDocument CorrectDocument
        (
          int? warehouseSourceId,
          int? warehouseTargetId,
          int authorId,
          List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses,
          WarehouseDocumentTypeEnum warehouseDocumentType,
          DateTime date
        )
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource = null;
            //Warehouse warehouseTarget = null;
            //Person person = null;

            //TranslateParameters(warehouseSourceId, warehouseTargetId, authorId, out warehouseSource, out warehouseTarget, out person);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                // Check if the document has any children
                int parentDocId = positionWithQuantityAndShelfses.First().documentParentIds.First();

                var docChildren = warehouseEntities.WarehouseDocumentRelations.Where(p => p.WarehouseDocument.Id == parentDocId);

                if (!docChildren.Any())
                {
                    warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, warehouseDocumentType, date);
                    warehouseEntities.SaveChanges();
                    transactionScope.Complete();
                    warehouseEntities.AcceptAllChanges();
                }
                else
                {
                    throw new Exception("Cannot correct document with offspring!");
                }
            }

            return warehouseDocument;
        }

        public PackageType CreateNewPackageType(int packageTypeNameId, int packageQuantity)
        {
            //WarehouseEntities warehouseEntities2 = new WarehouseEntities();

            PackageTypeName packageTypeName =
                warehouseEntities.PackageTypeNames.Where(p => p.Id == packageTypeNameId).First();

            // Add new type
            PackageType packageType = new PackageType();
            packageType.Name = packageTypeName.Name + " " + packageQuantity;
            packageType.PackageTypeName = packageTypeName;
            packageType.Quantity = packageQuantity;
            packageType.Unit = warehouseEntities.Units.Where(p => p.Id == 21).First();
            //warehouseDs.SaveNewPackageType(packageType);

            //try
            //{
            //    warehouseEntities2.AddToPackageType(packageType);
            //    warehouseEntities2.SaveChanges();
            //}
            //catch (Exception ex)
            //{
            //    var x = 1;
            //}

            warehouseEntities.AddToPackageTypes(packageType);
            warehouseEntities.SaveChanges(); //System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            //warehouseEntities.Dispose();

            return warehouseEntities.PackageTypes.Where(p => p.Id == packageType.Id).FirstOrDefault();
        }

        public void CreateNewPackageType(int unitId, int quantity, string name)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (!CheckIfPackageTypeExists(unitId, quantity, name))
                {
                    PackageType packageType = new PackageType();
                    packageType.Unit = warehouseEntities.Units.Where(p => p.Id == unitId).First();
                    packageType.Name = name;
                    packageType.Quantity = quantity;

                    warehouseEntities.AddToPackageTypes(packageType);

                    warehouseEntities.SaveChanges();
                }
                else
                {
                    throw new Exception("Duplicate parameters");
                }

                transactionScope.Complete();
            }
        }

        public Tuple<Order, OrderDelivery> CreateOrGetOrderAndDelivery(int processHeaderId, int? processIDH, int processIDPL, int processIDDPL, string orderNumber, string orderNumberPZ, bool forDepartment, int companyId)
        {
            Order order = null;
            Order orderDataOnly = null;
            OrderDelivery orderDelivery = null;
            OrderDelivery orderDeliveryDataOnly = null;

            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (!warehouseEntities.Orders.Any(o => o.OrderNumber == orderNumber))
                {
                    order = new Order()
                    {
                        OrderNumber = orderNumber,
                        CompanyId = companyId,
                        ExportDocsToSIMPLE = true
                    };

                    var entities = warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToList();
                    foreach (var entry in entities)
                    {
                        entry.ChangeState(EntityState.Unchanged);
                    }


                    warehouseEntities.AddToOrders(order);
                    warehouseEntities.SaveChanges();
                }
                else
                {
                    order = warehouseEntities.Orders.FirstOrDefault(
                        o =>
                        o.OrderNumber == orderNumber);
                    orderDelivery = warehouseEntities.OrderDeliveries.FirstOrDefault(od => od.Order.Id == order.Id && od.ProcessHeaderId == processHeaderId && od.ProcessIDH == processIDH && od.ProcessIDPL == processIDPL && od.ProcessIDDPL == processIDDPL && od.OrderNumberFromPZ == orderNumberPZ && od.ForDepartment == forDepartment);
                }
                if (order != null && orderDelivery == null)
                {

                    var entities = warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToList();

                    foreach (var entry in entities)
                    {
                        entry.ChangeState(EntityState.Unchanged);
                    }

                    orderDelivery = new OrderDelivery()
                    {
                        Order = order,
                        ProcessHeaderId = processHeaderId,
                        ProcessIDH = processIDH,
                        ProcessIDPL = processIDPL,
                        ProcessIDDPL = processIDDPL,
                        OrderNumberFromPZ = orderNumberPZ,
                        ForDepartment = forDepartment,
                        TimeCreated = DateTime.Now
                    };

                    warehouseEntities.AddToOrderDeliveries(orderDelivery);

                    warehouseEntities.SaveChanges();
                    transactionScope.Complete();
                    warehouseEntities.AcceptAllChanges();

                    foreach (var entry in entities)
                    {
                        entry.ChangeState(EntityState.Added);
                    }
                }
            }

            orderDataOnly = GetOrderByOrderDataOnly(order);
            orderDeliveryDataOnly = GetOrderDeliveryByOrderDataOnly(orderDelivery);

            return new Tuple<Order, OrderDelivery>(orderDataOnly, orderDeliveryDataOnly);
        }

        public void CancelInternalGiveDocument(int positionId)
        {
            var existingDoc = warehouseEntities.InternalGiveCancelled.FirstOrDefault(d => d.PositionId == positionId);

            if (existingDoc == null)
            {
                InternalGiveCancelled document = new InternalGiveCancelled()
                {
                    PositionId = positionId
                };

                warehouseEntities.InternalGiveCancelled.AddObject(document);
                warehouseEntities.SaveChanges();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public OrderDelivery CreateOrGetOrderDelivery(int? processHeaderId, int? processIDH, int? processIDPL, int? processIDDPL, string orderNumber, string orderNumberPZ, bool forDepartment)
        {

            //using (var transactionScope = new TransactionScope())
            //{
            OrderDelivery orderDelivery = null;
            var order = warehouseEntities.Orders
              .FirstOrDefault(o => o.OrderNumber == orderNumber);

            if (order == null)// !warehouseEntities.Orders.Any(o => o.OrderNumber == orderNumber))
            {
                order = new Order()
                {
                    OrderNumber = orderNumber,
                    CompanyId = DetectCompanyId(orderNumber),
                    ExportDocsToSIMPLE = true
                };
                //warehouseEntities.Orders.AddObject(order);
                //warehouseEntities.SaveChanges();
            }
            else
            {
                orderDelivery = order.OrderDeliveries
                  .FirstOrDefault
                  (od =>
                    od.ProcessHeaderId == processHeaderId &&
                    od.ProcessIDH == processIDH &&
                    od.ProcessIDPL == processIDPL &&
                    od.ProcessIDDPL == processIDDPL &&
                    od.OrderNumberFromPZ == orderNumberPZ &&
                    od.ForDepartment == forDepartment
                  );
            }

            if (orderDelivery == null)
            {
                orderDelivery = new OrderDelivery()
                {
                    Order = order,
                    ProcessHeaderId = processHeaderId,
                    ProcessIDH = processIDH,
                    ProcessIDPL = processIDPL,
                    ProcessIDDPL = processIDDPL,
                    OrderNumberFromPZ = orderNumberPZ,
                    ForDepartment = forDepartment,
                    TimeCreated = DateTime.Now
                };
                //warehouseEntities.OrderDeliveries.AddObject(orderDelivery);//.AddToOrderDeliveries(orderDelivery);

                //warehouseEntities.SaveChanges();
                //transactionScope.Complete();
                //warehouseEntities.AcceptAllChanges();
            }

            return orderDelivery;
            //}

            //orderDataOnly = GetOrderByOrderDataOnly(order);
            //orderDeliveryDataOnly = GetOrderDeliveryByOrderDataOnly(orderDelivery);

            //return new Tuple<Order, OrderDelivery>(orderDataOnly, orderDeliveryDataOnly);
        }

        private int DetectCompanyId(string orderNumber)
        {
#if DEBUG
            Contract.Requires<ArgumentException>(!String.IsNullOrEmpty(orderNumber), "Empty order number, company detection impossible");
            //based on code from: Client.Domain.ProcessToProductionConverter.ProcessToProductionConverter.GetCompanyIdFromProcessNumber(string processNumber):819
#endif
            orderNumber = orderNumber.Trim();

            int? companyId = Regex.IsMatch(orderNumber, "\\d{2}/(P/)?\\d{4}") ? 1
                : Regex.IsMatch(orderNumber, "\\d{2}/M/(P/)?\\d{4}") ? 2
                : Regex.IsMatch(orderNumber, "\\d{2}/MET/(P/)?\\d{4}") ? 3
                : (int?)null;

            if (companyId.HasValue)
            {
                return companyId.Value;
            }
            else
            {
                throw new Exception(String.Format("Incorrect order number: {0}. The company cannot be identified.", orderNumber));
            }
        }

        public Tuple<Order, OrderDeliveryNew> CreateOrGetOrderAndDeliveryNew(int supplyOrderId, int supplyOrderPositionId, string orderNumber, string orderNumberPZ, bool forDepartment)
        {
            Order order = null;
            Order orderDataOnly = null;
            OrderDeliveryNew orderDelivery = null;
            OrderDeliveryNew orderDeliveryDataOnly = null;

            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (
                    !warehouseEntities.Orders.Any(
                    o => o.OrderNumber == orderNumber)
                    )
                {
                    order = new Order()
                    {
                        OrderNumber = orderNumber,
                        ExportDocsToSIMPLE = true
                    };
                    warehouseEntities.AddToOrders(order);
                    warehouseEntities.SaveChanges();
                }
                else
                {
                    order = warehouseEntities.Orders.FirstOrDefault(
                        o =>
                        o.OrderNumber == orderNumber);

                    orderDelivery = warehouseEntities.OrderDeliveryNews.FirstOrDefault(od => od.Order.Id == order.Id && od.SupplyOrderPositionId == supplyOrderPositionId);
                }
                if (order != null && orderDelivery == null)
                {
                    orderDelivery = new OrderDeliveryNew()
                    {
                        Order = order,
                        SupplyOrderPositionId = supplyOrderPositionId,
                        SupplyOrderNumber = orderNumberPZ,
                        ForDepartment = forDepartment,
                        TimeCreated = DateTime.Now
                    };
                    warehouseEntities.AddToOrderDeliveryNews(orderDelivery);

                    warehouseEntities.SaveChanges();
                    transactionScope.Complete();
                    warehouseEntities.AcceptAllChanges();
                }
            }

            orderDataOnly = GetOrderByOrderDataOnly(order);
            orderDeliveryDataOnly = GetOrderDeliveryByOrderDataOnlyNew(orderDelivery);

            return new Tuple<Order, OrderDeliveryNew>(orderDataOnly, orderDeliveryDataOnly);
        }

        public void CreateSystemGroup(string groupName)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                SystemGroup systemGroup = new SystemGroup();
                systemGroup.Name = groupName;

                warehouseEntities.AddToSystemGroups(systemGroup);

                //systemGroup.EntityState = EntityState.Added;
                //EntityState es = systemGroup.EntityState;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public int CreateWarehouse(string name, string shortName, CompanyEnum company, int userId, int? department)
        {
            int newWarehouseId = operations.CreateWarehouse(name, shortName, (int)company, (int)WarehouseTypeEnum.WarehouseStandard, userId, department);

            //AddWarehousePermissionToAllWarehouses(newWarehouseId, WarehouseDocumentTypeEnum.MMMinus, true, true);

            return newWarehouseId;
        }

        public WarehouseDocumentPosition CreateWarehouseDocumentPosition(Material material, double quantity, decimal unitPrice)
        {
            WarehouseDocumentPosition warehouseDocumentPosition = new WarehouseDocumentPosition() { Material = material, Quantity = quantity, UnitPrice = unitPrice };
            return warehouseDocumentPosition;
        }

        public WarehouseDocumentPosition CreateWarehouseDocumentPositionDataOnly(int materialId, decimal unitPrice, decimal quantity, int parentWarehouseDocumentPositionId, int orderId, int? processIdpl = null, int? processIddpl = null)
        {
#if DEBUG
            Contract.Requires(materialId > 0);
            Contract.Requires(unitPrice >= 0);
            //Contract.Requires(quantity >= 0); // it turns out Quantity can be negative (?)
#endif
            WarehouseDocumentPosition warehouseDocumentPosition = CreateWarehouseDocumentPosition(materialId, unitPrice, quantity); //CreateWarehouseDocumentPositionDataOnly(materialId, unitPrice, quantity);

            if (processIdpl.HasValue)
                warehouseDocumentPosition.ProcessIdpl = processIdpl;
            if (processIddpl.HasValue)
                warehouseDocumentPosition.ProcessIddpl = processIddpl;

            if (orderId != 0)
            {

                warehouseDocumentPosition.Order = warehouseEntities.Orders.First(x => x.Id == orderId);

                //Order order = warehouseEntities.Orders.First(o => o.Id == orderId);

                //Order orderDataOnly = new Order();
                //orderDataOnly.Id = order.Id;
                //orderDataOnly.OrderNumber = order.OrderNumber;
                //orderDataOnly.OrderNumberPZ = order.OrderNumberPZ;
                //orderDataOnly.ProcessHeaderId = order.ProcessHeaderId;
                //orderDataOnly.ProcessIDH = order.ProcessIDH;
                //orderDataOnly.ProcessIDPL = order.ProcessIDPL;
                //orderDataOnly.ProcessIDDPL = order.ProcessIDDPL;
                //orderDataOnly.ForDepartment = order.ForDepartment;
                //orderDataOnly.ExportDocsToSIMPLE = true;

                //warehouseDocumentPosition.Order = orderDataOnly;
            }
            return warehouseDocumentPosition;
        }

        public WarehouseDocumentPosition CreateWarehouseDocumentPosition(int materialId, decimal unitPrice, decimal quantity)
        {
#if DEBUG
            Contract.Requires(materialId > 0);
            Contract.Requires(unitPrice >= 0); // 0 if price unknown
            //Contract.Requires(quantity >= 0); //INW can be 0 // it turns out Quantity can be negative (?)
#endif
            var warehouseDocumentPosition = new WarehouseDocumentPosition();

            var materialFromDb = warehouseEntities.Materials
              .Include(m => m.Unit)
              .First(p => p.Id == materialId);

            warehouseDocumentPosition.Material = materialFromDb;
            warehouseDocumentPosition.Quantity = (double)quantity;
            warehouseDocumentPosition.UnitPrice = unitPrice;

            return warehouseDocumentPosition;
        }

        [Obsolete("Use CreateWarehouseDocumentPosition(..) instead.")]
        public WarehouseDocumentPosition CreateWarehouseDocumentPositionDataOnly(int materialId, decimal unitPrice, decimal quantity)
        {
            var warehouseDocumentPosition = new WarehouseDocumentPosition();

            var materialFromDb = warehouseEntities.Materials
              .Include("Unit")
              .First(p => p.Id == materialId);

            var material = new Material();
            material.Id = materialFromDb.Id;
            material.Name = materialFromDb.Name;
            material.NameShort = materialFromDb.NameShort;

            material.Unit = new Unit();
            material.Unit.Id = materialFromDb.Unit.Id;
            material.Unit.Name = materialFromDb.Unit.Name;
            material.Unit.ShortName = materialFromDb.Unit.ShortName;

            //MaterialSIMPLE materialSimple = new MaterialSIMPLE();

            string indexSIMPLE = materialFromDb.IndexSIMPLE;
            if (indexSIMPLE != null)
            {
                material.IndexSIMPLE = indexSIMPLE;
            }

            string nameSIMPLE = materialFromDb.Name;
            if (nameSIMPLE != null)
            {
                material.Name = nameSIMPLE;
            }

            if (materialFromDb.IdSIMPLE.HasValue)
            {
                material.IdSIMPLE = materialFromDb.IdSIMPLE.Value;
            }

            //materialSimple.Material = material;

            warehouseDocumentPosition.Material = material;
            warehouseDocumentPosition.UnitPrice = unitPrice;
            warehouseDocumentPosition.Quantity = (double)quantity;

            return warehouseDocumentPosition;
        }

        //[Obsolete]
        //public WarehouseDocumentPosition CreateWarehouseDocumentPositionDataOnly2(int materialId, decimal unitPrice, decimal quantity, int? orderId)
        //{
        //  Contract.Requires(materialId > 0);
        //  Contract.Requires(unitPrice >= 0);
        //  Contract.Requires(quantity >= 0);

        //  var warehouseDocumentPosition = CreateWarehouseDocumentPosition(materialId, unitPrice, quantity); //CreateWarehouseDocumentPositionDataOnly(materialId, unitPrice, quantity);
        //  //warehouseEntities.Detach(warehouseDocumentPosition);

        //  if (orderId.HasValue && orderId.Value != 0)
        //  {
        //    Order order = warehouseEntities.Orders.First(o => o.Id == orderId);
        //    warehouseDocumentPosition.Order = order;
        //  }

        //  //parentWarehouseDocumentPositionId
        //  //            warehouseDocumentPosition.WarehouseDocumentPosition2 = new WarehouseDocumentPosition() { Id = parentWarehouseDocumentPositionId };

        //  return warehouseDocumentPosition;
        //}

        public int CreateWarehouseGroup(string name, string shortName, int userId)
        {
#if DEBUG
            Contract.Requires(shortName != null);
#endif
            int newWarehouseGroupId = operations.CreateWarehouseGroup(name, shortName, userId);

            //AddWarehousePermissionToAllWarehouses(newWarehouseId, WarehouseDocumentTypeEnum.MMMinus, true, true);

            return newWarehouseGroupId;
        }

        public IQueryable<CurrentStacksForWarehouseAdjusted_WithZTD> CurrentStacksForWarehouseAdjusted_WithZTD_Encapsulated()
        {
            return warehouseEntities.CurrentStacksForWarehouseAdjusted_WithZTD.AsQueryable();
        }

        public void DeleteObject(EntityObject entity)
        {
            warehouseEntities.DeleteObject(entity);
        }

        public void DetachEntity(EntityObject entity)
        {
#if DEBUG
            Contract.Requires(entity != null);
#endif
            warehouseEntities.Detach(entity);
            if (entity.EntityState != System.Data.EntityState.Detached)
                warehouseEntities.Detach(entity);
        }

        public void Dispose()
        {
            warehouseEntities.Dispose();
        }

        //public void DocumentCorrection(int warehouseDocumentId)
        //{ }

        //public WarehouseDocument ExternalGive(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int consumerId, bool exportDocToSIMPLE)
        //{
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());

        //  return ExternalGive(warehouseSourceId, authorId, positionWithQuantityAndShelfses, consumerId, DateTime.Now, exportDocToSIMPLE);
        //}

        public WarehouseDocument ExternalGive(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int consumerId, DateTime date, bool exportDocToSIMPLE, int? docSubtypeSIMPLE = 1)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
#endif
            WarehouseDocument warehouseDocument;

            //Person person = warehouseEntities.People.First(p => p.Id == authorId);

            foreach (var positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                if (positionWithQuantityAndShelf.position.Quantity >= 0) throw new DataException("Quantity must be less than 0");
            }

            warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, null, authorId, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.Wz, date, exportDocToSIMPLE, null, null, docSubtypeSIMPLE);

            int? contractingPartyId = null;

            var warehouseSource = warehouseEntities.Warehouses.First(p => p.Id == warehouseSourceId);
            contractingPartyId = UpdateContractingPartyInLocal(consumerId, warehouseSource.CompanyId);

            var contractingPartyForWarehouseDocument = new ContractingPartyForWarehouseDocument();
            if (warehouseEntities.ContractingParties.FirstOrDefault(c => c.Id == contractingPartyId && c.Company != null && c.Company.Id == warehouseSource.CompanyId) != null)
            {
                contractingPartyForWarehouseDocument.ContractingParty = warehouseEntities.ContractingParties.First(c => c.Id == contractingPartyId && c.Company.Id == warehouseSource.CompanyId);
                contractingPartyForWarehouseDocument.WarehouseDocument = warehouseDocument;

                warehouseEntities.AddToContractingPartyForWarehouseDocuments(contractingPartyForWarehouseDocument);
            }

            warehouseEntities.SaveChanges();

            return warehouseDocument;
        }

        public WarehouseDocument ExternalGiveOrderedMaterial(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int consumerId, DateTime date, bool exportDocToSIMPLE, int? docSubtypeSIMPLE = 1)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
#endif
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var document = ExternalGive(warehouseSourceId, authorId, positionWithQuantityAndShelfses, consumerId, date,
                                    exportDocToSIMPLE, docSubtypeSIMPLE);

                foreach (var position in positionWithQuantityAndShelfses)
                {
                    if (position.position.IdZF.HasValue)
                    {
                        var zmfi =
                            warehouseEntities.KL_Wyceny_Karta_zak_zmfi.Where(p => p.IDzf == position.position.IdZF && !p.Zat).
                                FirstOrDefault();
                        if (zmfi == null)
                            throw new Exception("Brak oczekujacego zamowienia o numerze " + position.position.IdZF);

                        zmfi.Zat = true;
                        warehouseEntities.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("Bledny numer zamowienia");
                    }
                }

                transactionScope.Complete();

                return document;
            }
        }

        //public WarehouseDocument ExternalReceive(int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int? contractingPartyInSIMPLE, bool exportDocToSIMPLE)
        //{
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());

        //  return ExternalReceive(warehouseTargetId, authorId, positionWithQuantityAndShelfses, contractingPartyInSIMPLE, DateTime.Now, exportDocToSIMPLE);
        //}

        public WarehouseDocument ExternalReceive(int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int? contractingPartyInSIMPLE, DateTime date, bool exportDocToSIMPLE)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
            Contract.Requires<DataException>(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null && pwqas.position.Quantity > 0), "Quantity of all positions must be positive i.e. greater than 0");
            Contract.Ensures(Contract.Result<WarehouseDocument>() != null);
#endif

            //foreach (var positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            //{
            //  if (positionWithQuantityAndShelf.position.Quantity <= 0) throw new DataException("Quantity must be greater than 0");
            //}

            //Person person = warehouseEntities.People.First(p => p.Id == authorId);
            Warehouse warehouseTarget = warehouseEntities.Warehouses
              .First(p => p.Id == warehouseTargetId);

            // Generating Pz document:
            var warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation
            (
              null,
              warehouseTarget.Id,
              authorId,
              positionWithQuantityAndShelfses,
              WarehouseDocumentTypeEnum.Pz,
              date,
              exportDocToSIMPLE
            );

            UpdateContractingPartyForWarehouseDocument(contractingPartyInSIMPLE, warehouseTarget.CompanyId, warehouseDocument);

            warehouseEntities.SaveChanges();

            return warehouseDocument;
        }

        public WarehouseDocument ExternalReceiveNew(int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int contractingPartyInSIMPLE, DateTime date, bool exportDocToSIMPLE)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
            Contract.Requires<DataException>(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null && pwqas.position.Quantity > 0), "Quantity of all positions must be positive i.e. greater than 0");
#endif
            WarehouseDocument warehouseDocument;
            Warehouse warehouseTarget = warehouseEntities.Warehouses.First(p => p.Id == warehouseTargetId);
            //Person person = warehouseEntities.People.First(p => p.Id == authorId);
            int? contractingPartyId = null;

            //foreach (var positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            //{
            //  if (positionWithQuantityAndShelf.position.Quantity <= 0) throw new DataException("Quantity must be greater than 0");
            //}

            // Generating Pz document:
            warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(null, warehouseTarget.Id, authorId, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.Pz, date, exportDocToSIMPLE);

            ContractingPartyForWarehouseDocument contractingPartyForWarehouseDocument = new ContractingPartyForWarehouseDocument();
            if (warehouseEntities.ContractingParties.FirstOrDefault(c => c.Id == contractingPartyId && c.Company != null && c.Company.Id == warehouseTarget.CompanyId) != null)
            {
                contractingPartyForWarehouseDocument.ContractingParty = warehouseEntities.ContractingParties.First(c => c.Id == contractingPartyId && c.Company.Id == warehouseTarget.CompanyId);
                contractingPartyForWarehouseDocument.WarehouseDocument = warehouseDocument;

                warehouseEntities.AddToContractingPartyForWarehouseDocuments(contractingPartyForWarehouseDocument);
            }

            warehouseEntities.SaveChanges();

            return warehouseDocument;
        }

        public WarehouseDocument ExternalReturn(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int contractingPartyId, DateTime date, bool exportDocToSIMPLE, int KCStateId, int? docSubtypeSIMPLE = 1)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
            Contract.Requires<DataException>(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null && pwqas.position.Quantity < 0), "Quantity of all positions must be negative i.e. less than 0");
#endif

            using (TransactionScope transactionScope = new TransactionScope())
            {
                //WarehouseDocument warehouseDocument;

                //Person person = warehouseEntities.People.First(p => p.Id == authorId);

                //foreach (var positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
                //{
                //  if (positionWithQuantityAndShelf.position.Quantity >= 0)
                //    throw new DataException("Quantity must be less than 0");
                //}

                var warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation
                 (
                   warehouseSourceId,
                   null,
                   authorId,
                   positionWithQuantityAndShelfses,
                   WarehouseDocumentTypeEnum.ZTD,
                   date,
                   exportDocToSIMPLE,
                   null,
                   null,
                   docSubtypeSIMPLE);

                // contractingPartyId = null;

                var warehouseSource = warehouseEntities.Warehouses.First(p => p.Id == warehouseSourceId);
                int companyId = warehouseSource.CompanyId;

                //int? contractingPartyId = contractingPartyId; // UpdateContractingPartyInLocal(consumerId, companyId);

                var contractingPartyForWarehouseDocument = new ContractingPartyForWarehouseDocument();
                var contractingParty = warehouseEntities.ContractingParties
                  .FirstOrDefault
                  (c =>
                    c.Id == contractingPartyId
                    && c.CompanyId == companyId
                  );

                if (contractingParty != null)
                {
                    contractingPartyForWarehouseDocument.ContractingParty = contractingParty;
                    contractingPartyForWarehouseDocument.WarehouseDocument = warehouseDocument;

                    warehouseEntities.AddToContractingPartyForWarehouseDocuments(contractingPartyForWarehouseDocument);
                }

                warehouseEntities.SaveChanges();

                KCStateReturnSaveSolvingPositionId(KCStateId, warehouseDocument.WarehouseDocumentPositions.First().Id);

                // force import doc from warehouse to simple, not allowed from [testrdb2]
                //warehouseEntities.Warehouse2SimpleDocExport(); //warehouseSource.Company.Id);

                transactionScope.Complete();

                return warehouseDocument;
            }
        }

        /// <summary>
        /// Retrives a single unique record based on a unique key built from a set of IDs
        /// Uniqueness of the key is derived from data since the design was not documented properly and lost
        /// </summary>
        /// <param name="idpl"></param>
        /// <param name="idSim"></param>
        /// <param name="iddpl"></param>
        /// <param name="flag">{1,2,3} loosely maps to type of order in K2</param>
        /// <returns>single object or null</returns>
        public WaitingForSupply FindRecordInWaitingForSupply(int idpl, int idSim, int iddpl, int flag, int contractingPartyId)
        {//[IDpl]	  ,[IDsim]	  ,[IDdpl]      ,[Flaga]
            var waintingForSupply = warehouseEntities.WaitingForSupplies
              .FirstOrDefault
              //.SingleOrDefault
              (wfs =>
                wfs.IDpl == idpl &&
                wfs.IDsim == idSim &&
                wfs.IDdpl == iddpl &&
                wfs.Flaga == flag
              );

            #region Region of weird shit

            // this crappy code is a forced workaround for Supply Department not using proper supplierId provided to them by SIMPLE
            if (waintingForSupply != null)
            {
                waintingForSupply.CorrectKontrahentId = contractingPartyId;
            }

            #endregion Region of weird shit

            return waintingForSupply;
        }

        public WaitingForSupply FindRecordInWaitingForSupply(int processHeaderId, int processIDPL, int processIDDPL, int contractingPartyId, int idSim, int companyId, string orderNumberPZ, bool forDepartment) //, double quantity)
        {
            int dostawcaId = warehouseEntities.GetSimpleDostawcaIdForKontrahentIdAndCompanyId(contractingPartyId, companyId).FirstOrDefault() ?? 0;

            var supplyRecordsWithoutQuantity = warehouseEntities.WaitingForSupplies.Where(
                    w =>
                    w.ID == processHeaderId
                    && w.IDpl == processIDPL
                    && w.IDdpl == processIDDPL
                    && w.kontrachent_id == dostawcaId
                    && w.IDsim == idSim && w.Firma == companyId
                    && w.NazwSK == orderNumberPZ
                    && (
                        (forDepartment == false && w.Flaga == 1)
                        ||
                        (forDepartment == true && (w.Flaga == 2 || w.Flaga == 3)))
                    );

            //var recordsTest = supplyRecordsWithoutQuantity.ToList();

            var supplyRecord =
                supplyRecordsWithoutQuantity.FirstOrDefault();
            //w =>
            //((double)w.IloscZam - (w.UsedQuantity != null? w.UsedQuantity : 0)) >= quantity
            //);

            if (supplyRecord != null)
            {
                supplyRecord.CorrectKontrahentId = contractingPartyId;
                //FakeTable fakeTable2 = warehouseEntities.GetSimpleKontrahentIdForDostawcaIdAndCompanyId(supplyRecord.kontrachent_id, companyId).FirstOrDefault();

                //if (fakeTable2 != null)
                //{
                //    int correct_kontrahent_id = fakeTable2.Id;
                //    supplyRecord.CorrectKontrahentId = correct_kontrahent_id;
                //}
                //else
                //{
                //    supplyRecord.CorrectKontrahentId = 0;
                //}
            }

            return supplyRecord;
        }

        public WaitingForSupplyNew FindRecordInWaitingForSupplyNew(int processHeaderId, int supplyOrderId, int supplyOrderPositionId, int contractingPartyId, int idSim, int companyId, string orderNumberPZ, bool forDepartment) //, double quantity)
        {
            //int dostawcaId;
            //FakeTable fakeTable1 = warehouseEntities.GetSimpleDostawcaIdForKontrahentIdAndCompanyId(contractingPartyId, companyId).FirstOrDefault();

            //if (fakeTable1 != null)
            //{
            //    int correct_kontrahent_id = fakeTable1.Id;
            //    dostawcaId = correct_kontrahent_id;
            //}
            //else
            //{
            //    dostawcaId = 0;
            //}

            var supplyRecordsWithoutQuantity = warehouseEntities.WaitingForSupplyNews
              .Where(
                    w =>
                    w.SupplyOrderPositionId == supplyOrderPositionId
                    && w.ContractingPartyId == contractingPartyId
                    && w.MaterialIdSIMPLE == idSim && w.CompanyId == companyId
                    && w.SupplyOrderNumber == orderNumberPZ
                    && w.DepartmentId.HasValue == forDepartment
                    );

            //var recordsTest = supplyRecordsWithoutQuantity.ToList();

            var supplyRecord =
                supplyRecordsWithoutQuantity.FirstOrDefault();
            //w =>
            //((double)w.IloscZam - (w.UsedQuantity != null? w.UsedQuantity : 0)) >= quantity
            //);

            if (supplyRecord != null)
            {
                //////supplyRecord.CorrectKontrahentId = contractingPartyId;
                //FakeTable fakeTable2 = warehouseEntities.GetSimpleKontrahentIdForDostawcaIdAndCompanyId(supplyRecord.kontrachent_id, companyId).FirstOrDefault();

                //if (fakeTable2 != null)
                //{
                //    int correct_kontrahent_id = fakeTable2.Id;
                //    supplyRecord.CorrectKontrahentId = correct_kontrahent_id;
                //}
                //else
                //{
                //    supplyRecord.CorrectKontrahentId = 0;
                //}
            }

            return supplyRecord;
        }

        [Obsolete]
        public void FreeMaterial(int materialReservationId, int? quantity)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                //MaterialReservation materialReservation = warehouseEntities.MaterialReservation.First(p => p.Id == materialReservationId);

                //if (quantity == null)
                //{
                //    materialReservation.Quantity = 0;
                //}
                //else
                //{
                //    if (materialReservation.Quantity < quantity) throw new ApplicationException("You cannot free more material than is reserved (is");
                //    materialReservation.Quantity -= (int)quantity;
                //}

                //warehouseEntities.SaveChanges();
                //transactionScope.Complete();
                //warehouseEntities.AcceptAllChanges();
            }
        }

        public string FreeMaterialInWarehouse(
            int warehouseId, int materialId, int orderId, int? processIdpl, int zoneId, decimal quantity, int operatorId, bool isOrder = false, bool isGave = false, bool isOrderAndStack=false)
        {
            Zone zone = GetShelfByIdDataOnly(zoneId);
            Order order = GetOrderByIdDataOnly(orderId);
            Material material = GetMaterialById(materialId);

            if (zone == null)
                return "Brak lokalizacji źródłowej";
            if (material == null)
                return "Brak materiału";

            try
            {
                if (isOrder && !isOrderAndStack )
                {
                    if (isGave) // częściowo wydany
                    {
                        warehouseEntities.spdWarehouseReorderMaterialForOrderedAndGiveDocs(warehouseId, materialId, orderId, null, quantity, false,
                            false, false, false, null, operatorId, processIdpl, null, zoneId, null, false, false);
                    }
                    else // pełne wydanie
                    {
                        warehouseEntities.ReorderMaterialInWarehouseForOrderedDocs(warehouseId, materialId, orderId, null, quantity, false,
                            false, false, false, null, operatorId, processIdpl, null, zoneId, null, false, false);
                    }
                }
                else
                {
                    warehouseEntities.spdWarehouseReorderMaterial(warehouseId, materialId, orderId, null, quantity, false,
                        false, false, false, null, operatorId, processIdpl, null, zoneId, null, false, false,false);
                }

                    return "Uwolniono " + quantity + "" + material.Unit.ShortName + " materiału " +
                        material.IndexSIMPLE + " z przypisania dok " + order.OrderNumber + (processIdpl.HasValue ? (" (IDPL " + processIdpl.Value.ToString() + ")") : "");
                
            }
            catch (Exception ex)
            {
                return "Wystąpił błąd w trakcie uwalniania materiału: " + (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }

        public WarehouseDocument GenerateWarehouseInventoryDocument(int warehouseTargetId, int authorId, List<WarehouseInventoryPosition> positionsWithShelves)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Zone> GetActiveShelfsList(string filterName)
        {
            var allShelfs = warehouseEntities.Zones
              .Include(z => z.Zone1)
              .Include(z => z.Zone2)//.AddIncludePaths("Zone1", "Zone2")
              .Where(shelf =>
                (string.IsNullOrEmpty(filterName)
                ||
                    //shelf.Name.ToLower().StartsWith(filterName.ToLower()))
                shelf.Name.ToLower().Contains(filterName.ToLower())

                && shelf.IsLeaf == true
                && shelf.IsActive == true)
              );
            return allShelfs;
        }

        public List<WarehouseInventoryPosition> GetActiveWarehouseInventoryPositions()
        {
            return warehouseEntities.WarehouseInventoryPositions
                .Include(x => x.Zone)
                .Include(x => x.Order)
                .Include(x => x.Department)
                .Include(x => x.Warehouse)
                .Include(x => x.Material.Unit)
                .Where(x => x.IsActive)
                .ToList();
        }

        public List<WarehouseInventoryPosition> GetActiveWarehouseInventoryPositions(int? warehouseId)
        {
            return warehouseEntities.WarehouseInventoryPositions
                .Include(x => x.Zone)
                .Include(x => x.Order)
                .Include(x => x.Department)
                .Include(x => x.Warehouse)
                .Include(x => x.Material.Unit)
                .Where(x => x.IsActive && x.WarehouseId == warehouseId)
                .ToList();
        }

        public IQueryable<ZoneParentPath> GetActiveZonesWithParentsList()
        {
            return warehouseEntities.ZoneParentPaths
                .Where(shelf => shelf.IsLeaf && shelf.IsActive);
        }

        public IQueryable<ProductionExtended> GetAllActiveProductions(int companyId)
        {
            return warehouseEntities.ProductionExtendeds
                .Where(p => (companyId == 1 && !p.ProductionNumber.Contains("/M/") || (companyId == 2 && p.ProductionNumber.Contains("/M/"))))
                .Where(p => p.IsCompleted == false);
        }

        public IQueryable<Company> GetAllCompanies()
        {
            return warehouseEntities.Companies
                .Where(x => x.IsVisible)
                .OrderBy(x => x.Name);
        }

        public C_ImportSimple_ContractingPartyFromSimple2[] GetAllContractingParties(int companyId)
        {
            return warehouseEntities.C_ImportSimple_ContractingPartyFromSimple2
                .Where(w => w.CompanyId == companyId).OrderBy(p => p.CompanyName).ToArray();
        }

        public IQueryable<Department> GetAllDepartments()
        {
            return warehouseEntities.Departments.OrderBy(x => x.Name);
        }

        public IQueryable<DepartmentSIMPLEFiltered> GetAllDepartmentsSIMPLE(int? companyId)
        {
            return warehouseEntities.DepartmentSIMPLEFiltereds
                .Where(x => !companyId.HasValue || x.CompanyId == companyId)
                .OrderBy(x => x.nazwa);
        }

        public WarehouseDocumentType[] GetAllDocumentTypes()
        {
            return warehouseEntities.WarehouseDocumentTypes.ToArray();
        }

        public IQueryable<WarehouseGroupMembership> GetAllGroupMembershipsForWarehouse(int warehouseId)
        {
            return warehouseEntities.WarehouseGroupMemberships
                .Include(x => x.Warehouse)
                .Include(x => x.WarehouseGroup)
                .Where(x => x.Warehouse.Id == warehouseId);
        }

        public IQueryable<WarehouseGroupPermission> GetAllGroupPermissions(int? operationId = null)
        {
            IQueryable<WarehouseGroupPermission> groupPermissions;

            if (operationId.HasValue)
            {
                groupPermissions = warehouseEntities.WarehouseGroupPermissions
                  .Include(wgp => wgp.WarehouseGroup)
                  .Include(wgp => wgp.WarehouseDocumentType)
                    //.AddIncludePaths("WarehouseGroup", "WarehouseDocumentType")
                  .Where(p => !p.RevokeTime.HasValue && (p.OperationId == operationId.Value));
            }
            else
            {
                groupPermissions = warehouseEntities.WarehouseGroupPermissions
                  .Include(wgp => wgp.WarehouseGroup)
                  .Include(wgp => wgp.WarehouseDocumentType)
                    //.AddIncludePaths("WarehouseGroup", "WarehouseDocumentType")
                  .Where(p => !p.RevokeTime.HasValue);
            }

            return groupPermissions;
        }

        public IEnumerable<GetAllInterWarehouseOperationAccessCombinationsForUser_Result> GetAllInterOperationAccessRightsVirtualForUser(int systemUserId)
        {
            return warehouseEntities.GetAllInterWarehouseOperationAccessCombinationsForUser(systemUserId);
        }

        public IQueryable<Order> GetAllOrders()
        {
            return warehouseEntities.Orders;
        }

        public IQueryable<PackageType> GetAllPackageTypes()
        {
            return warehouseEntities.PackageTypes.Include(x => x.PackageTypeName);
        }

        public IQueryable<Zone> GetAllShelfs(string shelfToFind)
        {
            return warehouseEntities.Zones
                .Where(x => x.Name.ToLower().Contains(shelfToFind.ToLower()) || String.IsNullOrEmpty(shelfToFind))
                .OrderBy(x => x.Name);
        }

        public IQueryable<SystemGroup> GetAllSystemGroups()
        {
            return warehouseEntities.SystemGroups
                .Include(x => x.WarehouseSecurityGroupPrivilegeForReads)
                .Include(x => x.WarehouseSecurityGroupPrivilegeForExecutions);
        }

        public IQueryable<SystemUserGroupMembership> GetAllSystemUserGroupMemberships(int systemUserId)
        {
            //if (_clientData != null && _clientData.UserGroupMemberships != null)
            //{
            //    return _clientData.UserGroupMemberships.AsQueryable();
            //}
            //else
            {
                var memberships = warehouseEntities.SystemUserGroupMemberships
                  .Include(sugm => sugm.SystemUser)
                  .Include(sugm => sugm.SystemGroup);
                //.AddIncludePaths("SystemUser", "SystemGroup");

                //if (_clientData != null)
                //{
                //    _clientData.UserGroupMemberships = memberships.ToList();
                //}

                return
                    memberships;
            }
        }

        public IQueryable<SystemUser> GetAllSystemUsers()
        {
            return warehouseEntities.SystemUsers
                .Include(x => x.Person)
                .Include(x => x.SystemUserSettings)
                .Include(x => x.WarehouseSecurityPrivilegeForReads)
                .Include(x => x.WarehouseSecurityPrivilegeForExecutions);
        }

        public Unit[] GetAllUnits()
        {
            IQueryable<Unit> units;
            lock (typeof(WarehouseDS))
            {
                ////object objFromCache = Toolbox.CacherEnt.GetObjectFromCache("AllUnitsList");
                ////if (objFromCache != null)
                ////{
                ////    units = (IQueryable<Unit>)objFromCache;
                ////}
                ////else
                {
                    units = warehouseEntities.Units.OrderBy(p => p.Name);

                    ////Toolbox.CacherEnt.StoreObjectInCache("AllUnitsList", units);
                    // test:
                    //object objFromCacheTest = Toolbox.CacherEnt.GetObjectFromCache("AllZonesWithParents");
                }
            }
            return units.ToArray();
        }

        public IQueryable<WarehouseDocumentPosition> GetAllWarehouseDocumentPositions()
        {
            return warehouseEntities.WarehouseDocumentPositions;
        }

        public IEnumerable<WarehouseDocument> GetAllWarehouseDocuments(
          int? warehouseId,
          string cmbWarehouseSourceText,
          string cmbWarehouseTargetText,
          string cmbDocumentTypeText,
          string cmbAuthorText,
          DateTime? date)
        {
            var warehouseDocuments = warehouseEntities.WarehouseDocuments
              .Where
              (p =>
                !p.WarehouseDocumentRelations.Any() &&
                (
                  p.DocumentTypeId == (int)WarehouseDocumentTypeEnum.Pw ||
                  p.DocumentTypeId == (int)WarehouseDocumentTypeEnum.Rw ||
                  p.DocumentTypeId == (int)WarehouseDocumentTypeEnum.MMPlus ||
                  p.DocumentTypeId == (int)WarehouseDocumentTypeEnum.MMMinus
                ) &&
                (p.WarehouseSourceId == warehouseId || p.WarehouseTargetId == warehouseId) &&
                (date == null || p.Date < date.Value)
              );

            if (string.IsNullOrEmpty(cmbWarehouseSourceText))
                warehouseDocuments = warehouseDocuments
                  .Where(p => !string.IsNullOrEmpty(p.Warehouse.Name) && p.Warehouse.Name.ToLower().Contains(cmbWarehouseSourceText.ToLower()));

            if (string.IsNullOrEmpty(cmbWarehouseTargetText))
                warehouseDocuments = warehouseDocuments
                  .Where(p => !string.IsNullOrEmpty(p.Warehouse1.Name) && p.Warehouse1.Name.ToLower().Contains(cmbWarehouseTargetText.ToLower()));

            if (string.IsNullOrEmpty(cmbDocumentTypeText))
                warehouseDocuments = warehouseDocuments
                  .Where(p => p.WarehouseDocumentType.ShortType.ToLower().Contains(cmbDocumentTypeText.ToLower()));

            if (string.IsNullOrEmpty(cmbAuthorText))
                warehouseDocuments = warehouseDocuments
                  .Where(p => (p.Person.FirstName + " " + p.Person.LastName).ToLower().Contains(cmbAuthorText.ToLower()));

            return warehouseDocuments.AsEnumerable();
        }

        public IEnumerable<WarehouseDocument> GetAllWarehouseDocuments(DateTime? dateTime,
          bool isFreeMode,
          int? companyId,
          int? warehouseId,
          int? docId,
          string cmbWarehouseSourceText,
          string cmbWarehouseTargetText,
          string cmbDocumentTypeText,
          string cmbAuthorText,
          string cmbMaterialText,
          string cmbOrderText,
          string cmbDepartmentText,
          string cmbSupplierText,
          string OrderNumberTextboxText,
            DateTime? dateTimeTo = null)
        {
            DateTime? date1 = dateTime.HasValue ? dateTime.Value.Date : (DateTime?)null;
            DateTime? date2 = dateTimeTo.HasValue ? dateTimeTo.Value.Date.AddDays(1) : (DateTime?)null;
                
            warehouseEntities.WarehouseDocuments.Context.CommandTimeout = 300;

            var warehouseDocuments = warehouseEntities.WarehouseDocuments
              .Include(wd => wd.Warehouse)
              .Include(wd => wd.Warehouse1)
              .Include(wd => wd.Warehouse.Company)//"Warehouse.Company")
              .Include(wd => wd.Warehouse1.Company)//"Warehouse1.Company")
              .Include(wd => wd.WarehouseDocumentType)//"WarehouseDocumentType")
              .Include(wd => wd.WarehouseDocumentPositions.Select(wdp => wdp.Material))//"WarehouseDocumentPosition.Material")
              .Include(wd => wd.WarehouseDocumentPositions.Select(wdp => wdp.Order))//"WarehouseDocumentPosition.Order")
              .Include(wd => wd.WarehouseDocumentPositions.Select(wdp => wdp.Department))//"WarehouseDocumentPosition.Department")
                //.Include(wd => wd.WarehouseDocumentPositions.Select(wdp => wdp.ProductionMachine))//"WarehouseDocumentPosition.ProductionMachine")
              .Include(wd => wd.WarehouseDocumentExtensions)//"WarehouseDocumentExtension")
              .Include(wd => wd.ContractingPartyForWarehouseDocuments.Select(cpfw => cpfw.ContractingParty))//"ContractingPartyForWarehouseDocument.ContractingParty")
                //.Include(wd => wd.ProductElementStructure)//"ProductElementStructure")
              .Include(wd => wd.WarehouseDataExchangeDocumentNews)//"WarehouseDataExchangeDocumentNews")
              .Include(wd => wd.Person)//"Person")
                //.Include(wd => wd.WarehouseDocumentType.WarehouseDataExchangeDocTypeMapSIMPLEOverrides1.Select(wdedtmso => wdedtmso.WarehouseDocumentType))//"WarehouseDocumentType.WarehouseDataExchangeDocTypeMapSIMPLEOverridesBefore.WarehouseDocumentTypeAfter")
              .Where
              (wd =>
                !wd.TimeDeleted.HasValue// && (!dateTime.HasValue || (wd.TimeStamp > date1 && wd.TimeStamp < date2))
              );

            //if (dateTime != null)
            //    warehouseDocuments = warehouseDocuments.Where
            //      (wd =>
            //        wd.Date.Year == dateTime.Value.Year
            //        && wd.Date.Month == dateTime.Value.Month
            //        && wd.Date.Day == dateTime.Value.Day
            //      );

            if (date1 != null)
            {
                warehouseDocuments = warehouseDocuments.Where
                  (wd => wd.TimeStamp.Year >= date1.Value.Year
                      && wd.TimeStamp.Month >= date1.Value.Month
                      && wd.TimeStamp.Day >= date1.Value.Day
                  );
            }

            if (date2 != null)
            {
                warehouseDocuments = warehouseDocuments.Where(
                    wd => wd.TimeStamp <= date2.Value);
            }
           

            if (isFreeMode) // this will create smaller query
                warehouseDocuments = warehouseDocuments.Where(p => (p.Warehouse != null && p.Warehouse.CompanyId == companyId) || (p.Warehouse1 != null && p.Warehouse1.CompanyId == companyId));
            else
                warehouseDocuments = warehouseDocuments.Where(p => p.WarehouseSourceId == warehouseId || p.WarehouseTargetId == warehouseId);

            if (!string.IsNullOrEmpty(cmbWarehouseSourceText))
                warehouseDocuments = warehouseDocuments.Where(p => p.Warehouse != null && !string.IsNullOrEmpty(p.Warehouse.Name) && p.Warehouse.Name.ToLower().Contains(cmbWarehouseSourceText.ToLower()));

            if (!string.IsNullOrEmpty(cmbWarehouseTargetText))
                warehouseDocuments = warehouseDocuments.Where(p => p.Warehouse1 != null && !string.IsNullOrEmpty(p.Warehouse1.Name) && p.Warehouse1.Name.ToLower().Contains(cmbWarehouseTargetText.ToLower()));

            if (!string.IsNullOrEmpty(cmbDocumentTypeText))
                warehouseDocuments = warehouseDocuments.Where(p => p.WarehouseDocumentType.ShortType.ToLower().Contains(cmbDocumentTypeText.ToLower()));

            if (!string.IsNullOrEmpty(cmbAuthorText))
                warehouseDocuments = warehouseDocuments.Where(p => (p.Person.FirstName + " " + p.Person.LastName).ToLower().Contains(cmbAuthorText.ToLower()));


            // ------------------------------------ // Additional filtering:

            if (!string.IsNullOrEmpty(cmbMaterialText))
                warehouseDocuments = warehouseDocuments
                  .Where
                  (p =>
                    p.WarehouseDocumentPositions
                      .Any(pp => !string.IsNullOrEmpty(pp.Material.Name) && pp.Material.Name.ToLower().Contains(cmbMaterialText.ToLower()))
                  );

            if (!string.IsNullOrEmpty(cmbOrderText))
                warehouseDocuments = warehouseDocuments
                  .Where
                  (p =>
                    p.WarehouseDocumentPositions
                      .Select(pp => pp.Order)
                      .Any(pp => !string.IsNullOrEmpty(pp.OrderNumber) && pp.OrderNumber.ToLower().Contains(cmbOrderText.ToLower()))
                  );

            if (!string.IsNullOrEmpty(cmbDepartmentText))
                warehouseDocuments = warehouseDocuments
                  .Where
                  (p =>
                    p.WarehouseDocumentPositions
                      .Select(pp => pp.Department)
                      .Any(pp => !string.IsNullOrEmpty(pp.Name) && pp.Name.ToLower().Contains(cmbDepartmentText.ToLower()))
                  );

            if (!string.IsNullOrEmpty(cmbSupplierText)) // supplier name
                warehouseDocuments = warehouseDocuments
                  .Where
                  (p =>
                    (p.ContractingPartyForWarehouseDocuments.Any() ? p.ContractingPartyForWarehouseDocuments.FirstOrDefault().ContractingParty.Name : "-")
                    .ToLower().Contains(cmbSupplierText.ToLower())
                  );

            if (!string.IsNullOrEmpty(OrderNumberTextboxText))
                warehouseDocuments = warehouseDocuments
                  .Where(p => p.Order != null && p.Order.OrderNumber != null && p.Order.OrderNumber.ToLower().Contains(OrderNumberTextboxText));

            return warehouseDocuments.AsEnumerable();
        }

        public IQueryable<WarehouseDocumentType> GetAllWarehouseDocumentTypes()
        {
            IQueryable<WarehouseDocumentType> warehouseDocumentTypes;
            lock (typeof(WarehouseDS))
            {
                // TODO: Turn caching on
                ////object objFromCache = Toolbox.CacherEnt.GetObjectFromCache("AllWarehouseDocymentTypesList");
                ////if (objFromCache != null)
                ////{
                ////    warehouseDocumentTypes = (IQueryable<WarehouseDocumentType>)objFromCache;
                ////}
                ////else
                {
                    warehouseDocumentTypes = warehouseEntities.WarehouseDocumentTypes.OrderBy(p => p.Type);

                    ////Toolbox.CacherEnt.StoreObjectInCache("AllWarehouseDocymentTypesList", warehouseDocumentTypes);
                }
            }
            return warehouseDocumentTypes;
        }

        public IQueryable<WarehouseGroupPermission> GetAllWarehouseGroupPermissions()
        {
            return warehouseEntities.WarehouseGroupPermissions.Where(p => !p.RevokeTime.HasValue);
        }

        public WarehouseGroup[] GetAllWarehouseGroups()
        {
            return warehouseEntities.WarehouseGroups.OrderBy(p => p.Name).ToArray();
        }

        public IQueryable<WarehousePermission> GetAllWarehousePermissions(int? operationId = null)
        {
            return warehouseEntities.WarehousePermissions
                .Include(x => x.WarehouseDocumentType)
                .Include(x => x.Warehouse)
                .Include(x => x.Warehouse1)
                .Where(x => !x.RevokeTime.HasValue && (!operationId.HasValue || x.WarehouseDocumentType.Id == operationId));
        }

        public Warehouse[] GetAllWarehouses(bool simpleOnly = false)
        {
            Warehouse[] warehouses;
            lock (typeof(WarehouseDS))
            {
                ////object objFromCache = Toolbox.CacherEnt.GetObjectFromCache("AllWarehousesList");
                ////if (objFromCache != null)
                ////{
                ////    warehouses = (Warehouse[])objFromCache;
                ////}
                ////else
                {
                    warehouses = warehouseEntities.Warehouses
                        .Include(x => x.WarehouseType)
                        .Include(x => x.Company)
                        .Where(x => x.Active && (!simpleOnly || x.WarehouseSIMPLEs.Any()))
                        .OrderBy(x => x.Name).ToArray();

                    ////Toolbox.CacherEnt.StoreObjectInCache("AllWarehousesList", warehouses);
                    // test:
                    //object objFromCacheTest = Toolbox.CacherEnt.GetObjectFromCache("AllZonesWithParents");
                }
            }

            //warehouses = warehouseEntities.Warehouse.Include("WarehouseType").Include("Company").Where(w => w.Active).OrderBy(p => p.Name);

            return warehouses; //.ToArray();
        }

        public Warehouse[] GetAllWarehouses(int? companyId, bool include = true, bool simpleOnly = true)
        {
            var warehouses = GetAllWarehouses(simpleOnly)
              .Where
              (w =>
                w.Active &&
                (
                  !companyId.HasValue ||
                  (
                    (include && w.Company.Id == companyId) ||
                    (!include && w.Company.Id != companyId)
                  )
                )
              )
              .OrderBy(p => p.Name);
            return warehouses.ToArray();
        }

        //    result.Items = allPersonsRadComboBoxItemData.ToArray();
        public IQueryable<WarehouseType> GetAllWarehouseTypes()
        {
            return warehouseEntities.WarehouseTypes.OrderBy(x => x.Name);
        }

        public IQueryable<Company> GetCompaniesList(string filterName)
        {
            string filterNameNew = filterName != null ? filterName.Trim().ToLower() : null;
            return warehouseEntities.Companies
                .Where(x => string.IsNullOrEmpty(filterNameNew) || x.Name.ToLower().Contains(filterNameNew));
        }

        public Company GetCompanyById(int companyId)
        {
            return warehouseEntities.Companies.FirstOrDefault(w => w.Id == companyId);
        }

        public int GetCompanyByWarehouseId(int warehouseId)
        {
            return GetAllWarehouses().First(w => w.Id == warehouseId).Company.Id;
        }

        public string GetCompanyNameByWarehouseId(int warehouseId)
        {
            return GetAllWarehouses().First(w => w.Id == warehouseId).Company.Name;
        }

        public C_ImportSimple_ContractingPartyFromSimple2 GetContractingPartyFromSimple2ForCPK3(int kontrahentId, int companyId)
        {
            return
                warehouseEntities.C_ImportSimple_ContractingPartyFromSimple2.Where(
                    p => p.ContractingPartyId == kontrahentId && p.CompanyId == companyId).FirstOrDefault();
        }

        public IQueryable<C_ImportSimple_ContractingPartyFromSimple2> GetContractingPartyFromSIMPLE2List(string filterName, int companyId)
        {
            filterName = filterName != null ? filterName.ToLower() : null;
            return warehouseEntities.C_ImportSimple_ContractingPartyFromSimple2
                .Where(x => x.CompanyId == companyId && (string.IsNullOrEmpty(filterName) || x.ContractingPartyName.ToLower().Contains(filterName)));
        }

        public IQueryable<ContractingParty> GetContractingPartyList(string filterName, int companyId)
        {
            filterName = filterName != null ? filterName.ToLower() : null;
            return warehouseEntities.ContractingParties.Include(x => x.Company)
                .Where(x => x.Company.Id == companyId && (string.IsNullOrEmpty(filterName) || x.Name.ToLower().Contains(filterName)));
        }

        public IQueryable<MaterialsToExpendTotal> GetCurrentMaterialsToExpendTotal(int companyId, DateTime? dateTo)
        {
            DateTime dateDate = DateTime.MaxValue;
            if (dateTo.HasValue)
            {
                dateDate = dateTo.Value.AddDays(1);
            }
            var materials = warehouseEntities.MaterialsToExpendTotals
                .Where(x => x.CompanyId == companyId && (!dateTo.HasValue || x.StartDate <= dateTo));

            var materialsMaterialized = new List<MaterialsToExpendTotal>();

            foreach (var material in materials)
            {
                var materialMaterialized = new MaterialsToExpendTotal();
                if (material.TargetDepartmentId.HasValue)
                {
                    Department targetDepartment = GetDepartmentById(material.TargetDepartmentId.Value);
                    //if (targetDepartment != null)
                    //{
                    //    material.TargetDepartmentNameK3 = targetDepartment.Name;
                    //}
                }
                materialMaterialized.CompanyId = material.CompanyId;
                materialMaterialized.IdSIMPLE = material.IdSIMPLE;
                materialMaterialized.IndexSIMPLE = material.IndexSIMPLE;
                materialMaterialized.ProductionNumber = material.ProductionNumber;
                materialMaterialized.StartDate = material.StartDate;
                materialMaterialized.TargetDepartmentId = material.TargetDepartmentId;
                materialMaterialized.TargetDepartmentNameK3 = material.TargetDepartmentNameK3;
                materialMaterialized.TotalQuantityToExpend = material.TotalQuantityToExpend;
                materialMaterialized.WarehouseOrderId = material.WarehouseOrderId;

                materialMaterialized.TargetProductionMachineId = material.TargetProductionMachineId;

                //materialMaterialized.

                materialsMaterialized.Add(materialMaterialized);
            }

            //var test = materialsMaterialized.ToArray();

            return materialsMaterialized.AsQueryable();
        }

        public IQueryable<CurrentStacksForWarehouseAdjusted_WithZTD> GetCurrentStackDetails(int warehouseId, int materialId, int? orderId = null, int? shelfId = null, int? departmentId = null)
        {
            //var currentStackInDocumentsQuery =
            //    from c in
            //        warehouseEntities.CurrentStacksForWarehouseAdjusted_WithZTD.Where(
            //            c => c.WarehouseId == warehouseId && c.MaterialId == materialId && (!orderId.HasValue || c.OrderId == orderId))
            //    select c;

            //return currentStackInDocumentsQuery;

            var query = warehouseEntities.CurrentStacksForWarehouseAdjusted_WithZTD.Where(q => q.WarehouseId == warehouseId && q.MaterialId == materialId);
            if (orderId != null)
                query = query.Where(q => q.OrderId == orderId);
            if (departmentId != null)
                query = query.Where(q => q.DepartmentId == departmentId);
            if (shelfId != null)
                query = query.Where(q => q.ShelfId == shelfId);

            return query;
        }

        public IQueryable<CurrentStackInDocument> GetCurrentStackInDocuments(string materialName, string indexSimple, int? warehouseId)
        {
            return GetCurrentStackInDocuments(materialName, indexSimple, warehouseId, "", null);
        }

        public IQueryable<CurrentStackInDocument> GetCurrentStackInDocuments(string materialName, string indexSimple, int? warehouseId, string orderNumberToFind = "", int? howManyResults = 50)
        {

            var currentStackInDocumentsQuery = warehouseEntities.CurrentStackInDocuments
                .Where(x => (warehouseId == null || x.WarehouseTargetId == warehouseId.Value)
                    && x.Quantity > 0
                    && (String.IsNullOrEmpty(materialName) || x.MaterialName.ToLower().Contains(materialName.ToLower()))
                    && ((String.IsNullOrEmpty(indexSimple) || (x.IndexSIMPLE != null && x.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))
                    || (!String.IsNullOrEmpty(indexSimple) && x.IndexSIMPLE != null && x.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))
                    && ((String.IsNullOrEmpty(orderNumberToFind) || (x.OrderNumber != null && x.OrderNumber.ToLower().Contains(orderNumberToFind.ToLower())))))
                    .OrderBy(x => x.WarehouseDocumentDate);

            //var currentStackInDocumentsQuery = from c in warehouseEntities.CurrentStackInDocuments
            //                                   where (warehouseId == null || c.WarehouseTargetId == warehouseId.Value)
            //                                         && c.Quantity > 0
            //                                         &&
            //                                         (String.IsNullOrEmpty(materialName) ||
            //                                          c.MaterialName.ToLower().Contains(materialName.ToLower()))
            //                                         &&
            //                                         ((String.IsNullOrEmpty(indexSimple) ||
            //                                           (c.IndexSIMPLE != null && c.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))
            //                                          ||
            //                                          (!String.IsNullOrEmpty(indexSimple) && c.IndexSIMPLE != null &&
            //                                           c.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))
            //                                           &&
            //                                         ((String.IsNullOrEmpty(orderNumberToFind) ||
            //                                           (c.OrderNumber != null && c.OrderNumber.ToLower().Contains(orderNumberToFind.ToLower()))))
            //                                   select c;

            currentStackInDocumentsQuery = currentStackInDocumentsQuery.OrderByDescending(p => p.WarehouseDocumentDate);
            if (howManyResults.HasValue)
            {
                return currentStackInDocumentsQuery.Take(howManyResults.Value);
            }
            else
            {
                return currentStackInDocumentsQuery;
            }
        }

        public IQueryable<CurrentStacksForWarehouseAdjusted> GetCurrentStackInDocumentsNew(string materialName, string indexSimple, int? warehouseId, int? shelfId, bool freeOnly, bool indexedOnly, string orderNumberToFind = "", int? howManyResults = 50, bool searchForFullMatch = false)
        {
            var currentStackInDocumentsQuery = warehouseEntities.CurrentStacksForWarehouseAdjusteds
                .Where(x => (warehouseId == null || x.WarehouseId == warehouseId.Value)
                && (!shelfId.HasValue || x.ShelfId == shelfId.Value)
                && x.Quantity > 0
                && (String.IsNullOrEmpty(materialName)
                    || ((searchForFullMatch && x.MaterialName.ToLower() == materialName.ToLower())
                    || (!searchForFullMatch && x.MaterialName.ToLower().Contains(materialName.ToLower()))))
                && ((String.IsNullOrEmpty(indexSimple)
                    || (x.IndexSIMPLE != null && ((searchForFullMatch && x.IndexSIMPLE.ToLower() == indexSimple.ToLower())
                    || (!searchForFullMatch && x.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))))
                    || (!String.IsNullOrEmpty(indexSimple) && x.IndexSIMPLE != null
                    && ((searchForFullMatch && x.IndexSIMPLE.ToLower() == indexSimple.ToLower()) || (!searchForFullMatch && x.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))))
                && ((String.IsNullOrEmpty(orderNumberToFind)
                    || (x.OrderNumber != null && ((searchForFullMatch && x.OrderNumber.ToLower() == orderNumberToFind.ToLower())
                    || (!searchForFullMatch && x.OrderNumber.ToLower().Contains(orderNumberToFind.ToLower()))))))
                && (!freeOnly || (freeOnly && !x.OrderId.HasValue))
                && (!indexedOnly || (indexedOnly && !String.IsNullOrEmpty(x.IndexSIMPLE)))
                && x.ShelfId > 0);

            //var currentStackInDocumentsQuery = from c in warehouseEntities.CurrentStacksForWarehouseAdjusteds
            //                                   where (warehouseId == null || c.WarehouseId == warehouseId.Value)
            //                                         && (!shelfId.HasValue || c.ShelfId == shelfId.Value)
            //                                         && c.Quantity > 0
            //                                         &&
            //                                         (String.IsNullOrEmpty(materialName) ||
            //                                          ((searchForFullMatch && c.MaterialName.ToLower() == materialName.ToLower()) || (!searchForFullMatch && c.MaterialName.ToLower().Contains(materialName.ToLower()))))
            //                                         &&
            //                                         ((String.IsNullOrEmpty(indexSimple) ||
            //                                           (c.IndexSIMPLE != null && ((searchForFullMatch && c.IndexSIMPLE.ToLower() == indexSimple.ToLower()) || (!searchForFullMatch && c.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))))
            //                                          ||
            //                                          (!String.IsNullOrEmpty(indexSimple) && c.IndexSIMPLE != null &&

            //                                           ((searchForFullMatch && c.IndexSIMPLE.ToLower() == indexSimple.ToLower()) || (!searchForFullMatch && c.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))))
            //                                           &&
            //                                         ((String.IsNullOrEmpty(orderNumberToFind) ||
            //                                           (c.OrderNumber != null && ((searchForFullMatch && c.OrderNumber.ToLower() == orderNumberToFind.ToLower()) || (!searchForFullMatch && c.OrderNumber.ToLower().Contains(orderNumberToFind.ToLower()))))))

            //                                           && (!freeOnly || (freeOnly && !c.OrderId.HasValue))
            //                                           && (!indexedOnly || (indexedOnly && !String.IsNullOrEmpty(c.IndexSIMPLE)))

            //                                           && c.ShelfId > 0
            //                                   select c;

            currentStackInDocumentsQuery = currentStackInDocumentsQuery.OrderBy(p => p.MaterialName);

            if (howManyResults.HasValue)
            {
                return currentStackInDocumentsQuery.Take(howManyResults.Value);
            }
            else
            {
                return currentStackInDocumentsQuery;
            }
        }

        public IEnumerable<CurrentStacksForWarehouseAdjusted_WithZTD_S_ToPresent> GetCurrentStackInDocumentsNewNew(int? materialId, string materialNameOrSimpleId, int warehouseId, bool indexedOnly, int? orderIdToFind, int? shelfIdToFind)
        {
            var query = warehouseEntities.CurrentStacksForWarehouseAdjusted_WithZTD.Where(c => c.WarehouseId == warehouseId);

            if (materialId != null)
            {
                query = query.Where(q => q.MaterialId == materialId);
            }
            else if (!String.IsNullOrEmpty(materialNameOrSimpleId))
            {
                materialNameOrSimpleId = materialNameOrSimpleId.Trim().ToLower();
                query = query.Where(q => q.IndexSIMPLE.ToLower().Contains(materialNameOrSimpleId) || q.MaterialName.ToLower().Contains(materialNameOrSimpleId));
            }

            if (orderIdToFind != null)
                query = query.Where(q => q.OrderId == orderIdToFind);

            if (shelfIdToFind != null)
                query = query.Where(q => q.ShelfId == shelfIdToFind);

            var group = query.GroupBy(q => new { q.WarehouseId, q.MaterialId }).Select(q => new CurrentStacksForWarehouseAdjusted_WithZTD_S_ToPresent()
                                                                                {
                                                                                    IndexSIMPLE = q.FirstOrDefault().IndexSIMPLE,
                                                                                    MaterialName = q.FirstOrDefault().MaterialName,
                                                                                    Quantity = q.Sum(qq => qq.Quantity),
                                                                                    // !orderIdToFind.HasValue ? p.Quantity : warehouseDs.GetCurrentStackDetails(p.WarehouseId.Value, p.MaterialId, orderIdToFind).Sum(q => q.Quantity),
                                                                                    UnitShortName = q.FirstOrDefault().UnitShortName + ".",
                                                                                    WarehouseId = q.FirstOrDefault().WarehouseId,
                                                                                    MaterialId = q.FirstOrDefault().MaterialId,
                                                                                });

            return group.OrderBy(p => p.IndexSIMPLE);
        }

        private IQueryable<CurrentStackInDocumentsGrouped> GetCurrentStacks(int companyId, DateTime dateToFilter, string materialToFind, string indexSimpleToFind, int currentWarehouseId, string orderNumberToFind, bool forOrder = true)//, Action<InvokeServerMethodOperation> userCallback = null)
        {
#if DEBUG
            Contract.Requires(materialToFind != null);
            Contract.Requires(indexSimpleToFind != null);
            Contract.Requires(orderNumberToFind != null);
#endif
            string materialToFindQ = materialToFind.ToLower();
            string indexSimpleToFindQ = indexSimpleToFind.ToLower();
            string orderNumberToFindQ = orderNumberToFind.ToLower();

            var query = warehouseEntities.CurrentStackInDocumentsGroupeds
                .Where
                (m =>
                  (string.IsNullOrEmpty(orderNumberToFind) || m.OrderId.HasValue == forOrder) && //(!string.IsNullOrEmpty(m.OrderNumber) == forOrder)
                  m.CompanyId == companyId &&
                  m.MaterialName.ToLower().Contains(materialToFindQ) &&
                  m.IndexSIMPLE.ToLower().Contains(indexSimpleToFindQ) &&
                  m.WarehouseId == currentWarehouseId &&
                  (m.OrderNumber.Contains(orderNumberToFindQ) || (m.OrderNumber == "" && orderNumberToFindQ == "")) &&
                  !m.ReservationDocSubtypeId.HasValue
                );

            return query;
        }

        public IEnumerable<MaterialsToGiveRadGridElement> GetMaterialsToGiveRadGrid(int companyId, DateTime dateToFilter, string materialToFind, string indexSimpleToFind, int currentWarehouseId, string orderNumberToFind, List<PositionWithQuantityAndShelfs> positionsToGive)
        {
#if DEBUG
            Contract.Requires(materialToFind != null);
            Contract.Requires(indexSimpleToFind != null);
            Contract.Requires(orderNumberToFind != null);
#endif
            var materialsToGiveTotalFromWarehouseGroupeds = GetCurrentStacks(companyId, dateToFilter, materialToFind, indexSimpleToFind, currentWarehouseId, orderNumberToFind);

            var elements = materialsToGiveTotalFromWarehouseGroupeds
              .AsEnumerable()
              .Select
              (x => new MaterialsToGiveRadGridElement()
                {
                    MaterialName = x.MaterialName,
                    IndexSIMPLE = x.IndexSIMPLE,
                    NameSIMPLE = x.NameSIMPLE,
                    Quantity = (decimal?)x.Quantity - Math.Abs
                    (
                      positionsToGive
                      .Where
                      (v =>
                        v.position.Material.Id == x.MaterialId &&
                        (
                          ((v.position.Order == null || v.position.Order.OrderNumber == "") && !x.OrderId.HasValue) ||
                          (v.position.Order != null && v.position.Order.Id == x.OrderId)
                        )
                      )
                      .Sum(w => (decimal)w.position.Quantity)
                    ),
                    UnitName = x.UnitName,
                    UnitPrice = x.UnitPrice,
                    OrderNumber = x.OrderNumber,
                    MaterialId = x.MaterialId,
                    OrderId = x.OrderId,
                }
              );

            return elements;
        }

        public IQueryable<GiveMaterialReturnMaterialsToGiveRadGridElement> GetCurrentStacksForReturns
        (
          int? companyId,
          DateTime dateToFilter,
          string materialToFind,
          string indexSimpleToFind,
          int? currentWarehouseId,
          string orderNumberToFind,
          ICollection<PositionWithQuantityAndShelfs> positionsToGive,
          bool forOrder = true)//, Action<InvokeServerMethodOperation> userCallback = null)
        {
#if DEBUG
            Contract.Requires(materialToFind != null);
            Contract.Requires(indexSimpleToFind != null);
            Contract.Requires(orderNumberToFind != null);
#endif
            string materialToFindQ = materialToFind.ToLower();
            string indexSimpleToFindQ = indexSimpleToFind.ToLower();
            string orderNumberToFindQ = orderNumberToFind.ToLower();

            var query = warehouseEntities.MaterialsToReturnFromWarehouses
                .Where
                (m =>
                  m.WarehouseSourceId.HasValue
                  && (m.OrderId.HasValue == forOrder) //(!string.IsNullOrEmpty(m.OrderNumber) == forOrder)
                    && m.CompanyId == companyId
                    && m.MaterialName.ToLower().Contains(materialToFindQ)
                    && m.IndexSIMPLE.ToLower().Contains(indexSimpleToFindQ)
                    && m.WarehouseId == currentWarehouseId
                  && m.OrderNumber.Contains(orderNumberToFindQ)
                );

            var finalMaterialsToGiveDataSource = query
              .AsEnumerable()
              .Select
              (p => new GiveMaterialReturnMaterialsToGiveRadGridElement()
                {
                    MaterialName = p.MaterialName,
                    IndexSIMPLE = p.IndexSIMPLE,
                    NameSIMPLE = p.NameSIMPLE,
                    Quantity = p.Quantity - Math.Abs
                    (
                      (
                        positionsToGive
                          .Where
                          (v =>
                            v.position.MaterialId == p.MaterialId &&
                            (
                              ((!v.position.OrderId.HasValue || v.position.Order.OrderNumber == string.Empty) && !p.OrderId.HasValue) ||
                              (v.position.OrderId.HasValue && v.position.OrderId == p.OrderId)
                            )
                          )
                      )
                      .Sum(w => w.position.Quantity)
                    ),
                    UnitName = p.UnitName,
                    UnitPrice = p.UnitPrice,
                    OrderNumber = p.OrderNumber,
                    MaterialId = p.MaterialId,
                    OrderId = p.OrderId,
                    SourceWarehouseId = p.WarehouseSourceId,
                    SourceWarehouseName = p.WarehouseSourceName
                }
              );

            return finalMaterialsToGiveDataSource.AsQueryable();
        }

        public WarehouseDocumentPositionKCStateType[] GetCustomChamberStateTypes()
        {
            return warehouseEntities.WarehouseDocumentPositionKCStateTypes.ToArray();
        }

        public WarehouseCustomsChamberPosition[] GetCustomsChamberPositions(int warehouseDocumentPositionId)
        {
            var ccposs = warehouseEntities.WarehouseCustomsChamberPositions
                .Where(p => p.WarehouseDocumentPositionId == warehouseDocumentPositionId && p.WarehouseDocumentPositionKCStateId > 0);

            if (ccposs != null)
            {
                return ccposs.ToArray();
            }
            throw new Exception("Błąd odczytu listy WarehouseCustomsChamberPosition[]");
        }

        public IEnumerable<CustomsChamberMaterialsGridElement> GetCustomsChamberPositionsHeaders(bool isExtendedMode)
        {
            var warehouseCustomsChamberPositions_Headers = warehouseEntities.WarehouseCustomsChamberPositions_Header
              .AsQueryable();

            if (!isExtendedMode)
                warehouseCustomsChamberPositions_Headers = warehouseCustomsChamberPositions_Headers
                  .Where(p => p.QuantityKCNotAssigned == 0 && p.QuantityKCAssignedFinalized == p.QuantityKCAssigned);

            var datasource = warehouseCustomsChamberPositions_Headers
              .Select
              (p => new
                {
                    WarehouseDocumentPositionId = p.WarehouseDocumentPositionId,
                    OrderId = p.OrderId,
                    ContractingPartyName = p.ContractingPartyName,
                    OrderNumberFromPZ = p.OrderNumberFromPZ,
                    IndexSIMPLE = p.IndexSIMPLE,
                    MaterialName = p.MaterialName,
                    QuantityOrdered = p.QuantityOrdered,
                    QuantityReceivedKC = p.QuantityReceivedKC,
                    UnitName = p.UnitShortName,
                    OrderNumber = p.OrderNumber,
                    Assignment = "nadwyżka", //isExtendedMode ? p.OrderNumber : 
                    ShelfName = p.ShelfName,
                    ArrivalDate = warehouseEntities.WarehouseDocuments.FirstOrDefault(d => d.Id == p.WarehouseDocumentId) != null
                      ? warehouseEntities.WarehouseDocuments.FirstOrDefault(d => d.Id == p.WarehouseDocumentId).Date
                      : (DateTime?)null,
                    QuantityKCAssigned = p.QuantityKCAssigned,
                    QuantityKCAssignedFinalized = p.QuantityKCAssignedFinalized,
                    QuantityKCNotAssigned = p.QuantityKCNotAssigned
                }
              );

            var elements = datasource.AsEnumerable()
              .Select
              ((p, i) => new CustomsChamberMaterialsGridElement()
              {
                  Lp = i + 1,
                  WarehouseDocumentPositionId = p.WarehouseDocumentPositionId,
                  OrderId = p.OrderId,
                  ContractingPartyName = p.ContractingPartyName,
                  OrderNumberFromPZ = p.OrderNumberFromPZ,
                  IndexSIMPLE = p.IndexSIMPLE,
                  MaterialName = p.MaterialName,
                  QuantityOrdered = p.QuantityOrdered,
                  QuantityReceivedKC = p.QuantityReceivedKC,
                  UnitName = p.UnitName,
                  OrderNumber = p.OrderNumber,
                  Assignment = p.Assignment,
                  ShelfName = p.ShelfName,
                  ArrivalDate = p.ArrivalDate,
                  QuantityKCAssigned = p.QuantityKCAssigned,
                  QuantityKCAssignedFinalized = p.QuantityKCAssignedFinalized,
                  QuantityKCNotAssigned = p.QuantityKCNotAssigned
              });

            return elements.AsEnumerable();
        }

        public WarehouseCustomsChamberPositionsToReturn[] GetCustomsChamberPositionsToReturn(int warehouseId)
        {
            return warehouseEntities.WarehouseCustomsChamberPositionsToReturns.Where(x => x.WarehouseId == warehouseId).ToArray();
        }

        public WarehouseCustomsChamberPosition[] GetCustomsChamberReturnedPositions(int? warehouseId = null)
        {
            return warehouseEntities.WarehouseCustomsChamberPositions
                .Where(x => (!warehouseId.HasValue || (x.WarehouseId == warehouseId.Value))
                    && x.KCStateTypeId == 1 && x.KCSolvingDocumentId.HasValue).ToArray();
        }

        public List<WarehouseDataExchangeDocumentNew> GetDataExchangeDocumentForMag3Doc(int warehouseDocumentId)
        {
            return
                warehouseEntities.WarehouseDataExchangeDocumentNews
                .Where(p => p.WarehouseDocumentId == warehouseDocumentId)
                    .ToList();
        }

        public Department GetDepartmentById(int departmentId)
        {
            return warehouseEntities.Departments.FirstOrDefault(d => d.Id == departmentId);
        }

        public IQueryable<Department> GetDepartmentsList(string filterName)
        {
            string filterNameNew = filterName != null ? filterName.Trim().ToLower() : null;

            return warehouseEntities.Departments
                .Where(x => string.IsNullOrEmpty(filterName) || x.Name.ToLower().Contains(filterNameNew));
        }

        public IQueryable<DocTypesFromSIMPLE_RW> GetDocTypesFromSIMPLE_RW(int companyId)
        {
            return warehouseEntities.DocTypesFromSIMPLE_RW
                .Where(x => x.CompanyId == companyId)
                .OrderBy(x => x.typdrw_id);
        }

        public IQueryable<DocTypesFromSIMPLE_WZ> GetDocTypesFromSIMPLE_WZ(int companyId)
        {
            return warehouseEntities.DocTypesFromSIMPLE_WZ
                .Where(x => x.CompanyId == companyId)
                .OrderBy(x => x.typdwz_id);
        }

        public int GetDocumentIdByPositionId(int warehouseDocumentPositionId)
        {
            return warehouseEntities.WarehouseDocumentPositions
                .Include(x => x.WarehouseDocument)
                .First(x => x.Id == warehouseDocumentPositionId)
                .WarehouseDocument.Id;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="warehouseDocumentId"></param>
        /// <returns>materialId, orderId, packageTypeName, packagesNumber, zoneName</returns>
        public List<Tuple<int, int?, string, int, string>> GetDocumentPackagesInfo(int warehouseDocumentId)
        {
            WarehouseDocument document =
                warehouseEntities.WarehouseDocuments
                .Include(x => x.WarehouseDocumentPositions)
                .Include(x => x.WarehouseDocumentPositions.Select(m => m.Material))
                .Where(x => x.Id == warehouseDocumentId).FirstOrDefault();

            List<Tuple<int, int?, string, int, string>> packages = new List<Tuple<int, int?, string, int, string>>();

            foreach (var position in document.WarehouseDocumentPositions)
            {
                // get package info from currentstacks
                var packagesStack1 =
                    warehouseEntities.CurrentStackInPackages
                    .Where(p => p.WarehouseDocumentPositionId == position.Id && p.PackageTypeId.HasValue);

                foreach (var package in packagesStack1)
                {
                    //Tuple<int, int, string, int> packageInfo = new Tuple<int, int, string, int>();
                    Tuple<int, int?, string, int, string> newPckgInfo =
                        new Tuple<int, int?, string, int, string>(
                            package.MaterialId,
                            package.OrderId,
                            package.PackageTypeName,
                            ((package.PosNumberOfPackages.HasValue ? package.PosNumberOfPackages.Value : 0)), package.ZoneName);

                    packages.Add(newPckgInfo);
                }

                //var packagesStack2 =
                //    warehouseEntities.CurrentStackInPackages.Where(p => p.WarehouseDocumentPositionId == position.Id && p.PackageTypeId.HasValue);

                foreach (var package in packagesStack1)
                {
                    //Tuple<int, int, string, int> packageInfo = new Tuple<int, int, string, int>();
                    Tuple<int, int?, string, int, string> newPckgInfo =
                        new Tuple<int, int?, string, int, string>(
                            package.MaterialId,
                            package.OrderId,
                            "{luz}",
                            (Convert.ToInt32(package.UnpackedQuantity)),
                            package.ZoneName); // - (package.RelNumberOfPackages.HasValue ? package.RelNumberOfPackages.Value : 0)));
                    packages.Add(newPckgInfo);
                }
            }
            return packages;
        }

        public List<int> GetDocumentParentIdsByPositionId(int warehouseDocumentPositionId)
        {
            return warehouseEntities.WarehouseDocumentRelations
                .Include(x => x.WarehouseDocument)
                .Include(x => x.WarehouseDocument1)
                .Include(x => x.WarehouseDocument1.WarehouseDocumentPositions)
                .Where(x => x.WarehouseDocument1.WarehouseDocumentPositions.Any(r => r.Id == warehouseDocumentPositionId))
                .Select(x => x.Id).ToList();
        }

        //public List<int> GetDocumentParentIdsByPositionId(int warehouseDocumentPositionId)
        //{
        //    List<int> documents = warehouseEntities
        //        .WarehouseDocumentRelations
        //        .Include("WarehouseDocument")
        //        .Include("WarehouseDocument1")
        //        .Include("WarehouseDocument1.WarehouseDocumentPosition")
        //        .Where(x => x.WarehouseDocument1.WarehouseDocumentPositions.Any(r => r.Id == warehouseDocumentPositionId))
        //        .Select(x => x.Id)
        //        .ToList();

        //    return documents;
        //}

        public List<Tuple<int, string, double, string>> GetDocumentSourceZonesInfo(int warehouseDocumentId)
        {
            var parentDocuments = warehouseEntities.WarehouseDocumentRelations
                .Include(x => x.WarehouseDocument.WarehouseDocumentType)
                .Include(x => x.WarehouseDocument1)
                .Where(x => x.WarehouseDocument1.Id == warehouseDocumentId);

            // Redirect to Za document's zones, in case of Za doc...:
            //var parentDocuments = warehouseEntities.WarehouseDocumentRelations
            //    .Include("WarehouseDocument.WarehouseDocumentType")
            //    .Include("WarehouseDocument1")
            //    .Where(wdr => wdr.WarehouseDocument1.Id == warehouseDocumentId);
            WarehouseDocumentRelation wdrParent = parentDocuments.FirstOrDefault();


            if (parentDocuments.Count() == 1 && wdrParent.WarehouseDocument.WarehouseDocumentType.Id == 10)
            {
                warehouseDocumentId = wdrParent.WarehouseDocument.Id;
            }

            WarehouseDocument document =
                warehouseEntities.WarehouseDocuments
                .Include(x => x.WarehouseDocumentPositions)
                .Include(x => x.WarehouseDocumentPositions.Select(y => y.Material))
                .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionRelations1))
                .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionRelations1.Select(z => z.Zone)))
                .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionPackages))
                .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionPackages.Select(z => z.PackageType)))
                .Where(x => x.Id == warehouseDocumentId).FirstOrDefault();

            // in old version 
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionPackage.WarehouseDocumentPositionPackageRelation")


            //WarehouseDocument document =
            //    warehouseEntities.WarehouseDocuments
            //    .Include("WarehouseDocumentPosition")
            //    .Include("WarehouseDocumentPosition.Material")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionRelation1")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionRelation1.Zone")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionPackage")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionPackage.PackageType") //WarehouseDocumentPositionPackageRelation
            //    .Where(d => d.Id == warehouseDocumentId).FirstOrDefault();


            ////List<string> zonesInfo = new List<string>();
            //WarehouseDocument document =
            //    warehouseEntities.WarehouseDocuments
            //    .Include("WarehouseDocumentPosition")
            //    .Include("WarehouseDocumentPosition.Material")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionRelation1")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionRelation1.Zone")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionPackage")
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionPackage.PackageType") //WarehouseDocumentPositionPackageRelation
            //    .Include("WarehouseDocumentPosition.WarehouseDocumentPositionPackage.WarehouseDocumentPositionPackageRelation")
            //    .Where(d => d.Id == warehouseDocumentId).FirstOrDefault();

            List<Tuple<int, string, double, string>> sources = new List<Tuple<int, string, double, string>>();

            foreach (var position in document.WarehouseDocumentPositions)
            {
                foreach (var positionRelation in position.WarehouseDocumentPositionRelations1
                  .Where(p => !p.TimeCancelled.HasValue))
                {
                    Tuple<int, string, double, string> existingSource =
                        sources
                        .Where(
                            s =>
                                s.Item1 == position.Material.Id
                                &&
                                s.Item2 == position.Material.Name
                                &&
                                s.Item4 == positionRelation.Zone.Name
                            ).FirstOrDefault();

                    if (existingSource != null)
                    {
                        double existingQuantity = existingSource.Item3;
                        int i = sources.IndexOf(existingSource);
                        sources[i] = new Tuple<int, string, double, string>(position.Material.Id, position.Material.Name, existingQuantity + positionRelation.Quantity, positionRelation.Zone.Name);
                    }
                    else
                    {
                        sources.Add(new Tuple<int, string, double, string>(position.Material.Id, position.Material.Name, positionRelation.Quantity, positionRelation.Zone != null ? positionRelation.Zone.Name : ""));
                    }
                }
            }

            //foreach (var s in sources)
            //{
            //    zonesInfo.Add(s.Item2 + "<b>: " + s.Item3 + "j.m. z półki: " + s.Item4 + "</b>");
            //}

            return sources; // zonesInfo; //.OrderBy(p => p).ToList();
        }

        public string GetDocumentSourceZonesInfoFormatted(int warehouseDocumentId)
        {
            //return "";
            var docLocationInfo = GetDocumentSourceZonesInfo(warehouseDocumentId);
            return "<font size='+1' color='#333333'><b><u>Lokalizacje źródłowe:</u><br /><ul>" + string.Join("",
                                                 (from p in docLocationInfo
                                                  select
                                                      "<li>[" + p.Item2 + "] * " + p.Item3 +
                                                      " z [" + p.Item4 + "]</li>").ToArray()) + "</ul></b></font>";
        }

        public IQueryable<WarehouseDocumentType> GetDocumentTypeList(string filterName)
        {
            return warehouseEntities.WarehouseDocumentTypes
                .Where(x => string.IsNullOrEmpty(filterName) || x.ShortType.ToLower().Contains(filterName.ToLower()));
        }

        public IQueryable<WarehouseDocumentPosition> GetEmergencyMaterialOrders(int orderId, int? materialId, int? personId, string technologistPersonName)
        {
            var query = warehouseEntities.WarehouseDocumentPositions
              .Include(wds => wds.WarehouseDocument.Person)
              .Include(wds => wds.Material.Unit)
              .Include(wds => wds.Order.WarehouseProductionOrders)
                //.AddIncludePaths("WarehouseDocument.Person",
                //                 "Material.Unit",
                //                 "Order.Productions")
                .Where(p =>
                  !p.WarehouseDocument.TimeDeleted.HasValue &&
                  p.OrderId == orderId &&
                  (materialId == null || p.MaterialId == materialId) &&
                  (personId == null || p.WarehouseDocument.AuthorId == personId) &&
                  (technologistPersonName == null || p.Order.Productions.Any(t => t.Technologist == technologistPersonName)) &&
                  p.WarehouseDocument.DocumentTypeId == 10 &&
                  p.WarehouseDocument.DocumentSubtypeId == 3
                );
            //query.QueryStrategy = QueryStrategy.DataSourceOnly;
            //query.ExecuteAsync(getEmergencyMaterialOrders1Completed);
            return query;
        }

        /// <summary>
        /// Returns Position's Material name extended by damage status markup
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public string GetExtendedMaterialName(WarehouseDocumentPosition position)
        {
#if DEBUG
            Contract.Requires(position != null);
#endif
            string nameStatusExtension = "";
            var featuredStatuses = GetPositionFeaturedStatusList(position);

            var warehouseDocument = GetWarehouseDocumentForPositionId(position.Id);
#if DEBUG
            Contract.Assert(warehouseDocument != null);
#endif
            if (warehouseDocument.DocumentTypeId == 13)
            {
                nameStatusExtension += " (KC)";
            }
            if (featuredStatuses.Contains(PositionFeaturedStatus.Garbage))
            {
                nameStatusExtension += " (G)";
            }
            if (featuredStatuses.Contains(PositionFeaturedStatus.Package))
            {
                nameStatusExtension += " (O)";
            }
            if (featuredStatuses.Contains(PositionFeaturedStatus.Damaged))
            {
                nameStatusExtension += " (Z/";
                nameStatusExtension = nameStatusExtension + "" + GetPositionDamageWarehouseShortName(position) + ")";
            }
            if (featuredStatuses.Contains(PositionFeaturedStatus.DamagedFromDamaged))
            {
                nameStatusExtension += " (ZZ)";
            }

            var refreshedPosition =
                warehouseEntities.WarehouseDocumentPositions
                  .Include(wdp => wdp.Material)
                  .Where(pos => pos.Id == position.Id);

            return refreshedPosition.FirstOrDefault().Material.Name + nameStatusExtension;
        }

        public WarehouseDocument.WarehouseDocumentEditStateEnum GetFormState(string formName)
        {
            var state = HttpContext.Current.Session["WDFormEditState_" + formName];
            if (state != null)
            {
                return (WarehouseDocument.WarehouseDocumentEditStateEnum)state;
            }
            else
            {
                return WarehouseDocument.WarehouseDocumentEditStateEnum.Unknown;
            }
        }

        public IQueryable<MaterialData> GetFullSimpleIndexesSet(string filterValue, int? companyId = null)
        {
            if (filterValue != null)
            {
                filterValue = filterValue.Trim().ToLower();
            }

            var prefilteredMaterials = warehouseEntities.Materials
              .Where(FilterMaterial(companyId, filterValue));
            var pm = prefilteredMaterials.ToList();

            var prefilteredMaterialsSimple = warehouseEntities.C_ImportSimple_MaterialFromSimple
              .Where(FilterMaterialSimple(companyId, filterValue));
            var mps = prefilteredMaterialsSimple.ToList();

            var materials = prefilteredMaterials
              .GroupJoin(prefilteredMaterialsSimple, m => m.IdSIMPLE, ms => ms.IdSimple, (m, msGroup) => new { m, msGroup })
              .SelectMany(x => x.msGroup.DefaultIfEmpty(), (x, ms) => new { x.m, ms });

            var materialsSimple = prefilteredMaterialsSimple
              .GroupJoin(prefilteredMaterials, ms => ms.IdSimple, m => m.IdSIMPLE, (ms, mGroup) => new { ms, mGroup })
              .SelectMany(x => x.mGroup.DefaultIfEmpty(), (x, m) => new { m, x.ms });

            var allMaterialsMaterialsSimple = materials.Union(materialsSimple);

            // result set
            return allMaterialsMaterialsSimple.Select(x => new MaterialData()
            {
                SimpleId = x.ms.IdSimple,	// SimpleId
                CompanyId = companyId,	// CompanyId
                MaterialName = x.ms.NameSimple,	// MaterialName
                IndexSIMPLE = x.ms.IndexSIMPLE,	// IndexSIMPLE
                IndexSimpleK3 = x.m.IndexSIMPLE, // the index in K3 can be older than index from Simple if Simple name was changed
                UnitName = x.ms.UnitName	// UnitName
            });

            // This method can be optimized to include more elements where K3 has some elements and Simple does not.
            // However this seems highly unlikely and would point to some data inconsistencies in the system,
            // so fully supporting such scenario might hide some otherwise difficult-to-track bugs
        }

        public IQueryable<Material> GetIndexesSIMPLEList(string filterName)
        {
            return warehouseEntities.Materials
                .Where(x => (string.IsNullOrEmpty(filterName) || x.IndexSIMPLE.ToLower().StartsWith(filterName.ToLower())));
        }

        public IQueryable<WarehousePermissionVirtual> GetJoinedWarehousePermissions(int? sourceWarehouseId, int? targetWarehouseId, int? operationId = null)
        {
            WarehouseGroupPermission[] groupPermissions;
            if (operationId.HasValue)
            {
                // Adding warehouse group permissions:
                groupPermissions = (from p in GetAllGroupPermissions(operationId)
                                    select p).ToArray();
            }
            else
            {
                groupPermissions = (from p in GetAllGroupPermissions()
                                    select p).ToArray();
            }

            List<WarehousePermissionVirtual> warehousePermissionsForGrid = new List<WarehousePermissionVirtual>();

            foreach (var permission in groupPermissions)
            {
                int perm_id = permission.Id;
                var wg1 = permission.WarehouseGroup;
                var wg2 = permission.WarehouseGroup1;

                // Check if the group contains warehouse
                bool sourceWarehouseInGroup = false;
                bool targetWarehouseInGroup = false;

                if (sourceWarehouseId.HasValue && sourceWarehouseId > 0)
                {
                    sourceWarehouseInGroup = CheckIfWarehouseInGroup(GetWarehouseByIdIB(sourceWarehouseId.Value), permission.WarehouseGroup);
                }

                if (targetWarehouseId.HasValue && targetWarehouseId > 0)
                {
                    targetWarehouseInGroup = CheckIfWarehouseInGroup(GetWarehouseByIdIB(targetWarehouseId.Value), permission.WarehouseGroup1);
                }

                if (
                    (
                        sourceWarehouseId == 0
                        ||
                        sourceWarehouseInGroup
                        ||
                        (
                            (
                                permission.WarehouseGroup == null || permission.WarehouseGroup.Id == 0
                            )
                            && sourceWarehouseId == null
                        )
                    )
                    &&
                    (
                        targetWarehouseId == 0
                        ||
                        targetWarehouseInGroup
                        ||
                        (
                            (
                                permission.WarehouseGroup1 == null || permission.WarehouseGroup1.Id == 0
                            )
                            && targetWarehouseId == null
                        )
                    )
                    )
                {
                    WarehousePermissionVirtual wpfg = new WarehousePermissionVirtual();

                    wpfg.WarehouseId = null;
                    wpfg.Warehouse1Id = null;

                    wpfg.WarehouseId = sourceWarehouseId;
                    wpfg.Warehouse1Id = targetWarehouseId;

                    if (wpfg.WarehouseId != null)
                    {
                        wpfg.Warehouse = GetWarehouseById(Convert.ToInt32(wpfg.WarehouseId));
                    }
                    if (wpfg.Warehouse1Id != null)
                    {
                        wpfg.Warehouse1 = GetWarehouseById(Convert.ToInt32(wpfg.Warehouse1Id));
                    }

                    wpfg.WarehouseDocumentType = GetDocumentTypeList("").Where(p => p.Id == permission.WarehouseDocumentType.Id).FirstOrDefault();

                    wpfg.MaterialIsPackage = permission.MaterialIsPackage;
                    wpfg.PositionIsGarbage = permission.PositionIsGarbage;
                    wpfg.PositionIsZZ = permission.PositionIsZZ;
                    wpfg.PositionIsPartlyDamaged = permission.PositionIsPartlyDamaged;
                    wpfg.PositionIsDamaged = permission.PositionIsDamaged;

                    //warehousePermissionsForGrid.Add(wpfg);
                    if (!warehousePermissionsForGrid.Any(
                        p =>
                            p.WarehouseDocumentType.Id == wpfg.WarehouseDocumentType.Id
                            &&
                            p.WarehouseId == wpfg.WarehouseId
                            &&
                            p.Warehouse1Id == wpfg.Warehouse1Id
                            ))
                    {
                        warehousePermissionsForGrid.Add(wpfg);
                    }
                }
            }

            if (targetWarehouseId > 1)
            {
                //int y = 0;
            }

            // Adding warehouse permissions:
            var warehousePermissions = from p in GetAllWarehousePermissions(operationId)
                                       where
                                       (sourceWarehouseId == 0 || ((p.Warehouse == null || p.Warehouse.Id == 0) && sourceWarehouseId == null) || ((p.Warehouse != null && p.Warehouse.Id > 0) && p.Warehouse.Id == sourceWarehouseId))
                                       &&
                                       (targetWarehouseId == 0 || ((p.Warehouse1 == null || p.Warehouse1.Id == 0) && targetWarehouseId == null) || ((p.Warehouse1 != null && p.Warehouse1.Id > 0) && p.Warehouse1.Id == targetWarehouseId))
                                       select p;

            foreach (var permission in warehousePermissions)
            {
                WarehousePermissionVirtual wpfg = new WarehousePermissionVirtual();

                if (permission.Warehouse != null)
                {
                    wpfg.WarehouseId = permission.Warehouse.Id;
                    wpfg.Warehouse = GetWarehouseById(Convert.ToInt32(wpfg.WarehouseId));
                }

                if (permission.Warehouse1 != null)
                {
                    wpfg.Warehouse1Id = permission.Warehouse1.Id;
                    wpfg.Warehouse1 = GetWarehouseById(Convert.ToInt32(wpfg.Warehouse1Id));
                }

                wpfg.WarehouseDocumentType = GetDocumentTypeList("").Where(p => p.Id == permission.WarehouseDocumentType.Id).FirstOrDefault();

                wpfg.MaterialIsPackage = permission.MaterialIsPackage;
                wpfg.PositionIsGarbage = permission.PositionIsGarbage;
                wpfg.PositionIsZZ = permission.PositionIsZZ;
                wpfg.PositionIsPartlyDamaged = permission.PositionIsPartlyDamaged;
                wpfg.PositionIsDamaged = permission.PositionIsDamaged;

                if (!warehousePermissionsForGrid.Any(
                    p =>
                        p.WarehouseDocumentType.Id == wpfg.WarehouseDocumentType.Id
                        &&
                        p.WarehouseId == wpfg.WarehouseId
                        &&
                        p.Warehouse1Id == wpfg.Warehouse1Id
                        ))
                {
                    warehousePermissionsForGrid.Add(wpfg);
                }
            }

            //var x = warehousePermissionsForGrid.ToArray();

            return warehousePermissionsForGrid.AsQueryable();
        }

        public Zone GetKCZone()
        {
            return warehouseEntities.Zones.Where(z => z.Name == "KC").First();
        }

        public Material GetMaterialById(int materialId, bool includeChildMaterials = false)
        {
            if (!includeChildMaterials)
            {
                return warehouseEntities.Materials.Include(x => x.Unit).First(p => p.Id == materialId);
            }
            else
            {
                return warehouseEntities.Materials.Include(x => x.Unit).Include("ChildMaterials.Unit").First(p => p.Id == materialId);
            }
        }

        public Material GetMaterialByIdDataOnly(int materialId)
        {
            Material material;

            material = warehouseEntities.Materials.First(p => p.Id == materialId);

            Material materialDataOnly = new Material();
            materialDataOnly.Id = material.Id;
            materialDataOnly.Name = material.Name;

            return materialDataOnly;
        }

        public Material GetMaterialByIndex(string index, int companyId)
        {
            return warehouseEntities.Materials.First(p => p.IndexSIMPLE == index && p.CompanyId == companyId);
        }

        public Material GetMaterialByNameWithIndex(string name, int companyId)
        {
            return warehouseEntities.Materials.First(p => p.Name.ToLower().Contains(name.Trim().ToLower()) && p.CompanyId == companyId);
        }

        public double GetMaterialQuantityForOrderId(int warehouseId, int materialId, int? orderId, int? shelfId)
        {
            var query = warehouseEntities.CurrentStacksForWarehouseAdjusted_WithZTD.AsQueryable();
            //.Where(c => c.WarehouseId == warehouseId && c.MaterialId == materialId);
            if (orderId != null)
                query = query.Where(q => q.OrderId == orderId);
            if (shelfId != null)
                query = query.Where(q => q.ShelfId == shelfId);

            double? val = query.Sum(q => q.Quantity);

            return val ?? 0.0;
        }

        public IQueryable<MaterialRow> GetMaterials(string materialName, string indexSimple, int companyId) //, bool getOnStock = true, bool getOutOfStock = true)
        {
            var materialQuery = from m in warehouseEntities.Materials
                                join u in warehouseEntities.Units on m.Unit.Id equals u.Id
                                //let materialSimpleWithLeftouter = (from s in m.MaterialSIMPLE
                                //                                   select s
                                //                                   ).FirstOrDefault()
                                where m.IdSIMPLE.HasValue && m.CompanyId == companyId

                                        && (String.IsNullOrEmpty(materialName) || m.Name.ToLower().Contains(materialName.ToLower()))
                                        && ((String.IsNullOrEmpty(indexSimple) || (m.IndexSIMPLE != null && m.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower()))))
                                //|| (!String.IsNullOrEmpty(indexSimple) && m.IndexSIMPLE != null && m.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))
                                select
                                    new MaterialRow
                                    {
                                        Id = m.Id,
                                        MaterialId = m.Id,
                                        MaterialName = m.NameShort,
                                        IndexSIMPLE = m.IndexSIMPLE,
                                        NameSIMPLE = m.Name,
                                        UnitName = u.ShortName,
                                        ExtendedName = m.ExtendedName
                                    };

            return materialQuery;
        }

        public IQueryable<C_ImportSimple_MaterialFromSimple> GetMaterialsFromSIMPLE(string materialName, string indexSimple, int comapnyId)
        {
            return warehouseEntities.C_ImportSimple_MaterialFromSimple
                  .Where(x => String.IsNullOrEmpty(materialName) || x.NameSimple.ToLower().Contains(materialName.ToLower())
                      && (String.IsNullOrEmpty(indexSimple) || x.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower()))
                      && x.CompanyId == comapnyId);
        }

        public IQueryable<MaterialsToUseInProduction> GetMaterialsInProduction(int warehouseId, string orderNumber)
        {
            return warehouseEntities.MaterialsToUseInProductions
                .Where(x => x.WarehouseTargetId == warehouseId
                    && (String.IsNullOrEmpty(orderNumber) || x.OrderNumber.ToLower() == orderNumber.ToLower()));
        }

        public IQueryable<MaterialsToUseInProduction> GetMaterialsInProduction(int warehouseId, string orderNumber, PositionFeaturedStatus[] damageStatus)
        {
            List<MaterialsToUseInProduction> materialsToReturnList = new List<MaterialsToUseInProduction>();
            WarehouseDocumentPosition position;

            IQueryable<MaterialsToUseInProduction> materialsInProduction = GetMaterialsInProduction(warehouseId, orderNumber);
            foreach (MaterialsToUseInProduction materials in materialsInProduction)
            {
                position = GetPositionById(materials.PositionId);
                var positionFeaturedStatuses = GetPositionFeaturedStatusList(position);
                bool view = false;
                foreach (var status in damageStatus)
                {
                    if (positionFeaturedStatuses.Contains(status))
                    {
                        view = true;
                        break;
                    }
                }
                if (view)
                {
                    materialsToReturnList.Add(materials);
                }
            }

            return materialsToReturnList.AsQueryable();
        }

        public IQueryable<MaterialsToUseInProductionFromRw> GetMaterialsInProductionFromRw(int? warehouseId, string orderNumber)
        {
            return warehouseEntities.MaterialsToUseInProductionFromRws
                .Where(x => (!warehouseId.HasValue || x.WarehouseTargetId == warehouseId)
                    && (String.IsNullOrEmpty(orderNumber) || x.OrderNumber.ToLower() == orderNumber.ToLower()));
        }

        public IQueryable<MaterialsToUseInProductionFromRw> GetMaterialsInProductionFromRw(int? warehouseId, string orderNumber, PositionFeaturedStatus[] damageStatus)
        {
            List<MaterialsToUseInProductionFromRw> materialsToReturnList = new List<MaterialsToUseInProductionFromRw>();
            WarehouseDocumentPosition position;

            IQueryable<MaterialsToUseInProductionFromRw> materialsInProduction = GetMaterialsInProductionFromRw(warehouseId, orderNumber);
            foreach (MaterialsToUseInProductionFromRw materials in materialsInProduction)
            {
                position = GetPositionById(materials.PositionId);
                var positionFeaturedStatuses = GetPositionFeaturedStatusList(position);
                bool view = false;
                foreach (var status in damageStatus)
                {
                    if (positionFeaturedStatuses.Contains(status))
                    {
                        view = true;
                        break;
                    }
                }
                if (view)
                {
                    materialsToReturnList.Add(materials);
                }
            }

            return materialsToReturnList.AsQueryable();
        }

        public IQueryable<Material> GetMaterialsList(string filterName, int? companyId = null)
        {
            string filterNameNew = filterName != null ? filterName.Trim().ToLower() : null;
            return warehouseEntities.Materials
                .Where(x => !x.TimeDeleted.HasValue && (!companyId.HasValue || x.CompanyId == companyId)
                    && x.IdSIMPLE.HasValue
                    && (string.IsNullOrEmpty(filterNameNew) || x.Name.ToLower().Contains(filterNameNew) || x.IndexSIMPLE.ToLower().Contains(filterNameNew)));
        }

        public IQueryable<MaterialSimpleRow> GetMaterialsSimple(string materialName, int companyId) //, bool getOnStock = true, bool getOutOfStock = true)
        {
            var materialQuery = warehouseEntities.C_ImportSimple_MaterialFromSimple
                  .Where(x => !x.IndexSIMPLE.StartsWith("ż") && x.IdSimple.HasValue && x.CompanyId == companyId
                      && (String.IsNullOrEmpty(materialName) || (x.NameSimple.ToLower().Contains(materialName.ToLower()))
                      || x.IndexSIMPLE.ToLower().Contains(materialName.ToLower())))
                  .Select(x => new MaterialSimpleRow
                  {
                      Id = x.IdSimple,
                      SimpleId = x.IdSimple,
                      CompanyId = companyId,
                      MaterialName = x.NameSimple,
                      IndexSIMPLE = x.IndexSIMPLE,
                      NameSIMPLE = x.NameSimple,
                      UnitName = x.UnitName,
                  });

            return materialQuery;

            //var materialQuery = from m in warehouseEntities.MaterialFromSIMPLEs
            //                    //join u in warehouseEntities.Unit on m.Unit.Id equals u.Id
            //                    //let materialSimpleWithLeftouter = (from s in m.MaterialSIMPLE
            //                    //                                   select s
            //                    //                                   ).FirstOrDefault()
            //                    where !m.IndexSIMPLE.StartsWith("ż") && m.IdSimple.HasValue && m.CompanyId == companyId

            //                            && (String.IsNullOrEmpty(materialName) || (m.NameSimple.ToLower().Contains(materialName.ToLower())) || m.IndexSIMPLE.ToLower().Contains(materialName.ToLower()))
            //                    // && ((String.IsNullOrEmpty(materialName) || (m.IndexSIMPLE != null && m.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower()))))
            //                    //|| (!String.IsNullOrEmpty(indexSimple) && m.IndexSIMPLE != null && m.IndexSIMPLE.ToLower().Contains(indexSimple.ToLower())))
            //                    select
            //                        new MaterialSimpleRow
            //                        {
            //                            Id = m.IdSimple,
            //                            SimpleId = m.IdSimple,
            //                            CompanyId = companyId,
            //                            MaterialName = m.NameSimple,
            //                            IndexSIMPLE = m.IndexSIMPLE,
            //                            NameSIMPLE = m.NameSimple,
            //                            UnitName = m.UnitName,
            //                        };
        }

        public IQueryable<MaterialFromSIMPLEExtractedData> GetMaterialsSIMPLEList(string filterName, int? companyId)
        {
            if (filterName != null)
            {
                filterName = filterName.ToLower();
            }

            var materials = warehouseEntities.C_ImportSimple_MaterialFromSimple.OrderBy(p => p.NameSimple)
                .Where(x => x.CompanyId == companyId
                    && (string.IsNullOrEmpty(filterName) || x.NameSimple.ToLower().StartsWith(filterName) || x.IndexSIMPLE.ToLower().StartsWith(filterName)))
                .Select(x => new MaterialFromSIMPLEExtractedData
                                {
                                    IdSimple = x.IdSimple,
                                    IndexSimple = x.IndexSIMPLE,
                                    NameSimple = x.NameSimple
                                });

            //var materials = from material in warehouseEntities.MaterialFromSIMPLEs.OrderBy(p => p.NameSimple)
            //                where material.CompanyId == companyId
            //                    &&
            //                      (
            //                          string.IsNullOrEmpty(filterName)
            //                          ||
            //                          material.NameSimple.ToLower().StartsWith(filterName)
            //                          ||
            //                          material.IndexSIMPLE.ToLower().StartsWith(filterName)
            //                      )
            //                select
            //                    new MaterialFromSIMPLEExtractedData
            //                    {
            //                        IdSimple = material.IdSimple,
            //                        IndexSimple = material.IndexSIMPLE,

            //                        NameSimple = material.NameSimple
            //                    };

            return materials;

            //var testMat = test.ToArray();
            //IQueryable<MaterialFromSIMPLE> allMaterials = from material in warehouseEntities.MaterialFromSIMPLE
            //                                    where
            //                                    material.CompanyId == 1
            //                                    &&
            //                                    (string.IsNullOrEmpty(filterName)
            //                                    || material.NameSimple.ToLower().StartsWith(filterName.ToLower()))
            //                                    select material;

            //return allMaterials;
        }

        public IQueryable<MaterialsToExpendTotalFromWarehouse> GetMaterialsToExpendTotalFromWarehouse(int companyId, DateTime dateToFilter, string materialToFind, string indexSimpleToFind, int currentWarehouseId, string orderNumberToFind, bool forOrder = true)//, Action<InvokeServerMethodOperation> userCallback = null)
        {
#if DEBUG
            Contract.Requires(materialToFind != null);
            Contract.Requires(indexSimpleToFind != null);
            Contract.Requires(orderNumberToFind != null);
#endif
            string materialToFindQ = materialToFind.ToLower();
            string indexSimpleToFindQ = indexSimpleToFind.ToLower();
            string orderNumberToFindQ = orderNumberToFind.ToLower();

            DateTime dateToFilterUpperLimit = dateToFilter.AddDays(1);

            var materials = warehouseEntities.MaterialsToExpendTotalFromWarehouses
                .Where(m =>
                    (m.OrderId.HasValue == forOrder) //(!string.IsNullOrEmpty(m.OrderNumber) == forOrder)
                    && m.CompanyId == companyId
                    && (!m.StartDate.HasValue || m.StartDate <= dateToFilterUpperLimit)
                    && m.MaterialName.ToLower().Contains(materialToFindQ)
                    && m.IndexSIMPLE.ToLower().Contains(indexSimpleToFindQ)
                    && m.WarehouseTargetId == currentWarehouseId
                    && m.OrderNumber.Contains(orderNumberToFindQ));

            return materials;
        }

        public MaterialsToGiveBetweenCompany[] GetMaterialsToGiveToAnotherCompany(int warehouseId)
        {
            var materials =
                warehouseEntities.MaterialsToGiveBetweenCompanies.Where(p => p.SourceWarehouseId == warehouseId);
            return materials.ToArray();
        }

        public int GetNumberOfDocuments(WarehouseDocumentTypeEnum warehouseDocumentTypeEnum, int? warehouseId = null, int? year = null)
        {
            return
                warehouseEntities.WarehouseDocuments.Count(
                    wd =>
                        (!year.HasValue || wd.TimeStamp.Year == year.Value)
                        &&
                        (!warehouseId.HasValue || (wd.Warehouse != null && wd.Warehouse.Id == warehouseId.Value) || (wd.Warehouse1 != null && wd.Warehouse1.Id == warehouseId.Value))
                        &&
                        wd.WarehouseDocumentType.Id == (int)warehouseDocumentTypeEnum
                        );
        }

        public Material GetOrAddNewNewMaterial(int IdSim, int companyId, int userId)
        {
            var materialFromSimple = warehouseEntities.C_ImportSimple_MaterialFromSimple.First(s => s.IdSimple == IdSim && s.CompanyId == companyId);

            var material = Material.GetOrCreateWarehouseMaterial
              (
                materialFromSimple.NameSimple
                , IdSim
                , companyId

              );

            return material;
        }

        [Obsolete("Use GetOrAddNewNewMaterial(..) instead.")]
        public Material GetOrAddNewNewMaterialDataOnly(int IdSim, int companyId, int userId)
        {
            List<object> entities = new List<object>();

            var materialFromSimple = warehouseEntities.C_ImportSimple_MaterialFromSimple.First(s => s.IdSimple == IdSim && s.CompanyId == companyId);

            var material = Material.GetOrCreateWarehouseMaterial
              (
                materialFromSimple.NameSimple
                , IdSim
                , companyId

              );

            var materialDataOnly = new Material();

            materialDataOnly.Id = material.Id;
            materialDataOnly.Name = material.Name;
            materialDataOnly.IndexSIMPLE = material.IndexSIMPLE;
            materialDataOnly.IdSIMPLE = material.IdSIMPLE;
            materialDataOnly.CompanyId = material.CompanyId;

            return materialDataOnly;
        }

        public Order GetOrderByIdDataOnly(int orderId)
        {
            Order order;

            order = warehouseEntities.Orders.First(p => p.Id == orderId);

            Order orderDataOnly = new Order();
            orderDataOnly.Id = order.Id;
            orderDataOnly.OrderNumber = order.OrderNumber;

            return orderDataOnly;
        }

        public Order GetOrderByNumber(string orderNumber)
        {
            return warehouseEntities.Orders.First(p => p.OrderNumber == orderNumber);
        }

        public Order GetOrderByOrderDataOnly(Order order)
        {
#if DEBUG
            Contract.Requires(order != null);
#endif
            Order orderDataOnly = new Order()
            {
                Id = order.Id,
                ProcessHeaderId = order.ProcessHeaderId,
                ProcessIDH = order.ProcessIDH,
                ProcessIDPL = order.ProcessIDPL,
                ProcessIDDPL = order.ProcessIDDPL,
                OrderNumber = order.OrderNumber,
                OrderNumberPZ = order.OrderNumberPZ,
                ForDepartment = order.ForDepartment
            };
            orderDataOnly.Id = order.Id;
            //warehouseEntities.Detach(orderDataOnly);
            return orderDataOnly;
        }

        public OrderDelivery GetOrderDeliveryByOrderDataOnly(OrderDelivery orderDelivery)
        {
#if DEBUG
            Contract.Requires(orderDelivery != null);
#endif
            OrderDelivery orderDeliveryDataOnly = new OrderDelivery()
            {
                Id = orderDelivery.Id,
                ProcessHeaderId = orderDelivery.ProcessHeaderId,
                ProcessIDH = orderDelivery.ProcessIDH,
                ProcessIDPL = orderDelivery.ProcessIDPL,
                ProcessIDDPL = orderDelivery.ProcessIDDPL,
                //OrderNumber = orderDelivery.OrderNumber,
                OrderNumberFromPZ = orderDelivery.OrderNumberFromPZ,
                ForDepartment = orderDelivery.ForDepartment
            };
            orderDeliveryDataOnly.Id = orderDelivery.Id;
            //warehouseEntities.Detach(orderDeliveryDataOnly);
            return orderDeliveryDataOnly;
        }

        public OrderDeliveryNew GetOrderDeliveryByOrderDataOnlyNew(OrderDeliveryNew orderDelivery)
        {
#if DEBUG
            Contract.Requires(orderDelivery != null);
#endif

            OrderDeliveryNew orderDeliveryDataOnly = new OrderDeliveryNew()
            {
                Id = orderDelivery.Id,
                SupplyOrderPositionId = orderDelivery.SupplyOrderPositionId,
                //OrderNumber = orderDelivery.OrderNumber,
                SupplyOrderNumber = orderDelivery.SupplyOrderNumber,
                ForDepartment = orderDelivery.ForDepartment
            };
            orderDeliveryDataOnly.Id = orderDelivery.Id;
            //warehouseEntities.Detach(orderDeliveryDataOnly);
            return orderDeliveryDataOnly;
        }

        public IQueryable<WarehouseDocument> GetOrderedToInternalGiveDocument(int warehouseSourceId, int? warehouseDocumentId)
        {
            int zaEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            int mmMinusEnumInt = (int)WarehouseDocumentTypeEnum.MMMinus;

            var allOrders = from o in warehouseEntities.WarehouseDocuments
                            .Include("ProductionMachineScheduleItem.ProductionMachine.Department")
                            where
                            !o.TimeDeleted.HasValue &&
                                o.WarehouseDocumentType.Id == zaEnumInt
                                &&
                                o.Warehouse.Id == warehouseSourceId
                            select o;

            var allOrdersWithoutMMMinus = allOrders.Except(
                warehouseEntities.WarehouseDocumentRelations.Where(
                    mmps => mmps.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt &&
                            mmps.WarehouseDocument1.WarehouseDocumentType.Id == mmMinusEnumInt).Select(c => c.WarehouseDocument));

            return allOrdersWithoutMMMinus;
        }

        public IQueryable<WarehouseDocument> GetOrderedToInternalReceiveDocument(int warehouseTargetId, int? warehouseDocumentId)
        {
            int zwEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            int mmMinusEnumInt = (int)WarehouseDocumentTypeEnum.MMMinus;
            int mmPlusEnumInt = (int)WarehouseDocumentTypeEnum.MMPlus;

            var allOrdersWithMMMinuses = warehouseEntities.WarehouseDocumentRelations
                .Where(x => x.WarehouseDocument.WarehouseDocumentType.Id == zwEnumInt
                    && x.WarehouseDocument1.WarehouseDocumentType.Id == mmMinusEnumInt
                    && x.WarehouseDocument.Warehouse1.Id == warehouseTargetId)
                .Select(x => x.WarehouseDocument1);


            var allOrdersWithMMMinusesButWithoutMMPluses = allOrdersWithMMMinuses.Except(
                warehouseEntities.WarehouseDocumentRelations.Where(
                    mmps => mmps.WarehouseDocument.WarehouseDocumentType.Id == mmMinusEnumInt &&
                            mmps.WarehouseDocument1.WarehouseDocumentType.Id == mmPlusEnumInt).Select(c => c.WarehouseDocument));

            return allOrdersWithMMMinusesButWithoutMMPluses;
        }

        public IQueryable<OrderFromSIMPLE> GetOrdersFromSIMPLEList(string filterName, int companyId)
        {
            return warehouseEntities.OrderFromSIMPLEs
                .Where(x => x.CompanyId == companyId
                    && (string.IsNullOrEmpty(filterName)
                    || x.OrderNumberShort.ToLower().StartsWith(filterName.ToLower())))
                .OrderByDescending(x => x.OrderNumberShort);

            //IQueryable<OrderFromSIMPLE> allOrders = from order in warehouseEntities.OrderFromSIMPLEs
            //                                        where
            //                                            order.CompanyId == companyId &&
            //                                            (string.IsNullOrEmpty(filterName) ||
            //                                             order.OrderNumberShort.ToLower().StartsWith(filterName.ToLower()))
            //                                        orderby order.OrderNumberShort descending
            //                                        select order;
            //return allOrders;
        }

        public IQueryable<Order> GetOrdersList(string filterName, int? companyId = null)
        {
            return warehouseEntities.Orders
                .Include(x => x.Company)
                .Where(x => !x.IsFake
                    && (!companyId.HasValue || (companyId.HasValue && x.Company.Id == companyId.Value))
                    && (string.IsNullOrEmpty(filterName) || x.OrderNumber.ToLower().Contains(filterName.ToLower())));

            //IQueryable<Order> allOrders = from order in warehouseEntities.Orders.Include("Company")
            //                              where
            //                                    !order.IsFake
            //                                    && (!companyId.HasValue || (companyId.HasValue && order.Company.Id == companyId.Value))
            //                                    && (string.IsNullOrEmpty(filterName) || order.OrderNumber.ToLower().Contains(filterName.ToLower()))
            //                              select order;
            //return allOrders;
        }

        public IQueryable<DepartmentSIMPLEFiltered> GetDepartmentFilteredList(string filterName, int companyId)
        {
            return warehouseEntities.DepartmentSIMPLEFiltereds
                .Where(x => x.CompanyId == companyId).OrderBy(x => x.nazwa);
        }

        public int? GetOrSetUserRecentWarehouseSubTabInfo(int userId, int? warehouseSubTabId = null)
        {
            var systemUser = warehouseEntities.SystemUsers.Where(p => p.Id == userId).FirstOrDefault();

            if (warehouseSubTabId.HasValue)
            {
                systemUser.RecentWarehouseSubTabId = warehouseSubTabId;
                warehouseEntities.SaveChanges();
            }
            else
            {
                warehouseSubTabId = systemUser.RecentWarehouseSubTabId;
            }

            return warehouseSubTabId;
        }

        public int? GetOrSetUserRecentWarehouseTabInfo(int userId, int? warehouseTabId = null)
        {
            var systemUser = warehouseEntities.SystemUsers.Where(p => p.Id == userId).FirstOrDefault();

            if (warehouseTabId.HasValue)
            {
                systemUser.RecentWarehouseTabId = warehouseTabId;
                warehouseEntities.SaveChanges();
            }
            else
                warehouseTabId = systemUser.RecentWarehouseTabId;

            return warehouseTabId;
        }

        public PackageType GetPackageTypeByIdDataOnly(int ptId)
        {
            PackageType pt;

            pt = warehouseEntities.PackageTypes.Include("Unit").First(p => p.Id == ptId);

            PackageType ptDataOnly = new PackageType();
            ptDataOnly.Id = pt.Id;
            ptDataOnly.Name = pt.Name;
            ptDataOnly.Quantity = pt.Quantity;

            ptDataOnly.Unit = pt.Unit; // warehouseEntities.Unit.First(p => p.Id == pt.Unit.Id);

            return ptDataOnly;
        }

        public IQueryable<PackageTypeName> GetPackageTypeList(string filterName)
        {
            filterName = filterName != null ? filterName.ToLower() : null;
            return warehouseEntities.PackageTypeNames
                .Where(x => (string.IsNullOrEmpty(filterName) || x.Name.ToLower().StartsWith(filterName)));
        }

        public PackageTypeName GetPackageTypeNameById(int id)
        {
            return warehouseEntities.PackageTypeNames.Where(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<PackageTypeName> GetPackageTypeNameList(string filterName)
        {
            filterName = filterName != null ? filterName.ToLower() : null;
            return warehouseEntities.PackageTypeNames
                .Where(x => (string.IsNullOrEmpty(filterName) || x.Name.ToLower().StartsWith(filterName)));
        }

        public IQueryable<WarehouseDocumentPosition> GetParentPositionsOfPosition(WarehouseDocumentPosition position)
        {
            IQueryable<WarehouseDocumentPosition> positions =
                from relation in warehouseEntities.WarehouseDocumentPositionRelations
                  .Include("WarehouseDocumentPosition")
                  .Include("Warehouse")
                where relation.WarehouseDocumentPosition1.Id == position.Id
                select relation.WarehouseDocumentPosition;

            return positions;
        }

        public IQueryable<WarehouseDocumentPosition> GetParentPositionsOfPositionWithQuantityAndShelfs(PositionWithQuantityAndShelfs positionWithQuantityAndShelfs)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfs != null);
#endif
            List<WarehouseDocumentPosition> parents = new List<WarehouseDocumentPosition>();

            if (positionWithQuantityAndShelfs.positionParentIds != null)
            {
                foreach (ChildParentPositionQuantity parent in positionWithQuantityAndShelfs.positionParentIds)
                {
                    parents.Add(GetPositionById(parent.PositionParentID));
                }
            }

            //IQueryable<WarehouseDocumentPosition> positions =
            //    from position in warehouseEntities.WarehouseDocumentPosition.Include("Warehouse")
            //    where position.Id in (from parent in positionWithQuantityAndShelfs.positionParentIds select parent.PositionParentID)
            //    select position;
            return parents.AsQueryable();
        }

        public Person GetParentWarehouseDocumentAuthor(int warehouseDocumentId)
        {
            var parentWarehouseDocumentRelations = warehouseEntities.WarehouseDocumentRelations
                .Include(x => x.WarehouseDocument.WarehouseDocumentType)
                .Include(x => x.WarehouseDocument.Person)
                .Include(x => x.WarehouseDocument1)
                .Where(p => p.WarehouseDocument1.Id == warehouseDocumentId && p.WarehouseDocument.WarehouseDocumentType.Id == 10);

            if (parentWarehouseDocumentRelations.Count() > 0)
                return parentWarehouseDocumentRelations.FirstOrDefault().WarehouseDocument.Person;

            return null;
        }

        public DateTime? GetParentWarehouseDocumentDueTime(int warehouseDocumentId)
        {
            var parentWarehouseDocumentRelations = warehouseEntities.WarehouseDocumentRelations
                .Include(x => x.WarehouseDocument.WarehouseDocumentType)
                .Include(x => x.WarehouseDocument1)
                .Where(p => p.WarehouseDocument1.Id == warehouseDocumentId && p.WarehouseDocument.WarehouseDocumentType.Id == 10);

            if (parentWarehouseDocumentRelations.Count() > 0)
                return parentWarehouseDocumentRelations.FirstOrDefault().WarehouseDocument.DueTime;

            return null;
        }

        public DateTime? GetParentWarehouseDocumentTimeStamp(int warehouseDocumentId)
        {
            var parentWarehouseDocumentRelations = warehouseEntities.WarehouseDocumentRelations
                .Include(x => x.WarehouseDocument.WarehouseDocumentType)
                .Include(x => x.WarehouseDocument1)
                .Where(p => p.WarehouseDocument1.Id == warehouseDocumentId && p.WarehouseDocument.WarehouseDocumentType.Id == 10);

            if (parentWarehouseDocumentRelations.Count() > 0)
                return parentWarehouseDocumentRelations.FirstOrDefault().WarehouseDocument.TimeStamp;

            return null;
        }

        public Person GetPersonById(int personId)
        {
            return warehouseEntities.People.First(p => p.Id == personId);
        }

        public Person GetPersonForLoginCredentials(string login, string password)
        {
            SystemUser authUser =
                warehouseEntities.SystemUsers.Include("Person").Where(p => p.Login == login && p.Password == password).FirstOrDefault();
            if (authUser != null)
            {
                Person person = authUser.Person;

                return person;
            }
            return null;
        }

        public Person GetPersonForSystemUser(int? systemUserId)
        {
            if (systemUserId == null)
                return null;

            return warehouseEntities.SystemUsers.Include("Person").FirstOrDefault(u => u.Id == systemUserId).Person;
        }

        public Person GetPersonForSystemUser(string login)
        {
            if (string.IsNullOrEmpty(login))
                return null;

            return warehouseEntities.SystemUsers.Include("Person").FirstOrDefault(u => u.Login == login).Person;
        }

        public IQueryable<Person> GetPersonList(string filterName)
        {
            filterName = filterName != null ? filterName.ToLower() : null;

            return warehouseEntities.People
                .Where(x => (string.IsNullOrEmpty(filterName)
                    || x.FirstName.ToLower().StartsWith(filterName)
                    || x.LastName.ToLower().StartsWith(filterName)
                    || (x.FirstName + " " + x.LastName).ToLower().Contains(filterName)));


            //filterName = filterName != null ? filterName.ToLower() : null;
            //IQueryable<Person> allPersons = from person in warehouseEntities.People
            //                                where
            //                                    (string.IsNullOrEmpty(filterName) ||
            //                                     person.FirstName.ToLower().StartsWith(filterName) ||
            //                                     person.LastName.ToLower().StartsWith(filterName) ||
            //                                     (person.FirstName + " " + person.LastName).ToLower().Contains(filterName))
            //                                select person;
            //return allPersons;
        }

        public WarehouseDocumentPosition GetPositionByOrderId(int OrderId)
        {
            WarehouseDocumentPosition warehouseDocumentPosition =
                warehouseEntities.WarehouseDocumentPositions.FirstOrDefault(p => p.OrderId == OrderId);
            return warehouseDocumentPosition;
        }

        public WarehouseDocumentPosition GetPositionById(int warehouseDocumentPositionId)
        {

            WarehouseDocumentPosition warehouseDocumentPosition =
                warehouseEntities.WarehouseDocumentPositions.Include("Warehouse").First(p => p.Id == warehouseDocumentPositionId);

            //if (!warehouseDocumentPosition.Warehouse.IsLoaded)
            //{
            //    warehouseDocumentPosition.Warehouse.Load();
            //}

            return warehouseDocumentPosition;
        }

        public int GetPositionById()
        {
            throw new NotImplementedException();
        }

        public WarehouseDocumentPosition GetPositionByIdDataOnly(int warehouseDocumentPositionId)
        {
            WarehouseDocumentPosition warehouseDocumentPosition =
                warehouseEntities.WarehouseDocumentPositions.First(p => p.Id == warehouseDocumentPositionId);

            WarehouseDocumentPosition warehouseDocumentPositionDataOnly = new WarehouseDocumentPosition();
            warehouseDocumentPositionDataOnly.Id = warehouseDocumentPosition.Id;

            return warehouseDocumentPositionDataOnly;
        }

        public string GetPositionDamageWarehouseShortName(WarehouseDocumentPosition position)
        {
            List<List<PositionNode>> pathBunch = new List<List<PositionNode>>();
            GetPositionGenealogy(position, pathBunch, null, null);
            WarehouseDocumentPosition positionOfDamage = null;
            int minDepth = int.MaxValue;
            foreach (List<PositionNode> list in pathBunch)
            {
                int listCount = list.Count - 1;
                int actualDepth = 0;
                for (int i = listCount; i >= 0; i--)
                {
                    actualDepth = listCount - i;
                    //if (list[i].Position.Warehouse != null && actualDepth < minDepth)
                    if (GetPositionById(list[i].Position.Id).Warehouse != null && actualDepth < minDepth)
                    {
                        minDepth = actualDepth;
                        positionOfDamage = list[i].Position;
                    }
                }
            }

            if (positionOfDamage != null)
            {
                return positionOfDamage.Warehouse.ShortName;
            }
            return "????";
        }

        public IQueryable<WarehouseDocumentPositionExtension> GetPositionExtensions()
        {
            return warehouseEntities.WarehouseDocumentPositionExtensions.Include(x => x.WarehouseDocumentPosition);
        }

        public ArrayList GetPositionExtensionXmlDocumentsByPositionId(int positionId)
        {
            ArrayList xmls = new ArrayList();
            var documents = from e in GetPositionExtenstionsByPositionId(positionId)
                            select e.Info;

            foreach (string str in documents)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(str);
                xmls.Add(xmlDoc);
            }
            return xmls;
        }

        public IQueryable<WarehouseDocumentPositionExtension> GetPositionExtenstionsByPositionId(int positionId)
        {
            using (WarehouseEntities warehouseEntities = new WarehouseEntities())
            {
                return GetPositionExtensions().Where(x => x.WarehouseDocumentPosition.Id == positionId);
            }
        }

        /// <summary>
        /// Returns actual damage status of a position. The order of operations inside the method is important
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public PositionFeaturedStatus GetPositionFeaturedStatus(WarehouseDocumentPosition position)
        {
#if DEBUG
            Contract.Requires(position != null);
#endif

            if (PositionStatusIs(position, PositionFeaturedStatus.Package))
            {
                return PositionFeaturedStatus.Package;
            }
            if (PositionStatusIs(position, PositionFeaturedStatus.DamagedFromDamaged))
            {
                return PositionFeaturedStatus.DamagedFromDamaged;
            }
            if (PositionStatusIs(position, PositionFeaturedStatus.Damaged))
            {
                return PositionFeaturedStatus.Damaged;
            }
            return PositionFeaturedStatus.Good;
        }

        // The same as above, but serving multi-status
        public List<PositionFeaturedStatus> GetPositionFeaturedStatusList(WarehouseDocumentPosition position)
        {
#if DEBUG
            Contract.Requires(position != null);
#endif
            var statusList = new List<PositionFeaturedStatus>();

            bool damaged = false;

            if (PositionStatusIs(position, PositionFeaturedStatus.Package))
            {
                statusList.Add(PositionFeaturedStatus.Package);
            }
            if (PositionStatusIs(position, PositionFeaturedStatus.DamagedFromDamaged))
            {
                statusList.Add(PositionFeaturedStatus.DamagedFromDamaged);
                damaged = true;
            }
            if (PositionStatusIs(position, PositionFeaturedStatus.Damaged))
            {
                statusList.Add(PositionFeaturedStatus.Damaged);
                damaged = true;
            }

            if (PositionStatusIs(position, PositionFeaturedStatus.PartlyDamaged))
            {
                statusList.Add(PositionFeaturedStatus.PartlyDamaged);
                damaged = true;
            }

            if (PositionStatusIs(position, PositionFeaturedStatus.Garbage))
            {
                statusList.Add(PositionFeaturedStatus.Garbage);
            }

            if (!damaged)
            {
                statusList.Add(PositionFeaturedStatus.Good);
            }

            return statusList;
        }

        /// <summary>
        /// Gets a structured list of Position family with a bunch (List) of all possible graph paths in the family tree
        /// </summary>
        /// <param name="position">the child position</param>
        /// <param name="pathsBunch">an object, which collects all the family</param>
        /// <param name="actualList">actual list, processed in the branch. If passed null, then a new list will be created</param>
        /// <param name="recLevel">level of recursion nest. Set it zero</param>
        public void GetPositionGenealogy(WarehouseDocumentPosition position, List<List<PositionNode>> pathsBunch, List<PositionNode> actualList, int? recLevel)
        {
            var pathsBunchFromCache = GetPathsBunchFromCache(position);
            if (pathsBunchFromCache != null)
            {
                pathsBunch = pathsBunchFromCache;
            }
            else
            {
                if (actualList == null)
                {
                    actualList = new List<PositionNode>();
                    pathsBunch.Add(actualList);
                }

                if (recLevel == null)
                {
                    recLevel = 0;
                }

                // Collecting relations for the given position
                IQueryable<WarehouseDocumentPositionRelation> relations =
                    from relation in warehouseEntities.WarehouseDocumentPositionRelations
                      .Include("WarehouseDocumentPosition")
                    where relation.WarehouseDocumentPosition1.Id == position.Id
                    select relation;

                // Dealing with entity framework issue
                IQueryable<WarehouseDocumentPosition> refreshedPosition =
                    from pos in warehouseEntities.WarehouseDocumentPositions
                      .Include("Warehouse")
                    where pos.Id == position.Id
                    select pos;

                var positionNode = new PositionNode(refreshedPosition.FirstOrDefault());

                if (positionNode.Position == null)
                {
                    throw new Exception("Error in fetching WarehouseDocumentPosition object from database");
                }

                actualList.Add(positionNode);

                int relationsCount = relations.Count();
                int actualListCount = actualList.Count();

                // Rec step
                foreach (var relation in relations)
                {
                    List<PositionNode> newList;
                    if (relationsCount > 1)
                    {
                        // Path fork, so create a new list
                        newList = new List<PositionNode>();

                        // And copy all elements from old list to new list
                        for (int i = 0; i < actualListCount; i++)
                        {
                            var newNode = new PositionNode(actualList[i].Position);
                            newList.Add(newNode);
                        }

                        // And add the new list to the bunch
                        pathsBunch.Add(newList);

                        // Remove actual list
                        pathsBunch.Remove(actualList);
                    }
                    else
                    {
                        // No fork, just follow the actual list
                        newList = actualList;
                    }
                    int? recLevelNew = recLevel + 1;
                    GetPositionGenealogy(relation.WarehouseDocumentPosition, pathsBunch, newList, recLevelNew);
                }

                if (recLevel == 0)
                {
                    // Reverting, so we will have chronological order
                    int pathsBunchCount = pathsBunch.Count;
                    for (int i = 0; i < pathsBunchCount; i++)
                    {
                        List<PositionNode> positionPath = pathsBunch[i];
                        positionPath.Reverse();
                    }
                }
                StorePathBunchInCache(pathsBunch, position);
            }
        }

        public IQueryable<WarehouseDocumentPosition> GetPositionsForWarehouseDocument(int warehouseId)
        {
            return warehouseEntities.WarehouseDocumentPositions
                   .Include(wdp => wdp.WarehousePositionZones.Select(wpz => wpz.Zone))
                   .Include(wdp => wdp.Material)
                   .Include(wdp => wdp.Material.Unit)
                   .Where(wpd => wpd.WarehouseDocument.Id == warehouseId);
        }

        public List<string> GetPositionSpecialTypes(WarehouseDocumentPosition position)
        {
#if DEBUG
            Contract.Requires(position != null);
#endif
            ArrayList extDocs = GetPositionExtensionXmlDocumentsByPositionId(position.Id);

            List<string> specialTypes = new List<string>();

            foreach (XmlDocument doc in extDocs)
            {
                XmlNodeList nodes = doc.SelectNodes("/document/specialPositionType");

                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        specialTypes.Add(node.InnerText);
                    }
                }
            }

            return specialTypes;
        }

        public List<Zone> GetPositionZones(int positionId)
        {
            return warehouseEntities.WarehousePositionZones.Where(x => x.WarehouseDocumentPosition.Id == positionId).Select(x => x.Zone).ToList();
        }

        public int GetRandomShelfId(int[] excludes)
        {
            Random random = new Random();
            var shelfIds = warehouseEntities.Zones.Select(x => x.Id).ToArray();
            int randomShelfId = random.Next(shelfIds.Length - 1);

            return shelfIds[randomShelfId];
        }

        public IQueryable<SystemUserSecurityPrivilege> GetSecurityPrivilegesForUser(int userId)
        {
            return warehouseEntities.SystemUserSecurityPrivileges
              .Where(susp => susp.SystemUserId == userId);
        }

        public int GetSessionTimeForUserId(int systemUserId)
        {
            var sus = GetSystemUserSetting("SessionTime", systemUserId);

            if (sus != null)
            {
                return Convert.ToInt32(sus.SettingValue);
            }
            else
            {
                return 1440; // DEFAULT VALUE - TO BE READ FROM CONFIG!!!
            }
        }

        public Zone GetShelfByIdDataOnly(int id)
        {
            Zone zoneDataOnly = null;
            var zone = warehouseEntities.Zones.Where(x => x.Id == id).FirstOrDefault();
            if (zone != null)
                zoneDataOnly = new Zone() { Id = zone.Id, Name = zone.Name };

            return zoneDataOnly;
        }

        public Zone GetShelfByName(string shelfName)
        {
#if DEBUG
            Contract.Requires(shelfName != null);
#endif
            string shelfNameWithoutParents = (shelfName.Split('.').Last()).Replace("[", "").Replace("]", "");

            var zone = warehouseEntities.Zones.FirstOrDefault(z => z.Name == shelfNameWithoutParents);

            return zone;
        }

        [Obsolete("Use GetShelfByName(..) instead.")]
        public Zone GetShelfByNameDataOnly(string shelfName)
        {
#if DEBUG
            Contract.Requires(shelfName != null);
#endif
            string shelfNameWithoutParents = (shelfName.Split('.').Last()).Replace("[", "").Replace("]", "");

            Zone zone;
            Zone zoneDataOnly = null;

            var zones = from z in warehouseEntities.Zones
                        where z.Name == shelfNameWithoutParents
                        select z;

            zone = zones.FirstOrDefault();

            if (zone != null) zoneDataOnly = new Zone() { Id = zone.Id, Name = zone.Name };

            return zoneDataOnly;
        }

        public IQueryable<ZoneParentPath> GetShelfListFromCache(string filterName)
        {
            return this.GetActiveZonesWithParentsList()
              .Where
              (p =>
                    p.IsActive
                    &&
                (
                    string.IsNullOrEmpty(filterName)
                  || p.FullPath.Trim().ToLower().Contains(filterName.Trim().ToLower())
        )
              );//.ToList();
        }

        public List<WarehousePositionZone> GetShelfsForPostion(WarehouseDocumentPosition position)
        {
            List<WarehousePositionZone> warehousePositionZones;

            warehousePositionZones = new List<WarehousePositionZone>(warehouseEntities.WarehousePositionZones.Where(pz => pz.WarehouseDocumentPosition.Id == position.Id).ToArray());

            return warehousePositionZones;
        }

        public WarehousePositionZone GetShelfForPositionId(int positionId)
        {
            var shelf = warehouseEntities.WarehousePositionZones.Where(p => p.PositionId == positionId).Include(x => x.Zone).FirstOrDefault();

            return shelf;
        }

        public List<WarehousePositionZone> GetShelfsForPostionDataOnly(WarehouseDocumentPosition position)
        {
            List<WarehousePositionZone> warehousePositionZones;

            warehousePositionZones = new List<WarehousePositionZone>(warehouseEntities.WarehousePositionZones.Include("Zone").Where(pz => pz.WarehouseDocumentPosition.Id == position.Id).ToArray());

            List<WarehousePositionZone> warehousePositionZonesData = new List<WarehousePositionZone>();

            foreach (WarehousePositionZone wps in warehousePositionZones)
            {
                warehousePositionZonesData.Add(new WarehousePositionZone()
                {
                    WarehouseDocumentPosition = new WarehouseDocumentPosition() { Id = wps.WarehouseDocumentPosition.Id },
                    Zone = new Zone() { Id = wps.Zone.Id },
                    Quantity = wps.Quantity
                });
            }

            return warehousePositionZonesData;
        }

        public Warehouse GetSourceWarehouseByDocumentId(int documentId)
        {
            WarehouseDocument document;
            Warehouse warehouse = null;
            document = warehouseEntities.WarehouseDocuments.Include("Warehouse").FirstOrDefault(d => d.Id == documentId);
            if (document != null)
            {
                warehouse = warehouseEntities.Warehouses.FirstOrDefault(w => w.Id == document.Warehouse.Id);
            }
            return warehouse;
        }

        public Warehouse GetSourceWarehouseByPositionId(int positionId)
        {
            WarehouseDocumentPosition position;
            WarehouseDocument document;
            Warehouse warehouse = null;

            position = warehouseEntities.WarehouseDocumentPositions.FirstOrDefault(p => p.Id == positionId);

            if (position != null)
            {
                document = warehouseEntities.WarehouseDocuments.FirstOrDefault(d => d.Id == position.WarehouseDocument.Id);

                if (document != null)
                {
                    warehouse = warehouseEntities.Warehouses.FirstOrDefault(w => w.Id == document.Warehouse.Id);
                }
            }

            return warehouse;
        }

        public SystemGroup GetSystemGroupById(int systemGroupId)
        {
            SystemGroup systemGroup;

            systemGroup = warehouseEntities.SystemGroups.First(g => g.Id == systemGroupId);

            return systemGroup;
        }

        public SystemUser GetSystemUserById(int systemUserId)
        {
            SystemUser systemUser;

            systemUser = warehouseEntities.SystemUsers.First(u => u.Id == systemUserId);

            return systemUser;
        }

        public SystemUserGroupMembership GetSystemUserGroupMembership(int userId, int groupID)
        {
            return
                (from gm in warehouseEntities.SystemUserGroupMemberships
                 where
                 gm.SystemUser.Id == userId
                 &&
                 gm.SystemGroup.Id == groupID
                 select gm).FirstOrDefault();
        }

        public SystemUserGroupMembership GetSystemUserGroupMembership(int membershipId)
        {
            return
                (from gm in warehouseEntities.SystemUserGroupMemberships
                 where
                 gm.Id == membershipId
                 select gm).FirstOrDefault();
        }

        public SystemUserSetting GetSystemUserSetting(string key, int systemUserId)
        {
            var settings = from s in warehouseEntities.SystemUserSettings
                           where
                           s.SystemUser.Id == systemUserId
                           &&
                           s.SettingKey == key
                           select s;
            return settings.FirstOrDefault();
        }

        public IQueryable<SystemUser> GetSystemUsersForPerson(int personId)
        {
            var users = from u in warehouseEntities.SystemUsers.Include("Person")
                        where u.Person.Id == personId
                        select u;

            return users;
        }

        public IQueryable<WarehouseDocument> GetToInternalReceiveDocument(int warehouseTargetId, int? warehouseDocumentId)
        {
            int zwEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            int mmMinusEnumInt = (int)WarehouseDocumentTypeEnum.MMMinus;
            int mmPlusEnumInt = (int)WarehouseDocumentTypeEnum.MMPlus;

            int poMinusEnumInt = (int)WarehouseDocumentTypeEnum.POMinus;
            int poPlusEnumInt = (int)WarehouseDocumentTypeEnum.POPlus;

            var allOrdersWithMMMinuses = from o in warehouseEntities.WarehouseDocumentRelations
                                         where
                                             o.WarehouseDocument.WarehouseDocumentType.Id != zwEnumInt &&
                                             (o.WarehouseDocument1.WarehouseDocumentType.Id == mmMinusEnumInt || o.WarehouseDocument1.WarehouseDocumentType.Id == poMinusEnumInt) &&
                                             o.WarehouseDocument1.Warehouse1.Id == warehouseTargetId
                                         select o.WarehouseDocument1;

            var allOrdersWithMMMinusesButWithoutMMPluses = allOrdersWithMMMinuses.Except(
                warehouseEntities.WarehouseDocumentRelations.Where(
                    mmps => (mmps.WarehouseDocument.WarehouseDocumentType.Id == mmMinusEnumInt || mmps.WarehouseDocument.WarehouseDocumentType.Id == poMinusEnumInt) &&
                            (mmps.WarehouseDocument1.WarehouseDocumentType.Id == mmPlusEnumInt || mmps.WarehouseDocument1.WarehouseDocumentType.Id == poPlusEnumInt)).Select(c => c.WarehouseDocument));

            return allOrdersWithMMMinusesButWithoutMMPluses;
        }

        public IQueryable<WarehouseDocument> GetToReceiveMaterialReturnDocument(int warehouseTargetId, int? warehouseDocumentId)
        {
            int zwMinusEnumInt = (int)WarehouseDocumentTypeEnum.ZwMinus;
            int zwPlusEnumInt = (int)WarehouseDocumentTypeEnum.ZwPlus;

            var allOrdersWithMMMinuses = from d in warehouseEntities.WarehouseDocuments
                                         where
                                             d.WarehouseDocumentType.Id == zwMinusEnumInt &&
                                             d.Warehouse1.Id == warehouseTargetId
                                         select d;

            var allOrdersWithMMMinusesButWithoutMMPluses = allOrdersWithMMMinuses.Except(
                warehouseEntities.WarehouseDocumentRelations.Where(
                    mmps => mmps.WarehouseDocument.WarehouseDocumentType.Id == zwMinusEnumInt &&
                            mmps.WarehouseDocument1.WarehouseDocumentType.Id == zwPlusEnumInt).Select(c => c.WarehouseDocument));

            return allOrdersWithMMMinusesButWithoutMMPluses;
        }


        public IEnumerable<BaseWarehouseDocumentPositionLayer> GetUngivenOrderedPositions(int warehouseId, int? orderId = null, int? materialId=null)
        {
            int zaEnumInt = (int)WarehouseDocumentTypeEnum.Za;

            var positions =
                warehouseEntities.WarehouseDocumentPositions
                    .Include(x => x.WarehousePositionZones)
                    .Include(x => x.InternalGiveCancelled)
                    .Include(x => x.WarehouseDocumentPositionRelations)
                    .Where(q =>
                        !q.WarehouseDocument.TimeDeleted.HasValue &&
                        q.WarehouseDocument.Warehouse.Id == warehouseId
                        && (q.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt)//||q.WarehouseDocument.WarehouseDocumentType.Id==32) // zmiana lokalizacji
                        && ((q.WarehouseDocumentPositionRelations.Any() && q.WarehouseDocumentPositionQuantityChanges.Any()) 
                        || !q.WarehouseDocumentPositionRelations.Any()
                        )
                        && (!orderId.HasValue || q.Order.Id == orderId)
                        && (!materialId.HasValue || q.MaterialId == materialId)
                        && !q.InternalGiveCancelled.Any()
                         ).OrderBy(p => p.WarehouseDocument.DueTime)
                         .Select(q => new BaseWarehouseDocumentPositionLayer()
                         {
                             ShelfId = q.WarehousePositionZones.Select(z => z.Zone.Id).FirstOrDefault(),
                             IDpl = q.ProcessIdpl,
                             IDDpl = q.ProcessIddpl,
                             Id = q.Id,
                             MaterialId = q.MaterialId,
                             Order = q.Order,
                             WarehouseDocumentDueTime = q.WarehouseDocument.DueTime,
                             Quantity = q.Quantity,
                             GivenQuantity = !q.WarehouseDocumentPositionRelations.Any() ? 0 : q.WarehouseDocumentPositionRelations.Sum(r => Math.Abs(r.Quantity))
                         });
#if DEBUG
            var test = ((System.Data.Objects.ObjectQuery)positions).ToTraceString();
#endif
            return positions;
        }

        public IEnumerable<WarehouseDocumentPositionLayer> GetUngivenOrderedDocumentPositions(int warehouseId, DateTime dateAndHour, int? orderId = null)
        {
            int zaEnumInt = (int)WarehouseDocumentTypeEnum.Za;
           // int zlEnumInt = (int)WarehouseDocumentTypeEnum.ZL;
           // DateTime nextDateAndHour = dateAndHour.AddMinutes(30);

            var positions =
                warehouseEntities.WarehouseDocumentPositions
                .Include(x => x.WarehouseDocument.Warehouse1)
                .Include(x=>x.WarehouseDocument.WarehouseDocumentRelations)
                .Include(x => x.InternalGiveCancelled)
                    .Where(q =>
                        !q.WarehouseDocument.TimeDeleted.HasValue
                        && q.WarehouseDocument.DueTime == dateAndHour //|| q.WarehouseDocument.DueTime == nextDateAndHour // zamówienie na niepełną godzine..!!
                        && q.WarehouseDocument.Warehouse.Id == warehouseId
                        && q.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt
                        //&& q.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt || q.WarehouseDocument.WarehouseDocumentType.Id == zlEnumInt //+zmiana lokalizacji..
                        && (!orderId.HasValue || q.Order.Id == orderId)
                        && (q.Order.Id == orderId)
                        && (!q.InternalGiveCancelled.Any())
                         ).OrderBy(p => p.WarehouseDocument.DueTime)
                         .Select(q => new WarehouseDocumentPositionLayer()
                         {
                             Id = q.Id,
                             //DocType = q.WarehouseDocument.WarehouseDocumentType.Id,
                             WarehouseDocumentId = q.WarehouseDocument.Id,
                             //WarehouseDocumentParentId =  q.WarehouseDocument.WarehouseDocumentType.Id == zlEnumInt
                             //   ?   q.WarehouseDocument.WarehouseDocumentRelations1.Select(x=>x.ParentId).FirstOrDefault()
                             //   :   q.WarehouseDocument.Id,
                             OrderId = q.Order.Id,
                             Material = q.Material,
                             MaterialShortName = q.Material.Unit.ShortName,
                             TargetWarehouse = q.WarehouseDocument.Warehouse1.Name,
                             DepartmentName = q.Department != null ? q.Department.Name : "",
                             TargetProductionMachineName = q.ProductionMachine != null ? q.ProductionMachine.Name : "",
                             ProcessIdpl = q.ProcessIdpl,
                             WarehouseDocumentTimeStamp = q.WarehouseDocument.TimeStamp,
                             //WarehouseDocumentTimeStamp = q.WarehouseDocument.WarehouseDocumentType.Id == zlEnumInt
                             //   ? warehouseEntities.WarehouseDocuments.Where(x=>x.Id == (q.WarehouseDocument.WarehouseDocumentRelations1.Select(z=>z.ParentId).FirstOrDefault())).Select(x=>x.TimeStamp).FirstOrDefault()
                             //   : q.WarehouseDocument.TimeStamp,
                             WarehouseDocumentDueTime = q.WarehouseDocument.DueTime,
                             Quantity = q.Quantity,
                             Warehouse = q.WarehouseDocument.Warehouse,
                             GivenQuantity = q.WarehouseDocumentPositionRelations.Any() ? q.WarehouseDocumentPositionRelations.Sum(r => r.Quantity) : 0,
                         });
            return positions;
        }
        //public IEnumerable<WarehouseDocumentPosition> GetUngivenOrderedPositions_old(int warehouseId, int? orderId = null, DateTime? dateAndHour = null)
        //{
        //  int zaEnumInt = (int)WarehouseDocumentTypeEnum.Za;
        //  //int mmMinusEnumInt = (int)WarehouseDocumentTypeEnum.MMMinus;
        //  var positions = warehouseEntities.WarehouseDocumentPositions
        //                      .Include(x => x.WarehouseDocument.WarehouseDocumentType)
        //                      .Include(x => x.WarehouseDocument.Warehouse)
        //                      .Include(x => x.WarehouseDocument.Warehouse1)
        //                      .Include(x => x.Order)
        //                      .Include(x => x.Department)
        //                      .Include(x => x.ProductionMachine)
        //                      .Include(x => x.WarehousePositionZones.Select(y => y.Zone))
        //                      .Include(x => x.Material)
        //                      .Include(x => x.Material.Unit)
        //                      .Include(x => x.Material.Material1)//???ChildMaterials
        //                      .Include(x => x.WarehouseDocumentPositionQuantityChanges.Select(y => y.QuestionAnswer1))
        //                      .Include(x => x.WarehouseDocumentPositionQuantityChanges.Select(y => y.QuestionAnswer))
        //                      .Where(q =>
        //                          !q.WarehouseDocument.TimeDeleted.HasValue &&
        //                          q.WarehouseDocument.Warehouse.Id == warehouseId
        //                          && q.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt
        //                          && !q.WarehouseDocumentPositionRelations.Any()
        //                          && (!orderId.HasValue || q.Order.Id == orderId)
        //                          ).OrderBy(p => p.WarehouseDocument.DueTime).Select(x => x);

        //  //var positions = from p in warehouseEntities.WarehouseDocumentPositions
        //  //                    .Include("WarehouseDocument.WarehouseDocumentType")
        //  //                    .Include("WarehouseDocument.Warehouse")
        //  //                    .Include("WarehouseDocument.Warehouse1")
        //  //                  //.Include("WarehouseDocument.ProductionMachineScheduleItem.ProductionMachine.Department")
        //  //                    .Include("Order")
        //  //                    .Include("Department")
        //  //                    .Include("ProductionMachine")
        //  //                    .Include("WarehousePositionZone.Zone")
        //  //                    .Include("Material")
        //  //                    .Include("Material.Unit")
        //  //                    .Include("Material.ChildMaterials")
        //  //                    .Include("WarehouseDocumentPositionQuantityChanges.ConfirmingAnswer")
        //  //                    .Include("WarehouseDocumentPositionQuantityChanges.CancellingAnswer")
        //  //                    .Where(q =>
        //  //                        !q.WarehouseDocument.TimeDeleted.HasValue &&
        //  //                        q.WarehouseDocument.Warehouse.Id == warehouseId
        //  //                        && q.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt
        //  //                        && !q.WarehouseDocumentPositionRelations.Any()
        //  //                        && (!orderId.HasValue || q.Order.Id == orderId)
        //  //                        ).OrderBy(p => p.WarehouseDocument.DueTime)

        //  //where (
        //  //        !dateAndHour.HasValue
        //  //        ||
        //  //        (
        //  //            p.WarehouseDocument.TimeStamp.Year == dateAndHour.Value.Year
        //  //            &&
        //  //            p.WarehouseDocument.TimeStamp.DayOfYear == dateAndHour.Value.DayOfYear
        //  //            &&
        //  //            p.WarehouseDocument.TimeStamp.Hour == dateAndHour.Value.Hour
        //  //        )
        //  //    )

        //  //where
        //  //    p.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt
        //  //    //&& p.WarehouseDocument.Id == warehouseId
        //  //select p;

        //  // TODO: Convert to nicer LINQ form!!!
        //  List<WarehouseDocumentPosition> positionsFiltered = new List<WarehouseDocumentPosition>();
        //  foreach (var position in positions)
        //  {
        //    if (!dateAndHour.HasValue
        //        ||
        //        (
        //            position.WarehouseDocument.DueTime.HasValue
        //            &&
        //            position.WarehouseDocument.DueTime.Value.Year == dateAndHour.Value.Year
        //            &&
        //            position.WarehouseDocument.DueTime.Value.DayOfYear == dateAndHour.Value.DayOfYear
        //            &&
        //            position.WarehouseDocument.DueTime.Value.Hour == dateAndHour.Value.Hour
        //         )
        //    )
        //    {
        //      positionsFiltered.Add(position);
        //    }
        //  }

        //  return positionsFiltered;
        //}

        public List<GivingOrderAndHourHeader> GetUngivenOrderedPositionsHeaders(int warehouseId)
        {
            //var positions = GetUngivenOrderedPositions(warehouseId).ToArray();
            var positions = GetUngivenOrderedPositions(warehouseId);
            var AvaiblePosiions = positions.Where(x => Math.Abs(x.Quantity) - x.GivenQuantity > 0);

            var headers = AvaiblePosiions.GroupBy(p => new
            {
                Order = p.Order,
                DateAndHour = new DateTime(
                          p.WarehouseDocumentDueTime.Value.Year,
                          p.WarehouseDocumentDueTime.Value.Month,
                          p.WarehouseDocumentDueTime.Value.Day,
                          p.WarehouseDocumentDueTime.Value.Hour,
                          p.WarehouseDocumentDueTime.Value.Minute, 0)  // podział na niepełne godziny!!!

                //p.WarehouseDocumentDueTime.Value.Hour, 0, 0) // podział na pełne godziny, nie jest widoczne dla niepełnych godzin!
                //}).Select(p => new GivingOrderAndHourHeader(p.Key.Order, p.Key.DateAndHour, 0));
            }).Select(p => new GivingOrderAndHourHeader(p.Key.Order, p.Key.DateAndHour, 0));


            // zliczanie ilośći pozycji - > pomijamy testowo od 28.07.2015? 

            //foreach (GivingOrderAndHourHeader header in headers)
            //{
            //    header.NumberOfPositions =
            //        positions.Count(
            //            p =>
            //            p.Order.Id == header.Order.Id &&
            //            p.WarehouseDocumentDueTime.Value.Year == header.DateAndHour.Year &&
            //            p.WarehouseDocumentDueTime.Value.DayOfYear == header.DateAndHour.DayOfYear);
            //}

            return headers.ToList();
        }

        //public List<GivingOrderAndHourHeader> GetUngivenOrderedPositionsHeaders(int warehouseId)
        //{
        //  var positions = GetUngivenOrderedPositions(warehouseId).ToArray();
        //  var headers = from p in positions

        //                group p by
        //                    new
        //                    {
        //                      Order = p.Order,
        //                      DateAndHour = new DateTime(
        //              p.WarehouseDocument.DueTime.Value.Year,
        //              p.WarehouseDocument.DueTime.Value.Month,
        //              p.WarehouseDocument.DueTime.Value.Day,
        //              p.WarehouseDocument.DueTime.Value.Hour,
        //              0, 0)
        //                    }

        //                  into q
        //                  select new GivingOrderAndHourHeader(q.Key.Order, q.Key.DateAndHour, 0);

        //  foreach (GivingOrderAndHourHeader header in headers)
        //  {
        //    header.NumberOfPositions =
        //        positions.Count(
        //            p =>
        //            p.Order.Id == header.Order.Id &&
        //            p.WarehouseDocument.DueTime.Value.Year == header.DateAndHour.Year &&
        //            p.WarehouseDocument.DueTime.Value.DayOfYear == header.DateAndHour.DayOfYear);
        //  }
        //  return headers.ToList();
        //}

        public Unit GetUnitById(int unitId)
        {
            Unit unit = warehouseEntities.Units.First(p => p.Id == unitId);
            return unit;
        }

        public IQueryable<Unit> GetUnitList(string filterName)
        {
            filterName = filterName != null ? filterName.ToLower() : null;
            IQueryable<Unit> allUnits = from unit in warehouseEntities.Units
                                        where
                                            (string.IsNullOrEmpty(filterName) ||
                                             unit.Name.ToLower().StartsWith(filterName))
                                        select unit;
            return allUnits;
        }

        public SystemUser GetUserByLogin(string login)
        {
            return warehouseEntities.SystemUsers.First(p => p.Login == login);
        }

        public int? GetUserRecentWarehouseInfo(int userId)
        {
            var systemUser = warehouseEntities.SystemUsers.FirstOrDefault(p => p.Id == userId);
            return systemUser.RecentWarehouseId;
        }

        public WaitingForSupply[] GetWaitingForSupply
        (
          string materialToSerach,
          string contractingPartyNameToSearch,
          DateTime? startDate,
          DateTime? endDate,
          int companyId,
          string quantity,
          string orderNumber,
          string orderNumberPZ,
          int quantityTolerance,
          int? targetDeptSimpleId = null)
        {
#if DEBUG
            Contract.Requires(orderNumber != null);
#endif
            decimal? quantityNum = 0;
            decimal? quantityNumLess = 0;
            decimal? quantityNumMore = 0;
            if (!String.IsNullOrEmpty(quantity))
            {
                try
                {
                    string qstr = quantity.Replace(".", ",");

                    quantityNum = Decimal.Parse(qstr);

                    decimal? quantityToleranceAbs = (Convert.ToDecimal(quantityTolerance) / 100) * quantityNum;

                    quantityNumLess = quantityNum - quantityToleranceAbs;
                    quantityNumMore = quantityNum + quantityToleranceAbs;
                }
                catch (Exception)
                {
                    throw new Exception("Incorrect quantity format");
                }
            }

            string orderNumberLowered = orderNumber.ToLower();

            //var supplyQueryTest = from s in warehouseEntities.WaitingForSupply
            //                  where s.IloscZam > 0
            //                        && (String.IsNullOrEmpty(orderNumber) || s.NRp.ToLower().Contains(orderNumberLowered) || s.SimpleDepartmentName.ToLower().Contains(orderNumberLowered))
            //                        //&& (String.IsNullOrEmpty(orderNumberPZ) || s.NazwSK.ToLower().Contains(orderNumberPZ.ToLower()))
            //                        && s.Firma == companyId
            //                        && (((startDate == null || s.SourceType == 3) || s.DataKL >= startDate) && (endDate == null || s.DataKL <= endDate))
            //                  select s;
            //var supplyQueryTestMaterialized = supplyQueryTest.ToArray();

            var supplyQuery = warehouseEntities.WaitingForSupplies
              .Where(s =>
                s.IloscZam > 0
                && (string.IsNullOrEmpty(materialToSerach) || s.MaterialName.ToLower().Contains(materialToSerach.ToLower()))
                && (string.IsNullOrEmpty(contractingPartyNameToSearch) || s.ContractingPartyName.ToLower().Contains(contractingPartyNameToSearch.ToLower()))
                && (quantityNum == 0 || (s.IloscZam >= quantityNumLess && s.IloscZam <= quantityNumMore))
                && (string.IsNullOrEmpty(orderNumber) || s.NRp.ToLower().Contains(orderNumberLowered) || s.SimpleDepartmentName.ToLower().Contains(orderNumberLowered))
                && (string.IsNullOrEmpty(orderNumberPZ) || s.NazwSK.ToLower().Contains(orderNumberPZ.ToLower()))
                && s.Firma == companyId
                && (((startDate == null || s.SourceType == 3) || s.DataKL >= startDate) && (endDate == null || s.DataKL <= endDate))
                && (!targetDeptSimpleId.HasValue || targetDeptSimpleId.Value <= 0 || s.Dzial == targetDeptSimpleId));

            //var supplyQuery = from s in warehouseEntities.WaitingForSupplies
            //                  where s.IloscZam > 0

            //                        && (String.IsNullOrEmpty(materialToSerach) || s.MaterialName.ToLower().Contains(materialToSerach.ToLower()))
            //                        && (String.IsNullOrEmpty(contractingPartyNameToSearch) || s.ContractingPartyName.ToLower().Contains(contractingPartyNameToSearch.ToLower()))
            //                        && (quantityNum == 0 || (s.IloscZam >= quantityNumLess && s.IloscZam <= quantityNumMore))
            //                        && (String.IsNullOrEmpty(orderNumber) || s.NRp.ToLower().Contains(orderNumberLowered) || s.SimpleDepartmentName.ToLower().Contains(orderNumberLowered))
            //                        && (String.IsNullOrEmpty(orderNumberPZ) || s.NazwSK.ToLower().Contains(orderNumberPZ.ToLower()))
            //                        && s.Firma == companyId
            //                    //&& ((startDate == null || s.SourceType == 3) || s.DataKL >= startDate) && (endDate == null || s.DataKL <= endDate)
            //                        && (((startDate == null || s.SourceType == 3) || s.DataKL >= startDate) && (endDate == null || s.DataKL <= endDate))

            //                        && (!targetDeptSimpleId.HasValue || targetDeptSimpleId.Value <= 0 || s.Dzial == targetDeptSimpleId)

            //                  select s;

            var materialToSupply = supplyQuery.ToArray().OrderBy(s => s.DataKL.ToString() + s.ContractingPartyName + s.NRp + s.MaterialName).ToArray();

            foreach (WaitingForSupply waitingForSupply in materialToSupply)
            {
                if (waitingForSupply.Flaga == 1)
                {
                    waitingForSupply.CorrectOrderNumber = waitingForSupply.NRp;
                }
                else
                {
                    waitingForSupply.CorrectOrderNumber = "";
                }
                waitingForSupply.CorrectKontrahentId = warehouseEntities.GetSimpleKontrahentIdForDostawcaIdAndCompanyId(waitingForSupply.kontrachent_id, companyId).FirstOrDefault() ?? 0;
            }

            return materialToSupply;
        }

        public WaitingForSupplyNew[] GetWaitingForSupplyNew(string materialToSerach, string contractingPartyNameToSearch, DateTime? startDate, DateTime? endDate, int companyId, string quantity, string orderNumber, string orderNumberPZ, int quantityTolerance)
        {
            decimal? quantityNum = 0;

            decimal? quantityNumLess = 0;
            decimal? quantityNumMore = 0;
            //string quantityStr;
            if (!String.IsNullOrEmpty(quantity))
            {
                try
                {
                    //var q = double.Parse(quantity.Replace(",", "."));

                    string qstr = quantity.Replace(".", ",");

                    quantityNum = Decimal.Parse(qstr);

                    decimal? quantityToleranceAbs = (Convert.ToDecimal(quantityTolerance) / 100) * quantityNum;

                    quantityNumLess = quantityNum - quantityToleranceAbs;
                    quantityNumMore = quantityNum + quantityToleranceAbs;
                }
                catch (Exception)
                {
                    throw new Exception("Incorrect quantity format");
                }
            }

            var supplyQuery = from s in warehouseEntities.WaitingForSupplyNews
                              where s.QuantityToReceive > 0 &&
                                    (String.IsNullOrEmpty(materialToSerach) || s.MaterialName.ToLower().Contains(materialToSerach.ToLower())) &&
                                    (String.IsNullOrEmpty(contractingPartyNameToSearch) || s.ContractingPartyName.ToLower().Contains(contractingPartyNameToSearch.ToLower())) &&
                                    (quantityNum == 0 || (s.QuantityToReceive >= (double)quantityNumLess && s.QuantityToReceive <= (double)quantityNumMore)) &&
                                    (String.IsNullOrEmpty(orderNumber) || s.OrderNumber.ToLower().Contains(orderNumber.ToLower())) &&
                                    (String.IsNullOrEmpty(orderNumberPZ) || s.SupplyOrderNumber.ToLower().Contains(orderNumberPZ.ToLower())) &&
                                    s.CompanyId == companyId &&
                                    (startDate == null || s.TimeOrderExpected >= startDate) && (endDate == null || s.TimeOrderExpected <= endDate)
                              select s;

            var materialToSupply = supplyQuery.ToArray().OrderBy(s => s.TimeOrderExpected.ToString() + s.ContractingPartyName + s.OrderNumber + s.MaterialName).ToArray();

            //foreach (WaitingForSupplyNew waitingForSupply in materialToSupply)
            //{
            //    if (waitingForSupply.DepartmentId.HasValue)
            //    {
            //        waitingForSupply.CorrectOrderNumber = waitingForSupply.NRp;
            //    }
            //    else
            //    {
            //        waitingForSupply.CorrectOrderNumber = "";
            //    }
            //    FakeTable fakeTable = warehouseEntities.GetSimpleKontrahentIdForDostawcaIdAndCompanyId(waitingForSupply.kontrachent_id, companyId).FirstOrDefault();

            //    if (fakeTable != null)
            //    {
            //        int correct_kontrahent_id = fakeTable.Id;
            //        waitingForSupply.CorrectKontrahentId = correct_kontrahent_id;
            //    }
            //    else
            //    {
            //        waitingForSupply.CorrectKontrahentId = 0;
            //    }
            //}

            return materialToSupply;
        }

        public WaitingForSupplyWithZeros1[] GetWaitingForSupplyWithZeros(string materialToSerach, string contractingPartyNameToSearch, DateTime? startDate, DateTime? endDate, int companyId, string quantity, string orderNumber, string orderNumberPZ, int quantityTolerance, bool includeArrived = true, bool includeNotArrived = true)
        {
            decimal? quantityNum = 0;

            decimal? quantityNumLess = 0;
            decimal? quantityNumMore = 0;
            //string quantityStr;
            if (!String.IsNullOrEmpty(quantity))
            {
                try
                {
                    quantityNum = decimal.Parse(quantity);

                    decimal? quantityToleranceAbs = (Convert.ToDecimal(quantityTolerance) / 100) * quantityNum;

                    quantityNumLess = quantityNum - quantityToleranceAbs;
                    quantityNumMore = quantityNum + quantityToleranceAbs;
                }
                catch (Exception)
                {
                    throw new Exception("Incorrect quantity format");
                }
            }

            var supplyQuery = from s in warehouseEntities.WaitingForSupplyWithZeros1
                              where (s.IloscZam > 0 || !string.IsNullOrEmpty(s.SimpleDocId)) &&
                                    (String.IsNullOrEmpty(materialToSerach) || s.MaterialName.ToLower().Contains(materialToSerach.ToLower())) &&
                                    (String.IsNullOrEmpty(contractingPartyNameToSearch) || s.ContractingPartyName.ToLower().Contains(contractingPartyNameToSearch.ToLower())) &&
                                    (quantityNum == 0 || (s.IloscZam >= quantityNumLess && s.IloscZam <= quantityNumMore)) &&
                                    (String.IsNullOrEmpty(orderNumber) || s.NRp.ToLower().Contains(orderNumber.ToLower())) &&
                                    (String.IsNullOrEmpty(orderNumberPZ) || s.NazwSK.ToLower().Contains(orderNumberPZ.ToLower())) &&
                                    s.Firma == companyId &&
                                    (startDate == null || s.DataKL >= startDate) && (endDate == null || s.DataKL <= endDate)
                              //&& ((includeArrived && !string.IsNullOrEmpty(s.SimpleDocId))) // || (includeNotArrived && string.IsNullOrEmpty(s.SimpleDocId)))
                              select s;

            if (!includeArrived)
            {
                supplyQuery = supplyQuery.Where(s => string.IsNullOrEmpty(s.SimpleDocId));
            }
            if (!includeNotArrived)
            {
                supplyQuery = supplyQuery.Where(s => !string.IsNullOrEmpty(s.SimpleDocId));
            }

            var materialToSupply = supplyQuery.ToArray().OrderBy(s => s.DataKL.ToString() + s.ContractingPartyName + s.NRp + s.MaterialName).ToArray();

            foreach (WaitingForSupplyWithZeros1 waitingForSupply in materialToSupply)
            {
                if (waitingForSupply.Flaga == 1)
                {
                    waitingForSupply.CorrectOrderNumber = waitingForSupply.NRp;
                }
                else
                {
                    waitingForSupply.CorrectOrderNumber = "";
                }
                waitingForSupply.CorrectKontrahentId = warehouseEntities.GetSimpleKontrahentIdForDostawcaIdAndCompanyId(waitingForSupply.kontrachent_id, companyId).FirstOrDefault() ?? 0;
            }

            return materialToSupply;
        }

        public Warehouse GetWarehouseById(int warehouseId)
        {
            Warehouse warehouse;

            //warehouse = warehouseEntities.Warehouse.Include("WarehouseType").First(w => w.Id == warehouseId);
            warehouse = GetAllWarehouses().FirstOrDefault(w => w.Id == warehouseId);

            return warehouse;
        }

        public Warehouse GetWarehouseByIdIB(int warehouseId)
        {
            Warehouse warehouse;

            //warehouse = warehouseEntities.Warehouse.Include("WarehouseType").First(w => w.Id == warehouseId);
            warehouse = warehouseEntities.Warehouses
              .Include(w => w.WarehouseType)
                //.AddIncludePaths("WarehouseType")
              .FirstOrDefault(w => w.Id == warehouseId);

            return warehouse;
        }

        //    IQueryable<Person> filteredPersons = GetPersonList(filtr);
        public WarehouseDocument GetWarehouseDocumentById(int warehouseDocumentId)
        {


            WarehouseDocument warehouseDocument;
            warehouseDocument = warehouseEntities.WarehouseDocuments
                .Include(x => x.Warehouse.Company)
                .Include(x => x.Warehouse1.Company)
                .Include(x => x.Person)
                .Include(x => x.WarehouseDocumentType)
              .Include(x => x.ContractingPartyForWarehouseDocuments.Select(y => y.ContractingParty))
                .Include(x => x.WarehouseDocumentType.WarehouseDataExchangeDocTypeMapSIMPLEOverrides1.Select(y => y.WarehouseDocumentType1))
                .First(x => !x.TimeDeleted.HasValue && x.Id == warehouseDocumentId);




            // must include WarehouseDataExchangeDocTypeMapSIMPLEOverridesBefore


            //WarehouseDocument warehouseDocument2 =
            //    warehouseEntities.WarehouseDocuments
            //    .Include("Warehouse.Company")
            //    .Include("Warehouse1.Company")
            //    .Include("Person")
            //    .Include("WarehouseDocumentType")
            //    .Include("ContractingPartyForWarehouseDocument.ContractingParty")
            //    .Include("WarehouseDocumentType.WarehouseDataExchangeDocTypeMapSIMPLEOverridesBefore.WarehouseDocumentTypeAfter")
            //    .First(
            //        d => !d.TimeDeleted.HasValue && d.Id == warehouseDocumentId);

            return warehouseDocument;
        }

        public WarehouseDocument GetWarehouseDocumentForPositionId(int warehouseDocumenPositiontId)
        {
            WarehouseDocumentPosition warehouseDocumentPosition;
            WarehouseDocument warehouseDocument = null;

            warehouseDocumentPosition =
                warehouseEntities.WarehouseDocumentPositions
                .Include(wdp => wdp.WarehouseDocument.WarehouseDocumentType)
                .Include(wdp => wdp.Order)
                .FirstOrDefault(d => d.Id == warehouseDocumenPositiontId);

            if (warehouseDocumentPosition != null)
            {
                warehouseDocument = warehouseDocumentPosition.WarehouseDocument;
            }

            return warehouseDocument;
        }

        //public RadComboBoxData GetTelericPersonList(RadComboBoxContext context)
        //{
        //    RadComboBoxData result = new RadComboBoxData();
        //    string filtr = context.Text;
        public WarehouseDocumentPosition GetWarehouseDocumentPositionById(int warehouseDocumenPositiontId)
        {
            WarehouseDocumentPosition warehouseDocumentPosition;


            warehouseDocumentPosition =
                warehouseEntities.WarehouseDocumentPositions
                .Include(x => x.Material)
                .Include(x => x.WarehouseDocument.Warehouse)
                .Include(x => x.WarehouseDocument.Warehouse1)
                .Include(x => x.Order)
                .Include(x => x.WarehouseDocumentPositionQuantityChanges.Select(y => y.QuestionAnswer1))
                .Include(x => x.WarehouseDocumentPositionQuantityChanges.Select(y => y.QuestionAnswer))
                .First(x => x.Id == warehouseDocumenPositiontId);


            //warehouseDocumentPosition =
            //    warehouseEntities.WarehouseDocumentPositions
            //    .Include("Material.Unit")
            //    .Include("WarehouseDocument.Warehouse")
            //    .Include("WarehouseDocument.Warehouse1")
            //    .Include("Order")
            //    .Include("WarehouseDocumentPositionQuantityChanges.ConfirmingAnswer")
            //    .Include("WarehouseDocumentPositionQuantityChanges.CancellingAnswer")
            //    .First(
            //        d => d.Id == warehouseDocumenPositiontId);

            return warehouseDocumentPosition;
        }

        //    return result;
        //}
        public IEnumerable<PositionWithShelfAndQuantity> GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(int warehouseDocumentId)
        {
            //TODO optimize query
            //var positions = warehouseEntities.WarehouseDocumentPosition
            //  //.Where(p => p.WarehouseDocument.Id == warehouseDocumentId)
            //  .Where(p => p.WarehouseDocument.Id == warehouseDocumentId)
            //  .Join(
            //    warehouseEntities.WarehousePositionZone
            //    //.Include("WarehouseDocumentPosition")
            //      .Include("Zone"),
            //    wdp => wdp.Id,
            //    wpz => wpz.WarehouseDocumentPosition.Id,
            //    (wdp, wpz) => new { wdp.Id, wpz.Zone, wpz.Quantity }
            //  );
            //var l = positions.ToList();

            var positionsForDocuments = warehouseEntities.WarehouseDocumentPositions
                  .Include(x => x.Order)
                  .Include(x => x.Department)
                  .Include(x => x.ProductionMachine)
                  .Include(x => x.OrderDelivery)
                  .Include(x => x.Material.Unit)
                  .Include(x => x.Material)
                  .Where(x => x.WarehouseDocument.Id == warehouseDocumentId)
              .AsEnumerable();

            List<PositionWithShelfAndQuantity> positionWithShelfAndQuantities = new List<PositionWithShelfAndQuantity>();

            foreach (var position in positionsForDocuments)
            {
                var warehousePositionZone = warehouseEntities
                    .WarehousePositionZones
                    .Include(x => x.Zone)
                    .Where(x => x.WarehouseDocumentPosition.Id == position.Id);

                if (warehousePositionZone != null && warehousePositionZone.Any())
                {
                    foreach (var positionZone in warehousePositionZone)
                    {
                        positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                        {
                            position = position,
                            shelf = positionZone.Zone,
                            quantity = (decimal)position.Quantity,
                            IsKJ = position.IsKJ.HasValue ? position.IsKJ.Value : false
                        });
                    }
                }
                else
                {
                    positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                    {
                        position = position,
                        shelf = GetZoneByIdDataOnly(48947), //positionZone.Zone, -- empty shelf name
                        quantity = (decimal)position.Quantity,
                        IsKJ = position.IsKJ.HasValue? position.IsKJ.Value : false
                    });
                }
            }

            //var hackToIncludeMaterial = from p in warehouseEntities.WarehouseDocumentPosition.Include("Material.Unit")
            //                            let shelfRelation =
            //                                (from s in warehouseEntities.WarehousePositionZone
            //                                 where s.WarehouseDocumentPosition.Id == p.Id
            //                                 select s).FirstOrDefault()
            //                            let shelf =
            //                                (from z in warehouseEntities.Zone where z.Id == shelfRelation.Id select z).FirstOrDefault()
            //                            where p.WarehouseDocument.Id == warehouseDocumentId
            //                            select new { p.Material, p.Material.Unit, p.Material.MaterialSIMPLE, pws = new PositionWithShelf() { position = p, order = p.Order, shelf = shelf } };
            //var rr = hackToIncludeMaterial.First().Material.Id;

            //var hackToIncludeMaterialMaterialized = hackToIncludeMaterial.ToArray();

            //positionWithShelves = from a in hackToIncludeMaterialMaterialized
            //                      select a.pws;

            return positionWithShelfAndQuantities;
        }

        public IQueryable<WarehouseFlowAction> GetWarehouseFlowActions(int warehouseId)//, Action<InvokeServerMethodOperation> userCallback = null)
        {
            var query = warehouseEntities.WarehouseFlowActions
                .Where(m => m.WarehouseSourceId == warehouseId || m.WarehouseTargetId == warehouseId)
                .OrderBy(m => m.DueTime);

            return query;
        }

        public WarehouseGroup GetWarehouseGroupById(int warehouseGroupId)
        {
            WarehouseGroup warehouseGroup;

            //warehouse = warehouseEntities.Warehouse.Include("WarehouseType").First(w => w.Id == warehouseId);
            warehouseGroup = warehouseEntities.WarehouseGroups
              .FirstOrDefault(w => w.Id == warehouseGroupId);

            return warehouseGroup;
        }

        public WarehouseGroupMembership GetWarehouseGroupMembership(int warehouseId, int groupID)
        {
            return
                (from gm in warehouseEntities.WarehouseGroupMemberships
                 where
                 gm.Warehouse.Id == warehouseId
                 &&
                 gm.WarehouseGroup.Id == groupID
                 select gm).FirstOrDefault();
        }

        public WarehouseGroupMembership GetWarehouseGroupMembership(int membershipId)
        {
            return
                (from gm in warehouseEntities.WarehouseGroupMemberships
                 where
                 gm.Id == membershipId
                 select gm).FirstOrDefault();
        }

        public IQueryable<WarehouseGroupPermission> GetWarehouseGroupPermissions(int? sourceWarehouseGroupId, int? targetWarehouseGroupId)
        {
            IQueryable<WarehouseGroupPermission> warehouseGroupPermissions;

            warehouseGroupPermissions = from p in warehouseEntities.WarehouseGroupPermissions
                                        where
                                        !p.RevokeTime.HasValue
                                        &&
                                         ((sourceWarehouseGroupId == null && p.WarehouseGroup == null) || (sourceWarehouseGroupId != null && p.WarehouseGroup.Id == sourceWarehouseGroupId))
                                         &&
                                         ((targetWarehouseGroupId == null && p.WarehouseGroup1 == null) || (targetWarehouseGroupId != null && p.WarehouseGroup1.Id == targetWarehouseGroupId))
                                        select p;

            return warehouseGroupPermissions;
        }

        public IQueryable<WarehouseGroupPermission> GetWarehouseGroupPermissions()
        {
            IQueryable<WarehouseGroupPermission> warehouseGroupPermissions;

            warehouseGroupPermissions = from p in warehouseEntities.WarehouseGroupPermissions.Include("WarehouseGroup").Include("WarehouseDocumentType")
                                        where !p.RevokeTime.HasValue
                                        select p;

            return warehouseGroupPermissions;
        }

        public int? GetWarehouseGroupPrivilegeForExecutionId(int? fromWarehouseId, int? toWarehouseId, int operationId, int SystemGroupId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityGroupPrivilegeForExecutions
                               .Include("SystemGroup")
                             where
                             !p.RevokeTime.HasValue
                             &&
                             ((p.FromWarehouseId == null && fromWarehouseId == null) || (p.FromWarehouseId != null && p.FromWarehouseId == fromWarehouseId))
                             &&
                             ((p.ToWarehouseId == null && toWarehouseId == null) || (p.ToWarehouseId != null && p.ToWarehouseId == toWarehouseId))
                             &&
                             p.WarehouseDocumentType.Id == operationId
                             &&
                             p.SystemGroup.Id == SystemGroupId
                             select p;

            WarehouseSecurityGroupPrivilegeForExecution privilegeForGroup = privileges.FirstOrDefault();

            if (privilegeForGroup != null)
                return privilegeForGroup.Id;
            else
                return null;
        }

        public int? GetWarehouseGroupPrivilegeForRead(int warehouseId, int systemGroupId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityGroupPrivilegeForReads
                             where
                             !p.RevokeTime.HasValue
                             &&
                             p.Warehouse.Id == warehouseId
                             &&
                             p.SystemGroup.Id == systemGroupId
                             select p;

            WarehouseSecurityGroupPrivilegeForRead privilegeForGroup = privileges.FirstOrDefault();

            if (privilegeForGroup != null)
                return privilegeForGroup.Id;
            else
                return null;
        }

        public IQueryable<Warehouse> GetWarehouseList(string filterName)
        {
            IQueryable<Warehouse> allWarehouses;
            lock (typeof(WarehouseDS))
            {
                ////object objFromCache = Toolbox.CacherEnt.GetObjectFromCache("AllWarehousesList");
                ////if (objFromCache != null)
                ////{
                ////    allWarehouses = (IQueryable<Warehouse>)objFromCache;
                ////}
                ////else
                {
                    allWarehouses = warehouseEntities.Warehouses.Include("Company");

                    ////Toolbox.CacherEnt.StoreObjectInCache("AllWarehousesList", allWarehouses);
                    // test:
                    //object objFromCacheTest = Toolbox.CacherEnt.GetObjectFromCache("AllZonesWithParents");
                }
            }

            var filteredWarehouses = from warehouse in allWarehouses
                                     where
                                           warehouse.Active &&
                                         (string.IsNullOrEmpty(filterName) ||
                                          warehouse.Name.ToLower().StartsWith(filterName.ToLower()))
                                     select warehouse;

            return filteredWarehouses;
        }

        //    result.Items = allWarehousesRadComboBoxItemData.ToArray();
        public IQueryable<WarehousePermission> GetWarehousePermissions(int? sourceWarehouseId, int? targetWarehouseId)
        {
            IQueryable<WarehousePermission> warehousePermissions;

            warehousePermissions = from p in warehouseEntities.WarehousePermissions
                                   where
                                    !p.RevokeTime.HasValue
                                    &&
                                    ((sourceWarehouseId == null && p.Warehouse == null) || (sourceWarehouseId != null && p.Warehouse.Id == sourceWarehouseId))
                                    &&
                                    ((targetWarehouseId == null && p.Warehouse1 == null) || (targetWarehouseId != null && p.Warehouse1.Id == targetWarehouseId))
                                   select p;

            return warehousePermissions;
        }

        //    var allWarehousesObjects = allWarehousesQuery.ToArray();
        //    var allWarehousesRadComboBoxItemData = from warehouse in allWarehousesObjects
        //                                           orderby warehouse.Name
        //                                           select new RadComboBoxItemData
        //                                           {
        //                                               Text = warehouse.Name,
        //                                               Value = warehouse.Id.ToString()
        //                                           };
        public IQueryable<WarehousePermission> GetWarehousePermissions()
        {
            return warehouseEntities.WarehousePermissions.Include("Warehouse").Include("WarehouseDocumentType");
        }

        //    var allWarehousesQuery = filteredWarehouses.Take(20);
        public int? GetWarehousePrivilegeForExecutionId(int? fromWarehouseId, int? toWarehouseId, int operationId, int SystemUserId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityPrivilegeForExecutions
                               .Include("SystemUser")
                             where
                             !p.RevokeTime.HasValue
                             &&
                             ((p.Warehouse == null && fromWarehouseId == null) || (p.Warehouse != null && p.Warehouse.Id == fromWarehouseId))
                             &&
                             ((p.Warehouse1 == null && toWarehouseId == null) || (p.Warehouse1 != null && p.Warehouse1.Id == toWarehouseId))
                             &&
                             p.WarehouseDocumentType.Id == operationId
                             &&
                             p.SystemUser.Id == SystemUserId
                             select p;

            WarehouseSecurityPrivilegeForExecution privilegeForUser = privileges.FirstOrDefault();

            if (privilegeForUser != null)
                return privilegeForUser.Id;
            else
                return null;
        }

        //    IQueryable<Warehouse> filteredWarehouses = GetWarehouseList(filtr);
        public int? GetWarehousePrivilegeForRead(int warehouseId, int systemUserId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityPrivilegeForReads
                             where
                             !p.RevokeTime.HasValue
                             &&
                             p.Warehouse.Id == warehouseId
                             &&
                             p.SystemUser.Id == systemUserId
                             select p;

            WarehouseSecurityPrivilegeForRead privilegeForUser = privileges.FirstOrDefault();

            if (privilegeForUser != null)
                return privilegeForUser.Id;
            else
                return null;
        }

        public WarehouseProductionOrder GetWarehouseProductionOrderById(int warehouseProductionOrderId)
        {
            WarehouseProductionOrder warehouseProductionOrder =
                warehouseEntities.WarehouseProductionOrders
                .Include("Order")
                .Where(p => p.Id == warehouseProductionOrderId)
                .FirstOrDefault();
            return warehouseProductionOrder;
        }

        public IQueryable<WarehouseSecurityPrivilegeForExecution> GetWarehouseSecurityPrivilegeForExecution(int? sourceWarehouseId, int? targetWarehouseId, int userId)
        {
            return warehouseEntities.WarehouseSecurityPrivilegeForExecutions
              .Include("SystemUser")
              .Where(p => !p.RevokeTime.HasValue && p.SystemUser.Id == userId);
        }

        public IEnumerable GetWarehouseStockMaster(int warehouseId, int? orderIdToFind, int? shelfIdToFind, int? materialId, string materialNameOrSimpleId, out int count, int pageSize, int pageNum)
        {
            /*
               SELECT B.Name, A.MaterialId, A.Quantity
                FROM (SELECT 	[MaterialId]
                      ,SUM([Quantity]) as [Quantity]
                  FROM [WB3].[dbo].[WarehouseDocumentPosition]
                  where WarehouseDocumentId in (select A.Id
                                  from [WarehouseDocument] as A
                                   where WarehouseTargetId = 37
                                   and TimeDeleted is null)
                      and Id in (select pz.PositionId
                               from WarehousePositionZone as pz
                              where pz.ZoneId = 285) -- WH5C

                  Group by [MaterialId] ) as A
                    LEFT JOIN [WB3].[dbo].[Material] as B ON A.MaterialId = B.Id
                ORDER BY Name
            */

            var x = warehouseEntities.WarehouseDocumentPositions
                    .Include("Warehouse")
                    .Include("WarehouseDocument")
                    .Include("WarehousePositionZone")
                    .Include("Material");

            var query = x.Where(w => w.WarehouseDocument.Warehouse1.Id == (int?)warehouseId && w.WarehouseDocument.TimeDeleted == null);

            if (orderIdToFind.HasValue)
            {
            }

            if (shelfIdToFind.HasValue)
                query = query.Where(w => w.WarehousePositionZones.Any(zone => zone.Id == shelfIdToFind));

            if (materialId.HasValue)
                query = query.Where(w => w.Material.Id == materialId);

            if (!string.IsNullOrEmpty(materialNameOrSimpleId))
                query = query.Where(w => w.Material.Name.ToLower().Contains(materialNameOrSimpleId) || w.Material.IndexSIMPLE.Contains(materialNameOrSimpleId));

            var items = query.GroupBy(w => new { WId = w.Warehouse.Id, MId = w.Material.Id }).Select(q1 => new
            {
                tmp = q1.FirstOrDefault(),
                Quantity = (double?)q1.Sum(qq => (double?)qq.Quantity)
            }).Select(q => new
            {
                IndexSIMPLE = q.tmp.Material.IndexSIMPLE,
                MaterialName = q.tmp.Material.Name,
                Quantity = (double?)q.Quantity,
                UnitShortName = q.tmp.Material.NameShort + ".",
                WarehouseId = (int?)q.tmp.Warehouse.Id,
                MaterialId = (int?)q.tmp.Material.Id,
            });

            items = items.OrderBy(p => p.IndexSIMPLE);

            count = items.Count();

            return items.ToArray();
        }


        public IQueryable<WarehouseStockReport> GetWarehouseStockReports(int? warehouseId, int? materialId, DateTime? timeFrom, DateTime? timeTo)
        {
            var stockReports =
                from p in
                    warehouseEntities.WarehouseStockReports
                    .Where
                    (p =>
                        (
                        !warehouseId.HasValue
                        || (p.Quantity < 0 && p.WarehouseSourceId == warehouseId)
                        || (p.Quantity > 0 && p.WarehouseTargetId == warehouseId)
                        )
                      && p.MaterialId == materialId
                      && (!timeFrom.HasValue || p.TimeStamp > timeFrom)
                      && (!timeTo.HasValue || p.TimeStamp < timeTo)
                        )
                select p;

            var stockReportsToPresent = stockReports.OrderBy(p => p.TimeStamp);

            // ??? Jak ma być
            //var test = stockReportsToPresent.ToList();

            return stockReportsToPresent; // stockReports; // stockReportsToPresent
        }


        public Zone GetZoneByIdDataOnly(int zoneId)
        {
            Zone zone;
            Zone zoneDataOnly;

            zone = warehouseEntities.Zones.FirstOrDefault(p => p.Id == zoneId);

            if (zone != null)
            {
                zoneDataOnly = new Zone();
                zoneDataOnly.Id = zone.Id;
                zoneDataOnly.Name = zone.Name;
            }
            else
            {
                zoneDataOnly = new Zone();
            }

            return zoneDataOnly;
        }

        public Zone GetZoneByName(string zoneName)
        {
            Zone zone;
            zone = warehouseEntities.Zones.First(p => p.Name == zoneName);
            return zone;
        }

        public WarehouseDocument GiveReturn(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, /*IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash,*/ bool exportDocToSIMPLE = true)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            return GiveReturn(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, DateTime.Now, /*docSignatures, docPositionsHash,*/ exportDocToSIMPLE);
        }

        public WarehouseDocument GiveReturn(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, DateTime date, /*IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash,*/ bool exportDocToSIMPLE = true)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource;
            //Warehouse warehouseTarget;
            //Person person;

            WarehouseDocumentTypeEnum warehouseDocumentType = WarehouseDocumentTypeEnum.ZwMinus;

            foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                if (positionWithQuantityAndShelf.position.Quantity >= 0) throw new ArgumentException("For ZwMinus quantity must be less than 0");
            }

            //TranslateParameters(warehouseSourceId, warehouseTargetId, authorId, out warehouseSource, out warehouseTarget, out person);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseEntities.SaveChanges();

                //var targetDeptId =
                //    warehouseEntities.WarehouseDepartment.Include("Department").Where(p => p.Warehouse.Id == warehouseTargetId).OrderBy(p => p.Department.Id).First().Department.Id;
                //int targetDeptSimpleId =
                //    warehouseEntities.DepartmentSIMPLEFiltered.Where(p => p.DepartmentId == targetDeptId).First().
                //        DepartmentId;

                // get target dept simple
                //int? targetDeptSimple = 20;
                //int? targetOrderId = null;
                //int? dokSubtype = 8;

                warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseTargetId, authorId,
                                                           positionWithQuantityAndShelfses, warehouseDocumentType, date, exportDocToSIMPLE); //, targetDeptSimple, targetOrderId, dokSubtype);

                
                //WarehouseDocument document = warehouseEntities.WarehouseDocuments.Where(p => p.Id == warehouseDocument.Id).First();


                //document.DocPositionsHash = docPositionsHash;

                //// Add signatures
                //foreach (WarehouseDocumentSignatureTmp wdst in docSignatures)
                //{
                //  WarehouseDocumentSignature wds = new WarehouseDocumentSignature();

                //  Person person1 = warehouseEntities.People.Where(p => p.Id == wdst.Person.Id).First();

                //  wds.Person = person1;
                //  wds.WarehouseDocument = document;
                //  wds.TimeStamp = wdst.TimeStamp;
                //  wds.Comment = wdst.Comment;
                //  wds.DocPositionsHash = wdst.Hash;

                //  warehouseEntities.AddToWarehouseDocumentSignatures(wds);
                //}

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        //public WarehouseDocument InternalExpenditure(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int consumerId, IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash, bool exportDocToSIMPLE, int? targetDeptSIMPLEId = null, int? targetOrderId = null, int? docSubtypeSIMPLE = null)
        //{
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
        //  Contract.Requires<DataException>(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null && pwqas.position.Quantity < 0), "Quantity of all positions must be negative i.e. less than 0");

        //  return InternalExpenditure(warehouseSourceId, authorId, positionWithQuantityAndShelfses, consumerId, DateTime.Now.Date, docSignatures, docPositionsHash, exportDocToSIMPLE, targetDeptSIMPLEId, targetOrderId, docSubtypeSIMPLE);
        //}

        //public WarehouseDocument InternalExpenditure(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int consumerId, DateTime date, IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash, bool exportDocToSIMPLE, int? targetDeptSIMPLEId = null, int? targetOrderId = null, int? docSubtypeSIMPLE = null)
        //{
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
        //  Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
        //  Contract.Requires<DataException>(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null && pwqas.position.Quantity < 0), "Quantity of all positions must be negative i.e. less than 0");

        //  WarehouseDocument warehouseDocument;

        //  using (TransactionScope transactionScope = new TransactionScope())
        //  {
        //    //Warehouse warehouseSource = warehouseEntities.Warehouses.Include("Company").First(p => p.Id == warehouseSourceId);

        //    //Person person = warehouseEntities.People.First(p => p.Id == authorId);

        //    //foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
        //    //{
        //    //  if (positionWithQuantityAndShelf.position.Quantity >= 0) throw new DataException("Quantity must be less than 0");
        //    //}

        //    warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation
        //    (
        //      warehouseSourceId,
        //      null,
        //      authorId,
        //      positionWithQuantityAndShelfses,
        //      WarehouseDocumentTypeEnum.Rw,
        //      date,
        //      exportDocToSIMPLE,
        //      targetDeptSIMPLEId,
        //      targetOrderId,
        //      docSubtypeSIMPLE);
        //    warehouseDocument.DocPositionsHash = docPositionsHash;

        //    // Add signatures
        //    foreach (var wdst in docSignatures)
        //    {
        //      var wds = new WarehouseDocumentSignature();
        //      var person1 = warehouseEntities.People.Where(p => p.Id == wdst.Person.Id).First();
        //      wds.Person = person1;
        //      wds.WarehouseDocument = warehouseDocument;
        //      wds.TimeStamp = wdst.TimeStamp;
        //      wds.Comment = wdst.Comment;
        //      wds.DocPositionsHash = wdst.Hash;
        //      warehouseEntities.AddToWarehouseDocumentSignatures(wds);
        //    }

        //    // Generate PP document on appropriate production dept
        //    if (targetDeptSIMPLEId.HasValue)
        //    {
        //      //int companyId = warehouseSource.Company.Id;
        //      int warehouseIdForMMplus = 0;
        //      var sw = new System.Diagnostics.Stopwatch();
        //      sw.Restart();
        //      int companyId = warehouseEntities.Warehouses.First(w => w.Id == warehouseSourceId).CompanyId;
        //      sw.Stop();
        //      var sw2 = new System.Diagnostics.Stopwatch();
        //      sw2.Restart();
        //      int companyIdC = warehouseEntities.Companies
        //        //.Include(c=>c.Warehouses)
        //        .First(c => c.Warehouses.Any(w => w.Id == warehouseSourceId))
        //        .Id;
        //      sw2.Stop();
        //      System.Diagnostics.Debug.WriteLine("CompanyId SQL execution by warehouse: {0}, by Company: {1}", sw.ElapsedMilliseconds, sw2.ElapsedMilliseconds);

        //      using (var transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
        //      {
        //        var productionDeptK3 = warehouseEntities.DepartmentSIMPLEFiltereds.FirstOrDefault(p => p.CompanyId == companyId && p.komorka_id == targetDeptSIMPLEId.Value);
        //        if (productionDeptK3 != null)
        //        {
        //          var productionWarehouse = warehouseEntities.WarehouseDepartments
        //            .Include("Warehouse")
        //            .FirstOrDefault(p => p.IsDefaultForProduction && p.DepartmentId == productionDeptK3.DepartmentId);

        //          // TODO: Check if the Rw document is created in Income or Production department

        //          if (productionWarehouse != null)
        //          {
        //            warehouseIdForMMplus = productionWarehouse.WarehouseId;//.Warehouse.Id;
        //          }
        //        }
        //      }

        //      if (warehouseIdForMMplus > 0)
        //      {
        //        var warehouseTarget = warehouseEntities.Warehouses
        //          .First(p => p.Id == warehouseIdForMMplus);

        //        throw new ArgumentException("positionWithQuantityAndShelfses is no longer passed by ref - the following code will not work and needs a redesign.");
        //        foreach (var position in positionWithQuantityAndShelfses)
        //        {
        //          position.position.Quantity = Math.Abs(position.position.Quantity);

        //          foreach (var posParent in position.positionParentIds)
        //          {
        //            posParent.Quantity = 0;
        //          }
        //        }

        //        // Modify structure
        //        foreach (var positionWithQuantityAndShelfs in positionWithQuantityAndShelfses)
        //        {
        //          // clear related stuff
        //          positionWithQuantityAndShelfs.positionParentIds.Clear();
        //          positionWithQuantityAndShelfs.documentParentIds.Clear();

        //          // Add position parent ids
        //          foreach (var wpz in positionWithQuantityAndShelfs.WarehousePositionZones)
        //          {
        //            ChildParentPositionQuantity childParentPositionQuantity = new ChildParentPositionQuantity();
        //            childParentPositionQuantity.PositionParentID = wpz.WarehouseDocumentPosition.Id;
        //            childParentPositionQuantity.Quantity = wpz.Quantity;
        //            //childParentPositionQuantity.LocationId = wpz.Zone.Id;
        //            positionWithQuantityAndShelfs.positionParentIds.Add(childParentPositionQuantity);
        //          }

        //          WarehousePositionZone warehousePositionZone = new WarehousePositionZone();
        //          warehousePositionZone.Zone = warehouseEntities.Zones.First(p => p.Id == 6292);
        //          warehousePositionZone.Quantity = (from p in positionWithQuantityAndShelfs.WarehousePositionZones select p.Quantity).Sum();

        //          warehouseEntities.Detach(warehousePositionZone);

        //          positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>() { warehousePositionZone };

        //          // Add document parent id
        //          positionWithQuantityAndShelfs.documentParentIds.Add(warehouseDocument.Id);

        //          Order forOrder = null;
        //          if (targetOrderId.HasValue && targetOrderId.Value > 0)
        //          {
        //            forOrder =
        //                warehouseEntities.Orders.Where(p => p.Id == targetOrderId.Value).FirstOrDefault();
        //          }
        //          else
        //          {
        //            forOrder = positionWithQuantityAndShelfs.position.Order;
        //          }

        //          positionWithQuantityAndShelfs.position =
        //              CreateWarehouseDocumentPositionDataOnly(
        //                  positionWithQuantityAndShelfs.position.Material.Id,
        //                  positionWithQuantityAndShelfs.position.UnitPrice,
        //                  positionWithQuantityAndShelfs.position.Quantity, 0,
        //                  forOrder != null ? forOrder.Id : 0);
        //          positionWithQuantityAndShelfs.order = positionWithQuantityAndShelfs.position.Order;
        //        }

        //        Person personSys = warehouseEntities.People.First(p => p.FirstName == "System" && p.LastName == "K3"); // System K3
        //        WarehouseDocument docPP = CreateWarehouseDocumentBasedOnWarehouseInternalOperation
        //        (
        //          warehouseSourceId,
        //          warehouseTarget.Id,
        //          personSys.Id,
        //          positionWithQuantityAndShelfses,
        //          WarehouseDocumentTypeEnum.PP,
        //          date,
        //          false,
        //          targetDeptSIMPLEId,
        //          targetOrderId,
        //          null);
        //      }
        //    }

        //    warehouseEntities.SaveChanges();
        //    transactionScope.Complete();
        //    warehouseEntities.AcceptAllChanges();
        //  }

        //  return warehouseDocument;
        //}

        public WarehouseDocument InternalGive(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum warehouseDocumentType, /*IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash,*/ bool exportDocToSIMPLE = true)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
            //Contract.Requires(docSignatures != null);
            Contract.Requires(warehouseSourceId > 0);
            Contract.Requires(warehouseTargetId > 0);
#endif
            return InternalGive(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, warehouseDocumentType, DateTime.Now, /*docSignatures, docPositionsHash,*/ exportDocToSIMPLE);
        }

        public WarehouseDocument InternalGive(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum warehouseDocumentType, DateTime date, /*IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash,*/ bool exportDocToSIMPLE = true)
        {
#if DEBUG
            //Contract.Requires(docSignatures != null);
            Contract.Requires(warehouseSourceId > 0);
            Contract.Requires(warehouseTargetId > 0);
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            Warehouse warehouse = GetWarehouseById(warehouseSourceId);

            if (warehouse.WarehouseType.IsOrderRequired)
            {
                throw new ArgumentException("For source warehouse order is required");
            }

            WarehouseDocument warehouseDocument = Give(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, warehouseDocumentType, date, exportDocToSIMPLE);

            WarehouseDocument document = warehouseEntities.WarehouseDocuments.Where(p => p.Id == warehouseDocument.Id).First();

            //document.DocPositionsHash = docPositionsHash;

            //// Add signatures
            //foreach (WarehouseDocumentSignatureTmp wdst in docSignatures)
            //{
            //  WarehouseDocumentSignature wds = new WarehouseDocumentSignature();

            //  Person person = warehouseEntities.People.Where(p => p.Id == wdst.Person.Id).First();

            //  wds.Person = person;
            //  wds.WarehouseDocument = document;
            //  wds.TimeStamp = wdst.TimeStamp;
            //  wds.Comment = wdst.Comment;
            //  wds.DocPositionsHash = wdst.Hash;

            //  warehouseEntities.AddToWarehouseDocumentSignatures(wds);
            //}
            warehouseEntities.SaveChanges();

            return warehouseDocument;
        }

        public IEnumerable<WarehouseDocument> InternalGiveForOrderMultiple(List<int> orderDocumentIds, int userId, DateTime date, /*IEnumerable<WarehouseDocumentSignatureTmp> docSignatures, byte[] docPositionsHash,*/ bool exportDocToSimple = true, int? targetDeptSIMPLEId = null, int? targetOrderId = null, int docSubtypeSIMPLE = 1)
        {
#if DEBUG
            Contract.Requires(orderDocumentIds != null);
#endif
            List<WarehouseDocument> warehouseDocuments = new List<WarehouseDocument>();

            using (TransactionScope transactionScope = new TransactionScope())
            {
                foreach (int orderDocumentId in orderDocumentIds)
                {
                    WarehouseDocument newGiveForOrderDocument = InternalGiveForOrder(orderDocumentId, userId, date,
                        /*docSignatures, docPositionsHash,*/
                                                                                     exportDocToSimple,
                                                                                     targetDeptSIMPLEId, targetOrderId,
                                                                                     docSubtypeSIMPLE);
                    warehouseDocuments.Add(newGiveForOrderDocument);

                }
                //warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
            return warehouseDocuments;
        }

        //[Obsolete]
        //public WarehouseDocument InternalIncome(int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, DateTime date, bool exportDocToSIMPLE)
        //{
        //  WarehouseDocument warehouseDocument;
        //  Warehouse warehouseTarget = warehouseEntities.Warehouses.First(w => w.Id == warehouseTargetId);
        //  Person person = warehouseEntities.People.First(p => p.Id == authorId);

        //  foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
        //  {
        //    if (positionWithQuantityAndShelf.position.Quantity <= 0) throw new DataException("Quantity must be greater than 0");
        //  }

        //  using (TransactionScope transactionScope = new TransactionScope())
        //  {
        //    // Generating new Internal Income (Pw) document
        //    warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(null, warehouseTarget, person, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.Pw, date, exportDocToSIMPLE);

        //    warehouseEntities.SaveChanges();
        //    transactionScope.Complete();
        //    warehouseEntities.AcceptAllChanges();
        //  }
        //  return warehouseDocument;
        //}

        //[Obsolete]
        //public WarehouseDocument InternalIncomeWithExpenditure(int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses)
        //{
        //  return InternalIncomeWithExpenditure(warehouseTargetId, authorId, positionWithQuantityAndShelfses, DateTime.Now);
        //}

        //public WarehouseDocument InternalIncomeWithExpenditure(int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, DateTime date)
        //{
        //  WarehouseDocument warehouseDocument;
        //  Warehouse warehouseTarget = warehouseEntities.Warehouses.First(w => w.Id == warehouseTargetId);
        //  Person person = warehouseEntities.People.First(p => p.Id == authorId);

        //  foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
        //  {
        //    if (positionWithQuantityAndShelf.position.Quantity <= 0) throw new DataException("Quantity must be greater than 0");
        //  }

        //  using (TransactionScope transactionScope = new TransactionScope())
        //  {
        //    // Generating a list of parent Positions of to add them to Rw document
        //    List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfsesForRw =
        //        new List<PositionWithQuantityAndShelfs>();

        //    // Iterating through all positions in the income document
        //    foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelfs in positionWithQuantityAndShelfses)
        //    {
        //      // Creating a special structure to match the

        //      // Iterating through all parent positions
        //      // GENERATING Rw POSITION FOR EACH PARENT
        //      foreach (ChildParentPositionQuantity childParentPositionQuantity in positionWithQuantityAndShelfs.positionParentIds)
        //      {
        //        PositionWithQuantityAndShelfs posForRw = new PositionWithQuantityAndShelfs();

        //        // GETTING FULL INFO OF THE POSITION
        //        WarehouseDocumentPosition positionParent =
        //            GetPositionById(childParentPositionQuantity.PositionParentID);

        //        // Dealing with entity framework issue
        //        IQueryable<WarehouseDocumentPosition> refreshedPositionParent =
        //            from pos in warehouseEntities.WarehouseDocumentPositions.Include("Warehouse").Include("Material").Include("WarehousePositionZone").Include("Order")
        //            where pos.Id == positionParent.Id
        //            select pos;

        //        var positionParentNewRefreshed = refreshedPositionParent.FirstOrDefault();

        //        // GENERATING A NEW POSITION FOR Rw
        //        WarehouseDocumentPosition warehouseDocumentPosition = CreateWarehouseDocumentPositionDataOnly(positionParentNewRefreshed.Material.Id, positionParent.UnitPrice,
        //                                                                                            -childParentPositionQuantity.Quantity);

        //        // ADDING PARENT POSITION RELATION INFO:
        //        posForRw.positionParentIds = new List<ChildParentPositionQuantity>
        //                                                     {
        //                                                         new ChildParentPositionQuantity()
        //                                                             {
        //                                                                 PositionParentID = positionParentNewRefreshed.Id,
        //                                                                 Quantity = childParentPositionQuantity.Quantity
        //                                                             }
        //                                                     };

        //        // ADDING PARENT DOCUMENT RELATION INFO:
        //        posForRw.documentParentIds = new List<int> { GetDocumentIdByPositionId(positionParentNewRefreshed.Id) };

        //        posForRw.position = warehouseDocumentPosition;

        //        // TODO: Check if the zone matching is correct!!!
        //        // CREATING A LIST OF POS ZONES
        //        posForRw.WarehousePositionZones = new List<WarehousePositionZone>();

        //        var zones = from z in warehouseEntities.WarehousePositionZones
        //                    where z.Zone.Id == childParentPositionQuantity.LocationId
        //                    select z;
        //        WarehousePositionZone zone = zones.FirstOrDefault();

        //        // ADDING THE MATCHED ZONE TO THE POSITION)
        //        if (zone != null)
        //        {
        //          IQueryable<WarehousePositionZone> zonesRefreshed = from z in warehouseEntities.WarehousePositionZones.Include("Zone")
        //                                                             where
        //                                                                 z.Id == zone.Id
        //                                                             select z;
        //          var zoneRefreshed = zonesRefreshed.FirstOrDefault();

        //          Zone zoneNew = GetShelfByIdDataOnly(zoneRefreshed.Zone.Id);
        //          posForRw.WarehousePositionZones.Add(new WarehousePositionZone()
        //          {
        //            WarehouseDocumentPosition = warehouseDocumentPosition,
        //            Quantity = Math.Abs(warehouseDocumentPosition.Quantity),
        //            Zone = zoneNew
        //          });
        //        }
        //        else
        //        {
        //          throw new Exception("Data is inconsistent - there is no enough material");
        //        }

        //        if (positionParentNewRefreshed.Order != null)
        //        {
        //          // ADDING ORDER INFO
        //          Order order = GetOrderByIdDataOnly(positionParentNewRefreshed.Order.Id);
        //          posForRw.position.Order = order;
        //        }

        //        // FINALLY, ADDING THE POSITION TO THE LIST
        //        positionWithQuantityAndShelfsesForRw.Add(posForRw);
        //      }
        //    }

        //    // Generating new Internal Expenditure (Rw) document
        //    var docRw = InternalExpenditure(warehouseTargetId, authorId, positionWithQuantityAndShelfsesForRw, 0, null, null, true);

        //    // Iterating through all positionWithQuantityAndShelfses positions
        //    foreach (var pos in positionWithQuantityAndShelfses)
        //    {
        //      //pos.WarehousePositionZones = new List<WarehousePositionZone>();

        //      pos.documentParentIds = new List<int>();
        //      foreach (var childParentPositionQuantity in pos.positionParentIds)
        //      {
        //        childParentPositionQuantity.PositionParentID =
        //            GetChildPositionMadeByDocument(GetPositionById(childParentPositionQuantity.PositionParentID), docRw).Id;

        //        pos.documentParentIds.Add(GetDocumentIdByPositionId(childParentPositionQuantity.PositionParentID));
        //      }
        //    }

        //    // Generating new Internal Income (Pw) document
        //    warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(null, warehouseTargetId, authorId, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.Pw, date);

        //    warehouseEntities.SaveChanges();
        //    transactionScope.Complete();
        //    warehouseEntities.AcceptAllChanges();
        //  }
        //  return warehouseDocument;
        //}

        ////public WarehouseDocument InternalReceive(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum warehouseDocumentType)
        //{
        //  return InternalReceive(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, warehouseDocumentType, DateTime.Now);
        //}

        public WarehouseDocument InternalReceive(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum warehouseDocumentType, DateTime date)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource;
            //Warehouse warehouseTarget;
            //Person person;


            if (warehouseDocumentType != WarehouseDocumentTypeEnum.MMPlus && warehouseDocumentType != WarehouseDocumentTypeEnum.POPlus && warehouseDocumentType != WarehouseDocumentTypeEnum.Pw) throw new ArgumentException("Parameter can be MMPlus, POPlus or Pw only", "warehouseDocumentType");

            foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                if (positionWithQuantityAndShelf.position.Quantity <= 0) throw new ArgumentException("For MMPlus quantity must be greater than 0");
            }

            //TranslateParameters(warehouseSourceId, warehouseTargetId, authorId, out warehouseSource, out warehouseTarget, out person);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (CheckAreThereGivesForReceive(positionWithQuantityAndShelfses, warehouseDocumentType) == false)
                {
                    throw new ApplicationException("There is no MMMinus/POMinus to generate MMPlus/POPlus (without order)");
                }

                // Copy MM- document's exportDocToSIMPLE flag:
                var firstSourcePosition = positionWithQuantityAndShelfses.FirstOrDefault();
                int firstSourcePositionId = firstSourcePosition.positionParentIds.First().PositionParentID;
                var firstSourcePositionDoc = warehouseEntities.WarehouseDocumentPositions
                  .Include(wdp => wdp.WarehouseDocument)
                  .Where(wdp => wdp.Id == firstSourcePositionId)
                  .Select(wdp => wdp.WarehouseDocument)
                  .FirstOrDefault();
                bool exportDocToSIMPLE = firstSourcePositionDoc.ExportToSIMPLE;

                var targetOrder = positionWithQuantityAndShelfses
                  .Where(p => p.position.Order != null)
                  .Select(p => p.position.Order)
                  .FirstOrDefault();
                var targetOrderId = targetOrder != null ? targetOrder.Id : (int?)null;

                warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses,
                  warehouseDocumentType, date, exportDocToSIMPLE, null, targetOrderId);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        public WarehouseDocument InternalReceive(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId, DateTime date)
        {
            return InternalReceive(givenDocumentId, userId, positionsList, false, date);
        }

        public WarehouseDocument InternalReceive(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId)
        {
            return InternalReceive(givenDocumentId, userId, positionsList, false, DateTime.Now);
        }

        public WarehouseDocument InternalReceiveDocumentTypeSet(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId, DateTime date, WarehouseDocumentTypeEnum warehouseDocumentType)
        {
            return InternalReceiveDocumentTypeSet2(givenDocumentId, userId, positionsList, false, date, warehouseDocumentType);
        }

        public WarehouseDocument InternalReceiveForOrder(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId, DateTime date)
        {
            return InternalReceive(givenDocumentId, userId, positionsList, true, date);
        }

        public WarehouseDocument InternalReceiveForOrder(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId)
        {
            return InternalReceive(givenDocumentId, userId, positionsList, true, DateTime.Now);
        }

        public WarehouseDocument Inventory(int warehouseTargetId, int authorId, List<WarehouseInventoryPosition> originalPositionsWithShelves, int? contractingPartyId, bool addOnly = true)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(originalPositionsWithShelves != null);
#endif
            var posWithQuantAndShelves = new List<PositionWithQuantityAndShelfs>();
            bool exportToSimple = false;
            int? targetDeptSIMPLEId = null;

            //WarehouseDocumentPosition wdp = new WarehouseDocumentPosition();
            //wdp.de
            var positionsWithShelves = new List<WarehouseInventoryPosition>(); // create empty list instead of null to avoid NullReferenceException down the line
            using (var transactionScope = new TransactionScope())
            {
                positionsWithShelves = GetActiveWarehouseInventoryPositions();
                // positionsWithShelves contains positions from all users so we need to filter only those submited by the current user
                positionsWithShelves = positionsWithShelves.Join(originalPositionsWithShelves, pws => pws.Id, opws => opws.Id, (pws, opws) => pws).ToList();

                //Set QuantityBefore from inventory grid
                foreach (var position in positionsWithShelves)
                {
                    position.QuantityBefore = originalPositionsWithShelves.Single(p => p.Id == position.Id).QuantityBefore.GetValueOrDefault(0);
                }

                foreach (var positionWithShelf in positionsWithShelves)
                {
                    PositionWithQuantityAndShelfs positionWithQuantityAndShelfs;
                    // = new PositionWithQuantityAndShelfs();

                    int? orderId = positionWithShelf.Order != null ? positionWithShelf.Order.Id : (int?)null;
                    double? sumQuantityForGivenPlace = 0;
                    exportToSimple = positionWithShelf.ExportToSIMPLE;
                    targetDeptSIMPLEId = positionWithShelf.TargetDeptSIMPLEId;

                    IQueryable<CurrentStackInDocument> matchingWarehouseStacks = null;
                    IQueryable<WarehouseDocumentPosition> matchingWS = null;
                    List<WarehouseDocumentPosition> matchWS = null;

                    int? docType = GetDocumentTypeForWarehouseDocumentPositionId(positionWithShelf.WarehouseDocumentPositionId);
                    if (docType == 0)
                        docType = null;


                    // Checking warehouse stacks and choosing whether to add or remove quantities
                    if ((docType == null || docType==0) ? false : (docType == 10 || docType == 29 || docType == 32) ? true : false)
                    {
                        matchingWS = GetOrderedStackList(warehouseTargetId, positionWithShelf.Material.Id, orderId, positionWithShelf.ZoneId).AsQueryable();
                        
                        if (orderId.HasValue)
                        {
                            matchingWS = matchingWS.Where(p => p.OrderId == orderId);
                        }
                        else
                        {
                            matchingWS = matchingWS.Where(p => !p.OrderId.HasValue);
                        }

                        if (positionWithShelf.ProcessIdpl.HasValue)
                        {
                            matchingWS = matchingWS.Where(p => p.ProcessIdpl == positionWithShelf.ProcessIdpl.Value);
                        }
                        else
                        {
                            matchingWS = matchingWS.Where(p => !p.ProcessIdpl.HasValue);
                        }

                        if (positionWithShelf.ProcessIddpl.HasValue)
                        {
                            matchingWS = matchingWS.Where(p => p.ProcessIddpl == positionWithShelf.ProcessIddpl.Value);
                        }
                        else
                        {
                            matchingWS = matchingWS.Where(p => !p.ProcessIddpl.HasValue);
                        }

                        if (positionWithShelf.Quantity.HasValue)
                        {
                            if (Math.Abs((double)positionWithShelf.Quantity) > 0)
                            {
                                if (docType == 29)
                                {
                                    matchingWS = matchingWS.Where(p => Math.Abs(p.Quantity) == Math.Abs(positionWithShelf.QuantityBefore.Value));
                                }
                                else
                                {
                                    matchingWS = matchingWS.Where(p => Math.Abs(p.Quantity) == Math.Abs(positionWithShelf.Quantity.Value));
                                }
                                
                            }
                        }

                        // TODO: Department stuff...

                        matchingWarehouseStacks = warehouseEntities.CurrentStackInDocuments
                            .Where(p => p.WarehouseTargetId == warehouseTargetId &&
                                p.MaterialId == positionWithShelf.Material.Id &&
                                //&& ((!orderId.HasValue && !p.OrderId.HasValue) || ((orderId.HasValue && p.OrderId.HasValue && p.OrderId == orderId)))
                                //&& ((!positionWithShelf.position.DepartmentId.HasValue && !p.DepartmentId.HasValue) || ((positionWithShelf.position.DepartmentId.HasValue && p.DepartmentId.HasValue && p.DepartmentId == positionWithShelf.position.DepartmentId)))
                                //&& ((!positionWithShelf.position.ProcessIdpl.HasValue && !p.ProcessIdpl.HasValue) || ((positionWithShelf.position.ProcessIdpl.HasValue && p.ProcessIdpl.HasValue && p.ProcessIdpl == positionWithShelf.position.ProcessIdpl)))
                                p.ShelfId == positionWithShelf.Zone.Id);

                        sumQuantityForGivenPlace = matchingWS.Sum(p => (double?)p.Quantity);

                        if (!sumQuantityForGivenPlace.HasValue)
                        {
                            sumQuantityForGivenPlace = 0;
                        }

                        matchWS = matchingWS.ToList();
                    }
                    else
                    {
                        List<int> usedId = new List<int>();
                        matchingWarehouseStacks = warehouseEntities.CurrentStackInDocuments
                            .Where(p => p.WarehouseTargetId == warehouseTargetId &&
                                p.MaterialId == positionWithShelf.Material.Id &&
                                //&& ((!orderId.HasValue && !p.OrderId.HasValue) || ((orderId.HasValue && p.OrderId.HasValue && p.OrderId == orderId)))
                                //&& ((!positionWithShelf.position.DepartmentId.HasValue && !p.DepartmentId.HasValue) || ((positionWithShelf.position.DepartmentId.HasValue && p.DepartmentId.HasValue && p.DepartmentId == positionWithShelf.position.DepartmentId)))
                                //&& ((!positionWithShelf.position.ProcessIdpl.HasValue && !p.ProcessIdpl.HasValue) || ((positionWithShelf.position.ProcessIdpl.HasValue && p.ProcessIdpl.HasValue && p.ProcessIdpl == positionWithShelf.position.ProcessIdpl)))
                                p.ShelfId == positionWithShelf.Zone.Id)
                                .OrderByDescending(x => x.WarehouseDocumentId); // bierzemy do inv ostatnio wprowadzone!



                        if (orderId.HasValue)
                        {
                            matchingWarehouseStacks = matchingWarehouseStacks.Where(p => p.OrderId == orderId);
                        }
                        else
                        {
                            matchingWarehouseStacks = matchingWarehouseStacks.Where(p => !p.OrderId.HasValue);
                        }

                        if (positionWithShelf.ProcessIdpl.HasValue)
                        {
                            matchingWarehouseStacks = matchingWarehouseStacks.Where(p => p.ProcessIdpl == positionWithShelf.ProcessIdpl.Value);
                        }
                        else
                        {
                            matchingWarehouseStacks = matchingWarehouseStacks.Where(p => !p.ProcessIdpl.HasValue);
                        }

                        if (positionWithShelf.ProcessIddpl.HasValue)
                        {
                            matchingWarehouseStacks = matchingWarehouseStacks.Where(p => p.ProcessIddpl == positionWithShelf.ProcessIddpl.Value);
                        }
                        else
                        {
                            matchingWarehouseStacks = matchingWarehouseStacks.Where(p => !p.ProcessIddpl.HasValue);
                        }

                        // TODO: Department stuff...

                        sumQuantityForGivenPlace = matchingWarehouseStacks.Sum(p => (double?)p.Quantity);

                        if (!sumQuantityForGivenPlace.HasValue)
                        {
                            sumQuantityForGivenPlace = 0;
                        }
                    }

                    if (sumQuantityForGivenPlace != null)
                    {
                        sumQuantityForGivenPlace = Math.Abs((double)sumQuantityForGivenPlace);
                    }

                    if ((decimal)positionWithShelf.QuantityBefore != Convert.ToDecimal(sumQuantityForGivenPlace) &&
                        !((docType == null || docType==0) ? false : (docType == 10 || docType == 29 || docType == 32) ? true : false))
                    {
                        var exMsg = string.Format(
                          "Quantity has changed during this operation!\nDetails: Id:{0}, MaterialId:{1}, TimeAdded:{2}, QtyBefore/After:{3}/{4}, UnitPrice:{5}, ZoneId:{6}, OrderId:{7}\nRefresh and try again!",
                          positionWithShelf.Id,
                          positionWithShelf.MaterialId,
                          positionWithShelf.AddedTime,
                          positionWithShelf.QuantityBefore,
                          sumQuantityForGivenPlace,
                          positionWithShelf.UnitPrice,
                          positionWithShelf.ZoneId,
                          positionWithShelf.OrderId
                        );
                        throw new Exception(exMsg);
                    }

                    var quantityToBeBalanced = addOnly
                                                      ? (decimal)positionWithShelf.Quantity.Value
                                                      : docType == 29?((decimal)positionWithShelf.Quantity.Value):((decimal)positionWithShelf.Quantity.Value - Convert.ToDecimal(sumQuantityForGivenPlace));

                    //if (Math.Abs(quantityToBeBalanced) > 0)
                    //{
                    if (!addOnly)
                    {
                        positionWithShelf.Quantity = (double)quantityToBeBalanced;
                    }

                    if (quantityToBeBalanced < 0 || ((docType == null) ? false : (docType == 10 || docType == 29 || docType == 32) ? true : false))
                    {
                        // Collect source positions
                        if ((docType == null) ? false : (docType == 10 || docType == 29 || docType == 32) ? true : false)
                        {
                            positionWithQuantityAndShelfs = CollectPositionsFromStacks(matchWS, Math.Abs(quantityToBeBalanced));
                        }
                        else
                        {
                            positionWithQuantityAndShelfs = CollectPositionsFromStacks(matchingWarehouseStacks, Math.Abs(quantityToBeBalanced));
                        }

                        if (positionWithShelf.Zone != null)
                        {
                            WarehousePositionZone warehousePositionZone = new WarehousePositionZone();
                            warehousePositionZone.Zone = positionWithShelf.Zone;
                            warehousePositionZone.Quantity = positionWithShelf.Quantity.Value;
                            positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();
                            positionWithQuantityAndShelfs.WarehousePositionZones.Add(warehousePositionZone);

                            warehousePositionZone.ZoneTmp = positionWithShelf.Zone;
                            warehouseEntities.Detach(warehousePositionZone); // this line is stuffed here by Marek Zakrzewski as a sloppy workaround for not creating entities in the correct order
                        }
                    }
                    else
                    {
                        positionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
                        if (positionWithShelf.Zone != null)
                        {
                            WarehousePositionZone warehousePositionZone = new WarehousePositionZone();
                            warehousePositionZone.Zone = positionWithShelf.Zone;
                            warehousePositionZone.Quantity = positionWithShelf.Quantity.Value;
                            positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();
                            positionWithQuantityAndShelfs.WarehousePositionZones.Add(warehousePositionZone);

                            warehousePositionZone.ZoneTmp = positionWithShelf.Zone;
                            warehouseEntities.Detach(warehousePositionZone); // this line is stuffed here by Marek Zakrzewski as a sloppy workaround for not creating entities in the correct order
                        }
                    }
                    positionWithQuantityAndShelfs.position = CreateWarehouseDocumentPosition(positionWithShelf.Material.Id, positionWithShelf.UnitPrice.Value, (decimal)positionWithShelf.Quantity.Value); //CreateWarehouseDocumentPositionDataOnly(positionWithShelf.Material.Id, positionWithShelf.UnitPrice.Value, (decimal)positionWithShelf.Quantity.Value);
                    positionWithQuantityAndShelfs.position.ProcessIdpl = positionWithShelf.ProcessIdpl;
                    positionWithQuantityAndShelfs.position.ProcessIddpl = positionWithShelf.ProcessIddpl;
                    positionWithQuantityAndShelfs.order = positionWithShelf.Order;
                    positionWithQuantityAndShelfs.position.SimpleOrderId = positionWithShelf.SIMPLEOrderId;


                    posWithQuantAndShelves.Add(positionWithQuantityAndShelfs);
                    //}
                    //else
                    //{
                    //  throw new Exception("Cannot balance zero quantity");
                    //}
                }

                //Warehouse warehouseTarget = warehouseEntities.Warehouses.First(p => p.Id == warehouseTargetId);
                //Person person = warehouseEntities.People.First(p => p.Id == authorId);

                var invDoc = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseTargetId, warehouseTargetId, authorId, posWithQuantAndShelves,
                                          WarehouseDocumentTypeEnum.INW, DateTime.Now, exportToSimple, targetDeptSIMPLEId);

                //invDoc.DocTypeSerialNumber = GetNumberOfDocuments(WarehouseDocumentTypeEnum.INW);
                //invDoc.DocTypeAndWarehouseSerialNumber = GetNumberOfDocuments(WarehouseDocumentTypeEnum.INW, invDoc.Warehouse.Id, invDoc.Warehouse1.Id);

                foreach (var warehouseInventoryPosition in positionsWithShelves)
                {
                    warehouseInventoryPosition.IsActive = false;
                }
                warehouseEntities.SaveChanges();

                transactionScope.Complete();

                return invDoc;
            }
        }

        public bool IsOperatorMappedToSIMPLEAccount(int operatorId, int companyId)
        {
            return
                warehouseEntities.WarehouseOperatorMappingSIMPLEs
                .Where(p => p.CompanyId == companyId && p.PersonId == operatorId)
                .Any();
        }

        public bool IsPositionDamaged(WarehouseDocumentPosition position)
        {

#if DEBUG
            Contract.Requires(position != null);
#endif
            return PositionStatusIs(position, PositionFeaturedStatus.Damaged);
        }

        //// NOT USED:
        //[Obsolete]
        //public bool IsPositionDamagedBasedOnDescription(WarehouseDocumentPosition position)
        //{
        //  ArrayList extDocs = GetPositionExtensionXmlDocumentsByPositionId(position.Id);
        //  foreach (XmlDocument doc in extDocs)
        //  {
        //    XmlNodeList nodes = doc.SelectNodes("/document/damageDescription");

        //    if (nodes != null && nodes.Count > 0)
        //    {
        //      return true;
        //    }
        //  }
        //  return false;
        //}

        public bool IsValidWarehouseGroupId(int id)
        {
            // TODO: implement
            return true;
        }

        public bool IsValidWarehouseGroupShortName(string shortName)
        {
#if DEBUG
            Contract.Requires(shortName != null);
#endif
            return shortName.Length > 0 && shortName.Length <= 7;
        }

        /// <summary>
        /// Marek Zakrzewski ; [2010-03-16] ; Check if a given number is a valid warehouse identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsValidWarehouseId(int id)
        {
            // TODO: implement
            return true;
        }

        /// <summary>
        /// Marek Zakrzewski ; [2010-03-16] ; Check if a given number is a valid warehouse short name
        /// </summary>
        /// <param name="shortName"></param>
        /// <returns></returns>
        public bool IsValidWarehouseShortName(string shortName)
        {
#if DEBUG
            Contract.Requires(shortName != null);
#endif
            return shortName.Length > 0 && shortName.Length <= 7;
        }

        public bool IsWarehouseMappedToSIMPLE(int warehouseId)
        {
            return
                warehouseEntities.WarehouseSIMPLEs
                .Include("Warehouse")
                .Where(p => p.Warehouse.Id == warehouseId)
                .Any();
        }

        public void KCStateReturnSaveSolvingPositionId(int kcStateId, int warehouseDocumentPositionId)
        {

            var KCState = warehouseEntities.WarehouseDocumentPositionKCStates.Where(p => p.Id == kcStateId).First();
            if (KCState != null && !KCState.SolvingDocumentPositionId.HasValue)
            {

                KCState.SolvingDocumentPositionId = warehouseDocumentPositionId;
                warehouseEntities.SaveChanges();
            }
            else
            {
                throw new Exception("Błędny identyfikator stanu lub stan już wcześniej zapisany");
            }
        }

        public string LogError(Exception exception, int? userId)
        {
#if DEBUG
            Contract.Requires(exception != null);
#endif
            string errorMessage = "";
            using (TransactionScope transactionScope = new TransactionScope())
            {
                ErrorLog errorLog = new ErrorLog();

                errorLog.ErrorMessage = " ### Message: ### " + exception.Message + " ### Source: ### " + " ### InnerExceptionMsg: ### " + (exception.InnerException != null ? exception.InnerException.Message : "*") +
                                        exception.Source + " ### Stack trace: ### " + exception.StackTrace +
                                         (exception.InnerException != null ? " ### InnerExceptionMessage: ### " + exception.InnerException.Message : "");
                errorLog.ErrorCode = exception.Message.GetHashCode().ToString() // this code was previously purely random by means of random.Next(), now there is a bit better chance of tracking it down
                  .Substring(0, 5); // limit added due to DB schema field limit and there was no time to change the schema
                errorLog.UserId = userId;
                errorLog.TimeStamp = DateTime.Now;
                errorLog.ClientHostInfo = HttpContext.Current.Request.UserHostAddress + ";" +
                                          HttpContext.Current.Request.UserHostName + ";" +
                                          HttpContext.Current.Request.UserAgent;

                warehouseEntities.AddToErrorLogs(errorLog);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();

                errorMessage = string.Format(
                  "Wystąpił błąd o kodzie: <b>{0}</b>. Skontaktuj się z administratorem systemu podając kod błędu.<br />Szczegóły błędu:<br />{1}{2}{3}",
                  exception.Message.GetHashCode(), // less random than pure random.Next() that was here before
                  exception.Message,
                  (exception.InnerException != null ? exception.InnerException.Message + "<br />" : ""),
                  (exception.InnerException != null && exception.InnerException.InnerException != null ? exception.InnerException.InnerException.Message : "")
                );
            }
            return errorMessage;
        }

        //public void LogNewMaterial(Material material, int userId)
        //{
        //  Log log = new Log();
        //  log.Table = warehouseEntities.Tables.First(p => p.Id == 1);
        //  log.RecordId = material.Id;
        //  log.TableOperation = warehouseEntities.TableOperations.First(p => p.Id == 1);
        //  log.Description = "Nowy materiał został dodany";

        //  log.Person = warehouseEntities.People.First(p => p.Id == userId);

        //  log.TimeStamp = DateTime.Now;
        //}

        public void OpeningBalance(int warehouseTargetId, int authorId, List<PositionWithShelf> positionsWithShelves, int? contractingPartyId, bool addOnly = true)
        {
#if DEBUG
            Contract.Requires(positionsWithShelves != null);
#endif
            var posWithQuantAndShelves = new List<PositionWithQuantityAndShelfs>();

            foreach (var positionWithShelf in positionsWithShelves)
            {
                PositionWithQuantityAndShelfs positionWithQuantityAndShelfs; // = new PositionWithQuantityAndShelfs();

                int? orderId = positionWithShelf.order != null ? positionWithShelf.order.Id : (int?)null;

                // Checking warehouse stacks and choosing whether to add or remove quantities
                var matchingWarehouseStacks = warehouseEntities.CurrentStackInDocuments.Where(p =>
                                                                                              p.MaterialId == positionWithShelf.position.Material.Id &&
                                                                                              ((!orderId.HasValue && !p.OrderId.HasValue) || ((orderId.HasValue && p.OrderId.HasValue && p.OrderId == orderId))) &&
                                                                                              p.WarehouseTargetId == warehouseTargetId &&
                                                                                              p.ShelfId == positionWithShelf.shelf.Id);

                var sumQuantityForGivenPlace = matchingWarehouseStacks.Sum(p => (decimal?)p.Quantity);

                if (!sumQuantityForGivenPlace.HasValue)
                {
                    sumQuantityForGivenPlace = 0;
                }

                var quantityToBeBalanced = addOnly
                  ? (decimal)positionWithShelf.position.Quantity
                  : (decimal)positionWithShelf.position.Quantity - sumQuantityForGivenPlace.Value;

                //if (Math.Abs(quantityToBeBalanced) > 0)
                //{
                if (!addOnly)
                    positionWithShelf.position.Quantity = (double)quantityToBeBalanced;

                if (quantityToBeBalanced < 0)
                {
                    // Collect source positions
                    positionWithQuantityAndShelfs = CollectPositionsFromStacks(matchingWarehouseStacks, Math.Abs(quantityToBeBalanced)); //warn: this probably looses information about WarehousePositionZone.Zone
                }
                else
                {
                    positionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
                    if (positionWithShelf.shelf != null)
                    {
                        WarehousePositionZone warehousePositionZone = new WarehousePositionZone();
                        warehousePositionZone.Zone = positionWithShelf.shelf;
                        warehousePositionZone.Quantity = positionWithShelf.position.Quantity;
                        positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();
                        positionWithQuantityAndShelfs.WarehousePositionZones.Add(warehousePositionZone);
                    }
                }

                positionWithQuantityAndShelfs.position = positionWithShelf.position;
                positionWithQuantityAndShelfs.order = positionWithShelf.order;

                posWithQuantAndShelves.Add(positionWithQuantityAndShelfs);
                //}
                //else
                //{
                //  throw new Exception("Cannot balance zero quantity");
                //}
            }

            //Warehouse warehouseTarget = warehouseEntities.Warehouses.First(p => p.Id == warehouseTargetId);
            //Person person = warehouseEntities.People.First(p => p.Id == authorId);

            CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseTargetId, warehouseTargetId, authorId, posWithQuantAndShelves, WarehouseDocumentTypeEnum.BO, DateTime.Now, false); //, contractingPartyId);
        }

        //public void OpeningBalance(int warehouseTargetId, int authorId, List<WarehouseDocumentPosition> warehouseDocumentPositions, int? contractingPartyId, int? orderId)
        //{
        //  if (orderId != null) throw new NotImplementedException("OrderId is not implementet yet");
        //  Warehouse warehouseTarget = warehouseEntities.Warehouses.First(p => p.Id == warehouseTargetId);
        //  Person person = warehouseEntities.People.First(p => p.Id == authorId);

        //  //WarehouseOperation(null, warehouseTarget, person, warehouseDocumentPositions, WarehouseDocumentTypeEnum.BO, contractingPartyId);
        //}

        //public WarehouseDocument OrderMaterialFrom(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int? orderId)
        //{
        //  return OrderMaterialFrom(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, orderId, DateTime.Now);
        //}

        public WarehouseDocument OrderMaterialFrom(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, int? orderId, DateTime date)
        {
            //Warehouse warehouseSource;
            //Warehouse warehouseTarget;
            //Person person;
            WarehouseDocument orderDocument;

            //CheckMaterialIsForOrder()

            WarehouseDocumentTypeEnum warehouseDocumentType = WarehouseDocumentTypeEnum.Za;

            using (TransactionScope transactionScope = new TransactionScope())
            {
                Hashtable usedPositions = new Hashtable();

                foreach (var positionWQAS in positionWithQuantityAndShelfses)
                {
                    if (usedPositions.ContainsKey(positionWQAS.position.Id))
                    {
                        throw new DataException("Data is not allign");
                    }
                    usedPositions.Add(positionWQAS.position.Id, null);
                }

                foreach (PositionWithQuantityAndShelfs positionWQAS in positionWithQuantityAndShelfses)
                {
                    var currentStack = GetCurrentStackInDocuments("", "", warehouseSourceId);

                    double possibleToUsedQuantity = 0;

                    foreach (ChildParentPositionQuantity cppq in positionWQAS.positionParentIds)
                    {
                        var currentStackForPostion = currentStack.First(c => c.PositionId == cppq.PositionParentID);

                        possibleToUsedQuantity += (double)currentStackForPostion.Quantity;
                    }

                    if ((decimal)possibleToUsedQuantity < (decimal)Math.Abs(positionWQAS.position.Quantity)) return null;
                }

                //TranslateParameters(warehouseSourceId, warehouseTargetId, authorId, out warehouseSource, out warehouseTarget, out person);

                orderDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses,
                                                                     warehouseDocumentType, date, false);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return orderDocument;
        }

        public void PlaceAnswerForQuestion(string answerToken)
        {
            //return false;
            using (TransactionScope transactionScope = new TransactionScope())
            {
                Guid ansGuid = Guid.Parse(answerToken);
                warehouseEntities.QuestionAnswerPlace(ansGuid);
                transactionScope.Complete();
            }
        }

        /// <summary>
        /// Checking if positions are equal for grouping NOT ABSOLUTELY
        /// </summary>
        /// <param name="position1"></param>
        /// <param name="position2"></param>
        /// <returns></returns>
        public bool PositionsAreEqual(WarehouseDocumentPosition position1, WarehouseDocumentPosition position2)
        {
#if DEBUG
            Contract.Requires(position1 != null);
            Contract.Requires(position2 != null);
#endif

            if (position1.Material.Id == position2.Material.Id
                &&
                position1.IsDamaged == position2.IsDamaged
                &&
                position1.IsGarbage == position2.IsGarbage
                &&
                position1.IsPartlyDamaged == position2.IsPartlyDamaged
                &&
                position1.IsZZ == position2.IsZZ
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the position status with the given one
        /// </summary>
        /// <param name="position">Position to check</param>
        /// <param name="status">Status to compare with</param>
        /// <returns></returns>
        public bool PositionStatusIs(WarehouseDocumentPosition position, PositionFeaturedStatus status)
        {
            // New technique (status flags):
            if (status == PositionFeaturedStatus.Good && position.Warehouse == null)
            {
                return true;
            }
            if (status == PositionFeaturedStatus.Damaged && position.IsDamaged)
            {
                return true;
            }
            if (status == PositionFeaturedStatus.DamagedFromDamaged && position.IsZZ) //position.Warehouse != null && (position.IsZZ == null ? false : (position.IsZZ == true ? true : false)))
            {
                return true;
            }
            if (status == PositionFeaturedStatus.PartlyDamaged && position.IsPartlyDamaged) // == null ? false : (position.IsGarbage==true ? true : false )))
            {
                return true;
            }
            if (status == PositionFeaturedStatus.Garbage && position.IsGarbage) // == null ? false : (position.IsGarbage==true ? true : false )))
            {
                return true;
            }

            if (status == PositionFeaturedStatus.Package && position.Material != null && position.Material.IsPackage)
            {
                return true;
            }
            return false;

            //// Old technique ("data-mining"):
            //var dictionary = new Dictionary<PositionFeaturedStatus, string>
            //                                                          {
            //                                                              {PositionFeaturedStatus.Good, "^[^C]$"},
            //                                                              {PositionFeaturedStatus.Damaged,"^[A-Z]*C[^B]*$"},
            //                                                              {PositionFeaturedStatus.DamagedFromDamaged,"^[A-Z]*C[A-Z]*[^B][A-Z]*C[A-Z]*$"},
            //                                                              {PositionFeaturedStatus.Package, "^[A-Z]*Q[A-Z]*$"},
            //                                                              {PositionFeaturedStatus.Garbage, "^[A-Z]*V[A-Z]*$"}
            //                                                          };

            //var pathsBunch = new List<List<PositionNode>>();

            //GetPositionGenealogy(position, pathsBunch, null, null);

            //try
            //{
            //    if (RegExCheckPositionBunch(pathsBunch, dictionary[status], true).Count() > 0)
            //    {
            //        return true;
            //    }
            //}
            //catch (KeyNotFoundException)
            //{
            //    throw new Exception("Invalid status code");
            //}

            //return false;
        }

        ///// <summary>
        ///// Checking if positions wqas are equal for grouping NOT ABSOLUTELY
        ///// </summary>
        ///// <param name="position1"></param>
        ///// <param name="position2"></param>
        ///// <returns></returns>
        //public bool PositionWithQuantityAndShelfsAreEqual(PositionWithQuantityAndShelfs positionWithQuantityAndShelfses1, PositionWithQuantityAndShelfs positionWithQuantityAndShelfses2)
        //{
        //  if (PositionsAreEqual(positionWithQuantityAndShelfses1.position, positionWithQuantityAndShelfses2.position))
        //  {
        //    if ((positionWithQuantityAndShelfses1.order == null && positionWithQuantityAndShelfses2.order == null)
        //        ||
        //        (
        //        positionWithQuantityAndShelfses1.order != null
        //        &&
        //        positionWithQuantityAndShelfses2.order != null
        //        &&
        //        positionWithQuantityAndShelfses1.order.Id == positionWithQuantityAndShelfses2.order.Id))
        //    {
        //      return true;
        //    }
        //    else
        //    {
        //      return false;
        //    }
        //  }
        //  else
        //  {
        //    return false;
        //  }
        //}

        //public WarehouseDocument ReceiveReturn(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId, bool exportDocToSIMPLE = true)
        //{
        //  return ReceiveReturn(givenDocumentId, positionsList, userId, DateTime.Now.Date, exportDocToSIMPLE);
        //}

        public WarehouseDocument ReceiveReturn(int givenDocumentId, List<WarehouseDocumentPosition> positionsList, int userId, DateTime date, bool exportDocToSIMPLE = true) // orderCheck=false
        {
            WarehouseDocument warehouseDocumentReceived = null;

            var warehouseDocument =
                warehouseEntities.WarehouseDocuments
                .Include("WarehouseDocumentPosition")
                .Include("WarehouseDocumentPosition.Material")
                .Include("Warehouse")
                .Include("Warehouse1")
                .Include("WarehouseDocumentPosition.Order")
                .First(d => d.Id == givenDocumentId);

            var warehouseDocumentPositions = warehouseDocument.WarehouseDocumentPositions.ToArray();
            var positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();

            foreach (var position in warehouseDocumentPositions)
            {
                var warehouseDocumentPositionTemp =
                    CreateWarehouseDocumentPositionDataOnly(position.Material.Id,
                                                            position.UnitPrice,
                                                            (decimal)-position.Quantity,
                                                            position.Id,
                                                            0); //position.Order != null ? position.Order.Id : 0);

                List<WarehousePositionZone> warehousePositionZone = positionsList != null
                                                                        ? new List<WarehousePositionZone>(
                                                                              positionsList.First(p => p.Id == position.Id).WarehousePositionZones)
                                                                        : null;

                positionWithQuantityAndShelfses.Add(new PositionWithQuantityAndShelfs()
                {
                    position = warehouseDocumentPositionTemp,
                    WarehousePositionZones = warehousePositionZone,
                    documentParentIds = new List<int>() { warehouseDocument.Id },
                    positionParentIds = new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { PositionParentID = position.Id, Quantity = (decimal)-position.Quantity } }
                });
            }

            using (var transactionScope = new TransactionScope())
            {
                // get target dept simple

                warehouseDocumentReceived = ReceiveReturn(warehouseDocument.Warehouse.Id, warehouseDocument.Warehouse1.Id,
                                                                            userId, positionWithQuantityAndShelfses, date, exportDocToSIMPLE);
                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocumentReceived;
        }

        //public WarehouseDocument ReceiveReturn(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, bool exportDocToSIMPLE = true)
        //{
        //  return ReceiveReturn(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, DateTime.Now, exportDocToSIMPLE);
        //}

        public WarehouseDocument ReceiveReturn(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, DateTime date, bool exportDocToSIMPLE = true)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
#endif
            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource;
            //Warehouse warehouseTarget;
            //Person person;

            int? targetDeptSimple = 20;
            int? targetOrderId = null;
            int? dokSubtype = 8;

            var warehouseDocumentType = WarehouseDocumentTypeEnum.ZwPlus;

            if (warehouseDocumentType != WarehouseDocumentTypeEnum.ZwPlus) throw new ArgumentException("Parameter can be ZwPlus only", "warehouseDocumentType");

            foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                if (positionWithQuantityAndShelf.position.Quantity <= 0) throw new ArgumentException("For ZwPlus quantity must be greater than 0");
            }

            //TranslateParameters(warehouseSourceId, warehouseTargetId, authorId, out warehouseSource, out warehouseTarget, out person);

            using (var transactionScope = new TransactionScope())
            {
                warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseTargetId, authorId, positionWithQuantityAndShelfses, warehouseDocumentType, date, exportDocToSIMPLE, targetDeptSimple, targetOrderId, dokSubtype);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        /// <summary>
        /// Returns a bunch of position paths which match the given regular expression
        /// </summary>
        /// <param name="positionBunch"></param>
        /// <param name="expression"></param>
        /// <param name="firstOnly">Only first match will be returned in the list (improves performance)</param>
        /// <returns></returns>
        public List<List<PositionNode>> RegExCheckPositionBunch(List<List<PositionNode>> positionBunch, string expression, bool firstOnly)
        {
#if DEBUG
            Contract.Requires(positionBunch != null);
#endif
            List<List<PositionNode>> matches = new List<List<PositionNode>>();
            Regex regex;
            for (var i = 0; i < positionBunch.Count; i++)
            {
                List<PositionNode> positionPath = positionBunch[i];
                string stringToCheck = ConvertPositionGraphPathToMarkupString(positionPath);
                regex = new Regex(@expression);
                if (regex.IsMatch(stringToCheck))
                {
                    matches.Add(positionPath);
                    if (firstOnly)
                    {
                        return matches;
                    }
                }
            }
            return matches;
        }

        public string RelocateMaterialInWarehouseWithOutNewDocument(int warehouseId, int materialId, int orderId, int? processIdpl, int zoneSrcId, int zoneDstId, double quantity, int operatorId, bool IsOrder = false)
        {
            Zone zone1 = GetShelfByIdDataOnly(zoneSrcId);
            Zone zone2 = GetShelfByIdDataOnly(zoneDstId);

            Material material = GetMaterialById(materialId);

            if (zone1 == null)
                return "Brak lokalizacji źródłowej";
            if (zone2 == null)
                return "Brak lokalizacji docelowej";
            if (material == null)
                return "Brak materiału";

            try
            {

                var position = warehouseEntities.WarehouseDocumentPositions
                    .Include(y => y.WarehouseDocument)
                    .Include(z => z.WarehouseDocumentPositionRelations)
                    .Where(x => 
                        (x.WarehouseDocument.WarehouseTargetId == warehouseId || x.WarehouseDocument.WarehouseSourceId == warehouseId)
                        && x.MaterialId == materialId
                        && x.WarehouseDocument.TimeDeleted == null
                        && x.OrderId == orderId
                        && x.ProcessIdpl == processIdpl).Select(x => new
                    {
                        Quantity = Math.Abs(x.Quantity) - (x.WarehouseDocumentPositionRelations.Any() ? x.WarehouseDocumentPositionRelations.Sum(z => z.Quantity) : 0),
                        Id = x.Id
                    }).ToList();


                var id = position.Where(x=>x.Quantity == quantity).Select(x=>x.Id).FirstOrDefault();

                var wdpr = warehouseEntities.WarehouseDocumentPositionRelations
                    .Where(x => x.ParentId == id).FirstOrDefault();

                int parentId=0;
                if (wdpr != null)
                {
                    wdpr.ParentZoneId = zone2.Id;
                    //parentId = warehouseEntities.WarehouseDocumentPositionRelations
                    //    .Where(x => x.ChildId == wdpr.ChildId)
                    //    .OrderByDescending(x=>x.ChildId)
                    //    .Select(x => x.ParentId)
                    //    .FirstOrDefault();
                        
                }

                
                var wpz = warehouseEntities.WarehousePositionZones
                    .Where(x => x.PositionId == id || x.PositionId == parentId);

                foreach(var poz in wpz)
                {
                    poz.ZoneId = zone2.Id;
                }
                

                warehouseEntities.SaveChanges();
                warehouseEntities.AcceptAllChanges();

                return "Przeniesiono " + quantity + "" + material.Unit.ShortName + " materiału " +
                       material.IndexSIMPLE + " z lokalizacji " + zone1.Name + " do " + zone2.Name;
            }
            catch (Exception ex)
            {
                return "Wystąpił błąd w trakcie zmiany lokalizacji: " + (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }

        public string RelocateMaterialInWarehouse(int warehouseId, int materialId, int orderId, int? processIdpl, int zoneSrcId, int zoneDstId, double quantity, int operatorId, bool IsOrder = false)
        {
            Zone zone1 = GetShelfByIdDataOnly(zoneSrcId);
            Zone zone2 = GetShelfByIdDataOnly(zoneDstId);

            Material material = GetMaterialById(materialId);

            if (zone1 == null)
                return "Brak lokalizacji źródłowej";
            if (zone2 == null)
                return "Brak lokalizacji docelowej";
            if (material == null)
                return "Brak materiału";

            try
            {
                if (IsOrder)
                {
                    System.Data.Objects.ObjectParameter warehouseDocumentId = new System.Data.Objects.ObjectParameter("docId", typeof(global::System.Int32));
                    
                    var documentId = warehouseEntities
                        .spdWarehouseRelocateMaterialForOrderedDocs(
                            warehouseId, materialId, orderId, processIdpl, zoneSrcId, zoneDstId, quantity, false, false, false, false, null, operatorId, warehouseDocumentId);


                    int docId;

                    if (int.TryParse(warehouseDocumentId.Value.ToString(), out docId))
                    {
                        var relationDoc = warehouseEntities.WarehouseDocumentRelations.Where(x => x.ChildId == docId).FirstOrDefault();
                        var childDoc = warehouseEntities.WarehouseDocuments.Where(x => x.Id == relationDoc.ChildId).FirstOrDefault();
                        var parentDoc = warehouseEntities.WarehouseDocuments.Where(x => x.Id == relationDoc.ParentId).FirstOrDefault();

                        childDoc.DocumentTypeId = parentDoc.DocumentTypeId;
                        childDoc.TimeStamp = parentDoc.TimeStamp;
                        childDoc.Date = parentDoc.Date;
                        childDoc.DueTime = parentDoc.DueTime;
                        childDoc.ProductionMachineScheduleItemId = parentDoc.ProductionMachineScheduleItemId;
                        childDoc.ProductElementStructureId = parentDoc.ProductElementStructureId;

                        /** 
                       
                        parentDoc.IsVisible = false;
                        parentDoc.TimeDeleted = DateTime.Now;
                        
                        **/

                        warehouseEntities.SaveChanges();
                        warehouseEntities.AcceptAllChanges();
                    }
                }
                else
                    warehouseEntities.RelocateMaterialInWarehouse(warehouseId, materialId, orderId, processIdpl, zoneSrcId, zoneDstId,
                                                              quantity, false, false, false, false, null, operatorId);

                return "Przeniesiono " + quantity + "" + material.Unit.ShortName + " materiału " +
                       material.IndexSIMPLE + " z lokalizacji " + zone1.Name + " do " + zone2.Name;
            }
            catch (Exception ex)
            {
                return "Wystąpił błąd w trakcie zmiany lokalizacji: " + (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }

        //public WarehouseDocument RemoveMaterial(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses)
        //{
        //  return RemoveMaterial(warehouseSourceId, authorId, positionWithQuantityAndShelfses, DateTime.Now);
        //}

        public WarehouseDocument RemoveMaterial(int warehouseSourceId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, DateTime date)
        {
#if DEBUG
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses != null);
            Contract.Requires<NullReferenceException>(positionWithQuantityAndShelfses.Any());
            Contract.Requires<DataException>(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null && pwqas.position.Quantity < 0), "Quantity of all positions must be negative i.e. less than 0");
#endif
            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource = null;
            //Person person = null;

            foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                if (positionWithQuantityAndShelf.position.Quantity >= 0)
                    throw new ArgumentException("For LM quantity must be less than 0");
            }

            //TranslateParameters(warehouseSourceId, null, authorId, out warehouseSource, out warehouseSource, out person);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, null, authorId, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.LM, date);
                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        public void RemoveSystemUserGroupMembership(int membershipId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseEntities.DeleteObject(warehouseEntities.SystemUserGroupMemberships.First(m => m.Id == membershipId));

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void RemoveWarehouseGroupMembership(int membershipId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseEntities.DeleteObject(warehouseEntities.WarehouseGroupMemberships
                  .First(m => m.Id == membershipId));

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void RemoveWarehouseGroupPermission(int permissionId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                //warehouseEntities.DeleteObject(warehouseEntities.WarehouseGroupPermissions.First(p => p.Id == permissionId));

                var permissions = GetWarehouseGroupPermissions();

                var permission = permissions.FirstOrDefault(p => p.Id == permissionId);
                permission.RevokeTime = DateTime.Now;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void RemoveWarehousePermission(int permissionId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseEntities.DeleteObject(warehouseEntities.WarehousePermissions.First(p => p.Id == permissionId));

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SaveNewPackageType(PackageType packageType)
        {
            WarehouseEntities warehouseEntities2 = new WarehouseEntities();

            warehouseEntities2.AddToPackageTypes(packageType);
            warehouseEntities2.SaveChanges();
        }

        public void SecurityAddGroupPriviledgeForExecution(int? fromWarehouseId, int? toWarehouseId, int operationId, int systemGroupId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                bool operationAvailability = false;
                using (TransactionScope transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    operationAvailability = SecurityCheckGroupPriviledgeForExecution(fromWarehouseId, toWarehouseId, operationId, systemGroupId);

                    transactionScope2.Complete();
                }

                // Check if the connection already exists
                if (!operationAvailability)
                {
                    // If not, create a connection
                    //WarehousePermissions wp = new WarehousePermissions();
                    //wp.Warehouse = fromWarehouseId != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    //wp.Warehouse1 = toWarehouseId != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    //wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);
                    var wp = new WarehouseSecurityGroupPrivilegeForExecution();

                    wp.FromWarehouseId = fromWarehouseId;// != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    wp.ToWarehouseId = toWarehouseId;// != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);
                    wp.GrantTime = DateTime.Now;
                    wp.SystemGroup = GetSystemGroupById(systemGroupId);
                }

                var groupMemberUserIds =
                  warehouseEntities.SystemUserGroupMemberships
                    .Where(m => m.SystemGroupId == systemGroupId)
                    .Select(p => p.SystemUserId);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityAddGroupPriviledgeForRead(int warehouseId, int systemGroupId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                bool privilege = false;
                using (TransactionScope transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    privilege = SecurityCheckGroupPriviledgeForRead(warehouseId, systemGroupId);

                    transactionScope2.Complete();
                }

                // Check if the connection already exists
                if (!privilege)
                {
                    WarehouseSecurityGroupPrivilegeForRead wp = new WarehouseSecurityGroupPrivilegeForRead();

                    wp.Warehouse = GetWarehouseById(warehouseId);
                    wp.SystemGroup = GetSystemGroupById(systemGroupId);
                    wp.GrantTime = DateTime.Now;
                }

                var groupMemberUserIds =
                    from p in
                        warehouseEntities.SystemUserGroupMemberships.Include("SystemGroup").Include("SystemUser").Where(m => m.SystemGroup.Id == systemGroupId)
                    select p.SystemUser.Id;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityAddUserPriviledgeForExecution(int? fromWarehouseId, int? toWarehouseId, int operationId, int systemUserId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                bool privilege = false;
                using (TransactionScope transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    privilege = SecurityCheckUserPriviledgeForExecution(fromWarehouseId, toWarehouseId, operationId, systemUserId);

                    transactionScope2.Complete();
                }

                // Check if the connection already exists
                if (!privilege)
                {
                    // If not, create a connection
                    //WarehousePermissions wp = new WarehousePermissions();
                    //wp.Warehouse = fromWarehouseId != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    //wp.Warehouse1 = toWarehouseId != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    //wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);
                    WarehouseSecurityPrivilegeForExecution wp = new WarehouseSecurityPrivilegeForExecution();

                    wp.Warehouse = fromWarehouseId != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    wp.Warehouse1 = toWarehouseId != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);
                    wp.GrantTime = DateTime.Now;
                    //wp.SystemUser = G
                    wp.SystemUser = GetSystemUserById(systemUserId);
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityAddUserPriviledgeForRead(int warehouseId, int systemUserId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                bool privilege = false;
                using (TransactionScope transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    privilege = SecurityCheckUserPriviledgeForRead(warehouseId, systemUserId);

                    transactionScope2.Complete();
                }

                // Check if the connection already exists
                if (!privilege)
                {
                    WarehouseSecurityPrivilegeForRead wp = new WarehouseSecurityPrivilegeForRead();

                    wp.Warehouse = GetWarehouseById(warehouseId);
                    wp.SystemUser = GetSystemUserById(systemUserId);
                    wp.GrantTime = DateTime.Now;
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public bool SecurityCheckGroupPriviledgeForExecution(int? fromWarehouseId, int? toWarehouseId, int operationId, int systemGroupId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityGroupPrivilegeForExecutions
                               .Include(wsgrfe => wsgrfe.Warehouse)
                               .Include(wsgrfe => wsgrfe.WarehouseDocumentType)
                               .Include(wsgrfe => wsgrfe.SystemGroup)
                             //.AddIncludePaths("Warehouse", "WarehouseDocumentType", "SystemGroup") //.Include("Warehouse").Include("WarehouseDocumentType").Include("SystemGroup")
                             where
                             !p.RevokeTime.HasValue
                             &&
                             ((fromWarehouseId == 0) || ((p.Warehouse == null || p.Warehouse.Id == 0) && fromWarehouseId == null) || ((p.Warehouse != null && p.Warehouse.Id > 0) && p.Warehouse.Id == fromWarehouseId))
                             &&
                             ((toWarehouseId == 0) || ((p.Warehouse1 == null || p.Warehouse1.Id == 0) && toWarehouseId == null) || ((p.Warehouse1 != null && p.Warehouse1.Id > 0) && p.Warehouse1.Id == toWarehouseId))
                             &&
                             p.WarehouseDocumentType.Id == operationId
                             &&
                             (p.SystemGroup != null && p.SystemGroup.Id == systemGroupId)
                             select p;

            return privileges.Count() > 0;
        }

        public bool SecurityCheckGroupPriviledgeForExecutionById(int privilegeId)
        {
            var priviledges = from p in warehouseEntities.WarehouseSecurityGroupPrivilegeForExecutions
                              where
                              !p.RevokeTime.HasValue
                              &&
                              p.Id == privilegeId
                              &&
                              p.SystemGroup != null
                              select p;

            return priviledges.Count() > 0;
        }

        public bool SecurityCheckGroupPriviledgeForRead(int warehouseId, int systemGroupId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityGroupPrivilegeForReads
                               .Include(wsgrfr => wsgrfr.Warehouse)
                               .Include(wsgrfr => wsgrfr.SystemGroup)
                             //.AddIncludePaths("Warehouse", "SystemGroup") //.Include("Warehouse").Include("SystemGroup")
                             where
                             !p.RevokeTime.HasValue
                             &&
                             p.Warehouse.Id == warehouseId
                             &&
                             p.SystemGroup.Id == systemGroupId
                             select p;

            return privileges.Count() > 0;
        }

        public bool SecurityCheckGroupPriviledgeForReadById(int privilegeId)
        {
            var priviledges = from p in warehouseEntities.WarehouseSecurityGroupPrivilegeForReads
                              where
                              !p.RevokeTime.HasValue
                              &&
                              p.Id == privilegeId
                              &&
                              p.SystemGroup != null
                              select p;

            return priviledges.Count() > 0;
        }

        public bool SecurityCheckUserPriviledgeForExecution(int? fromWarehouseId, int? toWarehouseId, int operationId, int systemUserId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityPrivilegeForExecutions
                               .Include(wspr => wspr.Warehouse)
                               .Include(wspr => wspr.WarehouseDocumentType)
                               .Include(wspr => wspr.SystemUser)// .AddIncludePaths("Warehouse", "WarehouseDocumentType", "SystemUser") //.Include("Warehouse").Include("WarehouseDocumentType").Include("SystemUser")
                             where
                             !p.RevokeTime.HasValue
                             &&
                             ((fromWarehouseId == 0) || ((p.Warehouse == null || p.Warehouse.Id == 0) && fromWarehouseId == null) || ((p.Warehouse != null && p.Warehouse.Id > 0) && p.Warehouse.Id == fromWarehouseId))
                             &&
                             ((toWarehouseId == 0) || ((p.Warehouse1 == null || p.Warehouse1.Id == 0) && toWarehouseId == null) || ((p.Warehouse1 != null && p.Warehouse1.Id > 0) && p.Warehouse1.Id == toWarehouseId))
                             &&
                             p.WarehouseDocumentType.Id == operationId
                             &&
                             (p.SystemUser != null && p.SystemUser.Id == systemUserId)
                             select p;

            return privileges.Count() > 0;
        }

        public bool SecurityCheckUserPriviledgeForExecutionById(int privilegeId)
        {
            var priviledges = from p in warehouseEntities.WarehouseSecurityPrivilegeForExecutions
                              where
                              !p.RevokeTime.HasValue
                              &&
                              p.Id == privilegeId
                              &&
                              p.SystemUser != null
                              select p;

            return priviledges.Count() > 0;
        }

        public bool SecurityCheckUserPriviledgeForRead(int warehouseId, int systemUserId)
        {
            var privileges = from p in warehouseEntities.WarehouseSecurityPrivilegeForReads
                               .Include(wspr => wspr.Warehouse)
                               .Include(wspr => wspr.SystemUser)//.AddIncludePaths("Warehouse", "SystemUser") //.Include("Warehouse").Include("SystemUser")
                             where
                             !p.RevokeTime.HasValue
                             &&
                             p.Warehouse.Id == warehouseId
                             &&
                             p.SystemUser.Id == systemUserId
                             select p;

            return privileges.Count() > 0;
        }

        public bool SecurityCheckUserPriviledgeForReadById(int privilegeId)
        {
            var priviledges = from p in warehouseEntities.WarehouseSecurityPrivilegeForReads
                              where
                              !p.RevokeTime.HasValue
                              &&
                              p.Id == privilegeId
                              &&
                              p.SystemUser != null
                              select p;

            return priviledges.Count() > 0;
        }

        public void SecurityRemoveGroupPriviledgeForExecution(int privilegeId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                WarehouseSecurityGroupPrivilegeForExecution wsgpfe =
                    warehouseEntities.WarehouseSecurityGroupPrivilegeForExecutions.Include("SystemGroup").First(g => g.Id == privilegeId);
                wsgpfe.RevokeTime = DateTime.Now;

                //warehouseEntities.DeleteObject(warehouseEntities.WarehouseSecurityGroupPrivilegeForExecution.First(g => g.Id == privilegeId));

                var groupMemberUserIds =
                    from p in
                        warehouseEntities.SystemUserGroupMemberships.Include("SystemGroup").Include("SystemUser").Where(m => m.SystemGroup.Id == wsgpfe.SystemGroup.Id)
                    select p.SystemUser.Id;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityRemoveGroupPriviledgeForRead(int privilegeId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                //warehouseEntities.DeleteObject(warehouseEntities.WarehouseSecurityGroupPrivilegeForRead.First(p => p.Id == privilegeId));

                WarehouseSecurityGroupPrivilegeForRead wsgpfe =
                    warehouseEntities.WarehouseSecurityGroupPrivilegeForReads
                    .Include("SystemGroup")
                    .First(p => p.Id == privilegeId);
                wsgpfe.RevokeTime = DateTime.Now;

                var groupMemberUserIds =
                    from p in
                        warehouseEntities.SystemUserGroupMemberships
                          .Include("SystemGroup")
                          .Include("SystemUser")
                          .Where(m => m.SystemGroup.Id == wsgpfe.SystemGroup.Id)
                    select p.SystemUser.Id;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityRemoveUserPriviledgeForExecution(int? fromWarehouseId, int? toWarehouseId, int operationId, int systemUserId)
        {
            throw new Exception("Strange code - check it!");
            using (var transactionScope = new TransactionScope())
            {
                // Check if the connection already exists
                if (!SecurityCheckUserPriviledgeForExecution(fromWarehouseId, toWarehouseId, operationId, systemUserId))
                {
                    // If not, create a connection
                    //WarehousePermissions wp = new WarehousePermissions();
                    //wp.Warehouse = fromWarehouseId != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    //wp.Warehouse1 = toWarehouseId != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    //wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);
                    WarehouseSecurityPrivilegeForExecution wp = new WarehouseSecurityPrivilegeForExecution();

                    wp.Warehouse = fromWarehouseId != null ? GetWarehouseById(fromWarehouseId.Value) : null;
                    wp.Warehouse1 = toWarehouseId != null ? GetWarehouseById(toWarehouseId.Value) : null;
                    wp.WarehouseDocumentType = GetDocumentTypeList("").First(p => p.Id == operationId);
                    wp.GrantTime = DateTime.Now;
                    //wp.SystemUser = G
                    wp.SystemUser = GetSystemUserById(systemUserId);
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityRemoveUserPriviledgeForExecution(int privilegeId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                //warehouseEntities.DeleteObject(warehouseEntities.WarehouseSecurityPrivilegeForExecution.First(p => p.Id == privilegeId));

                WarehouseSecurityPrivilegeForExecution wspfe =
                    warehouseEntities.WarehouseSecurityPrivilegeForExecutions
                      .Include("SystemUser")
                      .First(p => p.Id == privilegeId);
                wspfe.RevokeTime = DateTime.Now;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SecurityRemoveUserPriviledgeForRead(int privilegeId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                //warehouseEntities.DeleteObject(warehouseEntities.WarehouseSecurityPrivilegeForRead.First(p => p.Id == privilegeId));
                WarehouseSecurityPrivilegeForRead wspfe =
                    warehouseEntities.WarehouseSecurityPrivilegeForReads
                      .Include("SystemUser").First(p => p.Id == privilegeId);
                wspfe.RevokeTime = DateTime.Now;

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SetFormState(string formName, WarehouseDocument.WarehouseDocumentEditStateEnum state)
        {
            HttpContext.Current.Session["WDFormEditState_" + formName] = state;
        }

        public void SetSystemUserSetting(string key, string value, int systemUserId)
        {
#if DEBUG
            Contract.Requires(value != null);
#endif
            // Check if setting already exists
            bool settingExists = GetSystemUserSetting(key, systemUserId) != null;

            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (value.Length > 0)
                {
                    // Add or change setting
                    if (settingExists)
                    {
                        // Change
                        var sus = GetSystemUserSetting(key, systemUserId);
                        sus.SettingValue = value;
                    }
                    else
                    {
                        // Add
                        var sus = new Mag.Domain.Model.SystemUserSetting();
                        sus.SettingKey = key;
                        sus.SettingValue = value;
                        sus.SystemUser = GetSystemUserById(systemUserId);
                        sus.SettingTime = DateTime.Now;
                    }
                }
                else
                {
                    if (settingExists)
                    {
                        // Remove setting
                        warehouseEntities.DeleteObject(warehouseEntities.SystemUserSettings.First(s => s.SettingKey == key && s.SystemUser.Id == systemUserId));
                    }
                }
                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public void SetUserRecentWarehouseInfo(int userId, int warehouseId)
        {
            var systemUser = warehouseEntities.SystemUsers.FirstOrDefault(p => p.Id == userId);
            systemUser.RecentWarehouseId = warehouseId;
            warehouseEntities.SaveChanges();
        }

        public void UpdateDamageDescription(WarehouseDocumentPosition position, string damageDescription)
        {
#if DEBUG
            Contract.Requires(damageDescription != null);
            Contract.Requires(position != null);
#endif
            WarehouseDocumentPosition warehouseDocumentPosition = warehouseEntities.WarehouseDocumentPositions.First(p => p.Id == position.Id);

            WarehouseDocumentPositionExtension warehouseDocumentPositionExtension = new WarehouseDocumentPositionExtension();

            warehouseDocumentPositionExtension.WarehouseDocumentPosition = warehouseDocumentPosition;
            warehouseDocumentPositionExtension.Info = "<document><damageDescription>" + ToSafeSqlXmlString(damageDescription) + "</damageDescription></document>";

            warehouseEntities.SaveChanges();
            warehouseEntities.AcceptAllChanges();
        }

        public void UpdateInvoiceNumber(WarehouseDocument document, string invoiceNumber)
        {
#if DEBUG
            Contract.Requires(document != null);
            Contract.Requires(invoiceNumber != null);
#endif
            var warehouseDocument = warehouseEntities.WarehouseDocuments.First(d => d.Id == document.Id);

            var warehouseDocumentExtension = new WarehouseDocumentExtension();

            warehouseDocumentExtension.WarehouseDocument = warehouseDocument;
            warehouseDocumentExtension.Info = "<document><invoiceNumber>" + ToSafeSqlXmlString(invoiceNumber) + "</invoiceNumber></document>";

            warehouseEntities.SaveChanges();
            warehouseEntities.AcceptAllChanges();
        }

        public void UpdateSupplyTables(WarehouseDocumentPosition warehouseDocumentPosition, int contractingPartyIdSimple, int companyId)
        {
            // TODO: Remove this return after testing and ending with K2
            return;

            //double positionsQuantity = warehouseEntities.WarehouseDocumentPosition.Where(p => p.Order.Id == warehouseDocumentPosition.Order.Id).Sum(s => s.Quantity);

            //WaitingForSupply waiting =
            //    FindRecordInWaitingForSupply(
            //        warehouseDocumentPosition.Order.ProcessHeaderId != null ? (int)warehouseDocumentPosition.Order.ProcessHeaderId : 0,
            //        warehouseDocumentPosition.Order.ProcessIDPL != null ? (int)warehouseDocumentPosition.Order.ProcessIDPL : 0,
            //        contractingPartyIdSimple,
            //        warehouseDocumentPosition.Material.MaterialSIMPLE.First().IdSIMPLE,
            //        companyId,
            //        warehouseDocumentPosition.Order.OrderNumberPZ,
            //        warehouseDocumentPosition.Order.ForDepartment, positionsQuantity);

            //// TODO: Check whether it is correct to do it from ReceiveMaterial.cs
            //if (waiting.IloscZam <= 0)
            //{
            //    bool changes = false;

            //    if (waiting.Flaga == 1)
            //    {
            //        KL_Wyceny_Karta_zak klWycenyKartaZak = warehouseEntities.KL_Wyceny_Karta_zak.FirstOrDefault(kl => kl.IDpl == waiting.IDpl);
            //        if (klWycenyKartaZak != null)
            //        {
            //            klWycenyKartaZak.StanT = true;
            //            changes = true;
            //        }
            //    }
            //    else if (waiting.Flaga == 2)
            //    {
            //        KL_Wyceny_Karta_zak klWycenyKartaZak = warehouseEntities.KL_Wyceny_Karta_zak.FirstOrDefault(kl => kl.IDpl == waiting.IDpl);
            //        if (klWycenyKartaZak != null)
            //        {
            //            klWycenyKartaZak.StanMagT = true;
            //            changes = true;
            //        }
            //    }
            //    else if (waiting.Flaga == 3)
            //    {
            //        KL_Wyceny_Karta_dzial_zak klWycenyKartaDzialZak = warehouseEntities.KL_Wyceny_Karta_dzial_zak.FirstOrDefault(kl => kl.IDdpl == waiting.IDdpl);
            //        if (klWycenyKartaDzialZak != null)
            //        {
            //            klWycenyKartaDzialZak.StanT = true;
            //            changes = true;
            //        }
            //    }

            //    if (changes)
            //    {
            //        warehouseEntities.SaveChanges();
            //        warehouseEntities.AcceptAllChanges();
            //    }
            //}
            //else if (waiting.IloscZam < (decimal)0)
            //{
            //    throw new DataException("Ilość przyjęta jest wieksza od ilości zamówionej");
            //}
        }

        public int UpdateWarehouseGroupName(string newName, int warehouseGroupId)
        {
            WarehouseGroup warehouseGroup = null;

            if (!operations.ExistsWarehouseWithTheName(newName))
            {
                if (IsValidWarehouseGroupId(warehouseGroupId))
                {
                    warehouseGroup = GetWarehouseGroupById(warehouseGroupId);
                }

                if (warehouseGroup != null)
                {
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        warehouseGroup.Name = newName;

                        warehouseEntities.SaveChanges();
                        transactionScope.Complete();
                        warehouseEntities.AcceptAllChanges();

                        return 0; // OK
                    }
                }
                else
                {
                    return -1; // Unknown warehouse ID
                }
            }
            else
            {
                return -2; // Exists warehouse with the same name
            }

            // TODO: Put return codes into enum
        }

        public void UpdateWarehouseGroupPermission(int permissionId, bool AllowedMaterialIsPackage, bool AllowedPositionIsZZ, bool AllowedPositionIsGarbage, bool AllowedPositionIsDamaged, bool AllowedPositionIsPartlyDamaged)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var permissions = GetWarehouseGroupPermissions();

                var permission = permissions.FirstOrDefault(p => p.Id == permissionId);

                if (permission != null)
                {
                    permission.MaterialIsPackage = AllowedMaterialIsPackage;
                    permission.PositionIsZZ = AllowedPositionIsZZ;
                    permission.PositionIsGarbage = AllowedPositionIsGarbage;
                    permission.PositionIsDamaged = AllowedPositionIsDamaged;
                    permission.PositionIsPartlyDamaged = AllowedPositionIsPartlyDamaged;
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        public int UpdateWarehouseGroupShortName(string newShortName, int warehouseGroupId)
        {
#if DEBUG
            Contract.Requires(newShortName != null);
#endif
            WarehouseGroup warehouseGroup = null;

            newShortName = newShortName.ToUpper();

            if (IsValidWarehouseGroupShortName(newShortName))
            {
                if (!operations.ExistsWarehouseGroupWithTheShortName(newShortName))
                {
                    if (IsValidWarehouseGroupId(warehouseGroupId))
                    {
                        warehouseGroup = GetWarehouseGroupById(warehouseGroupId);
                    }

                    if (warehouseGroup != null)
                    {
                        using (TransactionScope transactionScope = new TransactionScope())
                        {
                            warehouseGroup.ShortName = newShortName;

                            warehouseEntities.SaveChanges();
                            transactionScope.Complete();
                            warehouseEntities.AcceptAllChanges();

                            return 0; // OK
                        }
                    }
                    else
                    {
                        return -1; // Unknown warehouse ID
                    }
                }
                else
                {
                    return -2; // Exists warehouse with the same ShortName
                }
            }
            else
            {
                return -3; // Invalid ShortName
            }

            // TODO: Put return codes into enum
        }

        /// <summary>
        /// Marek Zakrzewski ; [2010-03-16]
        /// </summary>
        /// <param name="newName">New warehouse name</param>
        /// <param name="warehouseId">Valid warehouse id</param>
        /// <returns>Response code (positive=OK, negative=ERROR)</returns>
        public int UpdateWarehouseName(string newName, int warehouseId)
        {
            Warehouse warehouse = null;

            if (!operations.ExistsWarehouseWithTheName(newName))
            {
                if (IsValidWarehouseId(warehouseId))
                {
                    warehouse = GetWarehouseById(warehouseId);
                }

                if (warehouse != null)
                {
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        warehouse.Name = newName;

                        warehouseEntities.SaveChanges();
                        transactionScope.Complete();
                        warehouseEntities.AcceptAllChanges();

                        return 0; // OK
                    }
                }
                else
                {
                    return -1; // Unknown warehouse ID
                }
            }
            else
            {
                return -2; // Exists warehouse with the same name
            }

            // TODO: Put return codes into enum
        }

        public void UpdateWarehousePermission(int permissionId, bool AllowedMaterialIsPackage, bool AllowedPositionIsZZ, bool AllowedPositionIsGarbage, bool AllowedPositionIsDamaged, bool AllowedPositionIsPartlyDamaged)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var permissions = GetWarehousePermissions();

                var permission = permissions.FirstOrDefault(p => p.Id == permissionId);

                if (permission != null)
                {
                    permission.MaterialIsPackage = AllowedMaterialIsPackage;
                    permission.PositionIsZZ = AllowedPositionIsZZ;
                    permission.PositionIsGarbage = AllowedPositionIsGarbage;
                    permission.PositionIsDamaged = AllowedPositionIsDamaged;
                    permission.PositionIsPartlyDamaged = AllowedPositionIsPartlyDamaged;
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }
        }

        /// <summary>
        /// Marek Zakrzewski ; [2010-03-16]
        /// </summary>
        /// <param name="newShortName">New warehouse shortName</param>
        /// <param name="warehouseId">Valid warehouse id</param>
        /// <returns>Response code (positive=OK, negative=ERROR)</returns>
        public int UpdateWarehouseShortName(string newShortName, int warehouseId)
        {
#if DEBUG
            Contract.Requires(newShortName != null);
#endif
            Warehouse warehouse = null;

            newShortName = newShortName.ToUpper();

            if (IsValidWarehouseShortName(newShortName))
            {
                if (!operations.ExistsWarehouseWithTheShortName(newShortName))
                {
                    if (IsValidWarehouseId(warehouseId))
                    {
                        warehouse = GetWarehouseById(warehouseId);
                    }

                    if (warehouse != null)
                    {
                        using (TransactionScope transactionScope = new TransactionScope())
                        {
                            warehouse.ShortName = newShortName;

                            warehouseEntities.SaveChanges();
                            transactionScope.Complete();
                            warehouseEntities.AcceptAllChanges();

                            return 0; // OK
                        }
                    }
                    else
                    {
                        return -1; // Unknown warehouse ID
                    }
                }
                else
                {
                    return -2; // Exists warehouse with the same ShortName
                }
            }
            else
            {
                return -3; // Invalid ShortName
            }

            // TODO: Put return codes into enum
        }

        public bool UserHasSecurityPrivilege(int systemUserId, int securityPrivilegeId)
        {
            bool retVal = false;
            SystemUser systemUser = warehouseEntities.SystemUsers
                                .Include(p => p.SystemUserSecurityPrivileges)
                                .Include(p => p.SystemUserGroupMemberships)
                                .Include(p => p.SystemUserGroupMemberships.Select(x => x.SystemGroup))
                                .Include(p => p.SystemUserGroupMemberships.Select(x => x.SystemGroup.SystemGroupSecurityPrivileges))
                                .FirstOrDefault(p => p.Id == systemUserId);

            if (systemUser != null)
            {
                if (systemUser.SystemUserSecurityPrivileges.Any(p => p.SecurityPrivilegeId == securityPrivilegeId))
                {
                    retVal = true;
                }
                else
                {
                    var systemUserGroups = from p in systemUser.SystemUserGroupMemberships
                                           select p.SystemGroup;
                    if (systemUserGroups.Any(p => p.SystemGroupSecurityPrivileges.Any(o => o.SecurityPrivilegeId == securityPrivilegeId)))
                    {
                        retVal = true;
                    }
                }
            }

            return retVal;
        }

        private static System.Linq.Expressions.Expression<Func<Material, bool>> FilterMaterial(int? companyId, string cleanedFilterValue)
        {
            return material =>
                      !material.TimeDeleted.HasValue &&
                      (!companyId.HasValue || material.CompanyId == companyId) &&
                      material.IdSIMPLE.HasValue &&
                      (
                        string.IsNullOrEmpty(cleanedFilterValue) ||
                        material.Name.ToLower().Contains(cleanedFilterValue) ||
                        material.IndexSIMPLE.ToLower().Contains(cleanedFilterValue)
                      );
        }

        private static System.Linq.Expressions.Expression<Func<C_ImportSimple_MaterialFromSimple, bool>> FilterMaterialSimple(int? companyId, string cleanedFilterValue)
        {
            return material =>
                      (!companyId.HasValue || material.CompanyId == companyId) &&
                      material.IdSimple.HasValue &&
                      (
                        string.IsNullOrEmpty(cleanedFilterValue) ||
                        material.NameSimple.ToLower().Contains(cleanedFilterValue) ||
                        material.IndexSIMPLE.ToLower().Contains(cleanedFilterValue)
                      );
        }

        //ToDo:lots of unit tests are needed
        private bool CheckAreThereGivesForReceive(List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum warehouseDocumentType)
        {
            int mmminusEnum = WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.MMMinus).Id;
            int mmplusEnum = WarehouseDocumentType.GetEnumValue(WarehouseDocumentTypeEnum.MMPlus).Id;

            if (warehouseDocumentType == WarehouseDocumentTypeEnum.MMPlus)
            {
                foreach (var positionWithQuantityAndShelfse in positionWithQuantityAndShelfses)
                {
                    foreach (var parentId in positionWithQuantityAndShelfse.positionParentIds)
                    {
                        WarehouseDocument warehouseDocument = GetWarehouseDocumentForPositionId(parentId.PositionParentID);

                        if (warehouseDocument == null || warehouseDocument.WarehouseDocumentType.Id != mmminusEnum)
                        {
                            throw new DataException("There is no MMMinus to generate this MMPlus");
                        }

                        double positionQuantity = GetWarehouseDocumentPositionById(parentId.PositionParentID).Quantity;

                        if (positionWithQuantityAndShelfse.position.Quantity > Math.Abs(positionQuantity))
                        {
                            throw new DataException("You cannot received more than has been given");
                        }
                    }
                }
            }

            return true;
        }

        //ToDo: TP intensive unit tests are required!!!
        private bool CheckAvailableQuantities(int warehouseSourceId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfsesList)
        {
            var currentStack = GetCurrentStackInDocuments("", "", warehouseSourceId);

            foreach (var positionWQAS in positionWithQuantityAndShelfsesList)
            {
                foreach (var cppq in positionWQAS.positionParentIds)
                {
                    var warehouseDocument = GetWarehouseDocumentForPositionId(cppq.PositionParentID);

                    if (warehouseDocument.WarehouseDocumentType.Id != ((int)WarehouseDocumentTypeEnum.Za)) // || positionWQAS.RefetchParentOrderedMaterial)
                    {
                        var currentStackForPostion = currentStack.First(c => c.PositionId == cppq.PositionParentID);

                        //if (currentStackForPostion.Quantity < Math.Abs(positionWQAS.position.Quantity)) return false;
                        if ((decimal?)currentStackForPostion.Quantity < Math.Abs(cppq.Quantity)) return false;
                    }
                }
            }

            return true;
        }

        private List<PositionWithQuantityAndShelfs> CollectParentPositionsInfoFromWarehouse
        (
          List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfes,
          WarehouseDocument warehouseDocument,
          bool mergeWithPrevious = false)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfes != null);
            Contract.Requires(warehouseDocument != null);
#endif

            var newPositionWithQuantityAndShelfes = new List<PositionWithQuantityAndShelfs>();

            foreach (var positionWithQuantityAndShelfs in positionWithQuantityAndShelfes)
            {
                PositionWithQuantityAndShelfs newPositionWithQuantityAndShelfs = null; // new PositionWithQuantityAndShelfs();
                //int? orderId = positionWithQuantityAndShelfs.position.Order != null
                //                   ? positionWithQuantityAndShelfs.position.Order.Id //.Order.Id
                //                   : (int?)null;

                int? orderId = positionWithQuantityAndShelfs.position.Order != null
                                   ? positionWithQuantityAndShelfs.position.Order.Id //.Order.Id
                                   : positionWithQuantityAndShelfs.position.OrderId != null ?
                                   positionWithQuantityAndShelfs.position.OrderId : (int?)null;

                var stacks = warehouseEntities.CurrentStackInDocuments
                    .Where(s =>
                      warehouseDocument.WarehouseSourceId.HasValue
                      && s.WarehouseTargetId == warehouseDocument.WarehouseSourceId //.Warehouse.Id
                      && s.MaterialId == positionWithQuantityAndShelfs.position.MaterialId //.Material.Id
                      && s.Quantity.HasValue
                      && !s.IsReserved
                      && !s.IsKC
                    );

                //var testy = warehouseEntities.CurrentStackInDocuments.Where(x => x.MaterialId == positionWithQuantityAndShelfs.position.MaterialId).ToList();
                //var testy = stacks.ToList();

                //var testy2 = stacks.ToList();


                if (orderId.HasValue)
                {
                    stacks = stacks.Where(s => s.OrderId == orderId.Value);
                }
                else
                {
                    stacks = stacks.Where(s => !s.OrderId.HasValue);
                }
                stacks = stacks.OrderByDescending(p => p.PackFactor > 0 ? p.PackFactor * p.Quantity : p.Quantity);
                // TO TRZEBA WYŁĄCZYĆ DO KOLEJNEJ WERSJI!!!
                if (!stacks.Any())
                {
                    // ZMIENIONO LOKALIZACJĘ PO CZĘŚCIOWYM WYDANIU
                    stacks = warehouseEntities.CurrentStackInDocuments
                            .Where(s =>
                              warehouseDocument.WarehouseSourceId.HasValue
                              && s.WarehouseSourceId == warehouseDocument.WarehouseSourceId //.Warehouse.Id
                              && s.MaterialId == positionWithQuantityAndShelfs.position.MaterialId //.Material.Id
                              && s.Quantity.HasValue
                              && !s.IsReserved
                              && !s.IsKC
                            );

                    if (warehouseDocument.DocumentTypeId == 5)
                    {
                        stacks = stacks.Where(x => x.DocumentTypeId != 1);
                    }

                    if (orderId.HasValue)
                    {
                        stacks = stacks.Where(s => s.OrderId == orderId.Value);
                    }
                    else
                    {
                        stacks = stacks.Where(s => !s.OrderId.HasValue);
                    }
                    stacks = stacks.OrderByDescending(p => p.PackFactor > 0 ? p.PackFactor * p.Quantity : p.Quantity);
                }

                var totalQuantityToCollect = Math.Abs((decimal)positionWithQuantityAndShelfs.position.Quantity);


                if (warehouseDocument.DocumentTypeId == 5)
                {
                    var testStack = stacks.Where(x => x.DocumentTypeId != 1).Any();
                    if (testStack)
                    {
                        var restQuantity = Math.Abs((decimal)stacks.Where(x => x.DocumentTypeId != 1).Sum(x => x.Quantity));
                        if (restQuantity < totalQuantityToCollect)
                        { }
                        else
                        {
                            stacks = stacks.Where(x => x.DocumentTypeId != 1);
                        }
                    }
                }

                
                var totalCollectedQuantity = 0M;
                var missingQuantity = totalQuantityToCollect;
                var quantityToTakeFromCurrentPosition = 0M;

                var sign = positionWithQuantityAndShelfs.position.Quantity >= 0 ? 1 : -1;

                decimal? previousUnitPrice = null;
                int counter = 0;
                foreach (var stack in stacks)
                {
                    counter++;
                    missingQuantity = totalQuantityToCollect - totalCollectedQuantity;
                    if (missingQuantity > 0)
                    {
                        if (counter == 1 || (previousUnitPrice.HasValue && previousUnitPrice.Value != stack.UnitPrice))
                        {
                            newPositionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
                            newPositionWithQuantityAndShelfs.documentParentIds = new List<int>();
                            newPositionWithQuantityAndShelfs.positionParentIds = new List<ChildParentPositionQuantity>();
                            newPositionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();
                            newPositionWithQuantityAndShelfes.Add(newPositionWithQuantityAndShelfs);

                            newPositionWithQuantityAndShelfs.position = CreateWarehouseDocumentPositionDataOnly(stack.MaterialId, stack.UnitPrice,
                                0, 0, stack.OrderId.HasValue ? stack.OrderId.Value : 0);
                        }

                        quantityToTakeFromCurrentPosition = (decimal)stack.Quantity > missingQuantity
                                                                ? missingQuantity
                                                                : (decimal)stack.Quantity.Value;

                        newPositionWithQuantityAndShelfs.position.Quantity += (double)(sign * quantityToTakeFromCurrentPosition);

                        if (!newPositionWithQuantityAndShelfs.documentParentIds.Contains(stack.WarehouseDocumentId))
                            newPositionWithQuantityAndShelfs.documentParentIds.Add(stack.WarehouseDocumentId);

                        newPositionWithQuantityAndShelfs.position.IsKC = positionWithQuantityAndShelfs.position.IsKC;
                        newPositionWithQuantityAndShelfs.position.IdZF = positionWithQuantityAndShelfs.position.IdZF;

                        newPositionWithQuantityAndShelfs.position.SimpleDepartmentId = positionWithQuantityAndShelfs.position.SimpleDepartmentId;
                        newPositionWithQuantityAndShelfs.position.Department = positionWithQuantityAndShelfs.position.Department;
                        newPositionWithQuantityAndShelfs.position.ProductionMachine = positionWithQuantityAndShelfs.position.ProductionMachine;

                        newPositionWithQuantityAndShelfs.position.ProcessIdpl = positionWithQuantityAndShelfs.position.ProcessIdpl;
                        newPositionWithQuantityAndShelfs.position.ProcessIddpl = positionWithQuantityAndShelfs.position.ProcessIddpl;

                        var childParentPositionQuantity = new ChildParentPositionQuantity();
                        childParentPositionQuantity.LocationId = stack.ShelfId;
                        childParentPositionQuantity.PositionParentID = stack.PositionId;
                        childParentPositionQuantity.Quantity = quantityToTakeFromCurrentPosition;

                        // Add WPZ
                        var warehousePositionZone = new WarehousePositionZone();
                        warehousePositionZone.ZoneTmp =
                            warehouseEntities.Zones.Where(z => z.Id == stack.ShelfId).FirstOrDefault();
                        warehousePositionZone.Quantity = (double)quantityToTakeFromCurrentPosition;

                        //warehouseEntities.Detach(warehousePositionZone);
                        newPositionWithQuantityAndShelfs.WarehousePositionZones.Add(warehousePositionZone);
                        newPositionWithQuantityAndShelfs.positionParentIds.Add(childParentPositionQuantity);
                        totalCollectedQuantity += quantityToTakeFromCurrentPosition;
                    }
                    else
                    {
                        break;
                    }
                    previousUnitPrice = stack.UnitPrice;
                }

                // Creating warehouse doc pos
                foreach (var pos in newPositionWithQuantityAndShelfes)
                {
                    pos.MaterialId = pos.position.Material.Id;
                    pos.UnitPrice = pos.position.UnitPrice;
                    pos.Quantity = (decimal)pos.position.Quantity;
                    pos.TargetDepartmentId = positionWithQuantityAndShelfs.TargetDepartmentId;
                    pos.StartDate = positionWithQuantityAndShelfs.StartDate;
                    pos.ProductionOrderId = positionWithQuantityAndShelfs.ProductionOrderId;
                }

                if (mergeWithPrevious)
                {
                    newPositionWithQuantityAndShelfs.documentParentIds.AddRange(positionWithQuantityAndShelfs.documentParentIds);
                    newPositionWithQuantityAndShelfs.positionParentIds.AddRange(positionWithQuantityAndShelfs.positionParentIds);
                }
            }

            var q1 = positionWithQuantityAndShelfes.Sum(p => (decimal)p.position.Quantity);
            var q2 = newPositionWithQuantityAndShelfes.Sum(p => p.Quantity);

            if (q1 == q2)
            {
                return newPositionWithQuantityAndShelfes;
            }
            else
            {
                return null;
            }
        }

        private WarehouseDocument CreateWarehouseDocumentBasedOnWarehouseInternalOperation
        (
          int? sourceWarehouseId,
          int? targetWarehouseId,
          int authorId,
          List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses,
          WarehouseDocumentTypeEnum warehouseDocumentType,
          DateTime date,
          bool exportDocToSIMPLE = true,
          int? targetDeptSIMPLEId = null,
          int? targetOrderId = null,
          int? docSubtypeSIMPLE = null)
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
            Contract.Requires(positionWithQuantityAndShelfses.Any());
#endif
            //int docType = Convert.ToInt32(warehouseDocumentType);

            // Grouping positions:
            //positionWithQuantityAndShelfses = GroupPositions(positionWithQuantityAndShelfses);

            WarehouseDocument warehouseDocument;
            List<PositionWithQuantityAndShelfs> newPositionWithQuantityAndShelfList = new List<PositionWithQuantityAndShelfs>();

            using (TransactionScope transactionScope = new TransactionScope())
            {
                warehouseDocument = operations.CreateWarehouseDocument(sourceWarehouseId, targetWarehouseId, authorId, warehouseDocumentType, date, exportDocToSIMPLE, targetDeptSIMPLEId, targetOrderId, docSubtypeSIMPLE);
                //warehouseDocument.IsForOrder = positionWithQuantityAndShelfses.Any(p => p.order != null || p.position.Order != null);

                var entities = warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToList();
                foreach (var entry in entities)
                {
                    if (entry.Entity.GetType() == typeof(WarehouseDocumentPosition))
                    {
                        entry.ChangeState(EntityState.Unchanged);
                    }
                }
                warehouseEntities.SaveChanges();

                //if (warehouseDocument.Warehouse != null)
                //    positionWithQuantityAndShelfses = CollectParentPositionsInfoFromWarehouse(ref positionWithQuantityAndShelfses, warehouseDocument);
                //if (positionWithQuantityAndShelfses == null)
                //    throw new Exception("Nie udało się zebrać całej wymaganej ilości");
                // TODO: Think about uncommenting above lines if we need to get materials from positions

                foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
                {
                    if (positionWithQuantityAndShelf.position.Quantity == 0.0d) throw new ArgumentException("Quantity cannot be 0", "warehouseDocumentPosition");

                    PositionWithQuantityAndShelfs newPositionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
                    newPositionWithQuantityAndShelfs.position = new WarehouseDocumentPosition();

                    newPositionWithQuantityAndShelfs.position.Material = warehouseEntities.Materials.First(m => m.Id == positionWithQuantityAndShelf.position.Material.Id);
                    newPositionWithQuantityAndShelfs.position.Quantity = positionWithQuantityAndShelf.position.Quantity;
                    newPositionWithQuantityAndShelfs.position.UnitPrice = positionWithQuantityAndShelf.position.UnitPrice;

                    // Flags propagation
                    var positionParents = from p in GetParentPositionsOfPositionWithQuantityAndShelfs(positionWithQuantityAndShelf) select p;

                    // Copying flags
                    newPositionWithQuantityAndShelfs.position.IsPartlyDamaged = positionParents.Any(p => p.IsPartlyDamaged == true) || positionWithQuantityAndShelf.position.IsPartlyDamaged;
                    newPositionWithQuantityAndShelfs.position.IsDamaged = positionParents.Any(p => p.IsDamaged == true) || positionWithQuantityAndShelf.position.IsDamaged;
                    newPositionWithQuantityAndShelfs.position.IsZZ = positionParents.Any(p => p.IsZZ == true) || positionWithQuantityAndShelf.position.IsZZ;
                    newPositionWithQuantityAndShelfs.position.IsGarbage = positionParents.Any(p => p.IsGarbage == true) || positionWithQuantityAndShelf.position.IsGarbage;

                    // In case of Zw- / Zw+ documents we don't want to keep track of order... Otherwise we act as we use to normally
                    if (warehouseDocumentType == WarehouseDocumentTypeEnum.ZwMinus || warehouseDocumentType == WarehouseDocumentTypeEnum.ZwPlus)
                    {
                        newPositionWithQuantityAndShelfs.order = null;
                        newPositionWithQuantityAndShelfs.position.Order = null;
                    }
                    else
                    {
                        if (positionWithQuantityAndShelf.position.Order != null)
                        {
                            newPositionWithQuantityAndShelfs.position.Order = warehouseEntities.Orders.First(m => m.Id == positionWithQuantityAndShelf.position.Order.Id);
                        }
                    }

                    newPositionWithQuantityAndShelfs.position.ProcessIdpl = positionWithQuantityAndShelf.position.ProcessIdpl;
                    newPositionWithQuantityAndShelfs.position.ProcessIddpl = positionWithQuantityAndShelf.position.ProcessIddpl;

                    if (warehouseDocumentType == WarehouseDocumentTypeEnum.Pz || warehouseDocumentType == WarehouseDocumentTypeEnum.PzKC)
                    {
                        if (positionWithQuantityAndShelf.additionalPositionInfo != null && positionWithQuantityAndShelf.additionalPositionInfo.SupplySourceType == 2)
                        {
                            // Update temporary supply table
                            Order order = warehouseEntities.Orders.Where(p => p.Id == positionWithQuantityAndShelf.position.Order.Id)
                                .FirstOrDefault();

                            if (order != null)
                            {
                                var warehouseSupplyMaterialsByHandTmp = warehouseEntities.WarehouseSupplyMaterialsByHandTmps.Where(p => p.NazwSK == order.OrderNumberPZ)
                                  .FirstOrDefault();

                                if (warehouseSupplyMaterialsByHandTmp != null)
                                {
                                    warehouseSupplyMaterialsByHandTmp.UsedQuantity += positionWithQuantityAndShelf.position.Quantity;
                                }
                            }
                        }
                    }

                    warehouseDocument.AttachWarehouseDocumentPosition(newPositionWithQuantityAndShelfs.position);

                    warehouseEntities.AddToWarehouseDocumentPositions(newPositionWithQuantityAndShelfs.position);

                    newPositionWithQuantityAndShelfList.Add(newPositionWithQuantityAndShelfs);

                    warehouseEntities.SaveChanges();

                    if (positionWithQuantityAndShelf.WarehousePositionZones != null)
                    {
                        newPositionWithQuantityAndShelfs.WarehousePositionZones = positionWithQuantityAndShelf.WarehousePositionZones;
                        foreach (WarehousePositionZone warehousePositionZone in newPositionWithQuantityAndShelfs.WarehousePositionZones)
                        {
                            warehousePositionZone.WarehouseDocumentPosition = newPositionWithQuantityAndShelfs.position; // new WarehouseDocumentPosition() { Id = newPositionWithQuantityAndShelfs.position.Id };
                        }
                    }

                    warehouseEntities.SaveChanges();

                    if (warehouseDocumentType != WarehouseDocumentTypeEnum.Pz && warehouseDocumentType != WarehouseDocumentTypeEnum.PzKC)
                    {
                        UpdateRelations(warehouseDocument, newPositionWithQuantityAndShelfs, positionWithQuantityAndShelf);
                    }

                    warehouseEntities.SaveChanges();

                    // detach unchagned entity positions for multiply order
                    foreach (var entry in entities)
                    {
                        if (entry.Entity.GetType() == typeof(WarehouseDocumentPosition))
                            entry.ChangeState(EntityState.Detached);
                    }

                }

                foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in newPositionWithQuantityAndShelfList)
                {
                    Bind(positionWithQuantityAndShelf);
                }

                warehouseEntities.SaveChanges();

                List<WarehouseDocumentPosition> warehouseDocumentPositionsList = new List<WarehouseDocumentPosition>(newPositionWithQuantityAndShelfList.Select(p => p.position));

                operations.UpdateCurrentStack(warehouseDocument, warehouseDocumentPositionsList);

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        private WarehouseDocument CreateWarehouseDocumentBasedOnWarehouseOperation
        (
          int? sourceWarehouseId,
          int? targetWarehouseId,
          int authorId,
          List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses,
          WarehouseDocumentTypeEnum warehouseDocumentType,
          DateTime date,
          bool exportDocToSIMPLE = true,
          int? targetDeptSIMPLEId = null,
          int? targetOrderId = null,
          int? docSubtypeSIMPLE = null,
          bool checkParentCorrectness = true
        )
        {
#if DEBUG
            Contract.Requires(positionWithQuantityAndShelfses != null);
            Contract.Requires(positionWithQuantityAndShelfses.Any());
            Contract.Requires(Contract.ForAll(positionWithQuantityAndShelfses, pwqas => pwqas.position != null));
#endif

            EnsureUserHasPrivilegeToPerformOperation(sourceWarehouseId, targetWarehouseId, authorId, warehouseDocumentType);

            targetDeptSIMPLEId = GetTargetDepartmentInSimple(targetWarehouseId, targetDeptSIMPLEId,targetOrderId);

            WarehouseDocument warehouseDocument = null;

            var materialIds = positionWithQuantityAndShelfses
              .Select(pwqas => pwqas.position.MaterialId)
              .ToArray();

            using (var transactionScope = new TransactionScope())
            {
                var materials = warehouseEntities.Materials
                  .Where(m => materialIds.Contains(m.Id) && m.TimeChildrenEnabled.HasValue);
                if (materials.Any())
                {
                    throw new Exception("Operacje na grupach materiałów zabronione!");
                }

                warehouseDocument = operations.CreateWarehouseDocument
                (
                  sourceWarehouseId,
                  targetWarehouseId,
                  authorId,
                  warehouseDocumentType,
                  date,
                  exportDocToSIMPLE,
                  targetDeptSIMPLEId,
                  targetOrderId,
                  docSubtypeSIMPLE);
                //warehouseDocument.IsForOrder = positionWithQuantityAndShelfses.Any(p => p.order != null || p.position.Order != null);

                var entities = warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToList();
                foreach (var entry in entities)
                {
                    if (entry.Entity.GetType() == typeof(WarehouseDocumentPosition))
                        entry.ChangeState(EntityState.Detached);
                }

                warehouseEntities.SaveChanges();

                //CollectParentPositionsInfoFromWarehouse

                #region Truly scary stuff. For some reason it replaces the positions. No DEV really knows why it does what it does.
                if
                    (
                      warehouseDocumentType != WarehouseDocumentTypeEnum.BO &&
                      warehouseDocumentType != WarehouseDocumentTypeEnum.INW &&
                      warehouseDocumentType != WarehouseDocumentTypeEnum.Pz &&
                      positionWithQuantityAndShelfses.All(p => p.AutoParentCollection)
                    )
                {
                    if
                    (
                      warehouseDocument.WarehouseSourceId != null &&
                      (
                        positionWithQuantityAndShelfses.Any(p => p.positionParentIds == null || p.positionParentIds.Count == 0) ||
                        positionWithQuantityAndShelfses.All(p => p.RefetchParentOrderedMaterial)
                      )
                    )
                    {
                        // TODO: ???
                        positionWithQuantityAndShelfses = CollectParentPositionsInfoFromWarehouse
                          (
                            positionWithQuantityAndShelfses,
                            warehouseDocument,
                            positionWithQuantityAndShelfses.All(p => p.RefetchParentOrderedMaterial)
                          );
                    }
                    if
                    (
                      positionWithQuantityAndShelfses == null ||
                      !positionWithQuantityAndShelfses.Any() ||
                      positionWithQuantityAndShelfses.Any(p => p.positionParentIds == null || !p.positionParentIds.Any())
                    )
                    {
                        throw new Exception("Nie udało się zebrać całej wymaganej ilości (1)");
                    }
                }
                #endregion

                #region Some type of parent checking, had to do with the magic above
                // positionWithQuantityAndShelfses.Any(p => !p.AutoParentCollection) &&
                if
                (
                  checkParentCorrectness &&
                  warehouseDocumentType != WarehouseDocumentTypeEnum.Pz &&
                  warehouseDocumentType != WarehouseDocumentTypeEnum.BO &&
                  warehouseDocumentType != WarehouseDocumentTypeEnum.INW &&
                  positionWithQuantityAndShelfses.Any
                  (p =>
                    p.positionParentIds == null ||
                    p.positionParentIds.Count == 0 ||
                    p.positionParentIds.Any(pp => /*pp.LocationId == null ||*/ pp.LocationId == 0)
                  )
                )
                {
                    throw new Exception("Nie udało się zebrać całej wymaganej ilości (2)");
                }
                #endregion

                if (warehouseDocumentType != WarehouseDocumentTypeEnum.INW && positionWithQuantityAndShelfses.Any(pwqas => pwqas.position.Quantity == 0.0))
                {
                    throw new ArgumentException("Position's quantity cannot be 0 for documents other than INW", "warehouseDocumentPosition");
                }

                foreach (var position in positionWithQuantityAndShelfses)
                {
                    position.position.WarehouseDocumentId = warehouseDocument.Id;

                    if (!position.position.OrderDeliveryId.HasValue && position.position.OrderDeliveryId == 0)
                    {
                        position.position.OrderDeliveryId = warehouseEntities
                            .OrderDeliveries.Where(x => x.OrderId == position.position.Order.Id
                                && x.ProcessIDPL == position.position.OrderDelivery.ProcessIDPL)
                            .Select(x => x.Id)
                            .FirstOrDefault();
                    }
                }

                var newPositionWithQuantityAndShelfList = new List<PositionWithQuantityAndShelfs>();

                foreach (var positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
                {
                    //if (warehouseDocumentType != WarehouseDocumentTypeEnum.INW && positionWithQuantityAndShelf.position.Quantity == 0.0) 
                    //  throw new ArgumentException("Quantity cannot be 0", "warehouseDocumentPosition");

                    //warehouseEntities.Detach(positionWithQuantityAndShelf.position);

                    var newPositionWithQuantityAndShelfs = new PositionWithQuantityAndShelfs();
                    newPositionWithQuantityAndShelfs.position = new WarehouseDocumentPosition();

                    newPositionWithQuantityAndShelfs.position.Material = warehouseEntities.Materials.First(m => m.Id == positionWithQuantityAndShelf.position.MaterialId);
                    newPositionWithQuantityAndShelfs.position.Quantity = positionWithQuantityAndShelf.position.Quantity;
                    newPositionWithQuantityAndShelfs.position.UnitPrice = positionWithQuantityAndShelf.position.UnitPrice;
                    newPositionWithQuantityAndShelfs.position.SimpleOrderId = positionWithQuantityAndShelf.position.SimpleOrderId;
                    newPositionWithQuantityAndShelfs.position.IsKJ = positionWithQuantityAndShelf.position.IsKJ;

                    // reassing original orders. Broken by Marek Zakrzewski
                    if (positionWithQuantityAndShelf.order != default(Order))
                    {
                        newPositionWithQuantityAndShelfs.order = positionWithQuantityAndShelf.order;
                        newPositionWithQuantityAndShelfs.position.Order = warehouseEntities.Orders.Where(x => x.Id == positionWithQuantityAndShelf.order.Id).FirstOrDefault();

                        // search for company for this order
                        var companyForOrder = warehouseEntities.Orders.SingleOrDefault(o => o.Id == positionWithQuantityAndShelf.order.Id);
                        if (companyForOrder != default(Order))
                        {
                            newPositionWithQuantityAndShelfs.order.CompanyId = companyForOrder.CompanyId;
                            newPositionWithQuantityAndShelfs.position.Order.CompanyId = companyForOrder.CompanyId;
                        }
                    }

                    // Flags propagation
                    var positionParents = from p in GetParentPositionsOfPositionWithQuantityAndShelfs(positionWithQuantityAndShelf) select p;

                    newPositionWithQuantityAndShelfs.position.IsKC = positionWithQuantityAndShelf.position.IsKC;
                    newPositionWithQuantityAndShelfs.position.IdZF = positionWithQuantityAndShelf.position.IdZF;
                    newPositionWithQuantityAndShelfs.position.SimpleDepartmentId = positionWithQuantityAndShelf.position.SimpleDepartmentId;

                    if (newPositionWithQuantityAndShelfs.position.SimpleDepartmentId.HasValue)
                    {
                        // get k3 dept and machine ids
                        var deptSimpleK3 = warehouseEntities.DepartmentSIMPLEMappings
                            .Include(dsm => dsm.Department)
                            .Include(dsm => dsm.ProductionMachine)
                            .FirstOrDefault(d => d.DepartmentSIMPLEId == newPositionWithQuantityAndShelfs.position.SimpleDepartmentId && d.CompanyId == newPositionWithQuantityAndShelfs.position.Material.CompanyId);
                        if (deptSimpleK3 != null)
                        {
                            newPositionWithQuantityAndShelfs.position.DepartmentId = deptSimpleK3.DepartmentId;// .Department = deptSimpleK3.Department;
                            newPositionWithQuantityAndShelfs.position.ProductionMachine = deptSimpleK3.ProductionMachine;
                        }
                    }


                    if (positionWithQuantityAndShelf.position.OrderDelivery != null)
                    {
                        if (positionWithQuantityAndShelf.position.ProcessIdpl == null)
                        {
                            positionWithQuantityAndShelf.position.ProcessIdpl = positionWithQuantityAndShelf.position.OrderDelivery.ProcessIDPL;
                        }

                        if (positionWithQuantityAndShelf.position.ProcessIddpl == null)
                        {
                            positionWithQuantityAndShelf.position.ProcessIddpl = positionWithQuantityAndShelf.position.OrderDelivery.ProcessIDDPL;
                        }
                    }

                    // commented out because there is problem in acquiring proper IDpl having only material and order - this has to be tripple checked
                    // Positions with Orders are required to have IDpl (consequence of K2 design)

                    // accept for inventory
                    if (positionWithQuantityAndShelf.order != default(Order) && warehouseDocumentType != WarehouseDocumentTypeEnum.INW)
                    {
                        if (!positionWithQuantityAndShelf.position.ProcessIdpl.HasValue && !positionWithQuantityAndShelf.position.ProcessIddpl.HasValue)
                        {
                            var exMsg = string.Format("positionWithQuantityAndShelf.order id:{0} number:{1} exists but positionWithQuantityAndShelf.position.ProcessIdpl has no value. Cannot add new position with Order but without IDpl"
                              , positionWithQuantityAndShelf.order.Id
                              , positionWithQuantityAndShelf.order.OrderNumber);
                            throw new Exception(exMsg);
                        }
                    }

                    newPositionWithQuantityAndShelfs.position.ProcessIdpl = positionWithQuantityAndShelf.position.ProcessIdpl;
                    newPositionWithQuantityAndShelfs.position.ProcessIddpl = positionWithQuantityAndShelf.position.ProcessIddpl;

                    newPositionWithQuantityAndShelfs.position.IsPartlyDamaged = positionParents.Any(p => p.IsPartlyDamaged == true) || positionWithQuantityAndShelf.position.IsPartlyDamaged;
                    newPositionWithQuantityAndShelfs.position.IsDamaged = positionParents.Any(p => p.IsDamaged == true) || positionWithQuantityAndShelf.position.IsDamaged;
                    newPositionWithQuantityAndShelfs.position.IsZZ = positionParents.Any(p => p.IsZZ == true) || positionWithQuantityAndShelf.position.IsZZ;
                    newPositionWithQuantityAndShelfs.position.IsGarbage = positionParents.Any(p => p.IsGarbage == true) || positionWithQuantityAndShelf.position.IsGarbage;

                    newPositionWithQuantityAndShelfs.ProductionOrderId = positionWithQuantityAndShelf.ProductionOrderId;

                    newPositionWithQuantityAndShelfs.position.SupplyOrderPositionId = positionWithQuantityAndShelf.position.SupplyOrderPositionId;
                    //// Rewrite OrderId if applies
                    //if (newPositionWithQuantityAndShelfs.ProductionOrderId > 0)
                    //{
                    //    WarehouseProductionOrder warehouseProductionOrder =
                    //        warehouseEntities.WarehouseProductionOrder.Include("Order").Where(
                    //            p => p.Id == newPositionWithQuantityAndShelfs.ProductionOrderId).First();
                    //    newPositionWithQuantityAndShelfs.order = warehouseProductionOrder.Order;
                    //    newPositionWithQuantityAndShelfs.position.Order = warehouseProductionOrder.Order;

                    //}

                    // Only for appropriate type of operation where WarehouseDocumentPositionExtension exists
                    if (positionWithQuantityAndShelf.position.WarehouseDocumentPositionExtensions != null)
                    {
                        WarehouseDocumentPositionExtension positionExtension =
                            positionWithQuantityAndShelf.position.WarehouseDocumentPositionExtensions.FirstOrDefault();

                        if (positionExtension != null)
                        {
                            string damageDescription =
                                ToSafeSqlXmlString(
                                    positionExtension.Info);

                            if (damageDescription.Length != 0)
                            {
                                // Adding damage descriptions
                                string info = "<document><damageDescription>" + damageDescription +
                                              "</damageDescription></document>";

                                WarehouseDocumentPositionExtension warehouseDocumentPositionExtension =
                                    new WarehouseDocumentPositionExtension() { Info = info };

                                //////warehouseDocumentPositionExtension.WarehouseDocumentPosition =
                                //////    newPositionWithQuantityAndShelfs.position;

                                // Adding DamagedOn relation
                                newPositionWithQuantityAndShelfs.position.DamagedOn = targetWarehouseId;// .Warehouse = target;
                            }
                        }
                        else
                        {
                            positionWithQuantityAndShelf.position.WarehouseDocumentPositionExtensions = null;
                        }
                    }
                    // In case of Zw- / Zw+ documents we don't want to keep track of order... Otherwise we act as we use to normally
                    if (warehouseDocumentType == WarehouseDocumentTypeEnum.ZwMinus || warehouseDocumentType == WarehouseDocumentTypeEnum.ZwPlus)
                    {
                        newPositionWithQuantityAndShelfs.order = null;
                        newPositionWithQuantityAndShelfs.position.Order = null;
                    }
                    else
                    {
                        if (positionWithQuantityAndShelf.position.Order != null)
                        {
                            newPositionWithQuantityAndShelfs.position.Order = warehouseEntities.Orders.First(m => m.Id == positionWithQuantityAndShelf.position.Order.Id);
                        }
                        if (positionWithQuantityAndShelf.position.OrderDelivery != null)
                        {
                            newPositionWithQuantityAndShelfs.position.OrderDelivery = warehouseEntities.OrderDeliveries.First(m => m.Id == positionWithQuantityAndShelf.position.OrderDelivery.Id);
                        }

                        if (positionWithQuantityAndShelf.position.OrderDeliveryNew != null)
                        {
                            newPositionWithQuantityAndShelfs.position.OrderDeliveryNew = warehouseEntities.OrderDeliveryNews.First(m => m.Id == positionWithQuantityAndShelf.position.OrderDeliveryNew.Id);
                        }
                    }

                    if (warehouseDocumentType == WarehouseDocumentTypeEnum.Pz || warehouseDocumentType == WarehouseDocumentTypeEnum.PzKC)
                    {
                        if (positionWithQuantityAndShelf.additionalPositionInfo != null && positionWithQuantityAndShelf.additionalPositionInfo.SupplySourceType == 2)
                        {
                            // Update temporary supply table
                            Order order =
                                warehouseEntities.Orders.Where(p => p.Id == positionWithQuantityAndShelf.position.Order.Id)
                                .FirstOrDefault();

                            if (order != null)
                            {
                                WarehouseSupplyMaterialsByHandTmp warehouseSupplyMaterialsByHandTmp =
                                    warehouseEntities.WarehouseSupplyMaterialsByHandTmps.Where(
                                        p => p.NazwSK == order.OrderNumberPZ).FirstOrDefault();

                                if (warehouseSupplyMaterialsByHandTmp != null)
                                {
                                    warehouseSupplyMaterialsByHandTmp.UsedQuantity +=
                                        positionWithQuantityAndShelf.position.Quantity;
                                }
                            }
                        }
                    }

                    foreach (var entity in warehouseEntities.ObjectStateManager.GetObjectStateEntries(EntityState.Added))
                    {
                        if (entity.Entity.GetType() == typeof(WarehouseDocumentPosition))
                            entity.ChangeState(EntityState.Detached);
                    }


                    // testy dla INV usuwamy IDpl i IDdpl oraz Order
                    //if (warehouseDocumentType == WarehouseDocumentTypeEnum.INW && (Math.Abs(newPositionWithQuantityAndShelfs.position.ActualQuantity) + newPositionWithQuantityAndShelfs.position.Quantity) == 0)
                    //{
                    //    newPositionWithQuantityAndShelfs.position.ProcessIdpl = null;
                    //    newPositionWithQuantityAndShelfs.position.ProcessIddpl = null;
                    //    newPositionWithQuantityAndShelfs.position.Order = null;
                    //    newPositionWithQuantityAndShelfs.position.OrderId = null;
                    //}


                    warehouseDocument.AttachWarehouseDocumentPosition(newPositionWithQuantityAndShelfs.position);
                    warehouseEntities.AddToWarehouseDocumentPositions(newPositionWithQuantityAndShelfs.position);
                    //newPositionWithQuantityAndShelfs.position.WarehouseDocument = warehouseDocument;
                    newPositionWithQuantityAndShelfList.Add(newPositionWithQuantityAndShelfs);

                    //var test = new List<object>();
#if DEBUG
                    var newObjects = warehouseEntities.ObjectStateManager.GetObjectStateEntries(System.Data.EntityState.Added).ToList();
#endif
                    warehouseEntities.SaveChanges();

                    if (positionWithQuantityAndShelf.WarehousePositionZones != null)
                    {
                        newPositionWithQuantityAndShelfs.WarehousePositionZones = positionWithQuantityAndShelf.WarehousePositionZones;

                        // dodać przepisywanie (na nowo) WarehousePositionZone

                        newPositionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>();
                        foreach (var wpz in positionWithQuantityAndShelf.WarehousePositionZones)
                        {
                            if (wpz.Zone != null || wpz.ZoneTmp != null)
                            {
                                int zoneId = 0;
                                if (wpz.Zone != null)
                                {
                                    zoneId = wpz.Zone.Id;
                                }
                                else
                                {
                                    zoneId = wpz.ZoneTmp.Id;
                                }
                                WarehousePositionZone newWPZ = new WarehousePositionZone();
                                newWPZ.WarehouseDocumentPosition = newPositionWithQuantityAndShelfs.position;
                                Zone zone = warehouseEntities.Zones.First(z => z.Id == zoneId);
                                newWPZ.Zone = zone;
                                newWPZ.Quantity = wpz.Quantity;
                                newPositionWithQuantityAndShelfs.WarehousePositionZones.Add(newWPZ);
                            }
                            else
                            {
                                throw new Exception("No Zone in WPZ");
                            }
                        }
                    }

                    if (positionWithQuantityAndShelf.Packages != null)
                    {
                        var testPackageList = new List<WarehouseDocumentPositionPackage>();

                        foreach (WarehouseDocumentPositionPackage package in positionWithQuantityAndShelf.Packages)
                        {
                            WarehouseDocumentPositionPackage newPackage = new WarehouseDocumentPositionPackage();
                            newPackage.WarehouseDocumentPosition = newPositionWithQuantityAndShelfs.position;
                            newPackage.NumberOfPackages = package.NumberOfPackages;
                            newPackage.PackageType = package.PackageType;

                            newPackage.Zone = package.Zone;

                            warehouseEntities.AddToWarehouseDocumentPositionPackages(newPackage);

                            testPackageList.Add(newPackage);
                        }

                        // detach old obj:
                        foreach (WarehouseDocumentPositionPackage package in positionWithQuantityAndShelf.Packages)
                        {
                            warehouseEntities.Detach(package);
                        }

                        foreach (WarehouseDocumentPositionPackage package in testPackageList)
                        {
                            int packageTypeId = package.PackageType.Id;
                            int? zoneId = package.Zone != null ? package.Zone.Id : (int?)null;

                            //DetachEntity(package.Zone);
                            DetachEntity(package.PackageType);

                            package.PackageType = warehouseEntities.PackageTypes.First(p => p.Id == packageTypeId);
                            if (zoneId.HasValue)
                                package.Zone = warehouseEntities.Zones.First(p => p.Id == zoneId);
                        }

                        foreach (var warehousePositionZone in newPositionWithQuantityAndShelfs.WarehousePositionZones)
                        {
                            if (testPackageList.Count > 0)
                            {
                                //int packageTypeId = package.PackageType.Id;
                                int? zoneId = warehousePositionZone.Zone != null ? warehousePositionZone.Zone.Id : (int?)null;

                                DetachEntity(warehousePositionZone.Zone);

                                warehousePositionZone.Zone = testPackageList.First(p => p.Zone.Id == zoneId).Zone;
                            }
                        }
                    }

                    warehouseEntities.SaveChanges(); // test

                    if (warehouseDocumentType != WarehouseDocumentTypeEnum.Pz && warehouseDocumentType != WarehouseDocumentTypeEnum.PzKC)
                    {
                        
                        UpdateRelations(warehouseDocument, newPositionWithQuantityAndShelfs, positionWithQuantityAndShelf);
                    }

                    warehouseEntities.SaveChanges();
                }

                var warehouseDocumentPositionsList = new List<WarehouseDocumentPosition>(newPositionWithQuantityAndShelfList.Select(p => p.position));

                operations.UpdateCurrentStack(warehouseDocument, warehouseDocumentPositionsList);

                // Warehouse warehouse = source != null ? source : target;

                warehouseEntities.SaveChanges();

                var warehouseId = sourceWarehouseId ?? targetWarehouseId.Value;
                int serialNumberForWh = GetNumberOfDocuments(warehouseDocumentType, warehouseId, warehouseDocument.TimeStamp.Year);

                var warehouse = warehouseEntities.Warehouses
                  .Include(w => w.Company)
                  .Single(w => w.Id == warehouseId);

                string docNumber = string.Format("{0}/{1}/{2}/{3}/{4:D4}"
                  , warehouse.Company.NameAbbreviation
                  , warehouse.NumLabel
                  , warehouseDocument.WarehouseDocumentType.ShortType.ToUpper()
                  , warehouseDocument.TimeStamp.Year
                  , serialNumberForWh);

                warehouseDocument.Number = docNumber;
                //if (warehouseDocument.TargetOrderId != null)
                //{
                //    var torder = warehouseEntities.Orders.Where(x => x.Id == warehouseDocument.TargetOrderId).FirstOrDefault();
                //    // sprawdzamy czy to przypisanie na dział
                //    if (torder.TypeId == 2)
                //    {
                //        // przypisanie na dział
                //        if (warehouseDocument.ExportToSIMPLE)
                //        {
                //            var wt = warehouseDocument.WarehouseTargetId.Value;

                //            targetDeptSIMPLEId = GetTargetDepartmentInSimple(wt, targetDeptSIMPLEId,true);
                //            //warehouseDocument.TargetResponsibleDeptSIMPLEId = targetDeptSIMPLEId;
                //        }
                        
                //    }
                //}

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        private int? GetTargetDepartmentInSimple(int? targetWarehouseId, int? targetDeptSIMPLEId, int? targetOrderId)
        {
            if (!targetDeptSIMPLEId.HasValue && targetWarehouseId.HasValue)
            //if (targetWarehouseId.HasValue)
            {
                var mainDeptWarehouse = warehouseEntities.WarehouseDepartments
                  .Where(p => p.WarehouseId == targetWarehouseId && p.IsDefaultForProduction)
                  .FirstOrDefault();

                if (mainDeptWarehouse != null)
                {
                    using (var transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        var companyId = warehouseEntities.Warehouses
                          .Where(p => p.Id == targetWarehouseId)
                          .First()
                          .CompanyId;

                        var deptsSimple = GetAllDepartmentsSIMPLE(companyId);

                        var matchingDeptSimple = deptsSimple
                            //.OrderBy(p => p.DepartmentMachineId)
                          .Where(p => p.DepartmentId == mainDeptWarehouse.Id && (!p.DepartmentMachineId.HasValue))
                          .FirstOrDefault();

                        if (targetOrderId != null)
                        {
                            var torder = warehouseEntities.Orders.Where(x => x.Id == targetOrderId).FirstOrDefault();
                            // sprawdzamy czy to przypisanie na dział
                            if (torder.TypeId == 2)
                            {
                                matchingDeptSimple = deptsSimple 
                                  .Where(p => p.DepartmentId == mainDeptWarehouse.DepartmentId && (!p.DepartmentMachineId.HasValue))
                                  .FirstOrDefault();
                            }
                        }



                        if (matchingDeptSimple != null)
                        {
                            targetDeptSIMPLEId = (int)matchingDeptSimple.komorka_id;
                        }
                        transactionScope2.Complete();
                    }
                }
            }
            return targetDeptSIMPLEId;
        }

        private void EnsureUserHasPrivilegeToPerformOperation(int? sourceWarehouseId, int? targetWarehouseId, int authorId, WarehouseDocumentTypeEnum warehouseDocumentType)
        {
            var users = GetSystemUsersForPerson(authorId).ToArray();
            StringBuilder exSb = new StringBuilder();
            bool hasPrivilege = false;

            if (users.Any())
            {
                foreach (var user in users)
                {
                    string exMessage = "";
                    using (var transactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        exMessage = CheckOperationAvailability(sourceWarehouseId, targetWarehouseId, warehouseDocumentType, user.Id);
                        transactionScope2.Complete();
                    }
                    if (exMessage.Length == 0)
                    {
                        hasPrivilege = true;
                        break;
                    }
                    else
                    {
                        exSb.Append(" [Użytkownik <b>" + user.Login + "</b>]: " + exMessage + "<br />");
                    }
                }
            }
            else
            {
                exSb.Append("Error in fetching SystemUser");
            }

            if (!hasPrivilege)
            {
                throw new Exception(exSb.ToString());
            }
        }

        /// <summary>
        /// Returns the child Position of the given Position, which exists on the given Document (only 1st level of descendants)
        /// </summary>
        /// <param name="position"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        private WarehouseDocumentPosition GetChildPositionMadeByDocument(WarehouseDocumentPosition position, WarehouseDocument document)
        {
            WarehouseDocumentPosition thePosition = null;
            // Collecting relations for the given position
            IQueryable<WarehouseDocumentPositionRelation> childrenRelations =
                from relation in warehouseEntities.WarehouseDocumentPositionRelations
                  .Include("WarehouseDocumentPosition")
                where relation.WarehouseDocumentPosition.Id == position.Id
                select relation;

            foreach (WarehouseDocumentPositionRelation relation in childrenRelations)
            {
                // WARNING! Test it intensively!!!
                WarehouseDocumentPosition positionToConsider = relation.WarehouseDocumentPosition1;
                if (positionToConsider != null)
                {
                    if (document.WarehouseDocumentPositions.Contains(positionToConsider))
                    {
                        thePosition = positionToConsider;
                        break;
                    }
                }
            }

            return thePosition;
        }

        private Person GetNewByOldPerson(Mag.Domain.Model.Person oldPerson)
        {
            return warehouseEntities.People.Where(p => p.Id == oldPerson.Id).FirstOrDefault();
        }

        private Warehouse GetNewByOldWarehouse(Mag.Domain.Model.Warehouse oldWarehouse)
        {
            return warehouseEntities.Warehouses.Where(p => p.Id == oldWarehouse.Id).FirstOrDefault();
        }

        private WarehouseDocumentPositionExtension GetNewByOldWarehouseDocumentPositionExtension(Mag.Domain.Model.WarehouseDocumentPositionExtension oldWarehouseDocumentPositionExtension)
        {
            return warehouseEntities.WarehouseDocumentPositionExtensions.Where(p => p.Id == oldWarehouseDocumentPositionExtension.Id).FirstOrDefault();
        }

        private WarehousePositionZone GetNewByOldWarehousePositionZone(Mag.Domain.Model.WarehousePositionZone oldWarehousePositionZone)
        {
            return warehouseEntities.WarehousePositionZones.Where(p => p.Id == oldWarehousePositionZone.Id).FirstOrDefault();
        }

        private Mag.Domain.Model.WarehouseDocument GetOldByNewWarehouseDocument(WarehouseDocument newWarehouseDocument)
        {
            return warehouseEntities.WarehouseDocuments.Where(p => p.Id == newWarehouseDocument.Id).FirstOrDefault();
        }

        private List<List<PositionNode>> GetPathsBunchFromCache(WarehouseDocumentPosition position)
        {
            if (this.positionBunchCache != null)
            {
                return positionBunchCache.GetBunch(position);
            }
            return null;
        }

        private WarehouseDocument Give(int warehouseSourceId, int warehouseTargetId, int authorId, List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum warehouseDocumentType, DateTime date, bool exportDocToSIMPLE = true, int? targetDeptSIMPLEId = null, int? targetOrderId = null, int docSubtypeSIMPLE = 1)
        {
#if DEBUG
            Contract.Requires(warehouseSourceId > 0);
            Contract.Requires(warehouseTargetId > 0);
#endif

            WarehouseDocument warehouseDocument = null;
            //Warehouse warehouseSource;
            //Warehouse warehouseTarget;
            //Person person;

            if (warehouseDocumentType != WarehouseDocumentTypeEnum.MMMinus && warehouseDocumentType != WarehouseDocumentTypeEnum.Rw && warehouseDocumentType != WarehouseDocumentTypeEnum.Za && warehouseDocumentType != WarehouseDocumentTypeEnum.POMinus) throw new ArgumentException("Parameter can be MMMinus, POMinus, Zw or Rw only", "warehouseDocumentType");

            foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelf in positionWithQuantityAndShelfses)
            {
                if (positionWithQuantityAndShelf.position.Quantity >= 0) throw new ArgumentException("For MMMinus quantity must be less than 0");
            }

            //TranslateParameters(warehouseSourceId, warehouseTargetId, authorId, out warehouseSource, out warehouseTarget, out person);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                //if (CheckOrderWasNotAlreadyGiven(warehouseDocument.Id))
                //{
                //}

                if (CheckAvailableQuantities(warehouseSourceId, positionWithQuantityAndShelfses) == false)
                {
                    throw new DataException("We cannot give more than we have");
                }

                warehouseDocument = CreateWarehouseDocumentBasedOnWarehouseOperation(warehouseSourceId, warehouseTargetId, authorId,
                                                           positionWithQuantityAndShelfses, warehouseDocumentType, date, exportDocToSIMPLE, targetDeptSIMPLEId, targetOrderId, docSubtypeSIMPLE);
                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocument;
        }

        private string HTMLEncodeSpecialChars(string text)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (char c in text)
            {
                if (c > 127) // special chars
                    sb.Append(String.Format("&#{0};", (int)c));
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }

        //public RadComboBoxData GetTelericWarehouseList(RadComboBoxContext context)
        //{
        //    RadComboBoxData result = new RadComboBoxData();
        //    string filtr = context.Text;

        private WarehouseDocument InternalGiveForOrder(int orderDocumentId, int userId, DateTime date,
               bool exportDocToSimple = true, int? targetDeptSIMPLEId = null, int? targetOrderId = null, int docSubtypeSIMPLE = 1)
        {
            WarehouseDocument warehouseDocumentGiven = null;

            WarehouseDocument warehouseDocument =
              warehouseEntities.WarehouseDocuments
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehousePositionZones))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.Material))
              .Include(x => x.Warehouse)
              .Include(x => x.Warehouse1)
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.ProductionMachine))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.ProductionMachine.Department))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.ProductionMachine.ProductionMachineScheduleItems))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.Order))
              .Include(x => x.ProductElementStructure)
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionQuantityChanges.Select(z => z.QuestionAnswer)))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionQuantityChanges.Select(z => z.QuestionAnswer1)))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionRelations))
              .Include(x => x.WarehouseDocumentPositions.Select(y => y.WarehouseDocumentPositionRelations1))
              .First(x => !x.TimeDeleted.HasValue && x.Id == orderDocumentId);

            WarehouseDocumentPosition[] warehouseDocumentPositions = warehouseDocument.WarehouseDocumentPositions.ToArray();
            List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();

            if (!targetOrderId.HasValue && warehouseDocumentPositions.Count() > 0)
            {
                // get order
                var order = warehouseDocumentPositions.First().Order;
                if (order != null)
                {
                    targetOrderId = order.Id;
                }
            }

            // For some unknown reason WarehouseDocument with Type=10 (ZA) can have only 1 position.
            // To remove this constraint one has to first analyze full WMS documents workflow to ensure consistency.
            if (warehouseDocumentPositions.Count() > 1)
            {
                throw new Exception("Cannot give more than 1 position per document!");
            }

            List<Tuple<int, int>> docChangedMaterials;
            if (HttpContext.Current.Session["INVChangedMaterials"] == null)
                docChangedMaterials = new List<Tuple<int, int>>();
            else
                docChangedMaterials = (List<Tuple<int, int>>)HttpContext.Current.Session["INVChangedMaterials"];

            foreach (var position in warehouseDocumentPositions)
            {
                if (position.IsLockedByQuantityChange)
                {
                    throw new Exception("Nie można wydać pozycji, oczekującej na akceptację kierownika.");
                }

                bool refetchOrderedMaterial = false;

                // TODO: To it in better way!!!!!!!!!!!!!!!!!
                Material oldMaterialToReplace = null; // TODO: To it in better way!!!!!!!!!!!!!!!!!
                // TODO: To it in better way!!!!!!!!!!!!!!!!!

                var chPosInfo = docChangedMaterials.FirstOrDefault(chp => chp.Item1 == position.Id);
                if (chPosInfo != null)
                {
                    oldMaterialToReplace = position.Material;
                    var newMaterial = GetMaterialById(chPosInfo.Item2); // DataOnly? ??? //DataOnly

                    position.Material = newMaterial;
                    position.UnitPrice = 0;
                    refetchOrderedMaterial = true;

                    //warehouseEntities.Detach(position);
                }

                // Marking position to refetch materials if quantity ahs changed
                if (Math.Abs(position.Quantity) != Math.Abs(position.ActualQuantity))
                {
                    refetchOrderedMaterial = true;
                }

                var warehouseDocumentPositionTemp = CreateWarehouseDocumentPositionDataOnly(
                    position.Material.Id,
                    position.UnitPrice,
                    (decimal)-position.ActualQuantity,
                    position.Id,
                    position.Order != null ? position.Order.Id : 0);

                warehouseDocumentPositionTemp.ProcessIdpl = position.ProcessIdpl;
                warehouseDocumentPositionTemp.ProcessIddpl = position.ProcessIddpl;

                List<WarehousePositionZone> warehousePositionZones; // = GetShelfsForPostionDataOnly(position);

                List<ChildParentPositionQuantity> positionParentIdss = new List<ChildParentPositionQuantity>();

                if (!refetchOrderedMaterial)
                {
                    warehousePositionZones = GetShelfsForPostionDataOnly(position);
                    foreach (var wpz in warehousePositionZones)
                    {
                        positionParentIdss.Add(new ChildParentPositionQuantity()
                        {
                            PositionParentID = position.Id,
                            Quantity = Math.Abs((decimal)wpz.Quantity),
                            LocationId = wpz.Zone.Id
                        });
                    }
                }
                else
                {
                    // Cancelling relations to free stacks, taken by order doc:
                    // TODO: Find out which one of these is necessary:
                    foreach (var wdpr in position.WarehouseDocumentPositionRelations1)
                    {
                        wdpr.TimeCancelled = DateTime.Now;
                    }

                    // TODO: Do it in different way!!!
                    warehousePositionZones = new List<WarehousePositionZone>();
                    warehousePositionZones.Add(new WarehousePositionZone()
                    {
                        WarehouseDocumentPosition = new WarehouseDocumentPosition() { Id = position.Id },
                        Zone = new Zone() { Id = 6292 },
                        Quantity = Math.Abs(position.ActualQuantity)
                    });

                    // Dodanie pustej lokalizacji
                    positionParentIdss.Add(new ChildParentPositionQuantity()
                    {
                        PositionParentID = position.Id,
                        Quantity = Math.Abs((decimal)position.ActualQuantity),
                        LocationId = 6292 // TODO: Do it in better way!
                    });
                }

                if (oldMaterialToReplace != null)
                    position.Material = oldMaterialToReplace;


                positionWithQuantityAndShelfses.Add(new PositionWithQuantityAndShelfs()
                {
                    position = warehouseDocumentPositionTemp,
                    WarehousePositionZones = warehousePositionZones,
                    documentParentIds = new List<int>() { warehouseDocument.Id },
                    positionParentIds = positionParentIdss, //new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { PositionParentID = position.Id, Quantity = -position.Quantity } },
                    AutoParentCollection = refetchOrderedMaterial,
                    RefetchParentOrderedMaterial = refetchOrderedMaterial
                });


                //warehouseEntities.Detach(warehouseDocumentPositionTemp);
            }



            if (CheckOrderWasNotAlreadyGiven(warehouseDocument.Id))
            {
                // TODO: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // TODO: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // TODO: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                // TODO: IF QUANTITY CHANGED - CANCEL ALL PREVIOUS "Za" takes from Stacks

                // TODO: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // TODO: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // TODO: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                warehouseDocumentGiven = Give(warehouseDocument.WarehouseSourceId ?? -1, warehouseDocument.WarehouseTargetId ?? -1,
                                                                        userId, positionWithQuantityAndShelfses,
                                                                        WarehouseDocumentTypeEnum.MMMinus, date, exportDocToSimple, targetDeptSIMPLEId, targetOrderId, docSubtypeSIMPLE);

                warehouseDocumentGiven.ProductionMachineScheduleItemId = warehouseDocument.ProductionMachineScheduleItemId;
                warehouseDocumentGiven.ProductElementStructure = warehouseDocument.ProductElementStructure;

            }
            else
            {
                throw new Exception("The document has already been given.");
            }

            // GENERATE PP DOC (MM+)

            //if (warehouseIdForMMplus > 0)
            {
                //Warehouse warehouseTarget = warehouseDocument.Warehouse1;

                foreach (PositionWithQuantityAndShelfs position in positionWithQuantityAndShelfses)
                {
                    position.position.Quantity = Math.Abs(position.position.Quantity);

                    foreach (var posParent in position.positionParentIds)
                    {
                        posParent.Quantity = 0;
                    }
                }

                // Modify structure
                foreach (PositionWithQuantityAndShelfs positionWithQuantityAndShelfs in positionWithQuantityAndShelfses)
                {
                    // clear related stuff
                    positionWithQuantityAndShelfs.positionParentIds.Clear();
                    positionWithQuantityAndShelfs.documentParentIds.Clear();

                    //// Add position parent ids

                    WarehousePositionZone warehousePositionZone = new WarehousePositionZone();
                    warehousePositionZone.Zone = warehouseEntities.Zones.First(p => p.Id == 6292);
                    warehousePositionZone.Quantity = (from p in positionWithQuantityAndShelfs.WarehousePositionZones select p.Quantity).Sum();

                    warehouseEntities.Detach(warehousePositionZone);

                    positionWithQuantityAndShelfs.WarehousePositionZones = new List<WarehousePositionZone>() { warehousePositionZone };

                    // Add document parent id
                    positionWithQuantityAndShelfs.documentParentIds.Add(warehouseDocumentGiven.Id);

                    Order forOrder = null;
                    if (targetOrderId.HasValue && targetOrderId.Value > 0)
                    {
                        forOrder =
                            warehouseEntities.Orders.Where(p => p.Id == targetOrderId.Value).FirstOrDefault();
                    }
                    else
                    {
                        forOrder = positionWithQuantityAndShelfs.position.Order;
                    }

                    positionWithQuantityAndShelfs.position =
                        CreateWarehouseDocumentPositionDataOnly(
                        //positionWithQuantityAndShelfs.position.Material.Id,
                            positionWithQuantityAndShelfs.position.MaterialId,
                            positionWithQuantityAndShelfs.position.UnitPrice,
                            (decimal)positionWithQuantityAndShelfs.position.Quantity,
                            0,
                            forOrder != null ? forOrder.Id : 0,
                            positionWithQuantityAndShelfs.position.ProcessIdpl,
                            positionWithQuantityAndShelfs.position.ProcessIddpl);

                    positionWithQuantityAndShelfs.order = positionWithQuantityAndShelfs.position.Order;
                }

                Person personSys = warehouseEntities.People.First(p => p.FirstName == "System" && p.LastName == "K3"); // System K3
                WarehouseDocument docPP = CreateWarehouseDocumentBasedOnWarehouseInternalOperation(warehouseDocument.WarehouseSourceId, warehouseDocument.WarehouseTargetId, personSys.Id, positionWithQuantityAndShelfses, WarehouseDocumentTypeEnum.PP, date, false, targetDeptSIMPLEId, targetOrderId, null);
                docPP.ProductionMachineScheduleItemId = warehouseDocument.ProductionMachineScheduleItemId;
                docPP.ProductElementStructure = warehouseDocument.ProductElementStructure;
            }

            warehouseEntities.SaveChanges();
            return warehouseDocumentGiven;
        }

        //    return result;
        //}
        private WarehouseDocument InternalReceive(int givenDocumentId, int userId, List<WarehouseDocumentPosition> positionsList, bool orderCheck, DateTime date)
        {
            WarehouseDocument warehouseDocumentReceived = null;

            WarehouseDocument warehouseDocument =
                warehouseEntities.WarehouseDocuments
                .Include("WarehouseDocumentPosition")
                .Include("WarehouseDocumentPosition.Material")
                .Include("Warehouse")
                .Include("Warehouse1")
                .Include("WarehouseDocumentPosition.Order")
                .First(d => d.Id == givenDocumentId);

            WarehouseDocumentPosition[] warehouseDocumentPositions = warehouseDocument.WarehouseDocumentPositions.ToArray();
            List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();

            foreach (var position in warehouseDocumentPositions)
            {
                var warehouseDocumentPositionTemp = CreateWarehouseDocumentPositionDataOnly(position.Material.Id,
                                                                                                                  position.UnitPrice,
                                                                                                                  (decimal)-position.Quantity,
                                                                                                                  position.Id,
                                                                                                                  position.Order != null ? position.Order.Id : 0);

                List<WarehousePositionZone> warehousePositionZone = positionsList != null
                                                                        ? new List<WarehousePositionZone>(
                                                                              positionsList.First(p => p.Id == position.Id).WarehousePositionZones)
                                                                        : null;

                positionWithQuantityAndShelfses.Add(new PositionWithQuantityAndShelfs()
                {
                    position = warehouseDocumentPositionTemp,
                    WarehousePositionZones = warehousePositionZone,
                    documentParentIds = new List<int>() { warehouseDocument.Id },
                    positionParentIds = new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { PositionParentID = position.Id, Quantity = (decimal)-position.Quantity } }
                });
            }

            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (!orderCheck || CheckGiveWasNotAlreadyReceived(warehouseDocument.Id))
                {
                    warehouseDocumentReceived = InternalReceive(warehouseDocument.Warehouse.Id, warehouseDocument.Warehouse1.Id,
                                                                            userId, positionWithQuantityAndShelfses,
                                                                            WarehouseDocumentTypeEnum.MMPlus, date);
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocumentReceived;
        }

        //    result.Items = allTypesRadComboBoxItemData.ToArray();
        private WarehouseDocument InternalReceiveDocumentTypeSet2(int givenDocumentId, int userId, List<WarehouseDocumentPosition> positionsList, bool orderCheck, DateTime date, WarehouseDocumentTypeEnum warehouseDocumentType)
        {
            WarehouseDocument warehouseDocumentReceived = null;

            WarehouseDocument warehouseDocument =
                warehouseEntities.WarehouseDocuments
                .Include("WarehouseDocumentPosition")
                .Include("WarehouseDocumentPosition.Material")
                .Include("Warehouse")
                .Include("Warehouse1")
                .Include("WarehouseDocumentPosition.Order")
                .Include("ProductionMachineScheduleItem")
                .Include("ProductElementStructure")
                .First(d => d.Id == givenDocumentId);

            WarehouseDocumentPosition[] warehouseDocumentPositions = warehouseDocument.WarehouseDocumentPositions.ToArray();
            List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();

            foreach (var position in warehouseDocumentPositions)
            {
                WarehouseDocumentPosition warehouseDocumentPositionTemp = CreateWarehouseDocumentPositionDataOnly(position.Material.Id,
                                                                                                                  position.UnitPrice,
                                                                                                                  (decimal)-position.Quantity,
                                                                                                                  position.Id,
                                                                                                                  position.Order != null ? position.Order.Id : 0);

                List<WarehousePositionZone> warehousePositionZone = positionsList != null
                                                                        ? new List<WarehousePositionZone>(
                                                                              positionsList.First(p => p.Id == position.Id).WarehousePositionZones)
                                                                        : null;

                positionWithQuantityAndShelfses.Add(new PositionWithQuantityAndShelfs()
                {
                    position = warehouseDocumentPositionTemp,
                    WarehousePositionZones = warehousePositionZone,
                    documentParentIds = new List<int>() { warehouseDocument.Id },
                    positionParentIds = new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { PositionParentID = position.Id, Quantity = (decimal)-position.Quantity } }
                });
            }

            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (!orderCheck || CheckGiveWasNotAlreadyReceived(warehouseDocument.Id))
                {
                    warehouseDocumentReceived = InternalReceive(warehouseDocument.Warehouse.Id, warehouseDocument.Warehouse1.Id,
                                                                            userId, positionWithQuantityAndShelfses,
                                                                            warehouseDocumentType, date);

                    warehouseDocumentReceived.ProductionMachineScheduleItemId = warehouseDocument.ProductionMachineScheduleItemId;
                    warehouseDocumentReceived.ProductElementStructure = warehouseDocument.ProductElementStructure;
                }

                warehouseEntities.SaveChanges();
                transactionScope.Complete();
                warehouseEntities.AcceptAllChanges();
            }

            return warehouseDocumentReceived;
        }

        private void StorePathBunchInCache(List<List<PositionNode>> bunch, WarehouseDocumentPosition position)
        {
            if (this.positionBunchCache != null)
            {
                positionBunchCache.AddBunch(bunch, position);
            }
        }

        public int? GetDocumentTypeForWarehouseDocumentPositionId(int? warehouseDocumentPositionId)
        {
            return (from wdp in warehouseEntities.WarehouseDocumentPositions
                    join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                    where wdp.Id == warehouseDocumentPositionId
                    select wd.DocumentTypeId).FirstOrDefault();
        }

        private string ToSafeSqlXmlString(string input)
        {
            return HTMLEncodeSpecialChars(input);
        }

        [Obsolete("This function is going to be removed in next version")]
        private void TranslateParameters(int? warehouseSourceId, int? warehouseTargetId, int authorId, out Warehouse warehouseSource, out Warehouse warehouseTarget, out Person person)
        {
            warehouseSource = warehouseSourceId != null ? warehouseEntities.Warehouses.Include("WarehouseType").First(p => p.Id == warehouseSourceId.Value) : null;
            warehouseTarget = warehouseTargetId != null ? warehouseEntities.Warehouses.First(p => p.Id == warehouseTargetId.Value) : null;

            //SystemUser user = warehouseEntities.SystemUser.FirstOrDefault(p => p.Id == authorId);

            person = warehouseEntities.People.Where(p => p.Id == authorId).First();

            //person = GetPersonForSystemUser(authorId);
        }

        private void UpdateContractingPartyForWarehouseDocument(int? contractingPartyInSIMPLE, int companyId, WarehouseDocument warehouseDocument)
        {
            var contractingPartyId = UpdateContractingPartyInLocal(contractingPartyInSIMPLE, companyId);
            var contractingParty = warehouseEntities.ContractingParties.FirstOrDefault(c => c.Id == contractingPartyId && c.Company != null && c.Company.Id == companyId);
            if (contractingParty != null)
            {
                var contractingPartyForWarehouseDocument = new ContractingPartyForWarehouseDocument();
                contractingPartyForWarehouseDocument.ContractingParty = contractingParty;
                contractingPartyForWarehouseDocument.WarehouseDocument = warehouseDocument;

                warehouseEntities.ContractingPartyForWarehouseDocuments.AddObject(contractingPartyForWarehouseDocument);
                warehouseEntities.SaveChanges();
            }
        }

        private int UpdateContractingPartyInLocal(int? contractingPartySIMPLEId, int companyId)
        {
            ContractingParty contractingPartyInDb = warehouseEntities.ContractingParties.FirstOrDefault(c => c.KontrahentSIMPLEId == contractingPartySIMPLEId && c.Company.Id == companyId);

            if (contractingPartyInDb != null) return contractingPartyInDb.Id;

            C_ImportSimple_ContractingPartyFromSimple2 C_ImportSimple_ContractingPartyFromSimple2 =
                warehouseEntities.C_ImportSimple_ContractingPartyFromSimple2.First(cs => cs.ContractingPartyId == contractingPartySIMPLEId && cs.CompanyId == companyId);

            Company company = warehouseEntities.Companies.Where(c => c.Id == companyId).FirstOrDefault();

            ContractingParty contractingParty = new ContractingParty();
            contractingParty.Name = C_ImportSimple_ContractingPartyFromSimple2.ContractingPartyName;
            contractingParty.KontrahentSIMPLEId = (int)contractingPartySIMPLEId;
            contractingParty.Company = company;
            warehouseEntities.AddToContractingParties(contractingParty);
            warehouseEntities.SaveChanges();

            return contractingParty.Id;
        }

        private void UpdateRelations(WarehouseDocument warehouseDocument, PositionWithQuantityAndShelfs newPositionWithQuantityAndShelfs, PositionWithQuantityAndShelfs positionWithQuantityAndShelf)
        {
            bool isOk = true;

            if (positionWithQuantityAndShelf.documentParentIds != null)
            {
                foreach (int id in positionWithQuantityAndShelf.documentParentIds)
                {
                    // sprawdzamy zgodność typów do mm-
                    if (warehouseDocument.DocumentTypeId == (int)WarehouseDocumentTypeEnum.MMMinus)
                    {
                        int parentDocType = warehouseEntities.WarehouseDocuments.Where(x => x.Id == id).Select(x => x.DocumentTypeId).First();
                        if (parentDocType == (int)WarehouseDocumentTypeEnum.Za)
                        {
                            isOk = true;
                        }
                        else
                            isOk = false;
                    }
                    else
                    {
                        isOk = true;
                    }
                    if (isOk)
                    {
                        if (warehouseEntities
                          .WarehouseDocumentRelations
                          .FirstOrDefault(r =>
                            r.WarehouseDocument.Id == id
                            && r.WarehouseDocument1.Id == warehouseDocument.Id) == null
                          )
                        {
                            var warehouseDocumentRelation = new WarehouseDocumentRelation();

                            warehouseDocumentRelation.WarehouseDocument = GetWarehouseDocumentById(id);
                            warehouseDocumentRelation.WarehouseDocument1 = warehouseDocument;

                            warehouseEntities.SaveChanges();
                        }
                    }
                }
            }

            if (positionWithQuantityAndShelf.positionParentIds != null)
            {
                foreach (var cppq in positionWithQuantityAndShelf.positionParentIds)
                {

                    // sprawdzamy zgodność typów do mm-
                    if (warehouseDocument.DocumentTypeId == (int)WarehouseDocumentTypeEnum.MMMinus)
                    {
                        int parentDocType = warehouseEntities.WarehouseDocumentPositions
                            .Include(x => x.WarehouseDocument)
                            .Where(x => x.Id == cppq.PositionParentID).Select(x => x.WarehouseDocument.DocumentTypeId).First();


                        if (parentDocType == (int)WarehouseDocumentTypeEnum.Za)
                        {
                            isOk = true;
                        }
                        else
                            isOk = false;
                    }
                    else
                    {
                        isOk = true;
                    }

                    if (isOk)
                    {
                        var warehouseDocumentPositionRelationFromDb =
                            warehouseEntities.WarehouseDocumentPositionRelations
                            .FirstOrDefault
                            (r =>
                              r.WarehouseDocumentPosition.Id == cppq.PositionParentID
                              && r.WarehouseDocumentPosition1.Id == newPositionWithQuantityAndShelfs.position.Id
                              && r.Zone.Id == cppq.LocationId
                            );
                        if (warehouseDocumentPositionRelationFromDb == null)
                        {
                            //if (cppq.Quantity <= 0) throw new ApplicationException("Quantity in a relation must be greater than 0");

                            //if (cppq.Quantity < 0 )
                            //throw new ApplicationException("Quantity in a relation must be >= 0");

                            var warehouseDocumentPositionRelation = new WarehouseDocumentPositionRelation();

                            warehouseDocumentPositionRelation.WarehouseDocumentPosition =
                                GetWarehouseDocumentPositionById(cppq.PositionParentID);
                            warehouseDocumentPositionRelation.WarehouseDocumentPosition1 = newPositionWithQuantityAndShelfs.position;
                            warehouseDocumentPositionRelation.Quantity = (double)cppq.Quantity;
                            warehouseDocumentPositionRelation.Zone = warehouseEntities.Zones.Where(z => z.Id == cppq.LocationId).FirstOrDefault();
                        }
                        else
                        {
                            warehouseDocumentPositionRelationFromDb.Quantity += (double)cppq.Quantity;
                        }
                    }

                    warehouseEntities.SaveChanges();
                }
                
            }
        }

        private WarehouseInventoryPosition UpdateWipFromNewer(WarehouseInventoryPosition pws, WarehouseInventoryPosition awipfd)
        {
            awipfd.Quantity = pws.Quantity;
            awipfd.QuantityBefore = pws.QuantityBefore;
            return awipfd;
        }

        public string GetOrderNumber(int simpleDepartmentId, int currentCompanyId)
        {
#if DEBUG
            Contract.Requires(simpleDepartmentId > 0);
            Contract.Requires(currentCompanyId > 0);
#endif

            var orderDept =
              warehouseEntities
              .OrderDepartments
              .Include(od => od.Order)
              .First(od =>
                od.Order.CompanyId == currentCompanyId &&
                od.SimpleDepartmentId == simpleDepartmentId
              );

            //var orderDept2 = warehouseEntities.OrderDepartments
            if (orderDept != null)
                return orderDept.Order.OrderNumber;
            else
                return "";
        }

        public string GetWarehouseDocumentSourceTypAndNumberByPositionId(int positionId)
        {
            var relationId = GetDocumentParentIdsByPositionId(positionId).FirstOrDefault();

            if (relationId == 0)
                return "";

            var warehouseDocument = warehouseEntities.WarehouseDocumentRelations
                .Where(x => x.Id == relationId)
                .Select(x => x.WarehouseDocument).FirstOrDefault();

            if (warehouseDocument == null)
                return "";

            var documentSource = warehouseDocument.WarehouseDocumentType.ShortType
                + (warehouseDocument.Number != null ? "</br>[ " + warehouseDocument.Number + " ]" : "")
                .ToString();

            return documentSource;
        }

        public Dictionary<int, double> GetOrderedQuantity(int? warehouseId, List<int> materialIds, int? orderIdToFind, int? shelfIdToFind)
        {
            int zaEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            int zlEnumInt = (int)WarehouseDocumentTypeEnum.ZL;
            int umzEnumInt = (int)WarehouseDocumentTypeEnum.UMZ;
            int inwEnumInt = (int)WarehouseDocumentTypeEnum.INW;

            Dictionary<int, double> materialsList = new Dictionary<int, double>();

            List<int> inventoryDocIds = (from wdp in warehouseEntities.WarehouseDocumentPositions
                                         join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                         join rel in warehouseEntities.WarehouseDocumentPositionRelations on wdp.Id equals rel.ChildId
                                         where wd.DocumentTypeId == inwEnumInt &&
                                               wd.WarehouseSourceId == warehouseId &&
                                               wd.TimeDeleted == null &&
                                               materialIds.Contains(wdp.MaterialId) &&
                                               (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                                join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                                join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                                where wdp.Id == r.ChildId &&
                                                (wd2.DocumentTypeId == zaEnumInt || wd2.DocumentTypeId == zlEnumInt || wd2.DocumentTypeId == umzEnumInt)
                                                select new { C1 = 1 }).FirstOrDefault().C1 != null
                                         select rel.ParentId).ToList();

            var quantityOrdered = warehouseEntities.WarehouseDocumentPositions
                .Include(x => x.WarehouseDocument.Warehouse1)
                .Where(q => !q.WarehouseDocument.TimeDeleted.HasValue
                    && q.WarehouseDocument.Warehouse.Id == warehouseId
                    && q.WarehouseDocument.WarehouseDocumentType.Id == zaEnumInt
                    && materialIds.Contains(q.MaterialId)
                    && (!q.WarehouseDocumentPositionRelations.Any())
                    && !inventoryDocIds.Contains(q.Id))
                .Select(q => new { q.MaterialId, Quantity = Math.Abs(q.Quantity), q.OrderId });

            var quantityOrderedZL = (from wdp in warehouseEntities.WarehouseDocumentPositions
                                     join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                     where wd.TimeDeleted == null &&
                                           wd.WarehouseSourceId == warehouseId &&
                                           wd.DocumentTypeId == zlEnumInt &&
                                           materialIds.Contains(wdp.MaterialId) &&
                                           (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                            join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                            join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                            where r.ChildId == wdp.Id &&
                                                  wd2.DocumentTypeId == zaEnumInt
                                            select new { C1 = 1 }).FirstOrDefault().C1 != null &&
                                            (from r2 in warehouseEntities.WarehouseDocumentPositionRelations
                                             join wdp3 in warehouseEntities.WarehouseDocumentPositions on r2.ChildId equals wdp3.Id
                                             join wd3 in warehouseEntities.WarehouseDocuments on wdp3.WarehouseDocumentId equals wd3.Id
                                             where wdp.Id == r2.ParentId &&
                                                   wd3.DocumentTypeId == umzEnumInt
                                             select new { C1 = 1 }).FirstOrDefault().C1 == null &&
                                            !inventoryDocIds.Contains(wdp.Id)
                                     select new
                                     {
                                         wdp.MaterialId,
                                         Quantity = Math.Abs(wdp.Quantity),
                                         wdp.OrderId
                                     });

            var quantityOrderedUMZ = (from wdp in warehouseEntities.WarehouseDocumentPositions
                                      join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                      where wd.TimeDeleted == null &&
                                            wd.WarehouseSourceId == warehouseId &&
                                            wd.DocumentTypeId == umzEnumInt &&
                                            materialIds.Contains(wdp.MaterialId) &&
                                            (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                             join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                             join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                             where r.ChildId == wdp.Id &&
                                                   (wd2.DocumentTypeId == zaEnumInt || wd2.DocumentTypeId == zlEnumInt)
                                             select new { C1 = 1 }).FirstOrDefault().C1 != null &&
                                             (from r2 in warehouseEntities.WarehouseDocumentPositionRelations
                                              join wdp3 in warehouseEntities.WarehouseDocumentPositions on r2.ChildId equals wdp3.Id
                                              join wd3 in warehouseEntities.WarehouseDocuments on wdp3.WarehouseDocumentId equals wd3.Id
                                              where wdp.Id == r2.ParentId &&
                                                    (wd3.DocumentTypeId == zlEnumInt)
                                              select new { C1 = 1 }).FirstOrDefault().C1 == null &&
                                             !inventoryDocIds.Contains(wdp.Id)
                                      select new
                                      {
                                          wdp.MaterialId,
                                          Quantity = Math.Abs(wdp.Quantity),
                                          wdp.OrderId
                                      });

            if (orderIdToFind.HasValue)
            {
                quantityOrdered = quantityOrdered.Where(x => x.OrderId == orderIdToFind);
                quantityOrderedZL = quantityOrderedZL.Where(x => x.OrderId == orderIdToFind);
                quantityOrderedUMZ = quantityOrderedUMZ.Where(x => x.OrderId == orderIdToFind);
            }

            var materialsJoined = (from ordered in quantityOrdered
                                   group ordered by new { ordered.MaterialId } into og
                                   select new
                                   {
                                       MaterialId = og.Key.MaterialId,
                                       Quantity = og.Sum(m => m.Quantity)
                                   }).Concat(from ordered in quantityOrderedZL
                                             group ordered by new { ordered.MaterialId } into og
                                             select new
                                             {
                                                 MaterialId = og.Key.MaterialId,
                                                 Quantity = og.Sum(m => m.Quantity)
                                             }).Concat(from ordered in quantityOrderedUMZ
                                                       group ordered by new { ordered.MaterialId } into og
                                                       select new
                                                       {
                                                           MaterialId = og.Key.MaterialId,
                                                           Quantity = og.Sum(m => m.Quantity)
                                                       }).ToList();

            foreach (var item in materialsJoined)
            {
                if (materialsList.ContainsKey(item.MaterialId))
                    materialsList[item.MaterialId] += item.Quantity;
                else
                    materialsList.Add(item.MaterialId, item.Quantity);
            }

            return materialsList;
        }

        public List<WarehouseDocumentPosition> GetOrderedStackList(int? warehouseId, int materialId, int? orderIdToFind, int? shelfIdToFind)
        {
            int zaEnumInt = (int)WarehouseDocumentTypeEnum.Za;
            int zlEnumInt = (int)WarehouseDocumentTypeEnum.ZL;
            int inwEnumInt = (int)WarehouseDocumentTypeEnum.INW;
            int umzEnumInt = (int)WarehouseDocumentTypeEnum.UMZ;

            //double returnQauntity = 0;
            //double? quantity = null;
            //List<double> positions;

            List<int> inventoryDocIds = (from wdp in warehouseEntities.WarehouseDocumentPositions
                                         join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                         join rel in warehouseEntities.WarehouseDocumentPositionRelations on wdp.Id equals rel.ChildId
                                         where wd.DocumentTypeId == inwEnumInt &&
                                               wd.WarehouseSourceId == warehouseId &&
                                               wd.TimeDeleted == null &&
                                               wdp.MaterialId == materialId &&
                                               (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                                join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                                join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                                where wdp.Id == r.ChildId &&
                                                (wd2.DocumentTypeId == zaEnumInt || wd2.DocumentTypeId == zlEnumInt || wd2.DocumentTypeId == umzEnumInt)
                                                select new { C1 = 1 }).FirstOrDefault().C1 != null
                                         select rel.ParentId).ToList();

            var quantityOrdered = (from wdp in warehouseEntities.WarehouseDocumentPositions
                                   join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                   //join wdpqc in warehouseEntities.WarehouseDocumentPositionQuantityChanges on wdp.Id equals wdpqc.WarehouseDocumentPositionId
                                   where wd.DocumentTypeId == zaEnumInt &&
                                         wd.WarehouseSourceId == warehouseId &&
                                         wd.TimeDeleted == null &&
                                         wdp.MaterialId == materialId &&
                                         (!wdp.WarehouseDocumentPositionRelations.Any() &&
                                         !inventoryDocIds.Contains(wdp.Id))
                                         //|| (wdp.WarehouseDocumentPositionRelations.Any() && wdp.WarehouseDocumentPositionQuantityChanges.Any()) && 
                                         //!inventoryDocIds.Contains(wdp.Id)
                                   select wdp).Concat(
                                   from wdp in warehouseEntities.WarehouseDocumentPositions
                                   join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                   where wd.DocumentTypeId == zlEnumInt &&
                                         wd.WarehouseSourceId == warehouseId &&
                                         wd.TimeDeleted == null &&
                                         wdp.MaterialId == materialId &&
                                         (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                          join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                          join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                          where wdp.Id == r.ChildId &&
                                                wd2.DocumentTypeId == zaEnumInt
                                          select new { C1 = 1 }).FirstOrDefault().C1 != null &&
                                          (from r2 in warehouseEntities.WarehouseDocumentPositionRelations
                                           join wdp3 in warehouseEntities.WarehouseDocumentPositions on r2.ChildId equals wdp3.Id
                                           join wd3 in warehouseEntities.WarehouseDocuments on wdp3.WarehouseDocumentId equals wd3.Id
                                           where wdp.Id == r2.ParentId &&
                                                 wd3.DocumentTypeId == umzEnumInt
                                           select new { C1 = 1 }).FirstOrDefault().C1 == null &&
                                          !inventoryDocIds.Contains(wdp.Id)
                                   select wdp).Concat(
                                   from wdp in warehouseEntities.WarehouseDocumentPositions
                                   join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                   where wd.DocumentTypeId == umzEnumInt &&
                                         wd.WarehouseSourceId == warehouseId &&
                                         wd.TimeDeleted == null &&
                                         wdp.MaterialId == materialId &&
                                         (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                          join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                          join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                          where wdp.Id == r.ChildId &&
                                                (wd2.DocumentTypeId == zaEnumInt || wd2.DocumentTypeId == zlEnumInt)
                                          select new { C1 = 1 }).FirstOrDefault().C1 != null &&
                                          (from r2 in warehouseEntities.WarehouseDocumentPositionRelations
                                           join wdp3 in warehouseEntities.WarehouseDocumentPositions on r2.ChildId equals wdp3.Id
                                           join wd3 in warehouseEntities.WarehouseDocuments on wdp3.WarehouseDocumentId equals wd3.Id
                                           where wdp.Id == r2.ParentId &&
                                                 (wd3.DocumentTypeId == zlEnumInt)
                                           select new { C1 = 1 }).FirstOrDefault().C1 == null &&
                                          !inventoryDocIds.Contains(wdp.Id)
                                   select wdp).Concat(
                                   from wdp in warehouseEntities.WarehouseDocumentPositions
                                   join wd in warehouseEntities.WarehouseDocuments on wdp.WarehouseDocumentId equals wd.Id
                                   where wd.DocumentTypeId == zlEnumInt &&
                                         wd.WarehouseSourceId == warehouseId &&
                                         wd.TimeDeleted == null &&
                                         wdp.MaterialId == materialId &&
                                         (from r in warehouseEntities.WarehouseDocumentPositionRelations
                                          join wdp2 in warehouseEntities.WarehouseDocumentPositions on r.ParentId equals wdp2.Id
                                          join wd2 in warehouseEntities.WarehouseDocuments on wdp2.WarehouseDocumentId equals wd2.Id
                                          join r2 in warehouseEntities.WarehouseDocumentPositionRelations on r.ParentId equals r2.ChildId
                                          join wdp3 in warehouseEntities.WarehouseDocumentPositions on r2.ParentId equals wdp3.Id
                                          join wd3 in warehouseEntities.WarehouseDocuments on wdp3.WarehouseDocumentId equals wd3.Id
                                          where wdp.Id == r.ChildId &&
                                                wd2.DocumentTypeId == umzEnumInt &&
                                                wd3.DocumentTypeId == zaEnumInt
                                          select new { C1 = 1 }).FirstOrDefault().C1 != null &&
                                          !inventoryDocIds.Contains(wdp.Id)
                                   select wdp).DistinctBy(x => new { x.MaterialId, x.Quantity, x.OrderId, x.ProcessIdpl, x.ProcessIddpl }).ToList();

            return quantityOrdered;

            //if (orderIdToFind.HasValue)
            //{
            //    quantityOrdered = quantityOrdered.Where(x => x.OrderId == orderIdToFind);
            //}

            //if (shelfIdToFind.HasValue)
            //{
            //    var PositionsIds = quantityOrdered.Select(x => x.Id);
            //    positions = warehouseEntities.WarehousePositionZones.Where(x => x.ZoneId == shelfIdToFind && PositionsIds.Contains(x.PositionId)).Select(x => x.Quantity).ToList();
            //}
            //else
            //{
            //    positions = quantityOrdered.Select(x => x.Quantity).ToList();
            //}
            //quantity = positions.Sum();

            //returnQauntity = quantity != null ? (double)quantity : returnQauntity;
        }

        public string GetShelfIds(int companyId, int warehouseId, string indexSIMPLE)
        {
            string shelfIds = warehouseEntities.GetShelfIds(companyId, warehouseId, indexSIMPLE).FirstOrDefault();

            return shelfIds;
        }

        public IQueryable<ZoneParentPath> GetShelfListFromCacheWithPrompt(string[] ids)
        {
            return this.GetActiveZonesWithParentsList()
              .Where
              (p => p.IsActive
                    &&
              (ids.Contains(SqlFunctions.StringConvert((double)p.Id).Trim()))
            );
        }



    }


}