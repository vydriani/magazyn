﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;

namespace Mag.Domain
{
	
	/// <summary>
	/// Wrapper for WarehouseDocumentPosition object reference together with some of it's info
	/// </summary>
	public class PositionNode
	{
		private enum PositionNodeDescription
		{
			Pz,
			PwGOOD,
			PwDAMAGED,
			Wz,
			Rw,
			MMMinus,
			MMPlus,
			PrMinus,
			PrPlus,
			BO,
			Za,
			ZwMinus,
			ZwPlus,
			Other,

			Package,
			Garbage
		}

		public WarehouseDocumentPosition Position;

		public string TextCode
		{
			get
			{
				string docShortType = "";

				using (var warehouseDS = new WarehouseDS())
				{
					docShortType =
							warehouseDS.GetWarehouseDocumentForPositionId(this.Position.Id).WarehouseDocumentType.
									ShortType.
									Trim();

					List<PositionNodeDescription> keys = new List<PositionNodeDescription>();

					// Adding production and damage status:
					bool damaged = warehouseDS.IsPositionDamaged(this.Position);
					keys.Add(GetPositionNodeCodeKeyByDocTypeAndDamageStatus(docShortType, damaged));

					// Adding position type status:
					List<string> types = warehouseDS.GetPositionSpecialTypes(this.Position);

					foreach (string type in types)
					{
						keys.Add(GetPositionNodeCodeKeyByPositionType(type));
					}

					return GetPositionNodeCode(keys);
				}
			}
		}

		public PositionNode(WarehouseDocumentPosition position)
		{
			this.Position = position;
		}


		/// <summary>
		/// Returns a code
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		private string GetPositionNodeCode(List<PositionNodeDescription> keys)
		{
			string value = "";

			Dictionary<PositionNodeDescription, string> dic = new Dictionary<PositionNodeDescription, string>();
			dic.Add(PositionNodeDescription.Pz, "A");
			dic.Add(PositionNodeDescription.PwGOOD, "B");
			dic.Add(PositionNodeDescription.PwDAMAGED, "C");
			dic.Add(PositionNodeDescription.Wz, "D");
			dic.Add(PositionNodeDescription.Rw, "E");
			dic.Add(PositionNodeDescription.MMMinus, "F");
			dic.Add(PositionNodeDescription.MMPlus, "G");
			dic.Add(PositionNodeDescription.PrMinus, "H");
			dic.Add(PositionNodeDescription.PrPlus, "I");
			dic.Add(PositionNodeDescription.BO, "J");
			dic.Add(PositionNodeDescription.Za, "K");
			dic.Add(PositionNodeDescription.ZwMinus, "L");
			dic.Add(PositionNodeDescription.ZwPlus, "M");

			dic.Add(PositionNodeDescription.Package, "Q");
			dic.Add(PositionNodeDescription.Garbage, "V");

			dic.Add(PositionNodeDescription.Other, "O");

			foreach (PositionNodeDescription key in keys)
			{
				try
				{
					value += dic[key];
				}
				catch (KeyNotFoundException)
				{
					return "X";
				}
			}

			return value;

			//try
			//{
			//    value = dic[key];
			//    return value;
			//}
			//catch (KeyNotFoundException)
			//{
			//    return "X";
			//}
		}

		/// <summary>
		/// Returns a code for given ShortType and damage status of single Position
		/// </summary>
		/// <param name="docShortType"></param>
		/// <param name="damaged"></param>
		/// <returns></returns>
		private static PositionNodeDescription GetPositionNodeCodeKeyByDocTypeAndDamageStatus(string docShortType, bool damaged)
		{
			if (docShortType == "Pz")
				return PositionNodeDescription.Pz;
			if (docShortType == "Pw")
			{
				if (!damaged)
				{
					return PositionNodeDescription.PwGOOD;
				}
				return PositionNodeDescription.PwDAMAGED;
			}
			if (docShortType == "Wz")
				return PositionNodeDescription.Wz;
			if (docShortType == "Rw")
				return PositionNodeDescription.Rw;
			if (docShortType == "MM-")
				return PositionNodeDescription.MMMinus;
			if (docShortType == "MM+")
				return PositionNodeDescription.MMPlus;
			if (docShortType == "Pr-")
				return PositionNodeDescription.PrMinus;
			if (docShortType == "Pr+")
				return PositionNodeDescription.PrPlus;
			if (docShortType == "Pr+")
				return PositionNodeDescription.PrPlus;
			if (docShortType == "Za")
				return PositionNodeDescription.Za;
			if (docShortType == "Zw-")
				return PositionNodeDescription.ZwMinus;
			if (docShortType == "Zw+")
				return PositionNodeDescription.ZwPlus;

			// BO document type is not taken under consideration at the moment            

			return PositionNodeDescription.Other;
		}

		private static PositionNodeDescription GetPositionNodeCodeKeyByPositionType(string posType)
		{
			if (posType == "Package")
			{
				return PositionNodeDescription.Package;
			}
			if (posType == "Garbage")
			{
				return PositionNodeDescription.Garbage;
			}

			return PositionNodeDescription.Other;
		}

	}
}
