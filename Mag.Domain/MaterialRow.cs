﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;

namespace Mag.Domain
{
  public class MaterialRow : EntityObject
  {
    public int Id { get; set; }

    public string IndexSIMPLE { get; set; }

    public int MaterialId { get; set; }

    public string MaterialName { get; set; }

    public string NameSIMPLE { get; set; }

    public string UnitName { get; set; }

    public string ExtendedName { get; set; }
  }
}
