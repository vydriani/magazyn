﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;
using System.Diagnostics.Contracts;

namespace Mag.Domain
{
  public class CustomsChamberViewDetails
  {
    public WarehouseCustomsChamberPosition Position;


    public int Lp { get; set; }
    public double? KCQuantity { get; set; }
    public string KCStateTypeName { get; set; }
    public string KCSolvingDocumentTypeName { get; set; }
    public string KCSolvingDocumentTypeShortName { get; set; }
    public DateTime? TimeRequired { get; set; }
    public DateTime? KCSolvingDocumentDate { get; set; }
    public string KCSolvingDocumentPositionOrderNumber { get; set; }
    public int? WarehouseDocumentPositionKCStateId { get; set; }
    public bool IsDateEditEnabled { get; set; }
    public string Comment { get; set; }

    public CustomsChamberViewDetails(WarehouseCustomsChamberPosition p, int lp, bool isEditEnabled)
    {
      Contract.Requires(p != null);

      Lp = lp;
      KCQuantity = p.KCQuantity;
      KCStateTypeName = p.KCStateTypeName;
      KCSolvingDocumentTypeName = p.KCSolvingDocumentTypeName;
      KCSolvingDocumentTypeShortName = p.KCSolvingDocumentTypeShortName;
      TimeRequired = p.TimeRequired;
      KCSolvingDocumentDate = p.KCSolvingDocumentDate;
      KCSolvingDocumentPositionOrderNumber = p.KCSolvingDocumentPositionOrderNumber;
      WarehouseDocumentPositionKCStateId = p.WarehouseDocumentPositionKCStateId;
      IsDateEditEnabled = isEditEnabled && !p.IsFinished;
      Comment = p.Comment;
    }
  }
}
