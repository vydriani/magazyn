﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;

namespace Mag.Domain
{
    public class InventoryPosition
    {
        public WarehouseDocumentPosition position { get; set; }
        public Zone shelf { get; set; }
        public Order order { get; set; }
        public Department department { get; set; }

        public int WarehouseId { get; set; }
        public DateTime AddedTime { get; set; }
        public DateTime ObservationTime { get; set; }

        public double QuantityBefore { get; set; }
        public double QuantityInv
        {
            get { return position.Quantity; }
        }

        public double QuantityDiff
        {
            get { return QuantityInv - QuantityBefore; }
        }
    }
}
