﻿namespace Mag.Domain.Data
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;

  public class CustomsChamberMaterialsGridElement
  {
    public int Lp { get; set; }

    public int WarehouseDocumentPositionId { get; set; }

    public int OrderId { get; set; }

    public string ContractingPartyName { get; set; }

    public string OrderNumberFromPZ { get; set; }

    public string IndexSIMPLE { get; set; }

    public string MaterialName { get; set; }

    public decimal? QuantityOrdered { get; set; }

    public double? QuantityReceivedKC { get; set; }

    public string UnitName { get; set; }

    public string OrderNumber { get; set; }

    public string Assignment { get; set; }

    public string ShelfName { get; set; }

    public DateTime? ArrivalDate { get; set; }

    public double QuantityKCAssigned { get; set; }

    public double QuantityKCAssignedFinalized { get; set; }

    public double QuantityKCNotAssigned { get; set; }
  }
}
