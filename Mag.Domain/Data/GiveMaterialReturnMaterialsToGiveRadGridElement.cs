﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Data
{
 public class GiveMaterialReturnMaterialsToGiveRadGridElement
  {
    public string MaterialName { get; set; }

    public string IndexSIMPLE { get; set; }

    public string NameSIMPLE { get; set; }

    public double? Quantity { get; set; }

    public string UnitName { get; set; }

    public decimal? UnitPrice { get; set; }

    public string OrderNumber { get; set; }

    public int MaterialId { get; set; }

    public int? OrderId { get; set; }

    public int? SourceWarehouseId { get; set; }

    public string SourceWarehouseName { get; set; }
  }
}
