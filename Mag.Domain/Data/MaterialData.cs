﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mag.Domain.Data
{
	public class MaterialData
	{
		public int? SimpleId { get; set; }
		public int? CompanyId { get; set; }
		public string MaterialName { get; set; }
		public string IndexSIMPLE { get; set; }
		public string IndexSimpleK3 { get; set; }
		public string UnitName { get; set; }
	}
}
