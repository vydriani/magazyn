﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;
using System.Data.Entity;

namespace Mag.Domain.Data
{
    public class BaseWarehouseDocumentPositionLayer
    {
        protected WarehouseEntities _warehouseEntities;
        protected int? DocumentPositionId;
        private IQueryable<WarehouseDocumentPositionRelation> myDocumentPositionRelation;

        #region ctr
        
        public BaseWarehouseDocumentPositionLayer()
            : this(new WarehouseEntities())
        {
            _warehouseEntities.CommandTimeout = 600;
        }

        public BaseWarehouseDocumentPositionLayer(WarehouseEntities warehouseEntities)
        {
            _warehouseEntities = warehouseEntities;
        }

        public BaseWarehouseDocumentPositionLayer(int id) 
            : this(new WarehouseEntities(), id) { }

        public BaseWarehouseDocumentPositionLayer(WarehouseEntities warehouseEntities, int _id)
        {
            _warehouseEntities = warehouseEntities;
            DocumentPositionId = _id;
        }

        #endregion

        #region fields
        public int Id
        {
            get
            {
                return DocumentPositionId.GetValueOrDefault();
            }
            set
            {
                DocumentPositionId = value;
            }
        }
        public int DocType { get; set; }
        public int WarehouseDocumentId { get; set; }
        public int WarehouseDocumentParentId { get; set; }
        public Order Order { get; set; }
        public DateTime? WarehouseDocumentDueTime { get; set; }
        public double Quantity { get; set; }
        public int? IDpl { get; set; }
        public int? IDDpl { get; set; }
        public int ShelfId { get; set; }
        public int MaterialId { get; set; }

        public double GivenQuantity
        {
            get;
            set;
        }

        private IQueryable<WarehouseDocumentPositionRelation> isDocumentPositionRelation
        {
            get
            {
                if (myDocumentPositionRelation == null)
                    return GetDocumentPositionRelation();
                return myDocumentPositionRelation;
            }
        }

        #endregion

        #region methods
        private IQueryable<WarehouseDocumentPositionRelation> GetDocumentPositionRelation()
        {
            myDocumentPositionRelation = _warehouseEntities
                .WarehouseDocumentPositionRelations
                .Where(x => x.WarehouseDocumentPosition.Id == this.DocumentPositionId);
            return myDocumentPositionRelation;
        }
        #endregion

    }
}
