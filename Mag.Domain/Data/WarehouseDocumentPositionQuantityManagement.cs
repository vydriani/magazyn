﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;
using System.Data.Entity;


namespace Mag.Domain.Data
{
    public static class WarehouseDocumentPositionQuantityManagement
    {
        public static void UpdateQuantity(int positionId, int UserId)
        {
            double QuantityToUpdate;
            using (WarehouseEntities _warehouseEntities = new WarehouseEntities())
            {
                var DocumentPosition = _warehouseEntities
                    .WarehouseDocumentPositions
                    .Where(x => x.WarehouseDocument.Id == positionId)
                    .Select(x => new
                    {
                        quantity = x.Quantity,
                        positionId = x.Id
                    })
                    .FirstOrDefault();

                var ReleaseQuantity = _warehouseEntities
                    .WarehouseDocumentPositionRelations
                    .Where(x => x.WarehouseDocumentPosition.Id == DocumentPosition.positionId)
                    .Sum(x => x.Quantity);

                if (ReleaseQuantity!=0)
                {
                    QuantityToUpdate = -(Math.Abs(DocumentPosition.quantity) - Math.Abs(ReleaseQuantity));

                    using (WarehouseDS warehouseDs = new WarehouseDS())
                    {
                        warehouseDs.ChangeWarehouseDocumentPositionQuantity(DocumentPosition.positionId, QuantityToUpdate, UserId);
                    }
                }
            }
        }
    }
}
