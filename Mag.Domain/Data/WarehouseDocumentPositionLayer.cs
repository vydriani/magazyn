﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mag.Domain.Model;
using System.Data.Entity;


namespace Mag.Domain.Data
{
    public class WarehouseDocumentPositionLayer:BaseWarehouseDocumentPositionLayer
    {
        private WarehouseDocumentPositionQuantityChange myQuantityQuery;
        private bool _isNullZone;

        #region ctr
        public WarehouseDocumentPositionLayer(int _id) : base(new WarehouseEntities(), _id) { }
        public WarehouseDocumentPositionLayer() { }
        #endregion

        #region fields

        public WarehouseDocumentPosition WarehouseDocumentPositionWithZone { get; set; }
        public Warehouse Warehouse { get; set; }
        public Material Material { get; set; }
        public bool isNullZone
        {
            get
            {
                if (WarehouseDocumentPositionWithZone == null && DocumentPositionId.HasValue)
                {
                    WarehouseDocumentPositionWithZone = _GetWarehouseDocumentPositionWithZone();
                    _isNullZone = WarehouseDocumentPositionWithZone.WarehousePositionZones.Count > 1;
                }
                
                _isNullZone = WarehouseDocumentPositionWithZone.WarehousePositionZones.Count > 1;
                
                return _isNullZone;
            }
        }
        public string TargetWarehouse { get; set; }
        public string TargetProductionMachineName { get; set; }
        public string DepartmentName { get; set; }
        public int OrderId { get; set; }
        public int? ProcessIdpl { get; set; }
        public DateTime WarehouseDocumentTimeStamp { get; set; }
        public string MaterialShortName { get; set; }
        public double AvaibleQuantity { get; set; }
        public double QuantityDiff
        {
            get
            {
                if (MyQuantityChange != null)
                {
                    return Math.Abs(Math.Abs(Quantity) + MyQuantityChange.QuantityDiff);
                }
                return Math.Abs(Quantity);
            }
        }
        private WarehouseDocumentPositionQuantityChange MyQuantityChange
        {
            get
            {
                if (myQuantityQuery == null)
                    return GetPositionQuantityChanges();
                return myQuantityQuery;
            }
        }
        public bool IsLockedByQuantityChange
        {
            get { return myQuantityQuery != null ? myQuantityQuery.QuestionAnswer != null && !myQuantityQuery.QuestionAnswer.TimePlaced.HasValue : false; }
        }

        #endregion

        #region methods

        private WarehouseDocumentPositionQuantityChange GetPositionQuantityChanges()
        {
            myQuantityQuery = _warehouseEntities.WarehouseDocumentPositionQuantityChanges
                            .Include(x => x.QuestionAnswer)
                            .Include(x => x.QuestionAnswer1)
                            .Where(q => q.WarehouseDocumentPositionId == this.DocumentPositionId)
                            .FirstOrDefault(p => !p.TimeCancelled.HasValue && (p.QuestionAnswer == null || !p.QuestionAnswer.TimePlaced.HasValue));
                            

            return myQuantityQuery;
        }

        private WarehouseDocumentPosition _GetWarehouseDocumentPositionWithZone()
        { 
            var documentPosition = _warehouseEntities
                .WarehouseDocumentPositions
                .Include(x=>x.WarehousePositionZones.Select(y=>y.Zone))
                .Where(x => x.Id == DocumentPositionId).FirstOrDefault();
            return documentPosition;
        }

        private WarehouseDocumentPosition _GetWarehouseDocumentPositionWithMaterial()
        {
            var documentPosition = _warehouseEntities
                .WarehouseDocumentPositions
                .Include(x=>x.Material)
                .Where(x => x.Id == DocumentPositionId).FirstOrDefault();
            return documentPosition;
        }

        public WarehouseDocumentPosition GetWarehouseDocumentPositionWithMaterial(int PositionId)
        {
            DocumentPositionId = PositionId;
            return _GetWarehouseDocumentPositionWithMaterial();
        }

        public WarehouseDocumentPosition GetWarehouseDocumentPositionWithZone(int PositionId)
        {
            DocumentPositionId = PositionId;
            return _GetWarehouseDocumentPositionWithZone();
        }

        #endregion

    }
}


