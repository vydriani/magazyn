using System;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;
using Mag.Domain;
using Mag.Domain.Model;
using MagWeb.Extensions;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;
using  System.Data.Entity;
using System.Collections.Generic;

namespace MagWeb
{
    public partial class Document : FeaturedSystemWebUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                inputDate.SelectedDate = DateTime.Now.Date;
                inputDateTo.SelectedDate = DateTime.Now.Date;
            }

            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            if (!Page.IsPostBack)
            {
                //txtName.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
                cmbMaterial.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
                cmbSupplier.WebServiceSettings.Method = "GetTelericContractingPartyListCompany" + companyId.ToString();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Activate or deactivate print button
            this.button_PrintDocument.Enabled = warehouseDocumentRadGrid.SelectedItems.Count > 0;
        }

        protected void SearchClick(object sender, EventArgs e)
        {

        }

        protected void WarehouseDocumentRadGrid_SelectRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick" && e.Item is GridDataItem)
            {
                warehouseDocumentRadGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                positionsRadGrid.Rebind();

                // Bind print button
                GridDataItem item = warehouseDocumentRadGrid.SelectedItems[0] as GridDataItem;

                int selectedDocumentId = item.GetCellValueAs<int>("Id");
                if (selectedDocumentId > 0)
                {
                    int selectedDocumentTypeId = item.GetCellValueAs<int>("DocumentTypeId");
                    DocumentPrintingUtils.BindDocumentToButton(this.button_PrintDocument, (WarehouseDocumentTypeEnum)selectedDocumentTypeId, selectedDocumentId);

                    positionsRadGrid.Columns[positionsRadGrid.Columns.Count - 1].Visible = (selectedDocumentTypeId == 1);
                    positionsRadGrid.Columns[positionsRadGrid.Columns.Count - 2].Visible = (selectedDocumentTypeId == 1);

                    positionPanel.Visible = true;

                }
                else
                {
                    positionPanel.Visible = false;
                }
                hfUnlock.Value = "";
                hfReturn.Value = "";
            }
        }

        protected void PositionsRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int selectedDocumentId = -1;

            if (warehouseDocumentRadGrid.SelectedItems.Count == 1)
            {
                GridDataItem item = warehouseDocumentRadGrid.SelectedItems[0] as GridDataItem;
                selectedDocumentId = item.GetCellValueAs<int>("Id");
            }

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var shelfs = from p in warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(selectedDocumentId)
                             select
                               new
                               {
                                   Id = p.position.Id,
                                   MaterialName = p.position.Material.NameShort,
                                   IndexSIMPLE = p.position.Material.IndexSIMPLE, //.MaterialSIMPLE.FirstOrDefault() != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().IndexSIMPLE : null,
                                   NameSIMPLE = p.position.Material.Name, //.MaterialSIMPLE.FirstOrDefault() != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().NameSIMPLE : null,
                                   Quantity = p.quantity,
                                   UnitName = p.position.Material.Unit.ShortName,
                                   UnitPrice = p.position.UnitPrice,
                                   OrderNumber = p.position.IsKC ? "~ nadwyżka ~" : (p.position.Order != null ? p.position.Order.OrderNumber : (p.position.Department != null ? p.position.Department.Name + (p.position.ProductionMachine != null ? "/" + p.position.ProductionMachine.Name : "") : "stan magazynowy")),
                                   ShelfName = p.shelf.Name,
                                   ProcessIdpl = p.position.ProcessIdpl,
                                   IsKJ = p.IsKJ
                               };

                //if (!string.IsNullOrEmpty(txtName.Text))
                //    shelfs = shelfs.Where(s => !string.IsNullOrEmpty(s.MaterialName) && s.MaterialName.ToLower().Contains(txtName.Text.ToLower()));

                //if (!string.IsNullOrEmpty(txtIndex.Text))
                //    shelfs = shelfs.Where(s => !string.IsNullOrEmpty(s.IndexSIMPLE) && s.IndexSIMPLE.ToLower().Contains(txtIndex.Text.ToLower()));

                //if (!string.IsNullOrEmpty(txtOrderNumber.Text))
                //    shelfs = shelfs.Where(s => !string.IsNullOrEmpty(s.OrderNumber) && s.OrderNumber.ToLower().Contains(txtOrderNumber.Text.ToLower()));

                //if (!string.IsNullOrEmpty(cmbShelf.Text))
                //    shelfs = shelfs.Where(s => !string.IsNullOrEmpty(s.ShelfName) && s.ShelfName.ToLower().Contains(cmbShelf.Text.ToLower()));
                shelfs = shelfs.ToList().Distinct();

                positionsRadGrid.DataSource = shelfs;
            }
        }

        protected void Grid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == GridRebindReason.InitialLoad)
                return;

            WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData);
            {
                DateTime? date = inputDate.SelectedDate;
                DateTime? date2 = inputDateTo.SelectedDate;

                //if (date != null)
                //{
                //    date = date.Value.Date.AddDays(1.0);
                //}

                bool isFreeMode = currentWarehouseOnly.Checked; // Request.QueryString["f"] == "ree";

                var warehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
                var companyId = (int?)Session[SessionData.SessionKey_CurrentCompanyId];

                int? docId = null;
                if (!string.IsNullOrEmpty(textBoxDocNumber.Text))
                {
                    int _docId;
                    if (int.TryParse(textBoxDocNumber.Text, out _docId))
                    {
                        docId = _docId;
                    }
                    else
                    {
                        textBoxDocNumber.Text = "";
                    }
                }



                var documents = warehouseDs.GetAllWarehouseDocuments
                  (
                    date,
                    isFreeMode,
                    companyId,
                    warehouseId,
                    docId,
                    cmbWarehouseSource.Text,
                    cmbWarehouseTarget.Text,
                    cmbDocumentType.Text,
                    cmbAuthor.Text,
                    cmbMaterial.Text,
                    cmbOrder.Text,
                    cmbDepartment.Text,
                    cmbSupplier.Text,
                    orderNumberTextbox.Text,
                    date2);

                var documentsToDisplay = documents
                  .ToList()
                  .Select
                  (p =>
                    new
                    {
                        Id = p.Id,
                        Number = p.Number,
                        //Number = p.DisplayType.Id==27?
                        //          warehouseDs.GetWarehouseDocumentById(warehouseDs.WarehouseEntitiesTest.WarehouseDocumentRelations.Where(x => x.ChildId == p.Id).Select(x => x.ParentId).FirstOrDefault()).Number
                        //          :p.Number,
                        Author = p.Person.FirstName + " " + p.Person.LastName,
                        CreationDate = p.Date,
                        TimeStamp = p.TimeStamp,
                        PesId = (p.ProductElementStructure != null ? p.ProductElementStructure.Id : 0),
                        SimpleDocNumber =
                        (
                          p.WarehouseDataExchangeDocumentNews.FirstOrDefault(d => d.StatusId == 2) != null
                            ? p.WarehouseDataExchangeDocumentNews.FirstOrDefault(d => d.StatusId == 2).SIMPLEDocNumber.Replace(" ", "")
                            : ""
                        ),
                        SupplierName =
                          p.ContractingPartyForWarehouseDocuments.Any()
                            ? p.ContractingPartyForWarehouseDocuments.FirstOrDefault().ContractingParty.Name
                            : "-",

                        // NOTICE we're removing "Magazyn " to save some screen space
                        WarehouseSource =
                          p.Warehouse != null
                            ? p.Warehouse.Name.Replace("Magazyn ", "") +
                            (
                              isFreeMode
                                ? " [" + p.Warehouse.Company.NameAbbreviation + "]"
                                : ""
                            )
                            : " ",
                        WarehouseTarget =
                          p.Warehouse1 != null
                            ? p.Warehouse1.Name.Replace("Magazyn ", "") +
                            (
                              isFreeMode
                                ? " [" + p.Warehouse1.Company.NameAbbreviation + "]"
                                : ""
                            )
                            : " ",

                        // TODO this could be done better to extract less data from database, but it's not worth the time
                        DocumentTypeId = p.DisplayType.Id,
                        DocumentType = p.DisplayType.ShortType,

                        // TODO: Parse XML in better way!!!
                        ExternalInvoiceNr = p.WarehouseDocumentExtensions
                          .Where(q => q.InfoString.Contains("invoiceNumber"))
                          .Any()
                            ? p.WarehouseDocumentExtensions
                              .Where(q => q.InfoString.Contains("invoiceNumber"))
                              .FirstOrDefault()
                              .InfoString
                              .Replace("<document><invoiceNumber>", "")
                              .Replace("</invoiceNumber></document>", "")
                            : "-",
                    }
                   );
                warehouseDocumentRadGrid.VirtualItemCount = documentsToDisplay.Count(); // this is so that control knows how many pages to create 
                var pageSize = warehouseDocumentRadGrid.PageSize;
                var pageIndex = warehouseDocumentRadGrid.CurrentPageIndex;

                documentsToDisplay = documentsToDisplay.OrderByDescending(p => p.Id).Skip(pageSize * pageIndex).Take(pageSize); // we're extracting _only_ the current visible data
                warehouseDocumentRadGrid.DataSource = documentsToDisplay.ToArray();
                RebindGrids();
                documentsPanel.Visible = true;
                positionPanel.Visible = false;
                documentsToExportPanel.Visible = false;
            }
        }

        protected void SearchClickDocument(object sender, EventArgs e)
        {
            if (cbToExport.Checked == false)
                warehouseDocumentRadGrid.Rebind();
            else
                warehouseDocumentToExportRadGrid.Rebind();
        }

        protected void SearchClickPosition(object sender, EventArgs e)
        {
            positionsRadGrid.Rebind();
        }

        private void RebindGrids()
        {
            warehouseDocumentRadGrid.SelectedIndexes.Clear();
            positionsRadGrid.Rebind();
        }

        protected void btUnlock_Click(object sender, EventArgs e)
        {
            var ids = hfUnlock.Value.Split(',');
            int testId = 0;
            List<int> idsToUpdate = new List<int>();
            foreach (var id in ids)
            {
                if (int.TryParse(id, out testId))
                {
                    idsToUpdate.Add(testId);
                }
            }

            if (idsToUpdate.Count > 0)
            {
                using (var warehouseDs = new WarehouseDS().WarehouseEntitiesTest)
                {
                    foreach (var id in idsToUpdate)
                    {
                        var position = warehouseDs.WarehouseDocumentPositions.FirstOrDefault(x => x.Id == id);
                        position.IsKJ = false;
                        warehouseDs.SaveChanges();
                        warehouseDs.AcceptAllChanges();
                    }
                }
                RebindGrids();
                hfUnlock.Value = "";
                WebTools.AlertMsgAdd("Odblokowano wybrane pozycje");
            }
        }

        protected void btReturn_Click(object sender, EventArgs e)
        {
            string info = "";
            var ids = hfReturn.Value.Split(',');
            int testId = 0;
            var documentId=0;
            List<int> idsToReturn = new List<int>();
            foreach (var id in ids)
            {
                if (int.TryParse(id, out testId))
                {
                    idsToReturn.Add(testId);
                }
            }

            if (idsToReturn.Count > 0)
            {
                using (var warehouseDs = new WarehouseDS().WarehouseEntitiesTest)
                {
                    foreach (var id in idsToReturn)
                    {
                        var position = warehouseDs.WarehouseDocumentPositions.FirstOrDefault(x => x.Id == id);
                        if(documentId==0)
                        {
                            documentId = position.WarehouseDocumentId;
                        }

                        WarehouseDocumentPositionRelation relations = warehouseDs.WarehouseDocumentPositionRelations.FirstOrDefault(x => x.ParentId == position.Id);
                        if (relations != null)
                        { 
                            info += string.Format("Pozycja {0} była już rozchodowana i nie może być wycofana.\n\r", position.Id); 
                        }
                        else
                        {
                            var positionZone = warehouseDs.WarehousePositionZones.FirstOrDefault(x => x.PositionId == position.Id);
                            if (positionZone != null)
                            {
                                try
                                {
                                    warehouseDs.WarehousePositionZones.DeleteObject(positionZone);
                                    warehouseDs.SaveChanges();
                                    warehouseDs.AcceptAllChanges();
                                    warehouseDs.WarehouseDocumentPositions.DeleteObject(position);
                                    warehouseDs.SaveChanges();
                                    warehouseDs.AcceptAllChanges();
                                    info += string.Format("Pozycja {0} została wycofana.\n\r", position.Id);
                                }
                                catch (Exception ex)
                                {
                                    info += string.Format("Błąd podczas wycofywania pozycji {0}, {1}\n\r", position.Id, ex.Message);
                                }
                            }
                        }
                    }

                    var restPositions = warehouseDs.WarehouseDocumentPositions.Where(x => x.WarehouseDocumentId == documentId).ToList();
                    if (restPositions.Count == 0) // usuwamy dokument
                    {
                        var document = warehouseDs.WarehouseDocuments.FirstOrDefault(x => x.Id == documentId);
                        if (document != null)
                        {
                            var documentExport = warehouseDs.WarehouseDataExchangeDocumentNews.FirstOrDefault(x => x.WarehouseDocumentId == document.Id);
                            var positionExport = warehouseDs.WarehouseDataExchangeDocumentPositionNews.Where(x => x.DocumentId == documentExport.Id);
                            foreach (var item in positionExport)
                            {
                                warehouseDs.WarehouseDataExchangeDocumentPositionNews.DeleteObject(item);
                                warehouseDs.SaveChanges();
                                warehouseDs.AcceptAllChanges();
                            }
                            warehouseDs.WarehouseDataExchangeDocumentNews.DeleteObject(documentExport);
                            warehouseDs.SaveChanges();
                            warehouseDs.AcceptAllChanges();
                        }
                        warehouseDs.WarehouseDocuments.DeleteObject(document);
                        warehouseDs.SaveChanges();
                        warehouseDs.AcceptAllChanges();
                        info += string.Format("Dokument {0} został wycofany\n\r", document.Id);
                    }
                }
                warehouseDocumentRadGrid.Rebind();
                hfReturn.Value = "";
                WebTools.AlertMsgAdd(info);
            }
        }

        public class DocumentsToExport
        {
            public int Id { get; set; }
            public string WarehouseSource { get; set; }
            public string WarehouseTarget { get; set; }
            public string DocumentType { get; set; }
            public string Author { get; set; }
            public DateTime TimeStamp { get; set; }
            public DateTime CreationDate { get; set; }
            public int DocumentTypeId { get; set; }
            public string SupplierName { get; set; }
            public string ExternalInvoiceNr { get; set; }
            public int PesId { get; set; }
            public string Number { get; set; }
            public string SimpleDocNumber { get; set; }
            // pozycja
            public string MaterialName { get; set; }
            public string IndexSIMPLE { get; set; }
            public decimal Quantity { get; set; }
            public string UnitName { get; set; }
            public string OrderNumber { get; set; }
        }

        protected void warehouseDocumentToExportRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == GridRebindReason.InitialLoad)
                return;

            if (cmbDocumentType.Text != "PP" && cmbDocumentType.Text != "Za" && cmbDocumentType.Text != "Pz")
            {
                WebTools.AlertMsgAdd("Zestawienie dostępne tylko dla dokumentów PP, Za i Pz");
                return;
            }

            WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData);
            {
                DateTime? date = inputDate.SelectedDate;
                DateTime? date2 = inputDateTo.SelectedDate;


                bool isFreeMode = currentWarehouseOnly.Checked; // Request.QueryString["f"] == "ree";

                var warehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
                var companyId = (int?)Session[SessionData.SessionKey_CurrentCompanyId];

                int? docId = null;
                if (!string.IsNullOrEmpty(textBoxDocNumber.Text))
                {
                    int _docId;
                    if (int.TryParse(textBoxDocNumber.Text, out _docId))
                    {
                        docId = _docId;
                    }
                    else
                    {
                        textBoxDocNumber.Text = "";
                    }
                }

                var documents = warehouseDs.GetAllWarehouseDocuments
                  (
                    date,
                    isFreeMode,
                    companyId,
                    warehouseId,
                    docId,
                    cmbWarehouseSource.Text,
                    cmbWarehouseTarget.Text,
                    cmbDocumentType.Text,
                    cmbAuthor.Text,
                    cmbMaterial.Text,
                    cmbOrder.Text,
                    cmbDepartment.Text,
                    cmbSupplier.Text,
                    orderNumberTextbox.Text, date2);

                var documentsToDisplay = documents
                  .ToList()
                  .Select
                  (p =>
                    new DocumentsToExport
                    {
                        Id = p.Id,
                        Number = p.Number,
                        Author = p.Person.FirstName + " " + p.Person.LastName,
                        CreationDate = p.Date,
                        TimeStamp = p.TimeStamp,
                        PesId = (p.ProductElementStructure != null ? p.ProductElementStructure.Id : 0),
                        SimpleDocNumber =
                        (
                          p.WarehouseDataExchangeDocumentNews.FirstOrDefault(d => d.StatusId == 2) != null
                            ? p.WarehouseDataExchangeDocumentNews.FirstOrDefault(d => d.StatusId == 2).SIMPLEDocNumber.Replace(" ", "")
                            : ""
                        ),
                        SupplierName =
                          p.ContractingPartyForWarehouseDocuments.Any()
                            ? p.ContractingPartyForWarehouseDocuments.FirstOrDefault().ContractingParty.Name
                            : "-",

                        // NOTICE we're removing "Magazyn " to save some screen space
                        WarehouseSource =
                          p.Warehouse != null
                            ? p.Warehouse.Name.Replace("Magazyn ", "") +
                            (
                              isFreeMode
                                ? " [" + p.Warehouse.Company.NameAbbreviation + "]"
                                : ""
                            )
                            : " ",
                        WarehouseTarget =
                          p.Warehouse1 != null
                            ? p.Warehouse1.Name.Replace("Magazyn ", "") +
                            (
                              isFreeMode
                                ? " [" + p.Warehouse1.Company.NameAbbreviation + "]"
                                : ""
                            )
                            : " ",

                        // TODO this could be done better to extract less data from database, but it's not worth the time
                        DocumentTypeId = p.DisplayType.Id,
                        DocumentType = p.DisplayType.ShortType,

                        // TODO: Parse XML in better way!!!
                        ExternalInvoiceNr = p.WarehouseDocumentExtensions
                          .Where(q => q.InfoString.Contains("invoiceNumber"))
                          .Any()
                            ? p.WarehouseDocumentExtensions
                              .Where(q => q.InfoString.Contains("invoiceNumber"))
                              .FirstOrDefault()
                              .InfoString
                              .Replace("<document><invoiceNumber>", "")
                              .Replace("</invoiceNumber></document>", "")
                            : "-",
                    }
                   );

                List<DocumentsToExport> DocumentsWithPositionToDisplay = new List<DocumentsToExport>();
                foreach (var doc in documentsToDisplay)
                {

                    var shelfs = from p in warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(doc.Id)
                                 select
                                   new
                                   {
                                       MaterialName = p.position.Material.NameShort,
                                       IndexSIMPLE = p.position.Material.IndexSIMPLE, //.MaterialSIMPLE.FirstOrDefault() != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().IndexSIMPLE : null,
                                       Quantity = p.quantity,
                                       UnitName = p.position.Material.Unit.ShortName,
                                       OrderNumber = p.position.IsKC ? "~ nadwyżka ~" : (p.position.Order != null ? p.position.Order.OrderNumber : (p.position.Department != null ? p.position.Department.Name + (p.position.ProductionMachine != null ? "/" + p.position.ProductionMachine.Name : "") : "stan magazynowy")),

                                   };

                    shelfs = shelfs.ToList().Distinct();

                    foreach (var poz in shelfs)
                    {
                        DocumentsWithPositionToDisplay.Add(
                            new DocumentsToExport()
                            {
                                Id = doc.Id,
                                Number = doc.Number,
                                Author = doc.Author,
                                CreationDate = doc.CreationDate,
                                TimeStamp = doc.TimeStamp,
                                PesId = doc.PesId,
                                SimpleDocNumber = doc.SimpleDocNumber,
                                SupplierName = doc.SupplierName,
                                WarehouseSource = doc.WarehouseSource,
                                WarehouseTarget = doc.WarehouseTarget,
                                DocumentTypeId = doc.DocumentTypeId,
                                DocumentType = doc.DocumentType,
                                ExternalInvoiceNr = doc.ExternalInvoiceNr,

                                MaterialName = poz.MaterialName,
                                IndexSIMPLE = poz.IndexSIMPLE,
                                Quantity = poz.Quantity,
                                UnitName = poz.UnitName,
                                OrderNumber = poz.OrderNumber
                            });
                    }
                }
                warehouseDocumentToExportRadGrid.VirtualItemCount = DocumentsWithPositionToDisplay.Count(); // this is so that control knows how many pages to create 
                var pageSize = warehouseDocumentToExportRadGrid.PageSize;
                var pageIndex = warehouseDocumentToExportRadGrid.CurrentPageIndex;

                DocumentsWithPositionToDisplay = DocumentsWithPositionToDisplay.OrderByDescending(x => x.Id).Skip(pageSize * pageIndex).Take(pageSize).ToList();
                warehouseDocumentToExportRadGrid.DataSource = DocumentsWithPositionToDisplay;
                documentsPanel.Visible = false;
                positionPanel.Visible = false;
                documentsToExportPanel.Visible = true;
            }

        }
    }
}
