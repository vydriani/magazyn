﻿using System;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
//using Client.Domain.Logging;
using Mag.Domain;
using Mag.Domain.SecurityData;
using System.Collections.Generic;

namespace MagWeb
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            // commented out because needs to have a valid use case. The permission cache setup procedure is tested and valid.
            //SetupPermissionCache();
            PreHeatDatabase();
        }

        /// <summary>
        /// Generate query to prepare SQL Server execution engine for user login
        /// </summary>
        private void PreHeatDatabase()
        {
            var preHeatTask = System.Threading.Tasks.Task.Factory.StartNew
            (() =>
              {
                  using (var warehouseDs = new WarehouseDS())
                  {
                      //var perfDebug = new PerformanceDebugger();
                      //perfDebug.Restart();
                      var systemUserSecurityPrivilegesForUser = warehouseDs.GetSecurityPrivilegesForUser(1);
                      var systemUserSecurityPrivilegesForUserList = systemUserSecurityPrivilegesForUser.ToList();
                      //perfDebug.LogPerformance("systemUserSecurityPrivilegesForUser.ToList()");
                  }
              }
            );
        }

        //private static void SetupPermissionCache()
        //{
        //  PerformanceDebugger perfDebug = new PerformanceDebugger();
        //  perfDebug.Restart();

        //  //var cache = new Cache();
        //  int?[] allFromWarehouseIds;
        //  int?[] allToWarehouseIds;
        //  int[] allOperationIds;
        //  using (var warehouseDs = new WarehouseDS())
        //  {
        //    var allWarehouses = warehouseDs.GetAllWarehouses();
        //    var allOperations = warehouseDs.GetAllWarehouseDocumentTypes();

        //    var alaNulle = new int?[] { null, 0 };
        //    allFromWarehouseIds = allWarehouses.Select(p => (int?)p.Id).Union(alaNulle).ToArray();
        //    allToWarehouseIds = allWarehouses.Select(p => (int?)p.Id).Union(alaNulle).ToArray();
        //    allOperationIds = allOperations.Select(p => p.Id).ToArray();
        //    perfDebug.LogPerformance("(from p in allOperations select p.Id).ToArray()", false);
        //  }

        //  int allFromWarehousesLength = allFromWarehouseIds.Length;
        //  int allToWarehousesLength = allToWarehouseIds.Length;
        //  int allOperationsLength = allOperationIds.Length;

        //  // Load config & security matrix
        //  List<SecurityConfigElement> allPermissions = new List<SecurityConfigElement>();

        //  for (int i = 0; i < allFromWarehousesLength; i++)
        //  {
        //    for (int j = 0; j < allToWarehousesLength; j++)
        //    {
        //      for (int k = 0; k < allOperationsLength; k++)
        //      {
        //        int? fromWarehouseId = allFromWarehouseIds[i];
        //        int? toWarehouseId = allToWarehouseIds[j];
        //        int operationId = allOperationIds[k];

        //        allPermissions.Add(new SecurityConfigElement(fromWarehouseId, toWarehouseId, operationId, false));
        //      }
        //    }
        //  }
        //  perfDebug.LogPerformance("for: allPermissions.Add(new SecurityConfigElement", false);

        //  // "Zero"/"General" sources/targets:
        //  // From:
        //  var zerosDoAddFrom = allPermissions
        //    .Where(z => z.FromWarehouseId.HasValue)
        //    .Select(z => z.FromWarehouseId)
        //    .Distinct();
        //  perfDebug.LogPerformance("allPermissions.FromWarehouseId).Distinct()", false);

        //  foreach (int fromWarehouseId in zerosDoAddFrom)
        //  {
        //    var matchingConfigElemsAllOperations = allPermissions
        //      .Where(p => p.FromWarehouseId == fromWarehouseId)
        //      .Select(p => p.OperationId)
        //      .Distinct();

        //    foreach (int operationId in matchingConfigElemsAllOperations)
        //    {
        //      SecurityConfigElement matchingConfigElement = allPermissions
        //        .FirstOrDefault(p =>
        //          p.FromWarehouseId == fromWarehouseId
        //          && p.ToWarehouseId == 0
        //          && p.OperationId == operationId);
        //      if (matchingConfigElement != null)
        //        matchingConfigElement.IsGranted = true;
        //    }
        //  }
        //  perfDebug.LogPerformance("foreach (int fromWarehouseId in zerosDoAddFrom)", false);

        //  // To:
        //  var zerosDoAddTo = allPermissions
        //    .Where(z => z.ToWarehouseId.HasValue)
        //    .Select(z => z.ToWarehouseId)
        //    .Distinct();
        //  perfDebug.LogPerformance("allPermissions.ToWarehouseId).Distinct()", false);

        //  foreach (int toWarehouseId in zerosDoAddTo)
        //  {
        //    var matchingConfigElemsAllOperations = allPermissions
        //      .Where(p => p.ToWarehouseId == toWarehouseId)
        //      .Select(p => p.OperationId)
        //      .Distinct();

        //    foreach (int operationId in matchingConfigElemsAllOperations)
        //    {
        //      SecurityConfigElement matchingConfigElement = allPermissions
        //        .FirstOrDefault
        //        (p =>
        //          p.FromWarehouseId == 0 &&
        //          p.ToWarehouseId == toWarehouseId &&
        //          p.OperationId == operationId
        //        );
        //      if (matchingConfigElement != null)
        //        matchingConfigElement.IsGranted = true;
        //    }
        //  }
        //  perfDebug.LogPerformance("foreach (int toWarehouseId in zerosDoAddTo)", false);

        //  var securityConfigMatrix = new UserSecurityConfigMatrix(allPermissions, null);
        //  var sqlDependency = SetupPermissionCacheDependency();

        //  HttpRuntime.Cache.Insert("SecurityConfigMatrix", securityConfigMatrix, sqlDependency, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(18));//add on remove callback
        //  perfDebug.LogPerformance("cache.Insert(\"SecurityConfigMatrix\", securityConfigMatrix, sqlDependency, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(18))");
        //}

        /// <summary>
        /// Setups dependencies for permissions
        /// </summary>
        /// <param name="cache">defaults to null. <typeparamref name="HttpRuntime.Cache"/> used when <paramref name="cache"/> = null</param>
        /// <returns>relevant cache dependency</returns>
        private static CacheDependency SetupPermissionCacheDependency(Cache cache = null)
        {
            var cachedDbName = "WBCache";
            var cachedTableNameWarehouse = "Warehouse";
            var cachedTableNameWarehouseDocumentType = "WarehouseDocumentType";
            var sqlDbExeptionCount = 0;
            var sqlTableExceptionCount = 0;
            var sqlDependenciesConfigured = false;
            if (cache == null)
            {
                cache = HttpRuntime.Cache;
            }

            var aggregateCacheDependency = new AggregateCacheDependency();

            // Because of possible exceptions thrown when this 
            // code runs, use Try...Catch...Finally syntax. 
            while (!sqlDependenciesConfigured && sqlDbExeptionCount < 2 && sqlTableExceptionCount < 2)
            {
                try
                {
                    // Instantiate SqlDep using the SqlCacheDependency constructor. 
                    aggregateCacheDependency.Add(new SqlCacheDependency(cachedDbName, cachedTableNameWarehouse));
                    aggregateCacheDependency.Add(new SqlCacheDependency(cachedDbName, cachedTableNameWarehouseDocumentType));
                    sqlDependenciesConfigured = true;
                }
                // Handle the DatabaseNotEnabledForNotificationException with 
                // a call to the SqlCacheDependencyAdmin.EnableNotifications method. 
                catch (DatabaseNotEnabledForNotificationException)
                {
                    try
                    {
                        var connString = DiscoverCachedDatabaseConnectionString(cachedDbName);
                        SqlCacheDependencyAdmin.EnableNotifications(connString);
                    }

                    //// If the database does not have permissions set for creating tables, 
                    //// the UnauthorizedAccessException is thrown. Handle it by redirecting 
                    //// to an error page. 
                    //catch (UnauthorizedAccessException exPerm)
                    //{
                    //  Response.Redirect(".\\ErrorPage.htm");
                    //}
                    finally { sqlDbExeptionCount++; }
                }

            // Handle the TableNotEnabledForNotificationException with 
                // a call to the SqlCacheDependencyAdmin.EnableTableForNotifications method. 
                catch (TableNotEnabledForNotificationException)
                {
                    try
                    {
                        var connString = DiscoverCachedDatabaseConnectionString(cachedDbName);
                        SqlCacheDependencyAdmin.EnableTableForNotifications(connString, cachedTableNameWarehouse);
                        SqlCacheDependencyAdmin.EnableTableForNotifications(connString, cachedTableNameWarehouseDocumentType);
                    }

                    //// If a SqlException is thrown, redirect to an error page. 
                    //catch (SqlException exc)
                    //{
                    //  Response.Redirect(".\\ErrorPage.htm");
                    //}
                    finally { sqlTableExceptionCount++; }
                }
            }

            //// If all the other code is successful, add MySource to the Cache 
            //// with a dependency on SqlDep. If the Categories table changes, 
            //// MySource will be removed from the Cache. Then generate a message 
            //// that the data is newly created and added to the cache. 
            //finally
            //{
            //  cache.Insert("SqlSource", Source1, SqlDep);
            //  CacheMsg.Text = "The data object was created explicitly.";
            //}

            return aggregateCacheDependency;
        }

        private static string DiscoverCachedDatabaseConnectionString(string cachedDbName)
        {
            // Get the web application's web.config file.
            var config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/");

            var sqlCacheDependencySection = config.GetSection("system.web/caching/sqlCacheDependency") as System.Web.Configuration.SqlCacheDependencySection;
            if (sqlCacheDependencySection != null)
            {
                var db = sqlCacheDependencySection.Databases[cachedDbName];
                if (db != null)
                {
                    var cs = db.ConnectionStringName;
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[cs].ConnectionString;
                    return connectionString;
                }
            }
            throw new MissingFieldException("Could not find ConnectionString for cached database in \"system.web/caching/sqlCacheDependency\" section in web.config. Make sure 'caching' section is configured properly.");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
                SetupSession(User.Identity.Name);
            else
                Response.Redirect(FormsAuthentication.LoginUrl, true);
        }

        public static void SetupSession(string login)
        {
            //PerformanceDebugger perfDebug = new PerformanceDebugger();
            //perfDebug.Restart();

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var user = warehouseDs.GetUserByLogin(login);
                var userId = user.Id;
                HttpContext.Current.Session[SessionData.SessionKey_UserId] = userId;

                var accessRightsForUser = warehouseDs.GetAllInterOperationAccessRightsVirtualForUser(userId).ToList();
                //perfDebug.LogPerformance("warehouseDs.GetAllInterOperationAccessRightsVirtualForUser(userId).ToList();", false);

                System.Diagnostics.Debug.Assert(!accessRightsForUser.Any(x => x.FromWarehouseId == 0), "At least 1 instance of FromWarehouseId=0 detected. None should be.");
                System.Diagnostics.Debug.Assert(!accessRightsForUser.Any(x => x.ToWarehouseId == 0), "At least 1 instance of ToWarehouseId=0 detected. None should be.");

                var validUserPermissions = accessRightsForUser.Select(x => new SecurityConfigElement(x.FromWarehouseId, x.ToWarehouseId, x.OperationId, true));
                SessionData.ClientData.CurrentUserSecurityConfigMatrix = new UserSecurityConfigMatrix(validUserPermissions, null);
                //perfDebug.LogPerformance("new UserSecurityConfigMatrix(validUserPermissions, null)", false);

                var systemUserSecurityPrivilegesForUser = warehouseDs.GetSecurityPrivilegesForUser(userId);
                var systemUserSecurityPrivilegesForUserList = systemUserSecurityPrivilegesForUser.ToList();
                //perfDebug.LogPerformance("warehouseDs.GetSecurityPrivilegesForUser(userId).ToList()", false);
                SessionData.ClientData.SystemUserSecurityPrivileges = systemUserSecurityPrivilegesForUserList;
                //perfDebug.LogPerformance("SessionData.ClientData.SystemUserSecurityPrivileges = systemUserSecurityPrivilegesForUserList", false);

                HttpContext.Current.Session[SessionData.SessionKey_LoggedUserHasAdminPrivileges] = false;
                HttpContext.Current.Session[SessionData.SessionKey_LoggedUserHasAccessToWarehouse] = warehouseDs.UserHasSecurityPrivilege(userId, 20);
                HttpContext.Current.Session[SessionData.SessionKey_LoggedUserHasAdminPrivilegesForConfig] = warehouseDs.UserHasSecurityPrivilege(userId, 21);
                HttpContext.Current.Session[SessionData.SessionKey_LoggedUserHasAdminPrivilegesForSecurity] = warehouseDs.UserHasSecurityPrivilege(userId, 22);

                var currentWarehouseId = warehouseDs.GetUserRecentWarehouseInfo(userId);
                HttpContext.Current.Session[SessionData.SessionKey_CurrentWarehouseId] = currentWarehouseId;
                if (currentWarehouseId.HasValue)
                {
                    HttpContext.Current.Session[SessionData.SessionKey_CurrentCompanyId] = warehouseDs.GetCompanyByWarehouseId(currentWarehouseId.Value);
                }
            }
            //perfDebug.LogPerformance();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var l = new Services.LoggingService();
            var context = HttpContext.Current;
            if (context != null)
            {
                var lastEx = Context.Server.GetLastError();
                var lastExBase = lastEx.GetBaseException();
                var file = HttpContext.Current.Request.Url.ToString();
                var page = HttpContext.Current.Request.UrlReferrer.ToString();
                var urlSrc = string.Format("In: '{0}' from: '{1}'", file, page);
                var loggedUserName = context.User != null && !string.IsNullOrEmpty(context.User.Identity.Name) ? context.User.Identity.Name : "GUEST";

                l.Log
                 (
                    loggedUserName,
                    lastEx.Message + lastEx.InnerException.Message + urlSrc,
                    lastEx.StackTrace + "\nbase: " + lastExBase != null ? lastExBase.StackTrace : "null",
                    "MagWeb",
                    "Error");
            }
            else
            {
                l.Log
                (
                  "unknown",
                  "HttpContext.Current=null, no information available.",
                  string.Empty,
                  "MagWeb",
                  "Error");
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            try
            {
                FormsAuthentication.SignOut(); // sometimes it throws NullReferenceException
            }
            finally
            {
                try
                {
                    // NullReferenceException fix from: http://stackoverflow.com/questions/1430180/what-is-needed-in-the-httpcontext-to-allow-formsauthentication-signout-to-exec
                    HttpContext.Current.Request.Browser = new HttpBrowserCapabilities() { Capabilities = new Dictionary<string, string> { { "supportsEmptyStringInCookieValue", "false" } } };
                    FormsAuthentication.SignOut();
                }
                finally
                {
                    Response.Redirect(FormsAuthentication.LoginUrl, true);
                }
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
