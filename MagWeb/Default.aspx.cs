using System;
using System.Collections.Generic;
//using Client.Domain.Logging;

namespace MagWeb
{
	public partial class Default : FeaturedSystemWebUIPage
	{
    //PerformanceDebugger perfDebug = new PerformanceDebugger();

#if DEBUG
		public override System.Web.SessionState.HttpSessionState Session
		{
			get
			{
				SessionData.TestSessionFor<List<Mag.Domain.Model.WarehouseInventoryPosition>, int>("INVPositions", wip => wip.Count);
				return base.Session;
			}
		}
#endif

		protected void Page_Init(object sender, EventArgs e)
		{
		}

		protected void Page_Load(object sender, EventArgs e)
		{
      //perfDebug.Restart();

			string displayToolbar = Request.QueryString["displayToolbar"];
			if (!string.IsNullOrEmpty(displayToolbar))
			{
				bool displayToolbarBool = bool.Parse(displayToolbar);
				Session["DisplayToolbar"] = displayToolbarBool;
			}
			else
			{
				Session["DisplayToolbar"] = false;
			}

      //perfDebug.LogPerformance();
			Response.Redirect("WarehouseStacks.aspx");
		}
	}
}
