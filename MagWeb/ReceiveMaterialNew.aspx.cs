using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using MagWeb.WarehouseDocuments;
using DBLayer;
using DomainServices;
using Telerik.Web.UI;
using System.Text;

namespace MagWeb
{
    public partial class ReceiveMaterialNew : FeaturedSystemWebUIPage
    {
        private const int INDEX_PROCESS_HEADER_ID_IN_GRID = 3;
        private const int INDEX_PROCESS_SUPPLYORDERID_IN_GRID = 4;
        private const int INDEX_PROCESS_SUPPLYORDERPOSID_IN_GRID = 5;
        private const int INDEX_UNIT_PRICE_IN_GRID = 6;
        private const int INDEX_CONTRACTING_PARTY_ID_IN_GRID = 7;
        private const int INDEX_IDSIM_IN_GRID = 8;
        private const int INDEX_ORDERNUMBERPZ_IN_GRID = 11;
        private const int INDEX_AVAILABLE_QUANTITY_IN_GRID = 13;
        private const int INDEX_FOR_DEPARTMENT_IN_GRID = 16;
        private const int INDEX_FOR_SOURCETYPE_IN_GRID = 17;

        private List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses;
        //private List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfsesOverQuantity;
        private int? contractingParty;
        private string orderNumberPZ;
        private int companyId = -1;




        protected void Page_Load(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
            using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.Pz, userId)) != "" &&
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.PzKC, userId)) != "" &&
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.MMPlus, userId)) != "")
                {
                    WebTools.AlertMsgAdd(message);
                    //Session[SessionData.SessionKey_Message] = message;
					PermissionDenied();
					return;
                }
            }

            if (!Page.IsPostBack)
            {
                if (RadDatePicker1.SelectedDate == null)
                {
                    RadDatePicker1.SelectedDate = DateTime.Now.Date.AddDays(-3).Date;
                }
                if (RadDatePicker2.SelectedDate == null)
                {
                    RadDatePicker2.SelectedDate = DateTime.Now.Date.AddDays(4).Date;
                }
                //RadDatePicker2.SelectedDate = DateTime.Now.Date;
                inputDate.SelectedDate = DateTime.Now.Date;
            }

            if (!IsPostBack)
            {
                Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"] = null;
                Session["ReceiveMaterialNew_ContractinPartyId"] = null;
                Session["ReceiveMaterialNew_OrderNumberPZ"] = null;
            }

            //if ((bool)Session["IsOrderRequired"])
            //{
            //    //RadToolBar1.Tabs.First()
            //}

            if (Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"] == null)
                positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();
            else
                positionWithQuantityAndShelfses = (List<PositionWithQuantityAndShelfs>)Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"];

            companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            contractingParty = (int?)Session["ReceiveMaterialNew_ContractinPartyId"];
            orderNumberPZ = (string)Session["ReceiveMaterialNew_OrderNumberPZ"];

            this.Panel_OverQuantityConfirmation.Visible = false;


            //if (this.RadComboBox_MaterialsSIMPLE.WebServiceSettings.Method == "")
            //    this.RadComboBox_MaterialsSIMPLE.WebServiceSettings.Method = "GetTelericMaterialsSIMPLEListCompany" + companyId.ToString();

            //////this.RadComboBox_MaterialsSIMPLE.WebServiceSettings.Method = "GetTelericMaterialsSIMPLEListCompany" + companyId.ToString();

            //if (this.RadComboBox_ContractingPartySimple.WebServiceSettings.Method == "")
            //    this.RadComboBox_ContractingPartySimple.WebServiceSettings.Method = "GetTelericContractingPartySIMPLEListCompany" + companyId.ToString();

            //////this.RadComboBox_ContractingPartySimple.WebServiceSettings.Method = "GetTelericContractingPartySIMPLEListCompany" + companyId.ToString();

            //////this.RadComboBox_OrderNumber.WebServiceSettings.Method = "GetTelericOrderNumbersSIMPLEListCompany" + companyId.ToString();

            //////this.RadDatePicker_SupplyDate.SelectedDate = DateTime.Now.Date;


            //AddPackageTypeOKLabel.Visible = false;

            btnNewDocument.Enabled = false;


            if (this.TextBox_PackagesNumber.Text == "")
                this.TextBox_PackagesNumber.Text = "0";

            using (WarehouseDS warehouseDs = new WarehouseDS())
            {
                bool isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility(currentWarehouseId.Value,
                                                                                      warehouseDs.GetPersonForSystemUser
                                                                                          (userId).Id,1);
                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }
        }



        protected void Page_PreRender(object sender, EventArgs e)
        {
            //this.RadComboBox_ContractingPartySimple.DataBind();
            //this.RadComboBox_MaterialsSIMPLE.DataBind();
            //////this.RadComboBox_ContractingPartySimple.Text = "";
            //////this.RadComboBox_MaterialsSIMPLE.Text = "";
            this.cmbShelf.Text = "";
            RadComboBox_PackageTypeName.Text = "";
            //this.RadComboBox_ContractingPartySimple.ClearSelection();
            //this.RadComboBox_MaterialsSIMPLE.DataBind();

            //if(this.btnSaveDocument.Enabled) this.btnSaveDocument.Enabled = positionWithQuantityAndShelfses.Count > 0;
            this.btnSaveDocument.Visible = this.btnSaveDocument.Enabled;
        }

        protected void AddPositionClick(object sender, EventArgs e)
        {
            //WarehouseDocumentPosition warehouseDocumentPosition;


            Page.Validate("AddPositionGroupMaterialChoice");

            if (IsValid)
            {
                GridItem item = MaterialGrid.SelectedItems[0];


                int newProcessHeaderId = 0; // int.Parse(item.Cells[INDEX_PROCESS_HEADER_ID_IN_GRID].Text);
                int newProcessIDH;
                int newSupplyOrderId = 0; // int.Parse(item.Cells[INDEX_PROCESS_SUPPLYORDERID_IN_GRID].Text);
                int newSupplyOrderPositionId = int.Parse(item.Cells[INDEX_PROCESS_SUPPLYORDERPOSID_IN_GRID].Text);
                decimal newValuePerUnit = decimal.Parse(item.Cells[INDEX_UNIT_PRICE_IN_GRID].Text);
                string newOrderNumer;
                int newContractingPartySimple = int.Parse(item.Cells[INDEX_CONTRACTING_PARTY_ID_IN_GRID].Text);       //sprawdzić czy się nie zmieniło,, jeśli stare różne od nowego to error - sprawdzic czy nie puste
                string newOrderNumberPZ = item.Cells[INDEX_ORDERNUMBERPZ_IN_GRID].Text; //sprawdzić czy nie puste
                bool newForDepartment = bool.Parse(item.Cells[INDEX_FOR_DEPARTMENT_IN_GRID].Text); // int.Parse(item.Cells[INDEX_FOR_DEPARTMENT_IN_GRID].Text) == 0 ? false : true;

                int newIdSim = int.Parse(item.Cells[INDEX_IDSIM_IN_GRID].Text);

                int sourceType = 0; // int.Parse(item.Cells[INDEX_FOR_SOURCETYPE_IN_GRID].Text);

                double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));
                int currentComapnyId = companyId;

                double availableQuantity = double.Parse(item.Cells[INDEX_AVAILABLE_QUANTITY_IN_GRID].Text);


                // Package stuff


                int? packagesNumber = null;
                int? packageQuantity = null;
                int? packageTypeNameId = null;

                if (!string.IsNullOrEmpty(RadComboBox_PackageTypeName.SelectedValue))
                {
                    packageTypeNameId = int.Parse(RadComboBox_PackageTypeName.SelectedValue);

                    if (packageTypeNameId > 0)
                    {
                        packagesNumber = int.Parse(TextBox_PackagesNumber.Text);
                        packageQuantity = int.Parse(TextBox_PackageQuantity.Text); //.Replace(".", ","));
                    }
                    else
                    {
                        packageTypeNameId = null;
                    }

                }


                AddPosition(newProcessHeaderId, newSupplyOrderId, newSupplyOrderPositionId, newValuePerUnit, newContractingPartySimple, newOrderNumberPZ, newForDepartment, newIdSim, quantity, currentComapnyId, availableQuantity, sourceType, packageTypeNameId, packagesNumber, packageQuantity);

            }

            //this.QuantityCustomValidator.Enabled = true;
        }

        private void AddPosition(
            int newProcessHeaderId,
            int newSupploOrderId,
            int newSupploOrderPositionId,
            decimal newValuePerUnit,
            int newContractingPartySimple,
            string newOrderNumberPZ,
            bool newForDepartment,
            int newIdSim,
            double quantity,
            int currentCompanyId,
            double availableQuantity,
            int sourceType,

            int? packageTypeNameId = null,
            int? packagesNumber = null,
            int? packageQuantity = null
            )
        {
            WarehouseDocumentPosition warehouseDocumentPosition;
            Zone zone;
            Order order = null;

            WarehouseDocumentPosition warehouseDocumentPositionAdditional = null;

            int newProcessIDH = 0;
            //int newProcessIDDPL;
            string newOrderNumer;

            double mainQuantity = quantity > availableQuantity ? availableQuantity : quantity;
            double additionalQuantity = quantity - mainQuantity;




            List<WarehouseDocumentPositionPackage> Packages = null;
            using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                zone = warehouseDs.GetShelfByNameDataOnly(cmbShelf.Text);

                // Packages
                if (packageTypeNameId.HasValue && packageQuantity.HasValue && packageQuantity > 0 && packagesNumber.HasValue && packagesNumber > 0)
                {
                    Packages = new List<WarehouseDocumentPositionPackage>();

                    WarehouseDocumentPositionPackage package = new WarehouseDocumentPositionPackage();

                    PackageType packageType = (from p in warehouseDs.GetAllPackageTypes()
                                               where
                                               p.PackageTypeName.Id == packageTypeNameId.Value
                                               &&
                                               p.Quantity == packageQuantity
                                               select p).FirstOrDefault();
                    if (packageType == null)
                    {
                        PackageTypeName packageTypeName = warehouseDs.GetPackageTypeNameById(packageTypeNameId.Value);
                        packageType = warehouseDs.CreateNewPackageType(packageTypeName.Id, packageQuantity.Value);



                    }

                    package.NumberOfPackages = packagesNumber.Value;
                    warehouseDs.DetachEntity(packageType);
                    package.PackageType = packageType;
                    package.Zone = zone;
                    Packages.Add(package);
                }



                WaitingForSupplyNew waiting = warehouseDs.FindRecordInWaitingForSupplyNew(newProcessHeaderId, newSupploOrderId, newSupploOrderPositionId, newContractingPartySimple, newIdSim, currentCompanyId, newOrderNumberPZ, newForDepartment); //, quantity);

                newProcessIDH = 0; // waiting.IDh != null ? (int)waiting.IDh : 0;
                //newProcessIDDPL = waiting.IDdpl;
                newOrderNumer = waiting.OrderNumber;

                int userId = (int)Session[SessionData.SessionKey_UserId];
                Material material = warehouseDs.GetOrAddNewNewMaterialDataOnly(newIdSim, currentCompanyId, warehouseDs.GetPersonForSystemUser(userId).Id);

                contractingParty = newContractingPartySimple;
                orderNumberPZ = newOrderNumberPZ;



                warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(material.Id, newValuePerUnit,
                                                                                                mainQuantity);

                //if (!newForDepartment)
                {
                    //Tuple<Order,OrderDelivery> orderWithOrderDelivery
                    Tuple<Order, OrderDeliveryNew> orderWithOrderDelivery = warehouseDs.CreateOrGetOrderAndDeliveryNew(newSupploOrderId, newSupploOrderPositionId, newOrderNumer, orderNumberPZ, newForDepartment);

                    if (!newForDepartment)
                        warehouseDocumentPosition.Order = orderWithOrderDelivery.Item1;
                    warehouseDocumentPosition.OrderDeliveryNew = orderWithOrderDelivery.Item2;

                    warehouseDocumentPosition.SupplyOrderPositionId = newSupploOrderPositionId;

                    order = warehouseDocumentPosition.Order;
                }

                //double additionalQuantity = quantity - availableQuantity;
                if (additionalQuantity > 0 && warehouseDocumentPosition.Order != null)
                {
                    warehouseDocumentPositionAdditional = warehouseDs.CreateWarehouseDocumentPositionDataOnly(material.Id, newValuePerUnit,
                                                                                                additionalQuantity);
                }
            }

            // ADDING INFO FOR MAIN QUANTITY:

            PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
            positionWithQuantityAndShelfses.FirstOrDefault(
                                        pq =>
                                        (
                                                pq.position.OrderDeliveryNew.SupplyOrderPositionId == warehouseDocumentPosition.OrderDeliveryNew.SupplyOrderPositionId
                                        )
                                            //&& pq.position.Material.Id == warehouseDocumentPosition.Material.Id 
                                            //&& pq.position.UnitPrice == warehouseDocumentPosition.UnitPrice
                                        && ((pq.order == null && order == null) || (pq.order != null && order != null && pq.order.Id == order.Id))
                                        && pq.IsReal
                                        );

            AdditionalPositionInfo additionalPositionInfo = new AdditionalPositionInfo();
            additionalPositionInfo.TotalOrderQuantity = availableQuantity;
            additionalPositionInfo.SupplySourceType = sourceType;

            if (positionsWithShelfsWhichExists != null)
            {
                WarehousePositionZone warehousePositionZone = positionsWithShelfsWhichExists.WarehousePositionZones.FirstOrDefault(p => p.Zone.Id == zone.Id);

                if (warehousePositionZone == null)
                {
                    positionsWithShelfsWhichExists.WarehousePositionZones.Add(new WarehousePositionZone()
                    {
                        WarehouseDocumentPosition = positionsWithShelfsWhichExists.position,
                        Quantity = mainQuantity,
                        Zone = zone
                    });
                }
                else
                {
                    warehousePositionZone.Quantity += warehouseDocumentPosition.Quantity;
                }
                positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;

                if (positionsWithShelfsWhichExists.Packages != null)
                {
                    if (Packages != null)
                    {
                        foreach (var package in Packages)
                        {
                            var existingPackage = (from p in positionsWithShelfsWhichExists.Packages
                                                   where p.PackageType.Id == package.PackageType.Id
                                                   select p).FirstOrDefault();

                            if (existingPackage != null)
                            {
                                existingPackage.NumberOfPackages += package.NumberOfPackages;

                                using (WarehouseDS warehouseDs = new WarehouseDS())
                                {
                                    //warehouseDs.DeleteObject(package);
                                    warehouseDs.DetachEntity(package);
                                }
                            }
                            else
                            {
                                positionsWithShelfsWhichExists.Packages.Add(package);
                            }
                        }
                    }
                }
                else
                {
                    positionsWithShelfsWhichExists.Packages = Packages;
                }
            }
            else
            {
                List<WarehousePositionZone> warehousePositionZone = new List<WarehousePositionZone>();

                warehousePositionZone.Add(new WarehousePositionZone()
                {
                    WarehouseDocumentPosition = warehouseDocumentPosition,
                    Quantity = mainQuantity,
                    Zone = zone
                });

                positionWithQuantityAndShelfses.Add(new PositionWithQuantityAndShelfs()
                {
                    position = warehouseDocumentPosition,
                    WarehousePositionZones = warehousePositionZone,
                    order = order,
                    additionalPositionInfo = additionalPositionInfo,
                    IsReal = true,
                    Packages = Packages
                    //PackageTypeId =
                    //    packageTypeId.HasValue
                    //        ? packageTypeId.Value
                    //        : (int?)null,
                    //PackagesNumber =
                    //    packagesNumber.HasValue
                    //        ? packagesNumber.Value
                    //        : (int?)null,
                    //NoPackedQuantity =
                    //    noPackedQuantity.HasValue
                    //        ? noPackedQuantity.Value
                    //        : (double?)null
                });
            }

            // ADDING INFO FOR ADDITIONAL QUANTITY:
            if (warehouseDocumentPositionAdditional != null)
            {

                PositionWithQuantityAndShelfs positionsWithShelfsWhichExistsAdditional =
                    positionWithQuantityAndShelfses.FirstOrDefault(pq =>
                                                                   pq.position.Material.Id == warehouseDocumentPositionAdditional.Material.Id &&
                                                                   pq.position.UnitPrice == warehouseDocumentPositionAdditional.UnitPrice &&
                                                                   pq.order == null &&
                                                                   !pq.IsReal);

                if (positionsWithShelfsWhichExistsAdditional != null)
                {
                    WarehousePositionZone warehousePositionZone = positionsWithShelfsWhichExistsAdditional.WarehousePositionZones.FirstOrDefault(p => p.Zone.Id == zone.Id);

                    if (warehousePositionZone == null)
                    {
                        positionsWithShelfsWhichExistsAdditional.WarehousePositionZones.Add(new WarehousePositionZone()
                        {
                            WarehouseDocumentPosition = positionsWithShelfsWhichExistsAdditional.position,
                            Quantity = additionalQuantity,
                            Zone = zone
                        });
                    }
                    else
                    {
                        warehousePositionZone.Quantity += warehouseDocumentPositionAdditional.Quantity;
                    }
                    positionsWithShelfsWhichExistsAdditional.position.Quantity += warehouseDocumentPositionAdditional.Quantity;
                }
                else
                {
                    List<WarehousePositionZone> warehousePositionZone = new List<WarehousePositionZone>();

                    warehousePositionZone.Add(new WarehousePositionZone()
                    {
                        WarehouseDocumentPosition = warehouseDocumentPositionAdditional,
                        Quantity = additionalQuantity,
                        Zone = zone
                    });

                    positionWithQuantityAndShelfses.Add(new PositionWithQuantityAndShelfs()
                    {
                        position = warehouseDocumentPositionAdditional,
                        WarehousePositionZones = warehousePositionZone,
                        order = null,
                        additionalPositionInfo = null,
                        IsReal = false

                        // Consider packages
                    });
                }
            }

            Session["ReceiveMaterialNew_ContractinPartyId"] = contractingParty;
            Session["ReceiveMaterialNew_OrderNumberPZ"] = orderNumberPZ;
            Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"] = positionWithQuantityAndShelfses;

            QuantityTextBox.Text = "";
            cmbShelf.Text = "";
            MaterialGrid.SelectedIndexes.Clear();
            MaterialGrid.Rebind();
            MaterialsToReceiveRadGrid.Rebind();

            this.btnSaveDocument.Enabled = true;

            this.QuantityCustomValidator.Enabled = true;


            //this.RadComboBox_PackageTypeName; //.ClearSelection();



            this.TextBox_PackageQuantity.Text = "0";
            this.TextBox_PackagesNumber.Text = "0";

            RadComboBox_PackageTypeName.Text = "";
        }


        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            int userId;

            if (IsValid)
            {
                if (positionWithQuantityAndShelfses.Count > 0)
                {
                    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
                    {
                        WarehouseDocument warehouseDocument = new WarehouseDocument();

                        using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            try
                            {
                                int currentWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                                if (contractingParty == null) throw new ApplicationException("Internal error with contracting party (is null)");
                                if (orderNumberPZ == null || orderNumberPZ == "")
                                    throw new ApplicationException("Internal error with order number PZ (is null or empty)");

                                int contractingPartyInSIMPLE = (int)contractingParty;

                                warehouseDocument = warehouseDs.ExternalReceiveNew(currentWarehoseId, warehouseDs.GetPersonForSystemUser(userId).Id, positionWithQuantityAndShelfses,
                                    contractingPartyInSIMPLE, inputDate.SelectedDate ?? DateTime.Now.Date, CheckBox_ExportToSIMPLE.Checked);

                                if (warehouseDocument == null) throw new ApplicationException();

                                //// TODO: Debug it
                                //foreach (WarehouseDocumentPosition position in warehouseDocument.WarehouseDocumentPosition)
                                //{
                                //    warehouseDs.UpdateSupplyTables(position, contractingPartyInSIMPLE, companyId);

                                //}

                                string externalInvoiceNumber = ExternalInvoiceNumber.Text.Replace(" ", "");

                                if (!String.IsNullOrEmpty(externalInvoiceNumber))
                                {
                                    warehouseDs.UpdateInvoiceNumber(warehouseDocument, ExternalInvoiceNumber.Text);
                                }

																// commented out because LoanTake functionality in K3 should not be used.
																//// Try to return loans:
																//int warehouseId = warehouseDs.GetWarehouseDocumentById(warehouseDocument.Id).Warehouse1.Id;
																//warehouseDs.TryToReturnLoans(warehouseId, warehouseDs.GetPersonForSystemUser(userId).Id);
                            }
                            catch (Exception ex)
                            {
                                WebTools.AlertMsgAdd(ex, userId);
                            }



                        }

                        positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();
                        Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"] = positionWithQuantityAndShelfses;
                        Session["ReceiveMaterialNew_ContractinPartyId"] = null;
                        Session["ReceiveMaterialNew_OrderNumberPZ"] = null;

                        DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument, WarehouseDocumentTypeEnum.Pz, warehouseDocument.Id);

                        cmbShelf.Text = "";
                        QuantityTextBox.Text = "";

                        MaterialGrid.SelectedIndexes.Clear();
                        MaterialGrid.Rebind();

                        cmbShelf.Enabled = false;
                        QuantityTextBox.Enabled = false;
                        ExternalInvoiceNumber.Enabled = false;
                        btnPrintDocument.Enabled = true;
                        btnSaveDocument.Enabled = false;
                        btnAddPosition.Enabled = false;

                        btnNewDocument.Enabled = true;

                        //////this.RadComboBox_ContractingPartySimple.Enabled = false;
                        //////this.RadComboBox_MaterialsSIMPLE.Enabled = false;
                        //////this.RadTextBox_SuppliedQuantity.Enabled = false;
                        //////this.RadTextBox_UnitCost.Enabled = false;
                        //////this.RadDatePicker_SupplyDate.Enabled = false;
                        //////this.Button_AddSupply.Enabled = false;
                        //////this.CheckBox_GetIncome.Enabled = false;

                        this.RadComboBox_PackageTypeName.Enabled = false;
                        this.TextBox_PackageQuantity.Enabled = false;
                        this.TextBox_PackagesNumber.Enabled = false;

                        RadComboBox_PackageTypeName.Text = "";

                    }
                }
            }
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = true;
            btnAddPosition.Enabled = true;
            ExternalInvoiceNumber.Enabled = true;
            QuantityTextBox.Enabled = true;
            cmbShelf.Enabled = true;

            //////this.RadComboBox_ContractingPartySimple.Enabled = true;
            //////this.RadComboBox_MaterialsSIMPLE.Enabled = true;
            //////this.RadTextBox_SuppliedQuantity.Enabled = true;
            //////this.RadTextBox_UnitCost.Enabled = true;
            //////this.RadDatePicker_SupplyDate.Enabled = true;
            //////this.Button_AddSupply.Enabled = true;
            //////this.CheckBox_GetIncome.Enabled = true;

            this.QuantityCustomValidator.Enabled = true;
            this.Panel_OverQuantityConfirmation.Visible = false;

            positionWithQuantityAndShelfses = new List<PositionWithQuantityAndShelfs>();
            Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"] = positionWithQuantityAndShelfses;
            Session["ReceiveMaterialNew_ContractinPartyId"] = null;
            Session["ReceiveMaterialNew_OrderNumberPZ"] = null;

            ExternalInvoiceNumber.Text = "";
            QuantityTextBox.Text = "";
            cmbShelf.Text = "";
            MaterialGrid.SelectedIndexes.Clear();
            MaterialGrid.Rebind();
            MaterialsToReceiveRadGrid.Rebind();

            //this.RadComboBox_ContractingPartySimple.ClearSelection();
            //this.RadComboBox_MaterialsSIMPLE.ClearSelection();
            //////this.RadDatePicker_SupplyDate.SelectedDate = DateTime.Now.Date;
            //////this.RadTextBox_SuppliedQuantity.Text = "";
            //////this.RadTextBox_UnitCost.Text = "";
            //////this.CheckBox_GetIncome.Checked = false;

            this.RadComboBox_PackageTypeName.Enabled = true;
            this.TextBox_PackageQuantity.Enabled = true;
            this.TextBox_PackagesNumber.Enabled = true;
            this.TextBox_PackagesNumber.Text = "0";
            //RadComboBox_PackageTypeName.ClearSelection();

            RadComboBox_PackageTypeName.Text = "";


        }

        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            QuantityRTB.Text = QuantityRTB.Text.Replace(".", ",");

            MaterialGrid.Rebind();
        }

        protected void ClearStartDateFilterClick(object sender, EventArgs e)
        {
            RadDatePicker1.SelectedDate = null;
        }
        protected void ClearEndDateFilterClick(object sender, EventArgs e)
        {
            RadDatePicker2.SelectedDate = null;
        }

        protected void ShelfRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
        }

        protected void MaterialsToReceiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (positionWithQuantityAndShelfses != null)
            {
                int i = 1;

                List<PositionWithShelfAndQuantity> positionWithShelfAndQuantities = new List<PositionWithShelfAndQuantity>();

                foreach (var externalIterator in positionWithQuantityAndShelfses)
                {
                    if (externalIterator.WarehousePositionZones.Count() > 0)
                    {
                        double usedQuantity = 0;
                        foreach (var internalIterator in externalIterator.WarehousePositionZones)
                        {
                            positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                            {
                                position = externalIterator.position,
                                shelf = internalIterator.Zone,
                                quantity = internalIterator.Quantity
                                //, order = internalIterator.WarehouseDocumentPosition.Order
                            });
                            usedQuantity += internalIterator.Quantity;
                        }
                        if (usedQuantity < externalIterator.position.Quantity)
                        {
                            positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                            {
                                position = externalIterator.position,
                                shelf = null,
                                quantity = externalIterator.position.Quantity - usedQuantity
                                //,order = externalIterator.order
                            });
                        }
                    }
                    else
                    {
                        positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                        {
                            position = externalIterator.position,
                            shelf = null,
                            quantity = externalIterator.position.Quantity
                            //,order = externalIterator.order
                        });
                    }
                }

                var source2 = from p in positionWithShelfAndQuantities
                              select
                                  new
                                  {
                                      RowNumber = i++,
                                      MaterialName = p.position.Material.Name,
                                      Quantity = p.quantity,
                                      UnitName = p.position.Material.Unit.Name,
                                      ShelfName = p.shelf != null ? p.shelf.Name : null,
                                      OrderNumber = p.position.Order != null ? p.position.Order.OrderNumber : ""
                                  };

                MaterialsToReceiveRadGrid.DataSource = source2;
            }
        }

        protected void MaterialGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                string materialToSearch = MaterialToSearchRTB.Text;
                string ContractingPartyNameToSearch = ContractingPartyName.Text;

                DateTime? startDate = null;
                DateTime? endDate = null;
                DateTime? RadDatePicker1_SelectedDate = RadDatePicker1.SelectedDate;
                DateTime? RadDatePicker2_SelectedDate = RadDatePicker2.SelectedDate;
                if (RadDatePicker1_SelectedDate != null)
                {
                    startDate = (DateTime)RadDatePicker1.SelectedDate;
                }
                if (RadDatePicker2_SelectedDate != null)
                {
                    endDate = (DateTime)RadDatePicker2.SelectedDate;
                }


                string quantityToSearch = QuantityRTB.Text.Replace(".", ",");
                string orderNumberToSearch = OrderNumberRTB.Text;
                string orderNumberPZToSearch = OrderNumberPZ_RTB.Text;


                var materialsCandidatesToReceiveMaterialNewized = warehouseDs.GetWaitingForSupplyNew(materialToSearch, ContractingPartyNameToSearch, startDate,
                                                                                               endDate.Value, companyId, quantityToSearch, orderNumberToSearch, orderNumberPZToSearch, Convert.ToInt32(QuantityTolerance.SelectedValue));

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                var materialsCandidatesToReceiveMaterializedOrdered = materialsCandidatesToReceiveMaterialNewized.OrderBy(p => p.TimeOrderExpected); ;

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                List<WaitingForSupplyNew> materialsCandidatesToPresent = new List<WaitingForSupplyNew>();

                foreach (var supply in materialsCandidatesToReceiveMaterializedOrdered)
                {
                    WaitingForSupplyNew waitingForSupplyForPresent = new WaitingForSupplyNew();

                    double usedQuantity = 0;

                    //                    WarehouseDocumentPosition[] positionsAlreadyReceived = warehouseDs.GetPositionsAlreadyReceived();

                    var positionWQAS = positionWithQuantityAndShelfses.FirstOrDefault(
                        p =>
                            p.IsReal
                            &&

                        p.position.Material.IdSIMPLE == supply.MaterialIdSIMPLE

                        &&


                        p.SupplyOrderPositionId == supply.SupplyOrderPositionId

                        //&&

                        ////&&

                        ////p.position.OrderDelivery.ProcessIDDPL == supply.IDdpl

                        ////&&

                        ////p.position.OrderDelivery.OrderNumberFromPZ.Trim() == supply.NazwSK.Trim()

                        ////&&

                        //(
                        //    (p.position.OrderDelivery != null && !p.position.OrderDelivery.ForDepartment && !supply.DepartmentId.HasValue)
                        //    ||
                        //    ((p.position.OrderDelivery == null || p.position.OrderDelivery.ForDepartment) && supply.DepartmentId.HasValue))
                        //
                        );

                    if (positionWQAS != null) usedQuantity = positionWQAS.position.Quantity;

                    waitingForSupplyForPresent.SupplyOrderPositionId = supply.SupplyOrderPositionId;
                    waitingForSupplyForPresent.UnitPrice = supply.UnitPrice;
                    waitingForSupplyForPresent.ContractingPartyId = supply.ContractingPartyId; //supply.kontrachent_id;
                    waitingForSupplyForPresent.TimeOrderExpected = supply.TimeOrderExpected;
                    waitingForSupplyForPresent.MaterialIdSIMPLE = supply.MaterialIdSIMPLE;
                    waitingForSupplyForPresent.ContractingPartyName = supply.ContractingPartyName;
                    waitingForSupplyForPresent.SupplyOrderNumber = supply.SupplyOrderNumber;
                    waitingForSupplyForPresent.MaterialName = supply.MaterialName;
                    waitingForSupplyForPresent.QuantityToReceive = supply.QuantityToReceive - (double)usedQuantity;
                    waitingForSupplyForPresent.UnitName = supply.UnitName;
                    waitingForSupplyForPresent.OrderNumber = supply.OrderNumber;
                    waitingForSupplyForPresent.DepartmentId = supply.DepartmentId;
                    //waitingForSupplyForPresent.SourceType = supply.SourceType;
                    //waitingForSupplyForPresent.ContractingPartyName = supply.

                    materialsCandidatesToPresent.Add(waitingForSupplyForPresent);
                }

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                int i = 1;

                var materials = from p in materialsCandidatesToPresent
                                select
                                    new
                                    {
                                        RowNumber = i++,
                                        SupplyOrderPositionId = p.SupplyOrderPositionId,
                                        UnitPrice = p.UnitPrice,
                                        ContractingPartyId = p.ContractingPartyId,
                                        SupplyDate = p.TimeOrderExpected,
                                        IdSim = p.MaterialIdSIMPLE,
                                        ContractingParty = p.ContractingPartyName,
                                        OrderNumberPZ = p.SupplyOrderNumber,
                                        MaterialName = p.MaterialName,
                                        Quantity = Convert.ToDouble(p.QuantityToReceive),
                                        UnitName = p.UnitName,
                                        OrderNumber = p.OrderNumber,
                                        ForDepartment = p.DepartmentId.HasValue
                                    };
                MaterialGrid.DataSource = materials;

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());
            }
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = MaterialGrid.SelectedItems.Count == 1 ? true : false; //co jesli juz jest invalid?
        }

        protected void Shelf_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Zone zone = null;

            using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                zone = warehouseDs.GetShelfByNameDataOnly(cmbShelf.Text);
            }

            if (zone == null) args.IsValid = false;
            else args.IsValid = true;
        }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = decimal.Parse(QuantityTextBox.Text.Replace(".", ",")) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void ContractingParty_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (contractingParty != null && MaterialGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialGrid.SelectedItems[0];
                var cell = item.Cells[INDEX_CONTRACTING_PARTY_ID_IN_GRID];
                int newContractingPartyId = int.Parse(cell.Text);

                if (contractingParty != newContractingPartyId)
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        protected void OrderNumberPZ_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //if (positionWithQuantityAndShelfses.Count() != 0 && MaterialGrid.SelectedItems.Count == 1)
            //{
            //    GridItem item = MaterialGrid.SelectedItems[0];
            //    var cell = item.Cells[INDEX_ORDERNUMBERPZ_IN_GRID];
            //    string newOrderNumberPZ = cell.Text;

            //    foreach (var s in positionWithQuantityAndShelfses)
            //    {
            //        if (s.position.Order.OrderNumberPZ != newOrderNumberPZ)
            //        {
            //            args.IsValid = false;
            //            return;
            //        }
            //    }
            //}

            args.IsValid = true;
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            QuantityTextBox.Text = QuantityTextBox.Text.Replace(".", ",");

            if (MaterialGrid.SelectedItems.Count == 1) // && this.QuantityTextBox.Text.Length != 0 && WarehouseDS.IsValidQuantity(QuantityTextBox.Text))
            {
                //GridItem item = MaterialGrid.SelectedItems[0];

                //try
                //{
                //    Quantity.Text = Quantity.Text.Replace(".", ",");
                //    double quantity = double.Parse(Quantity.Text);

                //    double availableQuantity = double.Parse(item.Cells[INDEX_AVAILABLE_QUANTITY_IN_GRID].Text);

                //    args.IsValid = quantity <= availableQuantity;

                //    //if (quantity > availableQuantity)
                //    //{
                //    //    if (availableQuantity > 0)
                //    //    {
                //    //        this.Panel_OverQuantityConfirmation.Visible = true;
                //    //    }
                //    //    args.IsValid = false;
                //    //    return;
                //    //}
                //}
                //catch (Exception)
                //{
                //    args.IsValid = false;
                //    return;
                //}
                args.IsValid = true;

            }
            else
            {
                args.IsValid = false;
            }


        }

        protected void CustomValidator_MaterialSimple_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //////if (!string.IsNullOrEmpty(RadComboBox_MaterialsSIMPLE.SelectedValue) && int.Parse(RadComboBox_MaterialsSIMPLE.SelectedValue) > 0)
            //////{
            //////    args.IsValid = true;
            //////}
            //////else
            //////{
            //////    args.IsValid = false;
            //////}

            args.IsValid = false;

        }

        //CustomValidator_ContractingPartySimple_ServerValidate
        protected void CustomValidator_ContractingPartySimple_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //////if (!string.IsNullOrEmpty(RadComboBox_ContractingPartySimple.SelectedValue) && int.Parse(RadComboBox_ContractingPartySimple.SelectedValue) > 0)
            //////{
            //////    args.IsValid = true;
            //////}
            //////else
            //////{
            //////    args.IsValid = false;
            //////}
            args.IsValid = false;
        }

        //protected void CustomValidator_Unit_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (!string.IsNullOrEmpty(RadComboBox_Unit.SelectedValue) && int.Parse(RadComboBox_Unit.SelectedValue) > 0)
        //    {
        //        args.IsValid = true;
        //    }
        //    else
        //    {
        //        args.IsValid = false;
        //    }


        //}

        protected void CustomValidator_SupplyDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //////if (RadDatePicker_SupplyDate.SelectedDate.HasValue && RadDatePicker_SupplyDate.SelectedDate > DateTime.Parse("1900-01-01"))
            //////{
            //////    args.IsValid = true;
            //////}
            //////else
            //////{
            //////    args.IsValid = false;
            //////}

            args.IsValid = false;
        }



        //CustomValidator_QuantityFormat_ServerValidate
        protected void CustomValidator_QuantityFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //////args.IsValid = true;
            //////try
            //////{
            //////    decimal.Parse(this.RadTextBox_SuppliedQuantity.Text);
            //////}
            //////catch (Exception)
            //////{
            //////    args.IsValid = false;
            //////}
            args.IsValid = false;
        }

        //CustomValidator_UnitCostFormat_ServerValidate
        protected void CustomValidator_UnitCostFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //////args.IsValid = true;
            //////try
            //////{
            //////    decimal.Parse(this.RadTextBox_UnitCost.Text);
            //////}
            //////catch (Exception)
            //////{
            //////    args.IsValid = false;
            //////}

            args.IsValid = false;
        }

        //Packages_ServerValidate
        protected void CustomValidator_Packages_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            try
            {
                int packageTypeId = int.Parse(RadComboBox_PackageTypeName.SelectedValue);
                if (packageTypeId > 1)
                {
                    int packageQuantity = int.Parse(this.TextBox_PackageQuantity.Text);
                    int packagesNumber = int.Parse(this.TextBox_PackagesNumber.Text);

                    //decimal quantityNoPacked = decimal.Parse(this.TextBox_NoPackageQuantity.Text);

                    double totalQuantity = int.Parse(QuantityTextBox.Text);

                    using (WarehouseDS warehouseDs = new WarehouseDS())
                    {
                        args.IsValid = totalQuantity == packagesNumber * packageQuantity;

                    }
                }
                else
                {
                    args.IsValid = true;
                }
            }
            catch (Exception)
            {
                args.IsValid = string.IsNullOrEmpty(RadComboBox_PackageTypeName.SelectedValue);
            }

            //if (RadComboBox_PackageTypeName.SelectedIndex < 1)
            //    args.IsValid = false;
        }


        //protected void NewPackageType_CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    try
        //    {
        //        int quantity = int.Parse(TextBox_PackageQuantity.Text);
        //        int unitId = int.Parse(RadComboBox_Units.SelectedValue);
        //        //RadComboBox_Units
        //        if (unitId > 0)
        //        {
        //            args.IsValid = true;
        //        }
        //        else
        //        {
        //            args.IsValid = false;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        args.IsValid = false;
        //    }
        //    //args.IsValid = true;
        //}

        //protected void NewPackageType_CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    try
        //    {
        //        int quantity = int.Parse(TextBox_PackageQuantity.Text);
        //        int unitId = int.Parse(RadComboBox_Units.SelectedValue);
        //        string packageTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(RadComboBox_PackageTypeNameName.Text.Trim().ToLower());

        //        if (!string.IsNullOrEmpty(packageTypeName))
        //        {
        //            using (WarehouseDS warehouseDs = new WarehouseDS())
        //            {
        //                string unitShortName = warehouseDs.GetUnitById(unitId).ShortName;
        //                string packageFullName = packageTypeName + " " + quantity + "" + unitShortName;
        //                args.IsValid = !warehouseDs.CheckIfPackageTypeExists(unitId, quantity, packageFullName);
        //            }
        //        }
        //        else
        //        {
        //            args.IsValid = false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        args.IsValid = false;
        //    }

        //    //args.IsValid = true;
        //}

        //protected void AddPackageTypeClick(object sender, EventArgs e)
        //{
        //    int userId;
        //    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
        //    {
        //        if (IsValid)
        //        {
        //            try
        //            {
        //                int quantity = int.Parse(TextBox_PackageQuantity.Text);
        //                int unitId = int.Parse(RadComboBox_Units.SelectedValue);
        //                string packageTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(RadComboBox_PackageTypeNameName.Text.Trim().ToLower());

        //                using(WarehouseDS warehouseDs = new WarehouseDS())
        //                {

        //                    string unitShortName = warehouseDs.GetUnitById(unitId).ShortName;
        //                    string packageFullName = packageTypeName + " " + quantity + "" + unitShortName;


        //                    warehouseDs.CreateNewPackageType(unitId, quantity, packageFullName);

        //                    RadComboBox_PackageTypeName.DataBind();

        //                    TextBox_PackageQuantity.Text = "";
        //                    //TextBox_PackageTypeName.Text = "";
        //                    //RadComboBox_PackageTypeNameName.DataBind();

        //                    AddPackageTypeOKLabel.Visible = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                WebTools.AlertMsgAdd(ex, userId);
        //            }
        //        }
        //    }
        //}

        protected void MaterialsToReceiveRadGrid_DeleteRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                positionWithQuantityAndShelfses.RemoveAt(e.Item.ItemIndex);
                Session["ReceiveMaterialNew_positionWithQuantityAndShelfses"] = positionWithQuantityAndShelfses;

                if (MaterialsToReceiveRadGrid.Items.Count <= 1)
                {
                    Session["ReceiveMaterialNew_ContractinPartyId"] = null;
                    Session["ReceiveMaterialNew_OrderNumberPZ"] = null;

                    contractingParty = (int?)Session["ReceiveMaterialNew_ContractinPartyId"];
                    orderNumberPZ = (string)Session["ReceiveMaterialNew_OrderNumberPZ"];
                }

                MaterialsToReceiveRadGrid.Rebind();
                MaterialGrid.Rebind();
            }
            else
            {
                throw new ApplicationException("Unknown grid command");
            }
        }

        protected void Button_OQC_Yes_Click(object sender, EventArgs e)
        {
            this.QuantityCustomValidator.Enabled = false;
            this.Panel_OverQuantityConfirmation.Visible = false;
        }

        protected void Button_OQC_No_Click(object sender, EventArgs e)
        {
            this.QuantityCustomValidator.Enabled = true;
            this.Panel_OverQuantityConfirmation.Visible = false;
            QuantityTextBox.Text = "";
        }


        //////protected void AddSupplyClick(object sender, EventArgs e)
        //////{
        //////    double suppliedQuantity = double.Parse(this.RadTextBox_SuppliedQuantity.Text);
        //////    if (this.CheckBox_GetIncome.Checked)
        //////    {
        //////        if (QuantityTextBox.Text == "")
        //////        {
        //////            QuantityTextBox.Text = suppliedQuantity.ToString();
        //////        }
        //////        Page.Validate("AddPositionGroup");
        //////    }

        //////    if (IsValid) // this.RadComboBox_MaterialsSIMPLE.SelectedValue != null && this.RadDatePicker_SupplyDate.SelectedDate.HasValue)
        //////    {

        //////        using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
        //////        {
        //////            string key = "";

        //////            string today = DateTime.Now.ToString("yyyy-MM-dd");

        //////            key = "INTERNAL/" + today.Replace("-", "") + "/" + WebTools.GenerateRandomString(8);

        //////            int simpleId = int.Parse(this.RadComboBox_MaterialsSIMPLE.SelectedValue);

        //////            DateTime supplyDate = this.RadDatePicker_SupplyDate.SelectedDate.Value;
        //////            decimal valuePerUnit = decimal.Parse(this.RadTextBox_UnitCost.Text);
        //////            int contractingPartySimple = int.Parse(this.RadComboBox_ContractingPartySimple.SelectedValue);
        //////            string orderNumberPz = key;
        //////            bool forDepartment = false;
        //////            int processHeaderId = 0; // 666000;
        //////            int processIdpl = 0; // 666000;
        //////            int processIddpl = 0;
        //////            int flaga = 1;

        //////            string orderId = this.RadComboBox_OrderNumber.SelectedValue;



        //////            int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

        //////            warehouseDs.AddNewSupplyByHand(simpleId,
        //////                                           suppliedQuantity,
        //////                                           supplyDate,
        //////                                           currentComapnyId,
        //////                                           processHeaderId,
        //////                                           processIdpl,
        //////                                           processIddpl,
        //////                                           contractingPartySimple,
        //////                                           orderNumberPz,
        //////                                           forDepartment,
        //////                                           flaga,
        //////                                           valuePerUnit,
        //////                                           orderId
        //////                                           );



        //////            if (this.CheckBox_GetIncome.Checked)
        //////            {

        //////                double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));
        //////                AddPosition(processHeaderId, processIdpl, processIddpl, valuePerUnit, contractingPartySimple,
        //////                            orderNumberPz,
        //////                            forDepartment, simpleId, quantity, currentComapnyId, suppliedQuantity, 2);
        //////            }



        //////            this.MaterialGrid.Rebind();

        //////            //this.RadComboBox_ContractingPartySimple.ClearSelection();
        //////            //this.RadComboBox_MaterialsSIMPLE.ClearSelection();
        //////            this.RadDatePicker_SupplyDate.SelectedDate = DateTime.Now.Date;
        //////            this.RadTextBox_SuppliedQuantity.Text = "";
        //////            this.RadTextBox_UnitCost.Text = "";
        //////            this.CheckBox_GetIncome.Checked = false;
        //////        }
        //////    }
        //////}



        protected const string unreadPattern = @"\(\d+\)";

        protected void RadTreeView1_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            RadTreeNode clickedNode = e.Node;

            switch (e.MenuItem.Value)
            {
                case "Copy":
                    RadTreeNode clonedNode = clickedNode.Clone();
                    clonedNode.Text = string.Format("Copy of {0}", clickedNode.Text);
                    clickedNode.InsertAfter(clonedNode);
                    //set node's value so we can find it in startNodeInEditMode
                    clonedNode.Value = clonedNode.GetFullPath("/");
                    clonedNode.Selected = true;
                    startNodeInEditMode(clonedNode.Value);
                    break;
                case "NewFolder":
                    RadTreeNode newFolder = new RadTreeNode(string.Format("New Folder {0}", clickedNode.Nodes.Count + 1));
                    newFolder.Selected = true;
                    newFolder.ImageUrl = clickedNode.ImageUrl;
                    clickedNode.Nodes.Add(newFolder);
                    clickedNode.Expanded = true;
                    //update the number in the brackets
                    if (Regex.IsMatch(clickedNode.Text, unreadPattern))
                        clickedNode.Text = Regex.Replace(clickedNode.Text, unreadPattern, "(" + clickedNode.Nodes.Count.ToString() + ")");
                    else
                        clickedNode.Text += string.Format(" ({0})", clickedNode.Nodes.Count);
                    clickedNode.Font.Bold = true;
                    //set node's value so we can find it in startNodeInEditMode
                    newFolder.Value = newFolder.GetFullPath("/");
                    startNodeInEditMode(newFolder.Value);
                    break;
                case "EmptyFolder":
                    emptyFolder(clickedNode, true);
                    break;
                case "MarkAsRead":
                    emptyFolder(clickedNode, false);
                    break;
                case "Delete":
                    clickedNode.Remove();
                    break;
            }
        }

        private void startNodeInEditMode(string nodeValue)
        {
            ////find the node by its Value and edit it when page loads
            //string js = "Sys.Application.add_load(editNode); function editNode(){ ";
            //js += "var tree = $find(\"" + RadTreeView1.ClientID + "\");";
            //js += "var node = tree.findNodeByValue('" + nodeValue + "');";
            //js += "if (node) node.startEdit();";
            //js += "Sys.Application.remove_load(editNode);};";

            //RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "nodeEdit", js, true);
        }

        protected void RadTreeView1_NodeEdit(object sender, RadTreeNodeEditEventArgs e)
        {
            e.Node.Text = e.Text;
        }

        //this method is used by Mark All as Read and Empty this folder 
        protected void emptyFolder(RadTreeNode node, bool removeChildNodes)
        {
            node.Font.Bold = false;
            node.Text = Regex.Replace(node.Text, unreadPattern, "");

            if (removeChildNodes)
            {
                //Empty this folder is clicked
                for (int i = node.Nodes.Count - 1; i >= 0; i--)
                {
                    node.Nodes.RemoveAt(i);
                }
            }
            else
            {
                //Mark all as read is clicked
                foreach (RadTreeNode child in node.Nodes)
                {
                    emptyFolder(child, removeChildNodes);
                }
            }
        }
    }
}
