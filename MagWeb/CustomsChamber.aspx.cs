﻿namespace MagWeb
{
  using System;
  using System.Linq;
  using System.Web.UI.WebControls;
  using Mag.Domain;
  using MagWeb.Extensions;
  using Telerik.Web.UI;
  using Telerik.Web.UI.Calendar;

  public partial class CustomsChamber : FeaturedSystemWebUIPage
  {
    private const string CustomsChamberMaterialsGrid_WarehouseDocumentPositionId_UniqueName = "WarehouseDocumentPositionId"; //2;
    private const string CustomsChamberMaterialsGrid_OrderId_UniqueName = "OrderId"; //3;
    private const string CustomsChamberMaterialsGrid_QuantityKCNotAssigned_UniqueName = "QuantityKCNotAssigned"; //10;

    //private List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfses;

    private bool isExtendedMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {
      string option = Request["o"];
      if (string.IsNullOrEmpty(option))
      {
        Session["tab_Reports"] = 0;
        isExtendedMode = false;
      }
      else
      {
        Session["tab_Reports"] = 1;
        isExtendedMode = true;
      }

      if (!Page.IsPostBack)
      {

        if (isExtendedMode)
        {
          Label_Title.Text = "Lista nadwyżek";
        }
        else
        {
          Label_Title.Text = "Lista materiałów zablokowanych";
        }

        int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
        int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

        Panel_Orders.Visible = false;
        Panel_Dates.Visible = false;
        //TextBox_Quantity.Enabled = false;
        Button_Disposition.Enabled = false;

        using (var warehouseDs = new WarehouseDS())
        {
          RadComboBox_KCStates.Items.Clear();
          var types = warehouseDs.GetCustomChamberStateTypes();
          RadComboBox_KCStates.Items.Add(new RadComboBoxItem("", "-1"));
          foreach (var type in types)
          {
            if (type.Id < 4) // Id=4 is 'Assginment to Department' and is not fully supported yet
              RadComboBox_KCStates.Items.Add(new RadComboBoxItem(type.Name, type.Id.ToString()));
          }


          //var orders = warehouseDs.GetOrdersList("")




          //RadComboBox_KCStates.DataSource =
          //    from p in types
          //    select new RadComboBoxItemData
          //    {
          //        Text = p.Name,
          //        Value = p.Id.ToString()
          //    };
          //RadComboBox_KCStates.DataBind();


        }

        Panel_Disposition.Enabled =
            isExtendedMode && SessionData.ClientData.SystemUserSecurityPrivileges.Any(p => p.SecurityPrivilegeId == 41 && p.Value == currentWarehouseId.ToString());
      }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (Panel_Disposition.Enabled && CheckBox_GetOrderFromKCPos.Checked)
      {
        foreach (RadComboBoxItem item in RadComboBox_Orders.Items)
        {
          if (item.Selected)
          {
            item.Selected = false;
          }
        }
        RadComboBox_Orders.Text = "";
      }


      Label_Dispository.Visible = Panel_Disposition.Enabled;
      //CustomsChamberMaterialsGrid.Enabled = Panel_Disposition.Enabled;
      RadComboBox_Orders.Enabled = !CheckBox_GetOrderFromKCPos.Checked;
      //foreach(GridDataItem gridDataItem in CustomsChamberMaterialsGrid.Items)
      //{
      //    gridDataItem.Expanded = true;
      //}
    }

    protected void CustomsChamberMaterialsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      // int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

      using (var warehouseDs = new WarehouseDS())
      {
        var datasource = warehouseDs.GetCustomsChamberPositionsHeaders(isExtendedMode);

        CustomsChamberMaterialsGrid.DataSource = datasource.ToList();

      }
    }



    protected void RadComboBox_KCStates_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
      if (!isExtendedMode)
        return;

      int orderId = 0;
      if (CustomsChamberMaterialsGrid.SelectedItems.Count == 0)
      {
        WebTools.AlertMsgAdd("Nie wybrano żadnego elementu. Prosimy o zaznaczenie pozycji.");
        return;
      }
      var item = CustomsChamberMaterialsGrid.SelectedItems[0] as GridDataItem;
      orderId = item.GetCellValueAs<int>(CustomsChamberMaterialsGrid_OrderId_UniqueName);

      int valueInt = RadComboBox_KCStates.GetSelectedValueAs<int>();

      //bool isOrderSelectVisible = false;
      Panel_Dates.Visible = false;
      Panel_Orders.Visible = false;

      //string textBoxQuantityText = TextBox_Quantity.Text;

      //TextBox_Quantity.Enabled = false;
      Button_Disposition.Enabled = false;

      if (valueInt > 0)
      {
        if (valueInt == 1)
        {
          Panel_Dates.Visible = true;
        }
        if (valueInt == 3)
        {
          Panel_Orders.Visible = true;
        }
        //TextBox_Quantity.Enabled = true;
        Button_Disposition.Enabled = true;
      }

      CheckBox_GetOrderFromKCPos.Checked = orderId > 0;
      CheckBox_GetOrderFromKCPos.Enabled = CheckBox_GetOrderFromKCPos.Checked;

      //TextBox_Quantity.Text = textBoxQuantityText;
    }

    protected void CustomsChamberMaterialsGrid_ItemDataBound(object sender, GridItemEventArgs e)
    {
      //if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "Master")
      //{
      //    GridDataItem item = (GridDataItem)e.Item;
      //    CheckBox check = (CheckBox)item["SelectColumn"].Controls[0];
      //    check.Attributes.Add("onclick", "CheckChanged(this,'" + item.ItemIndex + "');");
      //}
      if (e.Item is GridDataItem)
      {
        GridDataItem item = (GridDataItem)e.Item;

        if (item.Cells.OfType<GridTableCell>().Any(c => c.Column.UniqueName == CustomsChamberMaterialsGrid_QuantityKCNotAssigned_UniqueName) && item.GetCellValueAs<decimal>(CustomsChamberMaterialsGrid_QuantityKCNotAssigned_UniqueName) == 0)
        {
          item.ForeColor = System.Drawing.Color.LightGray;
        }
      }
    }

    protected void CustomsChamberMaterialsGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
    {
      GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;
      if (parentItem.Edit)
      {
        return;
      }

      //key = "{WarehouseId:\"37\",MaterialId:\"11099\"}"

      var gridTableView = (GridTableView)e.DetailTableView;
      gridTableView.Enabled = isExtendedMode;

      string keyRendered = e.DetailTableView.ParentItem.KeyValues;

      keyRendered = keyRendered.Replace("{WarehouseDocumentPositionId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
      //keyRendered = keyRendered.Replace(",MaterialId:", ";");
      keyRendered = keyRendered.Replace("\"", "");
      keyRendered = keyRendered.Replace("}", "");

      //string[] ids = keyRendered.Split(';');
      //int warehouseId = int.Parse(ids[0]);
      //int materialId = int.Parse(ids[1]);

      int warehouseDocumentPositionId = int.Parse(keyRendered);

      //var items = 

      bool isEditEnabled = Panel_Disposition.Enabled;

      using (var warehouseDs = new WarehouseDS())
      {
        var ccPoss = warehouseDs.GetCustomsChamberPositions(warehouseDocumentPositionId);

        int i = 1;
        var details = from p in ccPoss
                      select new CustomsChamberViewDetails(p, i++, isEditEnabled);


        //TODO move this to select statement and btw. this is kinda strange
        foreach (var detail in details)
        {
          if (detail.KCSolvingDocumentTypeShortName == "UMZ")
          {
            detail.KCSolvingDocumentTypeShortName = "[stan mag]";
          }
        }


        CustomsChamberMaterialsGrid.DataSource = details;
        //{
        //    Lp = i++,
        //    KCQuantity = p.KCQuantity,
        //    KCStateTypeName = p.KCStateTypeName,
        //    KCSolvingDocumentTypeName = p.KCSolvingDocumentTypeName,
        //    TimeRequired = p.TimeRequired,
        //    KCSolvingDocumentDate = p.KCSolvingDocumentDate,
        //    KCSolvingDocumentPositionOrderNumber = p.KCSolvingDocumentPositionOrderNumber,
        //    WarehouseDocumentPositionKCStateId = p.WarehouseDocumentPositionKCStateId,
        //    IsDateEditEnabled = isEditEnabled && !p.IsFinished
        //};
      }

      //if (e.DetailTableView.DataMember == "StockDetails")
      //{
      //    DataSet ds = (DataSet)e.DetailTableView.DataSource;
      //    e.DetailTableView.DataSource = ds.Tables["StockDetails"].Select("CustomerID = '" + parentItem["CustomerID"].Text + "'");
      //}
    }

    protected void CustomsChamberMaterialsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
      var item = CustomsChamberMaterialsGrid.SelectedItems[0];
    }

    protected void QuantityDataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      try
      {
        args.IsValid = decimal.Parse(TextBox_Quantity1.Text.Replace(".", ",")) > 0 ? true : false;
      }
      catch (Exception)
      {
        args.IsValid = false;
      }
    }

    protected void CustomValidator_OrderNumber_ServerValidate(object source, ServerValidateEventArgs args)
    {
      if (Panel_Orders.Visible)
      {
        int orderId = 0;
        if (CheckBox_GetOrderFromKCPos.Checked)
        {
          var item = CustomsChamberMaterialsGrid.SelectedItems[0] as GridDataItem;
          orderId = item.GetCellValueAs<int>(CustomsChamberMaterialsGrid_OrderId_UniqueName);
        }
        else
        {
          int.TryParse(RadComboBox_Orders.SelectedValue, out orderId);
        }
        args.IsValid = orderId > 0;
      }
      else
      {
        args.IsValid = true;
      }
    }

    protected void Button_Disposition_Click(object sender, EventArgs e)
    {
        if (!isExtendedMode)
            return;

        if (CustomsChamberMaterialsGrid.SelectedItems.Count != 1)
        {
            WebTools.AlertMsgAdd("Nie zaznaczono pojedynczej pozycji");
        }
        else
        {
            if (Page.IsValid)
            {
                int userId;

                if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
                {
                    double quantity = double.Parse(TextBox_Quantity1.Text.Replace(".", ","));

                    DateTime? requiredDate = null;

                    /* from dbo.WarehouseDocumentPositionKCStateType
                     * Id	Name
                     * 1	zwrot do dostawcy
                     * 2	na stan magazynowy
                     * 3	przypisanie do zlecenia
                     * 4	przypisanie do działu
                     */
                    int stateTypeId;
                    int.TryParse(RadComboBox_KCStates.SelectedValue, out stateTypeId);

                    if (stateTypeId <= 0)
                    {
                        WebTools.AlertMsgAdd("Nie wybrano typu stanu");
                    }
                    else
                    {
                        // return to supplier
                        if (stateTypeId == 1)
                        {
                            requiredDate = RadDatePicker_ReturnDate.SelectedDate;
                            if (!requiredDate.HasValue)
                            {
                                WebTools.AlertMsgAdd("Nie wybrano daty");
                                return;
                            }
                        }

                        int? orderId = null;
                        var item = CustomsChamberMaterialsGrid.SelectedItems[0] as GridDataItem;
                        if (stateTypeId == 3)
                        {
                            if (CheckBox_GetOrderFromKCPos.Checked)
                            {
                                orderId = item.GetCellValueAs<int>(CustomsChamberMaterialsGrid_OrderId_UniqueName);//var item = CustomsChamberMaterialsGrid.SelectedItems[0];
                            }
                            else
                            {
                                orderId = int.Parse(RadComboBox_Orders.SelectedValue);//int.TryParse(RadComboBox_Orders.SelectedValue, out orderId);
                            }

                            if (!orderId.HasValue || orderId < 1)
                            {
                                WebTools.AlertMsgAdd("Nie wybrano prawidłowego przypisania");
                                return;
                            }
                        }

                        //var item = CustomsChamberMaterialsGrid.SelectedItems[0];
                        int whDocPosId = item.GetCellValueAs<int>(CustomsChamberMaterialsGrid_WarehouseDocumentPositionId_UniqueName);

                        using (var warehouseDs = new WarehouseDS())
                        {
                            var systemUserId = warehouseDs.GetPersonForSystemUser(userId).Id;
                            var message = warehouseDs.AddNewCustomsChamberDispositionState(stateTypeId, whDocPosId, systemUserId, quantity, requiredDate ?? DateTime.Now, orderId, TextBox_Comment.Text.Replace("'", ""));

                            WebTools.AlertMsgAdd(message);
                            int itemOldIndex = CustomsChamberMaterialsGrid.SelectedItems[0].ItemIndex;
                            CustomsChamberMaterialsGrid.Rebind();
                            CustomsChamberMaterialsGrid.SelectedIndexes.Add(itemOldIndex);

                            RadComboBox_KCStates.Text = "";
                            RadComboBox_KCStates.ClearSelection();

                            RadComboBox_Orders.Text = "";
                            RadComboBox_Orders.ClearSelection();

                            RadDatePicker_ReturnDate.SelectedDate = null;
                            RadDatePicker_ReturnDate.Clear();

                            TextBox_Quantity1.Text = "";
                            TextBox_Comment.Text = "";

                            Panel_Dates.Visible = false;
                            Panel_Orders.Visible = false;
                            Button_Disposition.Enabled = false;

                            //CustomsChamberMaterialsGrid.SelectedItems[0].Expanded = true;
                        }

                    }
                }
            }
        }
    }


    protected void RadDatePicker_DueDateTime_OnLoad(object sender, EventArgs e)
    {

    }

    protected void RadDatePicker_DueDateTime_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
      if (!isExtendedMode)
        return;

      var gridTableRow = ((GridTableRow)(((RadDatePicker)sender).BindingContainer));
      var datepickeruniqueidcell = gridTableRow.Cells[11];
      datepickeruniqueidcell.Text = ((RadDatePicker)sender).UniqueID;
    }

    protected void Button_ChangeDueDateTime_Click(object sender, EventArgs e)
    {
      if (!isExtendedMode)
        return;

      var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer)); //GridDataItem

      //var cell0Text = gridTableRow.Cells[0].Text;
      //var cell1Text = gridTableRow.Cells[1].Text;
      //var cell2Text = gridTableRow.Cells[2].Text;
      var cell3Text = gridTableRow.Cells[3].Text;
      //var cell4Text = gridTableRow.Cells[4].Text;
      //var cell5Text = gridTableRow.Cells[5].Text;
      var datepickeruniqueidcellText = gridTableRow.Cells[11].Text;

      int KCStateId = int.Parse(cell3Text);

      RadDatePicker dateTimePicker = (RadDatePicker)Page.FindControl(datepickeruniqueidcellText);



      //var tableRowControls = gridTableRow.Controls;

      //foreach (var control in tableRowControls)
      //{
      //    var item = ((GridTableCell) control).Item;
      //    if(item.HasChildItems)
      //    {

      //    }

      //    var controlType = control.GetType();
      //    if(controlType == typeof(RadDateTimePicker))
      //    {
      //        dateTimePicker = (RadDateTimePicker)control;
      //    }
      //}

      //RadDateTimePicker dateTimePicker = (RadDateTimePicker) sender;

      if (dateTimePicker != null && dateTimePicker.SelectedDate.HasValue)
      {

        using (var warehouseDs = new WarehouseDS())
        {
          var res = warehouseDs.ChangeKCRequiredTime(KCStateId, dateTimePicker.SelectedDate.Value);
          WebTools.AlertMsgAdd(res);
        }
        var gridTable = (GridTableView)gridTableRow.BindingContainer;
        gridTable.Rebind();
      }
      else
      {
        WebTools.AlertMsgAdd("Nie wybrano nowego czasu!");
      }
      // Update KCState basing on Id

    }
    protected void Button_DeleteDisposition_Click(object sender, EventArgs e)
    {
      if (!isExtendedMode)
        return;

      var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer)); //GridDataItem
      var cell3Text = gridTableRow.Cells[3].Text;

      int KCStateId = int.Parse(cell3Text);
      using (var warehouseDs = new WarehouseDS())
      {
        var res = warehouseDs.CancelKCDisposition(KCStateId);
        WebTools.AlertMsgAdd(res);
      }
      //var gridTable = (GridTableView)gridTableRow.BindingContainer;
      //gridTable.Rebind();
      CustomsChamberMaterialsGrid.Rebind();
    }
  }
}