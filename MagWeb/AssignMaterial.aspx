﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignMaterial.aspx.cs" MasterPageFile="~/WarehouseMaster.Master" Inherits="MagWeb.AssignMaterial" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Przypisanie materiału
    </p>
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <table>
            <tr>
                <td valign="top">
                    <div>
                        <fieldset>
                            <legend>Wolne materiały na stanie magazynu</legend>
                            <table>
                                <tr>
                                    <td>
                                        <div class="floatLeft">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblName" AssociatedControlID="txtName">Nazwa:</asp:Label>
                                            <telerik:RadComboBox ID="txtName" runat="server">
                                                <WebServiceSettings Method="GetTelericMaterialsList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="floatLeft">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblIndex" AssociatedControlID="txtIndex">Indeks:</asp:Label>
                                            <telerik:RadComboBox ID="txtIndex" runat="server">
                                                <WebServiceSettings Method="GetTelericIndexesSIMPLEList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="floatLeft" id="divShelf" runat="server">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblShelf" AssociatedControlID="cmbShelf">Lokalizacja:</asp:Label>
                                            <telerik:RadComboBox ID="cmbShelf" runat="server">
                                                <WebServiceSettings Method="GetTelericShelfsList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td valign="bottom">
                                        <asp:Button ID="btnSearch" runat="server" OnClick="SearchClick" Text="Wyszukaj" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <telerik:RadGrid ID="StackGrid" runat="server" OnNeedDataSource="StackGrid_OnNeededDataSource" EnableViewState="false" PageSize="10">
                                            <ClientSettings Selecting-AllowRowSelect="true" />
                                            <MasterTableView EnableViewState="true">
                                                <Columns>
                                                    <telerik:GridBoundColumn HeaderText="MaterialId" UniqueName="MaterialId" DataField="MaterialId" Display="false" />
                                                    <telerik:GridBoundColumn HeaderText="PositionId" UniqueName="PositionId" DataField="PositionId" Display="false" />
                                                    <telerik:GridBoundColumn HeaderText="ShelfId" UniqueName="ShelfId" DataField="ShelfId" Display="false" />
                                                    <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                                    <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                    <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" />
                                                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                                    <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="labelQuantity" runat="server" Text="Ilość do przypisania:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadTextBox ID="txtQuantity" runat="server">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="QuantityRequiredFieldValidator" runat="server" ErrorMessage="Ilość jest wymagana"
                                    ControlToValidate="txtQuantity" ValidationGroup="AssignMaterialGroup">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Przypisywana ilość musi być liczbą dadatnią"
                                    ValidationGroup="AssignMaterialGroup" OnServerValidate="DataTypeValidator_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="QuantityCustomValidator" runat="server" ErrorMessage="Nie można przypisać więcej materiału niż jest dostępne"
                                    ValidationGroup="AssignMaterialGroup" OnServerValidate="Quantity_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału do przypisania"
                                    ValidationGroup="AssignMaterialGroup" OnServerValidate="MaterialCustomValidator_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="labelOrderNumber" runat="server" Text="Przypisanie:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadComboBox ID="cmbOrderNumber" runat="server">
                                    <WebServiceSettings Method="GetTelericOrdersList" Path="WarehouseWebWcf.svc" />
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="OrderNumberRequiredFieldValidator" runat="server"
                                    ErrorMessage="Przypisanie jest wymagane" ControlToValidate="cmbOrderNumber"
                                    ValidationGroup="AssignMaterialGroup">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="buttonAssignMaterial" runat="server" Text="Przypisz materiał"
                                    ValidationGroup="AssignMaterialGroup" OnClick="ButtonAssignMaterial_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
