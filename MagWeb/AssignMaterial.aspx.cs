﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;
//using WarehouseWeb;

namespace MagWeb
{
    public partial class AssignMaterial : FeaturedSystemWebUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, currentWarehouseId, WarehouseDocumentTypeEnum.PM, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
					PermissionDenied();
					return;
                }
            }
            
            if (!IsPostBack)
            {
                inputDate.SelectedDate = DateTime.Now.Date;
            }

            var companyId = (int?)Session[SessionData.SessionKey_CurrentCompanyId];

            if (!Page.IsPostBack)
                txtName.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
        }

        protected void StackGrid_OnNeededDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var materials = from p in warehouseDs.GetCurrentStackInDocuments(txtName.Text, txtIndex.Text, (int?) Session[SessionData.SessionKey_CurrentWarehouseId])
                    where p.OrderId == null &&
                          (String.IsNullOrEmpty(cmbShelf.Text) ||
                           p.ShelfName.ToLower().Contains(cmbShelf.Text.ToLower()))
                    select p;

                StackGrid.DataSource = materials.ToArray();

            }
        }

        protected void SearchClick(object sender, EventArgs e)
        {
            this.StackGrid.Rebind();
        }

        protected void ButtonAssignMaterial_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    if (Session[SessionData.SessionKey_UserId] != null)
                    {
                        var quantity = decimal.Parse(txtQuantity.Text);
                        int orderId = int.Parse(cmbOrderNumber.SelectedValue);

                        int userId = (int)Session[SessionData.SessionKey_UserId];
                        int warehouseSourceId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                        GridItem item = StackGrid.SelectedItems[0];

                        int materialId = int.Parse(item.Cells[2].Text);
                        int positionId = int.Parse(item.Cells[3].Text);
                        int shelfId = int.Parse(item.Cells[4].Text);

                        WarehouseDocumentPosition documentPosition = warehouseDs.GetPositionById(positionId);
                        WarehouseDocument document = warehouseDs.GetWarehouseDocumentForPositionId(positionId);
                        Zone zone = warehouseDs.GetShelfByIdDataOnly(shelfId);
                        
                        WarehouseDocumentPosition warehouseDocumentPosition1 =
                            warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, documentPosition.UnitPrice, -quantity, positionId, 0);
                        
                        WarehouseDocumentPosition warehouseDocumentPosition2 =
                            warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, documentPosition.UnitPrice, quantity, positionId, orderId);

                        PositionWithQuantityAndShelfs positionWithQuantityAndShelf1 =
                            new PositionWithQuantityAndShelfs()
                            {
                                position = warehouseDocumentPosition1,
                                documentParentIds = new List<int>() { document.Id },
                                positionParentIds =
                                    new List<ChildParentPositionQuantity>()
                                            {
                                                new ChildParentPositionQuantity()
                                                    {
                                                        PositionParentID = documentPosition.Id,
                                                        Quantity = (decimal)-warehouseDocumentPosition1.Quantity
                                                    }
                                            },
                                WarehousePositionZones =
                                    new List<WarehousePositionZone>()
                                            {
                                                new WarehousePositionZone()
                                                    {
                                                        WarehouseDocumentPosition = warehouseDocumentPosition1,
                                                        Zone = zone,
                                                        Quantity = -warehouseDocumentPosition1.Quantity
                                                    }
                                            }
                            };

                        PositionWithQuantityAndShelfs positionWithQuantityAndShelf2 =
                            new PositionWithQuantityAndShelfs()
                            {
                                position = warehouseDocumentPosition2,
                                documentParentIds = new List<int>() { document.Id },
                                positionParentIds =
                                    new List<ChildParentPositionQuantity>()
                                            {
                                                new ChildParentPositionQuantity()
                                                    {
                                                        PositionParentID = documentPosition.Id,
                                                        Quantity = (decimal)warehouseDocumentPosition2.Quantity
                                                    }
                                            },
                                WarehousePositionZones =
                                    new List<WarehousePositionZone>()
                                            {
                                                new WarehousePositionZone()
                                                    {
                                                        WarehouseDocumentPosition = warehouseDocumentPosition2,
                                                        Zone = zone,
                                                        Quantity = warehouseDocumentPosition2.Quantity
                                                    }
                                            }
                            };

                        lock (typeof(WarehouseDS))
                        {
                            try
                            {
                                warehouseDs.AssignMaterialToOrder(warehouseSourceId, warehouseDs.GetPersonForSystemUser(userId).Id,
                                                              new List<PositionWithQuantityAndShelfs>()
                                                                  {
                                                                      positionWithQuantityAndShelf1,
                                                                      positionWithQuantityAndShelf2
                                                                  }, inputDate.SelectedDate ?? DateTime.Now.Date);
                            }
                            catch (Exception ex)
                            {
                                WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);

                            }
                        }

                        StackGrid.SelectedIndexes.Clear();
                        StackGrid.Rebind();
                    }
                }
            }
        }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                txtQuantity.Text = txtQuantity.Text.Replace(".", ",");
                args.IsValid = (double.Parse(txtQuantity.Text) > 0);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (StackGrid.SelectedItems.Count == 1)
            {
                GridItem item = StackGrid.SelectedItems[0];

                double availableQuantity = double.Parse(item.Cells[8].Text);

                try
                {
                    double quantity = double.Parse(txtQuantity.Text.Replace(".", ","));
                    if (quantity > availableQuantity)
                    {
                        args.IsValid = false;
                        return;
                    }
                }
                catch
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (StackGrid.SelectedItems.Count == 1);
        }

    }
}
