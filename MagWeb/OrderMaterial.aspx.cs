using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class OrderMaterial : FeaturedSystemWebUIPage
    {
        private const int INDEX_MATERIAL_ID_IN_GRID = 3;
        private const int INDEX_AVAILABLE_QUANTITY_IN_GRID = 6;
        private const int INDEX_ORDERNUMBER_IN_GRID = 8;

        private List<PositionWithQuantityAndShelfs> positionsToAdd;
        private string orderNumber;

        protected void Page_Load(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.Za, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
					PermissionDenied();
					return;
                }
            }

            if (!IsPostBack)
            {
                Session["OrderMaterial_positionsToAdd"] = null;
                Session["OrderNumber"] = null;
            }

            if (!IsPostBack)
            {
                int currentWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];
                int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    WarehouseRCB.DataSource = from w in warehouseDs.GetAllWarehouses(currentComapnyId) where w.Id != currentWarehoseId select new { Value = w.Id, Text = w.Name };
                    WarehouseRCB.DataTextField = "Text";
                    WarehouseRCB.DataValueField = "Value";
                    WarehouseRCB.DataBind();
                    WarehouseRCB.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
                }

                inputDate.SelectedDate = DateTime.Now.Date;
            }

            if (Session["OrderMaterial_positionsToAdd"] == null)
                positionsToAdd = new List<PositionWithQuantityAndShelfs>();
            else
                positionsToAdd = (List<PositionWithQuantityAndShelfs>)Session["OrderMaterial_positionsToAdd"];

            orderNumber = (string)Session["OrderNumber"];
        }

        protected void MaterialsToReserveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string materialToFind = MaterialToSearchRTB.Text;
                string indexSimpleToFind = IndexSIMPLEToSearchRTB.Text;
                string orderNumberToFind = OrderNumberToSearchRTB.Text;
                int currentWarehouseId = int.Parse(WarehouseRCB.SelectedItem.Value);

                var materialToOrderWithoutPositions =
                    from p in warehouseDs.GetCurrentStackInDocuments(materialToFind, indexSimpleToFind, currentWarehouseId)
            where p.OrderNumber == null || (p.OrderNumber != null && (String.IsNullOrEmpty(orderNumberToFind) || p.OrderNumber.Contains(orderNumberToFind)))
                    group p by new { p.MaterialId, p.MaterialName, p.IndexSIMPLE, p.UnitName, p.OrderId, p.OrderNumber }
                        into g
                        select
                        new
                        {
                            g.Key.MaterialId,
                            g.Key.MaterialName,
                            g.Key.IndexSIMPLE,
                            Quantity = g.Sum(s => s.Quantity),
                            g.Key.UnitName,
                            g.Key.OrderId,
                            g.Key.OrderNumber
                        };

                var materialToOrderWithoutPositionsMaterialized = materialToOrderWithoutPositions.ToArray();

        List<CurrentStackInDocument> currentStackInDocumentses = new List<CurrentStackInDocument>();

                foreach (var materialized in materialToOrderWithoutPositionsMaterialized)
                {
                    double usedQuantity = 0;

                    var positionWQASs =
                        positionsToAdd.Where(
                            p =>
                            p.position.Material.Id == materialized.MaterialId && p.position.Material.Name == materialized.MaterialName &&
                            p.position.Order.OrderNumber == materialized.OrderNumber);

                    //var mat1 = positionsToAdd.ToArray();
                    //var mat = positionWQASs.ToArray();

                    if (positionWQASs.Count() > 0)
                    {
                        foreach (var wqaS in positionWQASs)
                        {
                            usedQuantity += Math.Abs(wqaS.position.Quantity);
                        }
                    }

          currentStackInDocumentses.Add(new CurrentStackInDocument()
                    {
                        MaterialId = materialized.MaterialId,
                        MaterialName = materialized.MaterialName,
                        IndexSIMPLE = materialized.IndexSIMPLE,
                        Quantity = materialized.Quantity - usedQuantity,
                        UnitName = materialized.UnitName,
                        OrderNumber = materialized.OrderNumber
                    });
                }

                int i = 1;

                var materialToOrder = from p in currentStackInDocumentses
                                      select
                                       new
                                       {
                                           RowNumber = i++,
                                           IdMaterial = p.MaterialId,
                                           MaterialName = p.MaterialName,
                                           IndexSIMPLE = p.IndexSIMPLE,
                                           NameSIMPLE = p.NameSIMPLE,
                                           Quantity = p.Quantity,
                                           UnitName = p.UnitName,
                                           OrderNumber = p.OrderNumber
                                       };

                MaterialsToReserveRadGrid.DataSource = materialToOrder;
            }
        }
        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            MaterialsToReserveRadGrid.Rebind();
        }

        protected void AddMaterialToReserveClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                WarehouseDocumentPosition warehouseDocumentPositionCandidate;
                GridItem item = MaterialsToReserveRadGrid.SelectedItems[0];
                var cell = item.Cells[INDEX_MATERIAL_ID_IN_GRID];
                int materialId = int.Parse(cell.Text);
                int sourceWarehouseId = int.Parse(WarehouseRCB.SelectedItem.Value);

        var quantity = decimal.Parse(QuantityRTB.Text);
        IQueryable<CurrentStackInDocument> stack;
        CurrentStackInDocument[] materializedStack;

        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
          warehouseDocumentPositionCandidate = warehouseDs.CreateWarehouseDocumentPosition(materialId, 0, quantity);//.CreateWarehouseDocumentPositionDataOnly(materialId, 0, quantity);
                    stack = from p in warehouseDs.GetCurrentStackInDocuments("", "", sourceWarehouseId)
                            where p.MaterialId == materialId
                            select p;

                    materializedStack = stack.ToArray();
                }

        foreach (var currentStackInDocument in materializedStack.OrderBy(s => s.PositionId))
                {
          using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        WarehouseDocumentPosition warehouseDocumentPosition;

            var used = 0M;

            var materialMatch = positionsToAdd.Where(p => p.position.Material.Id == currentStackInDocument.MaterialId);
                        foreach (var match in materialMatch)
                        {
              var foundShelf = match.WarehousePositionZones.FirstOrDefault(pz => pz.Zone.Id == currentStackInDocument.ShelfId);
                            if (foundShelf != null)
                            {
                used = (decimal)foundShelf.Quantity;
                            }
                        }

            if ((decimal)warehouseDocumentPositionCandidate.Quantity <= (decimal?)currentStackInDocument.Quantity - used)
                        {
              warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, currentStackInDocument.UnitPrice,
                                                                                              (decimal)-warehouseDocumentPositionCandidate.Quantity,
                                                                                              currentStackInDocument.PositionId,
                                                                                              currentStackInDocument.OrderId.Value);

                            warehouseDocumentPositionCandidate.Quantity = 0;
                        }
                        else
                        {
              warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly
              (
                materialId,
                currentStackInDocument.UnitPrice,
                (decimal)-currentStackInDocument.Quantity.Value,
                currentStackInDocument.PositionId,
                currentStackInDocument.OrderId.Value
              );

              warehouseDocumentPositionCandidate.Quantity -= (double)currentStackInDocument.Quantity;
                        }

            if (warehouseDocumentPosition.Quantity == 0)
              continue;

                        if (positionsToAdd.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id) == 1)
                        {
                            PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
                                positionsToAdd.First(
                                    p =>
                                    p.position.Material.Id == warehouseDocumentPosition.Material.Id);

                            if (Math.Abs(positionsWithShelfsWhichExists.position.Quantity + warehouseDocumentPosition.Quantity) <= materializedStack.Sum(m => m.Quantity))
                            {
                                positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
                positionsWithShelfsWhichExists.documentParentIds.Add(currentStackInDocument.WarehouseDocumentId);
                                positionsWithShelfsWhichExists.positionParentIds.Add(new ChildParentPositionQuantity()
                                                                                         {
                                                                           PositionParentID = currentStackInDocument.PositionId,
                                                                           Quantity = (decimal)-warehouseDocumentPosition.Quantity
                                                                                         });

          
                                    var positionWithZone =
                    positionsWithShelfsWhichExists.WarehousePositionZones.FirstOrDefault(pswz => pswz.Zone.Id == currentStackInDocument.ShelfId);
                                    if (positionWithZone != null)
                                    {
                                        positionWithZone.Quantity += -warehouseDocumentPosition.Quantity;
                                    }
                                    else
                                    {
                  Zone zone = warehouseDs.GetShelfByIdDataOnly((int)currentStackInDocument.ShelfId);

                                        positionsWithShelfsWhichExists.WarehousePositionZones.Add(new WarehousePositionZone()
                                                                                                      {
                                                                                                          WarehouseDocumentPosition = positionsWithShelfsWhichExists.position,
                                                                                                          Zone = zone,
                                                                                                          Quantity = -warehouseDocumentPosition.Quantity
                                                                                                      });
                                    }
                                
                            }
                            else
                            {
                warehouseDocumentPositionCandidate.Quantity += (double)currentStackInDocument.Quantity;
                            }
                        }
                        else
                        {
                            List<WarehousePositionZone> warehousePositionZones = null;
              Zone zone = warehouseDs.GetShelfByIdDataOnly((int)currentStackInDocument.ShelfId);

              if (zone != null)
              {
                                    warehousePositionZones = new List<WarehousePositionZone>();
                warehousePositionZones.Add(new WarehousePositionZone
                {
                                                                       WarehouseDocumentPosition = warehouseDocumentPosition,
                                                                       Zone = zone,
                                                                       Quantity = -warehouseDocumentPosition.Quantity
                                                                   });
                                }
                            

                            double? availableQuantityInStack = materializedStack.Sum(m => m.Quantity);

                            if (availableQuantityInStack == null) throw new ApplicationException("Something wrong with available quantity for Za");

                            if (Math.Abs(warehouseDocumentPosition.Quantity) <= availableQuantityInStack)
                            {
                                positionsToAdd.Add(new PositionWithQuantityAndShelfs()
                                                       {
                                                           position = warehouseDocumentPosition,
                                         documentParentIds = new List<int>() { currentStackInDocument.WarehouseDocumentId },
                                         positionParentIds = new List<ChildParentPositionQuantity>() 
                                                           { 
                                                             new ChildParentPositionQuantity() 
                                                             { 
                                                               PositionParentID = currentStackInDocument.PositionId, 
                                                               Quantity = (decimal)-warehouseDocumentPosition.Quantity
                                                             }
                                                           },
                                                           WarehousePositionZones = warehousePositionZones
                                                       });
                            }
                            else
                            {
                warehouseDocumentPositionCandidate.Quantity += (double)currentStackInDocument.Quantity;
                            }

                        }
                    }

          if (warehouseDocumentPositionCandidate.Quantity == 0)
            break;
          if (warehouseDocumentPositionCandidate.Quantity < 0)
            throw new ApplicationException("Internal application error during ordering material");
                }

                if (warehouseDocumentPositionCandidate.Quantity > 0)
                {
                    throw new ApplicationException("Not enought material for given position - probably data is inconsistent or a bug in application");
                }

                Session["OrderMaterial_positionsToAdd"] = positionsToAdd;

                QuantityRTB.Text = "";
                MaterialsToReserveRadGrid.SelectedIndexes.Clear();
                MaterialsToReserveRadGrid.Rebind();
                MaterialsWhichWillBeOrderRadGrid.Rebind();
            }
        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            int userId;

            if (positionsToAdd.Count > 0)
            {
                if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
                {
                    WarehouseDocument warehouseDocument;

          using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        int sourceWarehoseId = int.Parse(WarehouseRCB.Items.FindItemByText(WarehouseRCB.Text).Value);
            int targetWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                        lock (typeof(WarehouseDS))
                        {
                            try
                            {
                                warehouseDocument = warehouseDs.OrderMaterialFrom(sourceWarehoseId, targetWarehoseId, warehouseDs.GetPersonForSystemUser(userId).Id,
                                                                              positionsToAdd, null,
                                                                              inputDate.SelectedDate ??
                                                                              DateTime.Now.Date);

                                WarehouseRCB.Enabled = false;
                                QuantityRTB.Enabled = false;
                                btnPrintDocument.Enabled = true;
                                btnSaveDocument.Enabled = false;
                                btnAddPosition.Enabled = false;

                                DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument, WarehouseDocumentTypeEnum.Za, warehouseDocument.Id);
                            }
                            catch (Exception ex)
                            {
                                WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                            }
                        }

                        positionsToAdd = new List<PositionWithQuantityAndShelfs>();
                        Session["OrderMaterial_positionsToAdd"] = positionsToAdd;
                        WarehouseRCB.SelectedIndex = -1;
                        MaterialsToReserveRadGrid.Rebind();
                    }                   
                }
            }
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            WarehouseRCB.Enabled = true;
            QuantityRTB.Enabled = true;
            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = true;
            btnAddPosition.Enabled = true;

            positionsToAdd = new List<PositionWithQuantityAndShelfs>();
            Session["OrderMaterial_positionsToAdd"] = positionsToAdd;

            WarehouseRCB.SelectedIndex = -1;
            MaterialsToReserveRadGrid.Rebind();
            MaterialsWhichWillBeOrderRadGrid.Rebind();
        }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {

            //args.IsValid = WarehouseDS.IsValidQuantity(QuantityRTB.Text);

            try
            {
                QuantityRTB.Text = QuantityRTB.Text.Replace(".", ",");
                args.IsValid = double.Parse(QuantityRTB.Text) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void MaterialsToReceiveRadGrid_DeleteRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                positionsToAdd.RemoveAt(e.Item.ItemIndex);
                Session["OrderMaterial_positionsToAdd"] = positionsToAdd;
                MaterialsWhichWillBeOrderRadGrid.Rebind();
            }
            else
            {
                throw new ApplicationException("Unknown grid command");
            }
        }

        protected void MaterialsWhichWillBeOrderRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (positionsToAdd != null)
            {
                int i = 1;

                var source2 = from p in positionsToAdd
                              select
                                  new
                                      {
                                          RowNumber = i++,
                                          MaterialName = p.position.Material.Name,
                                          IndexSIMPLE = p.position.Material.IndexSIMPLE,
                                          NameSIMPLE = p.position.Material.Name,
                                          Quantity = -p.position.Quantity,
                                          UnitName = p.position.Material.Unit.Name,
                                      };

                MaterialsWhichWillBeOrderRadGrid.DataSource = source2;
            }
        }

        protected void WarehouseRCB_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            positionsToAdd = new List<PositionWithQuantityAndShelfs>();
            Session["OrderMaterial_positionsToAdd"] = positionsToAdd;
            MaterialsToReserveRadGrid.Rebind();
            MaterialsWhichWillBeOrderRadGrid.Rebind();
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = MaterialsToReserveRadGrid.SelectedItems.Count == 1 ? true : false;
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (MaterialsToReserveRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialsToReserveRadGrid.SelectedItems[0];
                double quantity = double.Parse(QuantityRTB.Text.Replace(".", ","));

                double availableQuantity = double.Parse(item.Cells[INDEX_AVAILABLE_QUANTITY_IN_GRID].Text);

                if (quantity > availableQuantity)
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        protected void OrderNumber_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (positionsToAdd.Count() != 0 && MaterialsToReserveRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialsToReserveRadGrid.SelectedItems[0];
                var cell = item.Cells[INDEX_ORDERNUMBER_IN_GRID];
                string newOrderNumber = cell.Text;

                foreach (var s in positionsToAdd)
                {
                    if (s.position.Order.OrderNumber != newOrderNumber)
                    {
                        args.IsValid = false;
                        return;
                    }
                }
            }

            args.IsValid = true;
        }
    }
}
