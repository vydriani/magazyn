﻿<%@ Page Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="ManageWarehouseGroups.aspx.cs" Inherits="MagWeb.ManageWarehouseGroups" %>


<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
 
    <p>
    <b>Zarządzanie istniejącymi grupami:</b>
    </p>
    <table style="width: 500px;">
        <tr>
<%--            <td valign="top">
                Wybierz&nbsp;firmę:
            </td>--%>
            <td valign="top">
                Wybierz&nbsp;grupę&nbsp;źródłową:
            </td>
            <td valign="top">
                Wybierz&nbsp;grupę&nbsp;docelową:
            </td>
            <td valign="top">
                Obsługiwane&nbsp;typy&nbsp;dokumentów:
            </td>
        </tr>
        <tr>
<%--            <td valign="top">
                <telerik:RadComboBox Skin="Metro" ID="CompanyRCB" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="CompanyRCB_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>--%>
            <td valign="top">
                <telerik:RadGrid ID="WarehouseGroupsGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                    OnNeedDataSource="WarehouseGroupsGrid_OnNeedDataSource" GridLines="None" 
                    OnItemCommand="WarehouseGroupsGrid_ItemCommand">
                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="ID" UniqueName="WarehouseGroupID"
                                DataField="WarehouseGroupID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa skrócona grupy" UniqueName="WarehouseGroupShortName"
                                DataField="WarehouseGroupShortName" Visible="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa grupy" UniqueName="WarehouseGroupName"
                                DataField="WarehouseGroupName" Visible="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="False" />
                    </ClientSettings>
                    
                </telerik:RadGrid>
            </td>
            <td valign="top">
                <telerik:RadGrid ID="TargetWarehouseGroupsGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                    OnNeedDataSource="TargetWarehouseGroupsGrid_OnNeedDataSource" GridLines="None" 
                    OnItemCommand="TargetWarehouseGroupsGrid_ItemCommand">
                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="ID" UniqueName="Id"
                                DataField="Id" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa skrócona grupy" UniqueName="ShortName"
                                DataField="ShortName" Visible="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa grupy" UniqueName="Name"
                                DataField="Name" Visible="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="False" />
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
            <td valign="top" colspan="3">
                <telerik:RadGrid ID="DocumentTypeGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                    OnNeedDataSource="DocumentTypeGrid_OnNeedDataSource" GridLines="None" 
                    OnItemCommand="DocumentTypeGrid_ItemCommand">
                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="PermissionID" UniqueName="PermissionId"
                                DataField="PermissionId" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="TypeID" UniqueName="TypeId"
                                DataField="TypeId" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa skrócona typu" UniqueName="ShortType"
                                DataField="ShortType" Visible="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa typu" UniqueName="Type"
                                DataField="Type" Visible="true">
                            </telerik:GridBoundColumn>    
                            <telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>                                
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="False" />
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
        
        <tr>
            <td colspan="5"></td>
            <td valign="top">
                <div align="right">
                    <asp:Button ID="Button_RemovePermission" runat="server" 
                        Text="Usuń typ dokumentu" OnClick="Button_RemovePermission_Click" ValidationGroup="RemovePermission" />
                </div>
            </td>
        </tr>
        
        <tr>
            <td colspan="3"></td>
            <td colspan="3" valign="top" align="right">
                <asp:CustomValidator ID="CustomValidatorRemovePermission" runat="server" 
                    ErrorMessage="" onservervalidate="CustomValidatorRemovePermission_ServerValidate" ValidationGroup="RemovePermission">
                </asp:CustomValidator>
            </td>
        </tr>
        
        <tr>

            <td valign="top" class="style1">
                <%--<div align="right">
                    <asp:Button ID="Button_ShowTargetWarehouses" runat="server" 
                        Text="Pokaż magazyny docelowe" OnClick="Button_ShowTargetWarehouses_Click" />
                </div>--%>
            </td>
            <td valign="top" class="style1">
                <div align="right">
                    <asp:Button ID="Button_ShowTypes" runat="server" 
                        Text="Pokaż obsługiwane typy dokumentów" OnClick="Button_ShowTypes_Click" ValidationGroup="ShowTypes" Visible="false" />
                </div>
            </td>
            <td valign="top" align="right" class="style1">
                <asp:Label ID="Label_DocumentType" runat="server" Text="Typ dokumentu:"></asp:Label>
            </td>
            <td valign="top" class="style1">
                <telerik:RadComboBox Skin="Metro" ID="DocumentTypeRCB" runat="server" />
            </td>
            <td valign="top" class="style1">
                <div align="right">
                    <asp:Button ID="Button_AddPermission" runat="server" 
                        Text="Dodaj typ dokumentu" OnClick="Button_AddPermission_Click" ValidationGroup="AddPermission" />
                </div>
            </td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td valign="top" align="right">
                <asp:CustomValidator ID="CustomValidatorShowTypes" runat="server" 
                    ErrorMessage="" onservervalidate="CustomValidatorShowTypes_ServerValidate" ValidationGroup="ShowTypes">
                </asp:CustomValidator>
            </td>
            <td valign="top" colspan="3" align="right">
                <asp:CustomValidator ID="CustomValidatorAddPermission" runat="server" 
                    ErrorMessage="" onservervalidate="CustomValidatorAddPermission_ServerValidate" ValidationGroup="AddPermission">
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
            &nbsp;
            </td>
            
            <td>
            &nbsp;
                <table style="width: 500px;">
                    <tr>
                        <td>
                            <asp:Label ID="Label_NewName" runat="server" 
                                Text="Nowa nazwa grupy (źródłowej):"></asp:Label>
                            
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>                
                    <tr>
                        <td>
                            <div align="left">
                                <asp:TextBox ID="TextBox_Name" runat="server" ValidationGroup="FormElements_Name" Width="200px"></asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <div align="right">
                                <asp:Button ID="Button_ChangeName" runat="server" Text="Zmień nazwę grupy" 
                                    onclick="Button_ChangeName_Click" ValidationGroup="FormElements_Name" 
                                    Width="200px" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div align="left">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator_Name" runat="server" 
                                    ErrorMessage="Nazwa nie może być pusta" ControlToValidate="TextBox_Name" 
                                    ValidationGroup="FormElements_Name"></asp:RequiredFieldValidator>
                                <br />
                                <asp:CustomValidator ID="CustomValidator_Name" runat="server" ErrorMessage="Error"></asp:CustomValidator>   
                            </div> 
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
            
                </table>
                <table style="width: 500px;">
                    <tr>
                        <td>
                            <asp:Label ID="Label_NewShortName" runat="server" 
                                Text="Nowa nazwa skrócona grupy (źródłowej):"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div align="left">
                                <asp:TextBox ID="TextBox_ShortName" runat="server" ValidationGroup="FormElements_ShortName" Width="200px" MaxLength="7"></asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <div align="right">
                                <asp:Button ID="Button_ChangeShortName" runat="server" Text="Zmień nazwę skróconą grupy" 
                                    onclick="Button_ChangeShortName_Click" ValidationGroup="FormElements_ShortName" 
                                    Width="200px" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div align="left">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator_ShortName" runat="server" 
                                    ErrorMessage="Nazwa skrócona nie może być pusta" ControlToValidate="TextBox_ShortName" 
                                    ValidationGroup="FormElements_ShortName"></asp:RequiredFieldValidator>
                                <br />
                                <asp:CustomValidator ID="CustomValidator_ShortName" runat="server" ErrorMessage="Error"></asp:CustomValidator>   
                            </div> 
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
            
                </table>
            </td>
            
            <td style="text-decoration: underline">
            </td>
            <td colspan="3">
                <asp:Panel ID="Panel_AllowedStatuses" runat="server">
                    <asp:Label ID="Label_AllowedStatuses" runat="server" Text="Dozwolone statusy obiektów:"></asp:Label>
                    <br />
                    <asp:CheckBox ID="CheckBox_Package" runat="server" Text="Opakowania" 
                        Checked="True" />
                    <br />
                    <asp:CheckBox ID="CheckBox_ZZ" runat="server" Text="Pozycje ZZ" 
                        Checked="True" />
                    <br />
                    <asp:CheckBox ID="CheckBox_Garbage" runat="server" Text="Odpady" />
                    <br />
                    <asp:CheckBox ID="CheckBox_Damaged" runat="server" Text="Pozycje uszkodzone" 
                        Checked="True" />
                    <br />
                    <asp:CheckBox ID="CheckBox_PartlyDamaged" runat="server" 
                        Text="Pozycje częściowo uszkodzone" Checked="True" />
                    <br />
                    <asp:Button ID="Button_AllowedStatusesUpdate" runat="server" 
                        onclick="Button_AllowedStatusesUpdate_Click" Text="Aktualizuj" />
                </asp:Panel>
            </td>
        </tr>        <tr>
            <td>
            &nbsp;
            </td>
            <td>
                &nbsp;</td>
            <td>
                <table style="width: 500px;"><tr><td></td></tr></table>
            </td>
            <td colspan="3">
                
            </td>
        </tr>
        
        
        <tr>
            <td>
            &nbsp;
            </td>
            <td valign="top">
                &nbsp;</td>
            <td colspan="4"></td>
        </tr>
        
        
    </table>
    
    
    <asp:Panel ID="Panel_CreateWarehouse" runat="server">

        <p>
        <b>Tworzenie nowej grupy:</b>
        </p>
        
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Nazwa grupy *"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label_ShortName" runat="server" Text="Nazwa skrócona *"></asp:Label>
                </td>            
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="WarehouseGroupNameRCB" runat="server" Width="90%">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="WarehouseGroupShortNameRCB" runat="server" Width="90%" MaxLength="7">
                    </telerik:RadTextBox>
                </td>            
                <td style="width:150px;"></td>
                <td>
                    <asp:Button ID="Button2" runat="server" Text="Utwórz" OnClick="CreateWarehouseGroupButton_Clicked"
                        ValidationGroup="NewWarehouseGroup" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Nazwa jest wymagana"
                        ControlToValidate="WarehouseGroupNameRCB" ValidationGroup="NewWarehouseGroup"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CustomValidator ID="CustomValidator_NameExists" runat="server" 
                        ErrorMessage="Grupa o podanej nazwie już istnieje"></asp:CustomValidator>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nazwa skrócona jest wymagana"
                        ControlToValidate="WarehouseGroupShortNameRCB" ValidationGroup="NewWarehouse"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
                        ErrorMessage="Grupa o podanej nazwie już istnieje"></asp:CustomValidator>
                </td>
    <%--            <td>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Firma jest wymagana"
                        ControlToValidate="CompanyRCB" ValueToCompare="(wybierz)" Operator="NotEqual"
                        ValidationGroup="NewWarehouse"></asp:CompareValidator>
                </td>--%>
                <td>
                    &nbsp;
<%--                    <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Typ jest wymagany"
                        ControlToValidate="WarehouseTypeRCB" ValueToCompare="(wybierz)" Operator="NotEqual"
                        ValidationGroup="NewWarehouse"></asp:CompareValidator>--%>
                </td>
                <td>
                    &nbsp;
<%--                    <asp:CustomValidator ID="DepartmentRCBCustomValidator" runat="server" ErrorMessage="Wybierz dział<BR>dla magazynu działowego"
                        ValidationGroup="NewWarehouse" OnServerValidate="DepartmentRCBCustomValidator_ServerValidate"></asp:CustomValidator>--%>
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="Label_StatusMessage" runat="server" Text="StatusMessage"></asp:Label>        
    
    </asp:Panel>
    
    
    

    

</asp:Content>
