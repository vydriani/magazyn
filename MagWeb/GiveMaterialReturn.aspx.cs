﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
//using WarehouseWeb;
//using Client.Domain;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;
//using Warehouse.Agent;
//using WarehouseDocument = Mag.Domain.Model.WarehouseDocument;
//using WarehouseDocumentPosition = Mag.Domain.Model.WarehouseDocumentPosition;
//using WarehouseDocumentTypeEnum = Mag.Domain.Model.WarehouseDocumentTypeEnum;

namespace MagWeb
{
    public partial class GiveMaterialReturn1 : FeaturedSystemWebUIPage
    {
        private const int INDEX_QUANTITY_IN_GRID = 6;
        private const int INDEX_ORDERNUMBER_IN_GRID = 8;
        private const int INDEX_MATERIAL_ID_IN_GRID = 11;

        private const int INDEX_KCDOC_ID_IN_GRID = 12;
        private const int INDEX_KCPOS_ID_IN_GRID = 13;
        private const int INDEX_KCSTATE_ID_IN_GRID = 14;

        private const int INDEX_KCSUPPLIER_ID_IN_GRID = 15;

        //ContractingPartyId

        private List<PositionWithQuantityAndShelfs> positionsToGive;

        protected void Page_Load(object sender, EventArgs e)
        {
            int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            Session["tab_ReturnMaterial"] = 2;

            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, null, WarehouseDocumentTypeEnum.ZTD, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
					PermissionDenied();
					return;
                }
            }

            //if (!IsPostBack)
            //{
            //    inputDate.SelectedDate = DateTime.Now.Date;
            //    Session["GiveMaterialReturn_positionsToGive"] = null;

            //    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            //    {
            //        //RadComboBox_DocSubtypeSIMPLE
            //        RadComboBox_DocSubtypeSIMPLE.DataSource =
            //            from d in warehouseDs.GetDocTypesFromSIMPLE_WZ(currentComapnyId)
            //            select new { Value = d.typdwz_id, Text = d.nazwa.Trim() + " (" + d.typdok_idn + ")" };
            //        RadComboBox_DocSubtypeSIMPLE.DataTextField = "Text";
            //        RadComboBox_DocSubtypeSIMPLE.DataValueField = "Value";
            //        RadComboBox_DocSubtypeSIMPLE.DataBind();
            //        RadComboBox_DocSubtypeSIMPLE.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
            //    }
            //}

            if (Session["GiveMaterialReturn_positionsToGive"] == null)
                positionsToGive = new List<PositionWithQuantityAndShelfs>();
            else
                positionsToGive = (List<PositionWithQuantityAndShelfs>)Session["GiveMaterialReturn_positionsToGive"];

            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            //this.CompanyClientRCB.WebServiceSettings.Method = "GetTelericContractingPartySIMPLEListCompany" + companyId.ToString();

            using (var warehouseDs = new WarehouseDS())
            {
                bool isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility(currentWarehouseId.Value,warehouseDs.GetPersonForSystemUser(userId).Id, 31);
                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }

            if(!Page.IsPostBack)
            {
                RadDatePicker_DateTo.SelectedDate = DateTime.Now.AddDays(2);
            }
        }

        protected void Tab_Prerender(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
        }

        protected void MaterialsToGiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            DateTime dateToFilter = RadDatePicker_DateTo.SelectedDate.HasValue ? RadDatePicker_DateTo.SelectedDate.Value : DateTime.Now.AddDays(1);
            string indexSimpleToFind = IndexSimpleRTB.Text.ToLower();

            string materialToFind = MaterialToSearchRTB.Text.ToLower();
            string orderNumberToFind = OrderNumberToSearchRTB.Text.ToLower();
            int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

            WarehouseCustomsChamberPositionsToReturn[] WarehouseCustomsChamberPositionsToReturns;

            using (var warehouseDs = new WarehouseDS())
            {
                WarehouseCustomsChamberPositionsToReturns =
                    warehouseDs.GetCustomsChamberPositionsToReturn(currentWarehouseId);
            }

            int i = 1;
            var finalMaterialsToGiveDataSource = WarehouseCustomsChamberPositionsToReturns
                .Where(x => x.MaterialName.ToLower().Contains(materialToFind)
                    && (string.IsNullOrEmpty(x.IndexSIMPLE) || x.IndexSIMPLE.ToLower().Contains(indexSimpleToFind))
                    && x.OrderNumber.ToLower().Contains(orderNumberToFind)
                    && (!RadDatePicker_DateTo.SelectedDate.HasValue || x.TimeRequired <= RadDatePicker_DateTo.SelectedDate.Value))
                .Select(x => new
                {
                    Lp = i++,
                    ContractingPartyId = x.ContractingPartyId,
                    ContractingPartyName = x.ContractingPartyName,
                    OrderNumberFromPZ = x.OrderNumberFromPZ,
                    MaterialId = x.MaterialId,
                    IndexSIMPLE = x.IndexSIMPLE,
                    MaterialName = x.MaterialName,
                    Quantity = x.KCQuantity - Math.Abs((from v in positionsToGive where v.KCStateId == x.WarehouseDocumentPositionKCStateId select v).Sum(w => w.position.Quantity)), // - (from v in positionsToExpend select v.positionParentIds.Where(w => w.PositionParentID == p.PositionId).Sum(x => x.Quantity)).Sum(y => y),
                    UnitName = x.UnitName,
                    OrderNumber = "zwrot do dostawcy",
                    ShelfName = x.ShelfName,
                    TimeRequired = x.TimeRequired.Value.ToShortDateString(),
                    WarehouseDocumentId = x.WarehouseDocumentId,
                    WarehouseDocumentPositionId = x.WarehouseDocumentPositionId,
                    WarehouseDocumentPositionKCStateId = x.WarehouseDocumentPositionKCStateId
                });
            MaterialsToGiveRadGrid.DataSource = finalMaterialsToGiveDataSource.ToArray();
        }

        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            MaterialsToGiveRadGrid.Rebind();
        }

        protected void AddMaterialToGiveClick(object sender, EventArgs e)
        {

            if(positionsToGive != null && positionsToGive.Count > 0)
            {
                WebTools.AlertMsgAdd("Nie można wydać zwrotu więcej niż 1 pozycji");
                return;
            }

            WarehouseDocumentPosition warehouseDocumentPosition;

            if (IsValid)
            {
                GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
                var orderNumber = item.Cells[INDEX_ORDERNUMBER_IN_GRID].Text;
                var materialIdCell = item.Cells[INDEX_MATERIAL_ID_IN_GRID];
                int materialId = int.Parse(materialIdCell.Text);

                var quantity = decimal.Parse(QuantityTextBox.Text);//  quantityCell.Text);

                var docKCIdCell = item.Cells[INDEX_KCDOC_ID_IN_GRID];
                int docKCId = int.Parse(docKCIdCell.Text);

                var posKCIdCell = item.Cells[INDEX_KCPOS_ID_IN_GRID];
                int posKCId = int.Parse(posKCIdCell.Text);

                var stateKCIdCell = item.Cells[INDEX_KCSTATE_ID_IN_GRID];
                int stateKCId = int.Parse(stateKCIdCell.Text);

                var supplierKCIdCell = item.Cells[INDEX_KCSUPPLIER_ID_IN_GRID];
                int supplierKCId = int.Parse(supplierKCIdCell.Text);

                using (var warehouseDs = new WarehouseDS())
                {
                    int orderId = 0;
                    Mag.Domain.Model.Order order = orderNumber != "" ? warehouseDs.GetAllOrders().Where(p => p.OrderNumber == orderNumber).FirstOrDefault() : null;
                    if (order != null)
                        orderId = order.Id;

                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, 0,-quantity, 0, orderId);

                    // get location id for position

                    var zones = warehouseDs.GetPositionZones(posKCId);
                    var zone = zones.First();


                    //if (positionsToGive.Count(
                    //    p =>
                    //    p.position.Material.Id == materialId
                    //    && p.position.Order.OrderNumber == orderNumber
                    //    && p.position.UnitPrice == valuePerUnit
                    //    )
                    //    == 1)
                    //{
                    //    PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
                    //            positionsToGive.First(
                    //    p =>
                    //    p.position.Material.Id == materialId
                    //    && p.position.Order.OrderNumber == orderNumber
                    //    && p.position.UnitPrice == valuePerUnit
                    //            );

                    //    positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
                    //}
                    //else
                    {
                        positionsToGive.Add(new PositionWithQuantityAndShelfs()
                        {
                            position = warehouseDocumentPosition,
                            order = warehouseDocumentPosition.Order,
                            documentParentIds = new List<int>() { docKCId },
                            positionParentIds = new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { LocationId = zone.Id, PositionParentID = posKCId, Quantity = quantity } },
                            MaterialId = warehouseDocumentPosition.Material.Id,
                            UnitPrice = warehouseDocumentPosition.UnitPrice,
                            Quantity = (decimal)warehouseDocumentPosition.Quantity,
                            KCDocumentId = docKCId,
                            KCPositionId = posKCId,
                            KCStateId = stateKCId,
                            KCSupplierId = supplierKCId
                        });
                    }
                }

                Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;

                MaterialsToGiveRadGrid.SelectedIndexes.Clear();
                MaterialsWhichWillBeGivenRadGrid.Rebind();

                MaterialsToGiveRadGrid.Rebind();

                btnSaveDocument.Enabled = true;
            }
        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int userId;

                if (positionsToGive.Count > 0)
                {
                    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId)) // && int.TryParse(RadComboBox_DocSubtypeSIMPLE.SelectedValue, out docSubtypeSIMPLE))
                    {
                        WarehouseDocument warehouseDocument;

                        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            int sourceWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                            //var positionsQuery = from p in positionsToGive
                            //                     select p.position;



                            int docSubtypeSimpleId = 12;

                            //List<WarehouseDocumentPosition> positionsToAdd = new List<WarehouseDocumentPosition>(positionsQuery);
                            lock (typeof(WarehouseDS))
                            {
                                try
                                {
                                    //using(TransactionScope transactionScope = new ())

                                    int consumerId = positionsToGive.First().KCSupplierId;

                                    //supplierKCId

                                    warehouseDocument = warehouseDs.ExternalReturn(sourceWarehoseId,
                                                                                 warehouseDs.GetPersonForSystemUser(userId).Id, 
                                                                                 positionsToGive,
                                                                                 consumerId,
                                                                                 inputDate.SelectedDate ??DateTime.Now.Date,
                                                                                 CheckBox_ExportToSIMPLE.Checked, 
                                                                                 positionsToGive.First().KCStateId, 
                                                                                 docSubtypeSimpleId); 

                                    positionsToGive = new List<PositionWithQuantityAndShelfs>();
                                    Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;
                                    MaterialsToGiveRadGrid.Rebind();
                                    //MaterialsWhichWillBeGivenRadGrid.Rebind();

                                    DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument,
                                                                               WarehouseDocumentTypeEnum.ZTD,
                                                                               warehouseDocument.Id);

                                    Label_SourceZones.Text = warehouseDs.GetDocumentSourceZonesInfoFormatted(warehouseDocument.Id);

                                    WebTools.AlertMsgAdd("Dokument został zapisany. Materiały zostały wydane z <b>" + warehouseDocument.Warehouse.Name + "</b>");
                                }
                                catch (Exception ex)
                                {
                                    WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                                }
                            }
                        }

                        btnPrintDocument.Enabled = true;
                        btnSaveDocument.Enabled = false;
                        btnAddPosition.Enabled = false;

                        SearchMaterialButton.Enabled = false;
                        MaterialsToGiveRadGrid.Enabled = false;
                        //QuantityTextBoxxx.Enabled = false;
                        MaterialsWhichWillBeGivenRadGrid.Enabled = false;
                        //CompanyClientRCB.Enabled = false;
                        inputDate.Enabled = false;

                        //RadComboBox_DocSubtypeSIMPLE.Enabled = false;

                        //Session["GiveMaterialReturn_positionsToGive"] = null;
                    }
                }
                else
                {
                    WebTools.AlertMsgAdd("Brak pozycji");
                }
            }
        }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = int.Parse(QuantityTextBox.Text) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (MaterialsToGiveRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
                double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));

                double availableQuantity = double.Parse(item.Cells[INDEX_QUANTITY_IN_GRID].Text);

                if (quantity > availableQuantity)
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        //protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    try
        //    {
        //        args.IsValid = int.Parse(QuantityTextBoxxx.Text) > 0 ? true : false;
        //    }
        //    catch (Exception)
        //    {
        //        args.IsValid = false;
        //    }
        //}

        //protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (MaterialsToGiveRadGrid.SelectedItems.Count == 1)
        //    {
        //        GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
        //        double quantity = double.Parse(QuantityTextBoxxx.Text.Replace(".", ","));

        //        //double availableQuantity = double.Parse(item.Cells[INDEX_QUANTITY_IN_GRID].Text);

        //        //if (quantity > availableQuantity)
        //        //{
        //        //    args.IsValid = false;
        //        //    return;
        //        //}
        //    }

        //    args.IsValid = true;
        //}

        //protected void DocSubtypeSIMPLE_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    try
        //    {
        //        args.IsValid = int.Parse(RadComboBox_DocSubtypeSIMPLE.SelectedValue) > 0 ? true : false;
        //    }
        //    catch (Exception)
        //    {
        //        args.IsValid = false;
        //    }
        //}

        protected void MaterialsToReceiveRadGrid_DeleteRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                positionsToGive.RemoveAt(e.Item.ItemIndex);
                Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;
                MaterialsWhichWillBeGivenRadGrid.Rebind();

                if (positionsToGive.Count == 0)
                {
                    btnSaveDocument.Enabled = false;
                }
            }
            else
            {
                throw new ApplicationException("Unknown grid command");
            }
        }

        protected void MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (positionsToGive != null)
            {
                var source2 = positionsToGive.Select(x => new
                {
                    Id = 0,
                    MaterialName = x.position.Material.Name,
                    IndexSIMPLE = x.position.Material.IndexSIMPLE,
                    Quantity = -x.position.Quantity,
                    UnitName = x.position.Material.Unit.Name,

                });

                MaterialsWhichWillBeGivenRadGrid.DataSource = source2;
            }
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            Session["GiveMaterialReturn_positionsToGive"] = new List<PositionWithQuantityAndShelfs>();
            positionsToGive = (List<PositionWithQuantityAndShelfs>)Session["GiveMaterialReturn_positionsToGive"];

            MaterialsToGiveRadGrid.Rebind();

            this.MaterialsWhichWillBeGivenRadGrid.Rebind();

            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = false;
            btnAddPosition.Enabled = true;

            SearchMaterialButton.Enabled = true;
            MaterialsToGiveRadGrid.Enabled = true;
            MaterialsWhichWillBeGivenRadGrid.Enabled = true;
            inputDate.Enabled = true;

            Label_SourceZones.Text = "";
        }
    }
}