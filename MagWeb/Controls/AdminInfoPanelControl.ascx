﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminInfoPanelControl.ascx.cs"
    Inherits="MagWeb.AdminInfoPanelControl" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="Timer_InfoService" />
    </Triggers>
    <ContentTemplate>
        <table>
            <tr>
                <td>
                    <asp:Panel ID="PanelDeploymentInfo" runat="server" Visible="true">
                        <asp:Label ID="Label_DeploymentInfo" runat="server" Text="" Font-Bold="true" Font-Size="X-Large"
                            ForeColor="Red"></asp:Label>
                    </asp:Panel>
                </td>
                <td>
                    <asp:Panel ID="Panel_RefreshButton" runat="server" Visible="true">
                        <asp:Button ID="Button_Refresh" runat="server" Text="Odśwież przeglądarkę" Font-Bold="true"
                            ForeColor="Red" Visible="false" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
