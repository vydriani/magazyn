﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using Mag.Domain;
using Telerik.Web.UI;

namespace MagWeb.Controls
{
	public partial class ChooseWarehouseControl : System.Web.UI.UserControl
	{
		//protected override void OnInit(EventArgs e)
		//{
		//  using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
		//  {
		//    var allcompanies = warehouseDs.GetAllCompanies();

		//    CompanyRCB.DataSource = allcompanies.Select(w => new { Value = w.Id, Text = w.Name }).ToArray();
		//    CompanyRCB.DataBind();

		//    //if (Session[SessionData.SessionKey_CurrentCompanyId] != null)
		//    //  GoToCompany((int)Session[SessionData.SessionKey_CurrentCompanyId]);
		//    //else
		//    //  GoToCompany(allcompanies.OrderBy(c => c.Id).First().Id); // 1st ID in DB is usually used the most often
		//  }

		//  base.OnInit(e);
		//}
		protected override void OnInit(EventArgs e)
		{
			if (Session[SessionData.SessionKey_UserId] == null)
			{
				//Session is null, redirect to login page
				FormsAuthentication.SignOut();
				Response.Redirect(FormsAuthentication.LoginUrl, true);
				return;
			}
			base.OnInit(e);
		}
		protected override void OnLoad(EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				// Fill the continents combo.
				LoadCompanies();

				//load state from DB
				//load state from session
				var currentCompanyId = LoadCurrentCompanyFromSession();
				LoadWarehouses(currentCompanyId);
				LoadCurrentWarehouseFromSession();

			}

			base.OnLoad(e);
		}

		private void LoadCurrentWarehouseFromSession()
		{
			var currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
			if (currentWarehouseId.HasValue && WarehouseRCB.Items.Select(i => i.Value).Contains(currentWarehouseId.ToString()))
			{
				WarehouseRCB.SelectedValue = currentWarehouseId.ToString();
			}
			// when no warehouse is present in session we opt to leave the default choice
		}

		private int LoadCurrentCompanyFromSession()
		{
			var currentCompanyId = (int?)(Session[SessionData.SessionKey_CurrentCompanyId]);
			if (!currentCompanyId.HasValue)
			{
				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					currentCompanyId = warehouseDs.GetAllCompanies()
						.OrderBy(c => c.Id)
						.First() // 1st ID in DB is usually used the most often
						.Id;

                    Session[SessionData.SessionKey_CurrentCompanyId] = currentCompanyId;
				}
			}
			Contract.Assert(currentCompanyId.HasValue);
			CompanyRCB.SelectedValue = currentCompanyId.ToString();
			return currentCompanyId.Value;
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (WebTools.AlertMsgIsAlert())
			{
				this.Panel_Alert.Visible = true;
				this.Label_Alert.Text = WebTools.AlertMsgRead();
			}
			else
			{
				this.Panel_Alert.Visible = false;
				this.Label_Alert.Text = "";
			}

			//save state to session
			UpdateCompanyInSession();
			UpdateWarehouseInSession();
			UpdateWarehouseInDB();

			this.Button_ManageUsers.Visible = (bool?)Session[SessionData.SessionKey_LoggedUserHasAdminPrivilegesForSecurity] ?? false;
			this.Button_ManageWarehouses.Visible = (bool?)Session[SessionData.SessionKey_LoggedUserHasAdminPrivilegesForConfig] ?? false;
		}

		protected void CompanyRCB_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			WarehouseRCB.Text = "";
			var currentCompanyId = int.Parse(e.Value);
			//UpdateCompanyInSession(currentCompanyId);
			LoadWarehouses(currentCompanyId);
		}

		protected void WarehouseRCB_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			var currentWarehouseId = int.Parse(e.Value);
			//UpdateWarehouseInSession(currentWarehouseId);
			//UpdateWarehouseInDB(currentWarehouseId);
		}

		private void LoadCompanies()
		{
			using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
			{
				var allcompanies = warehouseDs.GetAllCompanies();

				CompanyRCB.DataSource = allcompanies.Select(w => new { w.Id, w.Name }).ToArray();
				CompanyRCB.DataTextField = "Name";
				CompanyRCB.DataValueField = "Id";
				CompanyRCB.DataBind();
			}
		}

		private void LoadWarehouses(int companyId)
		{
			using (var warehouseDs = new WarehouseDS())
			{
				WarehouseRCB.DataSource = warehouseDs.GetAllWarehouses(companyId)
					.Select(w => new { w.Id, w.Name })
					.ToArray();
				WarehouseRCB.DataTextField = "Name";
				WarehouseRCB.DataValueField = "Id";
				WarehouseRCB.DataBind();
			}

			WarehouseRCB.SelectedIndex = 0;
		}

		private void SetupCurrentCompanyAndWarehouse()
		{
			if (Session[SessionData.SessionKey_CurrentCompanyId] != null)
			{
				int currentCompanyId = (int)(Session[SessionData.SessionKey_CurrentCompanyId]);
				CompanyRCB.SelectedValue = currentCompanyId.ToString();
			}

			if (Session[SessionData.SessionKey_CurrentWarehouseId] != null)
			{
				int currentWarehouseId = (int)(Session[SessionData.SessionKey_CurrentWarehouseId]);
				WarehouseRCB.SelectedValue = currentWarehouseId.ToString();
			}
			else if (!string.IsNullOrWhiteSpace(WarehouseRCB.SelectedValue))
			{
				int currentWarehouseId = int.Parse(WarehouseRCB.SelectedValue);
				Session[SessionData.SessionKey_CurrentWarehouseId] = currentWarehouseId;
			}
			else
			{
				throw new NullReferenceException(SessionData.SessionKey_CurrentCompanyId + " in Session is null & WarehouseRCB.SelectedValue is empty/null");
			}
		}

		private int UpdateCompanyInSession()
		{
			return UpdateCompanyInSession(int.Parse(CompanyRCB.SelectedValue));
		}

		private int UpdateCompanyInSession(int companyId)
		{
			Session[SessionData.SessionKey_CurrentCompanyId] = companyId;
			return companyId;
		}

		private int UpdateWarehouseInSession()
		{
			return UpdateWarehouseInSession(int.Parse(WarehouseRCB.SelectedValue));
		}

		private int UpdateWarehouseInSession(int warehouseId)
		{
			Session[SessionData.SessionKey_CurrentWarehouseId] = warehouseId;
			return warehouseId;
		}

		private int UpdateWarehouseInDB()
		{
			return UpdateWarehouseInDB(int.Parse(WarehouseRCB.SelectedValue));
		}

		private int UpdateWarehouseInDB(int warehouseId)
		{
			int userId = (int)Session[SessionData.SessionKey_UserId];
			using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
			{
				warehouseDs.SetUserRecentWarehouseInfo(userId, warehouseId); // Update user records
			}
			return warehouseId;
		}
	}
}