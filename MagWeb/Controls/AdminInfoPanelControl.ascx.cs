﻿using System;
using System.Diagnostics.Contracts;
//using MagWeb.Services;
//using MagWeb.WebConfigSettingsServiceReference;


namespace MagWeb
{
    public partial class AdminInfoPanelControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateAdminInfo();
            UpdateTime();
        }


        protected void UpdateTime()
        {
#if DEBUG
      Contract.Requires(0 <= this.Session.SessionID.Length - 5);
#endif
            string actualTime = DateTime.Now.ToShortDateString() + "," + DateTime.Now.ToShortTimeString();

            byte[] actualTimeHashedBytes;

            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            string timeAndSID = actualTime + "#" + Session.SessionID.Substring(0, 5).ToUpper();

            byte[] timeBytes = System.Text.Encoding.UTF8.GetBytes(timeAndSID);

            actualTimeHashedBytes = md5.ComputeHash(timeBytes);

            System.Text.StringBuilder actualTimeHashed = new System.Text.StringBuilder();

            foreach (byte b in actualTimeHashedBytes)
            {
                actualTimeHashed.Append(b.ToString("x2").ToLower());
            }

            //this.LabelActualTime.Text = actualTime + "," + actualTimeHashed.ToString().Substring(0, 5).ToUpper();
        }

        protected void UpdateAdminInfo()
        {
            //// Call WebConfig service
            //if (Session["WebConfigSettingsServiceClientObj"] == null)
            //{
            //    Session["WebConfigSettingsServiceClientObj"] = new WebConfigSettingsServiceClient();
            //}
            //WebConfigSettingsServiceClient webConfigSettingsServiceClient = (WebConfigSettingsServiceClient)Session["WebConfigSettingsServiceClientObj"];

            //WebConfigSettingsServiceReference.SettingsReturn setting = webConfigSettingsServiceClient.GetSetting("wdrozenie");

            //string val = setting.Value;
            //if (!string.IsNullOrEmpty(val))
            //{
            //    //this.PanelDeploymentInfo.Visible = true;
            //    this.Label_DeploymentInfo.Text = val;
            //}
            //else
            //{
            //    //this.PanelDeploymentInfo.Visible = false;
            //    this.Label_DeploymentInfo.Text = "";
            //}
        }


        protected void Timer_InfoService_Tick(object sender, EventArgs e)
        {
            UpdateTime();
            UpdateTime();
        }
    }
}