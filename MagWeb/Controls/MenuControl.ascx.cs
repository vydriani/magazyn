﻿using System;
using System.Text;
using System.Web;

namespace MagWeb.Controls
{
	public partial class MenuControl : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			CreateMenu();
		}

		private void CreateMenu()
		{
			StringBuilder b = new StringBuilder();

			int i = 0;
			foreach (SiteMapNode topNode in SiteMap.RootNode.ChildNodes)
			{
				bool isCurrent = topNode == SiteMap.CurrentNode || topNode.ChildNodes.Contains(SiteMap.CurrentNode);
				CreateNode(topNode, b, "topNode", i++, isCurrent);
			}

			i = 0;
			foreach (SiteMapNode topNode in SiteMap.RootNode.ChildNodes)
			{
				bool isCurrent = topNode == SiteMap.CurrentNode || topNode.ChildNodes.Contains(SiteMap.CurrentNode);

				b.Append("<div class=\"menuItems ");

					if (isCurrent)
						b.Append("currentNode ");

					b.AppendLine("\" data-menu-id=\"" + i + "\">");

					foreach (SiteMapNode node in topNode.ChildNodes)
						CreateNode(node, b, "subMenuNode", i, SiteMap.CurrentNode == node);

				b.AppendLine("</div>");
				i++;
			}

			b.AppendLine("<div style=\"clear:both\"> </div>");
			menu.InnerHtml = b.ToString();
		}

		private void CreateNode(SiteMapNode node, StringBuilder b, string cssClassName, int menuID, bool isCurrent)
		{
			bool isAllowed = true; // Roles.GetRolesForUser().Any(role => node.Roles.Contains(role));

			b.Append("<div class=\"" + cssClassName + " ");

			if (isCurrent)
				b.Append("currentNode ");

			if (!isAllowed)
				b.Append("menuNodeNoPermission ");

			b.Append("\" data-menu-id=\""+ menuID + "\"");

			if (!string.IsNullOrWhiteSpace(node.Url))
				b.Append("data-url=\"" + node.Url + "\" ");

			b.Append(" >" + node.Title + "</div>");
			b.AppendLine();
		}
	}
}