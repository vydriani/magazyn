﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseWarehouseControl.ascx.cs" Inherits="MagWeb.Controls.ChooseWarehouseControl" %>
<asp:Panel ID="Panel_Alert" runat="server" CssClass="AlertMsg">
	<asp:Label ID="Label_Alert" runat="server" Text="Alert"></asp:Label>
</asp:Panel>
<div>
	<telerik:RadComboBox ID="CompanyRCB" runat="server" AutoPostBack="True" OnSelectedIndexChanged="CompanyRCB_SelectedIndexChanged" SkinId="mainCombobox" />
	<telerik:RadComboBox ID="WarehouseRCB" runat="server" AutoPostBack="True" OnSelectedIndexChanged="WarehouseRCB_SelectedIndexChanged" Width="250" SkinId="mainCombobox"  />
	<%--warehouse & user management should be in a separate control--%>
	<a ID="Button_ManageWarehouses" runat="server" href="~/ManageWarehouses.aspx">Konfiguracja magazynów</a>
	<a ID="Button_ManageUsers" runat="server" href="~/ManageUsers.aspx">Uprawnienia dostępu</a>
	<div>
		<asp:Label ID="LabelMessage" runat="server" Text="" />
	</div>
</div>
<%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" >
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="CompanyRCB">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="WarehouseRCB" />
			</UpdatedControls>
		</telerik:AjaxSetting>
	</AjaxSettings>
</telerik:RadAjaxManager>--%><%--DefaultLoadingPanelID="RadAjaxLoadingPanel1"--%>
<%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />--%>
