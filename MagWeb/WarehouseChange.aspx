﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="WarehouseChange.aspx.cs" Inherits="MagWeb.WarehouseChange" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <link href="Content/css/bootstrap.min.css" rel=Stylesheet type="text/css" />
    <script src="Content/js/bootstrap.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <p class="text-center" style="color: Green; font-size: 18px; font-weight: bold;">
		Przesunięcia międzymagazynowe dokumentów PZ
	</p>

   
    <div class="row">
        <div class="col-md-8">


        <div class="inputPanel">
        <div class="row">
            <div class="col-md-3">
                <label class="labelSearch">Od</label>
                <telerik:RadDatePicker ID="RadDatePicker1" runat="server" Culture="pl-PL"></telerik:RadDatePicker>
            </div>

            <div class="col-md-3">
                <label class="labelSearch">Do</label>
                <telerik:RadDatePicker ID="RadDatePicker2" runat="server" Culture="pl-PL"></telerik:RadDatePicker>
            </div>

            <div class="col-md-3">
                <label class="labelSearch">Nr dokumentu PZ</label>
			    <telerik:RadTextBox ID="tbOrderNumberPZ" runat="server"></telerik:RadTextBox>
            </div>

            <div class="col-md-3">
                <asp:Label CssClass="labelSearch" runat="server" ID="lblAuthor" AssociatedControlID="cmbAuthor">Autor:</asp:Label>
                <telerik:RadComboBox ID="cmbAuthor" runat="server">
                    <WebServiceSettings Method="GetTelericPersonList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>
                <asp:Button ID="tbSearch" CssClass="pull-right"  runat="server" OnClick="SearchMaterialClick"  Text=">" Enabled="True" />
            </div>
            
        </div>
    </div>
    
    <asp:Panel runat="server" ID="gridPanel" Visible="false" CssClass="inputPanel">
                <telerik:RadGrid ID="DocPZGrid" runat="server" OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged">
                    <MasterTableView EnableViewState="true" ClientDataKeyNames="Id">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="ProcessHeaderId" UniqueName="processHeaderId" DataField="processHeaderId" Display="false" />
                            <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                            <telerik:GridBoundColumn HeaderText="Numer dokumentu" UniqueName="Number" DataField="Number" />
                            <telerik:GridBoundColumn HeaderText="Data dokumentu" UniqueName="TimeStamp" DataField="TimeStamp" />
                            <telerik:GridBoundColumn HeaderText="Autor" UniqueName="Person" DataField="Person" />
                        </Columns>
                    </MasterTableView>

                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                   
                </telerik:RadGrid>
    
    </asp:Panel>

        </div>

    <div class="col-md-4">
            <asp:Panel runat="server" ID="panelWarehouse" Visible="false" CssClass="inputPanel">
                <Label>Wybierz nowy magazyn</Label><br />
                <telerik:RadComboBox ID="WarehouseRCB" runat="server" Width="250" SkinId="mainCombobox"  />
                <asp:Button ID="Button1" CssClass="btn"  runat="server" OnClick="ChangeWarehouse" Text="Zmień magazyn" Enabled="True" />
            </asp:Panel>
        </div>

    </div>

    <asp:HiddenField runat="server" ID="DocId" Value="0" />

</asp:Content>
