<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="OrderMaterial.aspx.cs" Inherits="MagWeb.OrderMaterial" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Zamówienie materiału
    </p>
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Magazyn źródłowy:"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadComboBox ID="WarehouseRCB" runat="server" AutoPostBack="True" OnSelectedIndexChanged="WarehouseRCB_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <fieldset>
                        <legend>Materiały na stanie magazynu do zamówienia</legend>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="Materiał"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Indeks SIMPLE"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:Label ID="Label5" runat="server" Text="Przypisanie"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="MaterialToSearchRTB" runat="server">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="IndexSIMPLEToSearchRTB" runat="server">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="OrderNumberToSearchRTB" runat="server">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <asp:Button ID="Button1" runat="server" OnClick="SearchMaterialClick" Text="Wyszukaj materiał"
                                        Enabled="True" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału do przyjęcia"
                                        ValidationGroup="AddPositionGroup" OnServerValidate="MaterialCustomValidator_ServerValidate"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <telerik:RadGrid ID="MaterialsToReserveRadGrid" runat="server" OnNeedDataSource="MaterialsToReserveRadGrid_OnNeedDataSource">
                                        <MasterTableView EnableViewState="true">
                                            <Columns>
                                                <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
                                                <telerik:GridBoundColumn HeaderText="IdMateriał" UniqueName="IdMaterial" DataField="IdMaterial" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                                <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                                                <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                                <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Zamawiana ilość:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadTextBox ID="QuantityRTB" runat="server">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
                                    ControlToValidate="QuantityRTB" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Przyjmowana ilość musi być liczbą dadatnią w formacie"
                                    ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="QuantityCustomValidator" runat="server" ErrorMessage="Nie można zamówić wiecej materiału niż jest dostępne"
                                    ValidationGroup="AddPositionGroup" OnServerValidate="Quantity_ServerValidate"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="OrderNumberChanged" runat="server" ErrorMessage="Nie można wystawić dokumentu Za dla różnych numerów zleceń"
                                    ValidationGroup="AddPositionGroup" OnServerValidate="OrderNumber_ServerValidate"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnAddPosition" runat="server" OnClick="AddMaterialToReserveClick"
                                    Text="Dodaj pozycje" Enabled="True" ValidationGroup="AddPositionGroup" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Zamawiane materiały</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                <telerik:RadGrid ID="MaterialsWhichWillBeOrderRadGrid" runat="server" OnNeedDataSource="MaterialsWhichWillBeOrderRadGrid_OnNeedDataSource"
                                                    OnItemCommand="MaterialsToReceiveRadGrid_DeleteRow">
                                                    <MasterTableView EnableViewState="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
                                                            <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                                            <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                            <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="false" />
                                                            <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                                                            <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                                            <telerik:GridButtonColumn ButtonType="ImageButton" HeaderButtonType="PushButton"
                                                                UniqueName="column" CommandName="DeleteRow" ImageUrl="~/Images/delete.png">
                                                            </telerik:GridButtonColumn>
                                                        </Columns>
                                                        <EditFormSettings>
                                                            <EditColumn UniqueName="EditCommandColumn1">
                                                            </EditColumn>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokument" Enabled="False" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick" Text="Zapisz dokument"
                                                                Enabled="True" ValidationGroup="GiveMaterialGroup" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnNewDocument" runat="server" Text="Nowy dokument" OnClick="NewDocumentClick" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
