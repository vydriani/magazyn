using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
//using Client.Domain.Logging;
using MagWeb.Extensions;
using Telerik.Web.UI;

namespace MagWeb
{
  public partial class InventoryList : FeaturedSystemWebUIPage
  {
    //PerformanceDebugger perfDebug = new PerformanceDebugger();

    private List<WarehouseInventoryPosition> warehouseInventoryPositions;

    // Session key variables
    private const string sessionInvPositions = "INVPositions";
    private const string sessionCurrentWarehouseId = "CurrentWarehouseId";
    private const string sessionCurrentCompanyId = "CurrentCompanyId";
    private const string sessionUserId = "UserId";

    // MaterialGrid
    //private const int MATERIAL_SIMPLE_ID_INDEX_IN_GRID = 2;
    //private const int COMPANY_ID_INDEX_IN_GRID = 3;
    private const string materialGrid_SimpleId_UniqueName = "SimpleId";
    private const string materialGrid_CompanyId_UniqueName = "CompanyId";

    // OrderGrid
    //private const int ORDER_ID_INDEX_IN_GRID = 2;
    private const string orderRadGrid_Id_UniqueName = "Id";

    // ShelfGrid
    //private const int SHELF_ID_INDEX_IN_GRID = 2;
    private const string shelfRadGrid_Id_UniqueName = "Id";

#if DEBUG
    public override System.Web.SessionState.HttpSessionState Session
    {
      get
      {
        SessionData.TestSessionFor<List<WarehouseInventoryPosition>, int>(sessionInvPositions, wip => wip.Count);
        return base.Session;
      }
    }
#endif


    protected void InventoryRadGrid_ItemCreated(object sender, GridItemEventArgs e)
    {
        //if (e.Item is GridDataItem)
        //{
        //    GridDataItem dataItem = (GridDataItem)e.Item;
        //    CheckBox chkBox = (CheckBox)dataItem["ExportToSIMPLE"].Controls[0];
        //    chkBox.Enabled = true;
        //    //chkBox.AutoPostBack = true;
        //    chkBox.CheckedChanged += new EventHandler(chkBox_CheckedChanged);
        //}
    }

    //void chkBox_CheckedChanged(object sender, EventArgs e)
    //{
    //    CheckBox chk = (CheckBox)sender;
    //    GridDataItem item = (GridDataItem)chk.NamingContainer;

    //    var warehouseInventoryPosition = warehouseInventoryPositions[item.ItemIndex];
    //    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
    //    {
    //        var inventoryPosition = warehouseDs.WarehouseEntitiesTest.WarehouseInventoryPositions.First(p => p.Id == warehouseInventoryPosition.Id);
    //        inventoryPosition.ExportToSIMPLE = !inventoryPosition.ExportToSIMPLE;
    //        warehouseDs.WarehouseEntitiesTest.SaveChanges();
    //    }

    //    Session[sessionInvPositions] = warehouseInventoryPositions;
    //    InventoryRadGrid.Rebind();
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
      Contract.Requires(((int?)Session[sessionCurrentWarehouseId]).HasValue, "Brak informacji o aktualnym magazynie.");
      //perfDebug.Restart();

      SessionData.SessionIdToDebug();
      if (Session[sessionInvPositions] == null)
      {
        Session[sessionInvPositions] = new List<WarehouseInventoryPosition>();
      }
      warehouseInventoryPositions = (List<WarehouseInventoryPosition>)Session[sessionInvPositions];
      InventoryRadGrid.Rebind();

      if (!Page.IsPostBack)
      {
        PanelAddMaterial.Visible = false;
        ButtonNewDoc.Enabled = false;
      }
      ButtonConfirm.Enabled = warehouseInventoryPositions.Count() > 0;

      using (var warehouseDs = new WarehouseDS())
      {
        var warehouseId = (int?)Session[sessionCurrentWarehouseId];
        var warehouse = warehouseDs.WarehouseEntitiesTest.Warehouses
          .Include("Company")
          .Single(x => x.Id == warehouseId);

        var companyName = warehouse.Company.Name;
        var warehouseName = warehouse.Name;

        CurrentWarehouseAndCompanyName.Text = warehouseName + " (" + companyName + ")";
      }
      //perfDebug.LogPerformance();
    }

    protected void InventoryRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      if (warehouseInventoryPositions != null)
      {
        var source2 = warehouseInventoryPositions
          .OrderBy(p => p.AddedTime)
          .Select
          ((p, i) => new
            {
              RowNumber = i + 1,
              IndexSIMPLE = (p.Material != null ? p.Material.IndexSIMPLE : null),
              MaterialName = (p.Material != null ? p.Material.NameShort : null),
              UnitName = p.Material != null && p.Material.Unit != null ? p.Material.Unit.ShortName : null, // this will almost always return null because Unit is not easily retrived
              ObservationTime = p.ObservationTime,
              QuantityBefore = p.QuantityBefore,
              QuantityInv = p.Quantity,
              QuantityDiff = ((p.Quantity ?? 0.0) - (p.QuantityBefore ?? 0.0)),
              UnitPrice = p.UnitPrice,
              ShelfName = (p.Zone != null ? p.Zone.Name : null),
              Assignation = (p.Order != null ? p.Order.OrderNumber : (p.Department != null ? p.Department.Name : "stan magazynowy")),
              ProcessIdpl = p.ProcessIdpl,
              ProcessIddpl = p.ProcessIddpl,
              ExportToSIMPLE = p.ExportToSIMPLE
            });

        InventoryRadGrid.DataSource = source2; // avoid appending ToList/ToArray or refresh will not work correctly //.OrderBy(p => p.IndexSIMPLE);			
      }
    }

    protected void InventoryRadGrid_OnItemClicked(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            var warehouseInventoryPosition = warehouseInventoryPositions[e.Item.ItemIndex];
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var inventoryPosition = warehouseDs.WarehouseEntitiesTest.WarehouseInventoryPositions.First(p => p.Id == warehouseInventoryPosition.Id);
                inventoryPosition.IsActive = false;
                warehouseDs.WarehouseEntitiesTest.SaveChanges();
            }

            warehouseInventoryPositions.RemoveAt(e.Item.ItemIndex);
            Session[sessionInvPositions] = warehouseInventoryPositions;
            InventoryRadGrid.Rebind();
        }
        else
        {
            throw new ApplicationException("Unknown grid command");
        }


    }

    protected void ButtonConfirm_Click(object sender, EventArgs e)
    {
        Contract.Requires(((int?)Session[sessionCurrentWarehouseId]).HasValue, "Brak informacji o aktualnym magazynie.");

        Validate();
        if (!IsValid)
        {
            return;
        }
        if (warehouseInventoryPositions != null && warehouseInventoryPositions.Count() > 0)
        {
            int userId;
            List<WarehouseDocument> listInvDoc = null;
            listInvDoc = new List<WarehouseDocument>();
            WarehouseDocument invDocument = null;
            WarehouseDocument invDocument2 = null;
            bool isExport = false;
            

            Refresh();

            if (int.TryParse(Session[sessionUserId].ToString(), out userId))
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var warehouseId = (int?)Session[sessionCurrentWarehouseId];
                    if (warehouseId.HasValue)
                    {
                        try
                        {
                            var warehouseInventoryPositionsWithExport = warehouseInventoryPositions.Where(x => x.ExportToSIMPLE).ToList();
                            var warehouseInventoryPositionsNoExport = warehouseInventoryPositions.Where(x => !x.ExportToSIMPLE).ToList();

                            if (warehouseInventoryPositionsWithExport.Count > 0)
                            {
                                isExport = true;
                                //invDocument = warehouseDs.Inventory(warehouseId.Value, warehouseDs.GetPersonForSystemUser(userId).Id, warehouseInventoryPositionsNoExport, null, false);

                                // split doc by targetDeptSIMPLEId
                                var depts = warehouseInventoryPositionsWithExport.Select(x => x.TargetDeptSIMPLEId).Distinct();

                                foreach (var dept in depts)
                                {
                                    var poz = warehouseInventoryPositionsWithExport.Where(x => x.TargetDeptSIMPLEId == dept).ToList();
                                    invDocument2 = warehouseDs.Inventory(warehouseId.Value, warehouseDs.GetPersonForSystemUser(userId).Id, poz, null, false);
                                    listInvDoc.Add(invDocument2);
                                }
                                //invDocument = warehouseDs.Inventory(warehouseId.Value, warehouseDs.GetPersonForSystemUser(userId).Id, warehouseInventoryPositionsWithExport, null, false);
                            }

                            if(warehouseInventoryPositionsNoExport.Count > 0)
                            {

                                invDocument = warehouseDs.Inventory(warehouseId.Value, warehouseDs.GetPersonForSystemUser(userId).Id, warehouseInventoryPositionsNoExport, null, false);
                            }

#if DEBUG
                            if (false) // this is here just to press F12 while rewritting this piece of crap code
                            {
                                var forReferenceOnly = warehouseDs.Inventory(warehouseId.Value, warehouseDs.GetPersonForSystemUser(userId).Id, warehouseInventoryPositions, null, false);
                            }
#endif
                        }
                        catch (Exception ex)
                        {
                            WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                            return;
                        }
                    }
                    //                    warehouseDs.Bind(positionsWithShelfsToAdd);
                }

                warehouseInventoryPositions = new List<WarehouseInventoryPosition>();
                if(invDocument!=null)
                    LabelDocStatus.Text = "Zatwierdzony<br />Nr: " + invDocument.Number + "<br />Czas utworzenia: " + invDocument.TimeStamp.ToString("s") + "<br />";

                
                if (isExport)
                {
                    foreach (var item in listInvDoc)
                    {
                        LabelDocStatus.Text += "<br />Zatwierdzony<br />Nr: " + item.Number + "<br />Czas utworzenia: " + item.TimeStamp.ToString("s") + "<br />";
                    }
                    
                }
                ButtonConfirm.Enabled = false;
                ImageButtonRefresh.Visible = false;
                ImageButtonAddMaterial.Visible = false;

                InventoryRadGrid.Enabled = false;
                ButtonNewDoc.Enabled = true;

                InventoryRadGrid.Rebind();
            }
        }
    }

    protected void ButtonNewDoc_Click(object sender, EventArgs e)
    {
      LabelDocStatus.Text = "Nowy/niezapisany";
      Session[sessionInvPositions] = null;
      Response.Redirect("InventoryList.aspx");
    }

    private void Refresh()
    {
      Contract.Requires(((int?)Session[sessionCurrentWarehouseId]).HasValue, "Brak informacji o aktualnym magazynie.");

      // First, get actual wh stacks
      using (var warehouseDs = new WarehouseDS())
      {
          var warehouseId = (int?)Session[sessionCurrentWarehouseId];
          warehouseInventoryPositions = warehouseDs.GetActiveWarehouseInventoryPositions(warehouseId);
          Session[sessionInvPositions] = warehouseInventoryPositions;

          foreach (var invPos in warehouseInventoryPositions)
          {
              int? docType = warehouseDs.GetDocumentTypeForWarehouseDocumentPositionId(invPos.WarehouseDocumentPositionId);
              if (docType == 0)
                  docType = null;

              if ((docType == null) ? false : (docType == 10 || docType == 29 || docType == 32) ? true : false)
              {
                  var matchingDocs = warehouseDs.GetOrderedStackList(invPos.WarehouseId, (int)invPos.MaterialId, invPos.OrderId, invPos.ZoneId).AsQueryable();

                  matchingDocs = matchingDocs.Where(d =>
                                  ((!d.OrderId.HasValue && !invPos.OrderId.HasValue) || d.OrderId == invPos.OrderId) &&
                                  ((!d.ProcessIdpl.HasValue && !invPos.ProcessIdpl.HasValue) || d.ProcessIdpl == invPos.ProcessIdpl) &&
                                  ((!d.ProcessIddpl.HasValue && !invPos.ProcessIddpl.HasValue) || d.ProcessIddpl == invPos.ProcessIddpl));

                  var q = matchingDocs.Sum(p => p.Quantity);

                  double newQBefore = Math.Abs(q);

                  if (newQBefore != invPos.QuantityBefore)
                  {
                      var dbInvPos = warehouseDs.WarehouseEntitiesTest.WarehouseInventoryPositions.Single(wip => wip.Id == invPos.Id);
                      dbInvPos.QuantityBefore = invPos.QuantityBefore = newQBefore;
                      dbInvPos.ObservationTime = invPos.ObservationTime = DateTime.Now;
                      warehouseDs.WarehouseEntitiesTest.SaveChanges();
                  }
              }
              else
              {
                  var matchingStacks = warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments
                    .Where(
                        p =>
                        p.WarehouseTargetId == invPos.WarehouseId
                        && p.MaterialId == invPos.MaterialId
                        && ((!p.OrderId.HasValue && !invPos.OrderId.HasValue) || p.OrderId == invPos.OrderId)
                        && ((!p.DepartmentId.HasValue && !invPos.DepartmentId.HasValue) || p.DepartmentId == invPos.DepartmentId)
                        && ((!p.ProcessIdpl.HasValue && !invPos.ProcessIdpl.HasValue) || p.ProcessIdpl == invPos.ProcessIdpl)
                        && ((!p.ProcessIddpl.HasValue && !invPos.ProcessIddpl.HasValue) || p.ProcessIddpl == invPos.ProcessIddpl)
                        && p.ShelfId == invPos.ZoneId).ToList();

                  var q = matchingStacks.Sum(p => p.Quantity);

                  double newQBefore = q.HasValue ? q.Value : 0;

                  if (newQBefore != invPos.QuantityBefore)
                  {
                      var dbInvPos = warehouseDs.WarehouseEntitiesTest.WarehouseInventoryPositions.Single(wip => wip.Id == invPos.Id);
                      dbInvPos.QuantityBefore = invPos.QuantityBefore = newQBefore;
                      dbInvPos.ObservationTime = invPos.ObservationTime = DateTime.Now;
                      warehouseDs.WarehouseEntitiesTest.SaveChanges();
                  }
              }
          }
      }

      ButtonConfirm.Enabled = warehouseInventoryPositions.Count() > 0;
      InventoryRadGrid.Rebind();
    }

    protected void ImageButtonRefresh_Click(object sender, EventArgs e)
    {
      Refresh();
    }

    protected void ImageButtonAddMaterial_Click(object sender, EventArgs e)
    {
      if (PanelAddMaterial.Visible)
      {
        HideAddMaterialPanel();
      }
      else
      {
        PanelAddMaterial.Visible = true;
      }
    }

    private void FillUnitPrice()
    {
        if (MaterialGrid.SelectedItems.Count > 0)
        {
            var materialGridItem = MaterialGrid.SelectedItems[0] as GridDataItem;
            var materialSimpleId = materialGridItem.GetCellValueAs<int>(materialGrid_SimpleId_UniqueName);// int.Parse(materialGridItem[materialGrid_SimpleId_UniqueName].Text);
            var companyId = materialGridItem.GetCellValueAs<int>(materialGrid_CompanyId_UniqueName);// int.Parse(materialGridItem[materialGrid_CompanyId_UniqueName].Text);

            if (materialSimpleId > 0 && companyId > 0)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var mpfs = warehouseDs.WarehouseEntitiesTest.MaterialPricesFromSIMPLEs
                      .FirstOrDefault
                      (p =>
                        p.IdSimple == materialSimpleId
                        && p.CompanyId == companyId
                      );

                    if (mpfs != null)
                    {
                        ValuePerUnit.Text = mpfs.CenaLast.ToString();
                    }
                }
            }
        }
    }

    private void FillProcessIdpl()
    {
      if (MaterialGrid.SelectedItems.Count > 0 && OrderRadGrid.SelectedItems.Count > 0)
      {
        var materialGridItem = MaterialGrid.SelectedItems[0] as GridDataItem;
        var materialSimpleId = int.Parse(materialGridItem["SimpleId"].Text);
        var companyId = int.Parse(materialGridItem["CompanyId"].Text);

        var orderGridItem = OrderRadGrid.SelectedItems[0] as GridDataItem;
        var orderId = int.Parse(orderGridItem["Id"].Text);

        if (materialSimpleId > 0 && orderId > 0)
        {
          using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
          {
            var pivo = warehouseDs.WarehouseEntitiesTest.Process_IDplValidOnly
              .SingleOrDefault
              (p =>
                p.IDsim == materialSimpleId
                && p.CompanyId == companyId
                && p.OrderId == orderId
              );

            ProcessIdpl.Text = (pivo != null) ? pivo.IDpl.ToString() : "brak";
          }
        }
      }
    }

    private void FillProcessIddpl()
    {
        if (MaterialGrid.SelectedItems.Count > 0 && OrderRadGrid.SelectedItems.Count > 0)
        {
            var materialGridItem = MaterialGrid.SelectedItems[0] as GridDataItem;
            var materialSimpleId = int.Parse(materialGridItem["SimpleId"].Text);
            var companyId = int.Parse(materialGridItem["CompanyId"].Text);

            var orderGridItem = OrderRadGrid.SelectedItems[0] as GridDataItem;
            var orderId = int.Parse(orderGridItem["Id"].Text);
            var orderNumber = orderGridItem["Number"].Text;

            if (materialSimpleId > 0)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var list = (from p in warehouseDs.WarehouseEntitiesTest.Process_IDdplValidOnly
                                where p.idsim == materialSimpleId &&
                                      p.firma == companyId &&
                                      p.OrderId == orderId &&
                                      p.departmentName == orderNumber
                                select new
                                {
                                    iddpl = p.iddpl
                                }).AsEnumerable();

                    ProcessIddpl.DataSource = list;
                    ProcessIddpl.DataTextField = "iddpl";
                    ProcessIddpl.DataValueField = "iddpl";
                    ProcessIddpl.DataBind();
                    ProcessIddpl.EmptyMessage = "brak";
                    ProcessIddpl.Text = "0";
                }
            }
        }
    }

    protected void ImageButtonGetUnitPrice_Click(object sender, ImageClickEventArgs e)
    {
      FillUnitPrice();
    }

    protected void SearchMaterialClick(object sender, EventArgs e)
    {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        var warehouseId = (int?)Session[sessionCurrentWarehouseId];
        int companyId;
        if (warehouseInventoryPositions.Count > 0 && warehouseId.HasValue)
        {
          var warehouse = warehouseDs.GetWarehouseById(warehouseId.Value);
          companyId = warehouse.Company.Id;
        }
        else
        {
          companyId = (int)Session[sessionCurrentCompanyId];
        }

        string materialToSearch = MaterialToSearchRTB.Text;

        //string indexSIMPLEToSearch = IndexSIMPLEToSearchRTB.Text;


        var materials = warehouseDs.GetFullSimpleIndexesSet(materialToSearch, companyId);
              

        MaterialGrid.DataSource = materials.Take(20).ToArray();
      }
      MaterialGrid.Rebind();
    }

    protected void SearchShelfClick(object sender, EventArgs e)
    {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        string shelfToSearch = ShelfToFind.Text;
        var shelfs = from p in warehouseDs.GetShelfListFromCache(shelfToSearch)
                     select p;

        ShelfRadGrid.DataSource = shelfs.Take(10).ToArray();
      }
      ShelfRadGrid.Rebind();
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (RadioButtonList1.SelectedValue)
      {
          case "1":
              {
                  Label_visible.Visible = true;//function
                  lbExportToSimple.Visible = false;
                  cbExportToSIMPLE.Checked = false;
              }
              break;
          case "2":
              {
                  Label_visible.Visible = false;
                  lbExportToSimple.Visible = true;
                  cbExportToSIMPLE.Checked = false;
              }
              break;
        default:
          break;
      }
    }

    protected void SearchOrderClick(object sender, EventArgs e)
    {
      using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
      {
        var filteredOrders = warehouseDS.GetOrdersList(this.RadTextBox_OrderNumberToSearch.Text);

        var allOrdersQuery = filteredOrders
          .OrderBy(o => o.OrderNumber)
          .Take(10);

        var allOrdersRadComboBoxItemData = allOrdersQuery
          .Select(o => new
          {
            Number = o.OrderNumber,
            Id = o.Id
          });

        OrderRadGrid.DataSource = allOrdersRadComboBoxItemData.ToArray();
      }
      OrderRadGrid.Rebind();
    }

    protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      var inventoryQuantity = -1.0;
      args.IsValid = double.TryParse(Quantity.Text, out inventoryQuantity);
      args.IsValid = args.IsValid && MaterialGrid.SelectedItems.Count == 1 && inventoryQuantity >= 0;
    }

    protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      var valuePerUnit = -1.0;
      args.IsValid = double.TryParse(ValuePerUnit.Text, out valuePerUnit);
      args.IsValid = args.IsValid && valuePerUnit > 0;
    }

    protected void DataTypeValidatorIdpl_ServerValidate(object source, ServerValidateEventArgs args)
    {
      try
      {
        args.IsValid =
          RadioButtonList1.SelectedValue == "2" // "Stan Magazynowe"
          ||
          (
            RadioButtonList1.SelectedValue == "1" // "Przypisanie"
            && OrderRadGrid.SelectedItems.Count > 0
            && !(OrderRadGrid.SelectedItems[0] as GridDataItem)["Number"].Text.ToLower().Contains("dział")
            && int.Parse(ProcessIdpl.Text) > 0 // once fixed it should be reverted to '> 0'
          )
          ||
          (
            RadioButtonList1.SelectedValue == "1" // "Przypisanie"
            && OrderRadGrid.SelectedItems.Count > 0
            && (OrderRadGrid.SelectedItems[0] as GridDataItem)["Number"].Text.ToLower().Contains("dział")
          );
      }
      catch (Exception)
      {
        args.IsValid = false;
      }
    }

    protected void DataTypeValidatorIddpl_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            args.IsValid =
              RadioButtonList1.SelectedValue == "2" // "Stan Magazynowe"
              ||
              (
                RadioButtonList1.SelectedValue == "1" // "Przypisanie"
                && OrderRadGrid.SelectedItems.Count > 0
                && (OrderRadGrid.SelectedItems[0] as GridDataItem)["Number"].Text.ToLower().Contains("dział")
                && int.Parse(ProcessIddpl.Text) > 0 // once fixed it should be reverted to '> 0'
              )
              ||
              (
                RadioButtonList1.SelectedValue == "1" // "Przypisanie"
                && OrderRadGrid.SelectedItems.Count > 0
                && !(OrderRadGrid.SelectedItems[0] as GridDataItem)["Number"].Text.ToLower().Contains("dział")
              );
        }
        catch (Exception)
        {
            args.IsValid = false;
        }
    }

    protected void ShelfValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = this.ShelfRadGrid.SelectedItems.Count == 1;
    }

    protected void MaterialValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = this.MaterialGrid.SelectedItems.Count == 1;
    }

    protected void MaterialGrid_RowCommand(object source, GridCommandEventArgs e)
    {
      if (e.CommandName == "SelectRow")
      {
        MaterialGrid.SelectedIndexes.Clear();
        e.Item.Selected = true;
        //MaterialGrid.Rebind();

        FillUnitPrice();
        FillProcessIdpl();
        FillProcessIddpl();
      }
    }

    protected void OrderGrid_RowCommand(object source, GridCommandEventArgs e)
    {
      if (e.CommandName == "SelectRow")
      {
        OrderRadGrid.SelectedIndexes.Clear();
        e.Item.Selected = true;
        //MaterialGrid.Rebind();

        FillProcessIdpl();
        FillProcessIddpl();
      }
    }

    protected void AddPositionClick(object sender, EventArgs e)
    {
        Contract.Requires(((int?)Session[sessionCurrentWarehouseId]).HasValue, "Brak informacji o aktualnym magazynie.");
        //Contract.Requires(IsValid, "Position is not validated correctly.");
        //Contract.Requires(decimal.Parse(ValuePerUnit.Text.Replace(".", ",")) != 0, "Nie można wprowadzić pozycji z zerową ceną jednostkową.");

        Validate();
        if (!IsValid)
        {
            return;
        }
        if (warehouseInventoryPositions.Any(x => x.WarehouseId != (int?)Session[sessionCurrentWarehouseId]))
            throw new Exception("Przynajmniej jedna pozycja ma przypisany inny magazyn niż magazyn bieżący");

        var warehouseId = (int?)Session[sessionCurrentWarehouseId];

        var materialGridItem = MaterialGrid.SelectedItems[0] as GridDataItem;
        var materialSimpleId = materialGridItem.GetCellValueAs<int>(materialGrid_SimpleId_UniqueName);// int.Parse(materialGridItem[materialGrid_SimpleId_UniqueName].Text);
        var companyId = materialGridItem.GetCellValueAs<int>(materialGrid_CompanyId_UniqueName);// int.Parse(materialGridItem[materialGrid_CompanyId_UniqueName].Text);

        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        {
            int userId = (int)Session[sessionUserId];
            var material = warehouseDs.GetOrAddNewNewMaterial(materialSimpleId, companyId, warehouseDs.GetPersonForSystemUser(userId).Id);//.GetOrAddNewNewMaterialDataOnly(materialSimpleId, companyId, warehouseDs.GetPersonForSystemUser(userId).Id);

            decimal unitPrice = decimal.Parse(ValuePerUnit.Text.Replace(".", ","));
            double quantity = double.Parse(Quantity.Text.Replace(".", ","));

            int shelfId = -1;
            if (ShelfRadGrid.SelectedItems.Count == 1)
            {
                var shelfItem = ShelfRadGrid.SelectedItems[0] as GridDataItem;
                shelfId = shelfItem.GetCellValueAs<int>(shelfRadGrid_Id_UniqueName);// int.Parse(shelfItem[shelfRadGrid_Id_UniqueName].Text);
            }

            //Order order = null;
            var orderId = (int?)null;
            var SimpleOrderId = (int?)null;
            var processIdpl = (int?)null;
            var processIddpl = (int?)null;
            var targetDeptSIMPLEId = (int?)null;


            if (RadioButtonList1.SelectedValue == "1" && OrderRadGrid.SelectedItems.Count == 1) // '1' means assignment to order
            {
                var orderGridItem = OrderRadGrid.SelectedItems[0] as GridDataItem;
                orderId = orderGridItem.GetCellValueAs<int>(orderRadGrid_Id_UniqueName);// int.Parse(orderGridItem[orderRadGrid_Id_UniqueName].Text);
                if (!(OrderRadGrid.SelectedItems[0] as GridDataItem)["Number"].Text.ToLower().Contains("dział"))
                {
                    processIdpl = int.Parse(ProcessIdpl.Text);
                }
            }

            if (RadioButtonList1.SelectedValue == "2" && cbExportToSIMPLE.Checked == true)
            {
                // export to simplle
                if (!String.IsNullOrEmpty(rtbOrder.Text))
                {
                    var order = warehouseDs.WarehouseEntitiesTest.Orders.FirstOrDefault(x => x.OrderNumber == rtbOrder.Text);
                    if (order != null)
                        SimpleOrderId = order.Id;
                    else
                    {
                        WebTools.AlertMsgAdd("Nieprawidłowy numer przypisania!");
                        return;
                    }
                }
                else
                {
                    //throw new Exception("Nie wybrano przypisania do dokumentu ZW!");
                    WebTools.AlertMsgAdd("Nie wybrano przypisania do dokumentu ZW!");
                    return;
                }

                if (String.IsNullOrEmpty(rcbDepartment.SelectedValue))
                {
                    WebTools.AlertMsgAdd("Nie wybrano komórki organizacyjnej SIMPLE!");
                    return;
                }
                else
                    targetDeptSIMPLEId = int.Parse(rcbDepartment.SelectedValue);
            }


            if (RadioButtonList1.SelectedValue == "1" && OrderRadGrid.SelectedItems.Count == 1)
                if ((OrderRadGrid.SelectedItems[0] as GridDataItem)["Number"].Text.ToLower().Contains("dział"))
                    processIddpl = int.Parse(ProcessIddpl.Text);

            WarehouseInventoryPosition inventoryPositionWhichExists = warehouseInventoryPositions.FirstOrDefault
            (p =>
              p.WarehouseId == warehouseId
              && p.MaterialId == material.Id
              && p.UnitPrice == unitPrice
              &&
              (
                !p.OrderId.HasValue && !orderId.HasValue
                || (p.OrderId.HasValue && orderId.HasValue && p.OrderId == orderId)
              )
              && p.ZoneId == shelfId
              && p.ProcessIdpl == processIdpl
              && p.ProcessIddpl == processIddpl
            );

            if (inventoryPositionWhichExists != null)
            {
                WebTools.AlertMsgAdd("Ta pozycja została już dodana. Nie można jej dodać dwukrotnie.");
            }
            else
            {
                var warehouseEntities = warehouseDs.WarehouseEntitiesTest;
                var newWarehouseInventoryPosition = new WarehouseInventoryPosition()
                {
                    IsActive = true,
                    WarehouseDocumentPositionId = null,
                    Zone = warehouseEntities.Zones.FirstOrDefault(p => p.Id == shelfId),
                    Order = warehouseEntities.Orders.FirstOrDefault(p => p.Id == orderId),
                    DepartmentId = null,
                    WarehouseId = warehouseId,
                    AddedTime = DateTime.Now,
                    ObservationTime = DateTime.Now,
                    QuantityBefore = 0,
                    Material = warehouseEntities.Materials.FirstOrDefault(p => p.Id == material.Id),
                    Quantity = quantity,
                    UnitPrice = unitPrice,
                    ProcessIdpl = processIdpl,
                    ProcessIddpl = processIddpl,
                    ExportToSIMPLE = cbExportToSIMPLE.Checked,
                    SIMPLEOrderId = SimpleOrderId,
                    TargetDeptSIMPLEId = targetDeptSIMPLEId
                };
                warehouseDs.WarehouseEntitiesTest.AddToWarehouseInventoryPositions(newWarehouseInventoryPosition);
                warehouseDs.WarehouseEntitiesTest.SaveChanges();
                warehouseInventoryPositions.Add(newWarehouseInventoryPosition);
            }
        }

        Session[sessionInvPositions] = warehouseInventoryPositions;
        Quantity.Text = "";
        ValuePerUnit.Text = "";
        MaterialGrid.SelectedIndexes.Clear();

        HideAddMaterialPanel();

        Refresh();
    }

    public void HideAddMaterialPanel()
    {
      // Clear 
      MaterialToSearchRTB.Text = "";
      MaterialGrid.SelectedIndexes.Clear();
      MaterialGrid.Rebind();

      RadTextBox_OrderNumberToSearch.Text = "";
      OrderRadGrid.SelectedIndexes.Clear();
      OrderRadGrid.Rebind();

      ShelfToFind.Text = "";
      ShelfRadGrid.SelectedIndexes.Clear();
      ShelfRadGrid.Rebind();

      PanelAddMaterial.Visible = false;

      ValuePerUnit.Text = "brak";
      ProcessIdpl.Text = "0";
      ProcessIddpl.Text = "0";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (WebTools.AlertMsgIsAlert())
      {
        this.Panel_Alert.Visible = true;
        this.Label_Alert.Text = WebTools.AlertMsgRead();
      }
      else
      {
        this.Panel_Alert.Visible = false;
        this.Label_Alert.Text = "";
      }

      Session[sessionInvPositions] = warehouseInventoryPositions;
    }

  }
}
