﻿using System;
using System.Linq;
using Mag.Domain;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class DefineMaterial : FeaturedSystemWebUIPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            //    {
            //        UnitRadComboBox.DataSource = from u in warehouseDs.GetAllUnits() select new {Value = u.Id, Text = u.Name};
            //        UnitRadComboBox.DataTextField = "Text";
            //        UnitRadComboBox.DataValueField = "Value";
            //        UnitRadComboBox.DataBind();
            //        UnitRadComboBox.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
            //    }
            //}
        }



        //protected void AddMaterialClick(object sender, EventArgs e)
        //{
        //    int idSIMPLE;

        //    if (SIMPLERadGrid.SelectedItems.Count == 1)
        //    {
        //        GridItem item = SIMPLERadGrid.SelectedItems[0];
        //        var cell = item.Cells[2];
        //        idSIMPLE = int.Parse(cell.Text);

        //        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //        {
        //            warehouseDs.AddMaterial(NameTextBox.Text, idSIMPLE, (int) Session[SessionData.SessionKey_UserId]);
        //        }

        //        MaterialGrid.Rebind();
        //    }
        //}

        //protected void RadComboBox1_TextChanged(object sender, EventArgs e)
        //{
        //    var a = UnitRadComboBox.Text;
        //}

        protected void SIMPLERadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            //using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            //{
            //    if(String.IsNullOrEmpty(IndexNameSearchRadTextBox.Text))
            //    {
            //        SIMPLERadGrid.DataSource = warehouseDs.GetMaterialsFromSIMPLE().Take(20).ToArray();
            //    }
            //    else
            //    {
            //        int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            //        var a = from p in warehouseDs.GetMaterialsFromSIMPLE()
            //                where p.IndexSIMPLE.ToLower().Contains(IndexNameSearchRadTextBox.Text.ToLower()) && p.CompanyId == currentComapnyId
            //                select p;
                    
            //        SIMPLERadGrid.DataSource = a.Take(20).ToArray();
            //    }
            //}
        }

        protected void MaterialGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var materials = from p in warehouseDs.GetMaterials("", "", 0)
                                orderby p.Id descending 
                                select
                                    new
                                    {
                                        Id = p.Id,
                                        MaterialName = p.MaterialName,
                                        IndexSIMPLE = p.IndexSIMPLE,
                                        NameSIMPLE = p.NameSIMPLE,
                                        UnitName = p.UnitName,
                                    };

                MaterialGrid.DataSource = materials.ToArray();
            }
        }

        //protected void MaterialFromSIMPLECustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    args.IsValid = SIMPLERadGrid.SelectedItems.Count == 1 ? true : false;
        //}

        //protected void IndexNameSearchButton_Click(object sender, EventArgs e)
        //{
        //    SIMPLERadGrid.Rebind();
        //}
    }
}
