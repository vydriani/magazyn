﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefineMaterial.aspx.cs" MasterPageFile="~/WarehouseMaster.Master" Inherits="MagWeb.DefineMaterial" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <table>
<%--        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Nazwa materiału *"></asp:Label>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadTextBox ID="NameTextBox" runat="server" Width="90%">
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nazwa jest wymagana"
                    ControlToValidate="NameTextBox" ValidationGroup="AddMaterialGroup"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="btnAddMaterial" runat="server" OnClick="AddMaterialClick" Text="Dodaj materiał"
                    ValidationGroup="AddMaterialGroup" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Jednostka *"></asp:Label>
            </td>
            <td>
            </td>
            <td>
                <asp:CustomValidator ID="MaterialFromSIMPLECustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału z SIMPLE"
                    ValidationGroup="AddMaterialGroup" OnServerValidate="MaterialFromSIMPLECustomValidator_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="SIMPLERadGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                    OnNeedDataSource="SIMPLERadGrid_OnNeedDataSource" AllowSorting="True" GridLines="None">
                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="true" EnableViewState="true">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="IdSimple" UniqueName="IdSimple" DataField="IdSimple">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="IndexSIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="NameSimple" UniqueName="NameSimple" DataField="NameSimple">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="UnitName" UniqueName="UnitName" DataField="UnitName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="UnitIdSIMPLE" UniqueName="UnitIdSIMPLE" DataField="UnitIdSIMPLE">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="CompanyName" UniqueName="CompanyName" DataField="CompanyName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="CompanyId" UniqueName="CompanyId" DataField="CompanyId">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Indeks SIMPLE"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="IndexNameSearchRadTextBox" runat="server" Width="90%">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="IndexNameSearchButton" runat="server" Text="Szukaj w SIMPLE" OnClick="IndexNameSearchButton_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
--%>        <tr>
            <td colspan="3" align="center">
                <asp:Label ID="Label3" runat="server" Text="Ostatnio dodane materiały"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <telerik:RadGrid ID="MaterialGrid" runat="server" OnNeedDataSource="MaterialGrid_OnNeedDataSource">
                    <MasterTableView AllowSorting="true" EnableViewState="true">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                            <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                            <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                            <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" />
                            <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>
