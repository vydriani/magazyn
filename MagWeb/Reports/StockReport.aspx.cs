﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.Reports
{
    public partial class StockReport : System.Web.UI.Page
    {
        private Mag.Domain.Model.Warehouse warehouse;
        private Mag.Domain.Model.Material material;
        private int materialId = 0;
        private DateTime? timeFrom;
        private DateTime? timeTo;

        public int RepT = 0;

        private void GetAllWarehouses(int companyId)
        {
            if (companyId > 0)
            {
                using (var warehouseDs = new WarehouseDS())
                {
                    WarehouseRCB.DataSource = from w in warehouseDs.GetAllWarehouses(companyId) select new { Value = w.Id, Text = w.Name };
                    WarehouseRCB.DataTextField = "Text";
                    WarehouseRCB.DataValueField = "Value";
                    WarehouseRCB.DataBind();
                    WarehouseRCB.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (Request["t"] == "1")
            {
                RepT = 1;
            }
            else if (Request["t"] == "2")
            {
                RepT = 2;
            }

            if (RepT == 0)
            {
                Response.Clear();
                Response.Write("Podano błędne parametry raportu (1)");
                Response.End();
            }

            var colZone  = StockReportGrid.MasterTableView.Columns[10];
            var colValue = StockReportGrid.MasterTableView.Columns[11];


            switch(RepT)
            {
                case 1:
                    {
                        colZone.Visible = true;
                        colValue.Visible = false;
                        Label_ReportName.Text = "Kartoteka produktu (z lokalizacją)";
                    }
                    break;
                case 2:
                    {
                        colZone.Visible = false;
                        colValue.Visible = true;
                        Label_ReportName.Text = "Kartoteka produktu (z wartością)";
                    }
                    break;
            }

            timeFrom = RadDateTimePicker_TimeFrom.SelectedDate;
            timeTo = RadDateTimePicker_TimeTo.SelectedDate;

            //bool isOk = true;

            //isOk = !(

            //               string.IsNullOrEmpty(Request["warehouseID"])
            //               ||
            //               string.IsNullOrEmpty(Request["materialId"])
            //               ||
            //               (timeFrom.HasValue && timeTo.HasValue && timeFrom > timeTo)

            //               );
            //if (isOk)
            //{
            //    int warehouseId = int.Parse(Request["warehouseID"]);
            //    int materialId = int.Parse(Request["materialId"]);

            //    // check if data ok
            //    using(WarehouseDS warehouseDs = new WarehouseDS())
            //    {
            //        var warehouseTmp = warehouseDs.GetWarehouseById(warehouseId);
            //        var materialTmp = warehouseDs.GetMaterialById(materialId);

            //        if (warehouseTmp == null || materialTmp == null)
            //        {
            //            isOk = false;
            //        }
            //        else
            //        {
            //            warehouse = warehouseTmp;
            //            material = materialTmp;
            //        }
            //    }
            //}

            //if (!isOk)
            //{
            //    Response.Clear();
            //    Response.Write("Podano błędne parametry raportu (2)");
            //    Response.End();
            //}
            //else
            //{
            //    Label_WarehouseName.Text = warehouse.Name;
            //    Label_MaterialName.Text = material.Name;
            //}
            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            if (!Page.IsPostBack)
                materialsRCB.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();

            if(!Page.IsPostBack)
                GetAllWarehouses(companyId);

            //using (var warehouseDs = new WarehouseDS())
            //{
            //    WarehouseRCB.DataSource = from w in warehouseDs.GetAllWarehouses(companyId)
            //                              select new {Value = w.Id, Text = w.Name};
            //}

            if (!Page.IsPostBack)
            {
                var materialIdStr = Request["id"];
                if (!String.IsNullOrEmpty(materialIdStr))
                {
                    material = null;
                    int.TryParse(materialIdStr, out materialId);
                }

                var materialName = Request["m"];
                if (!String.IsNullOrEmpty(materialName))
                {
                    materialsRCB.Visible = false;

                    materialLabel.Visible = true;
                    materialLabel.Text = materialName;
                }
                else
                {
                    materialLabel.Text = "";
                    materialLabel.Visible = false;

                    materialsRCB.Visible = true;
                }
                /*if (!String.IsNullOrEmpty(materialName))
                {
                    materialName = HttpUtility.UrlDecode(materialName);
                    materialsRCB.Text = materialName;
                    materialsRCB.DataSource = GetTelericMaterialsList(materialName, companyId);
                    materialsRCB.DataTextField = "Text";
                    materialsRCB.DataValueField = "Value";
                    materialsRCB.DataBind();
                    var firstItem = materialsRCB.Items.FirstOrDefault();
                    if (firstItem != null && firstItem.Text != "brak")
                    {
                        materialsRCB.SelectedValue = firstItem.Value;
                    }
                }*/

                var warehouseId = Request["w"];
                if (!String.IsNullOrEmpty(warehouseId))
                {
                    var warehouseItem = WarehouseRCB.Items.FirstOrDefault(p => p.Value == warehouseId);
                    if (warehouseItem != null)
                    {
                        WarehouseRCB.SelectedValue = warehouseItem.Value;
                    }
                }

                if (!String.IsNullOrEmpty(materialName))
                {
                    this.Button_ApplyFilterClick(null, null);
                }
            }

        }

        /*private RadComboBoxItemData[] GetTelericMaterialsList(string filtr, int companyId)
        {
            RadComboBoxItemData[] result = null;

            using (WarehouseDS warehouseDS = new WarehouseDS())
            {

                IQueryable<Mag.Domain.Model.Material> filteredMaterials = warehouseDS.GetMaterialsList(filtr, companyId);


                var allMaterialsQuery = filteredMaterials.Take(20);

                var allMaterialsObjects = allMaterialsQuery.ToArray();
                var allMaterialsRadComboBoxItemData = from material in allMaterialsObjects
                                                      orderby material.Name
                                                      select new RadComboBoxItemData
                                                      {
                                                          Text = material.Name,
                                                          Value = material.Id.ToString()
                                                      };

                result = allMaterialsRadComboBoxItemData.ToArray();
                if (result == null || result.Length == 0)
                {
                    result = new RadComboBoxItemData[2]
                                 {
                                     new RadComboBoxItemData() {Text = "brak", Value = "1"},
                                     new RadComboBoxItemData() {Text = "wyników", Value = "2"}
                                 };
                }
            }

            return result;
        }*/

        protected void StockReportGrid_OnNeededDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (material != null)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var materialsCandidatesToConsider = warehouseDs.GetWarehouseStockReports(warehouse != null ? warehouse.Id : (int?)null, material != null ? material.Id : (int?)null,
                                                                                             timeFrom, timeTo);

                    //var x = materialsCandidatesToConsider.ToArray();

                    //var materialsCandidatesToConsiderOrdered = materialsCandidatesToConsider;
                        //.Where(p => (!CheckBox_OnlyFree.Checked || (CheckBox_OnlyFree.Checked && p.OrderId.HasValue)));//.OrderByDescending(p => p.WarehouseDocumentDate);

                    //var materialsCandidatesToConsiderList = materialsCandidatesToConsider.Where(p => p.DocumentTypeId != 28 && p.DocumentTypeId != 29 && p.DocumentTypeId != 32 && p.DocumentTypeId != 10).OrderBy(p => p.PositionId).ToList(); //  bez PMZ, UMZ i ZL, ZA

                    var materialsCandidatesToConsiderList = new List<WarehouseStockReport>();

                    var documentTypeIds = new List<int> {1, 9, 6, 11, 12, 4, 3, 5, 14, 31, 35}; // PZ, BO, MM+, ZW-, ZW+, RW, WZ, MM-, LM, ZTD, INW
                    var materialsCandidatesToConsiderList1 =
                        materialsCandidatesToConsider
                            .Where(p => documentTypeIds.Contains(p.DocumentTypeId))
                            .OrderBy(p => p.PositionId).ToList();
                    foreach (var warehouseStockReport in materialsCandidatesToConsiderList1)
                    {
                        //var positionId = warehouseStockReport.PositionId;
                        //var warehousePositionZoneId = warehouseStockReport.WarehousePositionZoneId;
                        //if (!materialsCandidatesToConsiderList.Any(p => p.PositionId == positionId && p.WarehousePositionZoneId == warehousePositionZoneId))
                        var stockReport = warehouseStockReport;
                        if (!materialsCandidatesToConsiderList.Contains(stockReport))
                        {
                            materialsCandidatesToConsiderList.Add(stockReport);
                        }
                    }

                    var headerWarehouseStockReports = new List<WarehouseStockReport>();

                    int number = 1;

                    var materialsCandidatesToConsiderListGrouped = materialsCandidatesToConsiderList.GroupBy(p => p.WarehouseShortName);
                    foreach (var materialGroup in materialsCandidatesToConsiderListGrouped)
                    {
                        decimal stockQuantity = 0;
                        var firstWarehouseStockReport = materialGroup.FirstOrDefault();
                        if (firstWarehouseStockReport != null)
                        {
                            stockQuantity = firstWarehouseStockReport.ActualStockQuantity;
                            var timeStamp = firstWarehouseStockReport.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss");
                            //double income = (firstWarehouseStockReport.Income ?? 0.0);
                            //double outcome = (firstWarehouseStockReport.Outcome ?? 0.0);
                            var income = materialGroup.Where(p => p.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") == timeStamp).Sum(p => (decimal?)p.Income ?? 0);
                            var outcome = materialGroup.Where(p => p.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") == timeStamp).Sum(p => (decimal?)p.Outcome ?? 0);
                            if (income > 0)
                            {
                                stockQuantity -= income;
                            }
                            if (outcome > 0)
                            {
                                stockQuantity += outcome;
                            }
                        }

                        var headerWarehouseStockReport = new WarehouseStockReport() {IsTemporary = true, WarehouseShortName = materialGroup.Key, StockQuantity = stockQuantity};

                        foreach (var warehouseStockReport in materialGroup)
                        {
                            var income = ((decimal?)warehouseStockReport.Income ?? 0);
                            var outcome = ((decimal?)warehouseStockReport.Outcome ?? 0);
                            if (income > 0)
                            {
                                stockQuantity += income;
                            }
                            if (outcome > 0)
                            {
                                stockQuantity -= outcome;
                            }
                            warehouseStockReport.StockQuantity = stockQuantity;

                            warehouseStockReport.SourceParty = ConvertSourceParty(warehouseStockReport.SourceParty,
                                                                                  warehouseStockReport.DocumentTypeShortName);
                            warehouseStockReport.Number = number;
                            number++;
                        }

                        headerWarehouseStockReports.Add(headerWarehouseStockReport);
                    }

                    materialsCandidatesToConsiderList.InsertRange(0, headerWarehouseStockReports);

                    var lastWarehouseStockReport = materialsCandidatesToConsiderList.LastOrDefault();
                    if (lastWarehouseStockReport != null)
                    {
                        //var quantityWaitingForSupply = lastWarehouseStockReport.QuantityWaitingForSupply;
                        //var futureDeliveriesContractingParty = lastWarehouseStockReport.FutureDeliveriesContractingParty;
                        var futureDeliveries = WarehouseStockReport.GetFutureDeliveries(material.Id);
                        if (futureDeliveries != null)
                        {
                            foreach (var getMaterialFutureDeliveriesResult in futureDeliveries)
                            {
                                var quantityWaitingForSupply = getMaterialFutureDeliveriesResult.IloscZamNetto ?? 0;
                                var futureDeliveriesContractingParty = String.Format("{0};{1}", getMaterialFutureDeliveriesResult.ContractingPartyName, getMaterialFutureDeliveriesResult.NRp);
                                if (quantityWaitingForSupply > 0)
                                {
                                    var footerWarehouseStockReport = new WarehouseStockReport()
                                    {
                                        IsTemporary = false,
                                        Number = number,
                                        WarehouseShortName = "",
                                        DocumentTypeShortName = "",
                                        TimeStamp = DateTime.Now,//lastWarehouseStockReport.TimeStamp,
                                        SourceParty = futureDeliveriesContractingParty,
                                        Income = null,
                                        Outcome = null,
                                        Reservation = null,
                                        QuantityForSupply = quantityWaitingForSupply,
                                        StockQuantity = lastWarehouseStockReport.StockQuantity,
                                        ShelfName = "",
                                        UnitPrice = 0
                                    };
                                    materialsCandidatesToConsiderList.Add(footerWarehouseStockReport);

                                    number++;
                                }
                            }
                        }
                    }

                    var materialsCandidatesToConsiderArray = materialsCandidatesToConsiderList.ToArray();

                    // test
                    // var newMaterialCandidatesToConsiderArray = materialsCandidatesToConsiderArray
                    //    .GroupBy(x => new
                    //    {
                    //        x.ShelfName,
                    //        x.Quantity,
                    //        x.ActualStockQuantity
                    //    })
                    //    .Select(x => new
                    //    {
                    //        Quantity = x.Key.Quantity,
                    //        ActualStockQuantity = x.Key.ActualStockQuantity,
                    //        ShelfName = x.Key.ShelfName
                    //    }
                    //    ).ToArray();
                    //var newMaterialCandidatesToConsiderArray2 = materialsCandidatesToConsiderArray
                    //    .GroupBy(x => new
                    //    {
                    //        x.ShelfName,
                    //    })
                    //    .Select(x => new 
                    //    {
                    //        Quantity = x.Sum(y => y.Quantity),
                    //        ShelfName = x.Key.ShelfName
                    //    }).ToArray();

                    StockReportGrid.DataSource = materialsCandidatesToConsiderArray; // materialsCandidatesToConsiderOrdered.ToArray();

                    //var x = 1;
                }
            }
        }

        private class StockMaterial
        {
            public double? Quantity { get; set; }
            public decimal ActualStockQuantity { get; set; }
            public string ShelfName { get; set; }
        }


        private string ConvertSourceParty(string sourceParty, string documentTypeShortName)
        {
            if (!String.IsNullOrEmpty(documentTypeShortName) &&
                !String.IsNullOrEmpty(sourceParty) &&
                sourceParty.StartsWith(documentTypeShortName))
            {
                return sourceParty
                    .Substring(documentTypeShortName.Length,
                               (sourceParty.Length - documentTypeShortName.Length))
                    .Trim(';', '/', ' ');
            }
            return sourceParty;
        }

        protected void StockReportGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            /*if (e.Item is GridDataItem) // && e.Item.OwnerTableView.DataSourceID == "AccessDataSource1")
            {
                var warehouseStockReport = e.Item.DataItem as WarehouseStockReport;

                Label lbl = e.Item.FindControl("numberLabel") as Label;
                if (lbl != null)
                {
                    lbl.Text = (warehouseStockReport != null && !warehouseStockReport.IsTemporary) ? e.Item.ItemIndex.ToString() : "";
                }

                //RepT

                //GridBoundColumn columnZone = e.Item.Cells[]

            }*/
        }

        protected void Button_ApplyFilterClick(object sender, EventArgs e)
        {
            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            //if(WarehouseRCB.se)
            int? warehouseId = int.Parse(WarehouseRCB.SelectedValue);

            int materialId = 0;
            if (materialsRCB.Visible)
            {
                int.TryParse(materialsRCB.SelectedValue, out materialId);
            }
            else
            {
                materialId = this.materialId;
            }

            //int? materialId = int.Parse(materialsRCB.SelectedValue);





            if (warehouseId.HasValue)
            {
                using(WarehouseDS warehouseDs = new WarehouseDS())
                {
                    warehouse = warehouseDs.GetWarehouseById(warehouseId.Value);
                }
            }

            if (materialId > 0)
            {
                using (var warehouseDs = new WarehouseDS())
                {
                    material = warehouseDs.GetMaterialById(materialId); //warehouseDs.GetMaterialByNameWithIndex(materialsRCB.Text, companyId).FirstOrDefault();
                }
                var whCol = StockReportGrid.MasterTableView.Columns[1];
                whCol.Visible = warehouse == null;

                StockReportGrid.Rebind();
            }


        }

        protected void ButtonReloadClick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}