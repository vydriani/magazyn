﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockReport.aspx.cs" Inherits="MagWeb.Reports.StockReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kartoteka produktu</title>
    
    <style type="text/css">
        .module1
        {
            background-color: #dff3ff;
            border: 1px solid #c6e1f2;
        }
    </style>
</head>
<body bgcolor="#dced99">
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" Skin="Metro" />
    <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--            <telerik:AjaxSetting AjaxControlID="CheckBox1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DropDownList1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:Panel ID="Panel1" runat="server" Height="275px" Style="padding-top: 15px;
        padding-left: 15px">
        <h1>
            <asp:Label ID="Label_ReportName" runat="server" Text="Kartoteka produktu" ForeColor="Navy"></asp:Label>
        </h1>
        <table border="0">
            <tr>
                <td>
                    Materiał/Indeks:
                </td>
                <td>
                    <telerik:RadComboBox Skin="Metro" ID="materialsRCB" runat="server" ShowToggleImage="false" EmptyMessage="Wprowadź nazwę lub indeks"
                        EnableLoadOnDemand="True" EnableVirtualScrolling="True" EnableItemCaching="true"
                        LoadingMessage="Pobieranie danych" MarkFirstMatch="true" AutoPostBack="False"
                        Width="300px">
                        <WebServiceSettings Method="GetTelericMaterialsList" Path="../WarehouseWebWcf.svc" />
                    </telerik:RadComboBox>
                    <asp:Label ID="materialLabel" runat="server" Text="" Visible="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0">
                        <tr>
                            <td>
                                Czas od:
                            </td>
                            <td>
                                <telerik:RadDateTimePicker ID="RadDateTimePicker_TimeFrom" runat="server">
                                </telerik:RadDateTimePicker>
                            </td>
                            <td>
                                do:
                            </td>
                            <td>
                                <telerik:RadDateTimePicker ID="RadDateTimePicker_TimeTo" runat="server">
                                </telerik:RadDateTimePicker>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Magazyn:
                </td>
                <td>
                    <telerik:RadComboBox Skin="Metro" ID="WarehouseRCB" runat="server" AutoPostBack="False" Width="300px">
                        <%--  OnSelectedIndexChanged="WarehouseRCB_SelectedIndexChanged">--%>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Button_ApplyFilter" runat="server" Text="Filtruj" OnClick="Button_ApplyFilterClick" />
                </td>
            </tr>
        </table>
        <p>
            <%--OnDetailTableDataBind="StockReportGrid_DetailTableDataBind"--%>
            <telerik:RadGrid ID="StockReportGrid" runat="server" OnNeedDataSource="StockReportGrid_OnNeededDataSource"
                OnItemDataBound="StockReportGrid_ItemDataBound" Skin="Metro" AutoGenerateColumns="false"
                AllowSorting="true" EnableViewState="false" ShowGroupPanel="true" AllowPaging="false"
                AllowMultiRowSelection="False" SelectionMode="Single">
                <ClientSettings AllowDragToGroup="true" AllowGroupExpandCollapse="true" AllowColumnsReorder="true"
                    ReorderColumnsOnClient="true" ColumnsReorderMethod="Reorder" Selecting-AllowRowSelect="False"
                    EnablePostBackOnRowClick="False">
                    <Selecting AllowRowSelect="False" />
                    <%--<ClientEvents OnRowSelected="RowSelected" />--%>
                </ClientSettings>
                <GroupPanel Text="">
                </GroupPanel>
                <GroupingSettings GroupContinuesFormatString=" Pozostałe elementy na następnej stronie."
                    GroupContinuedFormatString="Pozostałe elementy z poprzedniej strony. " GroupSplitDisplayFormat="{0} z {1} elementów." />
                <%--<PagerStyle Mode="NumericPages" FirstPageToolTip="Pierwsza strona" LastPageToolTip="Ostatnia strona"
                    PrevPageToolTip="Poprzednia strona" NextPageToolTip="Następna strona" PageSizeLabelText="Rozmiar strony:"
                    PagerTextFormat="{4} Strona {0} z {1}, elementy od {2} do {3} z {5}" AlwaysVisible="true" />--%>
                <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true"
                    AllowSorting="true" AllowMultiColumnSorting="true" EnableViewState="true" ShowGroupFooter="true"
                    ShowFooter="true" GroupLoadMode="Client" NoMasterRecordsText="">
                    <Columns>
                        <%--<telerik:GridTemplateColumn UniqueName="TemplateColumn" HeaderText="L.p.">
                            <ItemTemplate>
                                <asp:Label ID="numberLabel" runat="server" Width="30px" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                        </telerik:GridTemplateColumn>--%>
                        <telerik:GridBoundColumn HeaderText="L.p." UniqueName="NumberColumn" DataField="Number"
                            Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Magazyn" UniqueName="WarehouseShortName" DataField="WarehouseShortName"
                            Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Typ operacji" UniqueName="DocumentTypeShortName"
                            DataField="DocumentTypeShortName" Visible="True">
                        </telerik:GridBoundColumn>
                        <%--                    
                    <telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="ContextUnit" DataField="ContextUnit" Visible="True">
                    </telerik:GridBoundColumn>
                    

                        --%>
                        <%--                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity"
                        FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" Groupable="false">
                    </telerik:GridBoundColumn>--%>
                        <%--
                    <telerik:GridBoundColumn HeaderText="Przyszłe dostawy" UniqueName="QuantityWaitingForSupply" DataField="QuantityWaitingForSupply" Visible="True">
                    </telerik:GridBoundColumn>
                        --%>
                        <telerik:GridBoundColumn HeaderText="Data" UniqueName="TimeStamp" DataField="TimeStamp2"
                            Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Dostawca/przypisanie" UniqueName="SourceParty"
                            DataField="SourceParty" Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Przychód" UniqueName="Income" DataField="Income"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" Groupable="false"
                            Visible="True" Aggregate="Sum" FooterText="Suma: ">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Rozchód" UniqueName="Outcome" DataField="Outcome"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" Groupable="false"
                            Visible="True" Aggregate="Sum" FooterText="Suma: ">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Rezerwacja" UniqueName="Reservation" DataField="Reservation"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" Groupable="false"
                            Visible="True" Aggregate="Sum" FooterText="Suma: ">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Przyszłe dostawy" UniqueName="QuantityWaitingForSupply"
                            DataField="QuantityForSupply" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                            Groupable="false" Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Stan faktyczny" UniqueName="ActualStockQuantity"
                            DataField="StockQuantity" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                            Groupable="false" Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName"
                            Visible="True">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Wartość" UniqueName="UnitPrice" DataField="UnitPrice2"
                            Visible="True">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:Button ID="Button_Reload" runat="server" Text="Wyczyść widok" OnClick="ButtonReloadClick" />
        </p>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"  IsSticky="true"> <%--Width="100%" Height="100%"--%>
    </telerik:RadAjaxLoadingPanel>
    </form>
</body>
</html>
