using System;
using System.Diagnostics.Contracts;
using System.Web.Security;
using System.Web.UI;
using Mag.Domain;
//using Client.Domain.Logging;

namespace MagWeb
{
	public partial class WarehouseMaster : MasterPage
	{
    //private PerformanceDebugger perfDebug = new PerformanceDebugger();

        protected void Page_Init(object sender, EventArgs e)
        {
            ExposeCurrentCultureToClient();

            // app version (choose latest create-time from all loaded assemblies)
            var applicationVersion = WebToolsExtensions.GetApplicationVersion();
            AppVersionLabel.Text = "Wersja z " + applicationVersion;

            //detect database
            var isProduction = WebToolsExtensions.IsProduction();

            if (!isProduction)
                pageBody.Attributes.Add("class", "testServer");

            var dbInfo = WebToolsExtensions.DetectDatabase();
            InformationLabel.Text = dbInfo;
        }

    /// <summary>
    /// Exposes page's culture to JS
    /// </summary>
        private void ExposeCurrentCultureToClient()
        {
            Contract.Requires(
              System.Threading.Thread.CurrentThread.CurrentCulture.Name == System.Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag,
              "CurrentCulture.Name and CurrentCulture.IetfLanguageTag do NOT match but MS says they do.");

            CultureHF.Value = System.Threading.Thread.CurrentThread.CurrentCulture.Name; //Page.Culture returns garbage from DisplayName property like "Polski (Polska)", go figure.
        }

		protected void Page_PreRender(object sender, EventArgs e)
		{
      //perfDebug.Restart();
			if (WebTools.AlertMsgIsAlert())
			{
				this.Panel_Alert.Visible = true;
				this.Label_Alert.Text = WebTools.AlertMsgRead();
			}
			else
			{
				this.Panel_Alert.Visible = false;
				this.Label_Alert.Text = "";
			}

      //perfDebug.LogPerformance();


			SetUserNameLabel();
		}

        private void SetUserNameLabel()
        {
            if (Session[SessionData.SessionKey_UserId] != null)
            {
                if (Session[SessionData.SessionKey_UserFirstNameLastName] == null)
                {
                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        var person = warehouseDs.GetPersonForSystemUser((int)Session[SessionData.SessionKey_UserId]);
                        if (person != null)
                            Session[SessionData.SessionKey_UserFirstNameLastName] = person.FirstName + " " + person.LastName;
                        else
                            Session[SessionData.SessionKey_UserFirstNameLastName] = "";
                    }
                }
            }

            Label_FirstAndLastName.Text = Session[SessionData.SessionKey_UserFirstNameLastName].ToString();
        }

		protected void logoutButton_Click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Session.Abandon();
			FormsAuthentication.RedirectToLoginPage();
		}
	}
}
