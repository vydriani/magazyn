<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="QuantityChanges.aspx.cs" Inherits="MagWeb.QuantityChanges" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <asp:Panel ID="MainPanel" runat="server">
        <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
            <asp:Label ID="Label_Title" runat="server" Text="Zmiany ilości materiałów wydawanych na produkcję"></asp:Label>
        </p>
        <input type="search" class="light-table-filter" data-table="order-table" placeholder="filtruj" />
        <asp:CheckBox ID="ExcessQuantityOnlyCheckBox" runat="server" Checked="true" 
          Text="Pokaż tylko wydania ze zwiększoną ilością" AutoPostBack="True" 
          oncheckedchanged="ExcessQuantityOnlyCheckBox_CheckedChanged" ForeColor="White" />


        <telerik:RadGrid ID="QuantityChangesRadGrid" runat="server" OnNeedDataSource="QuantityChangesRadGrid_OnNeedDataSource">
            <MasterTableView CssClass="order-table table" EnableViewState="true">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" Display="False" />
                    <telerik:GridBoundColumn HeaderText="Id pozycji" UniqueName="WarehouseDocumentPositionId" DataField="WarehouseDocumentPositionId" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Czas utworzenia" UniqueName="TimeRequested" DataField="TimeRequested" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Operator" UniqueName="OperatorName" DataField="OperatorName" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="MaterialIndex" DataField="MaterialIndex" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Zmiana ilości" UniqueName="QuantityDiff" DataField="QuantityDiff" Display="True" />
                    <telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="UnitName" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" Display="True" />
                    <telerik:GridBoundColumn HeaderText="IDPL" UniqueName="ProcessIdpl" DataField="ProcessIdpl" Display="False" />
                    <telerik:GridBoundColumn HeaderText="Magazyn źródłowy" UniqueName="WarehouseSourceName" DataField="WarehouseSourceName" Display="True" />
                    <telerik:GridBoundColumn HeaderText="Magazyn docelowy" UniqueName="WarehouseTargetName" DataField="WarehouseTargetName" Display="True" />
                    <telerik:GridTemplateColumn UniqueName="Potwierdzenie" HeaderText="Potwierdzenie">
                        <ItemTemplate>
                            <%# Eval("ConfirmationText") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="Anulowanie" HeaderText="Anulowanie">
                        <ItemTemplate>
                            <%# Eval("CancellationText") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </asp:Panel>
</asp:Content>
