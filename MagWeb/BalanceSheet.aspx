<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="BalanceSheet.aspx.cs" Inherits="MagWeb.BalanceSheet" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <asp:Panel ID="PanelBack" runat="server">
        <div align="center">
            <b><a href="javascript: history.go(-1)">&#60;&#60;&#60;POWRÓT</a></b>
        </div>
    </asp:Panel>
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Wpis inwentaryzacyjny (bilans otwarcia)
    </p>
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Wybierz lokalizację</legend>
                                                <asp:Label ID="Label3" runat="server" Text="Lokalizacja"></asp:Label>
                                                <telerik:RadTextBox ID="ShelfToFind" runat="server" Width="90%" Enabled="True">
                                                </telerik:RadTextBox>
                                                <asp:Button ID="SearcjShelfButton" runat="server" OnClick="SearchShelfClick" Text="Wyszukaj lokalizację"
                                                    Enabled="True" />
                                                <telerik:RadGrid ID="ShelfRadGrid" runat="server" OnNeedDataSource="ShelfRadGrid_OnNeedDataSource">
                                                    <MasterTableView EnableViewState="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
                                                            <telerik:GridBoundColumn HeaderText="Name" UniqueName="Name" DataField="Name" />
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <Selecting AllowRowSelect="True" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                                <asp:CustomValidator ID="CustomValidator_Shelf" runat="server" ErrorMessage="Wybierz lokalizację"
                                                    ValidationGroup="AddPositionGroup" OnServerValidate="ShelfValidator_ServerValidate"></asp:CustomValidator>
                                            </fieldset>
                                        </td>
                                        <td>
                                            <fieldset>
                                                <legend>Wybierz przypisanie</legend>
                                                <asp:Label ID="Label_Order" runat="server" Text="Przypisanie"></asp:Label>
                                                <telerik:RadTextBox ID="RadTextBox_OrderNumberToSearch" runat="server" Width="90%" Enabled="True">
                                                </telerik:RadTextBox>
                                                <asp:Button ID="Button_SearchOrder" runat="server" OnClick="SearchOrderClick" Text="Wyszukaj przypisanie"
                                                    Enabled="True" />
                                                <telerik:RadGrid ID="OrderRadGrid" runat="server">
                                                    <MasterTableView EnableViewState="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
                                                            <telerik:GridBoundColumn HeaderText="Number" UniqueName="Number" DataField="Number" />
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <Selecting AllowRowSelect="True" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </fieldset>
                                        </td>
                                        <td>
                                            <fieldset>
                                                <legend>Dodawanie pozycji</legend>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label6" runat="server" Text="Ilość na stanie:"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Panel ID="PanelQuantityOnStacks" runat="server">
                                                                            <asp:Label ID="QuantityOnStacks" runat="server" Text="0.0" Font-Bold="true"></asp:Label>
                                                                            &nbsp;
                                                                            <asp:Label ID="QuantityOnStacksUnit" runat="server" Text="j.m." Font-Bold="true"></asp:Label>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label1" runat="server" Text="Stwierdzona ilość:"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadTextBox ID="Quantity" runat="server" SelectionOnFocus="SelectAll" Font-Bold="true">
                                                                        </telerik:RadTextBox>
                                                                        <br />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilośc jest wymagana"
                                                                            ControlToValidate="Quantity" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
                                                                        <br />
                                                                        <asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału"
                                                                            ValidationGroup="AddPositionGroup" OnServerValidate="MaterialCustomValidator_ServerValidate"></asp:CustomValidator>
                                                                        <asp:CustomValidator ID="QuantityChangeCustomValidator" runat="server" ErrorMessage="Stwierdzona ilość nie różni się od ilości na stanie"
                                                                            ValidationGroup="AddPositionGroup" OnServerValidate="QuantityChangeCustomValidator_ServerValidate_ServerValidate"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label2" runat="server" Text="Cena jednostkowa:"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadTextBox ID="ValuePerUnit" runat="server" Text="0.01">
                                                                        </telerik:RadTextBox>
                                                                        <br />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Cena jednostkowa jest wymagana"
                                                                            ControlToValidate="ValuePerUnit" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
                                                                        <br />
                                                                        <asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Wprowadzona ilość musi być dodatnia"
                                                                            ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Button ID="btnAddPosition" runat="server" OnClick="AddPositionClick" Text="Dodaj pozycje"
                                                    ValidationGroup="AddPositionGroup" />
                                            </fieldset>
                                            <fieldset>
                                                <legend>Pozycje wpisu inwentaryzacyjnego</legend>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="MaterialsToReceiveRadGrid" runat="server" 
																				OnNeedDataSource="MaterialsToReceiveRadGrid_OnNeedDataSource"
																				OnItemCommand="MaterialsToReceiveRadGrid_DeleteRow">
                                                                <MasterTableView EnableViewState="true">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
                                                                        <telerik:GridBoundColumn HeaderText="Indeks " UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                                        <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                                                        <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="false" />
                                                                        <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                                                                        <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                                                        <telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice" />
                                                                        <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                                                        <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                                                                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderButtonType="PushButton"
                                                                            UniqueName="column" Text="Usuń" CommandName="DeleteRow" ImageUrl="~/Images/delete.png">
                                                                        </telerik:GridButtonColumn>
                                                                    </Columns>
                                                                    <EditFormSettings>
                                                                        <EditColumn UniqueName="EditCommandColumn1" />
                                                                    </EditFormSettings>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CustomValidator ID="CustomValidator_DocumentPositions" runat="server" ErrorMessage="Dodaj pozycje dokumentu"
                                                                ValidationGroup="ReceiveMaterialGroup" OnServerValidate="PositionsValidator_ServerValidate"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:CheckBox ID="CheckBox_AddOnly" runat="server" Checked="false" Text="Tylko dodanie"
                                                                Enabled="false" />
                                                            <br />
                                                            <asp:ImageButton ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick"
                                                                Text="Wydaj" ForeColor="Yellow" Enabled="True" ValidationGroup="ReceiveMaterialGroup"
                                                                ImageUrl="/Images/Icons/48x48/shadow/check.png" ToolTip="Zapisz dokument" AlternateText="Zapisz dokument" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1">
                                <fieldset>
                                    <legend>Wybierz materiał</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Nazwa materiału"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Indeks SIMPLE"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <telerik:RadTextBox ID="MaterialToSearchRTB" runat="server" Width="90%">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="IndexSIMPLEToSearchRTB" runat="server" Width="90%">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="Button1" runat="server" OnClick="SearchMaterialClick" Text="Wyszukaj materiał"
                                                    Enabled="True" />
                                            </td>
                                        </tr>
                                    </table>
                                    <telerik:RadGrid ID="MaterialGrid" runat="server" OnNeedDataSource="MaterialGrid_OnNeedDataSource">
                                        <MasterTableView EnableViewState="true">
                                            <Columns>
                                                <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                                                <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                                <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" />
                                                <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                    <asp:CustomValidator ID="CustomValidator_Material" runat="server" ErrorMessage="Wybierz materiał"
                                        ValidationGroup="AddPositionGroup" OnServerValidate="MaterialValidator_ServerValidate"></asp:CustomValidator>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
