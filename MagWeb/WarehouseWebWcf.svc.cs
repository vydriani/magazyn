﻿namespace MagWeb
{
  using System.Diagnostics.Contracts;
  using System.Linq;
  using System.ServiceModel;
  using System.ServiceModel.Activation;
  using System.Text.RegularExpressions;
  using Mag.Domain;
  using Mag.Domain.Model;
  using Telerik.Web.UI;

    [ServiceContract(Namespace = "MagWeb")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WarehouseWebWcf
    {
        private RadComboBoxData _GetTelericCompaniesList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Company> filteredCompanies = warehouseDS.GetCompaniesList(filtr);


                var allCompaniesQuery = filteredCompanies.Take(20);

                var allCompaniesObjects = allCompaniesQuery.ToArray();
                var allCompaniesRadComboBoxItemData = from company in allCompaniesObjects
                                                      orderby company.Name
                                                      select new RadComboBoxItemData
                                                      {
                                                          Text = company.Name,
                                                          Value = company.Id.ToString()
                                                      };

                result.Items = allCompaniesRadComboBoxItemData.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericDepartmentsList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Department> filteredDepts = warehouseDS.GetDepartmentsList(filtr);


                var allDeptsQuery = filteredDepts.Take(20);

                var allDeptsObjects = allDeptsQuery.ToArray();
                var allDeptsRadComboBoxItemData = from company in allDeptsObjects
                                                  orderby company.Name
                                                  select new RadComboBoxItemData
                                                  {
                                                      Text = company.Name,
                                                      Value = company.Id.ToString()
                                                  };

                result.Items = allDeptsRadComboBoxItemData.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        // Supporting methods:
        private RadComboBoxData _GetTelericShelfsList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                var shelfsFromCache = warehouseDS.GetShelfListFromCache(filtr);
                var allShelfsFromCacheQuery = shelfsFromCache.Take(20);

                var allShelfsObjectsFromCache = allShelfsFromCacheQuery.ToArray();
                var allShelfsRadComboBoxItemDataFromCache = from shelf in allShelfsObjectsFromCache
                                                            orderby shelf.FullPath
                                                            select new RadComboBoxItemData
                                                            {
                                                                Text = shelf.FullPath,
                                                                Value = shelf.Id.ToString()
                                                            };


                result.Items = allShelfsRadComboBoxItemDataFromCache.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }
            return result;
        }

        private RadComboBoxData _GetTelericShelfsListWithPrompt(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;
            bool isChecked = !Regex.IsMatch(filtr.Replace(',', '1'), "\\D");
            var ids = filtr.Split(',');

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {
                if (filtr == "allShelfs")
                    filtr = "";

                IQueryable<ZoneParentPath> shelfsFromCache;
                if (isChecked)
                    shelfsFromCache = warehouseDS.GetShelfListFromCacheWithPrompt(ids);
                else
                    shelfsFromCache = warehouseDS.GetShelfListFromCache(filtr);

                var allShelfsFromCacheQuery = shelfsFromCache.Take(isChecked ? 60 : 20);

                var allShelfsObjectsFromCache = allShelfsFromCacheQuery.ToArray();
                var allShelfsRadComboBoxItemDataFromCache = from shelf in allShelfsObjectsFromCache
                                                            orderby shelf.FullPath
                                                            select new RadComboBoxItemData
                                                            {
                                                                Text = shelf.FullPath,
                                                                Value = shelf.Id.ToString()
                                                            };


                result.Items = allShelfsRadComboBoxItemDataFromCache.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }
            return result;
        }

        private RadComboBoxData _GetTelericMaterialsList(RadComboBoxContext context, int? companyId = null)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Material> filteredMaterials = warehouseDS.GetMaterialsList(filtr, companyId);


                var allMaterialsQuery = filteredMaterials.Take(20);

                var allMaterialsObjects = allMaterialsQuery.ToArray();
                var allMaterialsRadComboBoxItemData = from material in allMaterialsObjects
                                                      orderby material.Name
                                                      select new RadComboBoxItemData
                                                      {
                                                          Text = material.Name, // + (!companyId.HasValue ? " @@@" + material.CompanyId : ""),
                                                          Value = material.Id.ToString()
                                                      };

                result.Items = allMaterialsRadComboBoxItemData.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericMaterialsSIMPLEList(RadComboBoxContext context, int companyId)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<MaterialFromSIMPLEExtractedData> filteredMaterials = warehouseDS.GetMaterialsSIMPLEList(filtr, companyId);

                //var x1 = filteredMaterials.ToArray();

                var allMaterialsQuery = filteredMaterials.Take(20);

                //var x2 = allMaterialsQuery.ToArray();

                var allMaterialsObjects = allMaterialsQuery.ToArray();
                var allMaterialsRadComboBoxItemData = from material in allMaterialsObjects
                                                      orderby material.NameSimple
                                                      select new RadComboBoxItemData
                                                      {
                                                          Text = material.NameSimple + " (" + material.IndexSimple.ToString() + ")",
                                                          Value = material.IdSimple.ToString()
                                                      };



                result.Items = allMaterialsRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }


                //var x = 1;
            }
            //if (result.Items.Count() < 1)
            //{
            //    RadComboBoxItemData[] rcbid = new RadComboBoxItemData[1];


            //    result.Items[0] = new RadComboBoxItemData
            //    {
            //        Text = "",
            //        Value = ""
            //    };
            //}
            return result;
        }

        private RadComboBoxData _GetTelericIndexesSIMPLEList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;


            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {
                IQueryable<Mag.Domain.Model.Material> filteredIndexesSimple = warehouseDS.GetIndexesSIMPLEList(filtr);


                var allIndexesQuery = filteredIndexesSimple.Take(20);

                var allIndexesObjects = allIndexesQuery.ToArray();
                var allIndexesRadComboBoxItemData = from material in allIndexesObjects
                                                    orderby material.IndexSIMPLE
                                                    select new RadComboBoxItemData
                                                    {
                                                        Text = material.IndexSIMPLE,
                                                        Value = material.Id.ToString()
                                                    };

                result.Items = allIndexesRadComboBoxItemData.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericOrdersList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Order> filteredOrders = warehouseDS.GetOrdersList(filtr);


                var allOrdersQuery = filteredOrders.Take(20);

                var allOrdersObjects = allOrdersQuery.ToArray();
                var allOrdersRadComboBoxItemData = from order in allOrdersObjects
                                                   orderby order.OrderNumber
                                                   select new RadComboBoxItemData
                                                   {
                                                       Text = order.OrderNumber,
                                                       Value = order.Id.ToString()
                                                   };

                result.Items = allOrdersRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericOrderNumbersSIMPLEList(RadComboBoxContext context, int companyId)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.OrderFromSIMPLE> filteredOrders = warehouseDS.GetOrdersFromSIMPLEList(filtr, companyId);


                var allOrdersQuery = filteredOrders.Take(20);

                var allOrdersObjects = allOrdersQuery.ToArray();
                var allOrdersRadComboBoxItemData = from order in allOrdersObjects
                                                   orderby order.OrderNumberShort
                                                   select new RadComboBoxItemData
                                                   {
                                                       Text = order.OrderNumberShort,
                                                       Value = order.OrderId.ToString()
                                                   };

                result.Items = allOrdersRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericDocumentTypesList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.WarehouseDocumentType> filteredDocumentTypes = warehouseDS.GetDocumentTypeList(filtr);

                var allTypesQuery = filteredDocumentTypes.Take(20);

                var allTypesObjects = allTypesQuery.ToArray();
                var allTypesRadComboBoxItemData = from type in allTypesObjects
                                                  orderby type.ShortType
                                                  select new RadComboBoxItemData
                                                  {
                                                      Text = type.ShortType,
                                                      Value = type.Id.ToString()
                                                  };

                result.Items = allTypesRadComboBoxItemData.ToArray();
                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }

            }

            return result;
        }

        public RadComboBoxData _GetTelericWarehouseList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;


            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Warehouse> filteredWarehouses = warehouseDS.GetWarehouseList(filtr);

                var allWarehousesQuery = filteredWarehouses.Take(20);

                var allWarehousesObjects = allWarehousesQuery.ToArray();
                var allWarehousesRadComboBoxItemData = from warehouse in allWarehousesObjects
                                                       orderby warehouse.Name
                                                       select new RadComboBoxItemData
                                                       {
                                                           Text = warehouse.Name + " [" + warehouse.Company.NameAbbreviation + "]",
                                                           Value = warehouse.Id.ToString()
                                                       };

                result.Items = allWarehousesRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericPersonList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Person> filteredPersons = warehouseDS.GetPersonList(filtr);

                var allPersonsQuery = filteredPersons.Take(20);

                var allPersonsObjects = allPersonsQuery.ToArray();
                var allPersonsRadComboBoxItemData = from person in allPersonsObjects
                                                    orderby person.FirstName, person.LastName
                                                    select new RadComboBoxItemData
                                                    {
                                                        Text = person.FirstName + " " + person.LastName,
                                                        Value = person.Id.ToString()
                                                    };

                result.Items = allPersonsRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
          {
              new RadComboBoxItemData() { Text = "brak", Value = "1" },
              new RadComboBoxItemData() { Text = "wyników", Value = "2" }
          };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericUnitList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.Unit> filteredUnits = warehouseDS.GetUnitList(filtr);

                var allUnitsQuery = filteredUnits; //.Take(0);

                var allUnitsObjects = allUnitsQuery.ToArray();
                var allallUnitsQueryRadComboBoxItemData = from unit in allUnitsObjects
                                                          orderby unit.Name
                                                          select new RadComboBoxItemData
                                                          {
                                                              Text = unit.Name + " (" + unit.ShortName + ")",
                                                              Value = unit.Id.ToString()
                                                          };

                result.Items = allallUnitsQueryRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        private RadComboBoxData _GetTelericContractingPartyList(RadComboBoxContext context, int companyId)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.ContractingParty> filteredParties = warehouseDS.GetContractingPartyList(filtr, companyId);

                var allPartiesQuery = filteredParties.Take(20);

                var allPartiesObjects = allPartiesQuery.ToArray();
                var allPartiesQueryRadComboBoxItemData = from unit in allPartiesObjects
                                                         orderby unit.Name
                                                         select new RadComboBoxItemData
                                                         {
                                                             Text = unit.Name, // + " [" + (unit.DostawcaId.HasValue ? "DOSTAWCA" : "") + (unit.DostawcaId.HasValue && unit.OdbiorcaId.HasValue ? "," : "") + (unit.OdbiorcaId.HasValue ? "ODBIORCA" : "") + "]",
                                                             Value = unit.Id.ToString()
                                                         };

                result.Items = allPartiesQueryRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }
            if (result.Items.Count() < 1)
            {
                RadComboBoxItemData[] rcbid = new RadComboBoxItemData[1];


                result.Items[0] = new RadComboBoxItemData
                {
                    Text = "",
                    Value = ""
                };
            }
            return result;
        }

        private RadComboBoxData _GetTelericContractingPartySIMPLEList(RadComboBoxContext context, int companyId)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.C_ImportSimple_ContractingPartyFromSimple2> filteredParties = warehouseDS.GetContractingPartyFromSIMPLE2List(filtr, companyId);

                var allPartiesQuery = filteredParties.Take(20);

                var allPartiesObjects = allPartiesQuery.ToArray();
                var allPartiesQueryRadComboBoxItemData = from unit in allPartiesObjects
                                                         orderby unit.ContractingPartyName
                                                         select new RadComboBoxItemData
                                                         {
                                                             Text = unit.ContractingPartyName + " [" + (unit.DostawcaId.HasValue ? "DOSTAWCA" : "") + (unit.DostawcaId.HasValue && unit.OdbiorcaId.HasValue ? "," : "") + (unit.OdbiorcaId.HasValue ? "ODBIORCA" : "") + "]",
                                                             Value = unit.ContractingPartyId.ToString()
                                                         };

                result.Items = allPartiesQueryRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }
            if (result.Items.Count() < 1)
            {
                RadComboBoxItemData[] rcbid = new RadComboBoxItemData[1];


                result.Items[0] = new RadComboBoxItemData
                {
                    Text = "",
                    Value = ""
                };
            }
            return result;
        }
        //GetTelericContractingPartySIMPLEList


        private RadComboBoxData _GetTelericPackageTypeNameList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.PackageTypeName> filteredParties = warehouseDS.GetPackageTypeNameList(filtr);

                var allPartiesQuery = filteredParties; //.Take(20);

                var allPartiesObjects = allPartiesQuery.ToArray();
                var allPartiesQueryRadComboBoxItemData = from unit in allPartiesObjects
                                                         orderby unit.Id
                                                         select new RadComboBoxItemData
                                                         {
                                                             Text = unit.Name,
                                                             Value = unit.Id.ToString()
                                                         };

                result.Items = allPartiesQueryRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }
            if (result.Items.Count() < 1)
            {
                RadComboBoxItemData[] rcbid = new RadComboBoxItemData[1];


                result.Items[0] = new RadComboBoxItemData
                {
                    Text = "",
                    Value = ""
                };
            }
            return result;
        }


        private RadComboBoxData _GetTelericPackageTypeList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context.Text;

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.PackageTypeName> filteredParties = warehouseDS.GetPackageTypeList(filtr);

                var allPartiesQuery = filteredParties; //.Take(20);

                var allPartiesObjects = allPartiesQuery.ToArray();
                var allPartiesQueryRadComboBoxItemData = from unit in allPartiesObjects
                                                         orderby unit.Id
                                                         select new RadComboBoxItemData
                                                         {
                                                             Text = unit.Name,
                                                             Value = unit.Id.ToString()
                                                         };

                result.Items = allPartiesQueryRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }
            if (result.Items.Count() < 1)
            {
                RadComboBoxItemData[] rcbid = new RadComboBoxItemData[1];


                result.Items[0] = new RadComboBoxItemData
                {
                    Text = "",
                    Value = ""
                };
            }
            return result;
        }


        private string _CheckOperationAvailability(int? fromWarehouseId, int? toWarehouseId, WarehouseDocumentTypeEnum operation, int? systemUserId)
        {
            return string.Empty; //NOTICE: on request we're disabling all permissions because fo not work

            string exMessage = "";
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                exMessage = warehouseDs.CheckOperationAvailability(fromWarehouseId, toWarehouseId, operation, systemUserId);
            }

            return exMessage;
        }

        private RadComboBoxData _GetDepartmentsFilteredList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData result = new RadComboBoxData();
            string filtr = context["Text"].ToString();
            int comId = int.Parse(context["Company"].ToString());

            

            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {

                IQueryable<Mag.Domain.Model.DepartmentSIMPLEFiltered> filteredDepartment = warehouseDS.GetDepartmentFilteredList(filtr,comId);

                //var allDepartmentQuery = filteredDepartment.Take(20);

                var allDepartmentObjects = filteredDepartment.Select(x => new
                {
                    nazwa = x.nazwa,
                    komorka_id = x.komorka_id
                }).Distinct().Take(20).ToArray();


                //var allDepartmentObjects = allDepartmentQuery.ToArray();

                var allDepartmentsRadComboBoxItemData = allDepartmentObjects
                                                        .OrderBy(x => x.nazwa)
                                                        .Select(x => new RadComboBoxItemData
                                                            {
                                                                Text = x.nazwa,
                                                                Value = x.komorka_id.ToString()
                                                            });

                result.Items = allDepartmentsRadComboBoxItemData.ToArray();

                if (result.Items == null || result.Items.Count() == 0)
                {
                    result.Items = new RadComboBoxItemData[2]
                                       {
                                           new RadComboBoxItemData() { Text = "brak", Value = "1" },
                                           new RadComboBoxItemData() { Text = "wyników", Value = "2" }
                                       };
                }
            }

            return result;
        }

        // Add [WebGet] attribute to use HTTP GET
        [OperationContract]
        public string GetData(int val)
        {
            // Add your operation implementation here
            return "12";
        }

        //_GetTelericCompaniesList
        [OperationContract]
        public RadComboBoxData GetCompaniesList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericCompaniesList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetDepartmentsFilteredList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetDepartmentsFilteredList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericShelfsList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericShelfsList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericShelfsListWithPrompt(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericShelfsListWithPrompt(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericMaterialsListAllCompanies(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericMaterialsList(context);
            }

            return radComboBoxData;
        }


        [OperationContract]
        public RadComboBoxData GetTelericMaterialsListCompany1(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericMaterialsList(context, 1);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericMaterialsListCompany2(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericMaterialsList(context, 2);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericMaterialsListCompany3(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericMaterialsList(context, 3);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericMaterialsSIMPLEListCompany1(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            radComboBoxData = _GetTelericMaterialsSIMPLEList(context, 1);

            //radComboBoxData = GetTelericShelfsList(context);

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericMaterialsSIMPLEListCompany2(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;


            radComboBoxData = _GetTelericMaterialsSIMPLEList(context, 2);

            //radComboBoxData = GetTelericShelfsList(context);


            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericIndexesSIMPLEList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericIndexesSIMPLEList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericOrdersList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericOrdersList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetDepartmentList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericOrdersList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericDocumentTypesList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);
            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericDocumentTypesList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericWarehouseList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericWarehouseList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericPersonList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericPersonList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericUnitList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericUnitList(context);
            }

            return radComboBoxData;
        }


        [OperationContract]
        public RadComboBoxData GetTelericContractingPartyListCompany1(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericContractingPartyList(context, 1);
            }

            return radComboBoxData;
        }


        [OperationContract]
        public RadComboBoxData GetTelericContractingPartyListCompany2(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericContractingPartyList(context, 2);
            }

            return radComboBoxData;
        }


        [OperationContract]
        public RadComboBoxData GetTelericContractingPartyListCompany3(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericContractingPartyList(context, 3);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericContractingPartySIMPLEListCompany1(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericContractingPartySIMPLEList(context, 1);
            }

            return radComboBoxData;
        }


        [OperationContract]
        public RadComboBoxData GetTelericContractingPartySIMPLEListCompany2(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericContractingPartySIMPLEList(context, 2);
            }

            return radComboBoxData;
        }


        [OperationContract]
        public RadComboBoxData GetTelericContractingPartySIMPLEListCompany3(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericContractingPartySIMPLEList(context, 3);
            }

            return radComboBoxData;
        }


        //GetTelericOrderNumbersSIMPLEListCompany
        [OperationContract]
        public RadComboBoxData GetTelericOrderNumbersSIMPLEListCompany1(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericOrderNumbersSIMPLEList(context, 1);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericOrderNumbersSIMPLEListCompany2(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericOrderNumbersSIMPLEList(context, 2);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericOrderNumbersSIMPLEListCompany3(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericOrderNumbersSIMPLEList(context, 3);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericPackageTypeList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericPackageTypeList(context);
            }

            return radComboBoxData;
        }

        [OperationContract]
        public RadComboBoxData GetTelericPackageTypeNameList(RadComboBoxContext context)
        {
            Contract.Requires(context != null);

            RadComboBoxData radComboBoxData;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                radComboBoxData = _GetTelericPackageTypeNameList(context);
            }

            return radComboBoxData;
        }

        //_GetTelericPackageTypeList

        // Warehouse system API:

        [OperationContract]
        public string CheckOperationAvailability(int? fromWarehouseId, int? toWarehouseId, WarehouseDocumentTypeEnum operation, int? systemUserId)
        {
            return string.Empty; //NOTICE: on request we're disabling all permissions because fo not work

            return _CheckOperationAvailability(fromWarehouseId, toWarehouseId, operation, systemUserId);
        }

        //[Obsolete]
        //[OperationContract]
        //public PositionWithQuantityAndShelfs CollectWarehouseDocPositionsToBeTaken(int warehouseId, int materialId, IEnumerable<string> ordersWithQuantities, bool quantitySortOrderAscending = true)
        //{
        //      return Warehouse.CollectWarehouseDocPositionsToBeTaken(warehouseId, materialId, ordersWithQuantities, null, quantitySortOrderAscending);
        //}
    }
}
