﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;

namespace MagWeb
{
    public partial class PlaceAnswer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();
            if(Request.QueryString.Count != 1)
            {
                throw new Exception("Błędny parametr!");
            }
            string token = Request.QueryString[0];
            try
            {
                using(WarehouseDS warehouseDs = new WarehouseDS())
                {
                    warehouseDs.PlaceAnswerForQuestion(token);
                }
                Response.Write("Dziękuję za odpowiedź :-) System K3");
            }
            catch (Exception ex)
            {
                Response.Write("Błąd! " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            }
        }
    }
}