﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class ReportForReceives : System.Web.UI.Page
    {
        private int companyId = -1;

        

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["tab_Reports"] = 3;

            companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            if (!Page.IsPostBack)
            {
                if (RadDatePicker2.SelectedDate == null)
                {
                    DateTime dateTime = DateTime.Now;
                    RadDatePicker1.SelectedDate = new DateTime(dateTime.Year, dateTime.Month, 1);
                }
                RadDatePicker2.SelectedDate = DateTime.Now.Date;
                //inputDate.SelectedDate = DateTime.Now.Date;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            QuantityRTB.Text = QuantityRTB.Text.Replace(".", ",");
            MaterialGrid.Rebind();
        }

        protected void ClearStartDateFilterClick(object sender, EventArgs e)
        {
            RadDatePicker1.SelectedDate = null;
        }
        protected void ClearEndDateFilterClick(object sender, EventArgs e)
        {
            RadDatePicker2.SelectedDate = null;
        }


        protected void MaterialGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                string materialToSearch = MaterialToSearchRTB.Text;
                string ContractingPartyNameToSearch = ContractingPartyName.Text;

                DateTime? startDate = null;
                DateTime? endDate = null;
                DateTime? RadDatePicker1_SelectedDate = RadDatePicker1.SelectedDate;
                DateTime? RadDatePicker2_SelectedDate = RadDatePicker2.SelectedDate;
                if (RadDatePicker1_SelectedDate != null)
                {
                    startDate = (DateTime)RadDatePicker1.SelectedDate;
                }
                if (RadDatePicker2_SelectedDate != null)
                {
                    endDate = (DateTime)RadDatePicker2.SelectedDate;
                }


                string quantityToSearch = QuantityRTB.Text;
                string orderNumberToSearch = OrderNumberRTB.Text;
                string orderNumberPZToSearch = OrderNumberPZ_RTB.Text;


                var materialsCandidatesToReceiveMaterialized = warehouseDs.GetWaitingForSupplyWithZeros(materialToSearch, ContractingPartyNameToSearch, startDate,
                                                                                               endDate.Value, companyId, quantityToSearch, orderNumberToSearch, orderNumberPZToSearch, Convert.ToInt32(QuantityTolerance.SelectedValue), this.CheckBox_Arrived.Checked, this.CheckBox_NotArrived.Checked);

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                var materialsCandidatesToReceiveMaterializedOrdered = materialsCandidatesToReceiveMaterialized.OrderBy(p => p.DataKL);

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                List<WaitingForSupplyWithZeros1> materialsCandidatesToPresent = new List<WaitingForSupplyWithZeros1>();

                foreach (var supply in materialsCandidatesToReceiveMaterializedOrdered)
                {
                    WaitingForSupplyWithZeros1 waitingForSupplyForPresent = new WaitingForSupplyWithZeros1();

                    waitingForSupplyForPresent.ID = supply.ID;
                    waitingForSupplyForPresent.IDpl = supply.IDpl;
                    waitingForSupplyForPresent.Cena = supply.Cena;
                    waitingForSupplyForPresent.kontrachent_id = supply.CorrectKontrahentId; //supply.kontrachent_id;
                    waitingForSupplyForPresent.DataKL = supply.DataKL;
                    waitingForSupplyForPresent.IDsim = supply.IDsim;
                    waitingForSupplyForPresent.ContractingPartyName = supply.ContractingPartyName;
                    waitingForSupplyForPresent.NazwSK = supply.NazwSK;
                    waitingForSupplyForPresent.MaterialName = supply.MaterialName;
                    waitingForSupplyForPresent.IloscZam = supply.IloscZam;
                    waitingForSupplyForPresent.UnitName = supply.UnitName;
                    waitingForSupplyForPresent.NRp = supply.CorrectOrderNumber;
                    waitingForSupplyForPresent.Flaga = supply.Flaga;
                    waitingForSupplyForPresent.SourceType = supply.SourceType;
                    waitingForSupplyForPresent.SimpleDocId = supply.SimpleDocId;

                    materialsCandidatesToPresent.Add(waitingForSupplyForPresent);
                }

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                int i = 1;

                var materials = from p in materialsCandidatesToPresent
                                select
                                    new
                                        {
                                            RowNumber = i++,
                                            ProcessHeaderId = p.ID,
                                            ProcessIDPL = p.IDpl,
                                            UnitPrice = p.Cena,
                                            ContractingPartyId = p.kontrachent_id,
                                            SupplyDate = p.DataKL,
                                            IdSim = p.IDsim,
                                            ContractingParty = p.ContractingPartyName,
                                            OrderNumberPZ = p.NazwSK,
                                            MaterialName = p.MaterialName,
                                            Quantity = Convert.ToDouble(p.IloscZam),
                                            UnitName = p.UnitName,
                                            OrderNumber = p.NRp,
                                            ForDepartment = p.Flaga == 2 || p.Flaga == 3 ? 1 : 0,
                                            SourceType = p.SourceType,
                                            SimpleDocId = p.SimpleDocId
                                    };
                MaterialGrid.DataSource = materials;

                int received0Count = 0;
                int received1Count = 0;


                received0Count = (from m in materials where m.SimpleDocId== null || m.SimpleDocId.Trim() == "" select m).Count();
                received1Count = (from m in materials where m.SimpleDocId != null && m.SimpleDocId.Trim() != "" select m).Count();

                int all = received0Count + received1Count;

                this.Label_Stats.Text = "Wszystkich: " + all + ", Przyjętych: " + received1Count + " (" +
                                        Math.Round((double)(((double)received1Count)/((double)all))*100) + "%)";

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());
            }
        }
    }
}