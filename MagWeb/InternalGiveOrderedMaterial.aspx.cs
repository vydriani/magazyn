using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using MagWeb.Extensions;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using Mag.Domain.Data;

namespace MagWeb
{
    public partial class InternalGiveOrderedMaterial : FeaturedSystemWebUIPage
    {
        // General list:
        private const string ORDER_ID_IDEX_IN_GRID = "OrderId";
        private const string HOUR_ID_IDEX_IN_GRID = "DateAndHour";

        // Details:
        private const string ORDER_DOC_ID_INDEX_IN_TABS = "WarehouseDocumentId";
        private const string ORDER_DOC_CHANGED_MATERIAL_INDEX_IN_TABS = "WarehouseDocumentPositionId";

        private List<PositionWithQuantityAndShelfs> positions;

        private List<Tuple<int, int>> docChangedMaterials;

        protected void Page_Load(object sender, EventArgs e)
        {
            int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                        (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, null, WarehouseDocumentTypeEnum.MMMinus, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
                    PermissionDenied();
                    return;
                }
            }

            if (!IsPostBack)
            {
                inputDate.SelectedDate = DateTime.Now.Date;
            }



            if (!IsPostBack)
            {
                Session["InternalGiveOrderedMaterial_positionsToAdd"] = null;
            }

            if (Session["InternalGiveOrderedMaterial_positionsToAdd"] == null)
                this.positions = new List<PositionWithQuantityAndShelfs>();
            else
                this.positions = (List<PositionWithQuantityAndShelfs>)Session["InternalGiveOrderedMaterial_positionsToAdd"];



            //this.SignatureControl1.DefaultComment = "Przyjąłem materiał zgodny z opisem w niniejszym dokumencie.";

            //this.SignatureControl1.PositionsToSign = positions;




            if (WaitingOrdersRadGrid.SelectedItems != null && WaitingOrdersRadGrid.SelectedItems.Count > 0)
            {
                PositionWithQuantityAndShelfs position = new PositionWithQuantityAndShelfs();

                position.documentParentIds = new List<int>();
                position.documentParentIds.Add(666);  // why the fuck is this here?

                GridDataItem item = (GridDataItem)WaitingOrdersRadGrid.SelectedItems[0];

                if (item != null)
                {
                    var parentId = item.GetCellValueAsNullable<int>("OrderId");//.Cells[3];

                    if (parentId != null)
                    {
                        this.positions.Clear();
                        position.documentParentIds.Add(parentId.Value);
                    }
                }

                this.positions.Add(position);
                Session["InternalGiveOrderedMaterial_positionsToAdd"] = this.positions;
            }

            if (!IsPostBack)
            {
                using (var warehouseDs = new WarehouseDS())
                {
                    // get current warehouse type
                    var currentWarehouse = warehouseDs.GetWarehouseById(currentWarehouseId.Value);

                    if (currentWarehouse.IsInput)
                    {
                        Panel_SimpleDocTypeRw.Visible = true;
                        //if(currentWarehouse.)

                        RadComboBox_DocSubtypeSIMPLE.DataSource =
                                from d in warehouseDs.GetDocTypesFromSIMPLE_RW(currentComapnyId)
                                select new { Value = d.typdrw_id, Text = d.nazwa.Trim() + " (" + d.typdok_idn.Trim() + ")" };
                        RadComboBox_DocSubtypeSIMPLE.DataTextField = "Text";
                        RadComboBox_DocSubtypeSIMPLE.DataValueField = "Value";
                        RadComboBox_DocSubtypeSIMPLE.DataBind();
                        //RadComboBox_DocSubtypeSIMPLE.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));

                        // Selecting RW element
                        int selectedIndex =
                                (from p in this.RadComboBox_DocSubtypeSIMPLE.Items where p.Value == "1" select p.Index).
                                        FirstOrDefault();
                        this.RadComboBox_DocSubtypeSIMPLE.SelectedIndex = selectedIndex;

                        Label_TargetOrder.Text = "";
                    }
                    else
                    {
                        Panel_SimpleDocTypeRw.Visible = false;
                    }
                }

                //PrintButtonsPanel.Controls.Clear();
                btnPrintDocument.Enabled = false;
            }



            using (var warehouseDs = new WarehouseDS())
            {
                bool isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility(currentWarehouseId.Value,warehouseDs.GetPersonForSystemUser(userId).Id, 5);
                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }

            if (!Page.IsPostBack)
            {
                Session["INVChangedMaterials"] = new List<Tuple<int, int>>();
                HideChangeMaterialPanel();
                HideChangeQuantityPanel();

                btnSaveDocument.Enabled = false;
            }

            if (Session["INVChangedMaterials"] == null)
                docChangedMaterials = new List<Tuple<int, int>>();
            else
                docChangedMaterials = (List<Tuple<int, int>>)Session["INVChangedMaterials"];

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.btnSaveDocument.Visible = this.btnSaveDocument.Enabled;

            Session["INVChangedMaterials"] = docChangedMaterials;
        }

        protected void FilterClick(object sender, EventArgs e)
        {
            WaitingOrdersRadGrid.Rebind();
        }

        protected void WaitingOrdersRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int currentWarehouseId = (int)Session["CurrentWarehouseId"];

            using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                //warehouseDs.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

                int i = 1;
                var orderHeaders = warehouseDs
                    .GetUngivenOrderedPositionsHeaders(currentWarehouseId)
                    .Select(x => new
                    {
                        Lp = i++,
                        OrderId = x.Order.Id,
                        OrderNumber = x.Order.OrderNumber,
                        DateAndHour = x.DateAndHour
                    }).OrderByDescending(x => x.DateAndHour).ToArray();
                
                //var test = orderHeaders.ToArray();

                WaitingOrdersRadGrid.DataSource = orderHeaders;
            }
        }

        protected void MaterialsToGiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int currentWarehouseId = (int)Session["CurrentWarehouseId"];
            int? selectedOrderId = null;
            double AvaibleQuantity = 0;

            DateTime? selectedDateAndHour = null;

            if (WaitingOrdersRadGrid.SelectedItems.Count == 1)
            {
                //GridItem item = WaitingOrdersRadGrid.SelectedItems[0];
                GridDataItem item = (GridDataItem)WaitingOrdersRadGrid.SelectedItems[0];
                //var orderIdCell = item.Cells[ORDER_ID_IDEX_IN_GRID];
                var orderIdCell = item.GetCellValueAsNullable<int>("OrderId");//.Cells[3];


                //selectedOrderId = int.Parse(orderIdCell.Text);
                selectedOrderId = orderIdCell;
                var selectedDateAndHourCell = item.GetCellValueAs<DateTime>("DateAndHour");
                selectedDateAndHour = selectedDateAndHourCell;
            }


            if (selectedOrderId.HasValue && selectedDateAndHour.HasValue)
            {
                List<PositionWithShelfAndQuantityOrdered> positionWithShelfAndQuantities =
                    new List<PositionWithShelfAndQuantityOrdered>();

                using (WarehouseDS warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var positions = warehouseDs.GetUngivenOrderedDocumentPositions(
                        currentWarehouseId,
                        selectedDateAndHour.GetValueOrDefault(),
                        selectedOrderId);

                    foreach (var positionWithShelf in positions)
                    {
                        {
                            int ddadc = positionWithShelf.Id;

                            WarehouseDocumentPositionLayer warehouseDocumentPositionLayer = new WarehouseDocumentPositionLayer(positionWithShelf.Id);
                            string zones = "";
                            bool isNullZone = warehouseDocumentPositionLayer.isNullZone;

                            foreach (var zonePosition in warehouseDocumentPositionLayer.WarehouseDocumentPositionWithZone.WarehousePositionZones)
                            {
                                zones += zonePosition.Zone.Name + (isNullZone ? "(" + zonePosition.Quantity + ");" : "");
                            }

                            //if (positionWithShelf.DocType == (int)WarehouseDocumentTypeEnum.ZL)
                            //{
                            //    using (var warehouseDsEnt = new WarehouseDS(SessionData.ClientData))
                            //    {
                            //        var parentDoc = warehouseDsEnt.WarehouseEntitiesTest.WarehouseDocuments
                            //            .Where(x => x.Id == positionWithShelf.WarehouseDocumentParentId).FirstOrDefault();

                            //        positionWithShelf.WarehouseDocumentTimeStamp = parentDoc.TimeStamp;
                            //        positionWithShelf.WarehouseDocumentDueTime = parentDoc.DueTime;
                            //    }
                            //}


                            AvaibleQuantity = Math.Abs(positionWithShelf.Quantity) - Math.Abs(positionWithShelf.GivenQuantity);
                            if (AvaibleQuantity > 0)
                            {
                                positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantityOrdered()
                                {
                                    position = positionWithShelf,
                                    shelf = null,
                                    Zones = zones,
                                    quantity = AvaibleQuantity,
                                    ActualQuantity = Math.Abs(positionWithShelf.QuantityDiff),
                                    GivenQuantity = Math.Abs(positionWithShelf.GivenQuantity),
                                });
                            }
                        }
                    }


                    List<Tuple<int, int, string, string, string, string>> changedMaterials = new List<Tuple<int, int, string, string, string, string>>();
                    if (docChangedMaterials != null && docChangedMaterials.Count > 0)
                    {
                        foreach (var materialChange in docChangedMaterials)
                        {
                            Material material = warehouseDs.GetMaterialById(materialChange.Item2);
                            changedMaterials.Add(new Tuple<int, int, string, string, string, string>(materialChange.Item1, materialChange.Item2, material.NameShort, material.IndexSIMPLE, material.Unit.ShortName, "?"));
                        }
                    }


                    int i = 1;
                    var shelfs = positionWithShelfAndQuantities.GroupBy(
                        p => new
                        {
                            WarehouseId = currentWarehouseId,
                            DocumentId = p.position.WarehouseDocumentId,
                            PositionId = p.position.Id,
                            TargetWarehouse = p.position.TargetWarehouse,
                            TargetDepartment = p.position.DepartmentName,
                            TargetProductionMachineName = p.position.TargetProductionMachineName,
                            MaterialId = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.Id : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item2,
                            MaterialName = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.NameShort : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item3,
                            OrderId = p.position.OrderId,
                            ProcessIdpl = p.position.ProcessIdpl,
                            IndexSIMPLE = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.IndexSIMPLE : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item4,
                            UnitName = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.MaterialShortName : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item5,
                            ShelfName = !p.position.Material.Material1.Any() ? !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.Zones : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item6 : "?", // p.shelf != null ? p.shelf.Name : null,
                            TimeCreated = p.position.WarehouseDocumentTimeStamp,
                            DueTime = p.position.WarehouseDocumentDueTime,
                            IsParentMaterial = p.position.Material.Material1.Any(),
                            ChangeMaterialEnabled = p.position.Material.Material1.Any() && true,
                            IsLocked = p.position.IsLockedByQuantityChange,
                            QuantityExtended = (p.ActualQuantity != Math.Abs(p.quantity) ? p.ActualQuantity + " </br>(" + Math.Abs(p.quantity) + ")" : p.quantity.ToString())
                                                  + (p.GivenQuantity != 0 ? "</br>[Wydano:" + p.GivenQuantity.ToString().Trim() + "]" : ""),
                        }
                        ).Select(s => new
                        {
                            Lp = i++,
                            WarehouseId = s.Key.WarehouseId,
                            WarehouseDocumentId = s.Key.DocumentId,
                            WarehouseDocumentPositionId = s.Key.PositionId,
                            TargetWarehouse = s.Key.TargetWarehouse,

                            TargetDepartment = !string.IsNullOrEmpty(s.Key.TargetDepartment) ? s.Key.TargetDepartment : s.Key.TargetWarehouse,
                            TargetProductionMachineName = s.Key.TargetProductionMachineName,
                            MaterialName = s.Key.MaterialName,
                            IndexSIMPLE = s.Key.IndexSIMPLE,
                            QuantityExtended = s.Key.QuantityExtended,
                            UnitName = s.Key.UnitName,
                            ShelfName = s.Key.ShelfName,
                            CreationDate = s.Key.TimeCreated,
                            DueTime = s.Key.DueTime,
                            IsParentMaterial = s.Key.IsParentMaterial,
                            ChangeMaterialEnabled = s.Key.IsParentMaterial,
                            IsLocked = s.Key.IsLocked,
                            IsUnlocked = !s.Key.IsLocked
                        });
                    MaterialsToGiveRadGrid.DataSource = shelfs.ToArray();
                }
            }
        }

        //protected void WaitingOrdersRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        //{
        //    int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];
        //    string filter = filterTextBox.Text.ToLower();

        //    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //    {
        //        var orderHeaders = warehouseDs.GetUngivenOrderedPositionsHeaders(currentWarehouseId)
        //                    .Select((x, n) => new
        //                                            {
        //                                                Lp = ++n,
        //                                                OrderId = x.Order.Id,
        //                                                OrderNumber = x.Order.OrderNumber,
        //                                                DateAndHour = x.DateAndHour
        //                                            });

        //        if (!string.IsNullOrWhiteSpace(filter))
        //            orderHeaders = orderHeaders.Where(item => item.OrderNumber.ToLower().Contains(filter) || item.DateAndHour.ToString("yyyy-MM-dd HH:mm").Contains(filter));

        //        WaitingOrdersRadGrid.DataSource = orderHeaders.OrderByDescending(p => p.DateAndHour).ToArray();

        //    }
        //}

        //private List<MaterialAvailabilityInfo> GetMaterialAvailableChildren(int warehouseId, int materialId, int? orderId, int? processIdpl, double neededQuantity)
        //{
        //    List<MaterialAvailabilityInfo> list = new List<MaterialAvailabilityInfo>();

        //    using(WarehouseDS warehouseDs = new WarehouseDS())
        //    {
        //        Material material = warehouseDs.GetMaterialById(materialId, true);
        //        if(material.ChildMaterials != null)
        //        {
        //            foreach(var childMaterial in material.ChildMaterials)
        //            {
        //                var stocks =
        //                    warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments.Where(
        //                        p =>
        //                        p.WarehouseTargetId == warehouseId && p.MaterialId == childMaterial.Id && p.OrderId == orderId &&
        //                        p.ProcessIdpl == processIdpl);

        //                if(stocks.Sum(p => p.Quantity) >= neededQuantity)
        //                {
        //                    MaterialAvailabilityInfo materialAvailabilityInfo = new MaterialAvailabilityInfo();
        //                    materialAvailabilityInfo.MaterialId = childMaterial.Id;
        //                    materialAvailabilityInfo.MaterialName = childMaterial.Name;

        //                    materialAvailabilityInfo.ZoneQuantities = new List<Tuple<int, string, double>>();
        //                    foreach (var stock in stocks)
        //                    {
        //                        Tuple<int, string, double> tuple = new Tuple<int, string, double>(stock.ShelfId, stock.ShelfName, stock.Quantity.Value);
        //                        materialAvailabilityInfo.ZoneQuantities.Add(tuple);
        //                    }
        //                    list.Add(materialAvailabilityInfo);
        //                }

        //            }
        //        }
        //    }

        //    return list;
        //}

        //protected void MaterialsToGiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        //{
        //    int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

        //    int? selectedOrderId = null;
        //    DateTime? selectedDateAndHour = null;


        //    if (WaitingOrdersRadGrid.SelectedItems.Count == 1)
        //    {
        //        var item = WaitingOrdersRadGrid.SelectedItems[0] as GridDataItem;
        //        selectedOrderId = item.GetCellValueAsNullable<int>(ORDER_ID_IDEX_IN_GRID);
        //        var tmp = item.GetCellValueAs<string>(HOUR_ID_IDEX_IN_GRID);
        //        selectedDateAndHour = DateTime.ParseExact(tmp, "yyyy-MM-dd HH:mm", Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        //    }

        //    if (selectedOrderId.HasValue && selectedDateAndHour.HasValue)
        //    {
        //        var positionWithShelfAndQuantities = new List<PositionWithShelfAndQuantity>();

        //using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //        {
        //            var positions = warehouseDs.GetUngivenOrderedPositions(currentWarehouseId, selectedOrderId, selectedDateAndHour);

        //            foreach (var externalIterator in positions)
        //            {
        //    if (externalIterator.WarehousePositionZones.Any())
        //                {
        //      var usedQuantity = 0M;
        //                    string zones = "";

        //      bool isNultiZone = externalIterator.WarehousePositionZones.Count > 1;

        //      foreach (var internalIterator in externalIterator.WarehousePositionZones)
        //                    {
        //                        zones = zones + internalIterator.Zone.Name + (isNultiZone ? "(" + internalIterator.Quantity + ");" : "");
        //                    }

        //                    positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
        //                        {
        //                            position = externalIterator,
        //                            shelf = null,
        //                            Zones = zones,
        //          quantity = (decimal)externalIterator.Quantity //internalIterator.Quantity
        //                        });
        //      usedQuantity += (decimal)externalIterator.Quantity; //internalIterator.Quantity;


        //      if (usedQuantity < (decimal)externalIterator.Quantity)
        //                    {
        //                        positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
        //                                                                                                     {
        //                                                                                                         position = externalIterator,
        //                                                                                                         shelf = null,
        //                                                                                                         quantity =
        //                                                     (decimal)externalIterator.Quantity - usedQuantity
        //                                                                                                     });
        //                    }
        //                }
        //                else
        //                {
        //                    positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
        //                                                                                                 {
        //                                                                                                     position = externalIterator,
        //                                                                                                     shelf = null,
        //                                                                                                     Zones = "",
        //                                               quantity = (decimal)externalIterator.Quantity
        //                                                                                                 });
        //                }
        //            }


        //            List<Tuple<int, int, string, string, string, string>> changedMaterials = new List<Tuple<int, int, string, string, string, string>>();
        //            if (docChangedMaterials != null && docChangedMaterials.Count > 0)
        //            {
        //                foreach (var materialChange in docChangedMaterials)
        //                {
        //                    Material material = warehouseDs.GetMaterialById(materialChange.Item2);
        //                    changedMaterials.Add(new Tuple<int, int, string, string, string, string>(materialChange.Item1, materialChange.Item2, material.NameShort, material.IndexSIMPLE, material.Unit.ShortName, "?"));
        //                }
        //            }

        //            int i = 1;
        //            var shelfs = from p in positionWithShelfAndQuantities
        //                                     //warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id))
        //                                     group p by new
        //                                            {
        //                                                WarehouseId = currentWarehouseId,

        //                                                DocumentId = p.position.WarehouseDocument.Id,
        //                                                PositionId = p.position.Id,

        //                                                TargetWarehouse = p.position.WarehouseDocument.Warehouse1.Name,
        //                                                TargetDepartment = p.position.Department == null ? "---" : p.position.Department.Name,
        //                                                TargetProductionMachineName = p.position.ProductionMachine != null ? p.position.ProductionMachine.Name : "",

        //                                                MaterialId = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.Id : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item2,
        //                                                MaterialName = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.NameShort : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item3,

        //                                                OrderId = p.position.OrderId,
        //                                                ProcessIdpl = p.position.ProcessIdpl,

        //                                                IndexSIMPLE = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.IndexSIMPLE : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item4,
        //                                                //NameSIMPLE = p.position.Material.Name,
        //                                                UnitName = !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.position.Material.Unit.ShortName : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item5,
        //                    ShelfName = !p.position.Material.Material1.Any() ? !changedMaterials.Any(chm => chm.Item1 == p.position.Id) ? p.Zones : changedMaterials.First(chm => chm.Item1 == p.position.Id).Item6 : "?", // p.shelf != null ? p.shelf.Name : null,
        //                                                TimeCreated = p.position.WarehouseDocument.TimeStamp,
        //                                                DueTime = p.position.WarehouseDocument.DueTime,

        //                    IsParentMaterial = p.position.Material.Material1.Any(),
        //                    ChangeMaterialEnabled = p.position.Material.Material1.Any() && true,

        //                                                IsLocked = p.position.IsLockedByQuantityChange,

        //                                                ActualQuantity = p.position.ActualQuantity,

        //                                                QuantityExtended = p.position.ActualQuantity != Math.Abs(p.position.Quantity) ? p.position.ActualQuantity + " (" + Math.Abs(p.position.Quantity) + ")" : p.position.ActualQuantity.ToString()
        //                                            }
        //                                         into g
        //                                         select
        //                                                 new
        //                                                         {
        //                                                             Lp = i++,

        //                                                             WarehouseId = g.Key.WarehouseId,

        //                                                             WarehouseDocumentId = g.Key.DocumentId,
        //                                                             WarehouseDocumentPositionId = g.Key.PositionId,

        //                                                             TargetWarehouse = g.Key.TargetWarehouse,
        //                                                             TargetDepartment = !string.IsNullOrEmpty(g.Key.TargetDepartment) ? g.Key.TargetDepartment : g.Key.TargetWarehouse,
        //                                                             TargetProductionMachineName = g.Key.TargetProductionMachineName,

        //                                                             MaterialName = g.Key.MaterialName,

        //                                                             IndexSIMPLE = g.Key.IndexSIMPLE,

        //                                                             Quantity = Math.Abs(g.Sum(p => p.quantity)),

        //                                                             QuantityExtended = g.Key.QuantityExtended,





        //                                                             // TODO: Test if QuantityExtended good with aggregation!!!!!!!!!!




        //                                                             ActualQuantity = g.Key.ActualQuantity,

        //                                                             UnitName = g.Key.UnitName,
        //                                                             ShelfName = g.Key.ShelfName,

        //                                                             CreationDate = g.Key.TimeCreated,
        //                                                             DueTime = g.Key.DueTime,

        //                                                             IsParentMaterial = g.Key.IsParentMaterial,
        //                                                             //AvailableChildMaterials = GetMaterialAvailableChildren(g.Key.WarehouseId, g.Key.MaterialId, g.Key.OrderId, g.Key.ProcessIdpl, Math.Abs(g.Sum(p => p.quantity))),
        //                                                             ChangeMaterialEnabled = g.Key.IsParentMaterial,

        //                                                             IsLocked = g.Key.IsLocked,
        //                                                             IsUnlocked = !g.Key.IsLocked
        //                                                         };

        //            MaterialsToGiveRadGrid.DataSource = shelfs.ToArray();
        //        }
        //    }
        //}

        protected void MaterialsToChangeGiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {

        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            int userId;

            if (MaterialsToGiveRadGrid.SelectedItems == null || MaterialsToGiveRadGrid.SelectedItems.Count == 0)
            {
                WebTools.AlertMsgAdd("Brak zaznaczonych pozycji.");
                return;
            }

            if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
            {
                int docSubtypeSIMPLE = 1;

                if (Panel_SimpleDocTypeRw.Visible)
                    int.TryParse(RadComboBox_DocSubtypeSIMPLE.SelectedValue, out docSubtypeSIMPLE);

                if (IsValid) // && this.SignatureControl1.ValidateSignatures() != null)
                {
                    List<int> orderDocumentIds = new List<int>();
                    foreach (GridDataItem gridItem in MaterialsToGiveRadGrid.SelectedItems)
                    {
                        //var cell0 = gridItem.Cells[0];
                        //var cell1 = gridItem.Cells[1];
                        //var cell2 = gridItem.Cells[2];
                        //var cell3 = gridItem.Cells[3];
                        //var cell4 = gridItem.Cells[4];

                        int orderDocumentId = gridItem.GetCellValueAs<int>(ORDER_DOC_ID_INDEX_IN_TABS);

                        //// TODO: ORDER_DOC_CHANGED_MATERIAL_INDEX_IN_TABS not supported!!!!!!
                        //var cellMaterial = gridItem.Cells[ORDER_DOC_CHANGED_MATERIAL_INDEX_IN_TABS]; // TODO: ORDER_DOC_CHANGED_MATERIAL_INDEX_IN_TABS not supported!!!!!!
                        //// TODO: ORDER_DOC_CHANGED_MATERIAL_INDEX_IN_TABS not supported!!!!!!
                        //int changedMaterialId = 0;
                        //int.TryParse(cellMaterial.Text, out changedMaterialId);
                        //int? changedMaterialIdFinal = null;
                        //if(changedMaterialId > 0)
                        //{
                        //    changedMaterialIdFinal = changedMaterialId;
                        //}

                        orderDocumentIds.Add(orderDocumentId); //, changedMaterialIdFinal));
                    }

                    if (!orderDocumentIds.Any())
                    {
                        WebTools.AlertMsgAdd("Brak zaznaczonych pozycji.");
                        return;
                    }

                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        lock (typeof(WarehouseDS))
                        {
                            try
                            {
                                // tu na razie zostają nulle
                                int? targetDeptSIMPLEId = null;
                                int? targetOrderId = null;

                                //byte[] docPositionsHash = Signature.ComputeDocPositionsHash(positions);


                                // ATTENTION!!! At the moment, only 1 POSITION PER DOC IS SUPPORTED!!!!!!!!!!! Otherwise, the mechanism WILL NOT WORK!!!


                                //List<Tuple<int, int?>> documentMaterialIdTuples = new List<Tuple<int, int?>>();


                                IEnumerable<WarehouseDocument> warehouseDocuments = warehouseDs.InternalGiveForOrderMultiple(orderDocumentIds,
                                                                                                                                                         warehouseDs.
                                                                                                                                                                 GetPersonForSystemUser
                                                                                                                                                                 (userId).Id,
                                                                                                                                                         inputDate.SelectedDate ??
                                                                                                                                                         DateTime.Now.Date,
                                    /*null, //this.SignatureControl1.Signatures,
                                    docPositionsHash,*/
                                                                                                                                                         CheckBox_ExportToSIMPLE
                                                                                                                                                                 .Checked,
                                                                                                                                                         targetDeptSIMPLEId,
                                                                                                                                                         targetOrderId,
                                                                                                                                                         docSubtypeSIMPLE);
                                WaitingOrdersRadGrid.Rebind();
                                MaterialsToGiveRadGrid.Rebind();

                                Label_SourceZones.Text = "";
                                foreach (WarehouseDocument warehouseDocument in warehouseDocuments)
                                {
                                    Label_SourceZones.Text = Label_SourceZones.Text +
                                            warehouseDs.GetDocumentSourceZonesInfoFormatted(warehouseDocument.Id) + ";;;";
                                }


                                //this.SignatureControl1.ClearSignatures();


                                /*int j = 0;
                                foreach(WarehouseDocument warehouseDocument in warehouseDocuments)
                                {
                                        j++;
                                        Button button = new Button();
                                        button.ID = string.Format("ButtonDocument{0}", j);
                                        //btn.Click += new EventHandler(btn_Click);
                                        DocumentPrintingUtils.BindDocumentToButton(button,
                                                                                                                             WarehouseDocumentTypeEnum.MMMinus,
                                                                                                                             warehouseDocument.Id);
                                        button.Text = string.Format("Drukuj dokument #{0}", warehouseDocument.Id);
                                        PrintButtonsPanel.Controls.Add(button);
                                        PrintButtonsPanel.Controls.Add(new LiteralControl("<br />"));
                                }*/

                                var warehouseDocumentIds = new List<int>();
                                foreach (WarehouseDocument warehouseDocument in warehouseDocuments)
                                {
                                    warehouseDocumentIds.Add(warehouseDocument.Id);
                                }
                                DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument,
                                                                                                                     WarehouseDocumentTypeEnum.MMMinus,
                                                                                                                     warehouseDocumentIds);
                                btnPrintDocument.Enabled = true;

                                //DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument,
                                //                                           WarehouseDocumentTypeEnum.MMMinus,
                                //                                           warehouseDocument.Id);

                                StringBuilder stringBuilder = new StringBuilder();

                                string[] warehouseTargetNames =
                                        warehouseDocuments.Select(w => w.Warehouse1.Name).Distinct().ToArray();

                                int len = warehouseTargetNames.Length;
                                for (int i = 0; i < len; i++)
                                {
                                    stringBuilder.Append(warehouseTargetNames[i]);
                                    if (i + 1 < len)
                                    {
                                        stringBuilder.Append(", ");
                                    }
                                }

                                WebTools.AlertMsgAdd("Dokument został zapisany. Materiały zostały wydane do: <b>" +
                                                                         stringBuilder + "</b>");

                                foreach (var orderDocument in orderDocumentIds)
                                {
                                    WarehouseDocumentPositionQuantityManagement
                                        .UpdateQuantity(orderDocument, warehouseDs.GetPersonForSystemUser(userId).Id);

                                }
                            }
                            catch (Exception ex)
                            {
                                WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                            }
                        }

                        btnSaveDocument.Enabled = false;
                        RadComboBox_DocSubtypeSIMPLE.Enabled = false;
                        Label_TargetOrder.Text = "";

                        docChangedMaterials = new List<Tuple<int, int>>();
                        Session["INVChangedMaterials"] = docChangedMaterials;
                    }
                }
            }
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = WaitingOrdersRadGrid.SelectedItems.Count == 1 ? true : false;
        }

        protected void NewQuantityCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            double quantity;
            try
            {
                quantity = double.Parse(NewQuantityTextBox.Text.Replace(".", ","));
                args.IsValid = quantity > 0;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void WaitingOrdersRadGrid_SelectRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "RowClick")
            {
                var g = e.Item as GridDataItem;

                detailsPanel.Visible = true;
                WaitingOrdersRadGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                MaterialsToGiveRadGrid.Rebind();


                string orderNumber = g.GetCellValueAs<string>("OrderNumber");
                Label_TargetOrder.Text = "Do przypisania: " + orderNumber;

                btnPrintDocument.Enabled = false;
                this.btnSaveDocument.Enabled = true;
            }
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            //this.SignatureControl1.ClearSignatures();

            docChangedMaterials = new List<Tuple<int, int>>();
            Session["INVChangedMaterials"] = docChangedMaterials;

            //PrintButtonsPanel.Controls.Clear();
            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = false;
            RadComboBox_DocSubtypeSIMPLE.Enabled = true;

            WaitingOrdersRadGrid.SelectedIndexes.Clear();
            WaitingOrdersRadGrid.Rebind();
            MaterialsToGiveRadGrid.Rebind();

            this.positions = new List<PositionWithQuantityAndShelfs>();
            Session["InternalGiveOrderedMaterial_positionsToAdd"] = this.positions;

            Label_SourceZones.Text = "";
            Label_TargetOrder.Text = "";

            WaitingOrdersRadGrid.Enabled = true;
            MaterialsToGiveRadGrid.Enabled = true;
        }

        protected void MaterialsRCB_DueDateTime_OnLoad(object sender, EventArgs e)
        {

        }

        private void ShowChangeMaterialPanel()
        {
            //HideChangeQuantityPanel();

            WaitingOrdersRadGrid.Enabled = false;
            MaterialsToGiveRadGrid.Enabled = false;
            MaterialChangePanel.Visible = true;
        }

        private void ShowChangeQuantityPanel()
        {
            //HideChangeMaterialPanel();

            WaitingOrdersRadGrid.Enabled = false;
            MaterialsToGiveRadGrid.Enabled = false;
            QuantityChangePanel.Visible = true;
        }

        private void HideChangeMaterialPanel()
        {
            WaitingOrdersRadGrid.Enabled = true;
            MaterialsToGiveRadGrid.Enabled = true;
            MaterialChangePanel.Visible = false;
            HiddenField_QuickOp_ContainerID.Value = "";
            HiddenField_QuickOp_PositionId.Value = "";

            MaterialsToGiveRadGrid.Rebind();
        }

        private void HideChangeQuantityPanel()
        {
            WaitingOrdersRadGrid.Enabled = true;
            MaterialsToGiveRadGrid.Enabled = true;
            QuantityChangePanel.Visible = false;
            HiddenField_QuickOp_ContainerID.Value = "";
            HiddenField_QuickOp_PositionId.Value = "";

            NewQuantityTextBox.Text = "";

            MaterialsToGiveRadGrid.Rebind();
        }

        protected void ButtonChangeMaterialButtonClick(object sender, EventArgs e)
        {
            var gridTableRow = ((GridDataItem)(((RadButton)sender).BindingContainer));
            var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            var positionId = gridTableRow.GetCellValueAs<int>("WarehouseDocumentPositionId");//.Cells[3].Text; // PositionId
            //var materialIdCelltext = gridTableRow.Cells[3].Text; // PositionId
            //var quantityCelltext = gridTableRow.Cells[4].Text; // PositionId

            //int materialId = int.Parse(materialIdCelltext);

            HiddenField_QuickOp_PositionId.Value = positionId.ToString();
            //HiddenField_QuickOp_ContainerID.Value = detailTableView.UniqueID;

            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];


            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                WarehouseDocumentPosition warehouseDocumentPosition =
                        warehouseDs.GetWarehouseDocumentPositionById(positionId);


                int? orderId = warehouseDocumentPosition.OrderId;
                int? processIdpl = warehouseDocumentPosition.ProcessIdpl;
                double quantity = warehouseDocumentPosition.Quantity;

                var childMaterials = from m in warehouseDs.WarehouseEntitiesTest.Materials
                                     where m.ParentMaterialId == warehouseDocumentPosition.Material.Id
                                     select m;

                List<Tuple<int, double>> materialsToShow = new List<Tuple<int, double>>();
                List<int> materialIdsToShow = new List<int>();

                foreach (var material in childMaterials)
                {
                    var stockQ = (from s in warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments
                                  where
                                          s.WarehouseTargetId == currentWarehouseId
                                          && s.MaterialId == material.Id
                                          && s.OrderId == orderId
                                          && s.ProcessIdpl == processIdpl
                                          && s.Quantity.HasValue
                                  select s.Quantity);

                    double stockQVal = 0;
                    foreach (var q in stockQ)
                    {
                        if (q.HasValue)
                        {
                            stockQVal = stockQVal + q.Value;
                        }
                    }

                    if (stockQVal >= Math.Abs(quantity))
                    {
                        materialIdsToShow.Add(material.Id);
                        materialsToShow.Add(new Tuple<int, double>(material.Id, stockQVal));
                    }
                }
                var materialsExtractedInfo =
                        from m in childMaterials.ToList()
                        where materialIdsToShow.Contains(m.Id)
                        select new
                                             {
                                                 MaterialId = m.Id,
                                                 MaterialName = m.NameShort,
                                                 IndexSIMPLE = m.IndexSIMPLE,
                                                 AvailableQuantity = (from p in materialsToShow where p.Item1 == m.Id select p.Item2).FirstOrDefault()
                                             };

                MaterialsToChangeRadGrid.DataSource = materialsExtractedInfo.ToArray();
                MaterialsToChangeRadGrid.Rebind();

            }

            // TODO: add execute change action after selecting material

            // TODO: add position to session material changes

            // TODO: modify behavir of GiveFOrOrder save action when there is an entry in session positions

            // TODO: check changed material quantity before and after save

            //var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
            //var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
            //var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
            //var gridTableRow11 = gridTableRow.Cells[11].Text; // DepartmentId

            //int orderId = 0;
            //int shelfId = 0;
            //int processIdpl = 0;
            //int departmentId = 0;

            //double quantity = 0.0;

            //var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            //string keyRendered = detailTableView.ParentItem.KeyValues;
            //keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
            //keyRendered = keyRendered.Replace(",MaterialId:", ";");
            //keyRendered = keyRendered.Replace("\"", "");
            //keyRendered = keyRendered.Replace("}", "");

            //string[] ids = keyRendered.Split(';');
            //int warehouseId = int.Parse(ids[0]);

            var imageMark = gridTableRow.FindControl("ImageMark");
            imageMark.Visible = true;

            ShowChangeMaterialPanel();

        }

        //ButtonChangeQuantityButtonClick

        protected void ButtonChangeQuantityButtonClick(object sender, EventArgs e)
        {
            Contract.Requires(sender != null);

            HideChangeMaterialPanel();

            var gridTableRow = ((GridDataItem)(((RadButton)sender).BindingContainer));
            var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            int positionId = gridTableRow.GetCellValueAs<int>("WarehouseDocumentPositionId"); // .Cells[3].Text; // PositionId
            //var materialIdCelltext = gridTableRow.Cells[3].Text; // PositionId
            //var quantityCelltext = gridTableRow.Cells[4].Text; // PositionId

            //int materialId = int.Parse(materialIdCelltext);

            HiddenField_QuickOp_PositionId.Value = positionId.ToString();
            //HiddenField_QuickOp_ContainerID.Value = detailTableView.UniqueID;

            //int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];


            //using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            //{
            //    WarehouseDocumentPosition warehouseDocumentPosition =
            //        warehouseDs.GetWarehouseDocumentPositionById(positionId);


            //    int? orderId = warehouseDocumentPosition.OrderId;
            //    int? processIdpl = warehouseDocumentPosition.ProcessIdpl;
            //    double quantity = warehouseDocumentPosition.Quantity;

            //    //var childMaterials = from m in warehouseDs.WarehouseEntitiesTest.Material
            //    //                     where m.ParentMaterialId == warehouseDocumentPosition.Material.Id
            //    //                     select m;

            //    //List<Tuple<int, double>> materialsToShow = new List<Tuple<int, double>>();
            //    //List<int> materialIdsToShow = new List<int>();

            //    //foreach (var material in childMaterials)
            //    //{
            //    //    var stockQ = (from s in warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments
            //    //                  where
            //    //                      s.WarehouseTargetId == currentWarehouseId
            //    //                      && s.MaterialId == material.Id
            //    //                      && s.OrderId == orderId
            //    //                      && s.ProcessIdpl == processIdpl
            //    //                      && s.Quantity.HasValue
            //    //                  select s.Quantity);

            //    //    double stockQVal = 0;
            //    //    foreach (var q in stockQ)
            //    //    {
            //    //        if (q.HasValue)
            //    //        {
            //    //            stockQVal = stockQVal + q.Value;
            //    //        }
            //    //    }

            //    //    if (stockQVal >= Math.Abs(quantity))
            //    //    {
            //    //        materialIdsToShow.Add(material.Id);
            //    //        materialsToShow.Add(new Tuple<int, double>(material.Id, stockQVal));
            //    //    }
            //    //}
            //    //var materialsExtractedInfo =
            //    //    from m in childMaterials.ToList()
            //    //    where materialIdsToShow.Contains(m.Id)
            //    //    select new
            //    //    {
            //    //        MaterialId = m.Id,
            //    //        MaterialName = m.NameShort,
            //    //        IndexSIMPLE = m.IndexSIMPLE,
            //    //        AvailableQuantity = (from p in materialsToShow where p.Item1 == m.Id select p.Item2).FirstOrDefault()
            //    //    };

            //    //MaterialsToChangeRadGrid.DataSource = materialsExtractedInfo.ToArray();
            //    //MaterialsToChangeRadGrid.Rebind();

            //}

            var imageMark = gridTableRow.FindControl("ImageMark");
            imageMark.Visible = true;

            ShowChangeQuantityPanel();

        }

        protected void ButtonChangeMaterialExecuteClick(object sender, EventArgs e)
        {
            Page.Validate("ChangeMaterialGroup");

            if (IsValid)
            {
                if (MaterialsToChangeRadGrid.SelectedItems.Count == 0)
                {
                    WebTools.AlertMsgAdd("Brak zaznaczonych pozycji.");
                    return;
                }

                var gridTableRow = (GridDataItem)MaterialsToChangeRadGrid.SelectedItems[0];
                var changedMaterialId = gridTableRow.GetCellValueAs<int>("MaterialId"); // .Cells[2].Text; // PositionId

                int positionId = int.Parse(HiddenField_QuickOp_PositionId.Value);
                docChangedMaterials.RemoveAll(chm => chm.Item1 == positionId);

                docChangedMaterials.Add(new Tuple<int, int>(positionId, changedMaterialId));

                //var x = 1;
                // MaterialsToChangeRadGrid

                //var gridTableRowMaterial = from p in MaterialsToGiveRadGrid.Items where p.

                //var imageMark = gridTableRow.FindControl("ImageMarkSaved");
                //imageMark.Visible = true;


                HideChangeMaterialPanel();
            }
        }

        protected void ButtonCancelMaterialClick(object sender, EventArgs e)
        {
            int userId = 0;
            int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId);
            
            var gridTableRow = ((GridDataItem)(((RadButton)sender).BindingContainer));
            var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            int positionId = gridTableRow.GetCellValueAs<int>("WarehouseDocumentPositionId");

            try
            {
                using (var warehouseDS = new WarehouseDS(SessionData.ClientData))
                {
                    warehouseDS.CancelInternalGiveDocument(positionId);
                }
                
                WaitingOrdersRadGrid.Rebind();
                MaterialsToGiveRadGrid.Rebind();

                btnSaveDocument.Enabled = false;
                RadComboBox_DocSubtypeSIMPLE.Enabled = false;
                Label_TargetOrder.Text = "";
                WebTools.AlertMsgAdd("Dokument został anulowany.");
            }
            catch (Exception ex)
            {
                WebTools.AlertMsgAdd(ex, userId);
            }
        }

        protected void ButtonChangeQuantityExecuteClick(object sender, EventArgs e)
        {
            Page.Validate("ChangeQuantityGroup");

            if (IsValid)
            {
                double newQuantity = double.Parse(NewQuantityTextBox.Text);
                int userId = 0;
                int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId);
                int positionId = int.Parse(HiddenField_QuickOp_PositionId.Value);

                try
                {
                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        warehouseDs.ChangeWarehouseDocumentPositionQuantity(positionId, newQuantity, warehouseDs.GetPersonForSystemUser(userId).Id);
                    }
                }
                catch (Exception ex)
                {
                    WebTools.AlertMsgAdd(ex, userId);
                }
                HideChangeQuantityPanel();
            }
        }

        protected void ButtonCancelClick(object sender, EventArgs e)
        {
            //var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_ContainerID.Value);
            //GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
            //detailTableView.Rebind();
            HideChangeMaterialPanel();
            HideChangeQuantityPanel();
        }

        protected void MaterialsToGiveRadGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

        }
    }
}
