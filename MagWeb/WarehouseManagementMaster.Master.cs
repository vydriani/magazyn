﻿using System;
using System.Web.UI;
//using Client.Domain.Logging;

namespace MagWeb
{
	public partial class WarehouseManagementMaster : MasterPage
	{
    //PerformanceDebugger perfDebug = new PerformanceDebugger();

		protected void Page_Load(object sender, EventArgs e)
		{
      //perfDebug.Restart();
			object loggedUserHasAdminPrivilegesForConfig = Session["LoggedUserHasAdminPrivilegesForConfig"];

			if (!(bool)loggedUserHasAdminPrivilegesForConfig)
			{
				//TODO do this with Membership roles
				Response.Redirect("Login.aspx");
			}

			if (Session["tab_warehouse_mgmt"] == null)
			{
				RadToolBar1.Tabs[0].Selected = true;
				Session["tab_warehouse_mgmt"] = 0;
				Response.Redirect(RadToolBar1.Tabs[0].Target);
			}
      //perfDebug.LogPerformance();
		}

		protected void Tabs_OnClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
		{
			Session["tab_warehouse_mgmt"] = RadToolBar1.SelectedIndex;

			Response.Redirect(e.Tab.Target);
		}

		//
		protected void Button_Back_Click(object sender, EventArgs e)
		{
			//Response.Redirect("ChooseWarehouse.aspx");
		}

		protected void Tab_Prerender(object sender, EventArgs e)
		{
			RadToolBar1.Tabs[(int)Session["tab_warehouse_mgmt"]].Selected = true;
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
      //perfDebug.Restart();
			if (WebTools.AlertMsgIsAlert())
			{
				this.Panel_Alert.Visible = true;
				this.Label_Alert.Text = WebTools.AlertMsgRead();
			}
			else
			{
				this.Panel_Alert.Visible = false;
				this.Label_Alert.Text = "";
			}
      //perfDebug.LogPerformance();
		}
	}
}
