﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class ManageGroups : FeaturedSystemWebUIPage
    {

        private const int WAREHOUSE_ID_IN_GRID = 2;
        private const int TARGET_WAREHOUSE_ID_IN_GRID = 2;
        private const int PERMISSION_ID_IN_GRID = 2;
        private const int DOCUMENT_TYPE_ID_IN_GRID = 3;

        private const int Group_ID_IN_GRID = 2;
        private const int Group_NAME_IN_GRID = 3;
        private const int PRIVILEGE_ID_IN_GRID = 2;

        private const int PRIVILEGE_TO_READ_ID_IN_GRID = 2;
        private const int WAREHOUSE_TO_READ_ID_IN_GRID = 3;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    CompanyRCB.DataSource = from w in warehouseDs.GetAllCompanies()
                                            select new { Value = w.Id, Text = w.Name };
                    CompanyRCB.DataTextField = "Text";
                    CompanyRCB.DataValueField = "Value";
                    CompanyRCB.DataBind();
                    CompanyRCB.Items.Insert(0, new RadComboBoxItem("(wybierz firmę)", "-1"));
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.GroupsGrid.SelectedItems.Count == 1)
            {
                //Group_NAME_IN_GRID
                //int GroupId = int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text);
                this.Label_SystemGroup.Text = "Wybrany użytkownik: " + this.GroupsGrid.SelectedItems[0].Cells[Group_NAME_IN_GRID].Text + " (" + this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text + ")";


                //using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
                //{
                //    int systemGroupId = int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text);
                //    SystemGroupSettings sus = warehouseDS.GetSystemGroupSetting("SessionTime", systemGroupId);
                //    this.TextBox_SessionTimeLimit.Text = (sus == null ? "" : sus.SettingValue);

                //    this.TextBox_SessionTimeLimit.Enabled = true;
                //    this.Button_SetSessionTimeLimit.Enabled = true;


                //    //this.Label_SystemGroup.Text = "!!!!!!!!" + Context.Group.Identity.Name;
                //}


            }
            else
            {
                this.Label_SystemGroup.Text = "Nie wybrano użytkownika";
                //this.TextBox_SessionTimeLimit.Text = "";
                //this.TextBox_SessionTimeLimit.Enabled = false;
                //this.Button_SetSessionTimeLimit.Enabled = false;
            }
        }



        protected void GroupsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var GroupsCandidatesToConsider = warehouseDs.GetAllSystemGroups();
                var GroupsCandidatesToConsiderOrdered = GroupsCandidatesToConsider.OrderBy(p => p.Name);


                List<Mag.Domain.Model.SystemGroup> GroupsCandidatesToPresent = new List<SystemGroup>();


                foreach (var Group in GroupsCandidatesToConsiderOrdered)
                {
                    Mag.Domain.Model.SystemGroup waitingForGroupToPresent = new Mag.Domain.Model.SystemGroup();
                    //Mag.Domain.Model.Person waitingForGroupToPresent_Person = new Mag.Domain.Model.Person();
                    //waitingForGroupToPresent.Person = waitingForGroupToPresent_Person;

                    waitingForGroupToPresent.Id = Group.Id;
                    waitingForGroupToPresent.Name = Group.Name;
                    //waitingForGroupToPresent.Person.FirstName = Group.Person.FirstName;
                    //waitingForGroupToPresent.Person.LastName = Group.Person.LastName;

                    GroupsCandidatesToPresent.Add(waitingForGroupToPresent);
                }


                var GroupsCandidatesToPresentMaterialized = GroupsCandidatesToPresent.ToArray();

                var Groups = from p in GroupsCandidatesToPresentMaterialized
                            select
                                new
                                {
                                    GroupName = p.Name,
                                    //FullName = p.Person.LastName + " " + p.Person.FirstName,
                                    GroupID = p.Id
                                };

                GroupsGrid.DataSource = Groups.OrderBy(p => p.GroupName);
            }
        }

        protected void GroupsGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                WarehousesGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;

                WarehouseToReadGrid.Rebind();
                WarehousesGrid.Rebind();
                TargetWarehousesGrid.Rebind();
                DocumentTypeGrid.Rebind();
            }
        }



        protected void CompanyRCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            WarehouseToReadGrid.Rebind();
            WarehousesGrid.Rebind();
            TargetWarehousesGrid.Rebind();
            DocumentTypeGrid.Rebind();
        }

        protected void WarehousesToReadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.GroupsGrid.SelectedItems.Count == 1)
            {
                int GroupId = int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text);

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int company = int.Parse(CompanyRCB.SelectedValue); // Company selection from combo box

                    var warehouses = warehouseDs.GetAllWarehouses().Where(p => p.Company.Id == company);

                    List<WarehousesAndSecurityPrivileges> warehousesAndSecurityPrivileges = new List<WarehousesAndSecurityPrivileges>();

                    foreach (Mag.Domain.Model.Warehouse warehouse in warehouses)
                    {
                        WarehousesAndSecurityPrivileges warehouseAndSecurityPrivileges = new WarehousesAndSecurityPrivileges();

                        warehouseAndSecurityPrivileges.IdPrivilege = warehouseDs.GetWarehouseGroupPrivilegeForRead(warehouse.Id, GroupId);
                        warehouseAndSecurityPrivileges.IdWarehouse = warehouse.Id;
                        warehouseAndSecurityPrivileges.ShortName = warehouse.ShortName;
                        warehouseAndSecurityPrivileges.Name = warehouse.Name;

                        warehousesAndSecurityPrivileges.Add(warehouseAndSecurityPrivileges);
                    }


                    //var warehousesMaterialized = warehouses.ToArray();

                    var warehousesWithPrivileges = from p in warehousesAndSecurityPrivileges
                                                   select
                                                       new
                                                       {
                                                           PrivilegeID = p.IdPrivilege,
                                                           WarehouseID = p.IdWarehouse,
                                                           WarehouseShortName = p.ShortName,
                                                           WarehouseName = p.Name,
                                                           GrantedToGroup = (p.IdPrivilege != null ? true : false)
                                                       };

                    this.WarehouseToReadGrid.DataSource = warehousesWithPrivileges;
                }
            }
        }

        protected void WarehousesToReadGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    string warehouseToReadIDStr = e.Item.Cells[WAREHOUSE_TO_READ_ID_IN_GRID].Text;

                    string test1, test2, test3;
                    test1 = e.Item.Cells[1].Text;
                    test2 = e.Item.Cells[2].Text;
                    test3 = e.Item.Cells[3].Text;

                    int warehouseId = int.Parse(warehouseToReadIDStr);
                    int systemGroupId = int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text);

                    TableCellCollection tcl = e.Item.Cells;
                    int privilegeId = -1;
                    string privilegeIdStr = e.Item.Cells[PRIVILEGE_TO_READ_ID_IN_GRID].Text;

                    if (privilegeIdStr != "&nbsp;" && privilegeIdStr != "")
                    {
                        privilegeId = int.Parse(e.Item.Cells[PRIVILEGE_TO_READ_ID_IN_GRID].Text);
                    }

                    if (privilegeId == -1 || !warehouseDs.SecurityCheckGroupPriviledgeForReadById(privilegeId))
                    {
                        // Adding privilege
                        warehouseDs.SecurityAddGroupPriviledgeForRead(warehouseId, systemGroupId);
                    }
                    else
                    {
                        // Removing privilege
                        warehouseDs.SecurityRemoveGroupPriviledgeForRead(privilegeId);
                    }
                }


                WarehousesGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                WarehouseToReadGrid.Rebind();
            }
        }

        protected void WarehousesGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.GroupsGrid.SelectedItems.Count == 1 && int.Parse(this.CompanyRCB.SelectedValue) > 0)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int company = int.Parse(CompanyRCB.SelectedValue); // Company selection from combo box

                    var warehousesCandidatesToConsider = warehouseDs.GetAllWarehouses(company, true, false);

                    var warehousesCandidatesToConsiderOrdered = warehousesCandidatesToConsider.OrderBy(p => p.Name);

                    List<Mag.Domain.Model.Warehouse> warehousesCandidatesToPresent = new List<Mag.Domain.Model.Warehouse>();

                    foreach (var warehouse in warehousesCandidatesToConsiderOrdered)
                    {
                        Mag.Domain.Model.Warehouse waitingForWarehouseToPresent = new Mag.Domain.Model.Warehouse();

                        waitingForWarehouseToPresent.Id = warehouse.Id;
                        waitingForWarehouseToPresent.Name = warehouse.Name;
                        waitingForWarehouseToPresent.ShortName = warehouse.ShortName;

                        warehousesCandidatesToPresent.Add(waitingForWarehouseToPresent);

                    }

                    if (company != -1)
                    {
                        Mag.Domain.Model.Warehouse warehouseNull = new Mag.Domain.Model.Warehouse();
                        warehouseNull.Id = -1;
                        warehouseNull.Name = "(nieokreślony magazyn)";
                        warehouseNull.ShortName = "(nieokreślony magazyn)";
                        warehousesCandidatesToPresent.Insert(0, warehouseNull);
                    }

                    var warehousesCandidatesToPresentMaterialized = warehousesCandidatesToPresent.ToArray();

                    var warehouses = from p in warehousesCandidatesToPresentMaterialized
                                     select
                                         new
                                         {
                                             WarehouseName = p.Name,
                                             WarehouseShortName = p.ShortName,
                                             WarehouseID = p.Id
                                         };

                    WarehousesGrid.DataSource = warehouses;
                }
            }
        }

        protected void WarehousesGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                WarehousesGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                DocumentTypeGrid.Rebind();
            }
        }



        protected void TargetWarehousesGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.GroupsGrid.SelectedItems.Count == 1 && int.Parse(this.CompanyRCB.SelectedValue) > 0)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int company = int.Parse(CompanyRCB.SelectedValue);

                    var warehouses = warehouseDs.GetAllWarehouses(company, true, false).ToList();

                    if (company != -1)
                    {
                        Mag.Domain.Model.Warehouse warehouseNull = new Mag.Domain.Model.Warehouse();
                        warehouseNull.Id = -1;
                        warehouseNull.Name = "(nieokreślony magazyn)";
                        warehouseNull.ShortName = "(nieokreślony magazyn)";
                        warehouses.Insert(0, warehouseNull);
                    }

                    TargetWarehousesGrid.DataSource = warehouses;
                }
            }
        }

        protected void TargetWarehousesGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                TargetWarehousesGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                DocumentTypeGrid.Rebind();
            }
        }

        // ATTENTION: When some permission gets deleted, it will not be longer listed here, so we need to write a special code to add it
        // and probably fill with i.e. red color
        protected void DocumentTypeGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.GroupsGrid.SelectedItems.Count == 1 && int.Parse(this.CompanyRCB.SelectedValue) > 0)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int? sourceWarehouseId = -1;
                    if (WarehousesGrid.SelectedItems.Count == 1)
                    {
                        sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
                        sourceWarehouseId = sourceWarehouseId != -1 ? sourceWarehouseId : null;
                    }

                    int? targetWarehouseId = -1;
                    if (TargetWarehousesGrid.SelectedItems.Count == 1)
                    {
                        targetWarehouseId = int.Parse(TargetWarehousesGrid.SelectedItems[0].Cells[TARGET_WAREHOUSE_ID_IN_GRID].Text);
                        targetWarehouseId = targetWarehouseId != -1 ? targetWarehouseId : null;
                    }

                    int GroupId = int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text);
                    int? GroupId_nullable = GroupId;


                    var permissionsCandidates = warehouseDs.GetJoinedWarehousePermissions((sourceWarehouseId > 0 ? sourceWarehouseId : null), (targetWarehouseId > 0 ? targetWarehouseId : null)); //.GetWarehousePermissions(); //.GetWarehousePermissionsAndSecurityPriviledges(sourceWarehouseId, targetWarehouseId, GroupId);




                    List<WarehousePermissionVirtual> permissionsCandidatesMaterialized = new List<WarehousePermissionVirtual>();
                    foreach (var permission in permissionsCandidates)
                    {
                        if (((permission.WarehouseId == null && sourceWarehouseId == null) || (permission.WarehouseId != null && permission.WarehouseId == sourceWarehouseId)) && ((permission.Warehouse1Id == null && targetWarehouseId == null) || (permission.Warehouse1Id != null && permission.Warehouse1Id == targetWarehouseId)))
                        {
                            permissionsCandidatesMaterialized.Add(permission);
                        }
                    }

                    List<WarehousePermissionsAndSecurityGroupPriviledges> permissionsAndPrivilegesCandidatesMaterializedSelected = new List<WarehousePermissionsAndSecurityGroupPriviledges>();

                    foreach (var permission in permissionsCandidatesMaterialized)
                    {
                        WarehousePermissionsAndSecurityGroupPriviledges permissionAndSecurityPrivilege = new WarehousePermissionsAndSecurityGroupPriviledges();

                        permissionAndSecurityPrivilege.FromWarehouseId = null;
                        permissionAndSecurityPrivilege.ToWarehouseId = null;

                        if (permission.Warehouse != null)
                        {
                            permissionAndSecurityPrivilege.FromWarehouseId = permission.Warehouse.Id;
                        }

                        if (permission.Warehouse1 != null)
                        {
                            permissionAndSecurityPrivilege.ToWarehouseId = permission.Warehouse1.Id;
                        }

                        permissionAndSecurityPrivilege.FromWarehouseShortName = (permission.Warehouse != null ? permission.Warehouse.ShortName : "");
                        permissionAndSecurityPrivilege.ToWarehouseShortName = (permission.Warehouse1 != null ? permission.Warehouse1.ShortName : "");

                        permissionAndSecurityPrivilege.OperationId = permission.WarehouseDocumentType.Id;

                        //permissionAndSecurityPrivilege.IdPermission = permission.Id;

                        permissionAndSecurityPrivilege.IdType = permission.WarehouseDocumentType.Id;
                        permissionAndSecurityPrivilege.Type = permission.WarehouseDocumentType.Type;
                        permissionAndSecurityPrivilege.ShortType = permission.WarehouseDocumentType.ShortType;

                        permissionAndSecurityPrivilege.SystemGroupId = GroupId;
                        permissionAndSecurityPrivilege.IdPrivilege = warehouseDs.GetWarehouseGroupPrivilegeForExecutionId(sourceWarehouseId, targetWarehouseId, permission.WarehouseDocumentType.Id, GroupId);

                        permissionsAndPrivilegesCandidatesMaterializedSelected.Add(permissionAndSecurityPrivilege);

                    }



                    var permissions =
                        from p in permissionsAndPrivilegesCandidatesMaterializedSelected
                        select new
                        {
                            SystemGroupId = p.SystemGroupId,

                            PrivilegeId = p.IdPrivilege,
                            TypeId = p.IdType,
                            ShortType = p.ShortType,
                            Type = p.Type,
                            SourceWarehouse = p.FromWarehouseShortName,
                            TargetWarehouse = p.ToWarehouseShortName,
                            GrantedToGroup = p.IdPrivilege != null

                        };
                    DocumentTypeGrid.DataSource = permissions.ToArray(); // OrderBy(p => p.TypeId).ToArray();
                }
            }
        }

        protected void DocumentTypeGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {

                this.DocumentTypeGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int? sourceWarehouseId = -1;
                    if (WarehousesGrid.SelectedItems.Count == 1)
                    {
                        sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
                        sourceWarehouseId = sourceWarehouseId != -1 ? sourceWarehouseId : null;
                    }

                    int? targetWarehouseId = -1;
                    if (TargetWarehousesGrid.SelectedItems.Count == 1)
                    {
                        targetWarehouseId = int.Parse(TargetWarehousesGrid.SelectedItems[0].Cells[TARGET_WAREHOUSE_ID_IN_GRID].Text);
                        targetWarehouseId = targetWarehouseId != -1 ? targetWarehouseId : null;
                    }

                    int privilegeId = -1;
                    string privilegeIdStr = e.Item.Cells[PRIVILEGE_ID_IN_GRID].Text;

                    if (privilegeIdStr != "&nbsp;" && privilegeIdStr != "")
                    {
                        privilegeId = int.Parse(e.Item.Cells[PRIVILEGE_ID_IN_GRID].Text);
                    }

                    if (privilegeId == -1 || !warehouseDs.SecurityCheckGroupPriviledgeForExecutionById(privilegeId))
                    {
                        // Adding privilege
                        warehouseDs.SecurityAddGroupPriviledgeForExecution(sourceWarehouseId, targetWarehouseId, int.Parse(e.Item.Cells[DOCUMENT_TYPE_ID_IN_GRID].Text), int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text));
                    }
                    else
                    {
                        // Removing privilege
                        warehouseDs.SecurityRemoveGroupPriviledgeForExecution(privilegeId);
                    }
                }
                DocumentTypeGrid.Rebind();
            }
        }

        protected void Button_AddGroup_Click(object sender, EventArgs e)
        {
            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {
                try
                {
                    warehouseDS.CreateSystemGroup(this.TextBox_AddGroup.Text);

                    this.TextBox_AddGroup.Text = "";
                    this.GroupsGrid.Rebind();
                }
                catch (Exception ex)
                {
                    WebTools.AlertMsgAdd(ex.ToString());
                }
            }
        }

        //protected void Button_SetSessionTimeLimit_Click(object sender, EventArgs e)
        //{
        //    if (IsValid)
        //    {
        //        if (this.GroupsGrid.SelectedItems.Count == 1)
        //        {
        //            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
        //            {
        //                int systemGroupId = int.Parse(this.GroupsGrid.SelectedItems[0].Cells[Group_ID_IN_GRID].Text);
        //                warehouseDS.SetSystemGroupSetting("SessionTime", this.TextBox_SessionTimeLimit.Text, systemGroupId);
        //            }
        //        }
        //    }
        //}

        //protected void SessionTimeValidator_ServerValidate(object source, ServerValidateEventArgs e)
        //{
        //    int value = -1;
        //    try
        //    {
        //        value = int.Parse(this.TextBox_SessionTimeLimit.Text);
        //    }
        //    catch
        //    {
        //        e.IsValid = false;
        //        return;
        //    }
        //    if (value < 1 || value > 1440) // THESE PARAMETERS ALSO SHOULD BE GOT FROM CONFIG FILE
        //    {
        //        e.IsValid = false;
        //        return;
        //    }
        //    e.IsValid = true;
        //}
    }
}
