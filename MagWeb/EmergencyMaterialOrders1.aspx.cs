﻿using System;
using System.Linq;
using System.Web.UI;
using Mag.Domain;
using Telerik.Web.UI;

namespace MagWeb
{
  public partial class EmergencyMaterialOrders1 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        /*int? companyId = (int?)Session[SessionData.SessionKey_CurrentCompanyId];
        if (companyId.HasValue)
        {
            materialsRCB.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
        }*/
      }
    }

    private void ordersRCB_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
      var orderNumber = ordersRCB.SelectedItem != null ? ordersRCB.SelectedItem.Text : null;
      if (!String.IsNullOrEmpty(orderNumber))
      {
        int companyId = (!orderNumber.Contains('M') && !orderNumber.Contains('E'))
                            ? 1 //  Willson
                            : (!orderNumber.Contains('E'))
                                  ? 2   //  Mold
                                  : 3;  //  Metal
        materialsRCB.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
      }
    }

    protected void EmergencyMaterialOrders1Grid_OnNeededDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        int orderId = 0;
        int.TryParse(ordersRCB.SelectedValue, out orderId);

        int materialId = 0;
        int.TryParse(materialsRCB.SelectedValue, out materialId);

        int personId = 0;
        int.TryParse(personsRCB.SelectedValue, out personId);

        if (orderId > 0)
        {
          var warehouseDocumentPositions = warehouseDs
              .GetEmergencyMaterialOrders
              (
                orderId,
                (materialId > 0
                      ? (int?)materialId
                      : null),
                (personId > 0
                      ? (int?)personId
                      : null),
                null
              )
              .Select((o, i) => new
              {
                Number = i,
                IndexSIMPLE = o.Material.IndexSIMPLE,
                MaterialName = o.Material.Name,
                QuantityAbs = Math.Abs(o.Quantity),
                UnitShortName = o.Material.Unit.ShortName,
                OrderNumber = o.Order.OrderNumber,
                WarehouseDocumentTimeStamp = o.WarehouseDocument.TimeStamp,
                PersonName = o.WarehouseDocument.Person.PersonalId,
                Technologist = o.Order.Productions.First().Technologist
              })
              .OrderByDescending(p => p.Number)//.Id)
              .ToList();

          //DataField="Number"           
          //DataField="Material.IndexSIMPLE"
          //DataField="Material.Name"
          //DataField="QuantityAbs"
          //DataField="Material.Unit.ShortName"
          //DataField="Order.OrderNumber"
          //DataField="WarehouseDocument.TimeStamp"
          //DataField="WarehouseDocument.Person.PersonName"
          //DataField="Order.Productions[0].Technologist"

          //int i = 1;
          //foreach (var warehouseDocumentPosition in warehouseDocumentPositions)
          //{
          //    warehouseDocumentPosition.Number = i;
          //    i++;
          //}

          EmergencyMaterialOrders1Grid.DataSource = warehouseDocumentPositions;

          /*warehouseDs.GetEmergencyMaterialOrdersAsync(orderId,
                                                      (materialId > 0 ? (int?) materialId : null),
                                                      o =>
                                                          {
                                                              if (!o.HasError)
                                                              {
                                                                  EmergencyMaterialOrders1Grid.DataSource =
                                                                      o.Results.ToList();
                                                              }
                                                          });*/
        }
      }
    }

    /*public IEnumerable<Client.Domain.WarehouseDocumentPosition> GetEmergencyMaterialOrders(int orderId, int? materialId, int? personId, string technologistPersonName)
    {
        var query = clientEntities.WarehouseDocumentPositions
            .AddIncludePaths("WarehouseDocument.Person",
                             "Material.Unit",
                             "Order.Productions")
            .Where(p => p.OrderId == orderId &&
                        (materialId == null || p.MaterialId == materialId) &&
                        (personId == null || p.WarehouseDocument.AuthorId == personId) &&
                        (technologistPersonName == null || p.Order.Productions.Any(t => t.Technologist == technologistPersonName)) &&
                        p.WarehouseDocument.DocumentTypeId == 10 &&
                        p.WarehouseDocument.DocumentSubtypeId == 3);
        query.QueryStrategy = QueryStrategy.DataSourceOnly;
        //query.ExecuteAsync(getEmergencyMaterialOrders1Completed);
        return query.Execute();
    }*/

    protected void EmergencyMaterialOrders1Grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
      if (e.Item is GridDataItem)
      {
      }
    }

    protected void Button_ApplyFilterClick(object sender, EventArgs e)
    {
      EmergencyMaterialOrders1Grid.Rebind();
    }

    protected void ButtonReloadClick(object sender, EventArgs e)
    {
      Response.Redirect(Request.RawUrl);
    }
  }
}