﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;
using System.Diagnostics.Contracts;

namespace MagWeb
{
    public partial class CustomsChamberReturns : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["tab_Reports"] = 2;
        }

        protected void CustomsChamberMaterialsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];

            using (var warehouseDs = new WarehouseDS())
            {
                var ccPoss = warehouseDs.GetCustomsChamberReturnedPositions(currentWarehouseId);
                int i = 1;
                CustomsChamberMaterialReturnsGrid.DataSource = from p in ccPoss
                                                         select new
                                                         {
                                                             Lp = i++,
                                                             KCSolvingDocumentId = p.KCSolvingDocumentId,
                                                             WarehouseDocumentPositionId = p.WarehouseDocumentPositionId,
                                                             OrderId = p.OrderId,
                                                             ContractingPartyName = p.ContractingPartyName,
                                                             OrderNumberFromPZ = p.OrderNumberFromPZ,
                                                             IndexSIMPLE = p.IndexSIMPLE,
                                                             MaterialName = p.MaterialName,
                                                             QuantityOrdered = p.QuantityOrdered,
                                                             KCQuantity = p.KCQuantity,
                                                             UnitName = p.UnitShortName,
                                                             OrderNumber = "stan magazynowy", //p.OrderNumber,
                                                             ShelfName = p.ShelfName,
                                                             KCSolvingDocumentDate = p.KCSolvingDocumentDate,
                                                             KCSolvingSIMPLEDocNumber = p.KCSolvingSIMPLEDocNumber + (p.KCSolvingDocumentId.HasValue ? "//" + p.KCSolvingDocumentId.ToString() : "")
                                                         };
            }
        }

        protected void Button_Print_OnLoad(object sender, EventArgs e)
        {
          Contract.Requires(sender != null);

            var gridTableRow = ((GridTableRow)(((Button)sender).BindingContainer)); //GridDataItem

            var cell0Text = gridTableRow.Cells[0].Text;
            var cell1Text = gridTableRow.Cells[1].Text;
            var cell2Text = gridTableRow.Cells[2].Text;
            var cell3Text = gridTableRow.Cells[3].Text;
            var cell4Text = gridTableRow.Cells[4].Text;
            var cell5Text = gridTableRow.Cells[5].Text;
            //var sovingDocIdCellText = gridTableRow.Cells[11].Text;

            int solvingDocId = int.Parse(cell2Text);


            DocumentPrintingUtils.BindDocumentToButton((Button)sender,
                                                       WarehouseDocumentTypeEnum.ZTD,
                                                       solvingDocId);
        }

        //protected void Button_Print_Click(object sender, EventArgs e)
        //{
        //    var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer)); //GridDataItem

        //    var cell0Text = gridTableRow.Cells[0].Text;
        //    var cell1Text = gridTableRow.Cells[1].Text;
        //    var cell2Text = gridTableRow.Cells[2].Text;\
        //    var cell3Text = gridTableRow.Cells[3].Text;
        //    var cell4Text = gridTableRow.Cells[4].Text;
        //    var cell5Text = gridTableRow.Cells[5].Text;
        //    //var sovingDocIdCellText = gridTableRow.Cells[11].Text;

        //    int solvingDocId = int.Parse(cell3Text);


        //    DocumentPrintingUtils.BindDocumentToButton((Button)sender,
        //                                               WarehouseDocumentTypeEnum.ZTD,
        //                                               warehouseDocument.Id);


        //    //RadDatePicker dateTimePicker = (RadDatePicker)Page.FindControl(datepickeruniqueidcellText);



        //    //var tableRowControls = gridTableRow.Controls;

        //    //foreach (var control in tableRowControls)
        //    //{
        //    //    var item = ((GridTableCell) control).Item;
        //    //    if(item.HasChildItems)
        //    //    {

        //    //    }

        //    //    var controlType = control.GetType();
        //    //    if(controlType == typeof(RadDateTimePicker))
        //    //    {
        //    //        dateTimePicker = (RadDateTimePicker)control;
        //    //    }
        //    //}

        //    //RadDateTimePicker dateTimePicker = (RadDateTimePicker) sender;


        //    // Update KCState basing on Id

        //}
    }
}