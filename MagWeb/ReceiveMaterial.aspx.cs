namespace MagWeb
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Transactions;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.Services;
    using Mag.Domain;
    using Mag.Domain.Model;
    using MagWeb.Extensions;
    using MagWeb.WarehouseDocuments;
    using Telerik.Web.UI;
    using System.Diagnostics.Contracts;

    public partial class ReceiveMaterial : FeaturedSystemWebUIPage
    {
        protected const string unreadPattern = @"\(\d+\)";
        private const string materialGrid_CONTRACTING_PART_ID_UniqueName = "ContractingPartyId";
        private const string materialGrid_FOR_DEPARTMENT_UniqueName = "ForDepartment";
        private const string materialGrid_ID_SIM_UniqueName = "IdSim";
        private const string materialGrid_ID_ZF_UniqueName = "IdZF";
        private const string materialGrid_Flaga_UniqueName = "Flag";
        private const string materialGrid_ORDER_NUMBER_PZ_UniqueName = "OrderNumberPZ";
        private const string materialGrid_PROCESS_HEADER_ID_UniqueName = "processHeaderId";
        private const string materialGrid_PROCESS_IDDPL_UniqueName = "processIDDPL";
        private const string materialGrid_PROCESS_IDPL_UniqueName = "processIDPL";
        private const string materialGrid_Quantity_UniqueName = "Quantity";
        private const string materialGrid_SIMPLE_DEPARTMENT_ID_UniqueName = "SimpleDepartmentId";
        private const string materialGrid_SOURCE_TYPE_UniqueName = "SourceType";
        private const string materialGrid_UNIT_PRICE_UniqueName = "unitPrice";
        private int _companyId = -1;

        //private List<PositionWithQuantityAndShelfs> positionWithQuantityAndShelfsesOverQuantity;
        private int? _contractingParty;

        private string _orderNumberPZ;
        private string _pageName;
        private List<PositionWithQuantityAndShelfs> _positionWithQuantityAndShelfsCollection;

        public int ContractingPartyId { get; set; }

        protected void AddPositionClick(object sender, EventArgs e)
        {
            //WarehouseDocumentPosition warehouseDocumentPosition;

            Page.Validate("AddPositionGroup"); //AddPositionGroupMaterialChoice

            if (IsValid)
            {
                var item = GetSelectedMaterialGridDataItem();

                int newProcessHeaderId = item.GetCellValueAs<int>(materialGrid_PROCESS_HEADER_ID_UniqueName); //int.Parse(item.Cells[INDEX_PROCESS_HEADER_ID_IN_GRID].Text);
                int newProcessIDPL = item.GetCellValueAs<int>(materialGrid_PROCESS_IDPL_UniqueName);// int.Parse(item.Cells[INDEX_PROCESS_IDPL_IN_GRID].Text);
                int newProcessIDDPL = item.GetCellValueAs<int>(materialGrid_PROCESS_IDDPL_UniqueName); //int.Parse(item.Cells[INDEX_PROCESS_IDDPL_IN_GRID].Text);
                decimal newValuePerUnit = item.GetCellValueAs<decimal>(materialGrid_UNIT_PRICE_UniqueName); // decimal.Parse(item.Cells[INDEX_UNIT_PRICE_IN_GRID].Text);
                int newContractingPartySimple = item.GetCellValueAs<int>(materialGrid_CONTRACTING_PART_ID_UniqueName);// int.Parse(item.Cells[INDEX_CONTRACTING_PARTY_ID_IN_GRID].Text);       //sprawdzić czy się nie zmieniło,, jeśli stare różne od nowego to error - sprawdzic czy nie puste

                string newOrderNumberPZ = item.GetCellValueAs<string>(materialGrid_ORDER_NUMBER_PZ_UniqueName);// item.Cells[INDEX_ORDERNUMBERPZ_IN_GRID].Text; //sprawdzić czy nie puste
                bool newForDepartment = item.GetCellValueAs<bool>(materialGrid_FOR_DEPARTMENT_UniqueName);// int.Parse(item.Cells[INDEX_FOR_DEPARTMENT_IN_GRID].Text) == 0 ? false : true;

                int newIdSim = item.GetCellValueAs<int>(materialGrid_ID_SIM_UniqueName);// int.Parse(item.Cells[INDEX_IDSIM_IN_GRID].Text);
                var materialFlag = item.GetCellValueAs<int>(materialGrid_Flaga_UniqueName);
                int sourceType = item.GetCellValueAs<int>(materialGrid_SOURCE_TYPE_UniqueName);// int.Parse(item.Cells[INDEX_FOR_SOURCETYPE_IN_GRID].Text);
                int IDzf = item.GetCellValueAs<int>(materialGrid_ID_ZF_UniqueName);// int.Parse(item.Cells[INDEX_FOR_IDZF_IN_GRID].Text);
                int simpleDeptId = item.GetCellValueAs<int>(materialGrid_SIMPLE_DEPARTMENT_ID_UniqueName);// int.Parse(item.Cells[INDEX_FOR_SIMPLEDEPTID_IN_GRID].Text);        
                var expectedQuantity = item.GetCellValueAs<decimal>(materialGrid_Quantity_UniqueName);// double.Parse(item.Cells[INDEX_AVAILABLE_QUANTITY_IN_GRID].Text);

                var quantityFromUser = decimal.Parse(QuantityTextBox.Text);
                Contract.Assert(quantityFromUser > 0, "Quantity to receive has to be greater than 0.");
                // Package stuff

                int? packagesNumber = null;
                int? packageQuantity = null;
                int? packageTypeNameId = null;

                if (!string.IsNullOrEmpty(RadComboBox_PackageTypeName.SelectedValue))
                {
                    packageTypeNameId = int.Parse(RadComboBox_PackageTypeName.SelectedValue);

                    if (packageTypeNameId > 0)
                    {
                        packagesNumber = int.Parse(TextBox_PackagesNumber.Text);
                        packageQuantity = int.Parse(TextBox_PackageQuantity.Text); //.Replace(".", ","));
                    }
                    else
                    {
                        packageTypeNameId = null;
                    }
                }

                AddPosition(
                  newProcessHeaderId,
                  newProcessIDPL,
                  newProcessIDDPL,
                  newValuePerUnit,
                  newContractingPartySimple,
                  newOrderNumberPZ,
                  newForDepartment,
                  newIdSim,
                  materialFlag,
                  quantityFromUser,
                  _companyId,
                  expectedQuantity,
                  sourceType,
                  IDzf,
                  simpleDeptId,
                  packageTypeNameId,
                  packagesNumber,
                  packageQuantity);
            }

            //this.QuantityCustomValidator.Enabled = true;
        }

        protected void AddSupplyClick(object sender, EventArgs e)
        {
            var suppliedQuantity = decimal.Parse(this.RadTextBox_SuppliedQuantity.Text);
            if (this.CheckBox_GetIncome.Checked)
            {
                if (QuantityTextBox.Text == "")
                {
                    QuantityTextBox.Text = suppliedQuantity.ToString("0.##");
                }
                Page.Validate("AddPositionGroup");
            }

            if (IsValid) // this.RadComboBox_MaterialsSIMPLE.SelectedValue != null && this.RadDatePicker_SupplyDate.SelectedDate.HasValue)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    string key = "";

                    string today = DateTime.Now.ToString("yyyy-MM-dd");

                    key = "INTERNAL/" + today.Replace("-", "") + "/" + WebTools.GenerateRandomString(8);

                    int simpleId = int.Parse(this.RadComboBox_MaterialsSIMPLE.SelectedValue);

                    DateTime supplyDate = this.RadDatePicker_SupplyDate.SelectedDate.Value;
                    decimal valuePerUnit = decimal.Parse(this.RadTextBox_UnitCost.Text);
                    int contractingPartySimple = int.Parse(this.RadComboBox_ContractingPartySimple.SelectedValue);
                    string orderNumberPz = key;
                    bool forDepartment = false;
                    int processHeaderId = 0; // 666000;
                    int processIdpl = 0; // 666000;
                    int processIddpl = 0;
                    int flaga = 1;

                    string orderId = this.RadComboBox_OrderNumber.SelectedValue;

                    int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

                    warehouseDs.AddNewSupplyByHand(simpleId,
                                                   suppliedQuantity,
                                                   supplyDate,
                                                   currentComapnyId,
                                                   processHeaderId,
                                                   processIdpl,
                                                   processIddpl,
                                                   contractingPartySimple,
                                                   orderNumberPz,
                                                   forDepartment,
                                                   flaga,
                                                   valuePerUnit,
                                                   orderId
                                                   );

                    if (this.CheckBox_GetIncome.Checked)
                    {
                        // TODO: Uncomment if necessary:

                        //double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));
                        //AddPosition(processHeaderId, processIdpl, processIddpl, valuePerUnit, contractingPartySimple,
                        //            orderNumberPz,
                        //            forDepartment, simpleId, quantity, currentComapnyId, suppliedQuantity, 2);
                    }

                    this.MaterialGrid.Rebind();

                    //this.RadComboBox_ContractingPartySimple.ClearSelection();
                    //this.RadComboBox_MaterialsSIMPLE.ClearSelection();
                    this.RadDatePicker_SupplyDate.SelectedDate = DateTime.Now.Date;
                    this.RadTextBox_SuppliedQuantity.Text = "";
                    this.RadTextBox_UnitCost.Text = "";
                    this.CheckBox_GetIncome.Checked = false;
                }
            }
        }

        [WebMethod]
        public static string GetShelfIds(int companyId, int warehouseId, string indexSIMPLE)
        {
            string result;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                result = warehouseDs.GetShelfIds(companyId, warehouseId, indexSIMPLE);
            }

            return result;
        }

        protected void Button_OQC_No_Click(object sender, EventArgs e)
        {
            this.QuantityCustomValidator.Enabled = true;
            this.Panel_OverQuantityConfirmation.Visible = false;
            QuantityTextBox.Text = "";
        }

        protected void Button_OQC_Yes_Click(object sender, EventArgs e)
        {
            this.QuantityCustomValidator.Enabled = false;
            this.Panel_OverQuantityConfirmation.Visible = false;
        }

        protected void ClearEndDateFilterClick(object sender, EventArgs e)
        {
            RadDatePicker2.SelectedDate = null;
        }

        protected void ClearStartDateFilterClick(object sender, EventArgs e)
        {
            RadDatePicker1.SelectedDate = null;
        }

        protected void ContractingParty_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (_contractingParty != null && MaterialGrid.SelectedItems.Count == 1)
            {
                var item = GetSelectedMaterialGridDataItem();
                int newContractingPartyId = item.GetCellValueAs<int>(materialGrid_CONTRACTING_PART_ID_UniqueName); //int.Parse(cell.Text);

                if (_contractingParty != newContractingPartyId)
                {
                    args.IsValid = false;
                    return;
                }
            }
            args.IsValid = true;
        }

        //CustomValidator_ContractingPartySimple_ServerValidate
        protected void CustomValidator_ContractingPartySimple_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!string.IsNullOrEmpty(RadComboBox_ContractingPartySimple.SelectedValue) && int.Parse(RadComboBox_ContractingPartySimple.SelectedValue) > 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidator_MaterialSimple_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!string.IsNullOrEmpty(RadComboBox_MaterialsSIMPLE.SelectedValue) && int.Parse(RadComboBox_MaterialsSIMPLE.SelectedValue) > 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        //Packages_ServerValidate
        protected void CustomValidator_Packages_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = string.IsNullOrWhiteSpace(RadComboBox_PackageTypeName.SelectedValue);
            if (args.IsValid)
                return;

            try
            {
                int packageTypeId = int.Parse(RadComboBox_PackageTypeName.SelectedValue);
                if (packageTypeId > 1)
                {
                    var packageQuantity = decimal.Parse(this.TextBox_PackageQuantity.Text);
                    int packagesNumber = int.Parse(this.TextBox_PackagesNumber.Text);
                    //decimal quantityNoPacked = decimal.Parse(this.TextBox_NoPackageQuantity.Text);
                    var totalQuantity = decimal.Parse(QuantityTextBox.Text);

                    args.IsValid = totalQuantity == packagesNumber * packageQuantity;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        //CustomValidator_QuantityFormat_ServerValidate
        protected void CustomValidator_QuantityFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            try
            {
                decimal.Parse(this.RadTextBox_SuppliedQuantity.Text);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }


        protected void CustomValidator_SupplyDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadDatePicker_SupplyDate.SelectedDate.HasValue && RadDatePicker_SupplyDate.SelectedDate > DateTime.Parse("1900-01-01"))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        //}
        //CustomValidator_UnitCostFormat_ServerValidate
        protected void CustomValidator_UnitCostFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            try
            {
                decimal.Parse(this.RadTextBox_UnitCost.Text);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }


        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = decimal.Parse(QuantityTextBox.Text) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }


        //this method is used by Mark All as Read and Empty this folder
        protected void emptyFolder(RadTreeNode node, bool removeChildNodes)
        {
            Contract.Requires(node != null);

            node.Font.Bold = false;
            node.Text = Regex.Replace(node.Text, unreadPattern, "");

            if (removeChildNodes)
            {
                //Empty this folder is clicked
                for (int i = node.Nodes.Count - 1; i >= 0; i--)
                {
                    node.Nodes.RemoveAt(i);
                }
            }
            else
            {
                //Mark all as read is clicked
                foreach (RadTreeNode child in node.Nodes)
                {
                    emptyFolder(child, removeChildNodes);
                }
            }
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = MaterialGrid.SelectedItems.Count == 1 ? true : false; //co jesli juz jest invalid?
        }

        protected void MaterialsToReceiveRadGrid_DeleteRow(object source, GridCommandEventArgs e)
        {
            bool isGridActive = true;
            using (var warehouseDs = new WarehouseDS())
            {
                isGridActive = warehouseDs.GetFormState(_pageName) != WarehouseDocument.WarehouseDocumentEditStateEnum.Saved;
            }

            if (isGridActive)
            {
                if (e.CommandName == "DeleteRow")
                {

                    int rowIndex = _positionWithQuantityAndShelfsCollection[e.Item.ItemIndex].GridRow;
                    decimal quantityReturn = (decimal)_positionWithQuantityAndShelfsCollection[e.Item.ItemIndex].position.Quantity;
                    GridDataItem item = (GridDataItem)MaterialGrid.Items[rowIndex];
                    MaterialGridUpdateAvailableQuantity(-quantityReturn, item);

                    _positionWithQuantityAndShelfsCollection.RemoveAt(e.Item.ItemIndex);
                    Session["ReceiveMaterial_positionWithQuantityAndShelfses"] = _positionWithQuantityAndShelfsCollection;

                    if (MaterialsToReceiveRadGrid.Items.Count <= 1)
                    {
                        Session["ReceiveMaterial_ContractinPartyId"] = null;
                        Session["ReceiveMaterial_OrderNumberPZ"] = null;

                        _contractingParty = (int?)Session["ReceiveMaterial_ContractinPartyId"];
                        _orderNumberPZ = (string)Session["ReceiveMaterial_OrderNumberPZ"];
                    }

                    MaterialsToReceiveRadGrid.Rebind();
                    //MaterialGrid.Rebind();
                }
                else
                {
                    throw new ApplicationException("Unknown grid command");
                }
            }
        }

        protected void MaterialsToReceiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (_positionWithQuantityAndShelfsCollection != null)
            {
                //int i = 1;

                var positionWithShelfAndQuantities = new List<PositionWithShelfAndQuantity>();

                foreach (var positionWithQuantityAndShelfs in _positionWithQuantityAndShelfsCollection)
                {
                    if (positionWithQuantityAndShelfs.WarehousePositionZones.Count() > 0)
                    {
                        decimal usedQuantity = 0;
                        foreach (var warehousePositionZone in positionWithQuantityAndShelfs.WarehousePositionZones)
                        {
                            positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                            {
                                position = positionWithQuantityAndShelfs.position,
                                shelf = warehousePositionZone.Zone,
                                quantity = (decimal)warehousePositionZone.Quantity,
                                TargetSimpleDepartmentName = positionWithQuantityAndShelfs.TargetSimpleDepartmentName,
                                IsKJ = positionWithQuantityAndShelfs.position.IsKJ.HasValue?positionWithQuantityAndShelfs.position.IsKJ.Value:false,
                                //, order = internalIterator.WarehouseDocumentPosition.Order
                            });
                            usedQuantity += (decimal)warehousePositionZone.Quantity;
                        }
                        if (usedQuantity < (decimal)positionWithQuantityAndShelfs.position.Quantity)
                        {
                            positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                            {
                                position = positionWithQuantityAndShelfs.position,
                                shelf = null,
                                quantity = (decimal)positionWithQuantityAndShelfs.position.Quantity - usedQuantity,
                                TargetSimpleDepartmentName = positionWithQuantityAndShelfs.TargetSimpleDepartmentName,
                                IsKJ = positionWithQuantityAndShelfs.position.IsKJ.HasValue ? positionWithQuantityAndShelfs.position.IsKJ.Value : false,
                                //,order = externalIterator.order
                            });
                        }
                    }
                    else
                    {
                        positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                        {
                            position = positionWithQuantityAndShelfs.position,
                            shelf = null,
                            quantity = (decimal)positionWithQuantityAndShelfs.position.Quantity,
                            TargetSimpleDepartmentName = positionWithQuantityAndShelfs.TargetSimpleDepartmentName,
                            IsKJ = positionWithQuantityAndShelfs.position.IsKJ.HasValue ? positionWithQuantityAndShelfs.position.IsKJ.Value : false,
                           
                            //,order = externalIterator.order
                        });
                    }
                }

                var source2 = positionWithShelfAndQuantities
                  .Select
                  ((p, i) =>
                    new
                    {
                        RowNumber = ++i,
                        MaterialName = p.position.Material.Name,
                        Quantity = p.quantity,
                        UnitName = p.position.Material.Unit.Name,
                        ShelfName = p.shelf != null ? p.shelf.Name : null,
                        OrderNumber = p.position.Order != null ? p.position.Order.OrderNumber : (p.position.SimpleDepartmentId.HasValue ? p.TargetSimpleDepartmentName : (p.position.IsKC ? "~ nadwyżka ~" : "")),
                        IsKJ = p.IsKJ
                    }
                  );

                MaterialsToReceiveRadGrid.DataSource = source2;
            }
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            //btnPrintDocument.Enabled = false;
            //btnSaveDocument.Enabled = true;
            //btnAddPosition.Enabled = true;
            //ExternalInvoiceNumber.Enabled = true;
            //QuantityTextBox.Enabled = true;
            //cmbShelf.Enabled = true;

            //this.RadComboBox_ContractingPartySimple.Enabled = true;
            //this.RadComboBox_MaterialsSIMPLE.Enabled = true;
            //this.RadTextBox_SuppliedQuantity.Enabled = true;
            //this.RadTextBox_UnitCost.Enabled = true;
            //this.RadDatePicker_SupplyDate.Enabled = true;
            //this.Button_AddSupply.Enabled = true;
            //this.CheckBox_GetIncome.Enabled = true;

            //this.QuantityCustomValidator.Enabled = true;
            //this.Panel_OverQuantityConfirmation.Visible = false;

            _positionWithQuantityAndShelfsCollection = new List<PositionWithQuantityAndShelfs>();
            Session["ReceiveMaterial_positionWithQuantityAndShelfses"] = _positionWithQuantityAndShelfsCollection;
            Session["ReceiveMaterial_ContractinPartyId"] = null;
            Session["ReceiveMaterial_OrderNumberPZ"] = null;

            ExternalInvoiceNumber.Text = "";
            QuantityTextBox.Text = "";
            cmbShelf.Text = "";
            MaterialGrid.SelectedIndexes.Clear();
            MaterialGrid.Rebind();
            MaterialsToReceiveRadGrid.Rebind();

            //this.RadComboBox_ContractingPartySimple.ClearSelection();
            //this.RadComboBox_MaterialsSIMPLE.ClearSelection();
            this.RadDatePicker_SupplyDate.SelectedDate = DateTime.Now.Date;
            this.RadTextBox_SuppliedQuantity.Text = "";
            this.RadTextBox_UnitCost.Text = "";
            this.CheckBox_GetIncome.Checked = false;
            this.cbIsKJ.Checked = false;

            this.TextBox_PackagesNumber.Text = "0";
            //RadComboBox_PackageTypeName.ClearSelection();

            RadComboBox_PackageTypeName.Text = "";

            this.RadComboBox_TargetDepartmentSIMPLE.SelectedIndex = 0;
            this.RadComboBox_TargetDepartmentSIMPLE.Enabled = true;

            using (var warehouseDs = new WarehouseDS())
            {
                warehouseDs.SetFormState(_pageName, WarehouseDocument.WarehouseDocumentEditStateEnum.New);
            }
        }

        protected void OrderNumberPZ_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //if (positionWithQuantityAndShelfses.Count() != 0 && MaterialGrid.SelectedItems.Count == 1)
            //{
            //    GridItem item = MaterialGrid.SelectedItems[0];
            //    var cell = item.Cells[INDEX_ORDERNUMBERPZ_IN_GRID];
            //    string newOrderNumberPZ = cell.Text;

            //    foreach (var s in positionWithQuantityAndShelfses)
            //    {
            //        if (s.position.Order.OrderNumberPZ != newOrderNumberPZ)
            //        {
            //            args.IsValid = false;
            //            return;
            //        }
            //    }
            //}

            args.IsValid = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int currentCompanyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.Pz, userId)) != "" &&
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.PzKC, userId)) != "" //&&
                    //  (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.MMPlus, userId)) != ""
                  )
                {
                    WebTools.AlertMsgAdd(message);
                    //Session[SessionData.SessionKey_Message] = message;
                    PermissionDenied();
                    return;
                }

                if (!Page.IsPostBack)
                {
                    RadComboBox_TargetDepartmentSIMPLE.DataSource =
                      warehouseDs.GetAllDepartmentsSIMPLE(currentCompanyId)
                      .Select(d => new { Value = d.komorka_id, Text = d.nazwa })
                      .Distinct()
                      .OrderBy(p => p.Text);

                    RadComboBox_TargetDepartmentSIMPLE.DataTextField = "Text";
                    RadComboBox_TargetDepartmentSIMPLE.DataValueField = "Value";
                    RadComboBox_TargetDepartmentSIMPLE.DataBind();
                    RadComboBox_TargetDepartmentSIMPLE.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
                }
            }

            if (!Page.IsPostBack)
            {
                if (RadDatePicker1.SelectedDate == null)
                {
                    RadDatePicker1.SelectedDate = DateTime.Now.Date.AddDays(-3).Date;
                }
                if (RadDatePicker2.SelectedDate == null)
                {
                    RadDatePicker2.SelectedDate = DateTime.Now.Date.AddDays(4).Date;
                }
                //RadDatePicker2.SelectedDate = DateTime.Now.Date;
                inputDate.SelectedDate = DateTime.Now.Date;
            }

            if (!IsPostBack)
            {
                Session["ReceiveMaterial_positionWithQuantityAndShelfses"] = null;
                Session["ReceiveMaterial_ContractinPartyId"] = null;
                Session["ReceiveMaterial_OrderNumberPZ"] = null;
            }

            //if ((bool)Session["IsOrderRequired"])
            //{
            //    //RadToolBar1.Tabs.First()
            //}

            if (Session["ReceiveMaterial_positionWithQuantityAndShelfses"] == null)
                _positionWithQuantityAndShelfsCollection = new List<PositionWithQuantityAndShelfs>();
            else
                _positionWithQuantityAndShelfsCollection = (List<PositionWithQuantityAndShelfs>)Session["ReceiveMaterial_positionWithQuantityAndShelfses"];

            _companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            _contractingParty = (int?)Session["ReceiveMaterial_ContractinPartyId"];
            _orderNumberPZ = (string)Session["ReceiveMaterial_OrderNumberPZ"];

            this.Panel_OverQuantityConfirmation.Visible = false;

            //if (this.RadComboBox_MaterialsSIMPLE.WebServiceSettings.Method == "")
            //    this.RadComboBox_MaterialsSIMPLE.WebServiceSettings.Method = "GetTelericMaterialsSIMPLEListCompany" + companyId.ToString();

            this.RadComboBox_MaterialsSIMPLE.WebServiceSettings.Method = "GetTelericMaterialsSIMPLEListCompany" + _companyId.ToString();

            //if (this.RadComboBox_ContractingPartySimple.WebServiceSettings.Method == "")
            //    this.RadComboBox_ContractingPartySimple.WebServiceSettings.Method = "GetTelericContractingPartySIMPLEListCompany" + companyId.ToString();

            this.RadComboBox_ContractingPartySimple.WebServiceSettings.Method = "GetTelericContractingPartySIMPLEListCompany" + _companyId.ToString();

            this.RadComboBox_OrderNumber.WebServiceSettings.Method = "GetTelericOrderNumbersSIMPLEListCompany" + _companyId.ToString();

            this.RadDatePicker_SupplyDate.SelectedDate = DateTime.Now.Date;

            //AddPackageTypeOKLabel.Visible = false;

            //btnNewDocument.Enabled = false;

            if (this.TextBox_PackagesNumber.Text == "")
                this.TextBox_PackagesNumber.Text = "0";

            using (var warehouseDs = new WarehouseDS())
            {
                var isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility
                  (currentWarehouseId.Value,warehouseDs.GetPersonForSystemUser(userId).Id, 1);

                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }

            var sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            var oInfo = new System.IO.FileInfo(sPath);
            _pageName = oInfo.Name;
            if (!Page.IsPostBack)
            {
                using (var warehouseDs = new WarehouseDS())
                {
                    warehouseDs.SetFormState(_pageName, WarehouseDocument.WarehouseDocumentEditStateEnum.New);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ////this.RadComboBox_ContractingPartySimple.DataBind();
            ////this.RadComboBox_MaterialsSIMPLE.DataBind();
            //this.RadComboBox_ContractingPartySimple.Text = "";
            //this.RadComboBox_MaterialsSIMPLE.Text = "";
            //this.cmbShelf.Text = "";
            //RadComboBox_PackageTypeName.Text = "";
            //this.Panel_EntirePageContent.Visible = this.PageIsAllowedByConfigAndSecurity;
            ////this.RadComboBox_ContractingPartySimple.ClearSelection();
            ////this.RadComboBox_MaterialsSIMPLE.DataBind();

            ////if (this.btnSaveDocument.Enabled) this.btnSaveDocument.Enabled = positionWithQuantityAndShelfses.Count > 0;
            //this.RadComboBox_PackageTypeName.Enabled = true;
            //this.TextBox_PackageQuantity.Enabled = true;
            //this.TextBox_PackagesNumber.Enabled = true;

            using (var warehouseDs = new WarehouseDS())
            {
                WarehouseDocument.WarehouseDocumentEditStateEnum formState = warehouseDs.GetFormState(_pageName);

                if (formState == WarehouseDocument.WarehouseDocumentEditStateEnum.Unknown)
                {
                    warehouseDs.SetFormState(_pageName, WarehouseDocument.WarehouseDocumentEditStateEnum.New);
                }

                formState = warehouseDs.GetFormState(_pageName);

                if (formState == WarehouseDocument.WarehouseDocumentEditStateEnum.New)
                {
                    if (_positionWithQuantityAndShelfsCollection.Count > 0)
                    {
                        warehouseDs.SetFormState(_pageName, WarehouseDocument.WarehouseDocumentEditStateEnum.ReadyToSave);
                    }
                }

                if (formState == WarehouseDocument.WarehouseDocumentEditStateEnum.ReadyToSave)
                {
                    if (_positionWithQuantityAndShelfsCollection.Count == 0)
                    {
                        warehouseDs.SetFormState(_pageName, WarehouseDocument.WarehouseDocumentEditStateEnum.New);
                    }
                }

                formState = warehouseDs.GetFormState(_pageName);

                switch (formState)
                {
                    case WarehouseDocument.WarehouseDocumentEditStateEnum.New:
                        {
                            MaterialGrid.Enabled = true;
                            MaterialsToReceiveRadGrid.Enabled = true;

                            cmbShelf.Enabled = true;
                            QuantityTextBox.Enabled = true;
                            ExternalInvoiceNumber.Enabled = true;
                            btnPrintDocument.Enabled = false;
                            btnSaveDocument.Enabled = false;
                            btnAddPosition.Enabled = true;

                            btnNewDocument.Enabled = false;

                            this.RadComboBox_ContractingPartySimple.Enabled = true;
                            this.RadComboBox_MaterialsSIMPLE.Enabled = true;
                            this.RadTextBox_SuppliedQuantity.Enabled = true;
                            this.RadTextBox_UnitCost.Enabled = true;
                            this.RadDatePicker_SupplyDate.Enabled = true;
                            this.Button_AddSupply.Enabled = true;
                            this.CheckBox_GetIncome.Enabled = true;

                            //this.CheckBox_ExportToSIMPLE.Enabled = false;

                            this.RadComboBox_PackageTypeName.Enabled = true;
                            this.TextBox_PackageQuantity.Enabled = true;
                            this.TextBox_PackagesNumber.Enabled = true;

                            RadComboBox_PackageTypeName.Text = "";

                            this.QuantityCustomValidator.Enabled = true;
                            this.Panel_OverQuantityConfirmation.Visible = false;
                        }
                        break;

                    case WarehouseDocument.WarehouseDocumentEditStateEnum.ReadyToSave:
                        {
                            cmbShelf.Enabled = true;
                            QuantityTextBox.Enabled = true;
                            ExternalInvoiceNumber.Enabled = true;
                            btnPrintDocument.Enabled = false;
                            btnSaveDocument.Enabled = true;
                            btnAddPosition.Enabled = true;

                            btnNewDocument.Enabled = true;

                            this.RadComboBox_ContractingPartySimple.Enabled = true;
                            this.RadComboBox_MaterialsSIMPLE.Enabled = true;
                            this.RadTextBox_SuppliedQuantity.Enabled = true;
                            this.RadTextBox_UnitCost.Enabled = true;
                            this.RadDatePicker_SupplyDate.Enabled = true;
                            this.Button_AddSupply.Enabled = true;
                            this.CheckBox_GetIncome.Enabled = true;

                            //this.CheckBox_ExportToSIMPLE.Enabled = true;

                            this.RadComboBox_PackageTypeName.Enabled = true;
                            this.TextBox_PackageQuantity.Enabled = true;
                            this.TextBox_PackagesNumber.Enabled = true;

                            RadComboBox_PackageTypeName.Text = "";
                        }
                        break;

                    case WarehouseDocument.WarehouseDocumentEditStateEnum.Saved:
                        {
                            cmbShelf.Text = "";
                            QuantityTextBox.Text = "";
                            MaterialGrid.SelectedIndexes.Clear();
                            MaterialGrid.Rebind();
                            MaterialGrid.Enabled = false;

                            MaterialsToReceiveRadGrid.Enabled = false;

                            cmbShelf.Enabled = false;
                            QuantityTextBox.Enabled = false;
                            ExternalInvoiceNumber.Enabled = false;
                            btnPrintDocument.Enabled = true;
                            btnSaveDocument.Enabled = false;
                            btnAddPosition.Enabled = false;

                            btnNewDocument.Enabled = true;

                            this.RadComboBox_ContractingPartySimple.Enabled = false;
                            this.RadComboBox_MaterialsSIMPLE.Enabled = false;
                            this.RadTextBox_SuppliedQuantity.Enabled = false;
                            this.RadTextBox_UnitCost.Enabled = false;
                            this.RadDatePicker_SupplyDate.Enabled = false;
                            this.Button_AddSupply.Enabled = false;
                            this.CheckBox_GetIncome.Enabled = false;

                            this.CheckBox_ExportToSIMPLE.Enabled = false;

                            this.RadComboBox_PackageTypeName.Enabled = false;
                            this.TextBox_PackageQuantity.Enabled = false;
                            this.TextBox_PackagesNumber.Enabled = false;

                            RadComboBox_PackageTypeName.Text = "";
                        }
                        break;
                }
            }
            this.btnSaveDocument.Visible = this.btnSaveDocument.Enabled;
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (MaterialGrid.SelectedItems.Count == 1 && !string.IsNullOrWhiteSpace(QuantityTextBox.Text))
            {
                var quantity = decimal.MaxValue;
                args.IsValid = decimal.TryParse(QuantityTextBox.Text, out quantity);
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void RadTreeView1_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            RadTreeNode clickedNode = e.Node;

            switch (e.MenuItem.Value)
            {
                case "Copy":
                    RadTreeNode clonedNode = clickedNode.Clone();
                    clonedNode.Text = string.Format("Copy of {0}", clickedNode.Text);
                    clickedNode.InsertAfter(clonedNode);
                    //set node's value so we can find it in startNodeInEditMode
                    clonedNode.Value = clonedNode.GetFullPath("/");
                    clonedNode.Selected = true;
                    startNodeInEditMode(clonedNode.Value);
                    break;

                case "NewFolder":
                    RadTreeNode newFolder = new RadTreeNode(string.Format("New Folder {0}", clickedNode.Nodes.Count + 1));
                    newFolder.Selected = true;
                    newFolder.ImageUrl = clickedNode.ImageUrl;
                    clickedNode.Nodes.Add(newFolder);
                    clickedNode.Expanded = true;
                    //update the number in the brackets
                    if (Regex.IsMatch(clickedNode.Text, unreadPattern))
                        clickedNode.Text = Regex.Replace(clickedNode.Text, unreadPattern, "(" + clickedNode.Nodes.Count.ToString() + ")");
                    else
                        clickedNode.Text += string.Format(" ({0})", clickedNode.Nodes.Count);
                    clickedNode.Font.Bold = true;
                    //set node's value so we can find it in startNodeInEditMode
                    newFolder.Value = newFolder.GetFullPath("/");
                    startNodeInEditMode(newFolder.Value);
                    break;

                case "EmptyFolder":
                    emptyFolder(clickedNode, true);
                    break;

                case "MarkAsRead":
                    emptyFolder(clickedNode, false);
                    break;

                case "Delete":
                    clickedNode.Remove();
                    break;
            }
        }

        protected void RadTreeView1_NodeEdit(object sender, RadTreeNodeEditEventArgs e)
        {
            e.Node.Text = e.Text;
        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            //int userId;

            if (IsValid)
            {
                if (_positionWithQuantityAndShelfsCollection.Count > 0)
                {
                    //if (int.TryParse(.ToString(), out userId))
                    //{
                    var userId = (int?)Session[SessionData.SessionKey_UserId];
                    Contract.Assert(userId.HasValue, "Session expired. Please login again.");

                    try
                    {
                        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            WarehouseDocument warehouseDocument = null;

                            var transactionOptions = new TransactionOptions();
                            transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                            //////transactionOptions.Timeout = TransactionManager.MaximumTimeout;

                            int currentWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                            if (_contractingParty == null) throw new ApplicationException("Internal error with contracting party (is null)");
                            if (string.IsNullOrWhiteSpace(_orderNumberPZ))
                                throw new ApplicationException("Internal error with order number PZ (is null or empty)");

                            int contractingPartyInSIMPLE = (int)_contractingParty;

                            using (var transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions))
                            {
                                warehouseDocument = warehouseDs.ExternalReceive
                                (
                                  currentWarehoseId,
                                  warehouseDs.GetPersonForSystemUser(userId).Id,
                                  _positionWithQuantityAndShelfsCollection,
                                  contractingPartyInSIMPLE,
                                  inputDate.SelectedDate ?? DateTime.Now.Date,
                                  CheckBox_ExportToSIMPLE.Checked
                                );

                                Contract.Assert(warehouseDocument != null, "warehouseDocument is still null after call to warehouseDs.ExternalReceive(...)");

                                try
                                {
                                    foreach (var positionWithQuantityAndShelf in _positionWithQuantityAndShelfsCollection)
                                    {
                                        var material = warehouseDs.GetMaterialById(positionWithQuantityAndShelf.position.Material.Id);
                                        warehouseDs.WarehouseEntitiesTest.UpdateFlatViewWaitingForSupply(material.IdSIMPLE, material.CompanyId);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    warehouseDocument.TimeDeleted = DateTime.Now;
                                    warehouseDs.WarehouseEntitiesTest.SaveChanges();
                                    WebTools.AlertMsgAdd("Wystąpił błąd wewnętrzny przy aktualizacji WaitingForSupply_FlattenSrc. Dokument Pz o numerze " + warehouseDocument.Id + " został unieważniony. Prosimy o zachowanie zrzutu tego ekranu i kontakt z działem IT. Szczegóły błędu: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));

                                    return;
                                }

                                string externalInvoiceNumber = ExternalInvoiceNumber.Text.Replace(" ", "");

                                if (!String.IsNullOrEmpty(externalInvoiceNumber))
                                {
                                    warehouseDs.UpdateInvoiceNumber(warehouseDocument, ExternalInvoiceNumber.Text);
                                }

                                WebTools.AlertMsgAdd("Dokument został zapisany. Materiały zostały przyjęte na <b>" + warehouseDocument.Warehouse1.Name + "</b> dokumentem nr: <b>" + warehouseDocument.Number + "</b>.");

                                // commented out because LoanTake functionality in K3 should not be used.
                                //// Try to return loans:
                                //int warehouseId = warehouseDs.GetWarehouseDocumentById(warehouseDocument.Id).Warehouse1.Id;
                                //warehouseDs.TryToReturnLoans(warehouseId, warehouseDs.GetPersonForSystemUser(userId).Id);

                                transactionScope.Complete();
                            }
                            warehouseDs.SetFormState(_pageName, WarehouseDocument.WarehouseDocumentEditStateEnum.Saved);

                            this.RadComboBox_TargetDepartmentSIMPLE.Enabled = false;
                            _positionWithQuantityAndShelfsCollection = new List<PositionWithQuantityAndShelfs>();
                            Session["ReceiveMaterial_positionWithQuantityAndShelfses"] = _positionWithQuantityAndShelfsCollection;
                            Session["ReceiveMaterial_ContractinPartyId"] = null;
                            Session["ReceiveMaterial_OrderNumberPZ"] = null;
                            //MaterialsToReceiveRadGrid.DataSource = null;
                            //MaterialsToReceiveRadGrid.Rebind();
                            DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument, WarehouseDocumentTypeEnum.Pz, warehouseDocument.Id);
                        }
                    }
                    catch (Exception ex)
                    {
                        WebTools.AlertMsgAdd(ex, userId);
                    }
                }
            }
        }

        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            //QuantityRTB.Text = QuantityRTB.Text.Replace(".", ",");

            //onneeddatasource="MaterialGrid_OnNeedDataSource" 
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                string materialToSearch = MaterialToSearchRTB.Text;
                string contractingPartyNameToSearch = ContractingPartyName.Text;

                DateTime? startDate = RadDatePicker1.SelectedDate;// null;
                DateTime? endDate = RadDatePicker2.SelectedDate;// null;
                //DateTime? RadDatePicker1_SelectedDate = RadDatePicker1.SelectedDate;
                //DateTime? RadDatePicker2_SelectedDate = RadDatePicker2.SelectedDate;
                //if (RadDatePicker1_SelectedDate != null)
                //{
                //  startDate = (DateTime)RadDatePicker1.SelectedDate;
                //}
                //if (RadDatePicker2_SelectedDate != null)
                //{
                //  endDate = (DateTime)RadDatePicker2.SelectedDate;
                //}

                string quantityToSearch = QuantityRTB.Text;//.Replace(".", ",");
                string orderNumberToSearch = OrderNumberRTB.Text;
                string orderNumberPZToSearch = OrderNumberPZ_RTB.Text;

                int targetDeptIdSIMPLE = 0;
                int.TryParse(RadComboBox_TargetDepartmentSIMPLE.SelectedValue, out targetDeptIdSIMPLE);
                int? targetSimpleDepartmentId = targetDeptIdSIMPLE > 0 ? targetDeptIdSIMPLE : (int?)null;

                //if (targetDeptIdSIMPLE < 0)
                //{
                //  targetDeptIdSIMPLE = 0;
                //}
                if (startDate == null || endDate == null)
                {
                    WebTools.AlertMsgAdd("Proszę podaj datę początkową oraz końcową.");
                    return;
                }

                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                var materialsCandidatesToReceiveMaterialized = warehouseDs.GetWaitingForSupply
                (
                  materialToSearch,
                  contractingPartyNameToSearch,
                  startDate,
                  endDate,
                  _companyId,
                  quantityToSearch,
                  orderNumberToSearch,
                  orderNumberPZToSearch,
                  Convert.ToInt32(QuantityTolerance.SelectedValue),
                  targetSimpleDepartmentId);
                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                var items = materialsCandidatesToReceiveMaterialized
                  .Select
                  ((m, i) =>
                    {
                        var usedUpQuantity = GetUsedUpQuantity(m);

                        return new
                        {
                            RowNumber = i + 1,
                            ProcessHeaderId = m.ID,
                            ProcessIDPL = m.IDpl,
                            ProcessIDDPL = m.IDdpl,
                            UnitPrice = m.Cena,
                            //ContractingPartyId = m.kontrachent_id,
                            ContractingPartyId = m.CorrectKontrahentId,
                            SupplyDate = m.DataKL,
                            IdSim = m.IDsim,
                            ContractingParty = m.ContractingPartyName,
                            OrderNumberPZ = m.NazwSK,
                            MaterialName = m.MaterialName,
                            IndexSIMPLE = m.IndexSIMPLE,
                            // On DB level [WaitingForSupply_Src].[IloscZam] is defined as follows: 
                            // dbo.WaitingForSupplyMarek.IloscZam - CASE WHEN Used.UsedQuantity IS NOT NULL THEN Used.UsedQuantity ELSE 0 END
                            // It seems there may be some problem with double counting the used-up quantity, though this is not confirmed
                            Quantity = m.IloscZam - usedUpQuantity,
                            QuantityAdded = 0,
                            QuantityOrdered = m.IloscZamBrutto,
                            QuantityDelivered = warehouseDs.WarehouseEntitiesTest.WaitingForSupplies.First
                              (wfs =>
                                wfs.IDsim == m.IDsim
                                && wfs.Cena == m.Cena
                                && wfs.Flaga == m.Flaga
                                && wfs.IDpl == m.IDpl
                                && wfs.IDdpl == m.IDdpl

                                && wfs.Dzial == m.Dzial
                                && wfs.SourceType == m.SourceType
                              )
                              .UsedQuantity,// p.UsedQuantity,
                            UnitName = m.UnitName,
                            OrderNumber = !string.IsNullOrEmpty(m.CorrectOrderNumber) ? m.CorrectOrderNumber : (m.Dzial > 0 ? m.SimpleDepartmentName : "Stan mag."),
                            ForDepartment = m.Flaga == 2 || m.Flaga == 3 ? 1 : 0,
                            SourceType = m.SourceType,
                            IdZF = m.IdZF,
                            //SourceClass = m.SourceType == 3 ? "WEWN." : " ",
                            SimpleDepartmentId = m.Dzial,
                            DepartmentName = m.SimpleDepartmentName,
                            K3DepartmentId = (int?)null,
                            K3MachineId = (int?)null,
                            Flaga = m.Flaga,
                            ExtendedName = warehouseDs.GetMaterials(m.MaterialName,m.IndexSIMPLE,_companyId).Select(x=>x.ExtendedName).FirstOrDefault(),
                            Uwagi = string.IsNullOrEmpty(m.Uwagi) ? "" : m.Uwagi.Trim()
                        };
                    }
                    )
                  .OrderBy(m => m.SupplyDate)
                  .ToList();
                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

                MaterialGrid.DataSource = items;
                if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());
            }

            MaterialGrid.Rebind();
        }

        private decimal GetUsedUpQuantity(WaitingForSupply m)
        {
            var positionWQAS = _positionWithQuantityAndShelfsCollection
              .FirstOrDefault(p =>
                p.IsReal &&
                p.position.Material.IdSIMPLE == m.IDsim &&
                p.position.OrderDelivery.ProcessIDPL == m.IDpl &&
                p.position.OrderDelivery.ProcessIDDPL == m.IDdpl &&
                p.position.IdZF == m.IdZF &&
                    (
                    (p.position.OrderDelivery != null && !p.position.OrderDelivery.ForDepartment && m.Flaga == 1) ||
                    ((p.position.OrderDelivery == null || p.position.OrderDelivery.ForDepartment) && (m.Flaga == 2 || m.Flaga == 3)))
                );

            return positionWQAS != null ? (decimal)positionWQAS.position.Quantity : 0;
        }


        protected void Shelf_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Zone zone = null;

            if (!string.IsNullOrEmpty(cmbShelf.Text))
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    zone = warehouseDs.GetShelfByName(cmbShelf.Text);
                }
            }

            if (zone == null) args.IsValid = false;
            else args.IsValid = true;
        }

        protected void ShelfRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newProcessHeaderId"></param>
        /// <param name="newProcessIDPL"></param>
        /// <param name="newProcessIDDPL"></param>
        /// <param name="newValuePerUnit"></param>
        /// <param name="newContractingPartySimple"></param>
        /// <param name="newOrderNumberPZ"></param>
        /// <param name="newForDepartment"></param>
        /// <param name="newIdSim"></param>
        /// <param name="flag"></param>
        /// <param name="quantityFromUser">quantity specified by the User</param>
        /// <param name="currentCompanyId"></param>
        /// <param name="expectedQuantity">quantity ordered decreased by already used up amount</param>
        /// <param name="sourceType">1-external, 2-by hand, 3-internal</param>
        /// <param name="IDzf"></param>
        /// <param name="simpleDeptId"></param>
        /// <param name="packageTypeNameId"></param>
        /// <param name="packagesNumber"></param>
        /// <param name="packageQuantity"></param>
        private void AddPosition(
          int newProcessHeaderId,
          int newProcessIDPL,
          int newProcessIDDPL,
          decimal newValuePerUnit,
          int newContractingPartySimple,
          string newOrderNumberPZ,
          bool newForDepartment,
          int newIdSim,
          int flag,
          decimal quantityFromUser,
          int currentCompanyId,
          decimal expectedQuantity,
          int sourceType,
          int IDzf,
          int simpleDeptId,
          int? packageTypeNameId = null,
          int? packagesNumber = null,
          int? packageQuantity = null
        )
        {
#if DEBUG
            Contract.Requires(quantityFromUser > 0, "Quantity to receive has to be greater than 0.");
            Contract.Requires(expectedQuantity > 0, "Available quantity expected to be received.");
#endif

            WarehouseDocumentPosition warehouseDocumentPosition;
            WarehouseDocumentPosition warehouseDocumentPositionForKC = null;

            var mainQuantity = quantityFromUser >= expectedQuantity ? expectedQuantity : quantityFromUser;
            var excessQuantity = quantityFromUser - mainQuantity;
            Contract.Assert(excessQuantity >= 0);

            var packages = new List<WarehouseDocumentPositionPackage>();
            Order order = null;
            Zone zone;
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                zone = warehouseDs.GetShelfByName(cmbShelf.Text);

                // Packages
                if (packageTypeNameId.HasValue && packageQuantity.HasValue && packageQuantity > 0 && packagesNumber.HasValue && packagesNumber > 0)
                {
                    var package = CreatePositionPackage(packageTypeNameId, packagesNumber, packageQuantity, zone, warehouseDs);
                    packages.Add(package);
                }

                var userId = (int)Session[SessionData.SessionKey_UserId];
                var material = warehouseDs.GetOrAddNewNewMaterial(newIdSim, currentCompanyId, warehouseDs.GetPersonForSystemUser(userId).Id);

                warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPosition(material.Id, newValuePerUnit, mainQuantity);
#if Debug

                Contract.Assert(warehouseDocumentPosition != null);
#endif
                warehouseDocumentPosition.IsKC = false;
                warehouseDocumentPosition.IdZF = IDzf > 0 ? IDzf : (int?)null;
                warehouseDocumentPosition.IsKJ = cbIsKJ.Checked;

                //if (false)// effectively comment out but enable for debugging during run-time
                //{
                //  var waiting0 = warehouseDs.FindRecordInWaitingForSupply(newProcessHeaderId, newProcessIDPL, newProcessIDDPL, newContractingPartySimple, newIdSim, currentCompanyId, newOrderNumberPZ, newForDepartment); //, quantity);
                //}

                var waiting = warehouseDs.FindRecordInWaitingForSupply(newProcessIDPL, newIdSim, newProcessIDDPL, flag, newContractingPartySimple);
                string newOrderNumer="";
                _contractingParty = newContractingPartySimple;
                _orderNumberPZ = newOrderNumberPZ;

                //newProcessIDDPL = waiting.IDdpl;
                if (simpleDeptId > 0)
                {
                    try
                    {
                        newOrderNumer = warehouseDs.GetOrderNumber(simpleDeptId, currentCompanyId);

                        newForDepartment = false;
                    }
                    catch (Exception)
                    {
                        //WebTools.AlertMsgAdd("Nie znaleziono właściwego działu Simple.");
                        //return;
                    }

                    
                }

                if(string.IsNullOrEmpty(newOrderNumer))
                {
                    newOrderNumer = waiting != null ? string.Join(" ", waiting.NRp.Split(new Char[] { ' ' }).Distinct()).Trim() : string.Empty;
                    if(string.IsNullOrEmpty(newOrderNumer))
                        newOrderNumer = waiting.SimpleDepartmentName != null ? string.Join(" ", waiting.SimpleDepartmentName.Split(new Char[] { ' ' }).Distinct()).Trim() : string.Empty;

                }

                var newProcessIDH = waiting.IDh;
                Tuple<Order, OrderDelivery> orderDelivery = warehouseDs.CreateOrGetOrderAndDelivery(
                    newProcessHeaderId,
                    newProcessIDH,
                    newProcessIDPL,
                    newProcessIDDPL,
                    newOrderNumer,
                    _orderNumberPZ,
                    newForDepartment,
                    currentCompanyId);

                if (!newForDepartment && orderDelivery.Item1!=null)
                    warehouseDocumentPosition.Order = orderDelivery.Item1;
                warehouseDocumentPosition.OrderDelivery = orderDelivery.Item2;

                order = warehouseDocumentPosition.Order;

                //double additionalQuantity = quantity - availableQuantity;
                if (excessQuantity > 0) // && warehouseDocumentPosition.Order != null)
                {
                    warehouseDocumentPositionForKC = warehouseDs.CreateWarehouseDocumentPosition(material.Id, newValuePerUnit, excessQuantity);// .CreateWarehouseDocumentPositionDataOnly(material.Id, newValuePerUnit, additionalQuantity);
                    warehouseDocumentPositionForKC.IsKC = true;
                    //warehouseDocumentPositionForKC.OrderDelivery = orderDelivery;
                    warehouseDocumentPositionForKC.OrderDelivery = orderDelivery.Item2;
                }
                else
                {
                    Contract.Assert(excessQuantity == 0);
                    //warehouseDocumentPosition.Quantity += (double)excessQuantity;
                    //mainQuantity += excessQuantity;          
                }
            }

            // ADDING INFO FOR MAIN QUANTITY:
            var positionsWithShelfsWhichExists = _positionWithQuantityAndShelfsCollection
                .FirstOrDefault(x => (
                    x.position.OrderDelivery.ProcessIDPL == warehouseDocumentPosition.OrderDelivery.ProcessIDPL &&
                    x.position.OrderDelivery.ProcessIDDPL == warehouseDocumentPosition.OrderDelivery.ProcessIDDPL)
                    && ((x.order == null && order == null) || (x.order != null && order != null && x.order.Id == order.Id))
                    && x.IsReal
                    && x.WarehousePositionZones.Select(y => y.ZoneId).FirstOrDefault() == zone.Id);

            if (positionsWithShelfsWhichExists != null) // add quantity to existed positions
            {
                positionsWithShelfsWhichExists.position.Quantity += (double)mainQuantity;
                positionsWithShelfsWhichExists.WarehousePositionZones.FirstOrDefault().Quantity += (double)mainQuantity;
            }
            else // create new position with shelfs
            {
                _positionWithQuantityAndShelfsCollection.Add(new PositionWithQuantityAndShelfs()
                {
                    position = warehouseDocumentPosition,
                    WarehousePositionZones = new List<WarehousePositionZone>()
                {
                    new WarehousePositionZone()
                    {
                        WarehouseDocumentPosition = warehouseDocumentPosition,
                        Quantity = (double)mainQuantity,
                        Zone = zone
                    }
                },
                    order = order,
                    additionalPositionInfo = new AdditionalPositionInfo() { TotalOrderQuantity = excessQuantity, SupplySourceType = sourceType },
                    IsReal = true,
                    Packages = packages,
                    TargetSimpleDepartmentName = string.Empty,
                    GridRow = int.Parse(MaterialGrid.SelectedIndexes[0])
                });
            }



            // ADDING INFO FOR ADDITIONAL QUANTITY:
            if (warehouseDocumentPositionForKC != null)
            {
                var positionsWithShelfsWhichExistsAdditional = _positionWithQuantityAndShelfsCollection
                    .FirstOrDefault(x => x.position.Material.Id == warehouseDocumentPositionForKC.Material.Id
                    && x.position.UnitPrice == warehouseDocumentPositionForKC.UnitPrice
                    && x.order == null
                    && !x.IsReal
                    && x.WarehousePositionZones.Select(y => y.ZoneId).FirstOrDefault() == zone.Id);

                if (positionsWithShelfsWhichExistsAdditional != null) // add quantity to existed positions
                {
                    positionsWithShelfsWhichExists.position.Quantity += (double)mainQuantity;
                    positionsWithShelfsWhichExists.WarehousePositionZones.FirstOrDefault().Quantity += (double)mainQuantity;
                }
                else // create new positions whit shelfs
                {
                    _positionWithQuantityAndShelfsCollection.Add(new PositionWithQuantityAndShelfs()
                        {
                            position = warehouseDocumentPositionForKC,
                            WarehousePositionZones = new List<WarehousePositionZone>()
                        {
                            new WarehousePositionZone()
                            {
                              WarehouseDocumentPosition = warehouseDocumentPositionForKC,
                              Quantity = (double)excessQuantity,
                              Zone = zone //KC
                            }
                        },
                            order = null,
                            additionalPositionInfo = null,
                            IsReal = false,
                            GridRow = int.Parse(MaterialGrid.SelectedIndexes[0])
                            // Consider packages
                        }
                    );
                }
            }


            Session["ReceiveMaterial_ContractinPartyId"] = _contractingParty;
            Session["ReceiveMaterial_OrderNumberPZ"] = _orderNumberPZ;
            Session["ReceiveMaterial_positionWithQuantityAndShelfses"] = _positionWithQuantityAndShelfsCollection;

            var item = GetSelectedMaterialGridDataItem();
            MaterialGridUpdateAvailableQuantity(quantityFromUser, item);

            QuantityTextBox.Text = "";
            cmbShelf.Text = "";
            MaterialGrid.SelectedIndexes.Clear();
            MaterialsToReceiveRadGrid.Rebind();

            this.QuantityCustomValidator.Enabled = true;
            this.TextBox_PackageQuantity.Text = "0";
            this.TextBox_PackagesNumber.Text = "0";

            RadComboBox_PackageTypeName.Text = "";
        }

        private static WarehouseDocumentPositionPackage CreatePositionPackage(int? packageTypeNameId, int? packagesNumber, int? packageQuantity, Zone zone, WarehouseDS warehouseDs)
        {
            var package = new WarehouseDocumentPositionPackage();

            var packageType = warehouseDs.GetAllPackageTypes()
              .Where(p =>
                                                 p.PackageTypeName.Id == packageTypeNameId.Value
                && p.Quantity == packageQuantity)
              .FirstOrDefault();

            if (packageType == null)
            {
                var packageTypeName = warehouseDs.GetPackageTypeNameById(packageTypeNameId.Value);
                packageType = warehouseDs.CreateNewPackageType(packageTypeName.Id, packageQuantity.Value);
            }

            package.NumberOfPackages = packagesNumber.Value;
            warehouseDs.DetachEntity(packageType);
            package.PackageType = packageType;
            package.Zone = zone;

            return package;
        }

        //protected void MaterialGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        //{
        //    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //    {
        //        if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

        //        string materialToSearch = MaterialToSearchRTB.Text;
        //        string ContractingPartyNameToSearch = ContractingPartyName.Text;

        //        DateTime? startDate = null;
        //        DateTime? endDate = null;
        //        DateTime? RadDatePicker1_SelectedDate = RadDatePicker1.SelectedDate;
        //        DateTime? RadDatePicker2_SelectedDate = RadDatePicker2.SelectedDate;
        //        if (RadDatePicker1_SelectedDate != null)
        //        {
        //            startDate = (DateTime)RadDatePicker1.SelectedDate;
        //        }
        //        if (RadDatePicker2_SelectedDate != null)
        //        {
        //            endDate = (DateTime)RadDatePicker2.SelectedDate;
        //        }

        //        string quantityToSearch = QuantityRTB.Text.Replace(".", ",");
        //        string orderNumberToSearch = OrderNumberRTB.Text;
        //        string orderNumberPZToSearch = OrderNumberPZ_RTB.Text;

        //        int targetDeptIdSIMPLE = 0;
        //        int.TryParse(RadComboBox_TargetDepartmentSIMPLE.SelectedValue, out targetDeptIdSIMPLE);

        //        if (targetDeptIdSIMPLE < 0)
        //        {
        //            targetDeptIdSIMPLE = 0;
        //        }

        //        var materialsCandidatesToReceiveMaterialized = warehouseDs.GetWaitingForSupply(materialToSearch, ContractingPartyNameToSearch, startDate,
        //            endDate.Value, companyId, quantityToSearch, orderNumberToSearch, orderNumberPZToSearch, Convert.ToInt32(QuantityTolerance.SelectedValue), targetDeptIdSIMPLE);

        //        if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

        //        var materialsCandidatesToReceiveMaterializedOrdered = materialsCandidatesToReceiveMaterialized.OrderBy(p => p.DataKL); ;

        //        if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

        //        List<WaitingForSupply> materialsCandidatesToPresent = new List<WaitingForSupply>();

        //        foreach (var supply in materialsCandidatesToReceiveMaterializedOrdered)
        //        {
        //            WaitingForSupply waitingForSupplyForPresent = new WaitingForSupply();

        //            double usedQuantity = 0;

        //            //                    WarehouseDocumentPosition[] positionsAlreadyReceived = warehouseDs.GetPositionsAlreadyReceived();

        //            var positionWQAS = positionWithQuantityAndShelfses.FirstOrDefault(
        //                p =>
        //                    p.IsReal
        //                    &&

        //                p.position.Material.IdSIMPLE == supply.IDsim

        //                &&

        //                p.position.OrderDelivery.ProcessIDPL == supply.IDpl

        //                &&

        //                p.position.OrderDelivery.ProcessIDDPL == supply.IDdpl

        //                &&

        //                //p.position.OrderDelivery.OrderNumberFromPZ.Trim() == supply.NazwSK.Trim()

        //                p.position.IdZF == supply.IdZF

        //                &&

        //                //&&

        //                (
        //                    (p.position.OrderDelivery != null && !p.position.OrderDelivery.ForDepartment && supply.Flaga == 1)
        //                    ||
        //                    ((p.position.OrderDelivery == null || p.position.OrderDelivery.ForDepartment) && (supply.Flaga == 2 || supply.Flaga == 3)))
        //                )
        //                ;

        //            if (positionWQAS != null) usedQuantity = positionWQAS.position.Quantity;

        //            waitingForSupplyForPresent.ID = supply.ID;
        //            waitingForSupplyForPresent.IDpl = supply.IDpl;
        //            waitingForSupplyForPresent.IDdpl = supply.IDdpl;
        //            waitingForSupplyForPresent.Cena = supply.Cena;
        //            waitingForSupplyForPresent.kontrachent_id = supply.CorrectKontrahentId; //supply.kontrachent_id;
        //            waitingForSupplyForPresent.DataKL = supply.DataKL;
        //            waitingForSupplyForPresent.IDsim = supply.IDsim;
        //            waitingForSupplyForPresent.ContractingPartyName = supply.ContractingPartyName;
        //            waitingForSupplyForPresent.NazwSK = supply.NazwSK;
        //            waitingForSupplyForPresent.MaterialName = supply.MaterialName;
        //            waitingForSupplyForPresent.IndexSIMPLE = supply.IndexSIMPLE;
        //            waitingForSupplyForPresent.IloscZam = supply.IloscZam - (decimal)usedQuantity;
        //            waitingForSupplyForPresent.IloscZamBrutto = supply.IloscZamBrutto;
        //            waitingForSupplyForPresent.UnitName = supply.UnitName;
        //            waitingForSupplyForPresent.NRp = supply.CorrectOrderNumber;
        //            waitingForSupplyForPresent.Flaga = supply.Flaga;
        //            waitingForSupplyForPresent.SourceType = supply.SourceType;
        //            waitingForSupplyForPresent.IdZF = supply.IdZF;
        //            waitingForSupplyForPresent.SourceClass = supply.SourceType == 3 ? "WEWN." : " ";

        //            waitingForSupplyForPresent.Dzial = supply.Dzial;

        //            waitingForSupplyForPresent.SimpleDepartmentName = supply.SimpleDepartmentName;
        //            waitingForSupplyForPresent.K3DepartmentId = supply.K3DepartmentId;
        //            waitingForSupplyForPresent.K3DepartmentName = supply.K3DepartmentName;
        //            waitingForSupplyForPresent.K3MachineId = supply.K3MachineId;
        //            waitingForSupplyForPresent.K3MachineName = supply.K3MachineName;

        //            materialsCandidatesToPresent.Add(waitingForSupplyForPresent);
        //        }

        //        if (HttpContext.Current.IsDebuggingEnabled) Debug.WriteLine(DateTime.Now.ToLongTimeString());

        //        int i = 1;

        //        var materials = from p in materialsCandidatesToPresent
        //                        select
        //                            new
        //                            {
        //                                RowNumber = i++,
        //                                ProcessHeaderId = p.ID,
        //                                ProcessIDPL = p.IDpl,
        //                                ProcessIDDPL = p.IDdpl,
        //                                UnitPrice = p.Cena,
        //                                ContractingPartyId = p.kontrachent_id,
        //                                SupplyDate = p.DataKL,
        //                                IdSim = p.IDsim,
        //                                ContractingParty = p.ContractingPartyName,
        //                                OrderNumberPZ = p.NazwSK,
        //                                MaterialName = p.MaterialName,
        //                                IndexSIMPLE = p.IndexSIMPLE,
        //                                Quantity = Convert.ToDouble(p.IloscZam),
        //                                QuantityOrdered = Convert.ToDouble(p.IloscZamBrutto),
        //                                QuantityDelivered = Convert.ToDouble(p.UsedQuantity),
        //                                UnitName = p.UnitName,
        //                                OrderNumber = !string.IsNullOrEmpty(p.NRp) ? p.NRp : (p.Dzial > 0 ? p.SimpleDepartmentName : "STAN"),
        //                                ForDepartment = p.Flaga == 2 || p.Flaga == 3 ? 1 : 0,
        //                                SourceType = p.SourceType,
        //                                IdZF = p.IdZF,
        //                                SourceClass = p.SourceClass,

        //                                SimpleDepartmentId = p.Dzial,
        //                                DepartmentName = p.SimpleDepartmentName,
        //                                K3DepartmentId = p.K3DepartmentId,
        //                                K3MachineId = p.K3MachineId
        //                            };
        //        MaterialGrid.DataSource = materials;
        //protected void CustomValidator_Unit_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (!string.IsNullOrEmpty(RadComboBox_Unit.SelectedValue) && int.Parse(RadComboBox_Unit.SelectedValue) > 0)
        //    {
        //        args.IsValid = true;
        //    }
        //    else
        //    {
        //        args.IsValid = false;
        //    }
        //protected void NewPackageType_CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    try
        //    {
        //        int quantity = int.Parse(TextBox_PackageQuantity.Text);
        //        int unitId = int.Parse(RadComboBox_Units.SelectedValue);
        //        //RadComboBox_Units
        //        if (unitId > 0)
        //        {
        //            args.IsValid = true;
        //        }
        //        else
        //        {
        //            args.IsValid = false;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        args.IsValid = false;
        //    }
        //    //args.IsValid = true;
        //}

        //protected void NewPackageType_CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    try
        //    {
        //        int quantity = int.Parse(TextBox_PackageQuantity.Text);
        //        int unitId = int.Parse(RadComboBox_Units.SelectedValue);
        //        string packageTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(RadComboBox_PackageTypeNameName.Text.Trim().ToLower());

        //        if (!string.IsNullOrEmpty(packageTypeName))
        //        {
        //            using (var warehouseDs = new WarehouseDS())
        //            {
        //                string unitShortName = warehouseDs.GetUnitById(unitId).ShortName;
        //                string packageFullName = packageTypeName + " " + quantity + "" + unitShortName;
        //                args.IsValid = !warehouseDs.CheckIfPackageTypeExists(unitId, quantity, packageFullName);
        //            }
        //        }
        //        else
        //        {
        //            args.IsValid = false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        args.IsValid = false;
        //    }

        //    //args.IsValid = true;
        //}

        //protected void AddPackageTypeClick(object sender, EventArgs e)
        //{
        //    int userId;
        //    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
        //    {
        //        if (IsValid)
        //        {
        //            try
        //            {
        //                int quantity = int.Parse(TextBox_PackageQuantity.Text);
        //                int unitId = int.Parse(RadComboBox_Units.SelectedValue);
        //                string packageTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(RadComboBox_PackageTypeNameName.Text.Trim().ToLower());

        //                using(WarehouseDS warehouseDs = new WarehouseDS())
        //                {
        //                    string unitShortName = warehouseDs.GetUnitById(unitId).ShortName;
        //                    string packageFullName = packageTypeName + " " + quantity + "" + unitShortName;

        //                    warehouseDs.CreateNewPackageType(unitId, quantity, packageFullName);

        //                    RadComboBox_PackageTypeName.DataBind();

        //                    TextBox_PackageQuantity.Text = "";
        //                    //TextBox_PackageTypeName.Text = "";
        //                    //RadComboBox_PackageTypeNameName.DataBind();

        private void startNodeInEditMode(string nodeValue)
        {
            ////find the node by its Value and edit it when page loads
            //string js = "Sys.Application.add_load(editNode); function editNode(){ ";
            //js += "var tree = $find(\"" + RadTreeView1.ClientID + "\");";
            //js += "var node = tree.findNodeByValue('" + nodeValue + "');";
            //js += "if (node) node.startEdit();";
            //js += "Sys.Application.remove_load(editNode);};";
            //RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "nodeEdit", js, true);
        }

        private void MaterialGridUpdateAvailableQuantity(decimal quantityFromUser, GridDataItem item)
        {
            item["Quantity"].Text = (item.GetCellValueAs<decimal>("Quantity") - quantityFromUser).ToString();
            item["QuantityAdded"].Text = (item.GetCellValueAs<decimal>("QuantityAdded") + quantityFromUser).ToString();
        }

        private GridDataItem GetSelectedMaterialGridDataItem()
        {
            return (GridDataItem)MaterialGrid.SelectedItems[0];
        }
    }
}
