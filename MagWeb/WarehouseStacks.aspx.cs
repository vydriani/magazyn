using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
//using Client.Domain.Logging;
using Mag.Domain.Model;
using Telerik.Web.UI;
using System.Diagnostics.Contracts;
using System.Data.Entity;
    

namespace MagWeb
{
    public partial class WarehouseStacks : FeaturedSystemWebUIPage
    {
        public bool InventoryMode = false;
        //private PerformanceDebugger perfDebug = new PerformanceDebugger();

        private List<WarehouseInventoryPosition> warehouseInventoryPositions;

#if DEBUG

        public override System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                SessionData.TestSessionFor<List<WarehouseInventoryPosition>, int>("INVPositions", wip => wip.Count);
                return base.Session;
            }
        }

#endif

        protected void AdjustStacksClick(object sender, EventArgs e)
        {
            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            GridDataItem item = (GridDataItem)StackGrid.SelectedItems[0];//get selected row

            string materialIndex = item["IndexSIMPLE"].Text;
            string zoneName = item["ShelfName"].Text;
            string orderNumber = item["OrderNumber"].Text;
            string damageStatus = item["DamageStatus"].Text;

            if (zoneName == "&nbsp;")
                zoneName = "";

            if (orderNumber == "&nbsp;")
                orderNumber = "";

            Material material;
            Zone zone;
            Order order;

            using (var warehouseDs = new WarehouseDS())
            {
                material = warehouseDs.GetMaterialByIndex(materialIndex.Trim(), companyId);
                zone = !string.IsNullOrEmpty(zoneName) ? warehouseDs.GetZoneByName(zoneName) : null;
                order = !string.IsNullOrEmpty(orderNumber) ? warehouseDs.GetOrderByNumber(orderNumber) : null;
            }
            int? warehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];

            Response.Redirect("BalanceSheet.aspx?o=x&material=" + material.Id + "&zone=" + (zone != null ? zone.Id : 0) + "&order=" + (order != null ? order.Id : 0) + "&status=" + damageStatus); // &w=" + warehouseId.Value + "

            //var x = 1;
        }
        
        protected void ButtonCancelClick(object sender, EventArgs e)
        {
            var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
            GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
            detailTableView.Rebind();
            HideRelocationPanel();
            HideFreeMaterialPanel();
            HideInventoryMaterialPanel();
        }

        protected void ButtonChangeLocalizationExecuteClick(object sender, EventArgs e)
        {
            Page.Validate("RelocateMaterialGroup");

            if (IsValid)
            {
                var maxQuantity = double.Parse(HiddenField_QuickOp_MaxQuantity.Value);
                //var enteredQuantity = double.Parse(RelocationQuantityTextBox.Text);

                int warehouseId = int.Parse(HiddenField_QuickOp_WarehouseId.Value);
                int materialId = int.Parse(HiddenField_QuickOp_MaterialId.Value);
                int orderId = int.Parse(HiddenField_QuickOp_OrderId.Value);
                int departmentId = int.Parse(HiddenField_QuickOp_DepartmentId.Value);
                int processIdpl = int.Parse(HiddenField_QuickOp_ProcessIdpl.Value);
                int shelfId = int.Parse(HiddenField_QuickOp_ShelfSrcId.Value);
                bool isOrder = bool.Parse(HiddenField_QuickOp_IsOrder.Value);
                double newquantity = double.Parse(RelocationQuantityTextBox.Text.Replace(".", ","));

                if (departmentId > 0)
                {
                    WebTools.AlertMsgAdd("Nie można (jeszcze) operować na materiale przypisanym do działu!");
                    HideRelocationPanel();
                    return;
                }

                if (true) //(newquantity <= maxQuantity) na razie przy zamówieniach zmiana ilości jest zablokowana
                {
                    int userId = 0;
                    int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId);

                    using (var warehouseDs = new WarehouseDS())
                    {
                        Zone zone = warehouseDs.GetShelfByName(RelocationTargetZoneRCB.Text);//.GetShelfByNameDataOnly(RelocationTargetZoneRCB.Text);

                        string result = "";
                        if (!string.IsNullOrEmpty(RelocationTargetZoneRCB.Text) && zone != null)
                        {
                            result = warehouseDs.RelocateMaterialInWarehouse(
                            //result = warehouseDs.RelocateMaterialInWarehouseWithOutNewDocument(
                                warehouseId,
                                materialId,
                                orderId,
                                processIdpl != 0 && processIdpl < 2000000 ? processIdpl : (int?)null,
                                shelfId,
                                zone.Id,
                                newquantity,
                                warehouseDs.GetPersonForSystemUser(userId).Id,
                                isOrder
                                );

                            //result = warehouseDs.RelocateMaterialInWarehouse(
                            //    warehouseId,
                            //    materialId,
                            //    orderId,
                            //    processIdpl != 0 && processIdpl < 2000000 ? processIdpl : (int?)null,
                            //    shelfId,
                            //    zone.Id,
                            //    newquantity,
                            //    warehouseDs.GetPersonForSystemUser(userId).Id,
                            //    isOrder
                            //    );
                        }
                        else
                        {
                            result = "Błąd! Nie wybrano docelowej lokalizacji.";
                        }

                        WebTools.AlertMsgAdd(result);
                    }
                }
                else
                {
                    WebTools.AlertMsgAdd("Błąd! Nie można przenieść więcej materiału niż jest na lokalizacji.");
                }

                var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
                GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
                detailTableView.Rebind();

                HideRelocationPanel();
            }
        }

        protected void ButtonFreeMaterialExecuteClick(object sender, EventArgs e)
        {
            Page.Validate("FreeMaterialGroup");

            if (IsValid)
            {
                int warehouseId = int.Parse(HiddenField_QuickOp_WarehouseId.Value);
                int materialId = int.Parse(HiddenField_QuickOp_MaterialId.Value);
                int orderId = int.Parse(HiddenField_QuickOp_OrderId.Value);
                int departmentId = int.Parse(HiddenField_QuickOp_DepartmentId.Value);
                int? processIdpl = int.Parse(HiddenField_QuickOp_ProcessIdpl.Value);
                int shelfId = int.Parse(HiddenField_QuickOp_ShelfSrcId.Value);
                bool isOrder = bool.Parse(HiddenField_QuickOp_IsOrder.Value);
                var newquantity = decimal.Parse(FreeMaterialQuantityTextBox.Text.Replace(".", ","));
                decimal maxQuantity = decimal.Parse(HiddenField_QuickOp_MaxQuantity.Value);
                bool isGave = bool.Parse(HiddenField_QuickOp_IsGave.Value);
                bool isOrderAndStack = bool.Parse(HiddenField_QuickOp_IsOrderAndStack.Value);

                // uwalnianie materiału przypisanego na dział!
//#if !DEBUG
//                if (departmentId > 0)
//                {
//                    WebTools.AlertMsgAdd("Nie można (jeszcze) operować na materiale przypisanym do działu!");
//                    HideFreeMaterialPanel();
//                    return;
//                }
//#endif


                //if (true) //(newquantity <= maxQuantity)
                if (newquantity <= maxQuantity)
                {
                    int userId = 0;
                    int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId);

                    using (var warehouseDs = new WarehouseDS())
                    {
                        //Zone zone = warehouseDs.GetShelfByNameDataOnly(RelocationTargetZoneRCB.Text);
                        string result = "";

                        result = warehouseDs.FreeMaterialInWarehouse(
                            warehouseId,
                            materialId,
                            orderId,
                            processIdpl != 0 && processIdpl < 2000000 ? processIdpl : (int?)null,
                            shelfId,
                            newquantity,
                            warehouseDs.GetPersonForSystemUser(userId).Id,
                            isOrder,
                            isGave,
                            isOrderAndStack);

                        WebTools.AlertMsgAdd(result);
                    }
                    
                    
                    if ((isOrder && !isOrderAndStack) || departmentId>0)
                    {
                        #region inventary
                        //-------------------------------


                        InventoryMaterialButtonClick(senderSession, e);
                        processIdpl = null;
                        int? processIddpl = null;
                        int process, process2;


                        processIdpl = int.TryParse(HiddenField_QuickOp_ProcessIdpl.Value, out process) ? process : processIdpl;
                        processIddpl = int.TryParse(HiddenField_QuickOp_ProcessIddpl.Value, out process2) ? process2 : processIddpl;

                        int? wdpId = null;
                        string hiddenFieldQuickOpMaxQuantityValStr = HiddenField_QuickOp_MaxQuantity.Value.Replace(".", ",");
                        double quantityOnStack = double.Parse(hiddenFieldQuickOpMaxQuantityValStr);

                        //string inventoryQuantityTextBoxValStr = String.Empty;

                        //inventoryQuantityTextBoxValStr = InventoryQuantityTextBox.Text.Replace(".", ",");

                        //double quantityObserved = double.Parse(InventoryQuantityTextBox.Text);

                        Material material = null;
                        decimal unitPrice = 0;
                        double quantity = 0;
                        Order order = null;
                        decimal valuePerUnit = 0;
                        double testQ = 0;
                        bool skipInv= false;

                        Zone shelf;

                        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {

                            if (departmentId > 0)
                            {
                                #region uwalnianie z działu
                                IQueryable<CurrentStackInDocument> stackPosQ = warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments.AsQueryable();
                                stackPosQ = stackPosQ.Where(x => x.MaterialId == materialId).AsQueryable();
                                stackPosQ = stackPosQ.Where(x => x.WarehouseTargetId == warehouseId).AsQueryable();
                                stackPosQ = stackPosQ.Where(x => x.ShelfId == shelfId).AsQueryable();

                                // materiał mógł mieć przypisanie ale zostął uwolniony, szukamy dodatkowo w zamówieniach..
                                IQueryable<WarehouseDocumentPosition> stackQuantity = warehouseDs.GetOrderedStackList(warehouseId, materialId, orderId, shelfId).AsQueryable();

                                // order
                                if (orderId > 0)
                                {
                                    stackPosQ = stackPosQ.Where(p => p.OrderId == orderId);
                                    stackQuantity = stackQuantity.Where(p => p.OrderId == orderId);
                                    order = warehouseDs.GetOrderByIdDataOnly(orderId);
                                }
                                else
                                {
                                    stackPosQ = stackPosQ.Where(p => !p.OrderId.HasValue);
                                    stackQuantity = stackQuantity.Where(p => !p.OrderId.HasValue);
                                }

                                //ProcessIdpl
                                if (processIdpl.HasValue)
                                {
                                    stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == processIdpl);
                                    stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl);
                                }
                                else
                                {
                                    stackPosQ = stackPosQ.Where(p => !p.ProcessIdpl.HasValue);
                                    stackQuantity = stackQuantity.Where(p => !p.ProcessIdpl.HasValue);
                                }

                                //processIddpl
                                if (processIddpl.HasValue)
                                {
                                    stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == processIddpl);
                                    stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl);
                                }
                                else
                                {
                                    stackPosQ = stackPosQ.Where(p => !p.ProcessIddpl.HasValue);
                                    stackQuantity = stackQuantity.Where(p => !p.ProcessIddpl.HasValue);
                                }

                                //if (processIdpl == 0)
                                //{
                                //    stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == processIdpl || !p.ProcessIdpl.HasValue);
                                //    stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl || !p.ProcessIdpl.HasValue);
                                //}
                                //else
                                //{
                                //    if (processIdpl > 0)
                                //    {
                                //        stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == processIdpl);
                                //        stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl);
                                //    }
                                //    else
                                //    {
                                //        stackPosQ = stackPosQ.Where(p => !p.ProcessIdpl.HasValue);
                                //        stackQuantity = stackQuantity.Where(p => !p.ProcessIdpl.HasValue);
                                //    }
                                //}


                                //if (processIddpl == 0 || !processIddpl.HasValue)
                                //{
                                //    stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == processIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                                //    stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                                //}
                                //else
                                //{
                                //    if (processIddpl > 0)
                                //    {
                                //        stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == processIddpl);
                                //        stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl);
                                //    }
                                //    else
                                //    {
                                //        stackPosQ = stackPosQ.Where(p => !p.ProcessIddpl.HasValue);
                                //        stackQuantity = stackQuantity.Where(p => !p.ProcessIddpl.HasValue);
                                //    }
                                //}

                                if (!stackPosQ.Any())
                                {
                                    if (!stackQuantity.Any())
                                        skipInv = true;//throw new Exception("Brak dokumentów na stanie dla wybranego materiału.");
                                    else
                                    {
                                        {
                                            var stackPos = stackQuantity.First();
                                            valuePerUnit = stackPos.UnitPrice;
                                            wdpId = stackPos.Id;
                                            testQ = stackPos.Quantity;
                                        }
                                    }
                                }
                                else
                                {
                                    var stackP = stackPosQ.First();
                                    valuePerUnit = stackP.UnitPrice;
                                    wdpId = stackP.PositionId;
                                    testQ = (double)stackP.Quantity;

                                    //quantityOnStack = (double)stackP.Quantity>=(double)stackP.QuantityGross
                                    //    ?(double)stackP.Quantity
                                    //    :(double)stackP.QuantityGross;
                                }
                                if (skipInv == false)
                                {
                                    quantity = quantityOnStack - (double)newquantity;

                                    if (quantity == testQ)
                                    {
                                        skipInv = true;
                                    }

                                    if (quantity == 0 &&  testQ!=quantityOnStack)
                                    {
                                        quantityOnStack = testQ;
                                    }
                                }


                                #endregion
                            }
                            else
                            {

                                #region anulowanie zlecenia
                                // materiał mógł mieć przypisanie ale zostął uwolniony, szukamy dodatkowo w zamówieniach..
                                IQueryable<WarehouseDocumentPosition> stackQuantity = warehouseDs.GetOrderedStackList(warehouseId, materialId, orderId, shelfId).AsQueryable();
                                stackQuantity = stackQuantity.Where(p => !p.OrderId.HasValue);
                                stackQuantity = stackQuantity.Where(p => !p.ProcessIdpl.HasValue);
                                stackQuantity = stackQuantity.Where(p => !p.ProcessIddpl.HasValue);


                                // materiał mógł mieć przypisanie ale zostął uwolniony, szukamy dodatkowo w zamówieniach..
                                IQueryable<CurrentStackInDocument> stackQuantity2 = warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments.AsQueryable();
                                stackQuantity2 = stackQuantity2.Where(p => !p.OrderId.HasValue);
                                stackQuantity2 = stackQuantity2.Where(p => !p.ProcessIdpl.HasValue);
                                stackQuantity2 = stackQuantity2.Where(p => !p.ProcessIddpl.HasValue);
                                stackQuantity2 = stackQuantity2.Where(x => x.MaterialId == materialId).AsQueryable();
                                stackQuantity2 = stackQuantity2.Where(x => x.WarehouseTargetId == warehouseId).AsQueryable();
                                stackQuantity2 = stackQuantity2.Where(x => x.ShelfId == shelfId).AsQueryable();


                                if (!stackQuantity.Any())
                                {
                                    if (!stackQuantity2.Any())
                                    {
                                        throw new Exception("Brak dokumentów na stanie dla wybranego materiału.");
                                    }
                                    else
                                    {
                                        var stackPos = stackQuantity2.First();
                                        valuePerUnit = stackPos.UnitPrice;
                                        wdpId = stackPos.PositionId;
                                    }

                                }
                                else
                                {
                                    var stackPos = stackQuantity.First();
                                    valuePerUnit = stackPos.UnitPrice;
                                    wdpId = stackPos.Id;
                                }

                                quantity = quantityOnStack;
                                processIdpl = null;
                                processIddpl = null;
                                #endregion
                            }

                            if (!skipInv)
                            {
                                //processIdpl2 = (processIdpl >= 0 && processIdpl <= 2000000) ? processIdpl : (int?)null;
                                material = warehouseDs.WarehouseEntitiesTest.Materials.Include("Unit").First(p => p.Id == materialId);
                                unitPrice = valuePerUnit;

                                shelf = warehouseDs.GetZoneByIdDataOnly(shelfId);


                                WarehouseInventoryPosition inventoryPositionWhichExists =
                                    warehouseInventoryPositions.FirstOrDefault(
                                        p =>
                                        p.WarehouseId == warehouseId
                                        &&
                                        p.MaterialId == material.Id
                                        &&
                                        p.UnitPrice == unitPrice
                                        &&
                                        (p.Order == null && order == null || (p.Order != null && order != null && p.OrderId == order.Id))
                                        && p.ZoneId == shelfId
                                        &&
                                        p.ProcessIdpl == null
                                        &&
                                        p.ProcessIddpl == null
                                        );

                                using (var warehouseDs2 = new WarehouseDS())
                                {
                                    var warehouseEntities = warehouseDs2.WarehouseEntitiesTest;
                                    var newWarehouseInventoryPosition = new WarehouseInventoryPosition()
                                {
                                    IsActive = true,
                                    AddedTime = DateTime.Now,
                                    ObservationTime = DateTime.Now,
                                    //WarehouseDocumentPositionId = (isOrder == true || (wdpId.HasValue && wdpId > 0)) ? wdpId : null,
                                    WarehouseDocumentPositionId = wdpId,
                                    Zone = (shelf != null ? warehouseEntities.Zones.FirstOrDefault(p => p.Id == shelf.Id) : null),
                                    Order = (order != null ? warehouseEntities.Orders.FirstOrDefault(p => p.Id == order.Id) : null),
                                    DepartmentId = null,
                                    WarehouseId = warehouseId,
                                    QuantityBefore = quantityOnStack,
                                    Material = (material != null ? warehouseEntities.Materials.FirstOrDefault(p => p.Id == material.Id) : null),
                                    Quantity = quantity,
                                    UnitPrice = unitPrice,
                                    ProcessIdpl = processIdpl,
                                    ProcessIddpl = processIddpl
                                };

                                    warehouseDs2.WarehouseEntitiesTest.AddToWarehouseInventoryPositions(newWarehouseInventoryPosition);

                                    warehouseDs2.WarehouseEntitiesTest.SaveChanges();
                                    warehouseInventoryPositions.Add(newWarehouseInventoryPosition);
                                }
                            }

                        }

                        HideInventoryMaterialPanel(true);

                        if (!skipInv)
                        {
                            Session["INVPositions"] = warehouseInventoryPositions;

                            //-- ZATWIERDZENIE POZYCJI DO INWENTARYZACJI
                            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                            {
                                var invDocument = warehouseDs.Inventory(warehouseId, warehouseDs.GetPersonForSystemUser(userId).Id, warehouseInventoryPositions, null, false);
                                Session["INVPositions"] = null;
                                warehouseInventoryPositions = new List<WarehouseInventoryPosition>();
                            }
                            //-------------------------------
                        }
                        #endregion
                    }
                }
                else
                {
                    WebTools.AlertMsgAdd("Błąd! Nie można przenieść więcej materiału niż jest na lokalizacji.");
                }

                var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
                if (detailTableViewCtrl != null)
                {
                    GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
                    detailTableView.Rebind();

                    //detailTableView.Rebind();
                    //detailTableView.ParentItem.Expanded = false;
                    //detailTableView.ParentItem.Expanded = true;
                }
                HideFreeMaterialPanel();
                
            }
        }

        protected void ButtonInventoryMaterialExecuteClick(object sender, EventArgs e, bool AfterFreeMaterial)
        {

            Page.Validate("InventoryMaterialGroup");

            if (IsValid)
            {
                int warehouseId = int.Parse(HiddenField_QuickOp_WarehouseId.Value);
                int materialId = int.Parse(HiddenField_QuickOp_MaterialId.Value);
                int orderId = AfterFreeMaterial ? 0 : int.Parse(HiddenField_QuickOp_OrderId.Value);
                
                int departmentId = int.Parse(HiddenField_QuickOp_DepartmentId.Value);
                //int? processIdpl = int.Parse(HiddenField_QuickOp_ProcessIdpl.Value);
                //int? processIddpl = int.Parse(HiddenField_QuickOp_ProcessIddpl.Value);
                int? processIdpl = null;
                int? processIddpl = null;

                int process, process2;


                processIdpl = int.TryParse(HiddenField_QuickOp_ProcessIdpl.Value, out process) ? process : processIdpl;
                processIddpl = int.TryParse(HiddenField_QuickOp_ProcessIddpl.Value, out process2) ? process2 : processIddpl;

                int shelfId = int.Parse(HiddenField_QuickOp_ShelfSrcId.Value);
                //bool isOrder = AfterFreeMaterial ? false : bool.Parse(HiddenField_QuickOp_IsOrder.Value);
                bool isOrder = false;

                int? wdpId = 0;
                //if (isOrder)
                    wdpId = int.Parse(HiddenField_QuickOp_WarehouseDocumentPositionId.Value);

               // processIddpl = (processIddpl == 0 ? null : processIddpl);

                string hiddenFieldQuickOpMaxQuantityValStr = HiddenField_QuickOp_MaxQuantity.Value.Replace(".", ",");
                double quantityOnStack = double.Parse(hiddenFieldQuickOpMaxQuantityValStr);

                string inventoryQuantityTextBoxValStr = String.Empty;

                if (isOrder)
                {
                    inventoryQuantityTextBoxValStr = HiddenField_QuickOp_MaxQuantity.Value.Replace(".", ",");
                }
                else
                {
                    inventoryQuantityTextBoxValStr = InventoryQuantityTextBox.Text.Replace(".", ",");
                }

                #region inventory 
                //if (isOrder)
                if (true)
                {
                    double quantityObserved = double.Parse(inventoryQuantityTextBoxValStr);

                    Material material = null;
                    decimal unitPrice = 0;
                    double quantity = 0;
                    int? processIdpl2 = null;
                    Order order = null;
                    decimal valuePerUnit = 0;

                    Zone shelf;

                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        // TODO: Take care of DepartmentId !!! && s.DepartmentId == (departmentId > 0 ? departmentId : (int?)null)

                        if (isOrder)
                        {
                            IQueryable<WarehouseDocumentPosition> stackQuantity = warehouseDs.GetOrderedStackList(warehouseId, materialId, orderId, shelfId).AsQueryable();

                            if (orderId > 0)
                            {
                                stackQuantity = stackQuantity.Where(p => p.OrderId == orderId);
                            }
                            else
                            {
                                stackQuantity = stackQuantity.Where(p => !p.OrderId.HasValue);
                            }

                            if (processIdpl == 0)
                            {
                                stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl || !p.ProcessIdpl.HasValue);
                            }
                            else
                            {
                                if (processIdpl > 0)
                                {
                                    stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl);
                                }
                                else
                                {
                                    stackQuantity = stackQuantity.Where(p => !p.ProcessIdpl.HasValue);
                                }
                            }


                            if (processIddpl == 0 || !processIddpl.HasValue)
                            {
                                stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                            }
                            else
                            {
                                if (processIddpl > 0)
                                {
                                    stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl);
                                }
                                else
                                {
                                    stackQuantity = stackQuantity.Where(p => !p.ProcessIddpl.HasValue);
                                }
                            }

                            stackQuantity = stackQuantity.Where(p => Math.Abs(p.Quantity) == Math.Abs(quantityOnStack));

                            if (false) // TODO: Check
                            {
                                if (departmentId > 0)
                                {
                                    stackQuantity = stackQuantity.Where(p => p.DepartmentId == departmentId);
                                }
                                else
                                {
                                    stackQuantity = stackQuantity.Where(p => !p.DepartmentId.HasValue);
                                }
                            }

                            if (!stackQuantity.Any())
                            {
                                throw new Exception("Brak dokumentów na stanie dla wybranego materiału.");
                            }

                            var stackPos = stackQuantity.First();

                            valuePerUnit = stackPos.UnitPrice;
                        }
                        else
                        {
                            IQueryable<CurrentStackInDocument> stackPosQ = warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments.AsQueryable();
                            stackPosQ = stackPosQ.Where(x => x.MaterialId == materialId).AsQueryable();
                            stackPosQ = stackPosQ.Where(x => x.WarehouseTargetId == warehouseId).AsQueryable();
                            stackPosQ = stackPosQ.Where(x => x.ShelfId == shelfId).AsQueryable();

                            // materiał mógł mieć przypisanie ale zostął uwolniony, szukamy dodatkowo w zamówieniach..
                            IQueryable<WarehouseDocumentPosition> stackQuantity = warehouseDs.GetOrderedStackList(warehouseId, materialId, orderId, shelfId).AsQueryable();

                            if (orderId > 0)
                            {
                                stackPosQ = stackPosQ.Where(p => p.OrderId == orderId);
                                stackQuantity = stackQuantity.Where(p => p.OrderId == orderId);
                            }
                            else
                            {
                                stackPosQ = stackPosQ.Where(p => !p.OrderId.HasValue);
                                stackQuantity = stackQuantity.Where(p => !p.OrderId.HasValue);
                            }

                            if (processIdpl == 0)
                            {
                                stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == processIdpl || !p.ProcessIdpl.HasValue);
                                stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl || !p.ProcessIdpl.HasValue);
                            }
                            else
                            {
                                if (processIdpl > 0)
                                {
                                    stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == processIdpl);
                                    stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == processIdpl);
                                }
                                else
                                {
                                    stackPosQ = stackPosQ.Where(p => !p.ProcessIdpl.HasValue);
                                    stackQuantity = stackQuantity.Where(p => !p.ProcessIdpl.HasValue);
                                }
                            }

                            if (processIddpl == 0 || !processIddpl.HasValue)
                            {
                                stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == processIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                                stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                            }
                            else
                            {
                                if (processIddpl > 0)
                                {
                                    stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == processIddpl);
                                    stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == processIddpl);
                                }
                                else
                                {
                                    stackPosQ = stackPosQ.Where(p => !p.ProcessIddpl.HasValue);
                                    stackQuantity = stackQuantity.Where(p => !p.ProcessIddpl.HasValue);
                                }
                            }

                            if (wdpId.HasValue && wdpId > 0)
                            {

                                stackPosQ = stackPosQ.Where(x => x.PositionId == wdpId);
                                stackQuantity = stackQuantity.Where(x => x.Id == wdpId);
                            }


                            if (false) // TODO: Check
                            {
                                if (departmentId > 0)
                                {
                                    stackPosQ = stackPosQ.Where(p => p.DepartmentId == departmentId);
                                }
                                else
                                {
                                    stackPosQ = stackPosQ.Where(p => !p.DepartmentId.HasValue);
                                }
                            }

                            if (!stackPosQ.Any())
                            {
                                if (!stackQuantity.Any())
                                    throw new Exception("Brak dokumentów na stanie dla wybranego materiału.");
                                else
                                {
                                    //if (AfterFreeMaterial)
                                    {
                                        var stackPos = stackQuantity.First();
                                        valuePerUnit = stackPos.UnitPrice;
                                    }
                                    //else
                                    //{
                                    //    var stackPos = stackQuantity.First();
                                    //    valuePerUnit = stackPos.UnitPrice;
                                    //    isOrder = true;
                                    //    wdpId = int.Parse(HiddenField_QuickOp_WarehouseDocumentPositionId.Value);
                                    //}
                                }
                            }
                            else
                            {
                                var stackP = stackPosQ.First();
                                valuePerUnit = stackP.UnitPrice;
                            }
                        }

                        material = warehouseDs.WarehouseEntitiesTest.Materials.Include("Unit").First(p => p.Id == materialId);
                        unitPrice = valuePerUnit;
                        quantity = quantityObserved;


                        processIdpl2 = (processIdpl >= 0 && processIdpl <= 2000000) ? processIdpl : (int?)null;

                        if (orderId > 0)
                        {
                            order = warehouseDs.GetOrderByIdDataOnly(orderId);
                        }

                        shelf = warehouseDs.GetZoneByIdDataOnly(shelfId);


                        WarehouseInventoryPosition inventoryPositionWhichExists =
                            warehouseInventoryPositions.FirstOrDefault(
                                p =>
                                p.WarehouseId == warehouseId
                                &&
                                p.MaterialId == material.Id
                                &&
                                p.UnitPrice == unitPrice
                                &&
                                (p.Order == null && order == null || (p.Order != null && order != null && p.OrderId == order.Id))
                                &&
                                p.ZoneId == shelfId
                                &&
                                p.ProcessIdpl == processIdpl2
                                &&
                                p.ProcessIddpl == processIddpl
                                );

                        if (inventoryPositionWhichExists != null) //positionsWithShelfsToAdd.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id && p.position.UnitPrice == warehouseDocumentPosition.UnitPrice && p.shelf.Id == shelfId) == 1)
                        {
                            throw new Exception("Ta pozycja została już dodana. Nie można jej dodać dwukrotnie.");
                        }
                        else
                        {
                            if (CheckBox_ExportToSIMPLE.Checked && quantityOnStack > quantity)
                            {
                                WebTools.AlertMsgAdd("Eksport do SIMPLE niemożliwy dla ujemnej różnicy");
                                CheckBox_ExportToSIMPLE.Checked = false;
                                return;
                            }
                            else
                            {
                                using (var warehouseDs2 = new WarehouseDS())
                                {
                                    var warehouseEntities = warehouseDs2.WarehouseEntitiesTest;
                                    var newWarehouseInventoryPosition = new WarehouseInventoryPosition()
                                {
                                    IsActive = true,
                                    AddedTime = DateTime.Now,
                                    ObservationTime = DateTime.Now,
                                    WarehouseDocumentPositionId = (isOrder == true || (wdpId.HasValue && wdpId > 0)) ? wdpId : null,
                                    Zone = (shelf != null ? warehouseEntities.Zones.FirstOrDefault(p => p.Id == shelf.Id) : null),
                                    Order = (order != null ? warehouseEntities.Orders.FirstOrDefault(p => p.Id == order.Id) : null),
                                    DepartmentId = null,
                                    WarehouseId = warehouseId,
                                    QuantityBefore = quantityOnStack,
                                    Material = (material != null ? warehouseEntities.Materials.FirstOrDefault(p => p.Id == material.Id) : null),
                                    Quantity = quantity,
                                    UnitPrice = unitPrice,
                                    ProcessIdpl = processIdpl2,
                                    ProcessIddpl = processIddpl,
                                    ExportToSIMPLE = this.CheckBox_ExportToSIMPLE.Checked
                                };
                                    warehouseDs2.WarehouseEntitiesTest.AddToWarehouseInventoryPositions(newWarehouseInventoryPosition);
                                    warehouseDs2.WarehouseEntitiesTest.SaveChanges();
                                    warehouseInventoryPositions.Add(newWarehouseInventoryPosition);
                                }
                            }
                        }

                        Session["INVPositions"] = warehouseInventoryPositions;
                    }
                }

                #endregion

                var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
                GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
                detailTableView.Rebind();
                HideInventoryMaterialPanel();
            }
        }

        protected void ButtonInventoryMaterialExecuteClick(object sender, EventArgs e)
        {
            ButtonInventoryMaterialExecuteClick(sender, e, false);
        }

        protected void ChangeLocalizationButtonClick(object sender, EventArgs e)
        {

#if DEBUG
            Contract.Requires(sender != null);
#endif

            HideFreeMaterialPanel();
            HideInventoryMaterialPanel();

            var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer));

            var gridTableRow2 = gridTableRow.Cells[2].Text; // OrderId
            var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
            var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
            var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
            var gridTableRow11 = gridTableRow.Cells[11].Text; // DepartmentId
            var gridTableRow13 = gridTableRow.Cells[13].Text; // IsOrder

            int orderId = 0;
            int shelfId = 0;
            int processIdpl = 0;
            int departmentId = 0;
            bool isOrder = false;

            double quantity = 0.0;

            var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            string keyRendered = detailTableView.ParentItem.KeyValues;
            keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
            keyRendered = keyRendered.Replace(",MaterialId:", ";");
            keyRendered = keyRendered.Replace("\"", "");
            keyRendered = keyRendered.Replace("}", "");

            string[] ids = keyRendered.Split(';');
#if DEBUG
            Contract.Assert(ids.Length >= 2);
#endif
            int warehouseId = int.Parse(ids[0]);
            int materialId = int.Parse(ids[1]);

            int.TryParse(gridTableRow2, out orderId);
            int.TryParse(gridTableRow3, out shelfId);
            double.TryParse(gridTableRow6, out quantity);
            int.TryParse(gridTableRow10, out processIdpl);
            int.TryParse(gridTableRow11, out departmentId);
            bool.TryParse(gridTableRow13, out isOrder);

            HiddenField_QuickOp_WarehouseId.Value = warehouseId.ToString();
            HiddenField_QuickOp_MaterialId.Value = materialId.ToString();
            HiddenField_QuickOp_OrderId.Value = orderId.ToString();
            HiddenField_QuickOp_DepartmentId.Value = departmentId.ToString();
            HiddenField_QuickOp_ProcessIdpl.Value = processIdpl.ToString();
            HiddenField_QuickOp_ShelfSrcId.Value = shelfId.ToString();
            HiddenField_QuickOp_MaxQuantity.Value = quantity.ToString();
            HiddenField_QuickOp_IsOrder.Value = isOrder.ToString();

            HiddenField_QuickOp_DetailStacksContainerID.Value = detailTableView.UniqueID;

            RelocationQuantityTextBox.Text = quantity.ToString();

            if (isOrder)
            {
                RelocationQuantityTextBox.ReadOnly = true;
            }

            var imageMark = gridTableRow.FindControl("ImageMark");
            imageMark.Visible = true;

            MaterialRelocationPanel.Visible = true;
        }

        protected void FreeMaterialButtonClick(object sender, EventArgs e)
        {
#if DEBUG
            Contract.Requires(sender != null);
#endif

            HideRelocationPanel();
            HideInventoryMaterialPanel();

            var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer));

            var gridTableRow2 = gridTableRow.Cells[2].Text; // OrderId
            var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
            var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
            var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
            var gridTableRow11 = gridTableRow.Cells[11].Text; // DepartmentId
            var gridTableRow13 = gridTableRow.Cells[13].Text; // IsOrder
            var gridTableRow15 = gridTableRow.Cells[15].Text; // IsGave
            var gridTableRow16 = gridTableRow.Cells[16].Text; // IsOrderAndStack

            int orderId = 0;
            int shelfId = 0;
            int processIdpl = 0;
            int departmentId = 0;
            bool isOrder = false;
            bool isGave = false;
            bool isOrderAndStack = false;

            // TODO: Consider departments!!! Also in other op buttons!!!

            double quantity = 0.0;

            var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            string keyRendered = detailTableView.ParentItem.KeyValues;
            keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
            keyRendered = keyRendered.Replace(",MaterialId:", ";");
            keyRendered = keyRendered.Replace("\"", "");
            keyRendered = keyRendered.Replace("}", "");

            string[] ids = keyRendered.Split(';');
#if DEBUG
            Contract.Assert(ids.Length >= 2);
#endif
            int warehouseId = int.Parse(ids[0]);
            int materialId = int.Parse(ids[1]);

            int.TryParse(gridTableRow2, out orderId);
            int.TryParse(gridTableRow3, out shelfId);
            double.TryParse(gridTableRow6, out quantity);
            int.TryParse(gridTableRow10, out processIdpl);
            int.TryParse(gridTableRow11, out departmentId);
            bool.TryParse(gridTableRow13, out isOrder);
            bool.TryParse(gridTableRow15, out isGave);
            bool.TryParse(gridTableRow16, out isOrderAndStack);

            HiddenField_QuickOp_WarehouseId.Value = warehouseId.ToString();
            HiddenField_QuickOp_MaterialId.Value = materialId.ToString();
            HiddenField_QuickOp_OrderId.Value = orderId.ToString();
            HiddenField_QuickOp_DepartmentId.Value = departmentId.ToString();
            HiddenField_QuickOp_ProcessIdpl.Value = processIdpl.ToString();
            HiddenField_QuickOp_ShelfSrcId.Value = shelfId.ToString();
            HiddenField_QuickOp_MaxQuantity.Value = quantity.ToString();
            HiddenField_QuickOp_IsOrder.Value = isOrder.ToString();
            HiddenField_QuickOp_IsGave.Value = isGave.ToString();
            HiddenField_QuickOp_IsOrderAndStack.Value = isOrderAndStack.ToString();

            HiddenField_QuickOp_DetailStacksContainerID.Value = detailTableView.UniqueID;

            FreeMaterialQuantityTextBox.Text = quantity.ToString();
            
            FreeMaterialQuantityTextBox.ReadOnly = (isOrder && !isOrderAndStack);
            FreeMaterialQuantityTextBox.Enabled = !FreeMaterialQuantityTextBox.ReadOnly;

            var imageMark = gridTableRow.FindControl("ImageMark");
            imageMark.Visible = true;

            FreeMaterialPanel.Visible = true;
            senderSession = sender;
        }

        protected void FreeMaterialQuantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                var maxQuantity = double.Parse(HiddenField_QuickOp_MaxQuantity.Value);
                var enteredQuantity = double.Parse(FreeMaterialQuantityTextBox.Text);

                args.IsValid = enteredQuantity <= maxQuantity;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void Grid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    warehouseDs.WarehouseEntitiesTest.CommandTimeout = GetCommandTimeoutCounterFromPage(); // extend timeout to make WarehouseStacks usable in any way. Has to be fixed properly by optimizing view.
                    SetCommandTimeoutCounterOnPage(warehouseDs);

                    int? orderIdToFind = null;
                    if (!string.IsNullOrEmpty(cmbOrderNumber.SelectedValue) && !string.IsNullOrEmpty(cmbOrderNumber.Text))
                    {
                        orderIdToFind = int.Parse(cmbOrderNumber.SelectedValue);
                    }

                    int? shelfIdToFind = null;
                    if (!string.IsNullOrEmpty(cmbShelfNumber.SelectedValue) && !string.IsNullOrEmpty(cmbShelfNumber.Text))
                    {
                        shelfIdToFind = int.Parse(cmbShelfNumber.SelectedValue);
                    }

                    int mId = 0;

                    int.TryParse(materialsRCB.SelectedValue, out mId);

                    var materialId = mId > 0 ? mId : (int?)null;
                    var materialNameOrSimpleId = materialsRCB.Text;
                    var warehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                    var query = warehouseDs.CurrentStacksForWarehouseAdjusted_WithZTD_Encapsulated().Where(c => c.WarehouseId == warehouseId);

                    if (materialId != null)
                    {
                        query = query.Where(q => q.MaterialId == materialId);
                    }
                    else if (!String.IsNullOrEmpty(materialNameOrSimpleId))
                    {
                        materialNameOrSimpleId = materialNameOrSimpleId.Trim().ToLower();
                        query = query.Where(q => q.IndexSIMPLE.ToLower().Contains(materialNameOrSimpleId) || q.MaterialName.ToLower().Contains(materialNameOrSimpleId));
                    }

                    if (orderIdToFind != null)
                        query = query.Where(q => q.OrderId == orderIdToFind);

                    if (shelfIdToFind != null)
                        query = query.Where(q => q.ShelfId == shelfIdToFind);

                    var group = query.GroupBy(q => new { q.WarehouseId, q.MaterialId })
                      .Select(q => new
                      {
                          FoD = q.FirstOrDefault(),
                          Quantity = q.Sum(qq => qq.Quantity),
                      });

                    if (group.Count() != 0)
                    {
                        List<int> materialIds = group.Select(m => m.FoD.MaterialId).ToList();
                        Dictionary<int, double> materialsList = warehouseDs.GetOrderedQuantity(warehouseId, materialIds, orderIdToFind, shelfIdToFind);

                        var groupWithOrderedDocs = group.Select(g => new MaterialsCandidates
                        {
                            IndexSIMPLE = g.FoD.IndexSIMPLE,
                            MaterialName = g.FoD.MaterialName,
                            Quantity = (double)g.Quantity,
                            UnitShortName = g.FoD.UnitShortName + ".",
                            WarehouseId = g.FoD.WarehouseId,
                            MaterialId = g.FoD.MaterialId,
                            QuantityOrdered = 0
                        }).OrderBy(g => g.IndexSIMPLE).ToList();

                        groupWithOrderedDocs.ForEach(s =>
                        {
                            s.Quantity += materialsList.ContainsKey(s.MaterialId) ? materialsList[s.MaterialId] : 0;
                            s.QuantityOrdered = materialsList.ContainsKey(s.MaterialId) ? materialsList[s.MaterialId] : 0;
                        });

                        var materialsCandidatesToConsider = groupWithOrderedDocs;
                        var items = materialsCandidatesToConsider.ToArray();

                        // zdjęcie pozycji wydanych dla przypisanych pozycji
                        
                        //updateQuantity(items);

                        try
                        {
                            //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 1 " + w.ElapsedMilliseconds);
                            StackGrid.VirtualItemCount = items.Count();
                            //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 2 " + w.ElapsedMilliseconds);
                            int pageSize = StackGrid.PageSize;
                            int pageNum = StackGrid.CurrentPageIndex;

                            StackGrid.DataSource = items.Skip(pageSize * pageNum)
                              .Take(pageSize)
                              .ToArray();
                        }
                        catch (System.Data.SqlClient.SqlException sqlEx)
                        {
                            if (sqlEx.Message.Contains("deadlock"))
                            {
                                WebTools.AlertMsgAdd("Ze względu na duże obciążenie bazy danych Twoje zapytanie zostało anulowane. Odczekaj chwilę i spróbuj ponownie.", sqlEx, (int?)Session[SessionData.SessionKey_UserId]);
                            }
                            else
                            {
                                WebTools.AlertMsgAdd("Wystąpił błąd bazy danych. Spróbuj ponownie, a jeżeli błąd będzie się powtarzał wpisz zagadnienie w systemie zgłaszania błędów.", sqlEx, (int?)Session[SessionData.SessionKey_UserId]);
                            }
                        }
                        catch (Exception ex)
                        {
                            WebTools.AlertMsgAdd("Wystąpił błąd. Spróbuj ponownie, a jeżeli błąd będzie się powtarzał wpisz zagadnienie w systemie zgłaszania błędów.", ex, (int?)Session[SessionData.SessionKey_UserId]);
                        }
                    }
                    else if (materialId != null)
                    {
                        var orderedDocs = warehouseDs.GetOrderedStackList(warehouseId, (int)materialId, orderIdToFind, shelfIdToFind);
                        var orderedGroup = orderedDocs.GroupBy(q => q.MaterialId)
                            .Select(q => new
                            {
                                FoD = q.FirstOrDefault(),
                                Quantity = q.Sum(qq => qq.Quantity),
                            })
                            .Select(q => new MaterialsCandidates
                            {
                                IndexSIMPLE = warehouseDs.GetMaterialById(q.FoD.MaterialId).IndexSIMPLE,
                                MaterialName = warehouseDs.GetMaterialById(q.FoD.MaterialId).Name,
                                Quantity = Math.Abs(q.Quantity),
                                UnitShortName = warehouseDs.GetMaterialById(q.FoD.MaterialId).Unit.ShortName,
                                WarehouseId = warehouseId,
                                MaterialId = q.FoD.MaterialId,
                                QuantityOrdered = 0,
                            });
                        orderedGroup = orderedGroup.OrderBy(p => p.IndexSIMPLE);
                        var orderedMaterialsCandidatesToConsider = orderedGroup;
                        var items = orderedMaterialsCandidatesToConsider.ToArray();

                        // zdjęcie pozycji wydanych dla przypisanych pozycji
                        //updateQuantity(items);

                        try
                        {
                            //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 1 " + w.ElapsedMilliseconds);
                            StackGrid.VirtualItemCount = items.Count();
                            //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 2 " + w.ElapsedMilliseconds);
                            int pageSize = StackGrid.PageSize;
                            int pageNum = StackGrid.CurrentPageIndex;

                            StackGrid.DataSource = items.Skip(pageSize * pageNum)
                              .Take(pageSize)
                              .ToArray();
                        }
                        catch (System.Data.SqlClient.SqlException sqlEx)
                        {
                            if (sqlEx.Message.Contains("deadlock"))
                            {
                                WebTools.AlertMsgAdd("Ze względu na duże obciążenie bazy danych Twoje zapytanie zostało anulowane. Odczekaj chwilę i spróbuj ponownie.", sqlEx, (int?)Session[SessionData.SessionKey_UserId]);
                            }
                            else
                            {
                                WebTools.AlertMsgAdd("Wystąpił błąd bazy danych. Spróbuj ponownie, a jeżeli błąd będzie się powtarzał wpisz zagadnienie w systemie zgłaszania błędów.", sqlEx, (int?)Session[SessionData.SessionKey_UserId]);
                            }
                        }
                        catch (Exception ex)
                        {
                            WebTools.AlertMsgAdd("Wystąpił błąd. Spróbuj ponownie, a jeżeli błąd będzie się powtarzał wpisz zagadnienie w systemie zgłaszania błędów.", ex, (int?)Session[SessionData.SessionKey_UserId]);
                        }
                    }
                    else
                    {
                        var emptyGroup = group.Select(q => new MaterialsCandidates
                        {
                            IndexSIMPLE = q.FoD.IndexSIMPLE,
                            MaterialName = q.FoD.MaterialName,
                            Quantity = (double)q.Quantity,
                            UnitShortName = q.FoD.UnitShortName + ".",
                            WarehouseId = q.FoD.WarehouseId,
                            MaterialId = q.FoD.MaterialId,
                            QuantityOrdered = 0
                        });

                        emptyGroup = emptyGroup.OrderBy(g => g.IndexSIMPLE);
                        var items = emptyGroup.ToArray();

                        // zdjęcie pozycji wydanych dla przypisanych pozycji
                        //updateQuantity(items);

                        try
                        {
                            //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 1 " + w.ElapsedMilliseconds);
                            StackGrid.VirtualItemCount = items.Count();
                            //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 2 " + w.ElapsedMilliseconds);
                            int pageSize = StackGrid.PageSize;
                            int pageNum = StackGrid.CurrentPageIndex;

                            StackGrid.DataSource = items.Skip(pageSize * pageNum)
                              .Take(pageSize)
                              .ToArray();
                        }
                        catch (System.Data.SqlClient.SqlException sqlEx)
                        {
                            if (sqlEx.Message.Contains("deadlock"))
                            {
                                WebTools.AlertMsgAdd("Ze względu na duże obciążenie bazy danych Twoje zapytanie zostało anulowane. Odczekaj chwilę i spróbuj ponownie.", sqlEx, (int?)Session[SessionData.SessionKey_UserId]);
                            }
                            else
                            {
                                WebTools.AlertMsgAdd("Wystąpił błąd bazy danych. Spróbuj ponownie, a jeżeli błąd będzie się powtarzał wpisz zagadnienie w systemie zgłaszania błędów.", sqlEx, (int?)Session[SessionData.SessionKey_UserId]);
                            }
                        }
                        catch (Exception ex)
                        {
                            WebTools.AlertMsgAdd("Wystąpił błąd. Spróbuj ponownie, a jeżeli błąd będzie się powtarzał wpisz zagadnienie w systemie zgłaszania błędów.", ex, (int?)Session[SessionData.SessionKey_UserId]);
                        }
                    }
                }
            }
        }




        protected void InventoryMaterialButtonClick(object sender, EventArgs e)
        {
#if DEBUG
            Contract.Requires(sender != null);
#endif
            // TODO: Implement

            HideRelocationPanel();
            HideFreeMaterialPanel();

            var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer));

            var gridTableRow2 = gridTableRow.Cells[2].Text; // OrderId
            var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
            var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
            var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
            var gridTableRow11 = gridTableRow.Cells[11].Text; //ProcessIddpl
            var gridTableRow12 = gridTableRow.Cells[12].Text; // DepartmentId
            var gridTableRow13 = gridTableRow.Cells[13].Text; //IsOrder
            var gridTableRow14 = gridTableRow.Cells[14].Text; //wdpId

            int orderId = 0;
            int shelfId = 0;
            int processIdpl;
            int processIddpl;
            int departmentId = 0;
            bool isOrder = false;
            int wdpId = 0;

            // TODO: Consider departments!!! Also in other op buttons!!!

            double quantity = 0.0;

            var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

            string keyRendered = detailTableView.ParentItem.KeyValues;
            keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
            keyRendered = keyRendered.Replace(",MaterialId:", ";");
            keyRendered = keyRendered.Replace("\"", "");
            keyRendered = keyRendered.Replace("}", "");

            string[] ids = keyRendered.Split(';');
#if DEBUG
            Contract.Assert(ids.Length >= 2);
#endif
            int warehouseId = int.Parse(ids[0]);
            int materialId = int.Parse(ids[1]);

            if (warehouseInventoryPositions != null && warehouseInventoryPositions.Any(p => p.WarehouseId != warehouseId))
            {
                //throw new Exception("");

                WebTools.AlertMsgAdd("Nie można inwentaryzować kilku magazynów jednocześnie!");
                //HideInventoryMaterialPanel();
                return;
            }

            int.TryParse(gridTableRow2, out orderId);
            int.TryParse(gridTableRow3, out shelfId);
            double.TryParse(gridTableRow6, out quantity);
            //int.TryParse(gridTableRow10, out processIdpl);
            //int.TryParse(gridTableRow11, out processIddpl);
            int.TryParse(gridTableRow12, out departmentId);
            bool.TryParse(gridTableRow13, out isOrder);
            int.TryParse(gridTableRow14, out wdpId);

            HiddenField_QuickOp_WarehouseId.Value = warehouseId.ToString();
            HiddenField_QuickOp_MaterialId.Value = materialId.ToString();
            HiddenField_QuickOp_OrderId.Value = orderId.ToString();
            HiddenField_QuickOp_DepartmentId.Value = departmentId.ToString();

            //HiddenField_QuickOp_ProcessIdpl.Value = processIdpl.ToString();
            //HiddenField_QuickOp_ProcessIddpl.Value = processIddpl.ToString();

            HiddenField_QuickOp_ProcessIdpl.Value = int.TryParse(gridTableRow10, out processIdpl) ? processIdpl.ToString() : "";
            HiddenField_QuickOp_ProcessIddpl.Value = int.TryParse(gridTableRow11, out processIddpl) ? processIddpl.ToString() : "";

            HiddenField_QuickOp_ShelfSrcId.Value = shelfId.ToString();
            HiddenField_QuickOp_MaxQuantity.Value = quantity.ToString();
            HiddenField_QuickOp_IsOrder.Value = isOrder.ToString();
            HiddenField_QuickOp_WarehouseDocumentPositionId.Value = wdpId.ToString();

            HiddenField_QuickOp_DetailStacksContainerID.Value = detailTableView.UniqueID;


            if (shelfId > 0)
            {
                InventoryQuantityTextBox.Text = "0";
                if (isOrder)
                    InventoryQuantityTextBox.ReadOnly = true;

                var imageMark = gridTableRow.FindControl("ImageMark");
                imageMark.Visible = true;

                InventoryPanel.Visible = true;
            }
            else
            {
                HideInventoryMaterialPanel();
                WebTools.AlertMsgAdd("Brak lokalizacji dla wybranej pozycji.");
            }
        }

        // this does not seem to fire
        protected void InventoryMaterialQuantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var inventoryQuantity = -1.0;
            args.IsValid = double.TryParse(InventoryQuantityTextBox.Text, out inventoryQuantity);
            args.IsValid = args.IsValid && inventoryQuantity >= 0;
        }

        protected void OpenInventoryListClick(object sender, EventArgs e)
        {
            //Respo
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //perfDebug.Restart();
            var companyId = (int?)Session[SessionData.SessionKey_CurrentCompanyId];
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];

            if (Request["o"] == "i")
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    InventoryMode = warehouseDs.CheckOperationAvailability(currentWarehouseId, currentWarehouseId, WarehouseDocumentTypeEnum.INW, userId) == "";
                }
            }

            if (Session["INVPositions"] == null)
                warehouseInventoryPositions = new List<WarehouseInventoryPosition>();
            else
                warehouseInventoryPositions = (List<WarehouseInventoryPosition>)Session["INVPositions"];

            if (!Page.IsPostBack)
            {
                Session["tab_Stacks"] = InventoryMode ? 1 : 0;
                LabelTitle.Text = InventoryMode ? "Inwentaryzacja" : "Stany magazynowe";
                PanelInventory.Visible = InventoryMode;

                materialsRCB.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();

                if (InventoryMode)
                {
                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        warehouseInventoryPositions = warehouseDs.GetActiveWarehouseInventoryPositions(currentWarehouseId);
                        Session["INVPositions"] = warehouseInventoryPositions;
                    }
                }
            }

            MaterialRelocationPanel.Visible = false;
            FreeMaterialPanel.Visible = false;
            InventoryPanel.Visible = false;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.StackGrid.Enabled = !MaterialRelocationPanel.Visible && !FreeMaterialPanel.Visible && !InventoryPanel.Visible;
        }

        protected void RelocationQuantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                var maxQuantity = double.Parse(HiddenField_QuickOp_MaxQuantity.Value);
                var enteredQuantity = double.Parse(RelocationQuantityTextBox.Text);

                args.IsValid = enteredQuantity <= maxQuantity;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void RelocationTargetZone_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Zone zone = null;

            if (!string.IsNullOrEmpty(RelocationTargetZoneRCB.Text))
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    zone = warehouseDs.GetShelfByName(RelocationTargetZoneRCB.Text);//.GetShelfByNameDataOnly(RelocationTargetZoneRCB.Text);
                }
            }

            if (zone == null) args.IsValid = false;
            else args.IsValid = true;
        }

        protected void SearchClick(object sender, EventArgs e)
        {
            StackGrid.Visible = true;
            StackGrid.Rebind();
            hfUnlock.Value = "";
        }

        protected void ShowStockReportClick(object sender, EventArgs e)
        {
            GridDataItem item = (GridDataItem)StackGrid.SelectedItems[0];
            if (item != null)
            {
                int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
                string materialIndex = item["IndexSIMPLE"].Text;

                Material material;

                using (var warehouseDs = new WarehouseDS())
                {
                    material = warehouseDs.GetMaterialByIndex(materialIndex.Trim(), companyId);
                }
                int? warehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];

                Response.Redirect("Reports/StockReport.aspx?t=1&warehouseId=" + warehouseId + "&materialId=" + material.Id);
            }
        }

        protected void StackGrid_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;
            if (parentItem.Edit)
            {
                return;
            }

            int warehouseId = (int)parentItem.GetDataKeyValue("WarehouseId");
            int materialId = (int)parentItem.GetDataKeyValue("MaterialId");

            int? orderIdToFind = null;
            if (!string.IsNullOrWhiteSpace(cmbOrderNumber.SelectedValue))
            {
                orderIdToFind = int.Parse(cmbOrderNumber.SelectedValue);
            }

            int? shelfIdToFind = null;
            if (!string.IsNullOrWhiteSpace(cmbShelfNumber.SelectedValue))
            {
                shelfIdToFind = int.Parse(cmbShelfNumber.SelectedValue);
            }
            //double quantityOrdered;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
                int? userId = (int?)Session[SessionData.SessionKey_UserId];
                bool currentUserCanFreeMaterialsInCurrentWarehouse;
                currentUserCanFreeMaterialsInCurrentWarehouse = warehouseDs.CheckOperationAvailability(currentWarehouseId, currentWarehouseId, WarehouseDocumentTypeEnum.UMZ, userId) == "";


                ////* test
                //quantityOrdered = warehouseDs.GetOrderedQuantity(warehouseId, materialId, orderIdToFind, shelfIdToFind);

                //********************************************************************************************************


                var invMix = warehouseInventoryPositions.Select(wip => new
                {
                    wip.WarehouseId,
                    wip.MaterialId,
                    wip.OrderId,
                    wip.ProcessIdpl,
                    wip.ProcessIddpl,
                    wip.ZoneId,
                    wip.WarehouseDocumentPositionId
                }).Distinct();
                
                
                var materialsCandidatesToConsider = warehouseDs.GetCurrentStackDetails(warehouseId, materialId, orderIdToFind, shelfIdToFind);

                var detailsMaterialized = materialsCandidatesToConsider.Select(p => new MaterialsMaterializedStack()
                              {
                                  WarehouseId = (int)p.WarehouseId,
                                  MaterialId = p.MaterialId,
                                  WarehouseDocumentPositionId = null,
                                  Quantity = p.Quantity,
                                  UnitShortName = p.UnitShortName + ".",
                                  ShelfId = p.ShelfId,
                                  ShelfName = p.ShelfName,
                                  OrderId = p.OrderId,
                                  DepartmentId = p.DepartmentId,
                                  OrderNumber = (!string.IsNullOrEmpty(p.OrderNumber) ? p.OrderNumber : "stan magazynowy"),
                                  ProcessIdpl = p.ProcessIdpl,
                                  ProcessIddpl = p.ProcessIddpl,
                                  ZoneChangeEnabled = !InventoryMode && !p.TimeChildrenEnabled.HasValue && !(!p.OrderId.HasValue && !string.IsNullOrEmpty(p.OrderNumber) && p.OrderNumber != "zwrot do dostawcy"),
                                  FreeMaterialEnabled = !InventoryMode && !p.TimeChildrenEnabled.HasValue && p.OrderId.HasValue && currentUserCanFreeMaterialsInCurrentWarehouse,
                                  //FreeMaterialEnabled = false,
                                  InventoryMaterialEnabled = InventoryMode && !p.TimeChildrenEnabled.HasValue,
                                  IsOrder = false,
                                  isOrderAndStack = p.OrderId.HasValue ? true : false,
                                  EndDate = (string.IsNullOrEmpty(p.OrderNumber) ? null : warehouseDs.WarehouseEntitiesTest.KP_KartaP.Where(k => k.NRp.Contains(p.OrderNumber)).Select(k => k.DataS).FirstOrDefault()),
                              }).ToList();


                // blokada kontroli jakoci - brak z powodu widoków



                //#region blokada częściowo wydanych

                //// testy... JESLI WYDANO CZĘŚCIOWO TOWAR Z ZAMÓWIENIA RESZTA POWINNA BYĆ ZABLOKOWANA DO INWENTARYZACJI I ZMIANY LOKALIZACJI
                ////var detailsMaterialized = details.ToList();

                
                //// BLOKUJE TEŻ NIE WYDANYCH MATERIAŁÓW!! 
                //// TRZEBA TO ZROBIĆ INACZEJ !!!

                //foreach (var pos in detailsMaterialized)
                //{
                //    var UngivenOrders = warehouseDs.GetUngivenOrderedPositions(warehouseId, pos.OrderId)
                //        .Where(x=>
                //            Math.Abs(x.Quantity) != Math.Abs(x.GivenQuantity)
                //            //&& (Math.Abs(x.Quantity) - Math.Abs(x.GivenQuantity)) == Math.Abs((double)pos.Quantity) 
                //            && (
                //                ((x.IDpl.HasValue && pos.ProcessIdpl.HasValue) && (pos.ProcessIdpl == x.IDpl))
                //                || (!x.IDpl.HasValue && !pos.ProcessIdpl.HasValue)
                //               )
                //            //&& (
                //            //    ((x.IDDpl.HasValue && pos.ProcessIddpl.HasValue) && (pos.ProcessIddpl == x.IDDpl))
                //            //    || (!x.IDDpl.HasValue && !pos.ProcessIddpl.HasValue)
                //            //   )
                //            && x.ShelfId == pos.ShelfId
                //            && x.MaterialId == pos.MaterialId
                //            ).ToList();

                //    if (UngivenOrders.Count > 0)
                //    {
                //        foreach (var order in UngivenOrders)
                //        {
                //            // dziwny fix
                //            if (pos.Quantity > order.GivenQuantity)
                //            {
                //                pos.IsOrder = true;
                //                pos.InventoryMaterialEnabled = false;
                //                pos.FreeMaterialEnabled = false;
                //                pos.ZoneChangeEnabled = false;
                //                pos.Quantity = pos.Quantity - order.GivenQuantity;
                //                pos.isGave = true;
                //            }
                //        }
                //    }
                //}
                //////////////////
                //#endregion


                // DODAJE UWOLNIONY MATERIAŁ KTÓRY JUŻ NIE JEST ORDERED..!!!!!
                // SPRAWDZIĆ 13.06.2014
                var quantityOrdered = warehouseDs.GetOrderedStackList(warehouseId, materialId, orderIdToFind, shelfIdToFind);

                foreach (var item in quantityOrdered)
                {
                    item.Material = warehouseDs.GetMaterialById(item.MaterialId);
                    var shelf = warehouseDs.GetShelfForPositionId(item.Id);

                    detailsMaterialized.Add(new MaterialsMaterializedStack()
                    {
                        WarehouseId = warehouseId,
                        MaterialId = item.MaterialId,
                        WarehouseDocumentPositionId = item.Id,
                        Quantity = Math.Abs(item.Quantity),
                        UnitShortName = item.Material.Unit.ShortName,
                        ShelfId = (shelf != null) ? shelf.ZoneId : 0,
                        ShelfName = (shelf != null) ? shelf.Zone.Name : "",
                        OrderId = item.OrderId,
                        DepartmentId = item.DepartmentId,
                        OrderNumber = (item.OrderId != null) ? warehouseDs.WarehouseEntitiesTest.Orders.Where(x => x.Id == item.OrderId).Select(x => x.OrderNumber).FirstOrDefault() : "stan magazynowy",
                        ProcessIdpl = item.ProcessIdpl,
                        ProcessIddpl = item.ProcessIddpl,
                        //ZoneChangeEnabled = true && !InventoryMode,
                        ZoneChangeEnabled = false, // materiał uwolniony trzeba inwentaryzować przed zmianą lokalizacji!!!
                        FreeMaterialEnabled = item.OrderId.HasValue && currentUserCanFreeMaterialsInCurrentWarehouse && InventoryMode,
                        //FreeMaterialEnabled = false,
                        //InventoryMaterialEnabled = InventoryMode,
                        InventoryMaterialEnabled = InventoryMode && (item.OrderId == null),
                        IsOrder = (item.OrderId != null),
                        isOrderAndStack = item.OrderId.HasValue ? true : false,
                        EndDate = (item.OrderId != null ? warehouseDs.WarehouseEntitiesTest.KP_KartaP.Where(k => k.NRp.Contains(warehouseDs.WarehouseEntitiesTest.Orders.Where(x => x.Id == item.OrderId).Select(x => x.OrderNumber).FirstOrDefault())).Select(k => k.DataS).FirstOrDefault() : null)
                    });
                }


                var detailsFinal = detailsMaterialized
                    .Select(p => new MaterialsMaterializedStack()
                  {
                      WarehouseId = p.WarehouseId,
                      MaterialId = p.MaterialId,
                      Quantity = p.Quantity,
                      UnitShortName = p.UnitShortName,
                      ShelfId = p.ShelfId,
                      ShelfName = p.ShelfName,
                      OrderId = p.OrderId,
                      DepartmentId = p.DepartmentId,
                      OrderNumber = p.OrderNumber,
                      ProcessIdpl = p.ProcessIdpl,
                      ProcessIddpl = p.ProcessIddpl,
                      ZoneChangeEnabled = !p.IsOrder && p.ShelfId > 0 && p.WarehouseId != 66666 && InventoryMode,
                      //FreeMaterialEnabled = !p.IsOrder && p.ShelfId > 0 && InventoryMode && p.OrderId.HasValue,
                      FreeMaterialEnabled = (p.IsOrder && p.ShelfId > 0 && InventoryMode && p.OrderId.HasValue && p.FreeMaterialEnabled) || (p.isOrderAndStack && InventoryMode),
                      //FreeMaterialEnabled = false,
                      InventoryMaterialEnabled = InventoryMode && !p.IsOrder && p.InventoryMaterialEnabled && p.ShelfId > 0 && !invMix.Any(q =>
                             q.WarehouseId == warehouseId
                             && q.MaterialId == materialId
                             && ((q.OrderId == null && p.OrderId == null) || q.OrderId == p.OrderId)
                             && ((q.ProcessIdpl == null && p.ProcessIdpl == null) || q.ProcessIdpl == p.ProcessIdpl)
                             && ((q.ProcessIddpl == null && p.ProcessIddpl == null) || q.ProcessIddpl == p.ProcessIddpl)
                             && q.ZoneId == p.ShelfId
                             ),
                      PositionIsInventarized =
                         InventoryMode
                         && invMix.Any(q =>
                             q.WarehouseId == warehouseId
                             && q.MaterialId == materialId
                             && ((q.OrderId == null && p.OrderId == null) || q.OrderId == p.OrderId)
                             && ((q.ProcessIdpl == null && p.ProcessIdpl == null) || q.ProcessIdpl == p.ProcessIdpl)
                             && q.ZoneId == p.ShelfId
                             && ((q.ProcessIddpl == null && p.ProcessIddpl == null) || q.ProcessIddpl == p.ProcessIddpl)
                             && (p.IsOrder == false || p.WarehouseDocumentPositionId == q.WarehouseDocumentPositionId)
                             ),
                      IsOrder = p.IsOrder,
                      wdpId = p.WarehouseDocumentPositionId,
                      //EndDate = p.EndDate.HasValue ? p.EndDate.Value.ToString("yyyy-MM-dd") : null
                      EndDate = p.EndDate,
                      isGave = p.isGave,
                      SetZero = false,
                      FullInfo = string.Format("{0},{1},{2},{3},{4},{5},{6},{7};",
                        p.MaterialId,
                        p.WarehouseId,
                        p.OrderId,
                        p.ShelfId,
                        p.ProcessIddpl,
                        p.ProcessIdpl,
                        p.DepartmentId,
                        p.Quantity.HasValue ? p.Quantity.Value : 0)
                  });


                e.DetailTableView.VirtualItemCount = detailsFinal.Count();
                int pageSize = e.DetailTableView.PageSize;
                int pageNum = e.DetailTableView.CurrentPageIndex;

                //e.DetailTableView.DataSource = detailsFinal.ToArray(); // materialsCandidatesToConsider.ToArray();
                e.DetailTableView.DataSource = detailsFinal.Skip(pageSize*pageNum).Take(pageSize).ToArray(); 

            }
        }

        protected void StackGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem) // && e.Item.OwnerTableView.DataSourceID == "AccessDataSource1")
            {
                Label lbl = e.Item.FindControl("numberLabel") as Label;
                if (lbl != null)
                {
                    int pageSize = StackGrid.PageSize;
                    int pageNum = StackGrid.CurrentPageIndex;

                    lbl.Text = (e.Item.ItemIndex + 1 + pageSize * pageNum).ToString();
                }
            }

            if (e.Item is GridDataItem)
            {
                var gridDataItem = e.Item as GridDataItem;

                try
                {
                    var gridDataItem1 = gridDataItem["StockReportLink1"];
                    if (gridDataItem1 != null && gridDataItem1.Controls != null && gridDataItem1.Controls.Count > 0)
                    {
                        var hyperLink1 = gridDataItem1.Controls[0] as HyperLink;
                        if (hyperLink1 != null)
                        {
                            var url = hyperLink1.NavigateUrl;
                            int i = url.IndexOf("&m=") + 3;
                            hyperLink1.NavigateUrl = String.Format("{0}{1}", url.Substring(0, i),
                                                                   HttpUtility.UrlEncode(url.Substring(i)));
                            //int j = url.IndexOf("','Raport1'");
                            //var m = url.Substring(i, (j - i));
                            //hyperLink.NavigateUrl = url.Replace(m, HttpUtility.UrlEncode(m));
                        }
                    }

                    var gridDataItem2 = gridDataItem["StockReportLink2"];
                    if (gridDataItem2 != null && gridDataItem2.Controls != null && gridDataItem2.Controls.Count > 0)
                    {
                        var hyperLink2 = gridDataItem2.Controls[0] as HyperLink;
                        if (hyperLink2 != null)
                        {
                            var url = hyperLink2.NavigateUrl;
                            int i = url.IndexOf("&m=") + 3;
                            hyperLink2.NavigateUrl = String.Format("{0}{1}", url.Substring(0, i),
                                                                   HttpUtility.UrlEncode(url.Substring(i)));
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private int GetCommandTimeoutCounterFromPage()
        {
            if (Page.Master != null && Page.Master.Master != null)
            {
                var hiddenField = Page.Master.Master.FindControl("QueryTimeout") as HiddenField;
                if (hiddenField != null)
                {
                    int timeout = 0;
                    if (int.TryParse(hiddenField.Value, out timeout))
                    { return timeout; }
                }
            }
            return 0;
        }

        private void HideFreeMaterialPanel()
        {
            HiddenField_QuickOp_WarehouseId.Value = "";
            HiddenField_QuickOp_MaterialId.Value = "";
            HiddenField_QuickOp_OrderId.Value = "";
            HiddenField_QuickOp_DepartmentId.Value = "";
            HiddenField_QuickOp_ProcessIdpl.Value = "";
            HiddenField_QuickOp_ProcessIddpl.Value = "";
            HiddenField_QuickOp_ShelfSrcId.Value = "";
            HiddenField_QuickOp_MaxQuantity.Value = "";
            HiddenField_QuickOp_DetailStacksContainerID.Value = "";
            HiddenField_QuickOp_IsOrder.Value = "";

            FreeMaterialQuantityTextBox.Text = "";
            FreeMaterialQuantityTextBox.ReadOnly = false;

            FreeMaterialPanel.Visible = false;
        }

        private void HideInventoryMaterialPanel()
        {
            HideInventoryMaterialPanel(false);
        }

        private void HideInventoryMaterialPanel(bool saveDetails)
        {
            HiddenField_QuickOp_WarehouseId.Value = "";
            HiddenField_QuickOp_MaterialId.Value = "";
            HiddenField_QuickOp_OrderId.Value = "";
            HiddenField_QuickOp_DepartmentId.Value = "";
            HiddenField_QuickOp_ProcessIdpl.Value = "";
            HiddenField_QuickOp_ProcessIddpl.Value = "";
            HiddenField_QuickOp_ShelfSrcId.Value = "";
            HiddenField_QuickOp_MaxQuantity.Value = "";
            if (!saveDetails)
                HiddenField_QuickOp_DetailStacksContainerID.Value = "";
            HiddenField_QuickOp_IsOrder.Value = "";

            InventoryQuantityTextBox.Text = "";
            InventoryQuantityTextBox.ReadOnly = false;

            InventoryPanel.Visible = false;
        }

        private void HideRelocationPanel()
        {
            RelocationTargetZoneRCB.Text = "";
            RelocationQuantityTextBox.Text = "";
            RelocationQuantityTextBox.ReadOnly = false;

            HiddenField_QuickOp_WarehouseId.Value = "";
            HiddenField_QuickOp_MaterialId.Value = "";
            HiddenField_QuickOp_OrderId.Value = "";
            HiddenField_QuickOp_DepartmentId.Value = "";
            HiddenField_QuickOp_ProcessIdpl.Value = "";
            HiddenField_QuickOp_ProcessIddpl.Value = "";
            HiddenField_QuickOp_ShelfSrcId.Value = "";
            HiddenField_QuickOp_MaxQuantity.Value = "";
            HiddenField_QuickOp_DetailStacksContainerID.Value = "";
            HiddenField_QuickOp_IsOrder.Value = "";

            RelocationQuantityTextBox.Text = "";

            MaterialRelocationPanel.Visible = false;
        }

        private void SetCommandTimeoutCounterOnPage(WarehouseDS warehouseDs)
        {
            if (Page.Master != null && Page.Master.Master != null)
            {
                var hiddenField = Page.Master.Master.FindControl("QueryTimeout") as HiddenField;
                if (hiddenField != null && warehouseDs.WarehouseEntitiesTest.CommandTimeout.HasValue)
                {
                    hiddenField.Value = warehouseDs.WarehouseEntitiesTest.CommandTimeout.ToString();
                }
            }
        }

        public class MaterialsCandidates
        {
            public string IndexSIMPLE { get; set; }
            public string MaterialName { get; set; }
            public double? Quantity { get; set; }
            public string UnitShortName { get; set; }
            public int? WarehouseId { get; set; }
            public int MaterialId { get; set; }
            public double QuantityOrdered { get; set; }
            public int OrderId { get; set; }
        }

        private class MaterialsMaterializedStack
        {
            public int WarehouseId { get; set; }
            public int MaterialId { get; set; }
            public int? WarehouseDocumentPositionId { get; set; }
            public string OrderNumber { get; set; }
            public double? Quantity { get; set; }
            public string UnitShortName { get; set; }
            public int ShelfId { get; set; }
            public string ShelfName { get; set; }
            public int? OrderId { get; set; }
            public int? DepartmentId { get; set; }
            public int? ProcessIdpl { get; set; }
            public int? ProcessIddpl { get; set; }
            public bool ZoneChangeEnabled { get; set; }
            public bool FreeMaterialEnabled { get; set; }
            public bool InventoryMaterialEnabled { get; set; }
            public bool IsOrder { get; set; }
            public DateTime? EndDate { get; set; }
            public bool PositionIsInventarized { get; set; }
            public int? wdpId { get; set; }
            public bool isGave { get; set; }
            public bool isOrderAndStack { get; set; }
            public bool UnlockEnabled { get; set; }
            public bool SetZero { get; set; }
            public string FullInfo { get; set; }
        }

        private void updateQuantity(MaterialsCandidates[] items)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                foreach (var pos in items)
                {
                    var UngivenOrders = warehouseDs.GetUngivenOrderedPositions((int)pos.WarehouseId, null, pos.MaterialId)
                        .Where(x =>
                            Math.Abs(x.Quantity) != Math.Abs(x.GivenQuantity)
                            ).ToList();

                    if (UngivenOrders.Count > 0)
                    {
                        foreach (var order in UngivenOrders)
                        {
                            if(Math.Abs((double)pos.Quantity)>Math.Abs(order.GivenQuantity))
                                pos.Quantity = pos.Quantity - order.GivenQuantity;
                        }
                    }
                }
            }
        }

        protected void btSetZero_Click(object sender, EventArgs e)
        {
            var info = hfUnlock.Value.Split(';');
            foreach (var item in info.Where(x => x.Length > 0))
            {
                var hlp = item.Split(',');
                int test;
                InventoryPosition poz = new InventoryPosition()
                {
                    MaterialId = int.Parse(hlp[0]),
                    WarehouseId = int.Parse(hlp[1]),
                    //OrderId = int.Parse(hlp[2]),
                    ShelfId = int.Parse(hlp[3]),
                    //ProcessIddpl = int.Parse(hlp[4]),
                    //ProcessIdpl = int.Parse(hlp[5]),
                    //DepartmentId = int.Parse(hlp[6]),
                    Quantity = double.Parse(hlp[7])
                };

                if (int.TryParse(hlp[2], out test))
                {
                    poz.OrderId = int.Parse(hlp[2]);
                }
                else
                    poz.OrderId = null;


                if (int.TryParse(hlp[4], out test))
                {
                    poz.ProcessIddpl = int.Parse(hlp[4]);
                }
                else
                    poz.ProcessIddpl = null;


                if (int.TryParse(hlp[5], out test))
                {
                    poz.ProcessIdpl = int.Parse(hlp[5]);
                }
                else
                    poz.ProcessIdpl = null;


                if (int.TryParse(hlp[6], out test))
                {
                    poz.DepartmentId = int.Parse(hlp[6]);
                }
                else
                    poz.DepartmentId = null;


                decimal valuePerUnit = 0;

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {

                    IQueryable<CurrentStackInDocument> stackPosQ = warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments.AsQueryable();
                    stackPosQ = stackPosQ.Where(x => x.MaterialId == poz.MaterialId).AsQueryable();
                    stackPosQ = stackPosQ.Where(x => x.WarehouseTargetId == poz.WarehouseId).AsQueryable();
                    stackPosQ = stackPosQ.Where(x => x.ShelfId == poz.ShelfId).AsQueryable();

                    // materiał mógł mieć przypisanie ale zostął uwolniony, szukamy dodatkowo w zamówieniach..
                    IQueryable<WarehouseDocumentPosition> stackQuantity = warehouseDs.GetOrderedStackList(poz.WarehouseId, poz.MaterialId, poz.OrderId, poz.ShelfId).AsQueryable();

                    if (poz.OrderId > 0)
                    {
                        stackPosQ = stackPosQ.Where(p => p.OrderId == poz.OrderId);
                        stackQuantity = stackQuantity.Where(p => p.OrderId == poz.OrderId);
                    }
                    else
                    {
                        stackPosQ = stackPosQ.Where(p => !p.OrderId.HasValue);
                        stackQuantity = stackQuantity.Where(p => !p.OrderId.HasValue);
                    }

                    if (poz.ProcessIdpl == 0)
                    {
                        stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == poz.ProcessIdpl || !p.ProcessIdpl.HasValue);
                        stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == poz.ProcessIdpl || !p.ProcessIdpl.HasValue);
                    }
                    else
                    {
                        if (poz.ProcessIdpl > 0)
                        {
                            stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == poz.ProcessIdpl);
                            stackQuantity = stackQuantity.Where(p => p.ProcessIdpl == poz.ProcessIdpl);
                        }
                        else
                        {
                            stackPosQ = stackPosQ.Where(p => !p.ProcessIdpl.HasValue);
                            stackQuantity = stackQuantity.Where(p => !p.ProcessIdpl.HasValue);
                        }
                    }

                    if (poz.ProcessIddpl == 0 || !poz.ProcessIddpl.HasValue)
                    {
                        stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == poz.ProcessIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                        stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == poz.ProcessIddpl || !p.ProcessIddpl.HasValue || p.ProcessIddpl == 0);
                    }
                    else
                    {
                        if (poz.ProcessIddpl > 0)
                        {
                            stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == poz.ProcessIddpl);
                            stackQuantity = stackQuantity.Where(p => p.ProcessIddpl == poz.ProcessIddpl);
                        }
                        else
                        {
                            stackPosQ = stackPosQ.Where(p => !p.ProcessIddpl.HasValue);
                            stackQuantity = stackQuantity.Where(p => !p.ProcessIddpl.HasValue);
                        }
                    }


                    if (!stackPosQ.Any())
                    {
                        if (stackQuantity.Any())
                        {
                            var stackPos = stackQuantity.First();
                            valuePerUnit = stackPos.UnitPrice;
                        }

                    }
                    else
                    {
                        var stackP = stackPosQ.First();
                        valuePerUnit = stackP.UnitPrice;
                    }
                }                

                WarehouseInventoryPosition inventoryPositionWhichExists =
                    warehouseInventoryPositions.FirstOrDefault(
                        p => p.WarehouseId == poz.WarehouseId
                        && p.MaterialId == poz.MaterialId
                        && p.UnitPrice == valuePerUnit
                        && (p.Order == null && poz.OrderId == 0 || (p.Order != null && p.OrderId == poz.OrderId))
                        && p.ZoneId == poz.ShelfId
                        && p.ProcessIdpl == poz.ProcessIdpl
                        && p.ProcessIddpl == poz.ProcessIddpl
                        );

                if (inventoryPositionWhichExists != null) //positionsWithShelfsToAdd.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id && p.position.UnitPrice == warehouseDocumentPosition.UnitPrice && p.shelf.Id == shelfId) == 1)
                {
                    throw new Exception("Ta pozycja została już dodana. Nie można jej dodać dwukrotnie.");
                }
                else
                {
                    using (var warehouseDs2 = new WarehouseDS())
                    {
                        var warehouseEntities = warehouseDs2.WarehouseEntitiesTest;
                        var newWarehouseInventoryPosition = new WarehouseInventoryPosition()
                        {
                            IsActive = true,
                            AddedTime = DateTime.Now,
                            ObservationTime = DateTime.Now,
                            Zone = (warehouseEntities.Zones.FirstOrDefault(p => p.Id == poz.ShelfId)),
                            Order = (warehouseEntities.Orders.FirstOrDefault(p => p.Id == poz.OrderId)),
                            DepartmentId = null,
                            WarehouseId = poz.WarehouseId,
                            QuantityBefore = poz.Quantity,
                            Material = (warehouseEntities.Materials.FirstOrDefault(p => p.Id == poz.MaterialId)),
                            Quantity = 0,
                            UnitPrice = valuePerUnit,
                            ProcessIdpl = poz.ProcessIdpl,
                            ProcessIddpl = poz.ProcessIddpl,
                            ExportToSIMPLE = this.cbExportToSimple.Checked                        };
                        warehouseDs2.WarehouseEntitiesTest.AddToWarehouseInventoryPositions(newWarehouseInventoryPosition);
                        warehouseDs2.WarehouseEntitiesTest.SaveChanges();
                        warehouseInventoryPositions.Add(newWarehouseInventoryPosition);
                    }
                }

                Session["INVPositions"] = warehouseInventoryPositions;

                StackGrid.Rebind();
                //StackGrid.MasterTableView.DetailTables[0].Rebind();
                hfUnlock.Value = "";

            }
        }

        private class InventoryPosition
        {
            public int MaterialId { get; set; }
            public int WarehouseId { get; set; }
            public int? OrderId { get; set; }
            public int ShelfId { get; set; }
            public int? ProcessIddpl { get; set; }
            public int? ProcessIdpl { get; set; }
            public int? DepartmentId { get; set; }
            public double Quantity { get; set; }
        }
    }
}