﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentZrPlus.aspx.cs"
    Inherits="MagWeb.WarehouseDocuments.DocumentZrPlus" %>

<%@ Register TagPrefix="uc1" TagName="DocPrintLayoutFooter" Src="~/WarehouseDocuments/DocPrintLayoutFooter.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <table width="650">
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label22" runat="server" Text="Data wydruku"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="DocumentDate" runat="server" Text="DocumentDate"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="650" border="1">
        <tr>
            <td rowspan="6">
                <asp:Label ID="Company" runat="server" Text="Company"></asp:Label>
            </td>
            <td>
                <asp:Label ID="DocumentType" runat="server" Text="ZR+"></asp:Label>
            </td>
            <td>
                <asp:Label ID="DocumentDescription" runat="server" Text="Zwrot - przychód"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Nr dokumentu"></asp:Label>
            </td>
            <td>
                <asp:Label ID="DocumentId" runat="server" Text="DocumentId"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Data przyjęcia"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Date" runat="server" Text="Date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Z magazynu"></asp:Label>
            </td>
            <td>
                <asp:Label ID="WarehouseSource" runat="server" Text="WarehouseSource"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Do magazynu"></asp:Label>
            </td>
            <td>
                <asp:Label ID="WarehouseTarget" runat="server" Text="WarehouseTarget"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Operator"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Author" runat="server" Text="Author"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label11" runat="server" Text="Przypisanie"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="OrderNumber" runat="server" Text="OrderNumber"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <telerik:RadGrid ID="DataRadGrid" runat="server" OnNeedDataSource="DataRadGrid_OnNeedDataSource"
                    AutoGenerateColumns="False" GridLines="None" Width="100%">
                    <MasterTableView Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" EnableViewState="true">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa Towaru" UniqueName="MaterialName" DataField="MaterialName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Jm" UniqueName="UnitName" DataField="UnitName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="Shelf" DataField="Shelf">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <uc1:DocPrintLayoutFooter ID="DocPrintLayoutFooter1" runat="server" />
    </form>
</body>
<script language="javascript">
    window.print();
</script>
</html>
