﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentDefault.aspx.cs" Inherits="MagWeb.WarehouseDocuments.DocumentDefault" %>

<%@ Register TagPrefix="uc1" TagName="DocPrintLayoutFooter" Src="~/WarehouseDocuments/DocPrintLayoutFooter.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <table width="650">
            <tr>
                <td><b>Uwaga:</b></td>
            </tr>
            <tr>
                <td>Aby wydrukować wskazany dokument system wymaga najpierw zainstalowania szblonu wydruku dla typu <asp:Label ID="DocumentTypeLabel" runat="server" Text="" />.</td>
            </tr>
            <tr>
                <td>Zgłoszenie na <b>nową funkcjonalność</b> złóż w systemie Redmine:<br /><a href="http://willson-brown.com:3000/projects/k3/issues/new" target="_blank" />http://willson-brown.com:3000/projects/k3/issues/new</td>
            </tr>
        </table>
    </form>
</body>
</html>
