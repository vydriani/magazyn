﻿using System;
using System.Linq;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentRW : FeaturedSystemWebUIPage
    {
        private WarehouseDocument warehouseDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }

            SetParameters();
        }

        private void SetParameters()
        {
            int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            if (warehouseDocument.Warehouse.Company.Id == 1)
            {
                Company.Text = ConstForPrinting.companyWillson;
            }
            else
            {
                Company.Text = ConstForPrinting.companyMold;
            }

            DocumentDate.Text = DateTime.Now.ToString();
            DocumentId.Text = warehouseDocument.Id.ToString();
            Date.Text = warehouseDocument.Date.ToString();
            Warehouse.Text = warehouseDocument.Warehouse.Name;
            Author.Text = warehouseDocument.Person.FirstName + " " + warehouseDocument.Person.LastName;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                Dept.Text = (from p in warehouseDs.GetAllDepartmentsSIMPLE(currentComapnyId)
                            where p.komorka_id == warehouseDocument.TargetResponsibleDeptSIMPLEId
                            select p.nazwa).FirstOrDefault();

                var positions = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var firstPosition = positions.First();
                if (firstPosition.position.Order != null)
                {
                    OrderNumber.Text = firstPosition.position.Order.OrderNumber;
                }
                else
                {
                    OrderNumber.Text = "";
                }
            }
        }

        protected void DataRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int i = 1;

                var postionsForDocument = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);

                var docLocationInfo = warehouseDs.GetDocumentSourceZonesInfo(warehouseDocument.Id);

                var docPackageInfo = warehouseDs.GetDocumentPackagesInfo(warehouseDocument.Id);

                var src =
                    from p in postionsForDocument
                    group p by
                        new
                            {
                                IndexSIMPLE =
                        p.position.Material.MaterialSIMPLEs.FirstOrDefault() == null
                            ? ""
                            : p.position.Material.IndexSIMPLE,
                                MaterialName = p.position.Material.Name,
                                MaterialId = p.position.Material.Id,
                                UnitName = p.position.Material.Unit.Name,
                                ShelfName = p.shelf.Name,
                                PositionId = p.position.Id,
                                OrderNumber = p.position.Order != null ? p.position.Order.OrderNumber : ""
                            }
                        into g
                        orderby g.Key.PositionId

                        select
                        new
                            {
                                RowNumber = i++,
                                IndexSIMPLE = g.Key.IndexSIMPLE,
                                MaterialName = g.Key.MaterialName,
                                Quantity = g.Sum(p => p.quantity),
                                UnitName = g.Key.UnitName,
                                Shelf =   string.Join("; ",(from p in docLocationInfo where p.Item1 == g.Key.MaterialId select p.Item3 + "@" + p.Item4).ToArray()),
                                Package = string.Join("; ", (from p in docPackageInfo where p.Item1 == g.Key.MaterialId select "[" + p.Item3 + "] * " + p.Item4 + " z " + p.Item5).ToArray()),
                                OrderNumber = g.Key.OrderNumber
                            };


                DataRadGrid.DataSource = src.ToArray();
            }
        }
    }
}
