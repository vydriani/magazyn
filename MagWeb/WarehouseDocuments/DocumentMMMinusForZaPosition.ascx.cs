﻿using System;
using System.Linq;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentMMMinusForZaPosition : System.Web.UI.UserControl
    {
        private WarehouseDocument warehouseDocument;
        public WarehouseDocument WarehouseDocument
        {
            get { return warehouseDocument; }
            set { warehouseDocument = value; }
        }

        /*public DocumentMMMinusForZaPosition(int warehouseDocumentId)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }
        }*/

        protected void Page_Load(object sender, EventArgs e)
        {
            /*int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }

            SetParameters();*/
        }

        public void SetParameters()
        {
            //if (warehouseDocument.Warehouse1.Company.Id == 1)
            //{
            //    Company.Text = ConstForPrinting.companyWillson;
            //}
            //else
            //{
            //    Company.Text = ConstForPrinting.companyMold;
            //}

            DocumentId.Text = warehouseDocument.Id.ToString();
            
            WarehouseTarget.Text = warehouseDocument.Warehouse1.Name;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var positions = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var firstPosition = positions.First();
                if (firstPosition.position.Order != null)
                {
                    OrderNumber.Text = firstPosition.position.Order.OrderNumber;
                }
                else
                {
                    OrderNumber.Text = "";
                }

                //OrderingPerson
                var parentWarehouseDocumentAuthor = warehouseDs.GetParentWarehouseDocumentAuthor(warehouseDocument.Id);
                OrderingPerson.Text = parentWarehouseDocumentAuthor != null
                                   ? parentWarehouseDocumentAuthor.FirstName + " " + parentWarehouseDocumentAuthor.LastName
                                   : "";

                var parentWarehouseDocumentTimeStamp = warehouseDs.GetParentWarehouseDocumentTimeStamp(warehouseDocument.Id);
                Date.Text = parentWarehouseDocumentTimeStamp.HasValue
                                   ? parentWarehouseDocumentTimeStamp.Value.ToString("yyyy-MM-dd HH:mm")
                                   : "";

                var parentWarehouseDocumentDueTime = warehouseDs.GetParentWarehouseDocumentDueTime(warehouseDocument.Id);
                DueTime.Text = parentWarehouseDocumentDueTime.HasValue
                                   ? parentWarehouseDocumentDueTime.Value.ToString("yyyy-MM-dd HH:mm")
                                   : "";

                GiveTime.Text = warehouseDocument.TimeStamp.ToString("yyyy-MM-dd HH:mm");
            }
        }

        protected void DataRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int i = 1;
                var positionsForDocument = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var docLocationInfo = warehouseDs.GetDocumentSourceZonesInfo(warehouseDocument.Id);
                var docPackageInfo = warehouseDs.GetDocumentPackagesInfo(warehouseDocument.Id);
                var SourceDoc = warehouseDs.GetWarehouseDocumentSourceTypAndNumberByPositionId(positionsForDocument.Select(x => x.position.Id).FirstOrDefault());
                var src = positionsForDocument.GroupBy(x => new
                {
                    IndexSIMPLE = x.position.Material.IndexSIMPLE,
                    MaterialName = warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(x.position.Id)),
                    MaterialId = x.position.Material.Id,
                    UnitName = x.position.Material.Unit.ShortName,
                    ShelfName = x.shelf.Name,
                    PositionId = x.position.Id,
                    OrderNumber = x.position.Order!=null?x.position.Order.OrderNumber:"",
                    Source = SourceDoc
                }).Select(x => new
                {
                    RowNumber = i++,
                    IndexSIMPLE = x.Key.IndexSIMPLE,
                    MaterialName = x.Key.MaterialName,
                    Quantity = x.Sum(p => p.quantity),
                    UnitName = x.Key.UnitName,
                    Shelf = string.Join("; ", (docLocationInfo.Where(p => p.Item1 == x.Key.MaterialId).Select(p => p.Item3 + "@" + p.Item4).ToArray())),
                    Package = string.Join("; ", (docPackageInfo.Where(p => p.Item1 == x.Key.MaterialId).Select(p => "[" + p.Item3 + "] * " + p.Item4 + " z " + p.Item5).ToArray())),
                    OrderNumber = x.Key.OrderNumber,
                    Source = x.Key.Source != null ? x.Key.Source : "",
                });
                        


                //var src =
                //    from p in postionsForDocument
                //    group p by
                //        new
                //        {
                //            IndexSIMPLE =
                //    p.position.Material.IndexSIMPLE,
                //            MaterialName =
                //    warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id)),
                //            MaterialId = p.position.Material.Id,
                //            UnitName = p.position.Material.Unit.ShortName,
                //            ShelfName = p.shelf.Name,
                //            PositionId = p.position.Id,
                //            OrderNumber = p.position.Order.OrderNumber
                //        }
                //        into g
                //        orderby g.Key.PositionId

                //        select
                //        new
                //        {
                //            RowNumber = i++,
                //            IndexSIMPLE = g.Key.IndexSIMPLE,
                //            MaterialName = g.Key.MaterialName,
                //            Quantity = g.Sum(p => p.quantity),
                //            UnitName = g.Key.UnitName,
                //            Shelf = string.Join("; ", (from p in docLocationInfo where p.Item1 == g.Key.MaterialId select p.Item3 + "@" + p.Item4).ToArray()),
                //            Package = string.Join("; ", (from p in docPackageInfo where p.Item1 == g.Key.MaterialId select "[" + p.Item3 + "] * " + p.Item4 + " z " + p.Item5).ToArray()),
                //            OrderNumber = g.Key.OrderNumber
                //        };


                DataRadGrid.DataSource = src.ToArray();

                //from p in postionsForDocument
                //orderby p.position.Id
                //select
                //    new
                //        {
                //            RowNumber = i++,
                //            IndexSIMPLE = p.position.Material.MaterialSIMPLE.FirstOrDefault() != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault() == null.IndexSIMPLE : null,
                //            MaterialName = warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id)),//p.position.Material.Name,
                //            Quantity = p.quantity,
                //            UnitName = p.position.Material.Unit.Name,
                //            Shelf = p.shelf.Name
                //        };

            }
        }
    }
}