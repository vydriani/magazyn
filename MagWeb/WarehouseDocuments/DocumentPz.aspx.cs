﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentPz : FeaturedSystemWebUIPage
    {
        private WarehouseDocument warehouseDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument=warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }

            //SetDocumentType();
            SetParameters();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {


        }

        //private void SetDocumentType()
        //{
        //    if(warehouseDocument.WarehouseDocumentType.Id==(int)WarehouseDocumentTypeEnum.Pz)
        //    {
        //        DocumentType.Text = "PZ";
        //        DocumentDescription.Text = "Przyjęcie zewnętrzne";
        //    }
        //}

        private void SetParameters()
        {
            if(warehouseDocument.Warehouse1.Company.Id==1)
            {
                Company.Text = ConstForPrinting.companyWillson;
            }
            else
            {
                Company.Text = ConstForPrinting.companyMold;
            }

            DocumentDate.Text = DateTime.Now.ToString();
            DocumentId.Text = warehouseDocument.Number + " (" + warehouseDocument.Id.ToString() + ")";
            Date.Text = warehouseDocument.Date.ToString();
            Warehouse.Text = warehouseDocument.Warehouse1.Name;
            ContractingParty.Text = warehouseDocument.ContractingPartyForWarehouseDocuments.First().ContractingParty.Name;
            Author.Text = warehouseDocument.Person.FirstName + " " + warehouseDocument.Person.LastName;
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var positions = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var firstPosition = positions.First();
                OrderNumberPZ.Text = firstPosition.position.OrderDelivery.OrderNumberFromPZ;
            }
        }

        protected void DataRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int i = 1;

                var postionsForDocument = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var docPackageInfo = warehouseDs.GetDocumentPackagesInfo(warehouseDocument.Id);

                var src =
                    from p in postionsForDocument
                    group p by
                        new
                        {
                            IndexSIMPLE =
                    p.position.Material.IndexSIMPLE,
                            MaterialName = p.position.Material.Name,
                            MaterialId = p.position.Material.Id,
                            UnitName = p.position.Material.Unit.Name,
                            ShelfName = p.shelf.Name,
                            PositionId = p.position.Id,
                            OrderNumber = p.position.IsKC ? "~ nadwyżka ~" : (p.position.Order != null ? p.position.Order.OrderNumber :"")
                        }
                        into g
                        orderby g.Key.PositionId

                        select
                        new
                        {
                            RowNumber = i++,
                            IndexSIMPLE = g.Key.IndexSIMPLE,
                            MaterialName = g.Key.MaterialName,
                            Quantity = g.Sum(p => p.quantity),
                            UnitName = g.Key.UnitName,
                            Shelf = g.Key.ShelfName,
                            Package = string.Join("; ", (from p in docPackageInfo where p.Item1 == g.Key.MaterialId && (p.Item5 == g.Key.ShelfName || (p.Item5 == "" && g.Key.ShelfName == "")) select p.Item3 + " * " + p.Item4).ToArray()), 
                            OrderNumber = g.Key.OrderNumber,
                            WhDocPositionId = g.Key.PositionId
                        };

                DataRadGrid.DataSource = src.ToArray();
            }
        }

        protected void RadGrid_ItemDataBound(object aSender, GridItemEventArgs anEventArgs)
        {
            //Get the row from the grid.
            GridDataItem item = anEventArgs.Item as GridDataItem;
            if (item != null)
            {

                GridTableCell linkCell = (GridTableCell) item["TemplateLinkColumn"];
                HyperLink reportLink = (HyperLink) linkCell.FindControl("Link");

                // Set the text to the quote number
                reportLink.Text = "Drukuj etykietę";

                // Get PosId
                string PosID = item["WhDocPositionId"].Text;

                //Set the URL
                reportLink.NavigateUrl = "/Naklejka.aspx?PosID=" + PosID;

                //Tell it to open in a new window
                reportLink.Target = "_new";
            }
        }
    }
}
