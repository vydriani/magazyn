﻿using System;
using System.Linq;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentZTD : System.Web.UI.Page
    {
        private WarehouseDocument warehouseDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }

            SetParameters();
        }

        private void SetParameters()
        {
            if (warehouseDocument.Warehouse.Company.Id == 1)
            {
                Company.Text = ConstForPrinting.companyWillson;
            }
            else
            {
                Company.Text = ConstForPrinting.companyMold;
            }

            DocumentDate.Text = DateTime.Now.ToShortDateString();
            //DocumentId.Text = warehouseDocument.Id.ToString();
            //Date.Text = warehouseDocument.Date.ToString();
            //Warehouse.Text = warehouseDocument.Warehouse.Name;


            var contractingParty = warehouseDocument.ContractingPartyForWarehouseDocuments.First().ContractingParty;


            C_ImportSimple_ContractingPartyFromSimple2 contractingPartyFromSimple2 = null;
            using(var warehouseDs = new WarehouseDS())
            {
                contractingPartyFromSimple2 =
                    warehouseDs.GetContractingPartyFromSimple2ForCPK3(contractingParty.KontrahentSIMPLEId,
                                                                      contractingParty.Company.Id);

                ContractingParty.Text = contractingParty.Name + "<br />ul. " + contractingPartyFromSimple2.ulica + " " + contractingPartyFromSimple2.nrdomu + ((!string.IsNullOrEmpty(contractingPartyFromSimple2.nrmieszk) && !string.IsNullOrEmpty(contractingPartyFromSimple2.nrmieszk.Trim())) ? ("/" + contractingPartyFromSimple2.nrmieszk.Trim()) : "") + "<br />" + contractingPartyFromSimple2.kodpoczt + " " + contractingPartyFromSimple2.miasto;

                var exchangeDoc =
                    warehouseDs.GetDataExchangeDocumentForMag3Doc(warehouseDocument.Id).FirstOrDefault();
                if (exchangeDoc != null)
                {

                    Label_DocNumber.Text = (!string.IsNullOrEmpty(exchangeDoc.SIMPLEDocNumber) && !string.IsNullOrEmpty(exchangeDoc.SIMPLEDocNumber.Trim())) ? exchangeDoc.SIMPLEDocNumber.Trim() : "(K3#" + exchangeDoc.WarehouseDocumentId + ")";
                }
                else
                {
                    Label_DocNumber.Text = "???";
                }
            }

            
            //Author.Text = warehouseDocument.Person.FirstName + " " + warehouseDocument.Person.LastName;
            //OrderNumber.Text = "";
            //OrderNumber.Text =
        }

        protected void DataRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int i = 1;

                var postionsForDocument = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);

                //var src =
                //    from p in postionsForDocument
                //    group p by
                //        new
                //        {
                //            IndexSIMPLE =
                //    p.position.Material.MaterialSIMPLE.FirstOrDefault() == null
                //        ? ""
                //        : p.position.Material.MaterialSIMPLE.FirstOrDefault().IndexSIMPLE,
                //            MaterialName =
                //    warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id)),
                //            UnitName = p.position.Material.Unit.Name,
                //            ShelfName = p.shelf.Name,
                //            PositionId = p.position.Id,
                //            OrderNumber = p.position.Order.OrderNumber
                //        }
                //        into g
                //        orderby g.Key.PositionId

                //        select
                //        new
                //        {
                //            RowNumber = i++,
                //            IndexSIMPLE = g.Key.IndexSIMPLE,
                //            MaterialName = g.Key.MaterialName,
                //            Quantity = g.Sum(p => p.quantity),
                //            UnitName = g.Key.UnitName,
                //            Shelf = g.Key.ShelfName,
                //            OrderNumber = g.Key.OrderNumber
                //        };

                var docLocationInfo = warehouseDs.GetDocumentSourceZonesInfo(warehouseDocument.Id);
                var docPackageInfo = warehouseDs.GetDocumentPackagesInfo(warehouseDocument.Id);

                var src =
                    from p in postionsForDocument
                    group p by
                        new
                        {
                            IndexSIMPLE =
                    p.position.Material.IndexSIMPLE,
                            MaterialName =
                    warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id)),
                            MaterialId = p.position.Material.Id,
                            UnitName = p.position.Material.Unit.ShortName,
                            ShelfName = p.shelf.Name,
                            PositionId = p.position.Id,
                            OrderNumber = p.position.Order != null ? p.position.Order.OrderNumber : ""
                        }
                        into g
                        orderby g.Key.PositionId

                        select
                        new
                        {
                            RowNumber = i++,
                            IndexSIMPLE = g.Key.IndexSIMPLE,
                            MaterialName = g.Key.MaterialName,
                            Quantity = Math.Abs(g.Sum(p => p.quantity)),
                            UnitName = g.Key.UnitName,
                            Shelf = string.Join("; ", (from p in docLocationInfo where p.Item1 == g.Key.MaterialId select p.Item3 + "@" + p.Item4).ToArray()),
                            Package = string.Join("; ", (from p in docPackageInfo where p.Item1 == g.Key.MaterialId select "[" + p.Item3 + "] * " + p.Item4 + " z " + p.Item5).ToArray()),
                            OrderNumber = g.Key.OrderNumber
                        };

                DataRadGrid.DataSource = src.ToArray();
            }
        }
    }
}