﻿using System;
using System.Linq;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentZrPlus : FeaturedSystemWebUIPage
    {
        private WarehouseDocument warehouseDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }

            SetParameters();
        }

        private void SetParameters()
        {
            if (warehouseDocument.Warehouse1.Company.Id == 1)
            {
                Company.Text = ConstForPrinting.companyWillson;
            }
            else
            {
                Company.Text = ConstForPrinting.companyMold;
            }

            DocumentDate.Text = DateTime.Now.ToString();
            DocumentId.Text = warehouseDocument.Id.ToString();
            Date.Text = warehouseDocument.Date.ToString();
            WarehouseSource.Text = warehouseDocument.Warehouse.Name;
            WarehouseTarget.Text = warehouseDocument.Warehouse1.Name;
            Author.Text = warehouseDocument.Person.FirstName + " " + warehouseDocument.Person.LastName;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var positions = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var firstPosition = positions.First();
                if (firstPosition.position.Order != null)
                {
                    OrderNumber.Text = firstPosition.position.Order.OrderNumber;
                }
                else
                {
                    OrderNumber.Text = "";
                }
            }
        }

        protected void DataRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int i = 1;

                var postionsForDocument = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);

                var src =
                    from p in postionsForDocument
                    group p by
                        new
                        {
                            IndexSIMPLE =
                    p.position.Material.IndexSIMPLE,
                            MaterialName =
                    warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id)),
                            UnitName = p.position.Material.Unit.Name,
                            ShelfName = p.shelf.Name,
                            PositionId = p.position.Id
                        }
                        into g
                        orderby g.Key.PositionId

                        select
                        new
                        {
                            RowNumber = i++,
                            IndexSIMPLE = g.Key.IndexSIMPLE,
                            MaterialName = g.Key.MaterialName,
                            Quantity = g.Sum(p => p.quantity),
                            UnitName = g.Key.UnitName,
                            Shelf = g.Key.ShelfName
                        };

                DataRadGrid.DataSource = src.ToArray();
            }
        }
    }
}
