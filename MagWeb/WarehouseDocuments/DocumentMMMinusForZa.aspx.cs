﻿using System;
using System.Web.UI;
using Mag.Domain;
using Mag.Domain.Model;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentMMMinusForZa : FeaturedSystemWebUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DocumentMMMinusForZaPositionsPanel.Controls.Clear();

            //int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);
            var documentIds = Page.Request.QueryString["documentids"];
            if (documentIds != null)
            {
                WarehouseDocument firstWarehouseDocument = null;

                var documentIdsArr = documentIds.Split('_');
                foreach (var documentId in documentIdsArr)
                {
                    int warehouseDocumentId = -1;
                    int.TryParse(documentId, out warehouseDocumentId);
                    if (warehouseDocumentId > 0)
                    {
                        //var documentMmMinusForZaPosition = new DocumentMMMinusForZaPosition(warehouseDocumentId);
                        //documentMmMinusForZaPosition.ID = String.Format("DocumentMmMinusForZaPosition{0}", i);
                        var documentMmMinusForZaPosition = (DocumentMMMinusForZaPosition)Page.LoadControl("DocumentMMMinusForZaPosition.ascx");
                        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            var warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
                            if (firstWarehouseDocument == null)
                            {
                                firstWarehouseDocument = warehouseDocument;
                            }
                            documentMmMinusForZaPosition.WarehouseDocument = warehouseDocument;
                            documentMmMinusForZaPosition.SetParameters();
                        }
                        DocumentMMMinusForZaPositionsPanel.Controls.Add(documentMmMinusForZaPosition);
                    }
                }

                if (firstWarehouseDocument != null)
                {
                    SetParameters(firstWarehouseDocument);
                }
            }
        }

        private void SetParameters(WarehouseDocument warehouseDocument)
        {
            DocumentDate.Text = DateTime.Now.ToString();

            if (warehouseDocument.Warehouse.Company.Id == 1)
            {
                Company.Text = ConstForPrinting.companyWillson;
            }
            else
            {
                Company.Text = ConstForPrinting.companyMold;
            }

            WarehouseSource.Text = warehouseDocument.Warehouse.Name;

            Author.Text = warehouseDocument.Person.FirstName + " " + warehouseDocument.Person.LastName;
        }
    }
}