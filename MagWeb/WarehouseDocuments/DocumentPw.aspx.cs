﻿using System;
using System.Linq;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentPW : FeaturedSystemWebUIPage
    {
        private WarehouseDocument warehouseDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            int warehouseDocumentId = int.Parse(Page.Request.QueryString["documentid"]);

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                warehouseDocument = warehouseDs.GetWarehouseDocumentById(warehouseDocumentId);
            }

            //SetDocumentType();
            SetParameters();
        }

        //private void SetDocumentType()
        //{
        //    if(warehouseDocument.WarehouseDocumentType.Id==(int)WarehouseDocumentTypeEnum.Pz)
        //    {
        //        DocumentType.Text = "PZ";
        //        DocumentDescription.Text = "Przyjęcie zewnętrzne";
        //    }
        //}

        private void SetParameters()
        {
            if (warehouseDocument.Warehouse1.Company.Id == 1)
            {
                Company.Text = ConstForPrinting.companyWillson;
            }
            else
            {
                Company.Text = ConstForPrinting.companyMold;
            }

            DocumentDate.Text = DateTime.Now.ToString();
            DocumentId.Text = warehouseDocument.Id.ToString();
            Date.Text = warehouseDocument.Date.ToString();
            Warehouse.Text = warehouseDocument.Warehouse1.Name;
            Author.Text = warehouseDocument.Person.FirstName + " " + warehouseDocument.Person.LastName;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var positions = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);
                var firstPosition = positions.First();
                if (firstPosition.position.Order != null)
                {
                    OrderNumber.Text = firstPosition.position.Order.OrderNumber;
                }
                else
                {
                    OrderNumber.Text = "";
                }
            }
        }

        protected void DataRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int i = 1;

                var postionsForDocument = warehouseDs.GetWarehouseDocumentPositionsWithShelfByWarehouseDocumentId(warehouseDocument.Id);


                var src =
                    from p in postionsForDocument
                    group p by
                        new
                        {
                            IndexSIMPLE =
                    p.position.Material.IndexSIMPLE,
                            MaterialName = p.position.Material.Name,
                            UnitName = p.position.Material.Unit.Name,
                            ShelfName = p.shelf.Name,
                            PositionId = p.position.Id,
                            OrderNumber = p.position.Order.OrderNumber
                        }
                        into g
                        orderby g.Key.PositionId

                        select
                        new
                        {
                            RowNumber = i++,
                            IndexSIMPLE = g.Key.IndexSIMPLE,
                            MaterialName = g.Key.MaterialName,
                            Quantity = g.Sum(p => p.quantity),
                            UnitName = g.Key.UnitName,
                            Shelf = g.Key.ShelfName,
                            OrderNumber = g.Key.OrderNumber
                        };

                DataRadGrid.DataSource = src.ToArray();
            }
        }
    }
}
