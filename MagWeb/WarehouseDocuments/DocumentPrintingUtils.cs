﻿using System.Collections.Generic;
using System.Text;
using Mag.Domain.Model;
using System.Diagnostics.Contracts;

namespace MagWeb.WarehouseDocuments
{
    public class DocumentPrintingUtils
    {
        public static void BindDocumentToButton(System.Web.UI.WebControls.Button button, WarehouseDocumentTypeEnum warehouseDocumentTypeEnum, int documentId)
      {
        Contract.Requires(button != null);

            string width = "700";
            string height = "600";
            string pagePath = null;
            string windowName = null;

            if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Pz)
            {
                pagePath = @"WarehouseDocuments/DocumentPz.aspx?documentid=" + documentId;
                windowName = "DokumentPz";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.MMMinus)
            {
                pagePath = @"WarehouseDocuments/DocumentMMMinus.aspx?documentid=" + documentId;
                windowName = "DokumentMMMinus";                
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.MMPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentMMPlus.aspx?documentid=" + documentId;
                windowName = "DokumentMMPlus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Rw)
            {
                pagePath = @"WarehouseDocuments/DocumentRw.aspx?documentid=" + documentId;
                windowName = "DokumentRw";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Pw)
            {
                pagePath = @"WarehouseDocuments/DocumentPw.aspx?documentid=" + documentId;
                windowName = "DokumentPw";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Wz)
            {
                pagePath = @"WarehouseDocuments/DocumentWz.aspx?documentid=" + documentId;
                windowName = "DokumentWz";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Za)
            {
                pagePath = @"WarehouseDocuments/DocumentZa.aspx?documentid=" + documentId;
                windowName = "DokumentZa";                
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwMinus)
            {
                pagePath = @"WarehouseDocuments/DocumentZwMinus.aspx?documentid=" + documentId;
                windowName = "DokumentZwMinus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentZwPlus.aspx?documentid=" + documentId;
                windowName = "DokumentZwPlus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentPOMinus.aspx?documentid=" + documentId;
                windowName = "DokumentPOMinus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentPOPlus.aspx?documentid=" + documentId;
                windowName = "DokumentPOPlus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZTD)
            {
                pagePath = @"WarehouseDocuments/DocumentZTD.aspx?documentid=" + documentId;
                windowName = "DocumentZwD";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.INW)
            {
                pagePath = @"WarehouseDocuments/DocumentINW.aspx?documentid=" + documentId;
                windowName = "DocumentINW";
            }
            else
            {
                pagePath = @"WarehouseDocuments/DocumentDefault.aspx?documenttype=" + warehouseDocumentTypeEnum;
                windowName = "DocumentDefault";
            }

            string windowAttribs = "width=" + width + "px," +
                                   "height=" + height + "px," +
                                   "left=((screen.width -" + width + ") / 2)," +
                                   "top=((screen.height - " + height + ") / 2)," +
                                   "scrollbars=yes";


            string clientScript = "window.open('" + pagePath + "','" + windowName + "','" + windowAttribs + "');return false;";

            button.Attributes.Add("OnClick", clientScript);
        }

        public static void BindDocumentToButton(System.Web.UI.WebControls.Button button, WarehouseDocumentTypeEnum warehouseDocumentTypeEnum, IEnumerable<int> documentIds)
        {
            string width = "1000";
            string height = "800";
            string pagePath = null;
            string windowName = null;

            StringBuilder documentIdsStringBuilder = new StringBuilder();
            if (documentIds != null)
            {
                foreach (var documentId in documentIds)
                {
                    documentIdsStringBuilder.Append(documentId);
                    documentIdsStringBuilder.Append('_');
                }
            }
            var documentIdsStr = documentIdsStringBuilder.ToString().Trim('_');

            if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Pz)
            {
                pagePath = @"WarehouseDocuments/DocumentPz.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentPz";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.MMMinus)
            {
                pagePath = @"WarehouseDocuments/DocumentMMMinusForZa.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentMMMinus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.MMPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentMMPlus.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentMMPlus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Rw)
            {
                pagePath = @"WarehouseDocuments/DocumentRw.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentRw";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Pw)
            {
                pagePath = @"WarehouseDocuments/DocumentPw.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentPw";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Wz)
            {
                pagePath = @"WarehouseDocuments/DocumentWz.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentWz";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.Za)
            {
                pagePath = @"WarehouseDocuments/DocumentZa.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentZa";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwMinus)
            {
                pagePath = @"WarehouseDocuments/DocumentZwMinus.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentZwMinus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentZwPlus.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentZwPlus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentPOMinus.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentPOMinus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZwPlus)
            {
                pagePath = @"WarehouseDocuments/DocumentPOPlus.aspx?documentids=" + documentIdsStr;
                windowName = "DokumentPOPlus";
            }
            else if (warehouseDocumentTypeEnum == WarehouseDocumentTypeEnum.ZTD)
            {
                pagePath = @"WarehouseDocuments/DocumentZTD.aspx?documentids=" + documentIdsStr;
                windowName = "DocumentZwD";
            }

            string windowAttribs = "width=" + width + "px," +
                                   "height=" + height + "px," +
                                   "left=((screen.width -" + width + ") / 2)," +
                                   "top=((screen.height - " + height + ") / 2)," +
                                   "scrollbars=yes";


            string clientScript = "window.open('" + pagePath + "','" + windowName + "','" + windowAttribs + "');return false;";

            button.Attributes.Add("OnClick", clientScript);
        }
    }
}
