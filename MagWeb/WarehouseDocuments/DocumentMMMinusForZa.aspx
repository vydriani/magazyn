﻿<%@ Page Language="C#" AutoEventWireup="true" EnableTheming="false" CodeBehind="DocumentMMMinusForZa.aspx.cs" Inherits="MagWeb.WarehouseDocuments.DocumentMMMinusForZa" %>

<%@ Register Src="DocPrintLayoutFooter.ascx" TagName="DocPrintLayoutFooter" TagPrefix="uc1" %>
<%@ Register Src="DocumentMMMinusForZaPosition.ascx" TagName="DocumentMMMinusForZaPosition" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body class="printOut">
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>

	<div class="printDate">
		<asp:Label ID="Label22" runat="server" Text="Data wydruku:"></asp:Label> 
        <asp:Label ID="DocumentDate" CssClass="printData" runat="server" Text="DocumentDate"></asp:Label>
	</div>

    <table class="printHeader">
        <tr>
            <td rowspan="3">
                <asp:Label ID="Company" runat="server" Text="Company"></asp:Label>
            </td>
            <td>
                <asp:Label ID="DocumentType" runat="server" Text="MM-/Za"></asp:Label>
            </td>
            <td>
                <asp:Label ID="DocumentDescription" runat="server" Text="Wydanie do Zamówienia"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Z magazynu"></asp:Label>
            </td>
            <td>
                <asp:Label ID="WarehouseSource" runat="server" Text="WarehouseSource"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Operator"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Author" runat="server" Text="Author"></asp:Label>
            </td>
        </tr>
    </table>

    <br />

    <asp:Panel ID="DocumentMMMinusForZaPositionsPanel" runat="server">
    </asp:Panel>
    
    <uc1:DocPrintLayoutFooter ID="DocPrintLayoutFooter1" runat="server" />
    </form>
</body>
<script language="javascript" type="text/javascript">
    window.print();
</script>
</html>
