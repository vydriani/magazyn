﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentZTD.aspx.cs" Inherits="MagWeb.WarehouseDocuments.DocumentZTD" %>

<%@ Register TagPrefix="uc1" TagName="DocPrintLayoutFooter" Src="~/WarehouseDocuments/DocPrintLayoutFooter.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <h3 align="center">
        ZWROT TOWARU DO DOSTAWCY</h3>
    <h3 align="center">
        <asp:Label ID="Label_DocNumber" runat="server" Text="ZTD/TEST/2012/00001"></asp:Label>
        
    </h3>
    <table width="650">
        <tr>
            <td style="text-align: left; vertical-align: top;">
                <u><b>Dane zamawiającego towar:</b></u>
                <br />
                <asp:Label ID="Company" runat="server" Text="Company"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="text-align: right; vertical-align: top;">
                Warszawa,
                <asp:Label ID="DocumentDate" runat="server" Text="DocumentDate"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="650">
        <tr>
            <td style="text-align: right; vertical-align: top;">
                <h3 align="right">
                    <u>Dane odbierającego towar:</u></h3>
                <p align="right">
                    <asp:Label ID="ContractingParty" runat="server" Text="ContractingParty"></asp:Label>
                </p>
            </td>
        </tr>
    </table>
    <table width="650" border="0">
        <%--        <tr>
            <td rowspan="6">

            </td>


        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Nr dokumentu"></asp:Label>
            </td>
            <td>
                <asp:Label ID="DocumentId" runat="server" Text="DocumentId"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Data wydania"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Date" runat="server" Text="Date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Magazyn"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Warehouse" runat="server" Text="Warehouse"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Odbiorca"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ContractingParty" runat="server" Text="ContractingParty"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Operator"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Author" runat="server" Text="Author"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label11" runat="server" Text="Zamówienie"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="OrderNumber" runat="server" Text="OrderNumber"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td colspan="3">
                <telerik:RadGrid ID="DataRadGrid" runat="server" OnNeedDataSource="DataRadGrid_OnNeedDataSource"
                    AutoGenerateColumns="False" GridLines="None" Width="100%">
                    <MasterTableView Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" EnableViewState="true">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Identyfikator" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nazwa produktu" UniqueName="MaterialName" DataField="MaterialName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Jm" UniqueName="UnitName" DataField="UnitName">
                            </telerik:GridBoundColumn>
                            <%--                            <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="Shelf" DataField="Shelf">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber">
                            </telerik:GridBoundColumn>--%>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <uc1:DocPrintLayoutFooter ID="DocPrintLayoutFooter1" runat="server" />
    </form>
</body>
<script language="javascript">
    window.print();
</script>
</html>
