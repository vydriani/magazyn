﻿using System;

namespace MagWeb.WarehouseDocuments
{
    public partial class DocumentDefault : FeaturedSystemWebUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string warehouseDocumentType = Page.Request.QueryString["documenttype"];
            DocumentTypeLabel.Text = warehouseDocumentType;
        }

    }
}
