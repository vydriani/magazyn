﻿using System;
using System.Web.UI;

namespace MagWeb
{
    public partial class UserManagementMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			//TODO do this with Membership roles
            if (!(bool)Session[SessionData.SessionKey_LoggedUserHasAdminPrivilegesForSecurity])
            {
                Response.Redirect("Login.aspx");
            }

            if (Session["tab_user_mgmt"] == null)
            {
                RadToolBar1.Tabs[0].Selected = true;
                Session["tab_user_mgmt"] = 0;
                Response.Redirect(RadToolBar1.Tabs[0].Target);
            }
        }

        protected void Tabs_OnClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            Session["tab_user_mgmt"] = RadToolBar1.SelectedIndex;

            Response.Redirect(e.Tab.Target);
        }

        //
        protected void Button_Back_Click(object sender, EventArgs e)
        {
        }


        protected void Tab_Prerender(object sender, EventArgs e)
        {
            int selected_index = (int)Session["tab_user_mgmt"];
            RadToolBar1.Tabs[selected_index].Selected = true;
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (WebTools.AlertMsgIsAlert())
            {
                this.Panel_Alert.Visible = true;
                this.Label_Alert.Text = WebTools.AlertMsgRead();
            }
            else
            {
                this.Panel_Alert.Visible = false;
                this.Label_Alert.Text = "";
            }
        }
    }
}
