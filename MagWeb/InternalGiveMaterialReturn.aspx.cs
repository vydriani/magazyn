using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
//using Client.Domain;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;
//using Warehouse.Agent;
//using WarehouseDocument = Mag.Domain.Model.WarehouseDocument;
//using WarehouseDocumentPosition = Mag.Domain.Model.WarehouseDocumentPosition;
//using WarehouseDocumentTypeEnum = Mag.Domain.Model.WarehouseDocumentTypeEnum;
//using WarehousePositionZone = Mag.Domain.Model.WarehousePositionZone;
//using Zone = Mag.Domain.Model.Zone;

namespace MagWeb
{
    public partial class GiveMaterialReturn : FeaturedSystemWebUIPage
    {
        private const int INDEX_QUANTITY_IN_GRID = 5;
        private const int INDEX_UNITPRICE_IN_GRID = 7;
        private const int INDEX_ORDERNUMBER_IN_GRID = 8;
        private const int INDEX_MATERIAL_ID_IN_GRID = 9;

        private const int INDEX_SOURCEWAREHOUSEID_NAME_GRID = 10;
        private const int INDEX_SOURCEWAREHOUSEID_IN_GRID = 11;

        private List<PositionWithQuantityAndShelfs> positionsToGive;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["tab_ReturnMaterial"] = 0;

            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, null, WarehouseDocumentTypeEnum.ZwMinus, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
          PermissionDenied();
          return;
                }
            }

            if (!IsPostBack)
            {
                Session["GiveMaterialReturn_positionsToGive"] = null;

                inputDate.SelectedDate = DateTime.Now.Date;
            }

            if (Session["GiveMaterialReturn_positionsToGive"] == null)
                positionsToGive = new List<PositionWithQuantityAndShelfs>();
            else
                positionsToGive = (List<PositionWithQuantityAndShelfs>)Session["GiveMaterialReturn_positionsToGive"];

            if (positionsToGive.Count == 0)
                this.Label_SourceWarehouse.Text = "nieokreślony";



      //this.SignatureControl1.DefaultComment = "Przyjąłem materiał zgodny z opisem w niniejszym dokumencie.";
      //this.SignatureControl1.PositionsToSign = positionsToGive;

            Label_SourceZones.Text = "";

      using (var warehouseDs = new WarehouseDS())
            {
                bool isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility(currentWarehouseId.Value,
                                                                                      warehouseDs.GetPersonForSystemUser
                                                                                          (userId).Id, 11);
                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Setting name of source warehouse (destination for the return), only if we have sth to add so far
      if (positionsToGive.Any())
            {
                PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
                    positionsToGive.First();
                this.Label_SourceWarehouse.Text = positionsWithShelfsWhichExists.WarehouseSourceName;
            }

            this.btnSaveDocument.Visible = this.btnSaveDocument.Enabled;
        }

        protected void MaterialsToGiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var companyId = (int?)Session[SessionData.SessionKey_CurrentCompanyId];
            var dateToFilter = DateTime.Now;
            var indexSimpleToFind = IndexSimpleRTB.Text;

            var materialToFind = MaterialToSearchRTB.Text;
            var orderNumberToFind = OrderNumberToSearchRTB.Text;
            var currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];

      //IQueryable<MaterialsToReturnFromWarehouse> MaterialsToGiveTotalFromWarehouseGroupeds;

      //using (WarehouseAgent warehouseAgent = new WarehouseAgent())
      //{
      //    warehouseAgent.Information.GetCurrentStacksForReturns(companyId, dateToFilter, materialToFind, indexSimpleToFind, currentWarehouseId, orderNumberToFind);
      //    MaterialsToGiveTotalFromWarehouseGroupeds = warehouseAgent.Information.GetCurrentStacksForReturnsResult.Where(p => p.WarehouseSourceId.HasValue);
      //}
      using (var warehouseDs = new WarehouseDS())
      {
        var finalMaterialsToGiveDataSource = warehouseDs.GetCurrentStacksForReturns
          (
            companyId, 
            dateToFilter, 
            materialToFind, 
            indexSimpleToFind, 
            currentWarehouseId, 
            orderNumberToFind,
            positionsToGive
          );
        //  .Where(p => p.WarehouseSourceId.HasValue);

        //var finalMaterialsToGiveDataSource = from p in materialsToGiveTotalFromWarehouseGroupeds
        //  select new
        //  {
        //      MaterialName = p.MaterialName,
        //      IndexSIMPLE = p.IndexSIMPLE,
        //      NameSIMPLE = p.NameSIMPLE,
        //      Quantity = p.Quantity - Math.Abs
        //      (
        //        (
        //          from v in positionsToGive
        //          where
        //            v.position.MaterialId == p.MaterialId &&
        //            (
        //              ((v.position.Order == null || v.position.Order.OrderNumber == "") && !p.OrderId.HasValue) ||
        //              (v.position.Order != null && v.position.OrderId == p.OrderId)
        //            )
        //          select v
        //        )
        //        .Sum(w => w.position.Quantity)
        //      ), // - (from v in positionsToExpend select v.positionParentIds.Where(w => w.PositionParentID == p.PositionId).Sum(x => x.Quantity)).Sum(y => y),
        //      UnitName = p.UnitName,
        //      UnitPrice = p.UnitPrice,
        //      OrderNumber = p.OrderNumber,
        //      MaterialId = p.MaterialId,
        //      OrderId = p.OrderId,
        //      SourceWarehouseId = p.WarehouseSourceId,
        //      SourceWarehouseName = p.WarehouseSourceName
        //  };

        MaterialsToGiveRadGrid.DataSource = finalMaterialsToGiveDataSource.ToList();
        }
    }

        protected void MaterialsToGiveRadGrid_OnNeedDataSource_old(object source, GridNeedDataSourceEventArgs e)
        {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string materialToFind = MaterialToSearchRTB.Text;
                string orderNumberToFind = OrderNumberToSearchRTB.Text;
                int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];


                // only of doc type 27 (PP)
                var materialToOrderWithoutPositions =
                    from p in warehouseDs.GetCurrentStackInDocuments(materialToFind, "", currentWarehouseId)
                    where (String.IsNullOrEmpty(orderNumberToFind) || p.OrderNumber.Contains(orderNumberToFind)) && p.WarehouseSourceId != null
                    group p by new { p.MaterialId, p.MaterialName, p.IndexSIMPLE, p.NameSIMPLE, p.UnitName, p.OrderId, p.OrderNumber, p.WarehouseSourceId, p.WarehouseSourceName, p.PositionId }
                        into g
                        orderby g.Key.PositionId
                        select
                        new
                        {
                            g.Key.MaterialId,
                            g.Key.MaterialName,
                            g.Key.IndexSIMPLE,
                            g.Key.NameSIMPLE,
                            Quantity = g.Sum(s => s.Quantity),
                            g.Key.UnitName,
                            g.Key.OrderId,
                            g.Key.OrderNumber,
                            g.Key.WarehouseSourceId,
                            g.Key.WarehouseSourceName,
                            g.Key.PositionId
                        };

                var materialToOrderWithoutPositionsMaterialized = materialToOrderWithoutPositions.ToArray();

        List<CurrentStackInDocument> currentStackInDocumentses = new List<CurrentStackInDocument>();

                foreach (var materialized in materialToOrderWithoutPositionsMaterialized)
                {
                    double usedQuantity = 0;

                    var positionWQASs =
                        positionsToGive.Where(
                            p =>
                            p.position.Material.Id == materialized.MaterialId && p.position.Material.Name == materialized.MaterialName
                            &&
                            (
                                (
                                    (
                                        p.position.Order == null || string.IsNullOrEmpty(p.position.Order.OrderNumber)
                                    )
                                    &&
                                    (
                                        materialized.OrderNumber == null
                                        ||
                                        materialized.OrderNumber == ""
                                    )
                                )
                                ||
                                p.position.Order.OrderNumber == materialized.OrderNumber
                            )


                            );

                    if (positionWQASs.Count() > 0)
                    {
                        foreach (var wqaS in positionWQASs)
                        {
                            usedQuantity += Math.Abs(wqaS.position.Quantity);
                        }
                    }

          currentStackInDocumentses.Add(new CurrentStackInDocument()
                    {
                        MaterialId = materialized.MaterialId,
                        MaterialName = materialized.MaterialName,
                        IndexSIMPLE = materialized.IndexSIMPLE,
                        Quantity = materialized.Quantity - usedQuantity,
                        UnitName = materialized.UnitName,
                        OrderNumber = materialized.OrderNumber,
                        WarehouseSourceId = materialized.WarehouseSourceId,
                        WarehouseSourceName = materialized.WarehouseSourceName,
                        PositionId = materialized.PositionId
                    });
                }

                int i = 1;

                var materialToGive = from p in currentStackInDocumentses
                                     select
                                      new
                                      {
                                          RowNumber = i++,
                                          IdMaterial = p.MaterialId,
                                          MaterialName = p.MaterialName,
                                          Quantity = p.Quantity,
                                          UnitName = p.UnitName,
                                          OrderNumber = p.OrderNumber,
                                          SourceWarehouseName = p.WarehouseSourceName,
                                          SourceWarehouseID = p.WarehouseSourceId
                                      };

                MaterialsToGiveRadGrid.DataSource = materialToGive.ToArray();
            }
        }

        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            MaterialsToGiveRadGrid.Rebind();
        }

        //protected void AddMaterialToGiveClick_old(object sender, EventArgs e)
        //{
        //    if (IsValid)
        //    {
        //        WarehouseDocumentPosition warehouseDocumentPositionCandidate;
        //        GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
        //        var cell = item.Cells[INDEX_MATERIAL_ID_IN_GRID];
        //        int materialId = int.Parse(cell.Text);
        //        var orderNumber = item.Cells[INDEX_ORDERNUMBER_IN_GRID].Text;
        //        int sourceWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

        //        double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));
        //        IQueryable<CurrentStackInDocuments> stack;
        //        CurrentStackInDocuments[] materializedStack;

    //        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //        {
        //            warehouseDocumentPositionCandidate = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, 0,
        //                                                                                                     quantity);
        //            stack = from p in warehouseDs.GetCurrentStackInDocuments("", "", sourceWarehouseId)
        //                    where p.MaterialId == materialId && ((orderNumber == "" && p.OrderNumber == null) || (p.OrderNumber == orderNumber))
        //                    select p;

        //            materializedStack = stack.ToArray();
        //        }

        //        foreach (var t in materializedStack.OrderBy(s => s.PositionId))
        //        {
    //            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //            {
        //                WarehouseDocumentPosition warehouseDocumentPosition;

        //                double used = 0;

        //                var materialMatch = positionsToGive.Where(p => p.position.Material.Id == t.MaterialId);
        //                foreach (var match in materialMatch)
        //                {
        //                    var foundShelf = match.WarehousePositionZones.FirstOrDefault(pz => pz.Zone.Id == t.ShelfId);
        //                    if (foundShelf != null)
        //                    {
        //                        used = foundShelf.Quantity;
        //                    }
        //                }

        //                if (warehouseDocumentPositionCandidate.Quantity <= t.Quantity - used)
        //                {
        //                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, t.UnitPrice,
        //                                                                                                    -warehouseDocumentPositionCandidate.Quantity,
        //                                                                                                    t.PositionId, (int)(t.OrderId ?? 0));

        //                    warehouseDocumentPositionCandidate.Quantity = 0;
        //                }
        //                else
        //                {
        //                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, t.UnitPrice,
        //                                                                                                    (double)-t.Quantity + used, t.PositionId, (int)(t.OrderId ?? 0));

        //                    warehouseDocumentPositionCandidate.Quantity -= (double)t.Quantity - used;
        //                }

        //                if (warehouseDocumentPosition.Quantity == 0) continue;

        //                if (positionsToGive.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id) == 1)
        //                {
        //                    PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
        //                        positionsToGive.First(
        //                            p =>
        //                            p.position.Material.Id == warehouseDocumentPosition.Material.Id);

        //                    if (Math.Abs(positionsWithShelfsWhichExists.position.Quantity + warehouseDocumentPosition.Quantity) <= materializedStack.Sum(m => m.Quantity))
        //                    {
        //                        positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
        //                        positionsWithShelfsWhichExists.documentParentIds.Add(t.WarehouseDocumentId);
        //                        positionsWithShelfsWhichExists.positionParentIds.Add(new ChildParentPositionQuantity()
        //                        {
        //                            PositionParentID = t.PositionId,
        //                            Quantity = -warehouseDocumentPosition.Quantity,
        //                            LocationId = t.ShelfId
        //                        });

        //                        var positionWithZone =
        //                            positionsWithShelfsWhichExists.WarehousePositionZones.FirstOrDefault(pswz => pswz.Zone.Id == t.ShelfId);
        //                        if (positionWithZone != null)
        //                        {
        //                            positionWithZone.Quantity += -warehouseDocumentPosition.Quantity;
        //                        }
        //                        else
        //                        {
        //                            Zone zone = warehouseDs.GetShelfByIdDataOnly((int)t.ShelfId);

        //                            positionsWithShelfsWhichExists.WarehousePositionZones.Add(new WarehousePositionZone()
        //                            {
        //                                WarehouseDocumentPosition = positionsWithShelfsWhichExists.position,
        //                                Zone = zone,
        //                                Quantity = -warehouseDocumentPosition.Quantity
        //                            });
        //                        }
        //                    }
        //                    else
        //                    {
        //                        warehouseDocumentPositionCandidate.Quantity += (double)t.Quantity;
        //                    }
        //                }
        //                else
        //                {
        //                    List<WarehousePositionZone> warehousePositionZones = null;

        //                    Zone zone = warehouseDs.GetShelfByIdDataOnly((int)t.ShelfId);

        //                    if (zone != null)
        //                    {
        //                        warehousePositionZones = new List<WarehousePositionZone>();

        //                        warehousePositionZones.Add(new WarehousePositionZone()
        //                        {
        //                            WarehouseDocumentPosition = warehouseDocumentPosition,
        //                            Zone = zone,
        //                            Quantity = -warehouseDocumentPosition.Quantity
        //                        });
        //                    }

        //                    double? availableQuantityInStack = materializedStack.Sum(m => m.Quantity);

        //                    if (availableQuantityInStack == null) throw new ApplicationException("Something wrong with available quantity for Za");

        //                    if (Math.Abs(warehouseDocumentPosition.Quantity) <= availableQuantityInStack)
        //                    {
        //                        positionsToGive.Add(new PositionWithQuantityAndShelfs()
        //                        {
        //                            position = warehouseDocumentPosition,
        //                            documentParentIds = new List<int>() { t.WarehouseDocumentId },
        //                            positionParentIds = new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { PositionParentID = t.PositionId, Quantity = -warehouseDocumentPosition.Quantity, LocationId = t.ShelfId } },
        //                            WarehousePositionZones = warehousePositionZones
        //                        });
        //                    }
        //                    else
        //                    {
        //                        warehouseDocumentPositionCandidate.Quantity += (double)t.Quantity;
        //                    }

        //                }
        //            }

        //            if (warehouseDocumentPositionCandidate.Quantity == 0) break;
        //            if (warehouseDocumentPositionCandidate.Quantity < 0) throw new ApplicationException("Internal application error during ordering material");
        //        }

        //        if (warehouseDocumentPositionCandidate.Quantity > 0)
        //        {
        //            throw new ApplicationException("Not enought material for given position - probably data is inconsistent or a bug in application");
        //        }

        //        Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;

        //        QuantityTextBox.Text = "";
        //        MaterialsToGiveRadGrid.SelectedIndexes.Clear();
        //        MaterialsToGiveRadGrid.Rebind();
        //        MaterialsWhichWillBeGivenRadGrid.Rebind();
        //    }
        //}


        protected void AddMaterialToGiveClick(object sender, EventArgs e)
        {
            WarehouseDocumentPosition warehouseDocumentPosition;

            if (IsValid)
            {
                GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
                //var cell = item.Cells[INDEX_POSITIONID_IN_GRID];
                var valueCell = item.Cells[INDEX_UNITPRICE_IN_GRID];
                var orderNumber = item.Cells[INDEX_ORDERNUMBER_IN_GRID].Text;
                var materialIdCell = item.Cells[INDEX_MATERIAL_ID_IN_GRID];
        var valuePerUnit = decimal.Parse(valueCell.Text.Replace(".", ","));
        var quantity = decimal.Parse(QuantityTextBox.Text.Replace(".", ","));
                int materialId = int.Parse(materialIdCell.Text);

                var cellWhSrcId = item.Cells[INDEX_SOURCEWAREHOUSEID_IN_GRID];
                int newSourceWarehouseId = int.Parse(cellWhSrcId.Text);

                var cellWhSrcName = item.Cells[INDEX_SOURCEWAREHOUSEID_NAME_GRID];
                string newSourceWarehouseName = cellWhSrcName.Text;

        using (var warehouseDs = new WarehouseDS())
                {
                    int orderId = 0;
          Mag.Domain.Model.Order order = orderNumber != "" ? warehouseDs.GetAllOrders().Where(p => p.OrderNumber == orderNumber).FirstOrDefault() : null;
                    if (order != null)
                        orderId = order.Id;

                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, valuePerUnit,
                                                                                                    -quantity, 0, orderId);

                    if (positionsToGive.Count(
                        p =>
                        p.position.Material.Id == materialId
                        && p.position.Order.OrderNumber == orderNumber
                        && p.position.UnitPrice == valuePerUnit
                        )
                        == 1)
                    {
                        PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
                                positionsToGive.First(
                        p =>
                        p.position.Material.Id == materialId
                        && p.position.Order.OrderNumber == orderNumber
                        && p.position.UnitPrice == valuePerUnit
                                );

                        positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
                    }
                    else
                    {
                        //List<int> documentParentIds = warehouseDs.GetDocumentParentIdsByPositionId(warehouseDocumentPosition.Id);
                        positionsToGive.Add(new PositionWithQuantityAndShelfs()
                        {
                            position = warehouseDocumentPosition,
                            order = warehouseDocumentPosition.Order,
                            documentParentIds = new List<int>(),
                            positionParentIds = new List<ChildParentPositionQuantity>(),
                            MaterialId = warehouseDocumentPosition.Material.Id,
                            UnitPrice = warehouseDocumentPosition.UnitPrice,
              Quantity = (decimal)warehouseDocumentPosition.Quantity,
                            WarehouseSourceId = newSourceWarehouseId,
                            WarehouseSourceName = newSourceWarehouseName
                        });
                    }
                }

                Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;

                QuantityTextBox.Text = "";

                MaterialsToGiveRadGrid.SelectedIndexes.Clear();
                MaterialsWhichWillBeGivenRadGrid.Rebind();

                MaterialsToGiveRadGrid.Rebind();

                btnSaveDocument.Enabled = true;
            }
        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            int userId;

            if (positionsToGive.Count > 0)
            {
        //if (this.SignatureControl1.ValidateSignatures() != null)
        //{
                    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
                    {
                        WarehouseDocument warehouseDocument;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            int sourceWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];
                            int targetWarehoseId = positionsToGive.First().WarehouseSourceId.Value;

                            lock (typeof(WarehouseDS))
                            {
                                try
                                {
                  //byte[] docPositionsHash = Signature.ComputeDocPositionsHash(positionsToGive);

                                    // clear the list of doc parent ids
                                    foreach (var p in positionsToGive)
                                    {
                                        p.documentParentIds = new List<int>();
                                    }

                                    warehouseDocument = warehouseDs.GiveReturn(sourceWarehoseId, targetWarehoseId,
                                                                               warehouseDs.GetPersonForSystemUser(userId)
                                                                                   .Id,
                                                                               positionsToGive,
                                                                               inputDate.SelectedDate ??
                                                                               DateTime.Now.Date,
                                                             /*this.SignatureControl1.Signatures,
                                                             docPositionsHash, */
                                                             this.CheckBox_ExportToSIMPLE.Checked);

                                    Label_SourceZones.Text = warehouseDs.GetDocumentSourceZonesInfoFormatted(warehouseDocument.Id);

                                    QuantityTextBox.Enabled = false;
                                    btnPrintDocument.Enabled = true;
                                    btnSaveDocument.Enabled = false;
                                    btnAddPosition.Enabled = false;
                                    MaterialToSearchRTB.Enabled = false;
                                    OrderNumberToSearchRTB.Enabled = false;
                                    SearchMaterialButton.Enabled = false;

                                    MaterialsToGiveRadGrid.Enabled = false;
                                    MaterialsWhichWillBeGivenRadGrid.Enabled = false;

                                    DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument,
                                                                               WarehouseDocumentTypeEnum.ZwMinus,
                                                                               warehouseDocument.Id);

                  //this.SignatureControl1.ClearSignatures();

                                    WebTools.AlertMsgAdd("Dokument został zapisany. Materiały zostały zwrócone na <b>" + warehouseDocument.Warehouse1.Name + "</b>");
                                }
                                catch (Exception ex)
                                {
                                    WebTools.AlertMsgAdd(ex, userId);
                                }
                            }
                        }

                        positionsToGive = new List<PositionWithQuantityAndShelfs>();
                        Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;
                        MaterialsToGiveRadGrid.Rebind();
                    }
        //}
                }
            }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                QuantityTextBox.Text = QuantityTextBox.Text.Replace(".", ",");
                args.IsValid = double.Parse(QuantityTextBox.Text) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = MaterialsToGiveRadGrid.SelectedItems.Count == 1 ? true : false;
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (MaterialsToGiveRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
                double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));

                double availableQuantity = double.Parse(item.Cells[INDEX_QUANTITY_IN_GRID].Text);

                if (quantity > availableQuantity)
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        //protected void MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        //{
        //    if (positionsToGive != null)
        //    {
        //        int i = 1;

        //        List<PositionWithShelfAndQuantity> positionWithShelfAndQuantities = new List<PositionWithShelfAndQuantity>();

        //        foreach (var externalIterator in positionsToGive)
        //        {
        //            if (externalIterator.WarehousePositionZones.Count() > 0)
        //            {
        //                double usedQuantity = 0;
        //                foreach (var internalIterator in externalIterator.WarehousePositionZones)
        //                {
        //                    positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
        //                    {
        //                        position = externalIterator.position,
        //                        shelf = internalIterator.Zone,
        //                        quantity = Math.Abs(internalIterator.Quantity)
        //                    });
        //                    usedQuantity += Math.Abs(internalIterator.Quantity);
        //                }
        //                if (usedQuantity < externalIterator.position.Quantity)
        //                {
        //                    positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
        //                    {
        //                        position = externalIterator.position,
        //                        shelf = null,
        //                        quantity = Math.Abs(externalIterator.position.Quantity) - Math.Abs(usedQuantity)
        //                    });
        //                }
        //            }
        //            else
        //            {
        //                positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
        //                {
        //                    position = externalIterator.position,
        //                    shelf = null,
        //                    quantity = Math.Abs(externalIterator.position.Quantity)
        //                });
        //            }
        //        }

        //        var source2 = from p in positionWithShelfAndQuantities
        //                      select
        //                          new
        //                          {
        //                              RowNumber = i++,
        //                              MaterialName = p.position.Material.Name,
        //                              Quantity = Math.Abs(p.position.Quantity),
        //                              UnitName = p.position.Material.Unit.Name,
        //                              LocationName = p.shelf != null ? p.shelf.Name : null
        //                          };

        //        MaterialsWhichWillBeGivenRadGrid.DataSource = source2;
        //    }
        //}

        protected void MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (positionsToGive != null)
            {
                int i = 1;

                var source2 = from p in positionsToGive
                              select
                                  new
                                  {
                                      RowNumber = i++,
                                      MaterialName = p.position.Material.Name,
                                      IndexSIMPLE = p.position.Material.IndexSIMPLE, //.MaterialSIMPLE.FirstOrDefault(ms => ms.Material.Id == p.position.Material.Id) != null ? p.position.Material.MaterialSIMPLE.First().IndexSIMPLE : null,
                                      NameSIMPLE = p.position.Material.Name, //.MaterialSIMPLE.FirstOrDefault(ms => ms.Material.Id == p.position.Material.Id) != null ? p.position.Material.MaterialSIMPLE.First().NameSIMPLE : null,
                                      Quantity = -p.position.Quantity,
                                      UnitName = p.position.Material.Unit.Name,
                                      UnitPrice = p.position.UnitPrice,
                                      ShelfName = p.WarehousePositionZones != null && p.WarehousePositionZones.Count > 0 ? p.WarehousePositionZones.FirstOrDefault().Zone.Name : ""
                                  };
                MaterialsWhichWillBeGivenRadGrid.DataSource = source2;
            }
        }

        protected void OrderNumber_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (positionsToGive.Count() != 0 && MaterialsToGiveRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
                var cell = item.Cells[INDEX_ORDERNUMBER_IN_GRID];
                string newOrderNumber = cell.Text;

                foreach (var s in positionsToGive)
                {
                    if (s.position.Order != null)
                    {
                        if (s.position.Order.OrderNumber != newOrderNumber)
                        {
                            args.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        if (newOrderNumber != "")
                        {
                            args.IsValid = false;
                            return;
                        }
                    }
                }
            }

            args.IsValid = true;
        }


        protected void SourceWarehouse_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (positionsToGive.Count() > 0 && MaterialsToGiveRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
                var cell = item.Cells[INDEX_SOURCEWAREHOUSEID_IN_GRID];
                int newSourceWarehouseId = int.Parse(cell.Text);

                foreach (var s in positionsToGive)
                {
                    if (s.WarehouseSourceId != newSourceWarehouseId)
                    {
                        args.IsValid = false;
                        return;
                    }
                }
            }
            args.IsValid = true;
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            QuantityTextBox.Enabled = true;
            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = true;
            btnAddPosition.Enabled = true;

            MaterialToSearchRTB.Enabled = true;
            OrderNumberToSearchRTB.Enabled = true;
            SearchMaterialButton.Enabled = true;

            MaterialsToGiveRadGrid.Enabled = true;
            MaterialsWhichWillBeGivenRadGrid.Enabled = true;

            positionsToGive = new List<PositionWithQuantityAndShelfs>();
            Session["GiveMaterialReturn_positionsToGive"] = positionsToGive;

      //this.SignatureControl1.ClearSignatures();

            QuantityTextBox.Text = "";
            MaterialsToGiveRadGrid.Rebind();
            MaterialsWhichWillBeGivenRadGrid.Rebind();

            Label_SourceZones.Text = "";
        }
    }
}
