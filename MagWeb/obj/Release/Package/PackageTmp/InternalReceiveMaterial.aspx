﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalReceiveMaterial.aspx.cs" MasterPageFile="~/WarehouseMaster.Master" Inherits="MagWeb.InternalReceiveMaterial" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Przyjęcie wewnętrzne materiału
    </p>
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <table>
            <tr>
                <td valign="top" style="width: 50%; padding-right: 10px">
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="WarehouseDocumentRadGrid" runat="server" OnNeedDataSource="WarehouseDocumentRadGrid_OnNeedDataSource"
													OnItemCommand="WarehouseDocumentRadGrid_SelectRow">
                                        <MasterTableView EnableViewState="true">
                                            <Columns>
                                                <%--<telerik:GridButtonColumn ButtonType="PushButton" HeaderButtonType="PushButton" UniqueName="column" Text="Wybierz" CommandName="SelectRow" />--%>
                                                <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="Skąd" UniqueName="WarehouseSource" DataField="WarehouseSource" />
                                                <telerik:GridBoundColumn HeaderText="Dokąd" UniqueName="WarehouseTarget" DataField="WarehouseTarget" />
                                                <telerik:GridBoundColumn HeaderText="Typ dokumentu" UniqueName="DocumentType" DataField="DocumentType" />
                                                <telerik:GridBoundColumn HeaderText="Autor" UniqueName="Author" DataField="Author" />
                                                <telerik:GridBoundColumn HeaderText="Data utworzenia" UniqueName="CreationDate" DataField="CreationDate" />
                                                <telerik:GridBoundColumn HeaderText="Eksport do SIMPLE" UniqueName="ExportToSIMPLE" DataField="ExportToSIMPLE" />
                                            </Columns>
                                        </MasterTableView>
										<ClientSettings EnablePostBackOnRowClick="true" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">

                                </td>
                            </tr>
                        </table>
                </td>
                <td valign="top" style="width: 50%">
                    <div class="inputPanel">
                            <telerik:RadGrid ID="MaterialsToReceiveRadGrid" runat="server" OnNeedDataSource="MaterialsToReceiveRadGrid_OnNeedDataSource"
											OnItemCommand="MaterialsToReceiveRadGrid_SelectRow">
                                <MasterTableView EnableViewState="true">
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
                                        <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                        <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                        <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="false" />
                                        <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                                        <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                        <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                        <telerik:GridButtonColumn ButtonType="PushButton" HeaderButtonType="PushButton" UniqueName="column" Text="Ustaw lokalizacje" CommandName="SelectRow" />
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>

                            <table style="margin-top: 20px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Lokalizacja *"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox ID="cmbLocalization" runat="server" Enabled="False">
                                            <WebServiceSettings Method="GetTelericShelfsList" Path="WarehouseWebWcf.svc" />
                                        </telerik:RadComboBox>
										<asp:CustomValidator ID="Shelf" runat="server" ErrorMessage="Nie wybrano lokalizacji"
                                            ValidationGroup="SetLocalizationGroup" OnServerValidate="Localization_ServerValidate"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
										<asp:Label ID="Label1" runat="server" Text="Ilość *"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadTextBox ID="Quantity" runat="server" Width="90px" Enabled="False">
                                        </telerik:RadTextBox>
											<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
                                            ControlToValidate="Quantity" ValidationGroup="SetLocalizationGroup"></asp:RequiredFieldValidator>
                                    </td>
								</tr>
                                <tr>
                                    <td colspan="2" style="padding-top: 20px;">
                                        <asp:Button ID="btnAssignLocation" runat="server" OnClick="AssignLocationClick" Text="Przypisz lokalizacje"
                                            Enabled="False" ValidationGroup="SetLocalizationGroup" />
                                        <asp:Button ID="btnCancelAssignLocation" runat="server" OnClick="CancelAssignLocationClick"
                                            Text="Anuluj" Enabled="False" />

										<br />
											<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Nie zdefiniowano lokalizacji dla wszystkich materiałów"
                                            ValidationGroup="ReceiveMaterialGroup" OnServerValidate="Localization_Required_ServerValidate"></asp:CustomValidator>
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Nie można przypisać do lokalizacji więcej niż jest możliwe"
                                            ValidationGroup="SetLocalizationGroup" OnServerValidate="Localization_Quantity_ServerValidate"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
					</div>
				    <div class="inputPanel">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
                                    </td>
                                    <td>
										<telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-top: 20px;">
										<asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick"
                                            Text="Wydaj" Enabled="False" ValidationGroup="ReceiveMaterialGroup"
                                            ToolTip="Zapisz dokument" AlternateText="Zapisz dokument" />
                                        <asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokument" Enabled="False" />
										<asp:Button ID="btnNewDocument" runat="server" Text="Nowe przyjęcie" OnClick="NewDocumentClick" />

										<br />

										<asp:CustomValidator ID="SelectionCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego dokumentu"
                                            ValidationGroup="ReceiveMaterialGroup" OnServerValidate="MaterialCustomValidator_ServerValidate"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
