<%@ Page Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="InternalGiveMaterialReturn.aspx.cs" Inherits="MagWeb.GiveMaterialReturn" Title="Wydanie zwrotu materiału" %>
<%--<%@ Register Src="~/Signature.ascx" TagName="SignatureControl" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
	<!-- custom head section -->
	<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
		<script type="text/javascript">
			function RowSelected(sender, args) {
				document.getElementById("<%= QuantityTextBox.ClientID %>").value = args.getDataKeyValue("Quantity");
			}

			function GetFirstDataItemKeyValues() {
				var firstDataItem = $find("<%= MaterialsToGiveRadGrid.MasterTableView.ClientID %>").get_dataItems()[0];
				var keyValues =
					'Quantity = "' + firstDataItem.getDataKeyValue("Quantity") + '"';
				alert(keyValues);
			}
		</script>
	</telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
	<p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
		Wydanie zwrotu materiału
	</p>
	<asp:Panel ID="Panel_EntirePageContent" runat="server">
		<table>
			<tr>
				<td style="width: 50%; padding-right: 10px; vertical-align: top">
						<table style="width: 100%">
							<tr>
								<td>
									<asp:Label ID="Label4" runat="server" CssClass="labelSearch" Text="Nazwa materiału"></asp:Label>
									<telerik:RadTextBox ID="MaterialToSearchRTB" runat="server">
									</telerik:RadTextBox>
								</td>
								<td>
									<asp:Label ID="Label5" runat="server" CssClass="labelSearch" Text="Indeks SIMPLE"></asp:Label>
									<telerik:RadTextBox ID="IndexSimpleRTB" runat="server">
									</telerik:RadTextBox>
								</td>
								<td>
									<asp:Label ID="Label1" runat="server" CssClass="labelSearch" Text="Przypisanie"></asp:Label>
									<telerik:RadTextBox ID="OrderNumberToSearchRTB" runat="server">
									</telerik:RadTextBox>
								</td>
								<td style="vertical-align: bottom; text-align: right;">
									<asp:Button ID="SearchMaterialButton" runat="server" OnClick="SearchMaterialClick"
										Text="Wyszukaj materiał" Enabled="True" />
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<telerik:RadGrid ID="MaterialsToGiveRadGrid" runat="server" OnNeedDataSource="MaterialsToGiveRadGrid_OnNeedDataSource">
										<GroupingSettings GroupContinuesFormatString=" Pozostałe elementy na następnej stronie."
											GroupContinuedFormatString="Pozostałe elementy z poprzedniej strony. " GroupSplitDisplayFormat="{0} z {1} elementów." />
										<MasterTableView EnableViewState="true" ClientDataKeyNames="Quantity">
											<Columns>
												<telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
												<telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
												<telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="false" />
												<telerik:GridBoundColumn HeaderText="Ilość dostępna" UniqueName="Quantity" DataField="Quantity" />
												<telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
												<telerik:GridBoundColumn HeaderText="Śr.cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice" Display="false" />
												<telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" EmptyDataText="" />
												<telerik:GridBoundColumn HeaderText="IdMateriał" UniqueName="MaterialId" DataField="MaterialId" Display="false" />
												<telerik:GridBoundColumn HeaderText="Magazyn źródłowy (nazwa)" UniqueName="SourceWarehouseName" DataField="SourceWarehouseName" EmptyDataText="" />
												<telerik:GridBoundColumn HeaderText="Magazyn źródłowy (id)" UniqueName="SourceWarehouseID" DataField="SourceWarehouseID" EmptyDataText="" Display="false" />
											</Columns>
										</MasterTableView>
										<ClientSettings>
											<Selecting AllowRowSelect="True" />
											<ClientEvents OnRowSelected="RowSelected" />
										</ClientSettings>
									</telerik:RadGrid>
								</td>
							</tr>
						</table>
				</td>
				<td valign="top">
					<div class="inputPanel">
						<table>
							<tr>
								<td style="width: 120px; vertical-align: top">
									<asp:Label ID="Label3" runat="server" Text="Ilość do zwrotu:"></asp:Label>
								</td>
								<td>
									<asp:TextBox ID="QuantityTextBox" runat="server"></asp:TextBox>
									<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
										ControlToValidate="QuantityTextBox" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
									<asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Zwracana ilość musi być liczbą dadatnią"
										ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"></asp:CustomValidator>
									<asp:CustomValidator ID="QuantityCustomValidator" runat="server" ErrorMessage="Nie można zwrócić wiecej materiału niż jest dostępne"
										ValidationGroup="AddPositionGroup" OnServerValidate="Quantity_ServerValidate"></asp:CustomValidator>
									<asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału do zwrotu"
										ValidationGroup="AddPositionGroup" OnServerValidate="MaterialCustomValidator_ServerValidate"></asp:CustomValidator>
									<asp:CustomValidator ID="OrderNumberChanged" runat="server" ErrorMessage="Nie można wystawić dokumentu Zw- dla różnych przypisań&lt;/br&gt;lub mieszać materiałów z przypisaniem z materiałami bez przypisania"
										ValidationGroup="AddPositionGroup" OnServerValidate="OrderNumber_ServerValidate"></asp:CustomValidator>
									<asp:CustomValidator ID="SourceWarehouseChanged" runat="server" ErrorMessage="Nie można wystawić dokumentu Zw- dla różnych magazynów źródlowych"
										ValidationGroup="AddPositionGroup" OnServerValidate="SourceWarehouse_ServerValidate"></asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:Button ID="btnAddPosition" runat="server" OnClick="AddMaterialToGiveClick" Text="Dodaj pozycje"
										Enabled="True" ValidationGroup="AddPositionGroup" />
								</td>
							</tr>
						</table>
					</div>
					<div class="inputPanel">

							<telerik:RadGrid ID="MaterialsWhichWillBeGivenRadGrid" runat="server" OnNeedDataSource="MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource">
								<MasterTableView EnableViewState="true">
									<Columns>
										<telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
										<telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
										<telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
										<telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
										<telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
										<telerik:GridButtonColumn ButtonType="ImageButton" HeaderButtonType="PushButton"
											UniqueName="column" Text="Usuń" CommandName="DeleteRow" ImageUrl="~/Images/delete.png"
											Display="false">
										</telerik:GridButtonColumn>
									</Columns>
								</MasterTableView>
							</telerik:RadGrid>

							<table style="margin-top: 15px">
								<tr>
									<td>
										<asp:Label ID="Label2" runat="server" Text="Magazyn docelowy:"></asp:Label>
									</td>
									<td>
										<b><asp:Label ID="Label_SourceWarehouse" runat="server" Text="[Magazyn źródłowy]"></asp:Label></b>
										
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
									</td>
									<td>
										<telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd" />
									</td>
								</tr>
								<tr>
									<td></td>
									<td><asp:CheckBox ID="CheckBox_ExportToSIMPLE" runat="server" Checked="false" Text="Eksportuj do SIMPLE"
											Visible="false" /></td>
								</tr>
								<tr>
									<td colspan="2" style="padding-top: 20px;">
										<asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick"
											Text="Wydaj" ForeColor="Yellow" Enabled="False" ValidationGroup="GiveMaterialGroup"
											ToolTip="Zapisz dokument" AlternateText="Zapisz dokument" />

										<asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokument" Enabled="False" />

										<asp:Button ID="btnNewDocument" runat="server" Text="Nowy dokument" OnClick="NewDocumentClick" />
										<br />
										<asp:Label ID="Label_ExportToSIMPLE" runat="server" Text="Eksport do SIMPLE niemożliwy"
											ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
									</td>
								</tr>
							</table>
                                                <%--<uc1:SignatureControl ID="SignatureControl1" runat="server" />--%>
                                            </td>
                                        </tr>
                                    </table>
							<asp:Label ID="Label_SourceZones" runat="server" Text=""></asp:Label>
					</div>
				</td>
			</tr>
		</table>
	</asp:Panel>
</asp:Content>
