﻿<%@ Page Language="C#" MasterPageFile="~/UserManagementMaster.Master" AutoEventWireup="true" CodeBehind="ManageGroups.aspx.cs" Inherits="MagWeb.ManageGroups" Title="Untitled Page"   %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">

    <p>
        <b><asp:Label ID="Label_SystemGroup" runat="server" Text=""></asp:Label></b>
    </p>
    <table style="width: 500px;">
        <tr>
            <td valign="top">
                <p>
                    <b>Wybierz grupę:</b>
                    <telerik:RadGrid ID="GroupsGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                        OnNeedDataSource="GroupsGrid_OnNeedDataSource" GridLines="None" 
                        OnItemCommand="GroupsGrid_ItemCommand">
                        <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="ID" UniqueName="GroupID"
                                    DataField="GroupID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Nazwa uzytkownika" UniqueName="GroupName"
                                    DataField="GroupName" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="False" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </p>
            </td>
            <td valign="top">
                <p>
                    <b>Wybierz firmę:</b>
                    <telerik:RadComboBox Skin="Metro" ID="CompanyRCB" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="CompanyRCB_SelectedIndexChanged">
                    </telerik:RadComboBox>                
                </p>            
            </td>
            <td valign="top">

                <p>
                    <b>Uprawnienia do odczytu magazynów:</b>
                    <telerik:RadGrid ID="WarehouseToReadGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                        OnNeedDataSource="WarehousesToReadGrid_OnNeedDataSource" GridLines="None" 
                        OnItemCommand="WarehousesToReadGrid_ItemCommand">
                        <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="PrivilegeID" UniqueName="PrivilegeID"
                                    DataField="PrivilegeID" Display="false">  
                                </telerik:GridBoundColumn>                              
                                <telerik:GridBoundColumn HeaderText="ID" UniqueName="WarehouseID"
                                    DataField="WarehouseID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Nazwa skrócona magazynu" UniqueName="WarehouseShortName"
                                    DataField="WarehouseShortName" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Nazwa magazynu" UniqueName="WarehouseName"
                                    DataField="WarehouseName" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn HeaderText="Uprawnienie" Visible="true" UniqueName="GrantedToGroup" DataField="GrantedToGroup"></telerik:GridCheckBoxColumn>
                                <telerik:GridButtonColumn CommandName="Select" Text="Zmień"></telerik:GridButtonColumn>                            
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True" />
                        </ClientSettings>
                    </telerik:RadGrid>                
                </p>
                <div>
                    <b>Uprawnienia do wykonywania operacji pomiędzy magazynami:</b>
                    <br />
                    <table style="width: 500px;">          
                        <tr>
                            <td valign="top">
                                Wybierz&nbsp;magazyn&nbsp;źródłowy:
                            </td>
                            <td valign="top">
                                Wybierz&nbsp;magazyn&nbsp;docelowy:
                            </td>
                            <td valign="top">
                                Obsługiwane&nbsp;typy&nbsp;dokumentów:
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <telerik:RadGrid ID="WarehousesGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                                    OnNeedDataSource="WarehousesGrid_OnNeedDataSource" GridLines="None" 
                                    OnItemCommand="WarehousesGrid_ItemCommand">
                                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
                                        <Columns>
                                            <telerik:GridBoundColumn HeaderText="ID" UniqueName="WarehouseID"
                                                DataField="WarehouseID" Display="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Nazwa skrócona magazynu" UniqueName="WarehouseShortName"
                                                DataField="WarehouseShortName" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Nazwa magazynu" UniqueName="WarehouseName"
                                                DataField="WarehouseName" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="False" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                            <td valign="top">
                                <telerik:RadGrid ID="TargetWarehousesGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                                    OnNeedDataSource="TargetWarehousesGrid_OnNeedDataSource" GridLines="None" 
                                    OnItemCommand="TargetWarehousesGrid_ItemCommand">
                                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
                                        <Columns>
                                            <telerik:GridBoundColumn HeaderText="ID" UniqueName="Id"
                                                DataField="Id" Display="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Nazwa skrócona magazynu" UniqueName="ShortName"
                                                DataField="ShortName" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Nazwa magazynu" UniqueName="Name"
                                                DataField="Name" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="False" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                            <td valign="top" colspan="3">
                                <telerik:RadGrid ID="DocumentTypeGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
                                    OnNeedDataSource="DocumentTypeGrid_OnNeedDataSource" GridLines="None" 
                                    OnItemCommand="DocumentTypeGrid_ItemCommand">
                                    <MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
                                        <Columns>
                                            <telerik:GridBoundColumn HeaderText="PrivilegeId" UniqueName="PrivilegeId"
                                                DataField="PrivilegeId" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="TypeID" UniqueName="TypeId"
                                                DataField="TypeId" Display="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Nazwa skrócona typu" UniqueName="ShortType"
                                                DataField="ShortType" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Nazwa typu" UniqueName="Type"
                                                DataField="Type" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Magazyn źródłowy" UniqueName="SourceWarehouse"
                                                DataField="SourceWarehouse" Visible="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Magazyn docelowy" UniqueName="TargetWarehouse"
                                                DataField="TargetWarehouse" Visible="true">
                                            </telerik:GridBoundColumn>                            
                                            <telerik:GridCheckBoxColumn HeaderText="Uprawnienie" Visible="true" UniqueName="GrantedToGroup" DataField="GrantedToGroup"></telerik:GridCheckBoxColumn>
                                            <telerik:GridButtonColumn CommandName="Select" Text="Zmień"></telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="True" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </div>


                <p>
                    <b>Dodawanie nowej grupy:</b>
                    <br />
                    <asp:TextBox ID="TextBox_AddGroup" runat="server"></asp:TextBox>
                    <asp:Button ID="Button_AddGroup"
                        runat="server" Text="Dodaj grupę" onclick="Button_AddGroup_Click" />
                    
                </p>

            </td>
        </tr>
    </table>


</asp:Content>