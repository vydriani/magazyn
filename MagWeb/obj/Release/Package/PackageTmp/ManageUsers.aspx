<%@ Page Language="C#" MasterPageFile="~/UserManagementMaster.Master" AutoEventWireup="true" CodeBehind="ManageUsers.aspx.cs" Inherits="MagWeb.ManageUsers"  %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">

	<p>
		<b><asp:Label ID="Label_SystemUser" runat="server" Text=""></asp:Label></b>
	</p>
	<table style="width: 500px;">
		<tr>
			<td valign="top">
				<div>
					<b>Wybierz użytkownika:</b>
					<div></div>
					<telerik:RadGrid ID="UsersGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
						OnNeedDataSource="UsersGrid_OnNeedDataSource" GridLines="None" 
						OnItemCommand="UsersGrid_ItemCommand">
						<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
							<Columns>
								<telerik:GridBoundColumn HeaderText="ID" UniqueName="UserID"
									DataField="UserID" Display="false">
								</telerik:GridBoundColumn>
								<telerik:GridBoundColumn HeaderText="Nazwa uzytkownika" UniqueName="UserName"
									DataField="UserName" Visible="true">
								</telerik:GridBoundColumn>
								<telerik:GridBoundColumn HeaderText="Imię i nazwisko" UniqueName="FullName"
									DataField="FullName" Visible="true">
								</telerik:GridBoundColumn>
								<telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
							</Columns>
						</MasterTableView>
						<ClientSettings>
							<Selecting AllowRowSelect="False" />
						</ClientSettings>
					</telerik:RadGrid>
				</div>
			</td>
			<td valign="top">
				<p>
					<b>Wybierz firmę:</b>
					<telerik:RadComboBox Skin="Metro" ID="CompanyRCB" runat="server" AutoPostBack="True"
						OnSelectedIndexChanged="CompanyRCB_SelectedIndexChanged">
					</telerik:RadComboBox>                
				</p>            
			</td>
			<td valign="top">

				<p>
					<b>Uprawnienia do odczytu magazynów:</b>
					<telerik:RadGrid ID="WarehouseToReadGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
						OnNeedDataSource="WarehousesToReadGrid_OnNeedDataSource" GridLines="None" 
						OnItemCommand="WarehousesToReadGrid_ItemCommand">
						<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
							<Columns>
								<telerik:GridBoundColumn HeaderText="PrivilegeID" UniqueName="PrivilegeID"
									DataField="PrivilegeID" Display="false">  
								</telerik:GridBoundColumn>                              
								<telerik:GridBoundColumn HeaderText="ID" UniqueName="WarehouseID"
									DataField="WarehouseID" Display="false">
								</telerik:GridBoundColumn>
								<telerik:GridBoundColumn HeaderText="Nazwa skrócona magazynu" UniqueName="WarehouseShortName"
									DataField="WarehouseShortName" Visible="true">
								</telerik:GridBoundColumn>
								<telerik:GridBoundColumn HeaderText="Nazwa magazynu" UniqueName="WarehouseName"
									DataField="WarehouseName" Visible="true">
								</telerik:GridBoundColumn>
								<telerik:GridCheckBoxColumn HeaderText="Uprawnienie" Visible="true" UniqueName="GrantedToUser" DataField="GrantedToUser"></telerik:GridCheckBoxColumn>
								<telerik:GridButtonColumn CommandName="Select" Text="Zmień"></telerik:GridButtonColumn>                            
							</Columns>
						</MasterTableView>
						<ClientSettings>
							<Selecting AllowRowSelect="True" />
						</ClientSettings>
					</telerik:RadGrid>                
				</p>
				<div>
					<b>Uprawnienia do wykonywania operacji pomiędzy magazynami:</b>
					<br />
					<table style="width: 500px;">          
						<tr>
							<td valign="top">
								Wybierz&nbsp;magazyn&nbsp;źródłowy:
							</td>
							<td valign="top">
								Wybierz&nbsp;magazyn&nbsp;docelowy:
							</td>
							<td valign="top">
								Obsługiwane&nbsp;typy&nbsp;dokumentów:
							</td>
						</tr>
						<tr>
							<td valign="top">
								<telerik:RadGrid ID="WarehousesGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
									OnNeedDataSource="WarehousesGrid_OnNeedDataSource" GridLines="None" 
									OnItemCommand="WarehousesGrid_ItemCommand">
									<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="" >
										<Columns>
											<telerik:GridBoundColumn HeaderText="ID" UniqueName="WarehouseID"
												DataField="WarehouseID" Display="false">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Nazwa skrócona magazynu" UniqueName="WarehouseShortName"
												DataField="WarehouseShortName" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Nazwa magazynu" UniqueName="WarehouseName"
												DataField="WarehouseName" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
										</Columns>
									</MasterTableView>
									<ClientSettings>
										<Selecting AllowRowSelect="False" />
									</ClientSettings>
								</telerik:RadGrid>
							</td>
							<td valign="top">
								<telerik:RadGrid ID="TargetWarehousesGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
									OnNeedDataSource="TargetWarehousesGrid_OnNeedDataSource" GridLines="None" 
									OnItemCommand="TargetWarehousesGrid_ItemCommand">
									<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
										<Columns>
											<telerik:GridBoundColumn HeaderText="ID" UniqueName="Id"
												DataField="Id" Display="false">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Nazwa skrócona magazynu" UniqueName="ShortName"
												DataField="ShortName" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Nazwa magazynu" UniqueName="Name"
												DataField="Name" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridButtonColumn CommandName="Select" Text="Wybierz"></telerik:GridButtonColumn>
										</Columns>
									</MasterTableView>
									<ClientSettings>
										<Selecting AllowRowSelect="False" />
									</ClientSettings>
								</telerik:RadGrid>
							</td>
							<td valign="top" colspan="3">
								<telerik:RadGrid ID="DocumentTypeGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
									OnNeedDataSource="DocumentTypeGrid_OnNeedDataSource" GridLines="None" 
									OnItemCommand="DocumentTypeGrid_ItemCommand">
									<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
										<Columns>
											<telerik:GridBoundColumn HeaderText="PrivilegeId" UniqueName="PrivilegeId"
												DataField="PrivilegeId" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="TypeID" UniqueName="TypeId"
												DataField="TypeId" Display="false">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Nazwa skrócona typu" UniqueName="ShortType"
												DataField="ShortType" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Nazwa typu" UniqueName="Type"
												DataField="Type" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Magazyn źródłowy" UniqueName="SourceWarehouse"
												DataField="SourceWarehouse" Visible="true">
											</telerik:GridBoundColumn>
											<telerik:GridBoundColumn HeaderText="Magazyn docelowy" UniqueName="TargetWarehouse"
												DataField="TargetWarehouse" Visible="true">
											</telerik:GridBoundColumn>                            
											<telerik:GridCheckBoxColumn HeaderText="Uprawnienie" Visible="true" UniqueName="GrantedToUser" DataField="GrantedToUser"></telerik:GridCheckBoxColumn>
											<telerik:GridButtonColumn CommandName="Select" Text="Zmień"></telerik:GridButtonColumn>
										</Columns>
									</MasterTableView>
									<ClientSettings>
										<Selecting AllowRowSelect="True" />
									</ClientSettings>
								</telerik:RadGrid>
							</td>
						</tr>
					</table>
				</div>
				
				
				
				
				Przynależność użytkownika do grup:
				<br />
				<telerik:RadGrid ID="UserGroupMembershipsGrid" runat="server" Skin="Metro" AutoGenerateColumns="False"
					OnNeedDataSource="UserGroupMembershipsGrid_OnNeedDataSource" GridLines="None" 
					OnItemCommand="UserGroupMembershipsGrid_ItemCommand">
					<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true" AllowSorting="True" NoMasterRecordsText="">
						<Columns>
							<telerik:GridBoundColumn HeaderText="MembershipID" UniqueName="MembershipID"
								DataField="MembershipID" Visible="true">
							</telerik:GridBoundColumn>                        
							<telerik:GridBoundColumn HeaderText="GroupID" UniqueName="GroupID"
								DataField="GroupID" Visible="true">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Nazwa grupy" UniqueName="GroupName"
								DataField="GroupName" Visible="true">
							</telerik:GridBoundColumn>
							<telerik:GridCheckBoxColumn HeaderText="Przynależność" Visible="true" UniqueName="IsMember" DataField="IsMember"></telerik:GridCheckBoxColumn>
							<telerik:GridButtonColumn CommandName="Select" Text="Zmień"></telerik:GridButtonColumn>                                
						</Columns>
					</MasterTableView>
					<ClientSettings>
						<Selecting AllowRowSelect="False" />
					</ClientSettings>
				</telerik:RadGrid>                
				
				
				
				
				
				
				<p>
					
					<b>Indywidualny limit czasu trwania sesji WWW:</b>
					<br />
					<asp:TextBox ID="TextBox_SessionTimeLimit" runat="server" 
						ValidationGroup="SystemUserSettings"></asp:TextBox> min.
					<asp:Button ID="Button_SetSessionTimeLimit" runat="server" Text="Ustaw" 
						onclick="Button_SetSessionTimeLimit_Click" 
						ValidationGroup="SystemUserSettings" />
					<asp:CustomValidator ID="SessionTimeValidator" runat="server" 
						ControlToValidate="TextBox_SessionTimeLimit" 
						ErrorMessage="Wprowadź wartość całkowitoliczbową z przedziału &lt;1..1440&gt;"
						ValidationGroup="SystemUserSettings" 
						onservervalidate="SessionTimeValidator_ServerValidate"></asp:CustomValidator>
					<br />
					Wprowadzenie pustego ciągu znaków powoduje przywrócenie ustawienia domyślnego
				</p>
			</td>
		</tr>
	</table>


</asp:Content>