﻿ <%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="Document.aspx.cs" Title="Dokumenty magazynowe" Inherits="MagWeb.Document" %>

<asp:Content ContentPlaceHolderID="HeadContentPlaceHolder" ID="headPlace" runat="server">
    <link href="Content/css/bootstrap.css" type="text/css" rel="Stylesheet" />
    <style type="text/css">
        .RadForm a.rfdSkinnedButton, .RadForm a.rfdSkinnedButton * {
            padding:0 !important;
        }
    </style>
</asp:Content>


<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">

    <div class="inputPanel">

        <div class="row">
            <div class="col-md-2" id="div6" runat="server">
                <asp:Label CssClass="labelSearch" runat="server" ID="label4" AssociatedControlID="cmbWarehouseSource">Nr dokumentu:</asp:Label>
                <asp:TextBox ID="textBoxDocNumber" runat="server"></asp:TextBox>

                <asp:Label CssClass="labelSearch" runat="server" ID="lblSupplier" AssociatedControlID="cmbSupplier">Dostawca:</asp:Label>
                <telerik:RadComboBox ID="cmbSupplier" runat="server">
                    <WebServiceSettings Method="GetTelericContractingPartyList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>
            </div>

            <div class="col-md-2" id="divWarehouseSource" runat="server">
                <asp:Label CssClass="labelSearch" runat="server" ID="lblWarehouseSource" AssociatedControlID="cmbWarehouseSource">Skąd:</asp:Label>
                <telerik:RadComboBox ID="cmbWarehouseSource" runat="server">
                    <WebServiceSettings Method="GetTelericWarehouseList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>

                <asp:Label CssClass="labelSearch" runat="server" ID="lblMaterial" AssociatedControlID="cmbMaterial">Materiał:</asp:Label>
                <telerik:RadComboBox ID="cmbMaterial" runat="server">
                    <WebServiceSettings Method="GetTelericMaterialsList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>
            </div>

            <div class="col-md-2" id="divWarehouseTarget" runat="server">
                <asp:Label CssClass="labelSearch" runat="server" ID="lblWarehouseTarget" AssociatedControlID="cmbWarehouseTarget">Dokąd:</asp:Label>
                <telerik:RadComboBox ID="cmbWarehouseTarget" runat="server">
                    <WebServiceSettings Method="GetTelericWarehouseList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>

                <asp:Label CssClass="labelSearch" runat="server" ID="Label2" AssociatedControlID="cmbOrder">Przypisanie:</asp:Label>
                <telerik:RadComboBox ID="cmbOrder" runat="server">
                    <WebServiceSettings Method="GetTelericOrdersList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>
            </div>

            <div class="col-md-2" id="divAuthor" runat="server">
                <asp:Label CssClass="labelSearch" runat="server" ID="lblAuthor" AssociatedControlID="cmbAuthor">Autor:</asp:Label>
                <telerik:RadComboBox ID="cmbAuthor" runat="server">
                    <WebServiceSettings Method="GetTelericPersonList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>

                <asp:Label CssClass="labelSearch" runat="server" ID="Label3" AssociatedControlID="cmbDepartment">Dział:</asp:Label>
                <telerik:RadComboBox ID="cmbDepartment" runat="server">
                    <WebServiceSettings Method="GetDepartmentsList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>
            </div>

            <div class="col-md-2" id="divDocumentTypes" runat="server">
                <asp:Label CssClass="labelSearch" runat="server" ID="lblDocumentTypes" AssociatedControlID="cmbDocumentType">Typ&nbsp;dokumentu:</asp:Label>
                <telerik:RadComboBox ID="cmbDocumentType" runat="server">
                    <WebServiceSettings Method="GetTelericDocumentTypesList" Path="WarehouseWebWcf.svc" />
                </telerik:RadComboBox>

                <asp:Label ID="Label5" CssClass="labelSearch" runat="server" AssociatedControlID="orderNumberTextbox">Nr zamówienia</asp:Label>
                <asp:TextBox ID="orderNumberTextbox" runat="server"></asp:TextBox>
            </div>

            <div class="col-md-2" id="divDate" runat="server">
                <asp:Label CssClass="labelSearch" runat="server" ID="lblDate" AssociatedControlID="inputDate">Data od:</asp:Label>
                <telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                </telerik:RadDatePicker>

                <asp:Label CssClass="labelSearch" runat="server" ID="Label1" AssociatedControlID="inputDate">Data do:</asp:Label>
                <telerik:RadDatePicker ID="inputDateTo" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                </telerik:RadDatePicker>

                <div class="row">
                    <div class="col-md-6">
                        <asp:CheckBox ID="currentWarehouseOnly" runat="server" Text="Globalnie" Checked="False" AutoPostBack="false" />
                    </div>
                    <div class="col-md-6">
                        <asp:CheckBox ID="cbToExport" runat="server" Text="Do zestawień" Checked="False" AutoPostBack="false" />
                        <asp:Button ID="btnSearchDocument" runat="server" OnClick="SearchClickDocument" Text="Wyszukaj" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--<table style="margin-top: 10px; width: 100%;">
        <tr>
            <td colspan="6">
                <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="true"
                    OnItemCommand="WarehouseDocumentRadGrid_SelectRow"
                    OnNeedDataSource="Grid_OnNeedDataSource">
                    <MasterTableView EnableViewState="true">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                            <telerik:GridBoundColumn HeaderText="Skąd" UniqueName="WarehouseSource" DataField="WarehouseSource" />
                            <telerik:GridBoundColumn HeaderText="Dokąd" UniqueName="WarehouseTarget" DataField="WarehouseTarget" />
                            <telerik:GridBoundColumn HeaderText="Typ dokumentu" UniqueName="DocumentType" DataField="DocumentType" />
                            <telerik:GridBoundColumn HeaderText="Autor" UniqueName="Author" DataField="Author" />
                            <telerik:GridBoundColumn HeaderText="Data utworzenia" UniqueName="TimeStamp" DataField="TimeStamp" DataFormatString="{0:yyyy-MM-dd hh:mm}" />
                            <telerik:GridBoundColumn HeaderText="Data dostawy" UniqueName="CreationDate" DataField="CreationDate" DataFormatString="{0:yyyy-MM-dd hh:mm}" />
                            <telerik:GridBoundColumn HeaderText="Typ dokumentu ID" UniqueName="DocumentTypeId" DataField="DocumentTypeId" Display="false" />
                            <telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="SupplierName" DataField="SupplierName" />
                            <telerik:GridBoundColumn HeaderText="Nr faktury zew." UniqueName="ExternalInvoiceNr" DataField="ExternalInvoiceNr" />
                            <telerik:GridBoundColumn HeaderText="Pes" UniqueName="PesId" DataField="PesId" />
                            <telerik:GridBoundColumn HeaderText="Numer" UniqueName="Number" DataField="Number" />
                            <telerik:GridBoundColumn HeaderText="Numer dokumentu Simple" UniqueName="SimpleDocNumber" DataField="SimpleDocNumber" />
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>--%>

    <table style="margin-top: 10px; width: 100%;">
        <tr>
            <td colspan="6">
                <div class="inputPanel" id="documentsPanel" runat="server" visible="false">

                    <telerik:RadGrid ID="warehouseDocumentRadGrid" runat="server" AllowPaging="true"
                        OnItemCommand="WarehouseDocumentRadGrid_SelectRow"
                        OnNeedDataSource="Grid_OnNeedDataSource">
                        <MasterTableView EnableViewState="true">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                                <telerik:GridBoundColumn HeaderText="Skąd" UniqueName="WarehouseSource" DataField="WarehouseSource" />
                                <telerik:GridBoundColumn HeaderText="Dokąd" UniqueName="WarehouseTarget" DataField="WarehouseTarget" />
                                <telerik:GridBoundColumn HeaderText="Typ dokumentu" UniqueName="DocumentType" DataField="DocumentType" />
                                <telerik:GridBoundColumn HeaderText="Autor" UniqueName="Author" DataField="Author" />
                                <telerik:GridBoundColumn HeaderText="Data utworzenia" UniqueName="TimeStamp" DataField="TimeStamp" DataFormatString="{0:yyyy-MM-dd hh:mm}" />
                                <telerik:GridBoundColumn HeaderText="Data dostawy" UniqueName="CreationDate" DataField="CreationDate" DataFormatString="{0:yyyy-MM-dd hh:mm}" />
                                <telerik:GridBoundColumn HeaderText="Typ dokumentu ID" UniqueName="DocumentTypeId" DataField="DocumentTypeId" Display="false" />
                                <telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="SupplierName" DataField="SupplierName" />
                                <telerik:GridBoundColumn HeaderText="Nr faktury zew." UniqueName="ExternalInvoiceNr" DataField="ExternalInvoiceNr" />
                                <telerik:GridBoundColumn HeaderText="Pes" UniqueName="PesId" DataField="PesId" />
                                <telerik:GridBoundColumn HeaderText="Numer" UniqueName="Number" DataField="Number" />
                                <telerik:GridBoundColumn HeaderText="Numer dokumentu Simple" UniqueName="SimpleDocNumber" DataField="SimpleDocNumber" />
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="true" />
                    </telerik:RadGrid>

                </div>
            </td>
        </tr>
    </table>

    <table style="margin-top: 10px; width: 100%;">
        <tr>
            <td colspan="6">
                <div class="inputPanel" id="documentsToExportPanel" runat="server" visible="false">
                    <telerik:RadGrid ID="warehouseDocumentToExportRadGrid" runat="server" AllowPaging="true"
                        OnNeedDataSource="warehouseDocumentToExportRadGrid_NeedDataSource">
                        <MasterTableView EnableViewState="true">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                                <telerik:GridBoundColumn HeaderText="Skąd" UniqueName="WarehouseSource" DataField="WarehouseSource" />
                                <telerik:GridBoundColumn HeaderText="Dokąd" UniqueName="WarehouseTarget" DataField="WarehouseTarget" />
                                <telerik:GridBoundColumn HeaderText="Typ dokumentu" UniqueName="DocumentType" DataField="DocumentType" />
                                <telerik:GridBoundColumn HeaderText="Autor" UniqueName="Author" DataField="Author" />
                                <telerik:GridBoundColumn HeaderText="Data utworzenia" UniqueName="TimeStamp" DataField="TimeStamp" DataFormatString="{0:yyyy-MM-dd hh:mm}" />
                                <telerik:GridBoundColumn HeaderText="Data dostawy" UniqueName="CreationDate" DataField="CreationDate" DataFormatString="{0:yyyy-MM-dd hh:mm}" />
                                <telerik:GridBoundColumn HeaderText="Typ dokumentu ID" UniqueName="DocumentTypeId" DataField="DocumentTypeId" Display="false" />
                                <telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="SupplierName" DataField="SupplierName" />
                                <telerik:GridBoundColumn HeaderText="Nr faktury zew." UniqueName="ExternalInvoiceNr" DataField="ExternalInvoiceNr" />
                                <telerik:GridBoundColumn HeaderText="Pes" UniqueName="PesId" DataField="PesId" />
                                <telerik:GridBoundColumn HeaderText="Numer" UniqueName="Number" DataField="Number" />
                                <telerik:GridBoundColumn HeaderText="Numer dokumentu Simple" UniqueName="SimpleDocNumber" DataField="SimpleDocNumber" />
                                <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="False" />
                                <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" ItemStyle-HorizontalAlign="Right" />
                                <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>

                </div>
            </td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td style="width: 100%;">
                <div class="inputPanel" id="positionPanel" runat="server" visible="false">
                    <input type="search" class="light-table-filter" data-table="order-table" placeholder="filtruj" />

                    <telerik:RadGrid ID="positionsRadGrid" runat="server" OnNeedDataSource="PositionsRadGrid_OnNeedDataSource">
                        <MasterTableView CssClass="order-table table" EnableViewState="true">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" />
                                <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="False" />
                                <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" ItemStyle-HorizontalAlign="Right" />
                                <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                <telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice" ItemStyle-HorizontalAlign="Right" />
                                <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                                <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                <telerik:GridBoundColumn HeaderText="IDPL" UniqueName="ProcessIdpl" DataField="ProcessIdpl" />
                                <telerik:GridTemplateColumn>
                                    <HeaderTemplate>
                                        KJ
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="cbKJ" Checked='<%# Bind("IsKJ") %>' onchange="BindCheck(this);" Visible='<%# Bind("IsKJ") %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn>
                                    <HeaderTemplate>
                                        Wycofaj pozycję
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="cbReturn" onchange="BindReturn(this);"/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>

                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <p class="pull-right">
                        <asp:Button runat="server" ID="btUnlock" ClientIDMode="Static" Text="Odblokuj" OnClick="btUnlock_Click" style="display:none;"/>
                        <asp:Button runat="server" ID="btReturn" ClientIDMode="Static" Text="Wycofaj pozycję" style="display:none;" OnClick="btReturn_Click"/>
                    </p>
                    <p align="left">
                        <asp:Button ID="button_PrintDocument" runat="server" Text="Drukuj dokument" />
                    </p>

                </div>
            </td>
        </tr>
    </table>

    <asp:HiddenField ID="hfUnlock" runat="server" Value="" ClientIDMode="Static" />

    <script type="text/javascript">
        function BindCheck(element) {
            //alert("row" + element.parentNode.parentNode.rowIndex +
            //" - column" + element.parentNode.cellIndex);
            var positionId = element.parentNode.parentNode.cells[0].innerHTML;
            positionId += ',';
            var current = $('#hfUnlock').val();

            var ind = current.indexOf(positionId);

            if (current.indexOf(positionId) != -1) {
                current = current.replace(positionId, "");
            } else {
                current += positionId;
            }
            $('#hfUnlock').val(current);

            var current = $('#hfUnlock').val();
            if (current.length > 0) {
                $('#btUnlock').show();
            } else {
                $('#btUnlock').hide();
            }
        }

        function BindReturn(element) {
            var positionId = element.parentNode.parentNode.cells[0].innerHTML;
            positionId += ',';
            var current = $('#hfReturn').val();

            var ind = current.indexOf(positionId);

            if (current.indexOf(positionId) != -1) {
                current = current.replace(positionId, "");
            } else {
                current += positionId;
            }
            $('#hfReturn').val(current);

            var current = $('#hfReturn').val();
            if (current.length > 0) {
                $('#btReturn').show();
            } else {
                $('#btReturn').hide();
            }
        }

    </script>

        <asp:HiddenField ID="hfReturn" runat="server" Value="" ClientIDMode="Static" />

</asp:Content>
