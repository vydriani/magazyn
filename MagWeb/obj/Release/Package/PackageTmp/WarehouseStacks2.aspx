<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="WarehouseStacks2.aspx.cs" Inherits="MagWeb.WarehouseStacks2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
	<style type="text/css">
		.color1
		{
			background-image: none !important;
			background-color: Red !important;
		}
		.color2
		{
			background-image: none !important;
			background-color: Silver !important;
		}
		.color3
		{
			background-image: none !important;
			background-color: #f1dd56 !important;
		}
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
	<p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
		<asp:Label ID="LabelTitle" runat="server" Text="Stany magazynowe v2"></asp:Label>
		<div>
			<br />
			<asp:Panel ID="PanelInventory" runat="server">
				<p align="center">
					<a href="InventoryList.aspx" target="Mag3InventoryListWindow">
						<img src="/Images/Icons/48x48/shadow/briefcase_document.png" border="0" />
					</a>
				</p>
			</asp:Panel>
			<%--,'Lista Inwentaryzacji', 'menubar=0,location=0,toolbar=0,scrollbars=yes,status=0,top=0,left=0,resizable=yes,alwaysraised=yes,titlebar=no,height=750,width=666,top=300,left=300');"  CausesValidation="False" />--%>
			<div align="center">
				<a href="javascript:myWindow=window.open('Reports/StockReport.aspx?t=1','Raport1','scrollbars=1,width=1150,height=666');myWindow.focus();">Kartoteka produktu (z lokalizacją)</a>
				&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="javascript:myWindow=window.open('Reports/StockReport.aspx?t=2','Raport2','scrollbars=1,width=1150,height=666');myWindow.focus();">Kartoteka produktu (z wartością)</a>
			</div>
		</div>
	</p>
	<asp:Panel ID="Panel_EntirePageContent" runat="server">
		<table>
			<tr>
				<td>
					<div class="inputPanel">
						<table style="width: 100%">
							<tr>
								<td valign="bottom">
									<asp:Label CssClass="labelSearch" runat="server" ID="lblName" AssociatedControlID="materialsRCB">Nazwa lub indeks</asp:Label>
								</td>
								<td valign="bottom">
									<asp:Label CssClass="labelSearch" runat="server" ID="Label2" AssociatedControlID="materialsRCB">Lokalizacja</asp:Label>
								</td>
								<td valign="bottom">
									<asp:Label CssClass="labelSearch" runat="server" ID="lblOrderNumber" AssociatedControlID="cmbOrderNumber">Przypisanie</asp:Label>
								</td>
								<td>
									<asp:Label CssClass="labelSearch" runat="server" ID="Label7" AssociatedControlID="cmbOrderNumber">Tylko materiały z indeksem</asp:Label>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td>
										<telerik:RadComboBox ID="materialsRCB" runat="server">
											<WebServiceSettings Method="GetTelericMaterialsList" Path="WarehouseWebWcf.svc" />
										</telerik:RadComboBox>
								</td>
								<td>
										<telerik:RadComboBox ID="cmbShelfNumber" runat="server" ValidationGroup="AddPositionGroup">
											<WebServiceSettings Method="GetTelericShelfsList" Path="WarehouseWebWcf.svc" />
										</telerik:RadComboBox>
								</td>
								<td>
										<telerik:RadComboBox ID="cmbOrderNumber" runat="server">
											<WebServiceSettings Method="GetTelericOrdersList" Path="WarehouseWebWcf.svc" />
										</telerik:RadComboBox>
								</td>
								<td>
									<asp:CheckBox ID="CheckBox_OnlyIndexed" runat="server" Checked="false" Enabled="False" />
								</td>
								<td valign="bottom">
									<asp:Button ID="btnSearch" runat="server" OnClick="SearchClick" Text="Wyszukaj" />
								</td>
							</tr>
						</table>
					</div>
						<table style="margin-top: 10px;">
							<tr>
								<td valign="top">
									<telerik:RadGrid	ID="StackGrid" 
														runat="server" 
														AllowPaging="true" 
														EnableViewState="true" 
														OnItemDataBound="StackGrid_ItemDataBound" 
														OnDetailTableDataBind="StackGrid_DetailTableDataBind" 
														OnNeedDataSource="Grid_OnNeedDataSource" 
														Visible="false">
										<MasterTableView	DataKeyNames="WarehouseId,MaterialId">
											<Columns>
												<telerik:GridTemplateColumn UniqueName="TemplateColumn" HeaderStyle-Width="50px" HeaderText="L.p.">
													<ItemTemplate>
														<asp:Label ID="numberLabel" runat="server" Width="50px" />
													</ItemTemplate>
												</telerik:GridTemplateColumn>
												<telerik:GridBoundColumn HeaderText="Indeks" HeaderStyle-Width="175px" UniqueName="IndexSIMPLE"
													DataField="IndexSIMPLE" />
												<telerik:GridBoundColumn HeaderText="Nazwa" HeaderStyle-Width="350px" UniqueName="MaterialName"
													DataField="MaterialName" />
												<telerik:GridBoundColumn HeaderText="Ilość całkowita" UniqueName="GlobalQuantity"
													DataField="GlobalQuantity" DataFormatString="{0:0.####}" FooterStyle-HorizontalAlign="Right"
													ItemStyle-HorizontalAlign="Right" Groupable="false" HeaderStyle-Width="115px" />
												<telerik:GridBoundColumn HeaderText="Ilość" UniqueName="LocalQuantity" DataField="LocalQuantity"
													DataFormatString="{0:0.####}" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
													Groupable="false" HeaderStyle-Width="80px" />
												<telerik:GridBoundColumn HeaderText="Jedn. miary" UniqueName="UnitShortName" DataField="UnitShortName"
													Groupable="false" HeaderStyle-Width="90px" />
												<telerik:GridBoundColumn HeaderText="WarehouseId" UniqueName="WarehouseId" DataField="WarehouseId"
													Display="False" />
												<telerik:GridBoundColumn HeaderText="MaterialId" UniqueName="MaterialId" DataField="MaterialId"
													Display="False" />
												<telerik:GridHyperLinkColumn HeaderText="" UniqueName="StockReportLink1" Text="Kartoteka produktu (z lokalizacją)"
													Target="new" DataNavigateUrlFields="IndexSIMPLE,MaterialName,WarehouseId,MaterialId"
													DataNavigateUrlFormatString="Reports/StockReport.aspx?t=1&w={2}&id={3}&m={0} - {1}">
												</telerik:GridHyperLinkColumn>
												<telerik:GridHyperLinkColumn HeaderText="" UniqueName="StockReportLink2" Text="Kartoteka produktu (z wartością)"
													Target="new" DataNavigateUrlFields="IndexSIMPLE,MaterialName,WarehouseId,MaterialId"
													DataNavigateUrlFormatString="Reports/StockReport.aspx?t=2&w={2}&id={3}&m={0} - {1}">
												</telerik:GridHyperLinkColumn>
											</Columns>
											<DetailTables>
												<telerik:GridTableView DataKeyNames="WarehouseId,MaterialId" Name="StockDetails">
													<Columns>
														<telerik:GridBoundColumn HeaderText="OrderId" DataField="OrderId" Display="False" />
														<telerik:GridBoundColumn HeaderText="ShelfId" DataField="ShelfId" Display="False" />
														<telerik:GridTemplateColumn SortExpression="xxx" HeaderText=" " DataField="xxx">
															<HeaderStyle Width="428px" BorderStyle="None" />
															<ItemStyle CssClass="color2" />
															<ItemTemplate>
																<div align="center">
																	<asp:Image ID="ImageMark" runat="server" ImageUrl="/Images/Icons/32x32/shadow/find_next.png" Visible="False" />
																</div>
															</ItemTemplate>
														</telerik:GridTemplateColumn>
														<telerik:GridTemplateColumn>
															<HeaderStyle Width="320px" BorderStyle="None" />
															<ItemStyle CssClass="color2" />
															<ItemTemplate>
																<div align="right">
																	<telerik:RadButton ID="InventoryMaterialButton" runat="server" Text="Inwentaryzuj materiał" Width="200px" OnClick="InventoryMaterialButtonClick" Visible='<%# Eval("InventoryMaterialEnabled") %>'>
																	</telerik:RadButton>
																	<asp:Image ID="ImageMarkInv" runat="server" ImageUrl="/Images/Icons/32x32/shadow/briefcase.png" Visible='<%# Eval("PositionIsInventarized") %>' />
																	<telerik:RadButton ID="FreeMaterialButton" runat="server" Text="Uwolnij materiał" Width="140px" OnClick="FreeMaterialButtonClick" Visible='<%# Eval("FreeMaterialEnabled") %>'>
																	</telerik:RadButton>
																	<telerik:RadButton ID="ChangeLocalizationButton" runat="server" Text="Zmień lokalizację" Width="140px" OnClick="ChangeLocalizationButtonClick" Visible='<%# Eval("ZoneChangeEnabled") %>'>
																	</telerik:RadButton>
																</div>
															</ItemTemplate>
														</telerik:GridTemplateColumn>
														<telerik:GridBoundColumn SortExpression="Quantity" HeaderText="Ilość" HeaderButtonType="TextButton"
															DataField="Quantity" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
															Groupable="false">
															<HeaderStyle Width="125px" />
															<ItemStyle CssClass="color3" />
														</telerik:GridBoundColumn>
														<telerik:GridBoundColumn HeaderText="Jedn. miary" UniqueName="UnitShortName" DataField="UnitShortName"
															Groupable="false">
															<HeaderStyle Width="100px" />
															<ItemStyle CssClass="color3" />
														</telerik:GridBoundColumn>
														<telerik:GridBoundColumn SortExpression="ShelfName" HeaderText="Lokalizacja" HeaderButtonType="TextButton"
															DataField="ShelfName">
															<HeaderStyle Width="75px" />
															<ItemStyle CssClass="color3" />
														</telerik:GridBoundColumn>
														<telerik:GridBoundColumn SortExpression="OrderNumber" HeaderText="Przypisanie" HeaderButtonType="TextButton"
															DataField="OrderNumber">
															<HeaderStyle Width="100px" />
															<ItemStyle CssClass="color3" />
														</telerik:GridBoundColumn>
														<telerik:GridBoundColumn SortExpression="ProcessIdpl" HeaderText="IDPL" HeaderButtonType="TextButton"
															DataField="ProcessIdpl">
															<HeaderStyle Width="100px" />
															<ItemStyle CssClass="color3" />
														</telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn SortExpression="ProcessIddpl" HeaderText="IDDPL" HeaderButtonType="TextButton"
                                                            DataField="ProcessIddpl">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle CssClass="color3" />
                                                        </telerik:GridBoundColumn>
														<telerik:GridBoundColumn HeaderText="DepartmentId" DataField="DepartmentId" Display="False">
														</telerik:GridBoundColumn>
													</Columns>
												</telerik:GridTableView>
											</DetailTables>
										</MasterTableView>
									</telerik:RadGrid>
								</td>
								<td valign="top">
									<asp:Panel ID="MaterialRelocationPanel" runat="server">
										<table>
											<tr>
												<td>
													<asp:Label ID="Label3" runat="server" Text="Nowa lokalizacja:"></asp:Label>
												</td>
											</tr>
											<tr>
												<td>
													<telerik:RadComboBox ID="RelocationTargetZoneRCB" runat="server"  ValidationGroup="RelocateMaterialGroup">
														<WebServiceSettings Method="GetTelericShelfsList" Path="WarehouseWebWcf.svc" />
													</telerik:RadComboBox>
												</td>
											</tr>
											<tr>
												<td>
													<asp:CustomValidator ID="RelocationShelfValidator" runat="server" ErrorMessage="Nie wybrano lokalizacji"
														ValidationGroup="RelocateMaterialGroup" OnServerValidate="RelocationTargetZone_ServerValidate"></asp:CustomValidator>
												</td>
											</tr>
											<tr>
												<td>
													<asp:Label ID="Label1" runat="server" Text="Relokowana ilość:"></asp:Label>
												</td>
											</tr>
											<tr>
												<td>
													<asp:TextBox ID="RelocationQuantityTextBox" runat="server" ValidationGroup="RelocateMaterialGroup"></asp:TextBox>
												</td>
											</tr>
											<tr>
												<td>
													<asp:CustomValidator ID="RelocationQuantityTextBoxValidator" runat="server" ErrorMessage="Błędna ilość"
														ValidationGroup="RelocateMaterialGroup" OnServerValidate="RelocationQuantity_ServerValidate"></asp:CustomValidator>
												</td>
											</tr>
										</table>
										<asp:Button ID="Button_Relocate" runat="server" Text="Zmień lokalizację" OnClick="ButtonChangeLocalizationExecuteClick"
											ValidationGroup="RelocateMaterialGroup" CausesValidation="True" />
										<asp:Button ID="Button_Cancel" runat="server" Text="Anuluj" OnClick="ButtonCancelClick"
											ValidationGroup="RelocateMaterialGroup" />
									</asp:Panel>
									<asp:Panel ID="FreeMaterialPanel" runat="server">
										<table>
											<tr>
												<td>
													<asp:Label ID="Label5" runat="server" Text="Uwalniana ilość:"></asp:Label>
												</td>
											</tr>
											<tr>
												<td>
													<asp:TextBox ID="FreeMaterialQuantityTextBox" runat="server" ValidationGroup="FreeMaterialGroup"></asp:TextBox>
												</td>
											</tr>
											<tr>
												<td>
													<asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Błędna ilość"
														ValidationGroup="FreeMaterialGroup" OnServerValidate="FreeMaterialQuantity_ServerValidate"></asp:CustomValidator>
												</td>
											</tr>
										</table>
										<asp:Button ID="Button1" runat="server" Text="Uwolnij materiał" OnClick="ButtonFreeMaterialExecuteClick"
											ValidationGroup="FreeMaterialGroup" CausesValidation="True" />
										<asp:Button ID="Button2" runat="server" Text="Anuluj" OnClick="ButtonCancelClick"
											ValidationGroup="FreeMaterialGroup" />
									</asp:Panel>
									<asp:Panel ID="InventoryPanel" runat="server">
										<table>
											<tr>
												<td>
													<asp:Label ID="Label4" runat="server" Text="Stwierdzona ilość:"></asp:Label>
												</td>
											</tr>
											<tr>
												<td>
													<asp:TextBox ID="InventoryQuantityTextBox" runat="server" ValidationGroup="InventoryMaterialGroup"></asp:TextBox>
												</td>
											</tr>
											<tr>
												<td>
													<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Błędna ilość"
														ValidationGroup="InventoryMaterialGroup" OnServerValidate="InventoryMaterialQuantity_ServerValidate"></asp:CustomValidator>
												</td>
											</tr>
										</table>
										<asp:Button ID="Button3" runat="server" Text="Dopisz do Listy" OnClick="ButtonInventoryMaterialExecuteClick"
											ValidationGroup="InventoryMaterialGroup" CausesValidation="True" />
										<asp:Button ID="Button4" runat="server" Text="Anuluj" OnClick="ButtonCancelClick"
											ValidationGroup="InventoryMaterialGroup" />
									</asp:Panel>
								</td>
							</tr>
						</table>
						<asp:Button ID="AdjustStacksButton" runat="server" Text="Wyrównaj stan" OnClick="AdjustStacksClick" disabled="true" Visible="False" />
						<asp:Button ID="OpenStockReport" runat="server" Text="Zobacz raport" OnClick="ShowStockReportClick" Visible="False" />
						<asp:HiddenField ID="HiddenField_QuickOp_WarehouseId" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_MaterialId" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_OrderId" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_DepartmentId" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_ProcessIdpl" runat="server" />
                        <asp:HiddenField ID="HiddenField_QuickOp_ProcessIddpl" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_ShelfSrcId" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_MaxQuantity" runat="server" />
						<asp:HiddenField ID="HiddenField_QuickOp_DetailStacksContainerID" runat="server" />
				</td>
			</tr>
		</table>
	</asp:Panel>
</asp:Content>
