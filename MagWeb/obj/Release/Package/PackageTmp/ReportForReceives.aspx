﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="ReportForReceives.aspx.cs" Inherits="MagWeb.ReportForReceives" %>

<asp:Content ID="mainPlace" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">

	<p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
		Wybierz materiał do przyjęcia
	</p>

	<table style="width: 100%; margin-bottom: 20px;">
		<tr>
			<td style="width: 50%">
				<div class="inputPanel">
					<table class="searchTable" style="width: 100%">
						<tr style="height: 35px;">
							<td style="width: 25%">
								<span class="labelSearch">Od</span>
								<telerik:RadDatePicker ID="RadDatePicker1" runat="server" Culture="pl-PL" Width="120">
								</telerik:RadDatePicker>
							</td>
							<td style="width: 25%">
								<span class="labelSearch">Do</span>
								<telerik:RadDatePicker ID="RadDatePicker2" runat="server" Culture="pl-PL" Width="120">
								</telerik:RadDatePicker>
							</td>
							<td style="width: 25%">
								<span class="labelSearch">Tolerancja</span> <span style="font-size: 13pt">+/-</span>
								<asp:DropDownList ID="QuantityTolerance" runat="server">
									<asp:ListItem Value="0">0%</asp:ListItem>
									<asp:ListItem Value="10">10%</asp:ListItem>
									<asp:ListItem Value="20">20%</asp:ListItem>
									<asp:ListItem Value="30">30%</asp:ListItem>
									<asp:ListItem Value="40">40%</asp:ListItem>
									<asp:ListItem Value="50">50%</asp:ListItem>
									<asp:ListItem Value="60">60%</asp:ListItem>
									<asp:ListItem Value="70">70%</asp:ListItem>
									<asp:ListItem Value="80">80%</asp:ListItem>
									<asp:ListItem Value="90">90%</asp:ListItem>
									<asp:ListItem Value="100">100%</asp:ListItem>
								</asp:DropDownList>
							</td>
							<td style="width: 25%"> </td>
						</tr>
						<tr>
							<td>
								<span class="labelSearch">Ilość</span>
								<telerik:RadTextBox ID="QuantityRTB" runat="server" Width="100%">
								</telerik:RadTextBox>
							</td>
							<td>
								<span class="labelSearch">Przypisanie (zlecenie/dział)</span>
								<telerik:RadTextBox ID="OrderNumberRTB" runat="server" Width="100%">
								</telerik:RadTextBox>
							</td>
							<td>
								<span class="labelSearch">Numer zamówienia</span>
								<telerik:RadTextBox ID="OrderNumberPZ_RTB" runat="server" Width="100%">
								</telerik:RadTextBox>
							</td>
							<td> </td>
						</tr>
						<tr style="margin-bottom: 5px;">
							<td>
								<span class="labelSearch">Kontrahent</span>
								<telerik:RadTextBox ID="ContractingPartyName" runat="server" Width="100%">
								</telerik:RadTextBox>	
							</td>
							<td>
								<span class="labelSearch">Nazwa materiału</span>
								<telerik:RadTextBox ID="MaterialToSearchRTB" runat="server" Width="100%">
								</telerik:RadTextBox>	
							</td>
							<td>
								<span class="labelSearch">Status przyjęcia</span>
								<asp:CheckBox ID="CheckBox_Arrived" runat="server" Text="Przyjęte" Checked="true" />
								<asp:CheckBox ID="CheckBox_NotArrived" runat="server" Text="Nieprzyjęte" Checked="true" />
							</td>
							<td style="text-align: right;" valign="bottom">
								<asp:Button ID="Button2" runat="server" OnClick="SearchMaterialClick" Text="Wyszukaj" Enabled="True" />
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td style="text-align: center">
				<asp:Label ID="Label_StatsLabel" runat="server" Text="Statystyka przyjęć:"></asp:Label><asp:Label ID="Label_Stats" runat="server" Text="Label"></asp:Label>
			</td>
		</tr>
	</table>
	


    <telerik:RadGrid ID="MaterialGrid" runat="server" 
        OnNeedDataSource="MaterialGrid_OnNeedDataSource">
        <MasterTableView EnableViewState="true" AllowMultiRowSelection="true" ClientDataKeyNames="Quantity">
            <Columns>
                <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
                <telerik:GridBoundColumn HeaderText="ProcessHeaderId" UniqueName="processHeaderId" DataField="processHeaderId" Display="false" />
                <telerik:GridBoundColumn HeaderText="ProcessIDPL" UniqueName="processIDPL" DataField="processIDPL" Visible="true" />
                <telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="unitPrice" DataField="unitPrice" Visible="true" />
                <telerik:GridBoundColumn HeaderText="ContractingPartyId" UniqueName="ContractingPartyId" DataField="ContractingPartyId" Display="false" />
                <telerik:GridBoundColumn HeaderText="IdSim" UniqueName="IdSim" DataField="IdSim" Display="false" />
                <telerik:GridBoundColumn HeaderText="Kontrahent" UniqueName="ContractingParty" DataField="ContractingParty" />
                <telerik:GridBoundColumn HeaderText="Data dostawy" UniqueName="SupplyDate" DataField="SupplyDate" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" />
                <telerik:GridBoundColumn HeaderText="Numer zamówienia" UniqueName="OrderNumberPZ" DataField="OrderNumberPZ" />
                <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                <telerik:GridBoundColumn HeaderText="ForDepartment" UniqueName="ForDepartment" DataField="ForDepartment" />
                <telerik:GridBoundColumn HeaderText="SourceType" UniqueName="SourceType" DataField="SourceType" />
                <telerik:GridBoundColumn HeaderText="SimpleDocId" UniqueName="SimpleDocId" DataField="SimpleDocId" />
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <Selecting AllowRowSelect="False" />
        </ClientSettings>
    </telerik:RadGrid>
</asp:Content>
