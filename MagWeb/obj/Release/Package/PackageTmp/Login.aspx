<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MagWeb.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>MAG3 - Logowanie</title>
	<style type="text/css">
		body
		{
			background-color: #acd68a;
			color: White;
			font-family: Arial;
			font-size: 12px;
		}
		
		#loginPanel
		{
			background: rgba(255, 255, 255, .5);
			margin: 50px auto; 
			width: 400px; 
			height: 200px; 
			padding: 20px;
			text-align: center;
			border-radius: 15px;
		}

		.loginControl
		{
			margin: 40px auto;
			text-align: center;
			width: auto;
		}
		
		.loginButton
		{
			width: 100px;
			height: 30px; 
			margin-right: 5px;   
		}
		
		.loginTextBox
		{
			font-size: 16px;
			padding: 2px;
			color: darkgreen;
			width: 230px;
			margin-bottom: 10px;
		}
		
		.loginLabel
		{
			font-size: 18px;
			color: darkgreen;
			display: block;
			margin-top: 5px;
			opacity: 0.4;
		}
		 
		#logo
		{
			text-align: center;
			margin: 50px auto; 
			display:block;
		}   

	</style>
</head>
<body>
	<form id="form1" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server" />
	<telerik:RadStyleSheetManager ID="RadStyleSheetManager" runat="server" >
	 <StyleSheets>
		 <telerik:StyleSheetReference Name="Telerik.Web.UI.Skins.Forest.LoginForm.Forest.css" Assembly="Telerik.Web.UI" />
	 </StyleSheets>
	</telerik:RadStyleSheetManager>
	<telerik:RadFormDecorator ID="RadFormDecorator" runat="server" DecoratedControls="All" Skin="Forest" EnableEmbeddedSkins="false" />
	
	<img src="Images/WB_logo2.png" alt="Willson&Brown" id="logo" />
	
	<div id="loginPanel">
		<asp:Login ID="Login1" runat="server" CssClass="loginControl"
			LoginButtonText="Zaloguj"
			PasswordLabelText="Hasło:" DisplayRememberMe="false" TitleText=""
			UserNameLabelText="Użytkownik:" UserNameRequiredErrorMessage="Nazwa użytkownika jest wymagana."
			BorderWidth="0px" 
			FailureText="Nie udało się zalogować. Proszę wprowadzić dane jeszcze raz." DestinationPageUrl="Default.aspx"
			SlidingExpiration="true" LoginUrl="Login.aspx" DefaultUrl="Default.aspx"
			Cookieless="UseDeviceProfile" EnableCrossAppRedirects="false" 
			LoginButtonStyle-CssClass="loginButton"
			TextBoxStyle-CssClass="loginTextBox"
			LabelStyle-CssClass="loginLabel">
			<FailureTextStyle ForeColor="Maroon" />
		</asp:Login>
	</div>
	<br />
	</form>
</body>
</html>
