﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuControl.ascx.cs" Inherits="MagWeb.Controls.MenuControl" %>

<script language="javascript" type="text/javascript">
	$(document).ready(function () {

		$('.topNode').click(function () {

			$('.currentNode').removeClass('currentNode');
			$('.menuItems').removeClass('currentNode');

			$(this).addClass('currentNode');

			var menuId = $(this).attr('data-menu-id');

			$('.menuItems[data-menu-id=' + menuId + ']').addClass('currentNode');

		});


		$('[data-url]').click(function () {

			var url = $(this).attr('data-url');
			document.location = url;
			SubmitMainForm();
		});
	});
</script>

<div id="menu" runat="server" />