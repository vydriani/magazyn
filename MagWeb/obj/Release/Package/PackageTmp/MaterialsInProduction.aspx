﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaterialsInProduction.aspx.cs" Inherits="MagWeb.MaterialsInProduction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadGrid ID="MaterialGrid" runat="server" OnNeedDataSource="MaterialGrid_OnNeedDataSource">
            <MasterTableView EnableViewState="true" ClientDataKeyNames="Quantity">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
                    <telerik:GridBoundColumn HeaderText="ProcessHeaderId" UniqueName="processHeaderId" DataField="processHeaderId" Display="false" />
                    <telerik:GridBoundColumn HeaderText="IDPL" UniqueName="processIDPL" DataField="processIDPL" />
                    <telerik:GridBoundColumn HeaderText="IDDPL" UniqueName="processIDDPL" DataField="processIDDPL" />
                    <telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="unitPrice" DataField="unitPrice" />
                    <telerik:GridBoundColumn HeaderText="ContractingPartyId" UniqueName="ContractingPartyId" DataField="ContractingPartyId" Display="false" />
                    <telerik:GridBoundColumn HeaderText="IdSim" UniqueName="IdSim" DataField="IdSim" Display="false" />
                    <telerik:GridBoundColumn HeaderText="Kontrahent" UniqueName="ContractingParty" DataField="ContractingParty" />
                    <telerik:GridBoundColumn HeaderText="Data dostawy" UniqueName="SupplyDate" DataField="SupplyDate" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" />
                    <telerik:GridBoundColumn HeaderText="Numer zamówienia" UniqueName="OrderNumberPZ" DataField="OrderNumberPZ" />
                    <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                    <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                    <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                    <telerik:GridBoundColumn HeaderText="ForDepartment" UniqueName="ForDepartment" DataField="ForDepartment" />
                    <telerik:GridBoundColumn HeaderText="SourceType" UniqueName="SourceType" DataField="SourceType" />
                    <telerik:GridBoundColumn HeaderText="IndexSim" UniqueName="IndexSim" DataField="IndexSim"  />
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Selecting AllowRowSelect="True" />
                <ClientEvents OnRowSelected="RowSelected" />
            </ClientSettings>
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
