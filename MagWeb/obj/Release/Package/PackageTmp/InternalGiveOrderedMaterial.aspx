﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="InternalGiveOrderedMaterial.aspx.cs" Inherits="MagWeb.InternalGiveOrderedMaterial" %>

<asp:Content ContentPlaceHolderID="HeadContentPlaceHolder" runat="server" >

<script type="text/javascript">
	$(document).ready(function () {

		$('.selectAllCheckboxesOnClient input').change(function (event) {

			var value = $(this).prop('checked');
			$('.selectableCheckboxOnClient input').prop('checked', value);

			var grid = $find("<%=MaterialsToGiveRadGrid.ClientID %>").get_masterTableView();

			for (var i = 0; i < grid.get_dataItems().length; i++) {

				if (value) {
					grid.selectItem(grid.get_dataItems()[i].get_element());
				}
				else {
					grid.deselectItem(grid.get_dataItems()[i].get_element());
				}
			}
		});

		$('.selectableCheckboxOnClient input').change(function (event) {

			var i = $('.selectableCheckboxOnClient input').index($(this));

			var value = $(this).prop('checked');
			var grid = $find("<%=MaterialsToGiveRadGrid.ClientID %>").get_masterTableView();

			if (value) {
				grid.selectItem(grid.get_dataItems()[i].get_element());
			}
			else {
				grid.deselectItem(grid.get_dataItems()[i].get_element());
			}

		});
	});

	function RowSelected(sender, eventArgs) {
		
		var e = eventArgs.get_domEvent();
		var i = eventArgs.get_itemIndexHierarchical();

		$($('.selectableCheckboxOnClient input')[i]).prop('checked', true);
	}

	function RowDeselected(sender, eventArgs) {
		var e = eventArgs.get_domEvent();
		var i = eventArgs.get_itemIndexHierarchical();

		$($('.selectableCheckboxOnClient input')[i]).prop('checked', false);
	}

</script>

</asp:Content>


<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
	<p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
		Wydanie zamówionego materiału
	</p>
	<asp:Panel ID="Panel_EntirePageContent" runat="server">
		<table style="width: 100%">
			<tr>
				<td style="vertical-align: top; width: 25%; padding-right: 10px;">
					<table style="width: 100%">
						<tr>
							<td>
								<asp:TextBox ID="filterTextBox" runat="server" style="height: 18px; width: 250px" />		
							</td>
							<td style="text-align: right">
								<asp:Button ID="Button3" Text="Szukaj" runat="server" OnClick="FilterClick" />
							</td>
						</tr>
					</table>

					<telerik:RadGrid	ID="WaitingOrdersRadGrid" runat="server" OnNeedDataSource="WaitingOrdersRadGrid_OnNeedDataSource"
										OnItemCommand="WaitingOrdersRadGrid_SelectRow">
						<MasterTableView EnableViewState="true">
							<Columns>
								<telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" Visible="True" />
								<telerik:GridBoundColumn HeaderText="OrderId" UniqueName="OrderId" DataField="OrderId" Display="false" />
								<telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" Visible="True" />
								<telerik:GridBoundColumn HeaderText="Data dostarczenia" UniqueName="DateAndHour" DataField="DateAndHour" Visible="True" DataFormatString="{0:yyyy-MM-dd HH:mm}" />
							</Columns>
						</MasterTableView>
						<ClientSettings EnablePostBackOnRowClick="true" />
					</telerik:RadGrid>
				</td>
				<td style="vertical-align: top; width: 75%;">
					<div id="detailsPanel" runat="server">

						<div class="inputPanel">
							<h2 style="text-align: center; margin-bottom: 10px; margin-top: 0px;">
								<asp:Label ID="Label_TargetOrder" runat="server" Text=""></asp:Label>
							</h2>

							<telerik:RadGrid	ID="MaterialsToGiveRadGrid" runat="server" OnNeedDataSource="MaterialsToGiveRadGrid_OnNeedDataSource"  
											OnItemDataBound="MaterialsToGiveRadGrid_ItemDataBound" AllowMultiRowSelection="True">
							<ClientSettings ClientEvents-OnRowDeselected="RowDeselected" ClientEvents-OnRowSelected="RowSelected">
								<Selecting AllowRowSelect="True" EnableDragToSelectRows="false"  />
							</ClientSettings>
							<MasterTableView EnableViewState="true">
								<Columns>
									<telerik:GridTemplateColumn UniqueName="GiveDocumentCheckBoxColumn" HeaderText="" HeaderStyle-CssClass="tableHeaderCheckbox">
										<HeaderTemplate>
											<asp:CheckBox ID="Checkbox1" runat="server" class="selectAllCheckboxesOnClient"  />
										</HeaderTemplate>
										<ItemTemplate>
											<asp:CheckBox ID="GiveDocumentCheckBox" runat="server" Checked="False" class="selectableCheckboxOnClient" />
										</ItemTemplate>
									</telerik:GridTemplateColumn>
									<telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" />
									<telerik:GridBoundColumn HeaderText="Dok.Id" UniqueName="WarehouseDocumentId" DataField="WarehouseDocumentId" Visible="True" />
									<telerik:GridBoundColumn HeaderText="WarehouseDocumentPositionId" UniqueName="WarehouseDocumentPositionId" DataField="WarehouseDocumentPositionId" Display="false" />
									<telerik:GridBoundColumn HeaderText="Dział/magazyn" UniqueName="TargetDepartment" DataField="TargetDepartment" />
									<telerik:GridBoundColumn HeaderText="Maszyna/linia" UniqueName="TargetProductionMachineName" DataField="TargetProductionMachineName" />
									<telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
									<telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
									<telerik:GridBoundColumn HeaderText="Ilość" UniqueName="QuantityExtended" DataField="QuantityExtended" />
									<telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="UnitName" />
									<telerik:GridBoundColumn HeaderText="Data zamówienia" UniqueName="CreationDate" DataField="CreationDate" DataFormatString="{0:yyyy-MM-dd <br /> HH:mm}" />
									<telerik:GridBoundColumn HeaderText="Data dostarczenia" UniqueName="DueTime" DataField="DueTime" DataFormatString="{0:yyyy-MM-dd <br /> HH:mm}" />
									<telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
									<telerik:GridTemplateColumn UniqueName="Zmiana" HeaderText="">
										<ItemTemplate>
											<telerik:RadButton ID="ChangeMaterialButton" runat="server" Text="# ZMIANA MATERIAŁU #"
												Width="200px" OnClick="ButtonChangeMaterialButtonClick" Visible='<%# Eval("ChangeMaterialEnabled") %>'>
												<%--Visible='<%# Eval("ChangeMaterialEnabled") %>'--%>
											</telerik:RadButton>
											<telerik:RadButton ID="ChangeQuantityButton" runat="server" Text="Zmień ilość"
												Width="200px" OnClick="ButtonChangeQuantityButtonClick" Visible='<%# Eval("IsUnlocked") %>'>
												<%--Visible='<%# Eval("ChangeMaterialEnabled") %>'--%>
											</telerik:RadButton>
                                            <telerik:RadButton ID="CancelMaterialButton" runat="server" Text="Anuluj"
                                                Width="80px" OnClick="ButtonCancelMaterialClick" Visible="false">
                                            </telerik:RadButton>
										</ItemTemplate>
									</telerik:GridTemplateColumn>
									<telerik:GridTemplateColumn SortExpression="xxx" HeaderText=" " DataField="xxx">
										<%--<HeaderStyle Width="428px" BorderStyle="None" />--%>
										<ItemStyle CssClass="color2" />
										<ItemTemplate>
											<div align="center">
												<asp:Image ID="ImageMark" runat="server" ImageUrl="/Images/Icons/32x32/shadow/find_next.png" Visible="False" />
												<asp:Image ID="ImageMarkSaved" runat="server" ImageUrl="/Images/Icons/32x32/shadow/cube_yellow_preferences.png" Visible="False" />
												<asp:Image ID="ImageMarkLocked" runat="server" ImageUrl="/Images/Icons/32x32/shadow/lock_time.png" Visible='<%# Eval("IsLocked") %>' />
											</div>
										</ItemTemplate>
									</telerik:GridTemplateColumn>
								</Columns>
							</MasterTableView>
						</telerik:RadGrid>

							<asp:Panel ID="MaterialChangePanel" runat="server">
								<h3>
									Wybierz materiał po zmianie:</h3>
									<telerik:RadGrid ID="MaterialsToChangeRadGrid" runat="server" OnNeedDataSource="MaterialsToChangeGiveRadGrid_OnNeedDataSource"
										AutoGenerateColumns="False" GridLines="None" Skin="Metro" AllowPaging="false"
										PageSize="50">
										<MasterTableView EnableViewState="true">
											<Columns>
												<telerik:GridBoundColumn HeaderText="" UniqueName="MaterialId" DataField="MaterialId" Display="false" />
												<telerik:GridBoundColumn HeaderText="Nazwa" UniqueName="MaterialName" DataField="MaterialName" />
												<telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
												<telerik:GridBoundColumn HeaderText="Ilość dostępna" UniqueName="AvailableQuantity" DataField="AvailableQuantity" Visible="True" />
											</Columns>
										</MasterTableView>
										<ClientSettings>
											<Selecting AllowRowSelect="True" />
											<%-- <ClientEvents OnRowSelected="RowSelected" />--%>
										</ClientSettings>
									</telerik:RadGrid>
									<asp:Button ID="Button_ChangeMaterialExecute" runat="server" Text="Zmień materiał"
										OnClick="ButtonChangeMaterialExecuteClick" ValidationGroup="ChangeMaterialGroup"
										CausesValidation="True" />
									<asp:Button ID="Button_Cancel" runat="server" Text="Anuluj" OnClick="ButtonCancelClick"
										ValidationGroup="ChangeMaterialGroup" />
							</asp:Panel>
						</div>

						<asp:Panel ID="QuantityChangePanel" runat="server" CssClass="inputPanel">
							<h3>Wprowadź nową ilość do wydania:</h3>

							<asp:TextBox ID="NewQuantityTextBox" runat="server" Font-Size="Large" BorderStyle="Double"></asp:TextBox>
							<asp:CustomValidator ID="NewQuantityCustomValidator" runat="server" ErrorMessage="Wprowadź poprawną ilość"
										ValidationGroup="ChangeQuantityGroup" OnServerValidate="NewQuantityCustomValidator_ServerValidate"></asp:CustomValidator>
							<br /><br />
							<b><font color="red">Uwaga #1: Po wprowadzeniu ilości większej niż w zamówieniu pozycja zostanie zablokowana do czasu zaakceptowania przez kierownika magazynu.</font></b>
							<br /><br />
							<b><font color="blue">Uwaga #2: Po wprowadzeniu ilości mniejszej niż w zamówieniu wydanie róznicy będzie możliwe po utworzeniu nowego zamówienia przez kierownika produkcji.</font></b>
							<br /><br />
							<asp:Button ID="Button1" runat="server" Text="Zmień ilość"
								OnClick="ButtonChangeQuantityExecuteClick" ValidationGroup="ChangeQuantityGroup"
								CausesValidation="True" />
							<asp:Button ID="Button2" runat="server" Text="Anuluj" OnClick="ButtonCancelClick"
								ValidationGroup="ChangeQuantityGroup" />
						</asp:Panel>
							

						<div class="inputPanel" style="font-size: 9pt; color: gray">
							<table cellpadding="5px">
								<tr id="Panel_SimpleDocTypeRw" runat="server">
									<td>Typ dokumentu SIMPLE</td>
									<td>
										<telerik:RadComboBox Skin="Metro" ID="RadComboBox_DocSubtypeSIMPLE" runat="server" EmptyMessage="Wybierz typ dokumentu SIMPLE" EnableVirtualScrolling="True" EnableItemCaching="True" LoadingMessage="Pobieranie danych" Width="350" ValidationGroup="GiveMaterialGroup" />
									</td>
								</tr>
								<tr>
									<td><asp:Label ID="labelDate" runat="server" Text="Data dokumentu" /></td>
									<td>
										<telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd" />
										&nbsp;&nbsp;&nbsp;
										<asp:CustomValidator ID="SelectionCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego dokumentu"
											ValidationGroup="GiveMaterialGroup" OnServerValidate="MaterialCustomValidator_ServerValidate" />
									</td>
								</tr>
								<tr>
									<td>Export do SIMPLE</td>
									<td>
										<asp:CheckBox ID="CheckBox_ExportToSIMPLE" runat="server" Checked="true" />
										<asp:Label ID="Label_ExportToSIMPLE" runat="server" Text="Eksport do SIMPLE niemożliwy" ForeColor="Red" Font-Bold="true" Visible="false" />
									</td>
								</tr>
								<tr>
									<td>
									</td>
									<td style="padding-top: 30px;">
										<%--
											<asp:Button ID="Button1" runat="server" OnClick="SaveDocumentClick" Text="Zapisz dokument"
														Enabled="True" ValidationGroup="GiveMaterialGroup" />
											<asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick" Text="Wydaj"
														Enabled="True" ValidationGroup="GiveMaterialGroup" />--%>

										<asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick" Text="Wydaj" Enabled="False" ValidationGroup="ReceiveMaterialGroup" />
										<asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokumenty" Enabled="False" />
										<asp:Button ID="btnNewDocument" runat="server" Text="Nowe wydanie" OnClick="NewDocumentClick" />
									</td>
								</tr>
							</table>
						</div>
					</div>
					<asp:HiddenField ID="HiddenField_QuickOp_PositionId" runat="server" />
					<asp:HiddenField ID="HiddenField_QuickOp_ContainerID" runat="server" />
					<asp:Label ID="Label_SourceZones" runat="server" Text=""></asp:Label>
				</td>
			</tr>
		</table>
	</asp:Panel>
</asp:Content>
