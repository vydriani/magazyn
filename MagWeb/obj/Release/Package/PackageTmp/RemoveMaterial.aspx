﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master"	CodeBehind="RemoveMaterial.aspx.cs" Title="Likwidacja materiałów na magazynie"  Inherits="MagWeb.RemoveMaterial" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Likwidacja materiału
    </p>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <table>
            <tr>
                <td>
                    <div>
                        <fieldset>
                            <legend>Materiały na stanie magazynu</legend>
                            <table>
                                <tr>
                                    <td>
                                        <div class="floatLeft">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblName" AssociatedControlID="txtName">Nazwa:</asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="txtName" runat="server">
                                                <WebServiceSettings Method="GetTelericMaterialsList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="floatLeft">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblIndex" AssociatedControlID="txtIndex">Indeks:</asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="txtIndex" runat="server">
                                                <WebServiceSettings Method="GetTelericIndexesSIMPLEList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="floatLeft" id="divOrderNumber" runat="server">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblOrderNumber" AssociatedControlID="txtOrderNumber">Przypisanie:</asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="txtOrderNumber" runat="server">
                                                <WebServiceSettings Method="GetTelericOrdersList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="floatLeft" id="divShelf" runat="server">
                                            <asp:Label CssClass="labelSearch" runat="server" ID="lblShelf" AssociatedControlID="cmbShelf">Lokalizacja:</asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="cmbShelf" runat="server">
                                                <WebServiceSettings Method="GetTelericShelfsList" Path="WarehouseWebWcf.svc" />
                                            </telerik:RadComboBox>
                                        </div>
                                    </td>
                                    <td valign="bottom">
                                        <asp:Button ID="btnSearch" runat="server" OnClick="SearchClick" Text="Wyszukaj" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <telerik:RadGrid ID="StackGrid" runat="server" OnNeedDataSource="CurrentStackGrid_OnNeededDataSource"
                                            EnableViewState="false" PageSize="10" AllowMultiRowSelection="true">
                                            <ClientSettings Selecting-AllowRowSelect="true" />
                                            <MasterTableView EnableViewState="true">
                                                <Columns>
                                                    <telerik:GridBoundColumn HeaderText="MaterialId" UniqueName="MaterialId" DataField="MaterialId" Display="false" />
                                                    <telerik:GridBoundColumn HeaderText="PositionId" UniqueName="PositionId" DataField="PositionId" Display="false" />
                                                    <telerik:GridBoundColumn HeaderText="ShelfId" UniqueName="ShelfId" DataField="ShelfId" Display="false" />
                                                    <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                                    <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                    <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" />
                                                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                                    <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                                                    <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="labelQuantity" runat="server" Text="Ilość do likwidacji:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadTextBox ID="txtQuantity" runat="server">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
                                    ControlToValidate="txtQuantity" ValidationGroup="RemoveMaterialGroup">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Likwidowana ilość musi być liczbą dadatnią"
                                    ValidationGroup="RemoveMaterialGroup" OnServerValidate="DataTypeValidator_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="QuantityCustomValidator" runat="server" ErrorMessage="Nie można zlikwidować więcej materiału niż jest dostępne"
                                    ValidationGroup="RemoveMaterialGroup" OnServerValidate="Quantity_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału do likwidacji"
                                    ValidationGroup="RemoveMaterialGroup" OnServerValidate="MaterialCustomValidator_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="buttonRemoveMaterial" runat="server" Text="Likwiduj materiał" ValidationGroup="RemoveMaterialGroup"
                                    OnClick="ButtonRemoveMaterial_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
