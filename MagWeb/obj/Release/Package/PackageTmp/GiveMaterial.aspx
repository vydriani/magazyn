<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GiveMaterial.aspx.cs" MasterPageFile="~/WarehouseMaster.Master" Inherits="MagWeb.GiveMaterial" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    
    <!-- custom head section -->
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowSelected(sender, args) {
                document.getElementById("<%= QuantityTextBox.ClientID %>").value = args.getDataKeyValue("Quantity");
            }

            function GetFirstDataItemKeyValues() {
                var firstDataItem = $find("<%= MaterialsToGiveRadGrid.MasterTableView.ClientID %>").get_dataItems()[0];
                var keyValues =
                    'Quantity = "' + firstDataItem.getDataKeyValue("Quantity") + '"';
                alert(keyValues);
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Wydanie zewnętrzne materiału
    </p>
    <table width="100%">
        <tr>
            <td valign="top" width="50%">
                    <table width="100%" style="padding-right: 15px">
                        <tr>
                            <td>
                <asp:Label ID="Label4" runat="server" Text="Nazwa materiału" CssClass="labelSearch"></asp:Label>
                                <telerik:RadTextBox ID="MaterialToSearchRTB" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                <asp:Label ID="Label5" runat="server" Text="Indeks SIMPLE" CssClass="labelSearch"></asp:Label>
                                <telerik:RadTextBox ID="IndexSimpleRTB" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                <asp:Label ID="Label1" runat="server" Text="Przypisanie" CssClass="labelSearch"></asp:Label>
                                <telerik:RadTextBox ID="OrderNumberToSearchRTB" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td valign="bottom" align="right">
                                <asp:Button ID="SearchMaterialButton" runat="server" OnClick="SearchMaterialClick"
                                    Text="Wyszukaj materiał" Enabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <telerik:RadGrid ID="MaterialsToGiveRadGrid" runat="server"	AllowPaging="true" OnNeedDataSource="MaterialsToGiveRadGrid_OnNeedDataSource">
                                    <GroupingSettings GroupContinuesFormatString=" Pozostałe elementy na następnej stronie."
                                        GroupContinuedFormatString="Pozostałe elementy z poprzedniej strony. " GroupSplitDisplayFormat="{0} z {1} elementów." />
                                    <MasterTableView EnableViewState="true" ClientDataKeyNames="Quantity">
                                        <Columns>
                                            <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                            <telerik:GridBoundColumn HeaderText="Indeks SIMPLE" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                            <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="false" />
                                            <telerik:GridBoundColumn HeaderText="Ilość dostępna" UniqueName="Quantity" DataField="Quantity" />
                                            <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                            <telerik:GridBoundColumn HeaderText="Śr.cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice" Display="false" />
                                            <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" /> 
                                            <telerik:GridBoundColumn HeaderText="IdMateriał" UniqueName="MaterialId" DataField="MaterialId" Display="false" />
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="True" />
                                        <ClientEvents OnRowSelected="RowSelected" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>				    
                    </table>
            </td>
            <td valign="top">
        <div class="inputPanel">
          <table width="100%">
            <tr>
              <td style="width: 150px; vertical-align: top">
                <asp:Label ID="Label3" runat="server" Text="Ilość do wydania:"></asp:Label>
              </td>
              <td>
                <asp:TextBox ID="QuantityTextBox" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
                  ControlToValidate="QuantityTextBox" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Wydawana ilość musi być liczbą dadatnią"
                  ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"></asp:CustomValidator>
                  <asp:CustomValidator ID="QuantityValidator" runat="server" ErrorMessage="Nie można wydać więcej niż jest dostępne"
                  ValidationGroup="AddPositionGroup" OnServerValidate="Quantity_ServerValidate"></asp:CustomValidator>
              </td>
            </tr>
            <tr>
              <td>
                <asp:Button ID="btnAddPosition" runat="server" OnClick="AddMaterialToGiveClick" Text="Dodaj pozycje"
                  Enabled="True" ValidationGroup="AddPositionGroup" />	
              </td>
            </tr>
          </table>					
        </div>
        <div class="inputPanel">
                        <telerik:RadGrid ID="MaterialsWhichWillBeGivenRadGrid" runat="server" OnNeedDataSource="MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource" OnItemCommand="MaterialsToReceiveRadGrid_DeleteRow">
                            <MasterTableView EnableViewState="true">
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
                                    <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
                                    <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                    <telerik:GridBoundColumn HeaderText="Nazwa SIMPLE" UniqueName="NameSIMPLE" DataField="NameSIMPLE" Display="false" />
                                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                                    <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                    <telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice" Display="false" />
                                    <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                    <telerik:GridButtonColumn ButtonType="ImageButton" HeaderButtonType="PushButton" UniqueName="column" Text="Usuń" CommandName="DeleteRow" ImageUrl="~/Images/delete.png" />
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn UniqueName="EditCommandColumn1">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                        </telerik:RadGrid>
            <table style="margin-top: 20px;">
              <tr>
                <td>
                  Typ dokumentu SIMPLE
                </td>
                <td>
                  <telerik:RadComboBox Skin="Metro" ID="RadComboBox_DocSubtypeSIMPLE" runat="server" ValidationGroup="GiveMaterialGroup" />
                  <asp:CustomValidator ID="CustomValidator_DocSubtypeSIMPLE" runat="server" ErrorMessage="Wybierz typ dokumentu SIMPLE"
                    ValidationGroup="GiveMaterialGroup" OnServerValidate="DocSubtypeSIMPLE_ServerValidate" />
                </td>
              </tr>
              <tr>
                <td>
                  <asp:Label ID="Label2" runat="server" Text="Odbiorca:"></asp:Label>
                </td>
                <td>
                  <telerik:RadComboBox ID="CompanyClientRCB" runat="server" EmptyMessage="Wybierz kontrahenta SIMPLE" Width="250" ValidationGroup="GiveMaterialGroup">
                    <WebServiceSettings Method="" Path="WarehouseWebWcf.svc" />
                  </telerik:RadComboBox>
                                  <asp:CustomValidator ID="CustomValidator_ContractingPartySimple" runat="server" ErrorMessage="Nie wybrano kontrahenta"
                    ValidationGroup="GiveMaterialGroup" OnServerValidate="CustomValidator_ContractingPartySimple_ServerValidate"></asp:CustomValidator>

                </td>
              </tr>
              <tr>
                <td>
                  <asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
                </td>
                <td>
                  <telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                  </telerik:RadDatePicker>
                </td>
              </tr>
              <tr>
                <td>
                  Eksportuj do SIMPLE
                </td>
                <td>
                  <asp:CheckBox ID="CheckBox_ExportToSIMPLE" runat="server" Checked="true" Enabled="true" EnableTheming="true" /> 
                  <asp:Label ID="Label_ExportToSIMPLE" runat="server" Text="Eksport do SIMPLE niemożliwy" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                </td>
              </tr>
              <tr>
                <td colspan="2" style="padding-top: 20px;">
                  <asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick" Text="Wydaj" Enabled="False" ValidationGroup="GiveMaterialGroup"
                    ToolTip="Zapisz dokument" AlternateText="Zapisz dokument" />

                  <asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokument" Enabled="False" />
                                
                  <asp:Button ID="btnNewDocument" runat="server" Text="Nowy dokument" OnClick="NewDocumentClick" />
                </td>
              </tr>
                    </table>
          
                    <asp:Label ID="Label_SourceZones" runat="server" Text=""></asp:Label>
        </div>
      </td>
    </tr>
  </table>
</asp:Content>
