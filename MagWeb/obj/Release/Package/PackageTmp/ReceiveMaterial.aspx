<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="ReceiveMaterial.aspx.cs" Inherits="MagWeb.ReceiveMaterial" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <!-- custom head section -->
	<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
		<script type="text/javascript">
		    var shelfIds = '';

		    function RowSelected(sender, args) {
		        var QuantityAdded = 0;
		        var grid = sender;
		        var IndexSIMPLE;
			    var MasterTable = grid.get_masterTableView();
			    var selectedRows = MasterTable.get_selectedItems();
			    for (var i = 0; i < selectedRows.length; i++) {
			        var row = selectedRows[i];
			        var cell = MasterTable.getCellByColumnUniqueName(row, "QuantityAdded");
			        QuantityAdded += cell.innerHTML;
			        var simpleCell = MasterTable.getCellByColumnUniqueName(row, "IndexSIMPLE");
			        IndexSIMPLE = simpleCell.innerHTML;
			    }
			    var Quantity = args.getDataKeyValue("Quantity");
			    Quantity = Quantity.replace(",", ".");
			    QuantityAdded = QuantityAdded.replace(",", ".");
			    document.getElementById("<%= QuantityTextBox.ClientID %>").value = (parseFloat(Quantity).toFixed(4) - parseFloat(QuantityAdded).toFixed(4)).toString().replace(".", ",");

			    var companyId = '<%= Session["CurrentCompanyId"].ToString() %>';
			    var warehouseId = '<%= Session["CurrentWarehouseId"].ToString() %>';

                PageMethods.GetShelfIds(companyId, warehouseId, IndexSIMPLE, OnGetMessageSuccess, OnGetMessageFailure);
			}

			function OnGetMessageSuccess(result, userContext, methodName) {
			    shelfIds = result;
			    var shelfCombo = $find("<%= cmbShelf.ClientID %>");
			    shelfCombo.requestItems("", true);
			}

			function OnGetMessageFailure(error, userContext, methodName) {
			    shelfIds = '';
			    var shelfCombo = $find("<%= cmbShelf.ClientID %>");
                shelfCombo.requestItems("", true);
			}

			function OnClientItemsRequesting(sender, eventArgs) {
			    var checkBox = $('#<%= CheckBox_Shelf.ClientID %>').is(':checked');
			    var context = eventArgs.get_context();
			    
			    if (!checkBox) {
			        if (sender.get_text() != '' && sender.get_text() != "Wybierz")
			            context["Text"] = sender.get_text();
			        else
			            context["Text"] = 'allShelfs';
			        shelfIds = '';
			    }

			    sender.clearItems();

			    if (checkBox) {
			        sender.clearSelection();
			    }

			    if (shelfIds != '') {
			        context["Text"] = shelfIds;
			        
			        shelfIds = '';
			    }
            }
		</script>
	</telerik:RadCodeBlock>
</asp:Content>
<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
		Przyjęcie zewnętrzne materiału
	</p>
		<table>
			<tr>
				<td>

					<table>
						<tr>
							<td>
								<div class="inputPanel">
									<table class="searchTable" style="width: 100%">
										<tr style="height: 35px;">
											<td style="width: 25%">
												<span class="labelSearch">Od</span>
												<telerik:RadDatePicker ID="RadDatePicker1" runat="server" Culture="pl-PL" Width="120">
												</telerik:RadDatePicker>
											</td>
											<td style="width: 25%">
												<span class="labelSearch">Do</span>
												<telerik:RadDatePicker ID="RadDatePicker2" runat="server" Culture="pl-PL" Width="120">
												</telerik:RadDatePicker>
											</td>
											<td style="width: 25%">
												<span class="labelSearch">Tolerancja</span> <span style="font-size: 13pt">+/-</span>
												<asp:DropDownList ID="QuantityTolerance" runat="server">
													<asp:ListItem Value="0">0%</asp:ListItem>
													<asp:ListItem Value="10">10%</asp:ListItem>
													<asp:ListItem Value="20">20%</asp:ListItem>
													<asp:ListItem Value="30">30%</asp:ListItem>
													<asp:ListItem Value="40">40%</asp:ListItem>
													<asp:ListItem Value="50">50%</asp:ListItem>
													<asp:ListItem Value="60">60%</asp:ListItem>
													<asp:ListItem Value="70">70%</asp:ListItem>
													<asp:ListItem Value="80">80%</asp:ListItem>
													<asp:ListItem Value="90">90%</asp:ListItem>
													<asp:ListItem Value="100">100%</asp:ListItem>
												</asp:DropDownList>
											</td>
											<td style="width: 25%">
											</td>
										</tr>
										<tr>
											<td>
												<span class="labelSearch">Ilość</span>
												<telerik:RadTextBox ID="QuantityRTB" runat="server" Width="100%">
												</telerik:RadTextBox>
											</td>
											<td>
												<span class="labelSearch">Przypisanie (zlecenie/dział)</span>
												<telerik:RadTextBox ID="OrderNumberRTB" runat="server" Width="100%">
												</telerik:RadTextBox>
											</td>
											<td>
												<span class="labelSearch">Numer zamówienia</span>
												<telerik:RadTextBox ID="OrderNumberPZ_RTB" runat="server" Width="100%">
												</telerik:RadTextBox>
											</td>
											<td>
											</td>
										</tr>
										<tr style="margin-bottom: 5px;">
											<td>
												<span class="labelSearch">Kontrahent</span>
												<telerik:RadTextBox ID="ContractingPartyName" runat="server" Width="100%">
												</telerik:RadTextBox>	
											</td>
											<td>
												<span class="labelSearch">Nazwa materiału</span>
												<telerik:RadTextBox ID="MaterialToSearchRTB" runat="server" Width="100%">
												</telerik:RadTextBox>	
											</td>
											<td>
												<span class="labelSearch">Dział docelowy SIMPLE</span>
												<telerik:RadComboBox ID="RadComboBox_TargetDepartmentSIMPLE" runat="server" 
																		ValidationGroup="GiveMaterialGroup" Width="100%">
												</telerik:RadComboBox>
											</td>
											<td style="text-align: right;" valign="bottom">
												<asp:Button ID="Button1" runat="server" OnClick="SearchMaterialClick" Text="Wyszukaj"
													Enabled="True" />
											</td>
										</tr>
									</table>
								</div>
								<br />
                                <div style="height:400px;overflow:auto;">
								<telerik:RadGrid ID="MaterialGrid" runat="server">
									<MasterTableView EnableViewState="true" ClientDataKeyNames="Quantity,QuantityOrdered,QuantityAdded">
										<Columns>
											<telerik:GridBoundColumn HeaderText="ProcessHeaderId" UniqueName="processHeaderId" DataField="processHeaderId" Display="false" />
											<telerik:GridBoundColumn HeaderText="IDPL" UniqueName="processIDPL" DataField="processIDPL" Display="false" />
											<telerik:GridBoundColumn HeaderText="IDDPL" UniqueName="processIDDPL" DataField="processIDDPL" Display="false" />
											<telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="unitPrice" DataField="unitPrice" Display="false" />
											<telerik:GridBoundColumn HeaderText="ContractingPartyId" UniqueName="ContractingPartyId" DataField="ContractingPartyId" Display="false" />
											<telerik:GridBoundColumn HeaderText="IdSim" UniqueName="IdSim" DataField="IdSim" Display="false" />
											<telerik:GridBoundColumn HeaderText="ForDepartment" UniqueName="ForDepartment" DataField="ForDepartment" Display="false" />
											<telerik:GridBoundColumn HeaderText="SourceType" UniqueName="SourceType" DataField="SourceType" Display="false" />
											<%--<telerik:GridBoundColumn HeaderText="IndexSim" UniqueName="IndexSim" DataField="IndexSim" Display="false" />--%>
											<telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice" Display="false" />
											<telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
											<telerik:GridBoundColumn HeaderText="Kontrahent" UniqueName="ContractingParty" DataField="ContractingParty" />
											<telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
											<telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
											<telerik:GridBoundColumn HeaderText="Nazwa rozszerzona" UniqueName="ExtendedName" DataField="ExtendedName" />
											<telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
											<telerik:GridBoundColumn HeaderText="Ilość do przyjęcia" UniqueName="Quantity" DataField="Quantity" DataFormatString="{0:0.####}" />
                                            <telerik:GridBoundColumn HeaderText="Ilość dodana" UniqueName="QuantityAdded" DataField="QuantityAdded" DataFormatString="{0:0.####}" Display="false" />
											<telerik:GridBoundColumn HeaderText="Ilość zamówiona" UniqueName="QuantityOrdered" DataField="QuantityOrdered" DataFormatString="{0:0.####}" />
											<telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
											<telerik:GridBoundColumn HeaderText="Numer zamówienia" UniqueName="OrderNumberPZ" DataField="OrderNumberPZ" />
											<telerik:GridBoundColumn HeaderText="Planowana data dostawy" UniqueName="SupplyDate" DataField="SupplyDate"	DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" />
											<telerik:GridBoundColumn HeaderText="IdZF" UniqueName="IdZF" DataField="IdZF" Display="False" />
											<telerik:GridBoundColumn HeaderText="Flag" UniqueName="Flag" DataField="Flaga" Display="False" />
											<telerik:GridBoundColumn HeaderText="..." UniqueName="SimpleDepartmentId" DataField="SimpleDepartmentId" Display="False" />
                                            <telerik:GridBoundColumn HeaderText="Uwagi" UniqueName="Uwagi" DataField="Uwagi" Visible="true" />

											
										</Columns>
									</MasterTableView>
									<ClientSettings>
										<Selecting AllowRowSelect="True" />
										<ClientEvents OnRowSelected="RowSelected" />
									</ClientSettings>
								</telerik:RadGrid>
                                </div>
                                <%--<telerik:GridBoundColumn HeaderText="..." UniqueName="SourceClass" DataField="SourceClass" Display="false" />--%>
								<asp:Panel runat="server" ID="PanelSupplyByHand" Visible="false" Enabled="false">
									<br />
									<asp:Label ID="Label10" runat="server" Text="Dostawa spoza listy"></asp:Label>
									<br />
									<table>
										<tr>
											<td>
												<asp:Label ID="Label13" runat="server" Text="Wybierz kontrahenta SIMPLE:"></asp:Label>
											</td>
											<td>
												<asp:Label ID="Label11" runat="server" Text="Wybierz materiał SIMPLE:"></asp:Label>
											</td>
											<td>
												<asp:Label ID="Label12" runat="server" Text="Wprowadź ilość:"></asp:Label>
											</td>
											<td>
												<asp:Label ID="Label14" runat="server" Text="Wprowadź cenę jednostkową:"></asp:Label>
											</td>
											<td>
												<asp:Label ID="Label15" runat="server" Text="Wprowadź datę dostawy:"></asp:Label>
											</td>
											<td>
												<asp:Label ID="Label16" runat="server" Text="Wprowadź przypisanie::"></asp:Label>
											</td>
											<td>
												&nbsp;
											</td>
										</tr>
										<tr>
											<td>
												<telerik:RadComboBox ID="RadComboBox_ContractingPartySimple" runat="server"
													Width="250" ValidationGroup="MaterialByHandGroup">
													<WebServiceSettings Method="" Path="WarehouseWebWcf.svc" />
												</telerik:RadComboBox>
												<%--GetTelericContractingPartySIMPLEList--%>
											</td>
											<td>
												<telerik:RadComboBox ID="RadComboBox_MaterialsSIMPLE" runat="server"
													Width="250" ValidationGroup="MaterialByHandGroup">
													<WebServiceSettings Method="" Path="WarehouseWebWcf.svc" />
												</telerik:RadComboBox>
												<%--GetTelericMaterialsSIMPLEList--%>
											</td>
											<td>
												<%--<asp:TextBox ID="TextBox_SuppliedQuantity" runat="server" ValidationGroup="MaterialByHandGroup"></asp:TextBox>--%>
												<telerik:RadTextBox ID="RadTextBox_SuppliedQuantity" runat="server" Width="90px"
													ValidationGroup="MaterialByHandGroup" />
											</td>
											<%--                                        <td>
										<telerik:RadComboBox Skin="Metro" ID="RadComboBox_Unit" runat="server" EmptyMessage="Wybierz jednostkę SIMPLE"
											EnableLoadOnDemand="True" EnableVirtualScrolling="True" EnableItemCaching="true"
											LoadingMessage="Pobieranie danych" MarkFirstMatch="true" Width="100">
											<WebServiceSettings Method="GetTelericUnitList" Path="WarehouseWebWcf.svc" />
										</telerik:RadComboBox>
									</td>--%>
											<td>
												<telerik:RadTextBox ID="RadTextBox_UnitCost" runat="server" Width="90px" ValidationGroup="MaterialByHandGroup" />
											</td>
											<td>
												<telerik:RadDatePicker ID="RadDatePicker_SupplyDate" runat="server" Culture="pl-PL">
													<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
														runat="server">
													</Calendar>
													<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
												</telerik:RadDatePicker>
											</td>
											<td>
												<telerik:RadComboBox ID="RadComboBox_OrderNumber" runat="server" Width="250" ValidationGroup="MaterialByHandGroup">
													<WebServiceSettings Method="" Path="WarehouseWebWcf.svc" />
												</telerik:RadComboBox>
												<%--GetTelericContractingPartySIMPLEList--%>
											</td>
											<td>
												<asp:Button ID="Button_AddSupply" runat="server" Text="Dodaj dostawę" OnClick="AddSupplyClick"
													ValidationGroup="MaterialByHandGroup" />
											</td>
										</tr>
										<tr>
											<td>
												<asp:CustomValidator ID="CustomValidator_ContractingPartySimple" runat="server" ErrorMessage="Nie wybrano kontrahenta"
													ValidationGroup="MaterialByHandGroup" OnServerValidate="CustomValidator_ContractingPartySimple_ServerValidate"></asp:CustomValidator>
											</td>
											<td>
												<asp:CustomValidator ID="CustomValidator_MaterialSimple" runat="server" ErrorMessage="Nie wybrano materiału"
													ValidationGroup="MaterialByHandGroup" OnServerValidate="CustomValidator_MaterialSimple_ServerValidate"></asp:CustomValidator>
											</td>
											<td>
												<asp:RequiredFieldValidator ID="RequiredFieldValidator_Quantity" runat="server" ErrorMessage="Ilość jest wymagana"
													ControlToValidate="RadTextBox_SuppliedQuantity" ValidationGroup="MaterialByHandGroup"></asp:RequiredFieldValidator>
												<br />
												<asp:CustomValidator ID="CustomValidator_QuantityFormat" runat="server" ErrorMessage="Nieprawidłowy format liczby"
													ValidationGroup="MaterialByHandGroup" OnServerValidate="CustomValidator_QuantityFormat_ServerValidate"></asp:CustomValidator>
											</td>
											<td>
												<asp:RequiredFieldValidator ID="RequiredFieldValidator_UnitCost" runat="server" ErrorMessage="Nie podano ceny jednostkowej"
													ControlToValidate="RadTextBox_UnitCost" ValidationGroup="MaterialByHandGroup"></asp:RequiredFieldValidator>
												<br />
												<asp:CustomValidator ID="CustomValidator_UnitCostFormat" runat="server" ErrorMessage="Nieprawidłowy format liczby"
													ValidationGroup="MaterialByHandGroup" OnServerValidate="CustomValidator_UnitCostFormat_ServerValidate"></asp:CustomValidator>
											</td>
											<td>
												<asp:CustomValidator ID="CustomValidator_SupplyDate" runat="server" ErrorMessage="Nie wybrano daty dostawy"
													ValidationGroup="MaterialByHandGroup" OnServerValidate="CustomValidator_SupplyDate_ServerValidate"></asp:CustomValidator>
											</td>
											<td>
											</td>
											<td>
												<asp:CheckBox ID="CheckBox_GetIncome" runat="server" Checked="false" Text="Przyjmij materiał"
													Enabled="false" />
											</td>
										</tr>
									</table>
								</asp:Panel>
							</td>
						</tr>
					</table>
				</td>

			</tr>
            <tr>
				<td valign="top">
					<div class="inputPanel">
						<table style="width: 100%">
							<tr>
								<td valign="top" style="width: 50%">
									<table class="formTable" style="width: 100%">
										<tr>
											<td valign="top" style="width: 40%">
												<asp:Label ID="Label3" runat="server" Text="Lokalizacja *"></asp:Label>
											</td>
											<td>
												<telerik:RadComboBox Skin="Metro" ID="cmbShelf" runat="server" ValidationGroup="AddPositionGroup" onclientitemsrequesting="OnClientItemsRequesting">
													<WebServiceSettings Method="GetTelericShelfsListWithPrompt" Path="WarehouseWebWcf.svc" />
												</telerik:RadComboBox>
												<asp:CustomValidator ID="ShelfValidator" runat="server" ErrorMessage="Nie wybrano lokalizacji"
													ValidationGroup="AddPositionGroup" OnServerValidate="Shelf_ServerValidate" Display="Dynamic" />
											</td>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Podpowiedź lokalizacji"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="CheckBox_Shelf" Checked="true" AutoPostBack="true" />
                                            </td>
										</tr>
										<tr>
											<td valign="top">
												<asp:Label ID="Label1" runat="server" Text="Przyjmowana ilość *"></asp:Label>
											</td>
											<td>
												<asp:TextBox ID="QuantityTextBox" runat="server"></asp:TextBox><br />
												<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
													ControlToValidate="QuantityTextBox" ValidationGroup="AddPositionGroup" Display="Dynamic" />
												<asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Przyjmowana ilość musi być liczbą dadatnią w odpowiednim formacie"
													ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"
													Display="Dynamic" />
												<asp:CustomValidator ID="QuantityCustomValidator" runat="server" ErrorMessage="Nie można przyjąć większej ilości niż zamówiona."
													ValidationGroup="AddPositionGroup" OnServerValidate="Quantity_ServerValidate" Display="Dynamic" />
											</td>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Kontrola jakości"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="cbIsKJ" Checked="false"/>
                                            </td>
										</tr>
										<%--Aby przyjąć ilość większa niż zamówiona trzeba przekazać nadwyżkę do komory celnej (KC)--%>
										<tr>
											<td>
												<asp:Panel ID="Panel_OverQuantityConfirmation" runat="server">
													<asp:Label ID="Label_OverQuantityConfirmation" runat="server" Text="Czy chcesz przyjąć więcej towaru niż zostało zamówione?"></asp:Label>
													<br />
													<asp:Button ID="Button_OQC_Yes" runat="server" Text="Tak" OnClick="Button_OQC_Yes_Click" /><asp:Button
														ID="Button_OQC_No" runat="server" Text="Nie" OnClick="Button_OQC_No_Click" ValidationGroup="AddPositionGroup" />
												</asp:Panel>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<asp:RequiredFieldValidator ID="Package_Quantity_RequiredFieldValidator" runat="server"
													ErrorMessage="Ilość sztuk w opakowaniu jest wymagana" ControlToValidate="TextBox_PackageQuantity"
													ValidationGroup="AddPositionGroup" Display="Dynamic" />
												<asp:RequiredFieldValidator ID="Packages_Number_RequiredFieldValidator" runat="server"
													ErrorMessage="Liczba opakowań jest wymagana" ControlToValidate="TextBox_PackagesNumber"
													ValidationGroup="AddPositionGroup" Display="Dynamic" />
												<asp:CustomValidator ID="PackagesCustomValidator" runat="server" ErrorMessage="Należy zdefiniować opakowania dla całej przyjmowanej ilości lub wpisać ilość sztuk luzem"
													ValidationGroup="AddPositionGroup" OnServerValidate="CustomValidator_Packages_ServerValidate"
													Display="Dynamic" />
												<asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału"
													ValidationGroup="AddPositionGroup" OnServerValidate="MaterialCustomValidator_ServerValidate"
													CssClass="ConfirmationMessage" Display="Dynamic" />
												<asp:CustomValidator ID="ContractingPartyChanged" runat="server" ErrorMessage="Nie można wystawić dokumentu PZ dla różnych dostawców"
													ValidationGroup="AddPositionGroup" OnServerValidate="ContractingParty_ServerValidate"
													Display="Dynamic" />
												<asp:CustomValidator ID="OrderNumberPZChanged" runat="server" ErrorMessage="Nie można wystawić dokumentu PZ dla różnych numerów zamówień"
													ValidationGroup="AddPositionGroup" OnServerValidate="OrderNumberPZ_ServerValidate"
													Display="Dynamic" />
											</td>
										</tr>
										<tr>
											<td>
											</td>
										</tr>
									</table>
								</td>
								<td valign="top" style="width: 50%">
									<table class="formTable" style="border: 2px solid rgba(0,0,0,0.1); padding: 10px;
										width: 100%">
										<tr>
											<td>
												Typ opakowania
											</td>
											<td>
												<telerik:RadComboBox Skin="Metro" ID="RadComboBox_PackageTypeName" runat="server"
													EmptyMessage="Wybierz typ opakowania" EnableLoadOnDemand="True" EnableVirtualScrolling="True"
													EnableItemCaching="true" LoadingMessage="Pobieranie danych" MarkFirstMatch="true">
													<WebServiceSettings Method="GetTelericPackageTypeList" Path="WarehouseWebWcf.svc" />
												</telerik:RadComboBox>
											</td>
										</tr>
										<tr>
											<td>
												Ilość szt. w opakowaniu
											</td>
											<td>
												<asp:TextBox ID="TextBox_PackageQuantity" runat="server" Text="0"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td>
												Ilość opakowań
											</td>
											<td>
												<asp:TextBox ID="TextBox_PackagesNumber" runat="server" Text="0"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<div style="font-size: 8pt; opacity: 0.5">
													<asp:Label ID="Label17" runat="server" Text="Opakowania:"></asp:Label>
													Uwaga! Wprowadź informację o tych opakowaniach, w których przyjmujesz materiały
													i w których te materiały będą wydawane na produkcję.
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Button ID="btnAddPosition" runat="server" OnClick="AddPositionClick" Text="Dodaj pozycje"
										ValidationGroup="AddPositionGroup" />
								</td>
							</tr>
						</table>
					</div>
					<div class="inputPanel">
						<table style="width: 100%">
							<tr>
								<td>
									<telerik:RadGrid ID="MaterialsToReceiveRadGrid" runat="server" OnNeedDataSource="MaterialsToReceiveRadGrid_OnNeedDataSource"
										AutoGenerateColumns="False" GridLines="None" Skin="Metro" OnItemCommand="MaterialsToReceiveRadGrid_DeleteRow">
										<MasterTableView ForeColor="SteelBlue" Font-Names="Tahoma" Font-Size="Small" Font-Bold="true"
											EnableViewState="true">
											<Columns>
												<telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" />
												<telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="MaterialName" />
												<telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
												<telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
												<telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
												<telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                                                <telerik:GridCheckBoxColumn HeaderText="KJ" UniqueName="IsKJ" DataField="IsKJ"></telerik:GridCheckBoxColumn>
												<telerik:GridButtonColumn ButtonType="ImageButton" HeaderButtonType="PushButton"
													UniqueName="column" CommandName="DeleteRow" ImageUrl="~\Images\delete.png" Visible="True">
												</telerik:GridButtonColumn>
											</Columns>
											<EditFormSettings>
												<EditColumn UniqueName="EditCommandColumn1">
												</EditColumn>
											</EditFormSettings>
										</MasterTableView>
									</telerik:RadGrid>
								</td>
							</tr>
						</table>
						<table style="margin-top: 20px;" class="formTable">
							<tr>
								<td>
									<asp:Label ID="Label2" runat="server" Text="Numer faktury zewnętrznej"></asp:Label>
								</td>
								<td>
									<telerik:RadTextBox ID="ExternalInvoiceNumber" runat="server" Width="90px">
									</telerik:RadTextBox>
								</td>
							</tr>
							<tr>
								<td>
									Data dokumentu
								</td>
								<td>
									<telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
									</telerik:RadDatePicker>
								</td>
							</tr>
							<tr>
								<td>
									Eksportuj do SIMPLE
								</td>
								<td>
									<asp:CheckBox ID="CheckBox_ExportToSIMPLE" runat="server" Checked="true" />
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:Label ID="Label_ExportToSIMPLE" runat="server" Text="Eksport do SIMPLE niemożliwy"
										ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
									<br />
									<asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick" Text="Zapisz dokument"
										Enabled="True" ValidationGroup="ReceiveMaterialGroup" />
									<asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokument" Enabled="False" />
									<asp:Button ID="btnNewDocument" runat="server" Text="Nowy dokument" OnClick="NewDocumentClick" />
								</td>
							</tr>
						</table>
					</div>
				</td>
            </tr>
		</table>
</asp:Content>
