﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocPrintLayoutFooter.ascx.cs" Inherits="MagWeb.WarehouseDocuments.DocPrintLayoutFooter" %>

<table class="printFooter">
    <tr>
        <td>
			<asp:Label ID="Label_Podp6" runat="server" Text="___________________________"></asp:Label>
            <asp:Label ID="Label_Podp1" runat="server" CssClass="footerText" Text="Wydał"></asp:Label>
        </td>
        <td>
			<asp:Label ID="Label_Podp7" runat="server" Text="___________________________"></asp:Label>
            <asp:Label ID="Label_Podp2" runat="server" CssClass="footerText" Text="Odebrał, data"></asp:Label>
        </td>
    </tr>
</table>
