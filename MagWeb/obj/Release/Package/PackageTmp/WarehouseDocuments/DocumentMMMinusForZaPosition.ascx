﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentMMMinusForZaPosition.ascx.cs" Inherits="MagWeb.WarehouseDocuments.DocumentMMMinusForZaPosition" %>
<div class="printRow">

	<table>
		<tr>
			<td >
				<table class="printRowHeader">
					<tr>
						<td>
							<span class="printHeaderTitle">Przypisanie</span>
							<asp:Label ID="OrderNumber" runat="server" CssClass="printData" Text="OrderNumber"></asp:Label>
						</td>
						<td>
							<span class="printHeaderTitle">Do magazynu</span>
							<asp:Label ID="WarehouseTarget" runat="server" CssClass="printData" Text="WarehouseTarget"></asp:Label>
						</td>
						<td>
							<span class="printHeaderTitle">Zamawiający</span>
							<asp:Label ID="OrderingPerson" CssClass="printData" runat="server" Text=""></asp:Label>
						</td>
						<td style="visibility: hidden;">
							<span class="printHeaderTitle">Nr dokumentu</span> 
							<asp:Label ID="DocumentId" CssClass="printData" runat="server" Text="DocumentId"></asp:Label>
						</td>
					</tr>
				</table>

				<telerik:RadGrid ID="DataRadGrid" runat="server" OnNeedDataSource="DataRadGrid_OnNeedDataSource" AutoGenerateColumns="False" GridLines="None" Width="100%">
					<MasterTableView>
						<Columns>
							<telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber" HeaderStyle-Width="5%">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE"  HeaderStyle-Width="20%">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Nazwa Towaru" UniqueName="MaterialName" DataField="MaterialName" >
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity"  HeaderStyle-Width="5%">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Jm" UniqueName="UnitName" DataField="UnitName"  HeaderStyle-Width="5%">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="Shelf" DataField="Shelf"  HeaderStyle-Width="10%">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Opakowanie" UniqueName="Package" DataField="Package" HeaderStyle-Width="10%">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn HeaderText="Źródło" UniqueName="Source" DataField="Source" HeaderStyle-Width="10%">
							</telerik:GridBoundColumn>

						</Columns>
					</MasterTableView>
				</telerik:RadGrid>
			
			</td>
			<td class="printDates">
				<span class="printHeaderTitle">Czas zamówienia</span>
				<asp:Label ID="Date" runat="server" CssClass="printData" Text=""></asp:Label>

				<span class="printHeaderTitle">Czas wymagania</span>
				<asp:Label ID="DueTime" runat="server" CssClass="printData"  Text=""></asp:Label>

				<span class="printHeaderTitle">Czas wydania</span>
				<asp:Label ID="GiveTime" runat="server" CssClass="printData" Text=""></asp:Label>
			</td>
		</tr>
	</table>
	
</div>
