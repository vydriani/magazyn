﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="CustomsChamberReturns.aspx.cs" Inherits="MagWeb.CustomsChamberReturns" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Lista zwróconych materiałów
        <br />
        (<a href="GiveMaterialReturn.aspx">zobacz listę materiałów oczekujących na zwrot</a>)
    </p>
	
	<input type="search" class="light-table-filter" data-table="order-table" placeholder="filtruj" />
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <telerik:RadGrid ID="CustomsChamberMaterialReturnsGrid" runat="server" OnNeedDataSource="CustomsChamberMaterialsGrid_OnNeedDataSource">
            <MasterTableView CssClass="order-table table"  EnableViewState="true" DataKeyNames="WarehouseDocumentPositionId">
                <%--ClientDataKeyNames="Quantity"--%>
                <Columns>
                    <telerik:GridBoundColumn UniqueName="KCSolvingDocumentId" DataField="KCSolvingDocumentId" Display="false" />
                    <telerik:GridBoundColumn UniqueName="OrderId" DataField="OrderId" Display="false" />
                    <telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="ContractingPartyName" DataField="ContractingPartyName" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Nr zam. do dost." UniqueName="OrderNumberFromPZ" DataField="OrderNumberFromPZ" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Indeks materiału" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Nazwa materiału" UniqueName="MaterialName" DataField="MaterialName" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="KCQuantity" DataField="KCQuantity" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="UnitName" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Data zwrotu" UniqueName="KCSolvingDocumentDate" DataField="KCSolvingDocumentDate" Visible="true" />
                    <telerik:GridBoundColumn HeaderText="Nr dokumentu zwrotu" UniqueName="KCSolvingSIMPLEDocNumber" DataField="KCSolvingSIMPLEDocNumber" Visible="true" />
                    <telerik:GridTemplateColumn UniqueName="PrintDocument" HeaderText="Poddląd i wydruk dokumentu">
                        <ItemTemplate>
                            <asp:Button ID="Button_Print" 
                                runat="server" Text="Podgląd / Wydruk" Visible="True" OnLoad="Button_Print_OnLoad" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </asp:Panel>
</asp:Content>
