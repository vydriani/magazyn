<%@ Page Title="" Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="CustomsChamber.aspx.cs" Inherits="MagWeb.CustomsChamber" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
	<!-- custom head section -->
	<telerik:radcodeblock id="RadCodeBlock1" runat="server">
		<script type="text/javascript">
			function RowSelected(sender, args) {
				//alert(args);
				//if (args.get_tableView().get_name() != "Master") {
				var q = args.getDataKeyValue("QuantityKCNotAssigned");
				if (q != null && q != 'null')
					document.getElementById("<%= TextBox_Quantity1.ClientID %>").value = q;
				//}
				//alert(document.getElementById("<%= RadComboBox_Orders.ClientID %>").textContent);
			}

			function GetFirstDataItemKeyValues() {
				//                var firstDataItem = $find(x).get_dataItems()[0];
				//                var keyValues =
				//                    'Quantity = "' + firstDataItem.getDataKeyValue("Quantity") + '"';
				//                alert(keyValues);
			}
		</script>
	</telerik:radcodeblock>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
	<p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
		<asp:Label ID="Label_Title" runat="server" Text="Label"></asp:Label> <asp:Label ID="Label_Dispository" runat="server" Text="<b>+ możliwość dysponowania</b>"
			Visible="False"></asp:Label>
	</p>
		<asp:HiddenField ID="HiddenField1" runat="server" />
		<table width="100%">
			<tr>
				<td style="padding-right: 10px; vertical-align: top;">
					<input type="search" class="light-table-filter" data-table="order-table" placeholder="filtruj" />
					<telerik:RadGrid id="CustomsChamberMaterialsGrid" runat="server" 
						OnNeedDataSource="CustomsChamberMaterialsGrid_OnNeedDataSource" OnItemDataBound="CustomsChamberMaterialsGrid_ItemDataBound"
						OnDetailTableDataBind="CustomsChamberMaterialsGrid_DetailTableDataBind" selectionmode="Single" OnSelectedIndexChanged="CustomsChamberMaterialsGrid_SelectedIndexChanged"
						hierarchydefaultexpanded="Yes">
						<MasterTableView Name="Master" CssClass="order-table table"  EnableViewState="true" ClientDataKeyNames="WarehouseDocumentPositionId,OrderNumber,QuantityKCNotAssigned"
							DataKeyNames="WarehouseDocumentPositionId">
							<Columns>
								<telerik:GridBoundColumn UniqueName="WarehouseDocumentPositionId" DataField="WarehouseDocumentPositionId" Display="false" />
								<telerik:GridBoundColumn UniqueName="OrderId" DataField="OrderId" Display="false" />
								<telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" Display="true" />
								<telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="ContractingPartyName" DataField="ContractingPartyName" Display="true" />
								<telerik:GridBoundColumn HeaderText="Nr zam. do dost." UniqueName="OrderNumberFromPZ" DataField="OrderNumberFromPZ" Display="true" />
								<telerik:GridBoundColumn HeaderText="Indeks materiału" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" Display="true" />
								<telerik:GridBoundColumn HeaderText="Nazwa materiału" UniqueName="MaterialName" DataField="MaterialName" Display="true" />
								<telerik:GridBoundColumn HeaderText="Ilość zamówiona" UniqueName="QuantityOrdered" DataField="QuantityOrdered" Display="true" />
									<telerik:GridBoundColumn HeaderText="Ilość nadwyżki" UniqueName="QuantityKCNotAssigned" DataField="QuantityKCNotAssigned" Display="true" />
								<telerik:GridBoundColumn HeaderText="Ilość rozdysponowana" UniqueName="QuantityKCAssigned" DataField="QuantityKCAssigned" Display="false" />
								<telerik:GridBoundColumn HeaderText="Ilość oczekująca" UniqueName="QuantityKCNotAssigned" DataField="QuantityKCNotAssigned" Display="false" />
								<telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="UnitName" Display="true" />
								<telerik:GridBoundColumn HeaderText="Nr zlec.zam." UniqueName="OrderNumber" DataField="OrderNumber" Display="false" />
								<telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="Assignment" DataField="Assignment" Display="true" />
								<telerik:GridBoundColumn HeaderText="Data przyjazdu" UniqueName="ArrivalDate" DataField="ArrivalDate" Display="true" />
								<telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" Display="true" />
							</Columns>
							<DetailTables>
								<%--DataKeyNames="WarehouseDocumentPositionId" --%>
								<telerik:GridTableView Name="KCDetails" Width="100%">
									<HeaderStyle CssClass="color2" />
									<Columns>
										<telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" Display="true" />
										<telerik:GridBoundColumn UniqueName="WarehouseDocumentPositionKCStateId" DataField="WarehouseDocumentPositionKCStateId" Display="false" />
										<telerik:GridBoundColumn HeaderText="Ilość" UniqueName="KCQuantity" DataField="KCQuantity" Display="true" Aggregate="Sum" FooterText="<b>SUMA: </b>" />
										<telerik:GridBoundColumn HeaderText="Typ dyspozycji" UniqueName="KCStateTypeName" DataField="KCStateTypeName" Display="true" />
										<telerik:GridBoundColumn HeaderText="Czas wymagania" UniqueName="TimeRequired" DataField="TimeRequired" Display="false" />
										<telerik:GridTemplateColumn UniqueName="TimeRequiredPicker" HeaderText="Czas wymagania">
											<ItemTemplate>
												<telerik:RadDatePicker ID="RadDatePicker_DueDateTime" OnLoad="RadDatePicker_DueDateTime_OnLoad" OnSelectedDateChanged="RadDatePicker_DueDateTime_SelectedDateChanged" runat="server" SelectedDate='<%# Eval("TimeRequired") %>' Enabled='<%# Eval("IsDateEditEnabled") %>' />
												<telerik:RadButton ID="Button_ApplyDueDateTimeChange" OnClick="Button_ChangeDueDateTime_Click" runat="server" Text="Zmień" Visible='<%# Eval("IsDateEditEnabled") %>' />
												<telerik:RadButton ID="Button_DeleteDisposition" OnClick="Button_DeleteDisposition_Click" runat="server" Text="Anuluj" Visible='<%# Eval("IsDateEditEnabled") %>' />
											</ItemTemplate>
										</telerik:GridTemplateColumn>
										<telerik:GridBoundColumn HeaderText="Typ dokumentu" UniqueName="KCSolvingDocumentTypeShortName" DataField="KCSolvingDocumentTypeShortName" Display="true" />
										<telerik:GridBoundColumn HeaderText="Czas utw. dok." UniqueName="KCSolvingDocumentDate" DataField="KCSolvingDocumentDate" Display="true" />
										<telerik:GridBoundColumn HeaderText="Przypisanie docelowe" UniqueName="KCSolvingDocumentPositionOrderNumber" DataField="KCSolvingDocumentPositionOrderNumber" Display="true" />
										<telerik:GridBoundColumn HeaderText="DateTimePickerUniqueID" UniqueName="DateTimePickerUniqueID" DataField="DateTimePickerUniqueID" Display="false" />
										<telerik:GridBoundColumn HeaderText="Uwagi" UniqueName="Comment" DataField="Comment" Display="true" />
									</Columns>
								</telerik:GridTableView>
							</DetailTables>
						</MasterTableView>
						<ClientSettings>
							<Selecting AllowRowSelect="True" />
							<ClientEvents OnRowSelected="RowSelected" />
						</ClientSettings>
					</telerik:RadGrid>
				</td>
				<td style="vertical-align: top; width: 25%">
					<asp:Panel ID="Panel_Disposition" runat="server" Enabled="False">
						<div class="inputPanel">
						Dysponuj pozycją:<br />
							<telerik:RadComboBox ID="RadComboBox_KCStates" runat="server" OnSelectedIndexChanged="RadComboBox_KCStates_SelectedIndexChanged" AutoPostBack="True">
						</telerik:RadComboBox>
						<br />
						Ilość:
						<br />
						<%--<asp:TextBox ID="TextBox_Quantity" runat="server" ValidationGroup="Chamber" EnableViewState="True"></asp:TextBox>--%>
						<telerik:RadTextBox ID="TextBox_Quantity1" runat="server" ValidationGroup="Chamber" EnableViewState="True" EmptyMessage="Ilość" EmptyMessageStyle="font-style: italic"></telerik:RadTextBox>
						<br />
						<asp:CustomValidator ID="QuantityDataTypeValidator" runat="server" ErrorMessage="Przyjmowana ilość musi być liczbą dadatnią w odpowiednim formacie"
							OnServerValidate="QuantityDataTypeValidator_ServerValidate" ValidationGroup="Chamber"></asp:CustomValidator>
						<asp:Panel ID="Panel_Orders" runat="server">
							Przypisanie:
							<br />
							<asp:CheckBox ID="CheckBox_GetOrderFromKCPos" runat="server" Checked="True" Text="Przypisanie pierwotne"
								AutoPostBack="True" />
							<telerik:RadComboBox ID="RadComboBox_Orders" runat="server">
								<WebServiceSettings Method="GetTelericOrdersList" Path="WarehouseWebWcf.svc" />
							</telerik:RadComboBox>
							<asp:CustomValidator ID="CustomValidator_OrderNumber" runat="server" ErrorMessage="Nie określono numeru przypisania"
								OnServerValidate="CustomValidator_OrderNumber_ServerValidate" ValidationGroup="Chamber"></asp:CustomValidator>
						</asp:Panel>
						<asp:Panel ID="Panel_Dates" runat="server">
							Data zwrotu:
							<br />
							<telerik:raddatepicker id="RadDatePicker_ReturnDate" runat="server">
							</telerik:raddatepicker>
						</asp:Panel>
						<br />
						<telerik:RadTextBox ID="TextBox_Comment" runat="server" TextMode="MultiLine" MaxLength="100" Columns="25" Rows="3" EmptyMessage="Uwagi" EmptyMessageStyle="font-style: italic" ></telerik:RadTextBox>
						<br />
						<asp:Button ID="Button_Disposition" runat="server" OnClick="Button_Disposition_Click"
							Text="Dysponuj" ValidationGroup="Chamber" />
						</div>
					</asp:Panel>
				</td>
			</tr>
		</table>
</asp:Content>
