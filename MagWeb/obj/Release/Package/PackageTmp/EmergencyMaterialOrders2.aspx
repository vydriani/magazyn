﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmergencyMaterialOrders2.aspx.cs" Inherits="MagWeb.EmergencyMaterialOrders2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lista wydań dodatkowych odrzuconych/zaakceptowanych</title>
    
    <style type="text/css">
        .module1
        {
            background-color: #dff3ff;
            border: 1px solid #c6e1f2;
        }
    </style>
</head>
<body bgcolor="#dced99">
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" Skin="Metro" />
    <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:Panel ID="Panel1" runat="server" Height="275px" Style="padding-top: 15px;
        padding-left: 15px">
        <h1>
            <asp:Label ID="Label_ReportName" runat="server" Text="Lista wydań dodatkowych odrzuconych/zaakceptowanych" ForeColor="Navy"></asp:Label>
        </h1>
        <table border="0">
            <tr>
                <td>
                    Przypisanie:
                </td>
                <td>
                    <telerik:RadComboBox ID="ordersRCB" runat="server" EmptyMessage="Wprowadź przypisanie" SelectedIndexChanged="ordersRCB_SelectedIndexChanged" AutoPostBack="False" Width="300px">
                        <WebServiceSettings Method="GetTelericOrdersList" Path="WarehouseWebWcf.svc" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Materiał/Indeks:
                </td>
                <td>
                    <telerik:RadComboBox ID="materialsRCB" runat="server" EmptyMessage="Wprowadź nazwę lub indeks" AutoPostBack="False" Width="300px">
                        <WebServiceSettings Method="GetTelericMaterialsListCompany1" Path="WarehouseWebWcf.svc" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Osoba zamawiająca:
                </td>
                <td>
                    <telerik:RadComboBox ID="personsRCB" runat="server" EmptyMessage="Wprowadź imię lub nazwisko" AutoPostBack="False" Width="300px">
                        <WebServiceSettings Method="GetTelericPersonList" Path="WarehouseWebWcf.svc" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Technolog:
                </td>
                <td>
                    <telerik:RadComboBox ID="technologistPersonsRCB" runat="server" EmptyMessage="Wprowadź imię lub nazwisko" AutoPostBack="False" Width="300px">
                        <WebServiceSettings Method="GetTelericPersonList" Path="WarehouseWebWcf.svc" />
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Button_ApplyFilter" runat="server" Text="Filtruj" OnClick="Button_ApplyFilterClick" />
                </td>
            </tr>
        </table>
        <p>
            <telerik:RadGrid	ID="EmergencyMaterialOrders2Grid" runat="server" OnNeedDataSource="EmergencyMaterialOrders2Grid_OnNeededDataSource" OnItemDataBound="EmergencyMaterialOrders2Grid_ItemDataBound" 
								AllowMultiRowSelection="False" SelectionMode="Single">
                <ClientSettings AllowDragToGroup="true" AllowGroupExpandCollapse="true" AllowColumnsReorder="true"
                    ReorderColumnsOnClient="true" ColumnsReorderMethod="Reorder" Selecting-AllowRowSelect="False"
                    EnablePostBackOnRowClick="False">
                    <Selecting AllowRowSelect="False" />
                </ClientSettings>
                <GroupingSettings GroupContinuesFormatString=" Pozostałe elementy na następnej stronie."
                    GroupContinuedFormatString="Pozostałe elementy z poprzedniej strony. " GroupSplitDisplayFormat="{0} z {1} elementów." />
                <MasterTableView  EnableViewState="true" GroupLoadMode="Client">
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="L.p." UniqueName="NumberColumn" DataField="Number" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="MaterialIndex" DataField="Material.IndexSIMPLE" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Materiał" UniqueName="MaterialName" DataField="Material.Name" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="QuantityAbs" ItemStyle-HorizontalAlign="Right" Groupable="false" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="Material.Unit.ShortName" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="Order.OrderNumber" ItemStyle-HorizontalAlign="Right" Groupable="false" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Data zam." UniqueName="Date" DataField="WarehouseDocument.TimeStamp" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Osoba zam." UniqueName="OrderPersonName" DataField="WarehouseDocument.Person.PersonName" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Technolog prow." UniqueName="TechnologistPersonName" DataField="Order.Productions[0].Technologist" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Data wpisu technologa" UniqueName="TechnologistDate" DataField="WarehouseDocument.TimeStamp" Visible="True" />
                        <telerik:GridBoundColumn HeaderText="Data rozliczenia" UniqueName="Date2" DataField="WarehouseDocument.TimeStamp" Visible="True" />
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:Button ID="Button_Reload" runat="server" Text="Wyczyść widok" OnClick="ButtonReloadClick" />
        </p>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"  IsSticky="true">
    </telerik:RadAjaxLoadingPanel>
    </form>
</body>
</html>
