<%@ Page Language="C#" AutoEventWireup="true" Theme="Client" CodeBehind="InventoryList.aspx.cs" Inherits="MagWeb.InventoryList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		<title>MAG3 - Inwentaryzacja</title>
		<link rel="shortcut icon" href="~/Images/favicon.ico" />
</head>
<body>
		<form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="sm" AsyncPostBackTimeout="0" EnablePartialRendering="true"></asp:ScriptManager>
		<div style="margin: 15px">
<%--				<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
				</telerik:RadScriptManager>
--%>				<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" Skin="Metro" />
				<h2 align="center">
						Lista Inwentaryzacji</h2>
				<h4 align="center">
						Magazyn:
						<asp:Label ID="CurrentWarehouseAndCompanyName" runat="server" Text="Label"></asp:Label></h4>
				<h3 align="center">
						<b>Status:
								<asp:Label ID="LabelDocStatus" runat="server" Text="???"></asp:Label></b>
				</h3>
				<asp:Panel ID="Panel_Alert" runat="server" CssClass="AlertMsg">
						<table width="100%">
								<tr>
										<td>
												&nbsp;
										</td>
										<td valign="middle" width="100">
											<img src="~/Images/Icons/48x48/shadow/lightbulb_on.png" />
										</td>
										<td valign="middle">
												<asp:Label ID="Label_Alert" runat="server" Text="Alert Message"></asp:Label>
										</td>
										<td>
												&nbsp;
										</td>
								</tr>
						</table>
				</asp:Panel>
				<asp:Button ID="ImageButtonAddMaterial" runat="server" Text="Dodaj materiał" OnClick="ImageButtonAddMaterial_Click" />
				<asp:Panel ID="PanelAddMaterial" runat="server">
					<div class="inputPanel">
						<table>
								<tr>
										<td style="vertical-align: top; width: 20%">
											<div class="inputPanel" style="margin-right: 10px">
														<div class="panelLabel">Wybierz materiał</div>
														<asp:Label ID="Label4" runat="server"  CssClass="labelSearch" Text="Nazwa lub indeks materiału:"></asp:Label>
														<telerik:RadTextBox ID="MaterialToSearchRTB" runat="server" Width="70%">
														</telerik:RadTextBox>
														<asp:Button ID="Button1" runat="server" OnClick="SearchMaterialClick" Text="Wyszukaj materiał"
																Enabled="True" />
														<br /><br />		
														<telerik:RadGrid ID="MaterialGrid" runat="server" OnItemCommand="MaterialGrid_RowCommand">
																<MasterTableView EnableViewState="true">
																		<Columns>
																				<telerik:GridBoundColumn HeaderText="SimpleId" UniqueName="SimpleId" DataField="SimpleId" Display="false" />
																				<telerik:GridBoundColumn HeaderText="CompanyId" UniqueName="CompanyId" DataField="CompanyId" Display="false" />
																				<telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
																				<telerik:GridBoundColumn HeaderText="Indeks (K3)" UniqueName="IndexSimpleK3" DataField="IndexSimpleK3" />
																				<telerik:GridBoundColumn HeaderText="Nazwa" UniqueName="MaterialName" DataField="MaterialName" />
																				<telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
																				<telerik:GridButtonColumn CommandName="SelectRow" Text="Wybierz" />
																		</Columns>
																</MasterTableView>
																<ClientSettings>
																		<Selecting AllowRowSelect="False" />
																</ClientSettings>
														</telerik:RadGrid>
														<asp:CustomValidator ID="CustomValidator_Material" runat="server" ErrorMessage="Wybierz materiał"
																ValidationGroup="AddPositionGroup" OnServerValidate="MaterialValidator_ServerValidate"></asp:CustomValidator>
											</div>
										</td>
										<td style="vertical-align: top; width: 30%">
											<div class="inputPanel" style="margin-right: 10px; vertical-align: top">
                                                <asp:UpdatePanel ID="upExport" runat="server">
                                                    <ContentTemplate>

														<div class="panelLabel">Wybierz przypisanie</div>
														<asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
																<asp:ListItem Value="1" Selected="True">Zlecenie</asp:ListItem>
																<asp:ListItem Value="2">Stan Magazynowy</asp:ListItem>
														</asp:RadioButtonList>
														<br />

														<asp:Label ID="Label_visible" runat="server">
																<asp:Label ID="Label_Order" runat="server" CssClass="labelSearch" Text="Przypisanie:"></asp:Label>
																<telerik:RadTextBox ID="RadTextBox_OrderNumberToSearch" runat="server" Width="70%" Enabled="True">
																</telerik:RadTextBox>
																<asp:Button ID="Button_SearchOrder" runat="server" OnClick="SearchOrderClick" Text="Wyszukaj przypisanie"
																		Enabled="True" />
																<br /><br />

																<telerik:RadGrid ID="OrderRadGrid" runat="server" OnItemCommand="OrderGrid_RowCommand">
																		<MasterTableView EnableViewState="true">
																				<Columns>
																						<telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
																						<telerik:GridBoundColumn HeaderText="Number" UniqueName="Number" DataField="Number" />
																						<telerik:GridButtonColumn CommandName="SelectRow" Text="Wybierz" />
																				</Columns>
																		</MasterTableView>
																		<ClientSettings>
																				<Selecting AllowRowSelect="False" />
																		</ClientSettings>
																</telerik:RadGrid>
														</asp:Label>

                                                        
                                                        <asp:panel runat="server" ID="lbExportToSimple" Visible="false">
                                                            
                                                            <asp:CheckBox ID="cbExportToSIMPLE" runat="server" Checked="false" Text="Export do SIMPLE (ZW)"/>

                                                            
                                                            <table style="width:450px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lbOrder" runat="server" CssClass="labelSearch" Text="Przypisanie:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lbDepartment" runat="server" Text="Komórka organizacyjna:" CssClass="labelSearch"></asp:Label>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <telerik:RadTextBox ID="rtbOrder" runat="server" Width="50%" Enabled="True"></telerik:RadTextBox>    
                                                                        </td>

                                                                        <td>
                                                                            <telerik:RadComboBox ID="rcbDepartment" runat="server" Width="220px" OnClientItemsRequesting="OnClientRequesting" >
                                                                                <WebServiceSettings Method="GetDepartmentsFilteredList" Path="WarehouseWebWcf.svc"/>
                                                                            </telerik:RadComboBox>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            

                                                        </asp:panel>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

											</div>
										</td>
										<td style="vertical-align: top; width: 30%">
											<div class="inputPanel">
														<div class="panelLabel">Wybierz lokalizację</div>
														<asp:Label ID="Label3" runat="server" CssClass="labelSearch" Text="Lokalizacja:"></asp:Label>
														<telerik:RadTextBox ID="ShelfToFind" runat="server" Width="70%" Enabled="True">
														</telerik:RadTextBox>
														<asp:Button ID="SearcjShelfButton" runat="server" OnClick="SearchShelfClick" Text="Wyszukaj lokalizację"
																Enabled="True" />
														<br /><br />
														<telerik:RadGrid ID="ShelfRadGrid" runat="server">
																<MasterTableView EnableViewState="true">
																		<Columns>
																				<telerik:GridBoundColumn HeaderText="Id" UniqueName="Id" DataField="Id" Display="false" />
																				<telerik:GridBoundColumn HeaderText="Name" UniqueName="Name" DataField="Name" />
																		</Columns>
																</MasterTableView>
																<ClientSettings>
																		<Selecting AllowRowSelect="True" />
																</ClientSettings>
														</telerik:RadGrid>
														<asp:CustomValidator ID="CustomValidator_Shelf" runat="server" ErrorMessage="Wybierz lokalizację"
																ValidationGroup="AddPositionGroup" OnServerValidate="ShelfValidator_ServerValidate"></asp:CustomValidator>
											</div>
										</td>
                                        <td style="vertical-align: top; width: 20%"></td>
								</tr>
								<tr>
										<td style="vertical-align:top; width: 20%; ">
											<div class="inputPanel" style="margin-right: 10px;">
												<asp:Label ID="Label1" runat="server" Text="Stwierdzona ilość:" Font-Size="Large" ></asp:Label>
												<br />
												<telerik:RadTextBox ID="Quantity" runat="server" SelectionOnFocus="SelectAll" 
														Font-Bold="true">
												</telerik:RadTextBox>
												<br />
												<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilośc jest wymagana"
														ControlToValidate="Quantity" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
												<br />
												<asp:CustomValidator ID="MaterialCustomValidator" runat="server" ErrorMessage="Nie wybrano żadnego materiału"
														ValidationGroup="AddPositionGroup" OnServerValidate="MaterialCustomValidator_ServerValidate"></asp:CustomValidator>
												<%--                        <asp:CustomValidator ID="QuantityChangeCustomValidator" runat="server" ErrorMessage="Stwierdzona ilość nie różni się od ilości na stanie"
														ValidationGroup="AddPositionGroup" OnServerValidate="QuantityChangeCustomValidator_ServerValidate_ServerValidate"></asp:CustomValidator>--%>
											</div>
										</td>
										<td style="vertical-align:top; width: 30%">
											<div class="inputPanel" style="margin-right: 10px;">
												<asp:Label ID="Label2" runat="server" Text="Cena jednostkowa:" Font-Size="Large"></asp:Label>	  <br />
												<telerik:RadTextBox ID="ValuePerUnit" runat="server" Text="0.00" >
												</telerik:RadTextBox>
												<asp:ImageButton ID="ImageButtonGetUnitPrice" runat="server" ImageUrl="~/Images/Icons/24x24/shadow/package_view.png" />
												<br />
												<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Cena jednostkowa jest wymagana"
														ControlToValidate="ValuePerUnit" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
												<br />
												<asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Wprowadzona ilość musi być dodatnia"
														ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"></asp:CustomValidator>
											</div>
										</td>
										<td style="vertical-align:top; width: 30%">
												<div class="inputPanel">
													<asp:Label ID="Label5" runat="server" Text="IDPL:" Font-Size="Large"></asp:Label><br />
													<telerik:RadTextBox ID="ProcessIdpl" runat="server" Text="0" >
													</telerik:RadTextBox>
													<asp:ImageButton ID="ImageButtonGetProcessIdpl" runat="server" ImageUrl="~/Images/Icons/24x24/shadow/package_view.png" />
													<br />
													<asp:CustomValidator ID="DataTypeValidatorIdpl" runat="server" ErrorMessage="Wprowadzone wartość musi być niezerową liczbą całkowitą"
															ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidatorIdpl_ServerValidate"></asp:CustomValidator>
												</div>
										</td>
                                        <td style="vertical-align:top; width: 20%">
												<div class="inputPanel">
													<asp:Label ID="Label6" runat="server" Text="IDDPL:" Font-Size="Large"></asp:Label><br />
													<telerik:RadComboBox ID="ProcessIddpl" runat="server" Text="0">
                                                    </telerik:RadComboBox>
                                                    <asp:ImageButton ID="ImageButtonGetProcessIddpl" runat="server" ImageUrl="~/Images/Icons/24x24/shadow/package_view.png" />
													<br />
													<asp:CustomValidator ID="DataTypeValidatorIddpl" runat="server" ErrorMessage="Wprowadzone wartość musi być niezerową liczbą całkowitą"
															ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidatorIddpl_ServerValidate"></asp:CustomValidator>
												</div>
										</td>
								</tr>
								<tr>
										<td colspan="3">
												
										</td><asp:Button ID="btnAddPosition" runat="server" OnClick="AddPositionClick" Text="Dodaj pozycje" ValidationGroup="AddPositionGroup" />
								</tr>
						</table>
					</div>
				</asp:Panel>
				<div style="text-align: center; margin: 10px;">
					<asp:Button ID="ImageButtonRefresh" runat="server" Text="odśwież" AlternateText="Refresh" OnClick="ImageButtonRefresh_Click" />
				</div>
				<telerik:RadGrid ID="InventoryRadGrid" runat="server" 
                    OnNeedDataSource="InventoryRadGrid_OnNeedDataSource" 
                    OnItemCommand="InventoryRadGrid_OnItemClicked"
                    OnItemCreated="InventoryRadGrid_ItemCreated">
						<MasterTableView EnableViewState="true">
								<Columns>
										<telerik:GridBoundColumn HeaderText="Lp." UniqueName="RowNumber" DataField="RowNumber">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Indeks " UniqueName="IndexSIMPLE" DataField="IndexSIMPLE">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Nazwa" UniqueName="MaterialName" DataField="MaterialName">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="UnitName">
										</telerik:GridBoundColumn>
										<%--                    <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity">
										</telerik:GridBoundColumn>--%>
										<telerik:GridBoundColumn HeaderText="Czas Zliczenia" UniqueName="ObservationTime"
												DataField="ObservationTime">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Stan Początkowy" UniqueName="QuantityBefore"
												DataField="QuantityBefore">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Stan Inwentaryzacji" UniqueName="QuantityInv"
												DataField="QuantityInv">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Różnica" UniqueName="QuantityDiff" DataField="QuantityDiff">
										</telerik:GridBoundColumn>
										<%--                    <telerik:GridBoundColumn HeaderText="Cena Jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice">
										</telerik:GridBoundColumn>--%>
										<telerik:GridBoundColumn HeaderText="Cena jednostkowa" UniqueName="UnitPrice" DataField="UnitPrice">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="Assignation" DataField="Assignation">
										</telerik:GridBoundColumn>
										<telerik:GridBoundColumn HeaderText="IDPL" UniqueName="ProcessIdpl" DataField="ProcessIdpl">
										</telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="IDDPL" UniqueName="ProcessIddpl" DataField="ProcessIddpl">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridCheckBoxColumn HeaderText="Ekspor do SIMPLE (ZW)" UniqueName="ExportToSIMPLE" 
                                                DataField="ExportToSIMPLE" ItemStyle-HorizontalAlign="Center" ForceExtractValue="Always">
                                        </telerik:GridCheckBoxColumn>
										<telerik:GridButtonColumn ButtonType="LinkButton" HeaderButtonType="PushButton"
												UniqueName="DeleteRowColumn" Text="Usuń" CommandName="DeleteRow" ImageUrl="~/Images/delete.png">
										</telerik:GridButtonColumn>
								</Columns>
						</MasterTableView>
						<ClientSettings>
								<Selecting AllowRowSelect="False" />
						</ClientSettings>
				</telerik:RadGrid>
				<p align="center">
						<asp:Button ID="ButtonConfirm" runat="server" Text="ZATWIERDŹ DOKUMENT" OnClick="ButtonConfirm_Click" />
						<br />
						<asp:Button ID="ButtonNewDoc" runat="server" Text="NOWY DOKUMENT" OnClick="ButtonNewDoc_Click" />
				</p>
		</div>
		</form>


<script type="text/javascript">
    function OnClientRequesting(sender, args) {
        var context = args.get_context()
        context["Company"] = '<%= Session["CurrentCompanyId"] %>'
    }
</script>

</body>
</html>
