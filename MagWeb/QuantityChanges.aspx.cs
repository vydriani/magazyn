﻿namespace MagWeb
{
    using System;
    using System.Linq;
    using Mag.Domain;
    using Telerik.Web.UI;

    public partial class QuantityChanges : FeaturedSystemWebUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["tab_Reports"] = 4;
            MainPanel.Visible = SessionData.ClientData.SystemUserSecurityPrivileges.Any(p => p.SecurityPrivilegeId == 45);
        }

        protected void QuantityChangesRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var warehouseDocumentPositionQuantityChanges =
                  warehouseDs.WarehouseEntitiesTest.WarehouseDocumentPositionQuantityChangesReports;//.ToList();

                var items =
                  (
                    ExcessQuantityOnlyCheckBox.Checked
                      ? warehouseDocumentPositionQuantityChanges
                    // for some bizzare reason normal quantityDiff is in a negative but absolute value (ex. -1234) 
                    // but exessesive quantityDiff is a positive difference (ex. 1000 - 1234 = 234)
                        .Where(x => x.QuantityDiff > 0)
                      : warehouseDocumentPositionQuantityChanges
                  )
                  .ToList()
                  .Select
                  ((p, i) => new
                    /*
                    * Lp
                    * WarehouseDocumentId
                    * TimeRequested
                    * OperatorName
                    * MaterialName
                    * MaterialIndex
                    * Quantity
                    * QuantityDiff
                    * UnitName
                    * OrderNumber
                    * ProcessIdpl
                    * WarehouseSourceName
                    * WarehouseTargetName
                    * Potwierdzenie
                    * Anulowanie
                    * 
                    */
                    {
                        Lp = ++i,
                        WarehouseDocumentPositionId = p.WarehouseDocumentPositionId,
                        TimeRequested = p.TimeRequested,
                        OperatorName = p.OperatorName,
                        MaterialName = p.MaterialName,
                        MaterialIndex = p.MaterialIndex,
                        Quantity = p.Quantity,
                        QuantityDiff = p.QuantityDiff,
                        UnitName = p.UnitName,
                        OrderNumber = p.OrderNumber,
                        ProcessIdpl = p.ProcessIdpl,
                        WarehouseSourceName = p.WarehouseSourceName,
                        WarehouseTargetName = p.WarehouseTargetName,
                        ConfirmingUrl =
                          (p.ConfirmingToken != null
                                ? "http://testmag.w-b.pl/PlaceAnswer.aspx?" + p.ConfirmingToken
                                : string.Empty),
                        CancellingUrl =
                          (p.CancellingToken != null
                                ? "http://testmag.w-b.pl/PlaceAnswer.aspx?" + p.CancellingToken
                                : string.Empty),
                        ConfirmationText =
                          !p.TimeCancelled.HasValue
                            ?
                            (
                              p.ConfirmingToken != null
                                ?
                                (
                                  !p.TimeConfirmed.HasValue
                                    ? "<a href='PlaceAnswer.aspx?" + p.ConfirmingToken + "' target='_blank'>POTWIERDŹ</a>"
                                    : p.TimeConfirmed.ToString()
                                )
                                : p.TimeRequested.ToString()
                            )
                            : "-",
                        CancellationText =
                          (p.CancellingToken != null && p.ConfirmingToken != null && !p.TimeConfirmed.HasValue)
                            ?
                            (
                              !p.TimeCancelled.HasValue
                                ? "<a href='PlaceAnswer.aspx?" + p.CancellingToken + "' target='_blank'>ANULUJ</a>"
                                : p.TimeCancelled.ToString()
                            )
                            : "-",
                        // used in next query functions
                        RequiresAnswer = !p.TimeCancelled.HasValue && p.ConfirmingToken != null && !p.TimeConfirmed.HasValue,
                        TimeCancelled = p.TimeCancelled,
                        TimeConfirmed = p.TimeConfirmed
                    }
                  )
                  .GroupBy(x => x.RequiresAnswer)
                    //.Where(x => x.Key)
                  .OrderByDescending(xg => xg.Key)
                  .SelectMany(xg => xg.Key
                    ? xg.OrderBy(x => x.TimeRequested)
                    : xg.OrderByDescending(x => x.TimeConfirmed ?? x.TimeCancelled)
                  );

                QuantityChangesRadGrid.DataSource = items.ToList(); // ToList prevents IEnumerable.Reset() missing exception
            }
        }

        protected void ExcessQuantityOnlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            QuantityChangesRadGrid.Rebind();
        }
    }
}