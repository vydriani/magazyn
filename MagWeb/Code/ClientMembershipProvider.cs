using System;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
//using Client.Domain;
using Mag.Domain.Model;

namespace MagWeb
{
  //[DataContract]
  public sealed class ClientMembershipProvider : MembershipProvider
  {
    //public WarehouseEntities Domain;

    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
      if (config == null)
        throw new ArgumentNullException("config");

      if (String.IsNullOrEmpty(name) || name.Length == 0)
        name = "ClientMembershipProvider";

      if (String.IsNullOrEmpty(config["description"]))
      {
        config.Remove("description");
        config.Add("description", "Client Membership ASP.NET Provider");
      }


      base.Initialize(name, config);

      ApplicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
      maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
      passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
      minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
      minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
      passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
      requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));


      string tempFormat = String.IsNullOrEmpty(config["passwordFormat"]) ? "Hashed" : config["passwordFormat"];


      MembershipPasswordFormat membershipPasswordFormat;
      switch (tempFormat)
      {
        case "Hashed":
          membershipPasswordFormat = MembershipPasswordFormat.Hashed;
          break;
        case "Encrypted":
          membershipPasswordFormat = MembershipPasswordFormat.Encrypted;
          break;
        case "Clear":
          membershipPasswordFormat = MembershipPasswordFormat.Clear;
          break;
        default:
          throw new ProviderException("Password format not supported.");
      }


      // Get encryption and decryption key information from the configuration.
      var configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
      var machineKey = (MachineKeySection)configuration.GetSection("system.web/machineKey");

      if (machineKey.ValidationKey.Contains("AutoGenerate"))
        if (membershipPasswordFormat != MembershipPasswordFormat.Clear)
          throw new ProviderException("Hashed or Encrypted passwords are not supported with auto-generated keys.");
    }

    private string GetConfigValue(string configValue, string defaultValue)
    {
      if (String.IsNullOrEmpty(configValue))
        return defaultValue;

      return configValue;
    }


    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
      throw new NotImplementedException();
    }

    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
      return true;
    }

    public override string GetPassword(string username, string answer)
    {
      throw new NotImplementedException();
      //var user = Queryable.Where(Domain.SystemUsers, o => o.Login == username).FirstOrDefault();
      ////var user = Domain.GetAllSystemUsers().Where(o => o.Login == username).FirstOrDefault();
      //if (user != null) return user.Password;
      //return null;
    }

    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
      throw new NotImplementedException();
    }

    public override string ResetPassword(string username, string answer)
    {
      throw new NotImplementedException();
    }

    public override void UpdateUser(MembershipUser user)
    {
      throw new NotImplementedException();
    }

    public override bool ValidateUser(string username, string password)
    {
      bool isUserValidated = false;
      string connStr = "";
      try
      {
        System.Configuration.Configuration rootWebConfig =
                        System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/");
        System.Configuration.ConnectionStringSettings connString;
        if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
        {
          connString =
              rootWebConfig.ConnectionStrings.ConnectionStrings["ClientMembershipDatabase"];
          connStr = connString.ConnectionString;
        }

        using (SqlConnection conn = new SqlConnection(connStr))
        {
          conn.Open();
          SqlCommand cmd = new SqlCommand("dbo.ValidateUser", conn);
          cmd.CommandType = CommandType.StoredProcedure;
          //cmd.Connection = conn;

          SqlParameter loginParam = new SqlParameter("@Login", SqlDbType.VarChar, 128);
          SqlParameter passwordParam = new SqlParameter("@Password", SqlDbType.VarChar, 128);

          SqlParameter isValidRetValParam = new SqlParameter("@IsValid", SqlDbType.Bit);
          isValidRetValParam.Direction = ParameterDirection.ReturnValue;

          loginParam.Value = username;
          passwordParam.Value = password;
          isValidRetValParam.Value = null;

          cmd.Parameters.Add(loginParam);
          cmd.Parameters.Add(passwordParam);
          cmd.Parameters.Add(isValidRetValParam);

          cmd.ExecuteScalar();

          var isValid = isValidRetValParam.Value;
          isUserValidated = isValidRetValParam.Value != null && (int)isValidRetValParam.Value == 1;

          var context = HttpContext.Current;
          var loggedUserName = context.User != null && !string.IsNullOrEmpty(context.User.Identity.Name) ? context.User.Identity.Name : "GUEST";
          string logLabel = "";
          string logData = "Login:" + username;

          if (isUserValidated)
          {
            logLabel = "LoginOk";

            //if (HttpContext.Current.Session[SessionData.SessionKey_UserId] == null)
            Global.SetupSession(username);
          }
          else
          {
            logLabel = "LoginError";
            logData += ", Password:" + password;
          }

          var l = new Services.LoggingService();
          l.Log(loggedUserName, logLabel, logData + ", IP:" + context.Request.UserHostAddress, "MagWeb", "Info");
        }
      }
      catch (Exception ex)
      {
        var context = HttpContext.Current;
        var loggedUserName = context.User != null && !string.IsNullOrEmpty(context.User.Identity.Name) ? context.User.Identity.Name : "GUEST";

        var l = new Services.LoggingService();
        l.Log(loggedUserName, ex.Message, ex.StackTrace, "MagWeb", "Error");
      }
      return isUserValidated;
    }

    public override bool UnlockUser(string userName)
    {
      throw new NotImplementedException();
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
      throw new NotImplementedException();
      //var user = Queryable.Where(Domain.SystemUsers, o => o.Id == (int)providerUserKey).First().Login;
      //return GetUser(user, userIsOnline);
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
      throw new NotImplementedException();
      //var user = Queryable.Where(Domain.SystemUsers, o => o.Login == username).FirstOrDefault();
      //if (userIsOnline)
      //{
      //  user.LastAcitvityDate = DateTime.Now;
      //  //Helper.SaveChanges(Domain);
      //}
      //return ConvertSystemUserToMembershipUser(user);
    }

    public override string GetUserNameByEmail(string email)
    {
      throw new NotImplementedException();
    }

    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
      throw new NotImplementedException();
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotImplementedException();
      //return ConvertPersonsToMembershipUser(Domain.SystemUsers, pageIndex, pageSize, out totalRecords);
    }

    public override int GetNumberOfUsersOnline()
    {
      return 0;
    }

    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotImplementedException();
      //return ConvertPersonsToMembershipUser(Queryable.Where(Domain.SystemUsers, o => o.e_mail.Contains(usernameToMatch)), pageIndex, pageSize, out totalRecords);
    }

    /// <summary>
    /// Gets a collection of membership users where the e-mail address contains the specified e-mail address to match.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.Web.Security.MembershipUserCollection"/> collection that contains a page of <paramref name="pageSize"/><see cref="T:System.Web.Security.MembershipUser"/> objects beginning at the page specified by <paramref name="pageIndex"/>.
    /// </returns>
    /// <param name="emailToMatch">The e-mail address to search for.
    ///                 </param><param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex"/> is zero-based.
    ///                 </param><param name="pageSize">The size of the page of results to return.
    ///                 </param><param name="totalRecords">The total number of matched users.
    ///                 </param>
    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotImplementedException();
      //return ConvertPersonsToMembershipUser(Queryable.Where(Domain.SystemUsers, o => o.e_mail.Contains(emailToMatch)), pageIndex, pageSize, out totalRecords);
    }


    private static MembershipUserCollection ConvertPersonsToMembershipUser(IQueryable<SystemUser> systemUsers, int pageIndex, int pageSize, out int totalRecords)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Converts SystemUser object to MembershipUser object
    /// </summary>
    /// <param name="user">Input SystemUser</param>
    /// <returns>Converted MembershipUser</returns>
    private static MembershipUser ConvertSystemUserToMembershipUser(SystemUser user)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Indicates whether the membership provider is configured to allow users to retrieve their passwords.
    /// </summary>
    /// <returns>
    /// true if the membership provider is configured to support password retrieval; otherwise, false. The default is false.
    /// </returns>
    public override bool EnablePasswordRetrieval
    {
      get { return true; }
    }

    /// <summary>
    /// Indicates whether the membership provider is configured to allow users to reset their passwords.
    /// </summary>
    /// <returns>
    /// true if the membership provider supports password reset; otherwise, false. The default is true.
    /// </returns>
    public override bool EnablePasswordReset
    {
      get { return false; }
    }

    /// <summary>
    /// Gets a value indicating whether the membership provider is configured to require the user to answer a password question for password reset and retrieval.
    /// </summary>
    /// <returns>
    /// true if a password answer is required for password reset and retrieval; otherwise, false. The default is true.
    /// </returns>
    public override bool RequiresQuestionAndAnswer
    {
      get { return false; }
    }


    /// <summary>
    /// The name of the application using the custom membership provider.
    /// </summary>
    /// <returns>
    /// The name of the application using the custom membership provider.
    /// </returns>
    public override string ApplicationName
    {
      get;
      set;
    }

    private int maxInvalidPasswordAttempts;
    /// <summary>
    /// Gets the number of invalid password or password-answer attempts allowed before the membership user is locked out.
    /// </summary>
    /// <returns>
    /// The number of invalid password or password-answer attempts allowed before the membership user is locked out.
    /// </returns>
    public override int MaxInvalidPasswordAttempts
    {
      get { return maxInvalidPasswordAttempts; }
    }


    private int passwordAttemptWindow;
    /// <summary>
    /// Gets the number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before the membership user is locked out.
    /// </summary>
    /// <returns>
    /// The number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before the membership user is locked out.
    /// </returns>
    public override int PasswordAttemptWindow
    {
      get { return passwordAttemptWindow; }
    }

    private bool requiresUniqueEmail;
    /// <summary>
    /// Gets a value indicating whether the membership provider is configured to require a unique e-mail address for each user name.
    /// </summary>
    /// <returns>
    /// true if the membership provider requires a unique e-mail address; otherwise, false. The default is true.
    /// </returns>
    public override bool RequiresUniqueEmail
    {
      get { return requiresUniqueEmail; }
    }

    private const MembershipPasswordFormat PasswordFormatField = MembershipPasswordFormat.Clear;
    /// <summary>
    /// Gets a value indicating the format for storing passwords in the membership data store.
    /// </summary>
    /// <returns>
    /// One of the <see cref="T:System.Web.Security.MembershipPasswordFormat"/> values indicating the format for storing passwords in the data store.
    /// </returns>
    public override MembershipPasswordFormat PasswordFormat
    {
      get { return PasswordFormatField; }
    }

    private int minRequiredPasswordLength;
    /// <summary>
    /// Gets the minimum length required for a password.
    /// </summary>
    /// <returns>
    /// The minimum length required for a password. 
    /// </returns>
    public override int MinRequiredPasswordLength
    {
      get { return minRequiredPasswordLength; }
    }

    private int minRequiredNonAlphanumericCharacters;
    /// <summary>
    /// Gets the minimum number of special characters that must be present in a valid password.
    /// </summary>
    /// <returns>
    /// The minimum number of special characters that must be present in a valid password.
    /// </returns>
    public override int MinRequiredNonAlphanumericCharacters
    {
      get { return minRequiredNonAlphanumericCharacters; }
    }

    public string passwordStrengthRegularExpression;
    /// <summary>
    /// Gets the regular expression used to evaluate a password.
    /// </summary>
    /// <returns>
    /// A regular expression used to evaluate a password.
    /// </returns>
    public override string PasswordStrengthRegularExpression
    {
      get { return passwordStrengthRegularExpression; }
    }

    public bool UpdateSystemUser(SystemUser user)
    {
      throw new NotImplementedException();
    }
  }
}
