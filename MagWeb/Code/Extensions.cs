﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using Telerik.Web.UI;

namespace MagWeb.Extensions
{
    public static class Extensions
    {
        public static Nullable<T> GetCellValueAsNullable<T>(this GridDataItem gridItem, string columnName) where T : struct
        {
#if DEBUG
			Contract.Requires(gridItem != null);
			Contract.Requires(columnName != null);
#endif

            Nullable<T> result = new Nullable<T>();
            string value = gridItem[columnName].Text;

            result = ConvertValueToResult(result, value);

            return result;
        }

        public static T GetCellValueAs<T>(this GridDataItem gridItem, string columnName)
        {
#if DEBUG
			Contract.Requires(gridItem != null);
			Contract.Requires(columnName != null);
#endif
            T result = default(T);
            string value = gridItem[columnName].Text;

            result = ConvertValueToResult(result, value);

            return result;
        }

        public static Nullable<T> GetSelectedValueAsNullable<T>(this RadComboBox comboBox) where T : struct
        {
#if DEBUG
			Contract.Requires(comboBox != null);
#endif
            Nullable<T> result = new Nullable<T>();
            string value = comboBox.SelectedValue;

            result = ConvertValueToResult(result, value);

            return result;
        }

        public static T GetSelectedValueAs<T>(this RadComboBox comboBox)
        {
#if DEBUG
			Contract.Requires(comboBox != null);
#endif
            T result = default(T);
            string value = comboBox.SelectedValue;

            result = ConvertValueToResult(result, value);

            return result;
        }

        private static T ConvertValueToResult<T>(T result, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var type = typeof(T);
                var underlyingType = Nullable.GetUnderlyingType(type);
                TypeConverter converter = null;

                if (underlyingType != null)
                {
                    converter = TypeDescriptor.GetConverter(underlyingType);
                }
                else
                {
                    converter = TypeDescriptor.GetConverter(type);
                }
                // special case for "0"/"1" -> False/True i.e. string->int->bool conversion
                if (type == typeof(bool) || underlyingType == typeof(bool))
                {
                    return (T)(object)(!value.Equals("0"));
                }
                result = (T)converter.ConvertFrom(value);
            }
            return result;
        }

    }
}