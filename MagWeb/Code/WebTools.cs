using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Mag.Domain;
using System.Diagnostics.Contracts;

namespace MagWeb
{
    /// <summary>
    /// Class privides additional functionality to classes
    /// by MJZ
    /// </summary>
    public class WebTools
    {

        /// <summary>
        /// By MJZ
        /// </summary>
        /// <param name="input">String in which I will search</param>
        /// <param name="substring">String to search for</param>
        /// <param name="replacement">Replacement of substring</param>
        /// <param name="ignore">String to ignore</param>
        /// <returns></returns>
        public static string ReplaceIgnoringSpaces(string input, string substring, string replacement, string ignore)
        {
#if DEBUG
      Contract.Requires(input != null);
#endif
            string output_tmp = "";
            output_tmp = input;
            for (int i = 0; i < output_tmp.Length; i++)
            {
                for (int j = 1; j < output_tmp.Length - i - 1; j++)
                {
                    if (output_tmp.Substring(i, j).Replace(ignore, "") == substring)
                    {
                        output_tmp = output_tmp.Replace(input.Substring(i, j), replacement);
                    }
                }
            }
            return output_tmp;
        }

        //private static string[] GetAllSpaceCombinations(string[] inputArray)
        //{

        //}

        public static string MultiReplace(string input, string replacement, string[] strings2replace)
        {
#if DEBUG
      Contract.Requires(strings2replace != null);
#endif
            //StringBuilder output = new StringBuilder(input);

            string output = "";

            foreach (string str in strings2replace)
            {
                //output = output.Replace(str, replacement);
                output = Regex.Replace(input, str, "");
            }
            return output; //.ToString();
        }
        public static string[] TelerikTrialMessages =
						{
								//"Telerik.Web.UI 2011.1.315.40 trial version. Copyright telerik © 2002-2011. To remove this message, please purchase a developer version."
								//,
								//"Telerik.Web.UI 2011.1.315.40 trial version. Copyright telerik &copy; 2002-2011. To remove this message, please  <a href=\"#\" onclick=\"window.open('http://www.telerik.com/purchase'); return false\">purchase a developer version</a>."
								//,
								//"Telerik.Web.UI  2011.1.315.40  trial version. Copyright telerik &copy; 2002-2011. To remove this message, please   <a href=\"#\" onclick=\"window.open('http://www.telerik.com/purchase'); return false\">purchase a developer version</a>."
								//,
								//"Telerik.Web.UI    2011.1.315.40    trial version. Copyright telerik &copy; 2002-2011. To remove this message, please     <a href=\"#\" onclick=\"window.open('http://www.telerik.com/purchase'); return false\">purchase a developer version</a>."
								//,
								//"Telerik.Web.UI   2011.1.315.40   trial version. Copyright telerik &copy; 2002-2011. To remove this message, please    <a href=\"#\" onclick=\"window.open('http://www.telerik.com/purchase'); return false\">purchase a developer version</a>."
								//,
								//@"Telerik.Web.UI\s+2011.1.315.40\s+trial\s+version.\s+Copyright\s+telerik\s+&copy;\s+2002-2011.\s+To\s+remove\s+this\s+message,\s+please\s+<a (.*?)version</a>.",
								@"Telerik.Web.UI\s+2012.1.301.40\s+trial\s+version.\s+Copyright\s+telerik\s+&copy;\s+2002-2012.\s+To\s+remove\s+this\s+message,\s+please\s+<a (.*?)version</a>."
						};

        public static string GenerateAlertPopUp(string message)
        {
            return message;
        }

        public static void AlertMsgAdd(string alertMsg)
        {
#if DEBUG
      Contract.Assume(System.Web.HttpContext.Current != null);
#endif
            if (HttpContext.Current.Session[SessionData.SessionKey_AlertMsg] == null)
            {
                HttpContext.Current.Session[SessionData.SessionKey_AlertMsg] = new ArrayList();
            }
            ((ArrayList)HttpContext.Current.Session[SessionData.SessionKey_AlertMsg]).Add(alertMsg);
        }
        public static void AlertMsgAdd(Exception exception, int? userId)
        {
#if DEBUG
      Contract.Requires(exception != null);
      Contract.Assume(System.Web.HttpContext.Current != null);
#endif
            if (HttpContext.Current.Session[SessionData.SessionKey_AlertMsg] == null)
            {
                HttpContext.Current.Session[SessionData.SessionKey_AlertMsg] = new ArrayList();
            }

            string errorMsg;
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                errorMsg = warehouseDs.LogError(exception, userId);
            }

            ((ArrayList)HttpContext.Current.Session[SessionData.SessionKey_AlertMsg]).Add(errorMsg);
        }
        public static void AlertMsgAdd(string alertMsg, Exception exception = null, int? userId = null)
        {
            AlertMsgAdd(alertMsg);

            if (exception != null)
            {
                AlertMsgAdd(exception, userId);
            }
        }



        public static string AlertMsgRead()
        {
#if DEBUG
            Contract.Assume(System.Web.HttpContext.Current != null);
#endif
            StringBuilder sb = new StringBuilder();
            if (HttpContext.Current.Session[SessionData.SessionKey_AlertMsg] != null)
            {
                foreach (string alert in (ArrayList)HttpContext.Current.Session[SessionData.SessionKey_AlertMsg])
                {
                    sb.Append(alert + "<br />\n");
                }

                HttpContext.Current.Session[SessionData.SessionKey_AlertMsg] = null;
            }
            return sb.ToString();
        }


        public static bool AlertMsgIsAlert()
        {
#if DEBUG
            Contract.Assume(System.Web.HttpContext.Current != null);
#endif
            ArrayList alerts = (ArrayList)HttpContext.Current.Session[SessionData.SessionKey_AlertMsg];
            return (alerts == null ? false : (alerts.Count > 0));
        }



        public static bool IntToBool(int val)
        {
            return val > 0;
        }

        public static int BoolToInt(bool val)
        {
            return (val ? 1 : 0);
        }


        public static string GenerateRandomString(int length)
        {
            string letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int lettersLen = letters.Length;


            string key = "";
            StringBuilder sb = new StringBuilder();

            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                sb.Append(letters[random.Next() % lettersLen]);
            }
            key = sb.ToString();

            return key;
        }

        public static byte[] ComputeStringShaHash(string str)
        {
#if DEBUG
      Contract.Requires(str != null);
#endif
            int strLen = str.Length;
            byte[] data = new byte[strLen];
            byte[] result;


            for (int i = 0; i < strLen; i++)
            {
                data[i] = (byte)str[i];
            }


            SHA1 sha = new SHA1CryptoServiceProvider();

            result = sha.ComputeHash(data);

            return result;
        }
    }
}
