﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagWeb.Helpers
{
	public class SelectedWarehouse
	{
		public int CompanyID { get; set; }
		public string CompanyName { get; set; }

		public int WarehouseID { get; set; }
		public string WarehouseName { get; set; }
	}
}