﻿namespace MagWeb
{
    using System;
    using System.Data;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    public class FeaturedSystemWebUIPage : Page
    {
        protected void PermissionDenied()
        {
            Response.Redirect("~/AccessDenied.aspx?source=" + Uri.EscapeUriString(Request.RawUrl));
        }

        public object senderSession
        {
            get { return Session["senderSession"]; }
            set { Session["senderSession"] = value; }
        }
    }
}
