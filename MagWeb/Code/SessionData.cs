using System;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using Mag.Domain.SecurityData;
using System.Diagnostics.Contracts;

namespace MagWeb
{
    public class SessionData
    {
        public const string SessionKey_UserId = "UserId";
        public const string SessionKey_UserFirstNameLastName = "UserFirstNameLastName";
        public const string SessionKey_IsOrderRequired = "IsOrderRequired";
        public const string SessionKey_CurrentWarehouseId = "CurrentWarehouseId";
        //public const string SessionKey_CurrentWarehouseName = "CurrentWarehouseName";
        public const string SessionKey_CurrentCompanyId = "CurrentCompanyId";
        //public const string SessionKey_CurrentCompanyName = "CurrentCompanyName";
        public const string SessionKey_LoggedUserHasAdminPrivilegesForSecurity = "LoggedUserHasAdminPrivilegesForSecurity";
        public const string SessionKey_LoggedUserHasAdminPrivilegesForConfig = "LoggedUserHasAdminPrivilegesForConfig";
        public const string SessionKey_LoggedUserHasAccessToWarehouse = "LoggedUserHasAccessToWarehouse";
        public const string SessionKey_LoggedUserHasAdminPrivileges = "LoggedUserHasAdminPrivileges";
        public const string SessionKey_Message = "Message";
        public const string SessionKey_AlertMsg = "AlertMsg";

        const string sessionKey_WarehouseClientData = "WarehouseClientData";

        public static ClientSecurityData ClientData
        {
            get
            {
#if DEBUG
        Contract.Assume(System.Web.HttpContext.Current != null);
#endif
                if (HttpContext.Current.Session[sessionKey_WarehouseClientData] == null)
                {
                    HttpContext.Current.Session[sessionKey_WarehouseClientData] = new ClientSecurityData();
                }
                return (ClientSecurityData)HttpContext.Current.Session[sessionKey_WarehouseClientData];
            }
        }
        public static long GetSessionSizeInBytes()
        {
            long totalSizeInBytes = 0;
            BinaryFormatter b = new BinaryFormatter();
#if DEBUG
      Contract.Assume(System.Web.HttpContext.Current != null);
#endif

            foreach (var item in HttpContext.Current.Session)
            {
                totalSizeInBytes += ClientSecurityData.GetObjectSizeInBytes(item, b);
            }
            return totalSizeInBytes;
        }

        [Conditional("DEBUG")]
        public static void SessionIdToDebug()
        {
#if DEBUG
      Contract.Assume(System.Web.HttpContext.Current != null);
#endif
            var sessionIdByUserAgent = string.Format("SessionID={0} requested from {1}", HttpContext.Current.Session.SessionID, HttpContext.Current.Request.UserAgent);
            System.Diagnostics.Debug.WriteLine(sessionIdByUserAgent);
        }

        /// <summary>
        /// Tests if specified SessionKey is present.
        /// If so then prints it out to the Debug window
        /// </summary>
        /// <param name="sessionKey">sessionKey</param>
        [Conditional("DEBUG")]
        public static void TestSessionFor(string sessionKey)
        {
            TestSessionFor<object, string>(sessionKey, o => o.ToString());
        }

        /// <summary>
        /// Tests if specified SessionKey is present.
        /// If so then performs a specified function on it and prints out the result to the Debug window
        /// </summary>
        /// <typeparam name="T">Type of object specified by SessionKey</typeparam>
        /// <typeparam name="V">Type of return value of typeFunction</typeparam>
        /// <param name="sessionKey">sessionKey</param>
        /// <param name="typeFunction">function to be performed on the object</param>
        [Conditional("DEBUG")]
        public static void TestSessionFor<T, V>(string sessionKey, Func<T, V> typeFunction)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Session[\"{0}\"]", sessionKey);
#if DEBUG
      Contract.Assume(System.Web.HttpContext.Current != null);
#endif
            var obj = HttpContext.Current.Session[sessionKey];
            if (obj == null)
            {
                sb.Append(" is empty");
            }
            else
            {
                if (obj is T)
                {
                    var tObj = (T)obj;
                    sb.AppendFormat(" => {0}", typeFunction(tObj));
                }
                else
                {
                    sb.AppendFormat(" not of type {0}", typeof(T).FullName);
                }
            }
            Debug.WriteLine(sb.ToString());
        }

    }
}