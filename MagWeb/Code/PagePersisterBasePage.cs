﻿using System.Web.UI;

namespace MagWeb
{
    public class PagePersisterBasePage : Page
    {
        public PagePersisterBasePage()
        {
        }
        protected override PageStatePersister PageStatePersister
        {
            get
            {
                return new SessionPageStatePersister(this);
            }
        }
    } 


}