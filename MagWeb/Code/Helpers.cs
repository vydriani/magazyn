﻿using System;
using System.IO;
using System.Web;
using NLog;
using System.Diagnostics.Contracts;

namespace MagWeb.Helpers
{
    /// <summary>
    /// Kalsa logujaca do pliku wywolywana przez 
    /// ClientWeb.Services.Logging.Service
    /// </summary>
    public class NLogger
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private void WriteEx(string ex)
        {
            string path = HttpContext.Current.Server.MapPath("/logClient");

            using (StreamWriter fileStream = new StreamWriter(path + "/LOGERROR.txt", true))
            {
                fileStream.WriteLine(ex + "#" + path + "/LOGERROR.txt");
            }
        }

        public string Log(string user, string error, string source, string comment, string logLevel)
        {
#if DEBUG
      Contract.Requires(source != null);
      Contract.Requires(error != null);
      Contract.Requires(comment != null);
#endif
            try
            {
                try
                {
                    source = source.Replace(System.Environment.NewLine, string.Empty);
                    error = error.Replace(System.Environment.NewLine, string.Empty);
                    comment = comment.Replace(System.Environment.NewLine, string.Empty);

                    //string[] s = source.Split('\x13');
                    //string[] ss = source.Split('\x10');
                    //string[] sss = source.Split(')');
                    //source.Replace('\x13', ' ');
                    //source.Replace('\x10', '\x9');
                }
                catch (Exception e)
                {
                    WriteEx(e.Message);
                }


                LogLevel _logLevel;
                switch (logLevel)
                {
                    case "Debug":
                        _logLevel = LogLevel.Debug;
                        break;

                    case "Info":
                        _logLevel = LogLevel.Info;
                        break;

                    case "Warn":
                        _logLevel = LogLevel.Warn;
                        break;

                    case "Error":
                        _logLevel = LogLevel.Error;
                        break;
                    default:
                        _logLevel = LogLevel.Error;
                        break;
                }
                string dir = Directory.GetCurrentDirectory();
                logger.Log(_logLevel, "{0}|{1}|{2}|{3}", user, error, source, comment);

                return "LOG OK";
            }
            catch (Exception e)
            {
                WriteEx(e.Message);
                return e.Message;
            }
        }

    }
}