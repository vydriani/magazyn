﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb
{
	public partial class ManageUsers : FeaturedSystemWebUIPage
	{

		private const int WAREHOUSE_ID_IN_GRID = 2;
		private const int TARGET_WAREHOUSE_ID_IN_GRID = 2;
		private const int PERMISSION_ID_IN_GRID = 2;
		private const int DOCUMENT_TYPE_ID_IN_GRID = 3;

		private const int USER_ID_IN_GRID = 2;
		private const int USER_NAME_IN_GRID = 3;
		private const int PRIVILEGE_ID_IN_GRID = 2;

		private const int PRIVILEGE_TO_READ_ID_IN_GRID = 2;
		private const int WAREHOUSE_TO_READ_ID_IN_GRID = 3;

		private const int MEMBERSHIP_ID_IN_GRID = 2;
		private const int GROUP_ID_IN_GRID = 3;
		private const int GROUP_NAME_IN_GRID = 4;


		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					CompanyRCB.DataSource = from w in warehouseDs.GetAllCompanies()
																	select new { Value = w.Id, Text = w.Name };
					CompanyRCB.DataTextField = "Text";
					CompanyRCB.DataValueField = "Value";
					CompanyRCB.DataBind();
					CompanyRCB.Items.Insert(0, new RadComboBoxItem("(wybierz firmę)", "-1"));
				}
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (this.UsersGrid.SelectedItems.Count == 1)
			{
				//USER_NAME_IN_GRID
				//int userId = int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);
				this.Label_SystemUser.Text = "Wybrany użytkownik: " + this.UsersGrid.SelectedItems[0].Cells[USER_NAME_IN_GRID].Text + " (" + this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text + ")";


				using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
				{
					int systemUserId = int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);
					var sus = warehouseDS.GetSystemUserSetting("SessionTime", systemUserId);
					this.TextBox_SessionTimeLimit.Text = (sus == null ? "" : sus.SettingValue);

					this.TextBox_SessionTimeLimit.Enabled = true;
					this.Button_SetSessionTimeLimit.Enabled = true;


					//this.Label_SystemUser.Text = "!!!!!!!!" + Context.User.Identity.Name;
				}


			}
			else
			{
				this.Label_SystemUser.Text = "Nie wybrano użytkownika";
				this.TextBox_SessionTimeLimit.Text = "";
				this.TextBox_SessionTimeLimit.Enabled = false;
				this.Button_SetSessionTimeLimit.Enabled = false;
			}
		}



		protected void UsersGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
			{
				var usersCandidatesToConsider = warehouseDs.GetAllSystemUsers();
				var usersCandidatesToConsiderOrdered = usersCandidatesToConsider.OrderBy(p => p.Login);


				List<Mag.Domain.Model.SystemUser> usersCandidatesToPresent = new List<SystemUser>();


				foreach (var user in usersCandidatesToConsiderOrdered)
				{
					Mag.Domain.Model.SystemUser waitingForUserToPresent = new Mag.Domain.Model.SystemUser();
					Mag.Domain.Model.Person waitingForUserToPresent_Person = new Mag.Domain.Model.Person();
					waitingForUserToPresent.Person = waitingForUserToPresent_Person;

					waitingForUserToPresent.Id = user.Id;
					waitingForUserToPresent.Login = user.Login;
					waitingForUserToPresent.Person.FirstName = user.Person.FirstName;
					waitingForUserToPresent.Person.LastName = user.Person.LastName;

					usersCandidatesToPresent.Add(waitingForUserToPresent);
				}


				var usersCandidatesToPresentMaterialized = usersCandidatesToPresent.ToArray();

				var users = from p in usersCandidatesToPresentMaterialized
										select
												new
												{
													UserName = p.Login,
													FullName = p.Person.LastName + " " + p.Person.FirstName,
													UserID = p.Id
												};

				UsersGrid.DataSource = users.OrderBy(p => p.UserName);
			}
		}

		protected void UsersGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == "Select")
			{
				WarehousesGrid.SelectedIndexes.Clear();
				e.Item.Selected = true;

				WarehouseToReadGrid.Rebind();
				WarehousesGrid.Rebind();
				TargetWarehousesGrid.Rebind();
				DocumentTypeGrid.Rebind();

				UserGroupMembershipsGrid.Rebind();
			}
		}



		protected void CompanyRCB_SelectedIndexChanged(object sender, EventArgs e)
		{
			WarehouseToReadGrid.Rebind();
			WarehousesGrid.Rebind();
			TargetWarehousesGrid.Rebind();
			DocumentTypeGrid.Rebind();
		}

		protected void WarehousesToReadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			if (this.UsersGrid.SelectedItems.Count == 1)
			{
				int userId = int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);

				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					int company = int.Parse(CompanyRCB.SelectedValue); // Company selection from combo box

					var warehouses = warehouseDs.GetAllWarehouses().Where(p => p.Company.Id == company);

					List<WarehousesAndSecurityPrivileges> warehousesAndSecurityPrivileges = new List<WarehousesAndSecurityPrivileges>();

					foreach (Mag.Domain.Model.Warehouse warehouse in warehouses)
					{
						WarehousesAndSecurityPrivileges warehouseAndSecurityPrivileges = new WarehousesAndSecurityPrivileges();

						warehouseAndSecurityPrivileges.IdPrivilege = warehouseDs.GetWarehousePrivilegeForRead(warehouse.Id, userId);
						warehouseAndSecurityPrivileges.IdWarehouse = warehouse.Id;
						warehouseAndSecurityPrivileges.ShortName = warehouse.ShortName;
						warehouseAndSecurityPrivileges.Name = warehouse.Name;

						warehousesAndSecurityPrivileges.Add(warehouseAndSecurityPrivileges);
					}


					//var warehousesMaterialized = warehouses.ToArray();

					var warehousesWithPrivileges = from p in warehousesAndSecurityPrivileges
																				 select
																						 new
																						 {
																							 PrivilegeID = p.IdPrivilege,
																							 WarehouseID = p.IdWarehouse,
																							 WarehouseShortName = p.ShortName,
																							 WarehouseName = p.Name,
																							 GrantedToUser = (p.IdPrivilege != null ? true : false)
																						 };

					this.WarehouseToReadGrid.DataSource = warehousesWithPrivileges;
				}
			}
		}

		protected void WarehousesToReadGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == "Select")
			{
				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					string warehouseToReadIDStr = e.Item.Cells[WAREHOUSE_TO_READ_ID_IN_GRID].Text;

					string test1, test2, test3;
					test1 = e.Item.Cells[1].Text;
					test2 = e.Item.Cells[2].Text;
					test3 = e.Item.Cells[3].Text;

					int warehouseId = int.Parse(warehouseToReadIDStr);
					int systemUserId = int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);

					TableCellCollection tcl = e.Item.Cells;
					int privilegeId = -1;
					string privilegeIdStr = e.Item.Cells[PRIVILEGE_TO_READ_ID_IN_GRID].Text;

					if (privilegeIdStr != "&nbsp;" && privilegeIdStr != "")
					{
						privilegeId = int.Parse(e.Item.Cells[PRIVILEGE_TO_READ_ID_IN_GRID].Text);
					}

					if (privilegeId == -1 || !warehouseDs.SecurityCheckUserPriviledgeForReadById(privilegeId))
					{
						// Adding privilege
						warehouseDs.SecurityAddUserPriviledgeForRead(warehouseId, systemUserId);
					}
					else
					{
						// Removing privilege
						warehouseDs.SecurityRemoveUserPriviledgeForRead(privilegeId);
					}
				}


				WarehousesGrid.SelectedIndexes.Clear();
				e.Item.Selected = true;
				WarehouseToReadGrid.Rebind();
			}
		}

		protected void WarehousesGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			if (this.UsersGrid.SelectedItems.Count == 1 && int.Parse(this.CompanyRCB.SelectedValue) > 0)
			{
				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					int company = int.Parse(CompanyRCB.SelectedValue); // Company selection from combo box

					var warehousesCandidatesToConsider = warehouseDs.GetAllWarehouses(company, true, false);

					var warehousesCandidatesToConsiderOrdered = warehousesCandidatesToConsider.OrderBy(p => p.Name);

					List<Mag.Domain.Model.Warehouse> warehousesCandidatesToPresent = new List<Mag.Domain.Model.Warehouse>();

					foreach (var warehouse in warehousesCandidatesToConsiderOrdered)
					{
						Mag.Domain.Model.Warehouse waitingForWarehouseToPresent = new Mag.Domain.Model.Warehouse();

						waitingForWarehouseToPresent.Id = warehouse.Id;
						waitingForWarehouseToPresent.Name = warehouse.Name;
						waitingForWarehouseToPresent.ShortName = warehouse.ShortName;

						warehousesCandidatesToPresent.Add(waitingForWarehouseToPresent);

					}

					if (company != -1)
					{
						Mag.Domain.Model.Warehouse warehouseNull = new Mag.Domain.Model.Warehouse();
						warehouseNull.Id = -1;
						warehouseNull.Name = "(nieokreślony magazyn)";
						warehouseNull.ShortName = "(nieokreślony magazyn)";
						warehousesCandidatesToPresent.Insert(0, warehouseNull);
					}

					var warehousesCandidatesToPresentMaterialized = warehousesCandidatesToPresent.ToArray();

					var warehouses = from p in warehousesCandidatesToPresentMaterialized
													 select
															 new
															 {
																 WarehouseName = p.Name,
																 WarehouseShortName = p.ShortName,
																 WarehouseID = p.Id
															 };

					WarehousesGrid.DataSource = warehouses;
				}
			}
		}

		protected void WarehousesGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == "Select")
			{
				WarehousesGrid.SelectedIndexes.Clear();
				e.Item.Selected = true;
				DocumentTypeGrid.Rebind();
			}
		}



		protected void TargetWarehousesGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			if (this.UsersGrid.SelectedItems.Count == 1 && int.Parse(this.CompanyRCB.SelectedValue) > 0)
			{
				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					int company = int.Parse(CompanyRCB.SelectedValue);

					var warehouses = warehouseDs.GetAllWarehouses(company, true, false).ToList();

					if (company != -1)
					{
						Mag.Domain.Model.Warehouse warehouseNull = new Mag.Domain.Model.Warehouse();
						warehouseNull.Id = -1;
						warehouseNull.Name = "(nieokreślony magazyn)";
						warehouseNull.ShortName = "(nieokreślony magazyn)";
						warehouses.Insert(0, warehouseNull);
					}

					TargetWarehousesGrid.DataSource = warehouses;
				}
			}
		}

		protected void TargetWarehousesGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == "Select")
			{
				TargetWarehousesGrid.SelectedIndexes.Clear();
				e.Item.Selected = true;
				DocumentTypeGrid.Rebind();
			}
		}

		// ATTENTION: When some permission gets deleted, it will not be longer listed here, so we need to write a special code to add it
		// and probably fill with i.e. red color
		protected void DocumentTypeGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			if (this.UsersGrid.SelectedItems.Count == 1 && int.Parse(this.CompanyRCB.SelectedValue) > 0)
			{
				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					int? sourceWarehouseId = -1;
					if (WarehousesGrid.SelectedItems.Count == 1)
					{
						sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
						sourceWarehouseId = sourceWarehouseId != -1 ? sourceWarehouseId : null;
					}

					int? targetWarehouseId = -1;
					if (TargetWarehousesGrid.SelectedItems.Count == 1)
					{
						targetWarehouseId = int.Parse(TargetWarehousesGrid.SelectedItems[0].Cells[TARGET_WAREHOUSE_ID_IN_GRID].Text);
						targetWarehouseId = targetWarehouseId != -1 ? targetWarehouseId : null;
					}

					int userId = int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);
					int? userId_nullable = userId;


					var permissionsCandidates = warehouseDs.GetJoinedWarehousePermissions((sourceWarehouseId > 0 ? sourceWarehouseId : null), (targetWarehouseId > 0 ? targetWarehouseId : null)); //.GetWarehousePermissions(); //.GetWarehousePermissionsAndSecurityPriviledges(sourceWarehouseId, targetWarehouseId, userId);




					List<WarehousePermissionVirtual> permissionsCandidatesMaterialized = new List<WarehousePermissionVirtual>();
					foreach (var permission in permissionsCandidates)
					{
						if (((permission.WarehouseId == null && sourceWarehouseId == null) || (permission.WarehouseId != null && permission.WarehouseId == sourceWarehouseId))
							&& ((permission.Warehouse1Id == null && targetWarehouseId == null) || (permission.Warehouse1Id != null && permission.Warehouse1Id == targetWarehouseId)))
						{
							if (!permissionsCandidatesMaterialized.Any(p =>
									p.WarehouseDocumentType.Id == permission.WarehouseDocumentType.Id
									&&
									p.Warehouse.Id == permission.Warehouse.Id
									&&
									p.Warehouse1.Id == permission.Warehouse1.Id
									))
							{
								permissionsCandidatesMaterialized.Add(permission);
							}
						}
					}

					List<WarehousePermissionsAndSecurityPriviledges> permissionsAndPrivilegesCandidatesMaterializedSelected = new List<WarehousePermissionsAndSecurityPriviledges>();

					foreach (var permission in permissionsCandidatesMaterialized)
					{
						WarehousePermissionsAndSecurityPriviledges permissionAndSecurityPrivilege = new WarehousePermissionsAndSecurityPriviledges();

						permissionAndSecurityPrivilege.FromWarehouseId = null;
						permissionAndSecurityPrivilege.ToWarehouseId = null;

						if (permission.Warehouse != null)
						{
							permissionAndSecurityPrivilege.FromWarehouseId = permission.Warehouse.Id;
						}

						if (permission.Warehouse1 != null)
						{
							permissionAndSecurityPrivilege.ToWarehouseId = permission.Warehouse1.Id;
						}

						permissionAndSecurityPrivilege.FromWarehouseShortName = (permission.Warehouse != null ? permission.Warehouse.ShortName : "");
						permissionAndSecurityPrivilege.ToWarehouseShortName = (permission.Warehouse1 != null ? permission.Warehouse1.ShortName : "");

						permissionAndSecurityPrivilege.OperationId = permission.WarehouseDocumentType.Id;

						//permissionAndSecurityPrivilege.IdPermission = permission.Id;

						permissionAndSecurityPrivilege.IdType = permission.WarehouseDocumentType.Id;
						permissionAndSecurityPrivilege.Type = permission.WarehouseDocumentType.Type;
						permissionAndSecurityPrivilege.ShortType = permission.WarehouseDocumentType.ShortType;

						permissionAndSecurityPrivilege.SystemUserId = userId;
						permissionAndSecurityPrivilege.IdPrivilege = warehouseDs.GetWarehousePrivilegeForExecutionId(sourceWarehouseId, targetWarehouseId, permission.WarehouseDocumentType.Id, userId);

						permissionsAndPrivilegesCandidatesMaterializedSelected.Add(permissionAndSecurityPrivilege);

					}



					var permissions =
							from p in permissionsAndPrivilegesCandidatesMaterializedSelected
							select new
							{
								SystemUserId = p.SystemUserId,

								PrivilegeId = p.IdPrivilege,
								TypeId = p.IdType,
								ShortType = p.ShortType,
								Type = p.Type,
								SourceWarehouse = p.FromWarehouseShortName,
								TargetWarehouse = p.ToWarehouseShortName,
								GrantedToUser = p.IdPrivilege != null

							};
					DocumentTypeGrid.DataSource = permissions.ToArray(); // OrderBy(p => p.TypeId).ToArray();
				}
			}
		}

		protected void DocumentTypeGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == "Select")
			{

				this.DocumentTypeGrid.SelectedIndexes.Clear();
				e.Item.Selected = true;

				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					int? sourceWarehouseId = -1;
					if (WarehousesGrid.SelectedItems.Count == 1)
					{
						sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
						sourceWarehouseId = sourceWarehouseId != -1 ? sourceWarehouseId : null;
					}

					int? targetWarehouseId = -1;
					if (TargetWarehousesGrid.SelectedItems.Count == 1)
					{
						targetWarehouseId = int.Parse(TargetWarehousesGrid.SelectedItems[0].Cells[TARGET_WAREHOUSE_ID_IN_GRID].Text);
						targetWarehouseId = targetWarehouseId != -1 ? targetWarehouseId : null;
					}


					int privilegeId = -1;
					string privilegeIdStr = e.Item.Cells[PRIVILEGE_ID_IN_GRID].Text;

					if (privilegeIdStr != "&nbsp;" && privilegeIdStr != "")
					{
						privilegeId = int.Parse(e.Item.Cells[PRIVILEGE_ID_IN_GRID].Text);
					}

					if (privilegeId == -1 || !warehouseDs.SecurityCheckUserPriviledgeForExecutionById(privilegeId))
					{
						// Adding privilege
						warehouseDs.SecurityAddUserPriviledgeForExecution(sourceWarehouseId, targetWarehouseId, int.Parse(e.Item.Cells[DOCUMENT_TYPE_ID_IN_GRID].Text), int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text));
					}
					else
					{
						// Removing privilege
						warehouseDs.SecurityRemoveUserPriviledgeForExecution(privilegeId);
					}
				}
				DocumentTypeGrid.Rebind();
			}
		}

		protected void Button_SetSessionTimeLimit_Click(object sender, EventArgs e)
		{
			if (IsValid)
			{
				if (this.UsersGrid.SelectedItems.Count == 1)
				{
					using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
					{
						int systemUserId = int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);
						warehouseDS.SetSystemUserSetting("SessionTime", this.TextBox_SessionTimeLimit.Text, systemUserId);
					}
				}
			}
		}

		protected void SessionTimeValidator_ServerValidate(object source, ServerValidateEventArgs e)
		{
			int value = -1;
			try
			{
				value = int.Parse(this.TextBox_SessionTimeLimit.Text);
			}
			catch
			{
				e.IsValid = false;
				return;
			}
			if (value < 1 || value > 1440) // THESE PARAMETERS ALSO SHOULD BE GOT FROM CONFIG FILE
			{
				e.IsValid = false;
				return;
			}
			e.IsValid = true;
		}









		protected void UserGroupMembershipsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
			{
				int userId = -1;
				if (UsersGrid.SelectedItems.Count == 1)
				{
					userId = int.Parse(UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);
				}
				if (userId > -1)
				{
					var groups = from g in warehouseDs.GetAllSystemGroups()
											 select g;

					List<GroupAndMembership> groupsAndMemberships = new List<GroupAndMembership>();

					foreach (var group in groups)
					{
						GroupAndMembership membership = new GroupAndMembership();

						SystemUserGroupMembership _membership = warehouseDs.GetSystemUserGroupMembership(userId, group.Id);

						if (_membership != null)
						{
							membership.MembershipID = _membership.Id;
						}
						else
						{
							membership.MembershipID = null;
						}

						//membership.PermissionID = (_membership != null ? _membership.Id : null);
						membership.GroupID = group.Id;
						membership.GroupName = group.Name;
						membership.IsMember = _membership != null;

						groupsAndMemberships.Add(membership);
					}

					UserGroupMembershipsGrid.DataSource = groupsAndMemberships.OrderBy(m => m.GroupName).ToArray();
				}
			}
		}
		protected void UserGroupMembershipsGrid_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == "Select")
			{

				this.DocumentTypeGrid.SelectedIndexes.Clear();
				e.Item.Selected = true;

				using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
				{
					int userId = -1;
					if (UsersGrid.SelectedItems.Count == 1)
					{
						userId = int.Parse(UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text);
					}
					int membershipId = -1;
					string membershipIdStr = e.Item.Cells[MEMBERSHIP_ID_IN_GRID].Text;

					if (membershipIdStr != "&nbsp;" && membershipIdStr != "")
					{
						membershipId = int.Parse(e.Item.Cells[MEMBERSHIP_ID_IN_GRID].Text);
					}

					int groupId = -1;
					groupId = int.Parse(e.Item.Cells[GROUP_ID_IN_GRID].Text);

					if (membershipId == -1 || warehouseDs.GetSystemUserGroupMembership(membershipId) == null)
					{
						// Adding membership
						warehouseDs.AddSystemUserGroupMembership(userId, groupId);
						//warehouseDs.SecurityAddUserPriviledgeForExecution(sourceWarehouseId, targetWarehouseId, int.Parse(e.Item.Cells[DOCUMENT_TYPE_ID_IN_GRID].Text), int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text));
					}
					else
					{
						// Removing membership
						warehouseDs.RemoveSystemUserGroupMembership(membershipId);
					}
				}
				UserGroupMembershipsGrid.Rebind();
			}
		}








	}
}
