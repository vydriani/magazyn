using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
//using Client.Domain.Logging;
using Mag.Domain.Model;
using Telerik.Web.UI;
using System.Diagnostics.Contracts;

namespace MagWeb
{
  public partial class WarehouseStacks2 : FeaturedSystemWebUIPage
  {
    public bool InventoryMode = false;
    //private PerformanceDebugger perfDebug = new PerformanceDebugger();

    private List<WarehouseInventoryPosition> warehouseInventoryPositions;

#if DEBUG

    public override System.Web.SessionState.HttpSessionState Session
    {
      get
      {
        SessionData.TestSessionFor<List<WarehouseInventoryPosition>, int>("INVPositions", wip => wip.Count);
        return base.Session;
      }
    }
#endif

    protected void AdjustStacksClick(object sender, EventArgs e)
    {
      int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

      GridDataItem item = (GridDataItem)StackGrid.SelectedItems[0];//get selected row

      string materialIndex = item["IndexSIMPLE"].Text;
      string zoneName = item["ShelfName"].Text;
      string orderNumber = item["OrderNumber"].Text;
      string damageStatus = item["DamageStatus"].Text;

      if (zoneName == "&nbsp;")
        zoneName = "";

      if (orderNumber == "&nbsp;")
        orderNumber = "";

      Material material;
      Zone zone;
      Order order;

      using (var warehouseDs = new WarehouseDS())
      {
        material = warehouseDs.GetMaterialByIndex(materialIndex.Trim(), companyId);
        zone = !string.IsNullOrEmpty(zoneName) ? warehouseDs.GetZoneByName(zoneName) : null;
        order = !string.IsNullOrEmpty(orderNumber) ? warehouseDs.GetOrderByNumber(orderNumber) : null;
      }
      int? warehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];

      Response.Redirect("BalanceSheet.aspx?o=x&material=" + material.Id + "&zone=" + (zone != null ? zone.Id : 0) + "&order=" + (order != null ? order.Id : 0) + "&status=" + damageStatus); // &w=" + warehouseId.Value + "

      //var x = 1;
    }

    protected void ButtonCancelClick(object sender, EventArgs e)
    {
      var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
      GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
      detailTableView.Rebind();
      HideRelocationPanel();
      HideFreeMaterialPanel();
      HideInventoryMaterialPanel();
    }

    protected void ButtonChangeLocalizationExecuteClick(object sender, EventArgs e)
    {
      Page.Validate("RelocateMaterialGroup");

      if (IsValid)
      {
        var maxQuantity = double.Parse(HiddenField_QuickOp_MaxQuantity.Value);
        //var enteredQuantity = double.Parse(RelocationQuantityTextBox.Text);

        int warehouseId = int.Parse(HiddenField_QuickOp_WarehouseId.Value);
        int materialId = int.Parse(HiddenField_QuickOp_MaterialId.Value);
        int orderId = int.Parse(HiddenField_QuickOp_OrderId.Value);
        int departmentId = int.Parse(HiddenField_QuickOp_DepartmentId.Value);
        int processIdpl = int.Parse(HiddenField_QuickOp_ProcessIdpl.Value);
        int shelfId = int.Parse(HiddenField_QuickOp_ShelfSrcId.Value);
        double newquantity = double.Parse(RelocationQuantityTextBox.Text.Replace(".", ","));

        if (departmentId > 0)
        {
          WebTools.AlertMsgAdd("Nie można (jeszcze) operować na materiale przypisanym do działu!");
          HideRelocationPanel();
          return;
        }

        if (true) //(newquantity <= maxQuantity)
        {
          int userId = 0;
          int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId);

          using (var warehouseDs = new WarehouseDS())
          {
            Zone zone = warehouseDs.GetShelfByName(RelocationTargetZoneRCB.Text);//.GetShelfByNameDataOnly(RelocationTargetZoneRCB.Text);
            string result = "";
            if (!string.IsNullOrEmpty(RelocationTargetZoneRCB.Text) && zone != null)
            {
              result = warehouseDs.RelocateMaterialInWarehouse(
                  warehouseId,
                  materialId,
                  orderId,
                  processIdpl != 0 && processIdpl < 2000000 ? processIdpl : (int?)null,
                  shelfId,
                  zone.Id,
                  newquantity,
                  warehouseDs.GetPersonForSystemUser(userId).Id
                  );
            }
            else
            {
              result = "Błąd! Nie wybrano docelowej lokalizacji.";
            }

            WebTools.AlertMsgAdd(result);
          }
        }
        else
        {
          WebTools.AlertMsgAdd("Błąd! Nie można przenieść więcej materiału niż jest na lokalizacji.");
        }

        var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
        GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
        detailTableView.Rebind();

        HideRelocationPanel();
      }
    }

    protected void ButtonFreeMaterialExecuteClick(object sender, EventArgs e)
    {
      Page.Validate("FreeMaterialGroup");

      if (IsValid)
      {
        int warehouseId = int.Parse(HiddenField_QuickOp_WarehouseId.Value);
        int materialId = int.Parse(HiddenField_QuickOp_MaterialId.Value);
        int orderId = int.Parse(HiddenField_QuickOp_OrderId.Value);
        int departmentId = int.Parse(HiddenField_QuickOp_DepartmentId.Value);
        int processIdpl = int.Parse(HiddenField_QuickOp_ProcessIdpl.Value);
        int shelfId = int.Parse(HiddenField_QuickOp_ShelfSrcId.Value);
        var newquantity = decimal.Parse(FreeMaterialQuantityTextBox.Text.Replace(".", ","));

        if (departmentId > 0)
        {
          WebTools.AlertMsgAdd("Nie można (jeszcze) operować na materiale przypisanym do działu!");
          HideFreeMaterialPanel();
          return;
        }

        if (true) //(newquantity <= maxQuantity)
        {
          int userId = 0;
          int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId);

          using (var warehouseDs = new WarehouseDS())
          {
            //Zone zone = warehouseDs.GetShelfByNameDataOnly(RelocationTargetZoneRCB.Text);
            string result = "";
            result = warehouseDs.FreeMaterialInWarehouse(
                warehouseId,
                materialId,
                orderId,
                processIdpl != 0 && processIdpl < 2000000 ? processIdpl : (int?)null,
                shelfId,
                newquantity,
                warehouseDs.GetPersonForSystemUser(userId).Id
                );

            WebTools.AlertMsgAdd(result);
          }
        }
        else
        {
          WebTools.AlertMsgAdd("Błąd! Nie można przenieść więcej materiału niż jest na lokalizacji.");
        }
        var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
        GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
        detailTableView.Rebind();
        HideFreeMaterialPanel();
      }
    }

    protected void ButtonInventoryMaterialExecuteClick(object sender, EventArgs e)
    {
      Page.Validate("InventoryMaterialGroup");

      if (IsValid)
      {
        int warehouseId = int.Parse(HiddenField_QuickOp_WarehouseId.Value);
        int materialId = int.Parse(HiddenField_QuickOp_MaterialId.Value);
        int orderId = int.Parse(HiddenField_QuickOp_OrderId.Value);
        int departmentId = int.Parse(HiddenField_QuickOp_DepartmentId.Value);
        int processIdpl = int.Parse(HiddenField_QuickOp_ProcessIdpl.Value);
        int? processIddpl = int.Parse(HiddenField_QuickOp_ProcessIddpl.Value);
        int shelfId = int.Parse(HiddenField_QuickOp_ShelfSrcId.Value);

        if (departmentId > 0)
        {
          WebTools.AlertMsgAdd("Nie można (jeszcze) operować na materiale przypisanym do działu!");
          HideInventoryMaterialPanel();
          return;
        }

        processIddpl = (processIddpl == 0 ? null : processIddpl);

        string hiddenFieldQuickOpMaxQuantityValStr = HiddenField_QuickOp_MaxQuantity.Value.Replace(".", ",");
        double quantityOnStack = double.Parse(hiddenFieldQuickOpMaxQuantityValStr);

        string inventoryQuantityTextBoxValStr = InventoryQuantityTextBox.Text.Replace(".", ",");
        double quantityObserved = double.Parse(inventoryQuantityTextBoxValStr);

        //WarehouseDocumentPosition warehouseDocumentPosition;

        Material material = null;
        decimal unitPrice = 0;
        double quantity = 0;
        int? processIdpl2 = null;
        Order order = null;

        Zone shelf;

        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        {
          // TODO: Take care of DepartmentId !!! && s.DepartmentId == (departmentId > 0 ? departmentId : (int?)null)
          var stackPosQ =
              warehouseDs.WarehouseEntitiesTest.CurrentStackInDocuments.Where(
                  s =>
                  s.WarehouseTargetId == warehouseId
                  && s.MaterialId == materialId
                    //&& s.ProcessIdpl == (processIdpl > 0 ? processIdpl : (int?)null)
                  && s.ShelfId == shelfId);

          //&& ((orderId > 0 && s.OrderId == orderId) || (orderId == 0 && !s.OrderId.HasValue)) && //s.OrderId == (orderId > 0 ? orderId : (int?)null)
          if (orderId > 0)
          {
            stackPosQ = stackPosQ.Where(p => p.OrderId == orderId);
          }
          else
          {
            stackPosQ = stackPosQ.Where(p => !p.OrderId.HasValue);
          }
          if (processIdpl > 0)
          {
            stackPosQ = stackPosQ.Where(p => p.ProcessIdpl == processIdpl);
          }
          else
          {
            stackPosQ = stackPosQ.Where(p => !p.ProcessIdpl.HasValue);
          }
          if (processIddpl > 0)
          {
              stackPosQ = stackPosQ.Where(p => p.ProcessIddpl == processIddpl);
          }
          else
          {
              stackPosQ = stackPosQ.Where(p => !p.ProcessIddpl.HasValue);
          }

          if (false) // TODO: Check
          {
              if (departmentId > 0)
              {
                  stackPosQ = stackPosQ.Where(p => p.DepartmentId == departmentId);
              }
              else
              {
                  stackPosQ = stackPosQ.Where(p => !p.DepartmentId.HasValue);
              }
          }

          var stackPos = stackPosQ.First();

          decimal valuePerUnit = stackPos.UnitPrice;

          material = warehouseDs.WarehouseEntitiesTest.Materials.Include("Unit").First(p => p.Id == materialId);
          unitPrice = valuePerUnit;
          quantity = quantityObserved;

          //warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, valuePerUnit, quantityObserved);

          processIdpl2 = (processIdpl > 0 && processIdpl < 2000000) ? processIdpl : (int?)null;

          if (orderId > 0)
          {
            order = warehouseDs.GetOrderByIdDataOnly(orderId);
          }

          shelf = warehouseDs.GetZoneByIdDataOnly(shelfId);
        }

        WarehouseInventoryPosition inventoryPositionWhichExists =
            warehouseInventoryPositions.FirstOrDefault(
                p =>
                p.WarehouseId == warehouseId
                &&
                p.MaterialId == material.Id
                &&
                p.UnitPrice == unitPrice
                &&
                (p.Order == null && order == null || (p.Order != null && order != null && p.OrderId == order.Id))
                &&
                p.ZoneId == shelfId
                &&
                p.ProcessIdpl == processIdpl2
                &&
                p.ProcessIddpl == processIddpl
                );

        if (inventoryPositionWhichExists != null) //positionsWithShelfsToAdd.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id && p.position.UnitPrice == warehouseDocumentPosition.UnitPrice && p.shelf.Id == shelfId) == 1)
        {
          throw new Exception("Ta pozycja została już dodana. Nie można jej dodać dwukrotnie.");
        }
        else
        {
          using (var warehouseDs = new WarehouseDS())
          {
            var warehouseEntities = warehouseDs.WarehouseEntitiesTest;
            var newWarehouseInventoryPosition = new WarehouseInventoryPosition()
            {
              IsActive = true,
              AddedTime = DateTime.Now,
              ObservationTime = DateTime.Now,
              WarehouseDocumentPositionId = null,
              Zone = (shelf != null ? warehouseEntities.Zones.FirstOrDefault(p => p.Id == shelf.Id) : null),
              Order = (order != null ? warehouseEntities.Orders.FirstOrDefault(p => p.Id == order.Id) : null),
              DepartmentId = null,
              WarehouseId = warehouseId,
              QuantityBefore = quantityOnStack,
              Material = (material != null ? warehouseEntities.Materials.FirstOrDefault(p => p.Id == material.Id) : null),
              Quantity = quantity,
              UnitPrice = unitPrice,
              ProcessIdpl = processIdpl2,
              ProcessIddpl = processIddpl
            };
            warehouseDs.WarehouseEntitiesTest.AddToWarehouseInventoryPositions(newWarehouseInventoryPosition);
            warehouseDs.WarehouseEntitiesTest.SaveChanges();
            warehouseInventoryPositions.Add(newWarehouseInventoryPosition);
          }
        }

        Session["INVPositions"] = warehouseInventoryPositions;

        var detailTableViewCtrl = Page.FindControl(HiddenField_QuickOp_DetailStacksContainerID.Value);
        GridTableView detailTableView = (GridTableView)detailTableViewCtrl;
        detailTableView.Rebind();
        HideInventoryMaterialPanel();
      }
    }

    protected void ChangeLocalizationButtonClick(object sender, EventArgs e)
    {
      Contract.Requires(sender != null);

      HideFreeMaterialPanel();
      HideInventoryMaterialPanel();

      var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer));

      var gridTableRow2 = gridTableRow.Cells[2].Text; // OrderId
      var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
      var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
      var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
      var gridTableRow11 = gridTableRow.Cells[11].Text; // DepartmentId

      int orderId = 0;
      int shelfId = 0;
      int processIdpl = 0;
      int departmentId = 0;

      double quantity = 0.0;

      var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

      string keyRendered = detailTableView.ParentItem.KeyValues;
      keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
      keyRendered = keyRendered.Replace(",MaterialId:", ";");
      keyRendered = keyRendered.Replace("\"", "");
      keyRendered = keyRendered.Replace("}", "");

      string[] ids = keyRendered.Split(';');
      Contract.Assert(ids.Length >= 2);
      int warehouseId = int.Parse(ids[0]);
      int materialId = int.Parse(ids[1]);

      int.TryParse(gridTableRow2, out orderId);
      int.TryParse(gridTableRow3, out shelfId);
      double.TryParse(gridTableRow6, out quantity);
      int.TryParse(gridTableRow10, out processIdpl);
      int.TryParse(gridTableRow11, out departmentId);

      HiddenField_QuickOp_WarehouseId.Value = warehouseId.ToString();
      HiddenField_QuickOp_MaterialId.Value = materialId.ToString();
      HiddenField_QuickOp_OrderId.Value = orderId.ToString();
      HiddenField_QuickOp_DepartmentId.Value = departmentId.ToString();
      HiddenField_QuickOp_ProcessIdpl.Value = processIdpl.ToString();
      HiddenField_QuickOp_ShelfSrcId.Value = shelfId.ToString();
      HiddenField_QuickOp_MaxQuantity.Value = quantity.ToString();

      HiddenField_QuickOp_DetailStacksContainerID.Value = detailTableView.UniqueID;

      RelocationQuantityTextBox.Text = quantity.ToString();

      //gridTableRow.BorderStyle = BorderStyle.Solid;
      //gridTableRow.BorderWidth = 3;
      //gridTableRow.BackColor = System.Drawing.Color.Orange;

      //Style s = new Style();
      //s.BackColor = System.Drawing.Color.Red;
      //s.BorderStyle = BorderStyle.Solid;
      //s.BorderColor = System.Drawing.Color.Blue;
      //gridTableRow.ApplyStyle(s);

      //ImageMark

      var imageMark = gridTableRow.FindControl("ImageMark");
      imageMark.Visible = true;

      MaterialRelocationPanel.Visible = true;
    }

    protected void FreeMaterialButtonClick(object sender, EventArgs e)
    {
      Contract.Requires(sender != null);

      HideRelocationPanel();
      HideInventoryMaterialPanel();

      var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer));

      var gridTableRow2 = gridTableRow.Cells[2].Text; // OrderId
      var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
      var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
      var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
      var gridTableRow11 = gridTableRow.Cells[11].Text; // DepartmentId

      int orderId = 0;
      int shelfId = 0;
      int processIdpl = 0;
      int departmentId = 0;

      // TODO: Consider departments!!! Also in other op buttons!!!

      double quantity = 0.0;

      var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

      string keyRendered = detailTableView.ParentItem.KeyValues;
      keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
      keyRendered = keyRendered.Replace(",MaterialId:", ";");
      keyRendered = keyRendered.Replace("\"", "");
      keyRendered = keyRendered.Replace("}", "");

      string[] ids = keyRendered.Split(';');
      Contract.Assert(ids.Length >= 2);
      int warehouseId = int.Parse(ids[0]);
      int materialId = int.Parse(ids[1]);

      int.TryParse(gridTableRow2, out orderId);
      int.TryParse(gridTableRow3, out shelfId);
      double.TryParse(gridTableRow6, out quantity);
      int.TryParse(gridTableRow10, out processIdpl);
      int.TryParse(gridTableRow11, out departmentId);

      HiddenField_QuickOp_WarehouseId.Value = warehouseId.ToString();
      HiddenField_QuickOp_MaterialId.Value = materialId.ToString();
      HiddenField_QuickOp_OrderId.Value = orderId.ToString();
      HiddenField_QuickOp_DepartmentId.Value = departmentId.ToString();
      HiddenField_QuickOp_ProcessIdpl.Value = processIdpl.ToString();
      HiddenField_QuickOp_ShelfSrcId.Value = shelfId.ToString();
      HiddenField_QuickOp_MaxQuantity.Value = quantity.ToString();

      HiddenField_QuickOp_DetailStacksContainerID.Value = detailTableView.UniqueID;

      FreeMaterialQuantityTextBox.Text = quantity.ToString();

      //gridTableRow.BorderStyle = BorderStyle.Solid;
      //gridTableRow.BorderWidth = 3;
      //gridTableRow.BackColor = System.Drawing.Color.Orange;

      //Style s = new Style();
      //s.BackColor = System.Drawing.Color.Red;
      //s.BorderStyle = BorderStyle.Solid;
      //s.BorderColor = System.Drawing.Color.Blue;
      //gridTableRow.ApplyStyle(s);

      //ImageMark

      var imageMark = gridTableRow.FindControl("ImageMark");
      imageMark.Visible = true;

      FreeMaterialPanel.Visible = true;
    }

    protected void FreeMaterialQuantity_ServerValidate(object source, ServerValidateEventArgs args)
    {
      try
      {
        var maxQuantity = double.Parse(HiddenField_QuickOp_MaxQuantity.Value);
        var enteredQuantity = double.Parse(FreeMaterialQuantityTextBox.Text);

        args.IsValid = enteredQuantity <= maxQuantity;
      }
      catch (Exception)
      {
        args.IsValid = false;
      }
    }

    protected void Grid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      //perfDebug.Restart();

      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        warehouseDs.WarehouseEntitiesTest.CommandTimeout = GetCommandTimeoutCounterFromPage(); // extend timeout to make WarehouseStacks usable in any way. Has to be fixed properly by optimizing view.
        SetCommandTimeoutCounterOnPage(warehouseDs);

        int? orderIdToFind = null;
        if (!string.IsNullOrWhiteSpace(cmbOrderNumber.SelectedValue) && !string.IsNullOrWhiteSpace(cmbOrderNumber.Text))
        {
          orderIdToFind = int.Parse(cmbOrderNumber.SelectedValue);
        }

        int? shelfIdToFind = null;
        if (!string.IsNullOrWhiteSpace(cmbShelfNumber.SelectedValue) && !string.IsNullOrWhiteSpace(cmbShelfNumber.Text))
        {
          shelfIdToFind = int.Parse(cmbShelfNumber.SelectedValue);
        }

        int mId = 0;

        int.TryParse(materialsRCB.SelectedValue, out mId);

        var materialId = mId > 0 ? mId : (int?)null;
        var materialNameOrSimpleId = materialsRCB.Text;
        var warehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

        //UniqueName="IndexSIMPLE" DataField="IndexSIMPLE"
        //UniqueName="MaterialName" DataField="MaterialName"
        //UniqueName="GlobalQuantity" DataField="GlobalQuantity" // all warehouses
        //UniqueName="LocalQuantity" DataField="LocalQuantity" // current warehouse
        //UniqueName="UnitShortName" DataField="UnitShortName"
        //UniqueName="WarehouseId" DataField="WarehouseId"
        //UniqueName="MaterialId" DataField="MaterialId"
        //used in filtering: OrderId, ShelfId

        var query = warehouseDs.WarehouseEntitiesTest.CurrentStack2.Where(c => c.WarehouseId == warehouseId);

        if (materialId.HasValue)
        {
          query = query.Where(q => q.MaterialId == materialId);
        }
        else if (!String.IsNullOrEmpty(materialNameOrSimpleId))
        {
          materialNameOrSimpleId = materialNameOrSimpleId.Trim().ToLower();
          query = query.Where
            (q =>
              q.IndexSIMPLE.ToLower().Contains(materialNameOrSimpleId) ||
              q.MaterialName.ToLower().Contains(materialNameOrSimpleId)
            );
        }

        if (orderIdToFind.HasValue)
          query = query.Where(q => q.OrderId == orderIdToFind);

        if (shelfIdToFind.HasValue)
          query = query.Where(q => q.ShelfId == shelfIdToFind);

        var group = query.GroupBy(q => new { q.WarehouseId, q.MaterialId })
          .Select(q => new
          {
            FoD = q.FirstOrDefault(),
            LocalQuantity = q.Sum(qq => qq.Quantity)
          })
          .Select(q => new
          {
            IndexSIMPLE = q.FoD.IndexSIMPLE,
            MaterialName = q.FoD.MaterialName,
            LocalQuantity = q.LocalQuantity,
            GlobalQuantity = q.LocalQuantity, // this is a mock to prevent exceptions on the view, proper GlobalQuantity is to be implemented yet
            UnitShortName = q.FoD.UnitShortName + ".",
            WarehouseId = q.FoD.WarehouseId,
            MaterialId = q.FoD.MaterialId,
          });

        var items = group.OrderBy(p => p.IndexSIMPLE).ToArray();

        //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 1 " + w.ElapsedMilliseconds);
        StackGrid.VirtualItemCount = items.Count();
        //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 2 " + w.ElapsedMilliseconds);
        int pageSize = StackGrid.PageSize;
        int pageNum = StackGrid.CurrentPageIndex;

        var data = items.Skip(pageSize * pageNum)
          .Take(pageSize)
          .ToArray();
        StackGrid.DataSource = data;

        //Debug.WriteLine(">>>>>>>>> materialsCandidatesToConsider 3 " + w.ElapsedMilliseconds);
        //perfDebug.LogPerformance();
      }
    }

    protected void InventoryMaterialButtonClick(object sender, EventArgs e)
    {
      Contract.Requires(sender != null);

      // TODO: Implement

      HideRelocationPanel();
      HideFreeMaterialPanel();

      var gridTableRow = ((GridTableRow)(((RadButton)sender).BindingContainer));

      var gridTableRow2 = gridTableRow.Cells[2].Text; // OrderId
      var gridTableRow3 = gridTableRow.Cells[3].Text; // ShelfId
      var gridTableRow6 = gridTableRow.Cells[6].Text; // Quantity
      var gridTableRow10 = gridTableRow.Cells[10].Text; // ProcessIdpl
      var gridTableRow11 = gridTableRow.Cells[11].Text; //ProcessIddpl
      var gridTableRow12 = gridTableRow.Cells[12].Text; // DepartmentId

      int orderId = 0;
      int shelfId = 0;
      int processIdpl = 0;
      int processIddpl = 0;
      int departmentId = 0;

      // TODO: Consider departments!!! Also in other op buttons!!!

      double quantity = 0.0;

      var detailTableView = (GridTableView)((((RadButton)sender).BindingContainer).BindingContainer);

      string keyRendered = detailTableView.ParentItem.KeyValues;
      keyRendered = keyRendered.Replace("{WarehouseId:", ""); //keyRendered.Replace("{WarehouseId:\\\"", "");
      keyRendered = keyRendered.Replace(",MaterialId:", ";");
      keyRendered = keyRendered.Replace("\"", "");
      keyRendered = keyRendered.Replace("}", "");

      string[] ids = keyRendered.Split(';');
      Contract.Assert(ids.Length >= 2);
      int warehouseId = int.Parse(ids[0]);
      int materialId = int.Parse(ids[1]);

      if (warehouseInventoryPositions != null && warehouseInventoryPositions.Any(p => p.WarehouseId != warehouseId))
      {
        //throw new Exception("");

        WebTools.AlertMsgAdd("Nie można inwentaryzować kilku magazynów jednocześnie!");
        //HideInventoryMaterialPanel();
        return;
      }

      int.TryParse(gridTableRow2, out orderId);
      int.TryParse(gridTableRow3, out shelfId);
      double.TryParse(gridTableRow6, out quantity);
      int.TryParse(gridTableRow10, out processIdpl);
      int.TryParse(gridTableRow11, out processIddpl);
      int.TryParse(gridTableRow12, out departmentId);

      HiddenField_QuickOp_WarehouseId.Value = warehouseId.ToString();
      HiddenField_QuickOp_MaterialId.Value = materialId.ToString();
      HiddenField_QuickOp_OrderId.Value = orderId.ToString();
      HiddenField_QuickOp_DepartmentId.Value = departmentId.ToString();
      HiddenField_QuickOp_ProcessIdpl.Value = processIdpl.ToString();
      HiddenField_QuickOp_ProcessIddpl.Value = processIddpl.ToString();
      HiddenField_QuickOp_ShelfSrcId.Value = shelfId.ToString();
      HiddenField_QuickOp_MaxQuantity.Value = quantity.ToString();

      HiddenField_QuickOp_DetailStacksContainerID.Value = detailTableView.UniqueID;

      InventoryQuantityTextBox.Text = quantity.ToString();

      //gridTableRow.BorderStyle = BorderStyle.Solid;
      //gridTableRow.BorderWidth = 3;
      //gridTableRow.BackColor = System.Drawing.Color.Orange;

      //Style s = new Style();
      //s.BackColor = System.Drawing.Color.Red;
      //s.BorderStyle = BorderStyle.Solid;
      //s.BorderColor = System.Drawing.Color.Blue;
      //gridTableRow.ApplyStyle(s);

      //ImageMark

      var imageMark = gridTableRow.FindControl("ImageMark");
      imageMark.Visible = true;

      InventoryPanel.Visible = true;
    }

    // this does not seem to fire
    protected void InventoryMaterialQuantity_ServerValidate(object source, ServerValidateEventArgs args)
    {
      var inventoryQuantity = -1.0;
      args.IsValid = double.TryParse(InventoryQuantityTextBox.Text, out inventoryQuantity);
      args.IsValid = args.IsValid && inventoryQuantity >= 0;
    }

    protected void OpenInventoryListClick(object sender, EventArgs e)
    {
      //Respo
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      //perfDebug.Restart();
      int? companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
      int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
      int? userId = (int?)Session[SessionData.SessionKey_UserId];

      if (Request["o"] == "i")
      {
        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        {
          InventoryMode = warehouseDs.CheckOperationAvailability(currentWarehouseId, currentWarehouseId, WarehouseDocumentTypeEnum.INW, userId) == "";
        }
      }

      if (Session["INVPositions"] == null)
        warehouseInventoryPositions = new List<WarehouseInventoryPosition>();
      else
        warehouseInventoryPositions = (List<WarehouseInventoryPosition>)Session["INVPositions"];

      if (!Page.IsPostBack)
      {
        Session["tab_Stacks"] = InventoryMode ? 1 : 0;
        LabelTitle.Text = InventoryMode ? "Inwentaryzacja" : "Stany magazynowe";
        PanelInventory.Visible = InventoryMode;

        materialsRCB.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();

        if (InventoryMode)
        {
          using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
          {
            warehouseInventoryPositions = warehouseDs.GetActiveWarehouseInventoryPositions(currentWarehouseId);
            Session["INVPositions"] = warehouseInventoryPositions;
          }
        }
      }

      MaterialRelocationPanel.Visible = false;
      FreeMaterialPanel.Visible = false;
      InventoryPanel.Visible = false;
      //perfDebug.LogPerformance();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      //perfDebug.Restart();
      //this.Panel_EntirePageContent.Visible = this.PageIsAllowedByConfigAndSecurity;
      this.StackGrid.Enabled = !MaterialRelocationPanel.Visible && !FreeMaterialPanel.Visible && !InventoryPanel.Visible;
      //perfDebug.LogPerformance();
    }

    protected void RelocationQuantity_ServerValidate(object source, ServerValidateEventArgs args)
    {
      try
      {
        var maxQuantity = double.Parse(HiddenField_QuickOp_MaxQuantity.Value);
        var enteredQuantity = double.Parse(RelocationQuantityTextBox.Text);

        args.IsValid = enteredQuantity <= maxQuantity;
      }
      catch (Exception)
      {
        args.IsValid = false;
      }
    }

    protected void RelocationTargetZone_ServerValidate(object source, ServerValidateEventArgs args)
    {
      Zone zone = null;

      if (!string.IsNullOrEmpty(RelocationTargetZoneRCB.Text))
      {
        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        {
          zone = warehouseDs.GetShelfByName(RelocationTargetZoneRCB.Text);//.GetShelfByNameDataOnly(RelocationTargetZoneRCB.Text);
        }
      }

      if (zone == null) args.IsValid = false;
      else args.IsValid = true;
    }

    protected void SearchClick(object sender, EventArgs e)
    {
      StackGrid.Visible = true;
      StackGrid.Rebind();
    }

    protected void ShowStockReportClick(object sender, EventArgs e)
    {
      GridDataItem item = (GridDataItem)StackGrid.SelectedItems[0];
      if (item != null)
      {
        int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
        string materialIndex = item["IndexSIMPLE"].Text;

        Material material;

        using (var warehouseDs = new WarehouseDS())
        {
          material = warehouseDs.GetMaterialByIndex(materialIndex.Trim(), companyId);
        }
        int? warehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];

        Response.Redirect("Reports/StockReport.aspx?t=1&warehouseId=" + warehouseId + "&materialId=" + material.Id);
      }
    }

    protected void StackGrid_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
      GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;
      if (parentItem.Edit)
      {
        return;
      }

      int warehouseId = (int)parentItem.GetDataKeyValue("WarehouseId");
      int materialId = (int)parentItem.GetDataKeyValue("MaterialId");

      int? orderIdToFind = null;
      if (!string.IsNullOrWhiteSpace(cmbOrderNumber.SelectedValue))
      {
        orderIdToFind = int.Parse(cmbOrderNumber.SelectedValue);
      }

      int? shelfIdToFind = null;
      if (!string.IsNullOrWhiteSpace(cmbShelfNumber.SelectedValue))
      {
        shelfIdToFind = int.Parse(cmbShelfNumber.SelectedValue);
      }

      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
        int? userId = (int?)Session[SessionData.SessionKey_UserId];
        bool currentUserCanFreeMaterialsInCurrentWarehouse;
        currentUserCanFreeMaterialsInCurrentWarehouse = warehouseDs.CheckOperationAvailability(currentWarehouseId, currentWarehouseId, WarehouseDocumentTypeEnum.UMZ, userId) == "";

        var invMix = warehouseInventoryPositions.Select(wip => new
        {
          wip.WarehouseId,
          wip.MaterialId,
          wip.OrderId,
          wip.ProcessIdpl,
          wip.ProcessIddpl,
          wip.ZoneId
        }).Distinct();

        var materialsCandidatesToConsider = warehouseDs.GetCurrentStackDetails(warehouseId, materialId, orderIdToFind, shelfIdToFind);

        var details = materialsCandidatesToConsider.Select(p => new
                      {
                        WarehouseId = 12345,
                        MaterialId = 12345,
                        Quantity = p.Quantity,
                        UnitShortName = p.UnitShortName + ".",
                        ShelfId = p.ShelfId,
                        ShelfName = p.ShelfName,
                        OrderId = p.OrderId,
                        DepartmentId = p.DepartmentId,
                        OrderNumber = (!string.IsNullOrEmpty(p.OrderNumber) ? p.OrderNumber : "stan magazynowy"),
                        ProcessIdpl = p.ProcessIdpl,
                        ProcessIddpl = p.ProcessIddpl,
                        ZoneChangeEnabled = !InventoryMode && !p.TimeChildrenEnabled.HasValue && !(!p.OrderId.HasValue && !string.IsNullOrEmpty(p.OrderNumber) && p.OrderNumber != "zwrot do dostawcy"),
                        FreeMaterialEnabled = !InventoryMode && !p.TimeChildrenEnabled.HasValue && p.OrderId.HasValue && currentUserCanFreeMaterialsInCurrentWarehouse,
                        InventoryMaterialEnabled = InventoryMode && !p.TimeChildrenEnabled.HasValue
                      });

        var detailsMaterialized = details.ToArray();

        var detailsFinal = detailsMaterialized
            .Select(p => new
            {
              WarehouseId = p.WarehouseId,
              MaterialId = p.MaterialId,
              Quantity = p.Quantity,
              UnitShortName = p.UnitShortName,
              ShelfId = p.ShelfId,
              ShelfName = p.ShelfName,
              OrderId = p.OrderId,
              DepartmentId = p.DepartmentId,
              OrderNumber = p.OrderNumber,
              ProcessIdpl = p.ProcessIdpl,
              ProcessIddpl = p.ProcessIddpl,
              ZoneChangeEnabled = p.ZoneChangeEnabled,
              FreeMaterialEnabled = p.FreeMaterialEnabled,
              InventoryMaterialEnabled = p.InventoryMaterialEnabled && !invMix.Any(q =>
                     q.WarehouseId == warehouseId
                     && q.MaterialId == materialId
                     && ((q.OrderId == null && p.OrderId == null) || q.OrderId == p.OrderId)
                     && ((q.ProcessIdpl == null && p.ProcessIdpl == null) || q.ProcessIdpl == p.ProcessIdpl)
                     && ((q.ProcessIddpl == null && p.ProcessIddpl == null) || q.ProcessIddpl == p.ProcessIddpl)
                     && q.ZoneId == p.ShelfId
                     ),

              PositionIsInventarized =
                 InventoryMode
                 && invMix.Any(q =>
                     q.WarehouseId == warehouseId
                     && q.MaterialId == materialId
                     && ((q.OrderId == null && p.OrderId == null) || q.OrderId == p.OrderId)
                     && ((q.ProcessIdpl == null && p.ProcessIdpl == null) || q.ProcessIdpl == p.ProcessIdpl)
                     && q.ZoneId == p.ShelfId
                     && ((q.ProcessIddpl == null && p.ProcessIddpl == null) || q.ProcessIddpl == p.ProcessIddpl)
                     )
            });

        e.DetailTableView.DataSource = detailsFinal.ToArray(); // materialsCandidatesToConsider.ToArray();
      }
    }

    protected void StackGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
      if (e.Item is GridDataItem) // && e.Item.OwnerTableView.DataSourceID == "AccessDataSource1")
      {
        Label lbl = e.Item.FindControl("numberLabel") as Label;
        if (lbl != null)
        {
          int pageSize = StackGrid.PageSize;
          int pageNum = StackGrid.CurrentPageIndex;

          lbl.Text = (e.Item.ItemIndex + 1 + pageSize * pageNum).ToString();
        }
      }

      if (e.Item is GridDataItem)
      {
        var gridDataItem = e.Item as GridDataItem;

        try
        {
          var gridDataItem1 = gridDataItem["StockReportLink1"];
          if (gridDataItem1 != null && gridDataItem1.Controls != null && gridDataItem1.Controls.Count > 0)
          {
            var hyperLink1 = gridDataItem1.Controls[0] as HyperLink;
            if (hyperLink1 != null)
            {
              var url = hyperLink1.NavigateUrl;
              int i = url.IndexOf("&m=") + 3;
              hyperLink1.NavigateUrl = String.Format("{0}{1}", url.Substring(0, i),
                                                     HttpUtility.UrlEncode(url.Substring(i)));
              //int j = url.IndexOf("','Raport1'");
              //var m = url.Substring(i, (j - i));
              //hyperLink.NavigateUrl = url.Replace(m, HttpUtility.UrlEncode(m));
            }
          }

          var gridDataItem2 = gridDataItem["StockReportLink2"];
          if (gridDataItem2 != null && gridDataItem2.Controls != null && gridDataItem2.Controls.Count > 0)
          {
            var hyperLink2 = gridDataItem2.Controls[0] as HyperLink;
            if (hyperLink2 != null)
            {
              var url = hyperLink2.NavigateUrl;
              int i = url.IndexOf("&m=") + 3;
              hyperLink2.NavigateUrl = String.Format("{0}{1}", url.Substring(0, i),
                                                     HttpUtility.UrlEncode(url.Substring(i)));
            }
          }
        }
        catch (Exception)
        {
        }
      }
    }

    private int GetCommandTimeoutCounterFromPage()
    {
      if (Page.Master != null && Page.Master.Master != null)
      {
        var hiddenField = Page.Master.Master.FindControl("QueryTimeout") as HiddenField;
        if (hiddenField != null)
        {
          int timeout = 0;
          if (int.TryParse(hiddenField.Value, out timeout))
          { return timeout; }
        }
      }
      return 0;
    }

    private void HideFreeMaterialPanel()
    {
      HiddenField_QuickOp_WarehouseId.Value = "";
      HiddenField_QuickOp_MaterialId.Value = "";
      HiddenField_QuickOp_OrderId.Value = "";
      HiddenField_QuickOp_DepartmentId.Value = "";
      HiddenField_QuickOp_ProcessIdpl.Value = "";
      HiddenField_QuickOp_ProcessIddpl.Value = "";
      HiddenField_QuickOp_ShelfSrcId.Value = "";
      HiddenField_QuickOp_MaxQuantity.Value = "";
      HiddenField_QuickOp_DetailStacksContainerID.Value = "";

      FreeMaterialQuantityTextBox.Text = "";

      FreeMaterialPanel.Visible = false;
    }

    private void HideInventoryMaterialPanel()
    {
      HiddenField_QuickOp_WarehouseId.Value = "";
      HiddenField_QuickOp_MaterialId.Value = "";
      HiddenField_QuickOp_OrderId.Value = "";
      HiddenField_QuickOp_DepartmentId.Value = "";
      HiddenField_QuickOp_ProcessIdpl.Value = "";
      HiddenField_QuickOp_ProcessIddpl.Value = "";
      HiddenField_QuickOp_ShelfSrcId.Value = "";
      HiddenField_QuickOp_MaxQuantity.Value = "";
      HiddenField_QuickOp_DetailStacksContainerID.Value = "";

      InventoryQuantityTextBox.Text = "";

      InventoryPanel.Visible = false;
    }

    private void HideRelocationPanel()
    {
      RelocationTargetZoneRCB.Text = "";
      RelocationQuantityTextBox.Text = "";

      HiddenField_QuickOp_WarehouseId.Value = "";
      HiddenField_QuickOp_MaterialId.Value = "";
      HiddenField_QuickOp_OrderId.Value = "";
      HiddenField_QuickOp_DepartmentId.Value = "";
      HiddenField_QuickOp_ProcessIdpl.Value = "";
      HiddenField_QuickOp_ProcessIddpl.Value = "";
      HiddenField_QuickOp_ShelfSrcId.Value = "";
      HiddenField_QuickOp_MaxQuantity.Value = "";
      HiddenField_QuickOp_DetailStacksContainerID.Value = "";

      RelocationQuantityTextBox.Text = "";

      MaterialRelocationPanel.Visible = false;
    }

    private void SetCommandTimeoutCounterOnPage(WarehouseDS warehouseDs)
    {
      if (Page.Master != null && Page.Master.Master != null)
      {
        var hiddenField = Page.Master.Master.FindControl("QueryTimeout") as HiddenField;
        if (hiddenField != null && warehouseDs.WarehouseEntitiesTest.CommandTimeout.HasValue)
        {
          hiddenField.Value = warehouseDs.WarehouseEntitiesTest.CommandTimeout.ToString();
        }
      }
    }
  }
}
