﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WarehouseMaster.Master" CodeBehind="AccessDenied.aspx.cs" Inherits="MagWeb.AccessDenied" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="mainPlace" runat="server">

	<div style="text-align: center; margin-top: 100px;">
		 <div style="font-size: 16pt; margin-bottom: 20px">Nie posiadasz uprawnień koniecznych do korzystania z tej części systemu. </div>
		 By uzyskać dostęp wyślij maila na adres k3@w-b.pl ze swoim przełożonym w cc (kopi dw).
	</div>

</asp:Content>