﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;
//using WarehouseWeb;

namespace MagWeb
{
    public partial class RemoveMaterial : FeaturedSystemWebUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, null, WarehouseDocumentTypeEnum.LM, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
					PermissionDenied();
					return;
                }
            }

            if (!IsPostBack)
            {
                inputDate.SelectedDate = DateTime.Now.Date;
            }

            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            if (!Page.IsPostBack)
                txtName.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
        }

        protected void CurrentStackGrid_OnNeededDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var materials = from p in warehouseDs.GetCurrentStackInDocuments(txtName.Text, txtIndex.Text, (int?) Session[SessionData.SessionKey_CurrentWarehouseId])
                                where (String.IsNullOrEmpty(txtOrderNumber.Text) ||
                                        p.OrderNumber != null && p.OrderNumber.ToLower().Contains(txtOrderNumber.Text.ToLower())) &&
                                      (String.IsNullOrEmpty(cmbShelf.Text) ||
                                        p.ShelfName.ToLower().Contains(cmbShelf.Text.ToLower()))
                                select p;

                StackGrid.DataSource = materials.ToArray();

            }
        }

        protected void SearchClick(object sender, EventArgs e)
        {
            this.StackGrid.Rebind();
        }

        protected void ButtonRemoveMaterial_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    if (Session[SessionData.SessionKey_UserId] != null)
                    {
                        var quantity = decimal.Parse(txtQuantity.Text);

                        int userId = (int)Session[SessionData.SessionKey_UserId];
                        int warehouseSourceId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                        GridItem item = StackGrid.SelectedItems[0];

                        int materialId = int.Parse(item.Cells[2].Text);
                        int positionId = int.Parse(item.Cells[3].Text);
                        int shelfId = int.Parse(item.Cells[4].Text);

                        WarehouseDocumentPosition documentPosition = warehouseDs.GetPositionById(positionId);
                        WarehouseDocument document = warehouseDs.GetWarehouseDocumentForPositionId(positionId);
                        Zone zone = warehouseDs.GetShelfByIdDataOnly(shelfId);

                        WarehouseDocumentPosition warehouseDocumentPosition =
                            warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, documentPosition.UnitPrice, -quantity, positionId,
                                                                                documentPosition.Order != null ? documentPosition.Order.Id : 0);

                        PositionWithQuantityAndShelfs positionWithQuantityAndShelf =
                            new PositionWithQuantityAndShelfs()
                                {
                                    position = warehouseDocumentPosition,
                                    documentParentIds = new List<int>() {document.Id},
                                    positionParentIds =
                                        new List<ChildParentPositionQuantity>()
                                            {
                                                new ChildParentPositionQuantity()
                                                    {
                                                        PositionParentID = documentPosition.Id,
                                                        Quantity = (decimal)-warehouseDocumentPosition.Quantity
                                                    }
                                            },
                                    WarehousePositionZones =
                                        new List<WarehousePositionZone>()
                                            {
                                                new WarehousePositionZone()
                                                    {
                                                        WarehouseDocumentPosition = warehouseDocumentPosition,
                                                        Zone = zone,
                                                        Quantity = -warehouseDocumentPosition.Quantity
                                                    }
                                            }
                                };

                        lock (typeof(WarehouseDS))
                        {
                            try
                            {
                                warehouseDs.RemoveMaterial(warehouseSourceId, warehouseDs.GetPersonForSystemUser(userId).Id,
                                                       new List<PositionWithQuantityAndShelfs>() { positionWithQuantityAndShelf },
                                                       inputDate.SelectedDate ?? DateTime.Now.Date);
                            }
                            catch (Exception ex)
                            {
                                WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                            }
                        }

                        StackGrid.SelectedIndexes.Clear();
                        StackGrid.Rebind();
                    }
                }
            }
        }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                txtQuantity.Text = txtQuantity.Text.Replace(".", ",");
                args.IsValid = (double.Parse(txtQuantity.Text) > 0);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (StackGrid.SelectedItems.Count == 1)
            {
                GridItem item = StackGrid.SelectedItems[0];
                
                double availableQuantity = double.Parse(item.Cells[8].Text);

                try
                {
                    double quantity = double.Parse(txtQuantity.Text.Replace(".", ","));
                    if (quantity > availableQuantity)
                    {
                        args.IsValid = false;
                        return;
                    }
                }
                catch
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (StackGrid.SelectedItems.Count == 1);
        }

    }
}
