using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
//using Warehouse.Agent;
using MagWeb.Extensions;
//using WarehouseWeb;
//using Client.Domain;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;
//using WarehouseDocument = Mag.Domain.Model.WarehouseDocument;
//using WarehouseDocumentPosition = Mag.Domain.Model.WarehouseDocumentPosition;
//using WarehouseDocumentTypeEnum = Mag.Domain.Model.WarehouseDocumentTypeEnum;

namespace MagWeb
{
    public partial class GiveMaterial : FeaturedSystemWebUIPage
    {
        private const string QUANTITY_IN_GRID = "Quantity";//5;
        private const string UNITPRICE_IN_GRID = "UnitPrice";//7;
        private const string ORDERNUMBER_IN_GRID = "OrderNumber";//8;
        private const string MATERIAL_ID_IN_GRID = "MaterialId";//9;

        private List<PositionWithQuantityAndShelfs> positionsToGive;

        protected void Page_Load(object sender, EventArgs e)
        {
            int currentComapnyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];

            Session["tab_GiveMaterial"] = 3;

            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                  (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, null, WarehouseDocumentTypeEnum.Wz, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
                    PermissionDenied();
                    return;
                }
            }

            if (!IsPostBack)
            {
                inputDate.SelectedDate = DateTime.Now.Date;
                Session["GiveMaterial_positionsToGive"] = null;

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    //RadComboBox_DocSubtypeSIMPLE
                    RadComboBox_DocSubtypeSIMPLE.DataSource =
                      from d in warehouseDs.GetDocTypesFromSIMPLE_WZ(currentComapnyId)
                      select new { Value = d.typdwz_id, Text = d.nazwa.Trim() + " (" + d.typdok_idn + ")" };
                    RadComboBox_DocSubtypeSIMPLE.DataTextField = "Text";
                    RadComboBox_DocSubtypeSIMPLE.DataValueField = "Value";
                    RadComboBox_DocSubtypeSIMPLE.DataBind();
                    RadComboBox_DocSubtypeSIMPLE.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
                }
            }

            if (Session["GiveMaterial_positionsToGive"] == null)
                positionsToGive = new List<PositionWithQuantityAndShelfs>();
            else
                positionsToGive = (List<PositionWithQuantityAndShelfs>)Session["GiveMaterial_positionsToGive"];

            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            this.CompanyClientRCB.WebServiceSettings.Method = "GetTelericContractingPartySIMPLEListCompany" + companyId.ToString();

            using (var warehouseDs = new WarehouseDS())
            {
                bool isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility(currentWarehouseId.Value,
                                                    warehouseDs.GetPersonForSystemUser
                                                      (userId).Id, 3);
                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }
        }

        protected void Tab_Prerender(object sender, EventArgs e)
        {
            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
        }

        protected void MaterialsToGiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
            DateTime dateToFilter = DateTime.Now;
            string indexSimpleToFind = IndexSimpleRTB.Text;

            string materialToFind = MaterialToSearchRTB.Text;
            string orderNumberToFind = OrderNumberToSearchRTB.Text;
            int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

            using (var wds = new WarehouseDS())
            {
                var elements = wds.GetMaterialsToGiveRadGrid(companyId, dateToFilter, materialToFind, indexSimpleToFind, currentWarehouseId, orderNumberToFind, positionsToGive);

                MaterialsToGiveRadGrid.VirtualItemCount = elements.Count();
                int pageSize = MaterialsToGiveRadGrid.PageSize;
                int pageNum = MaterialsToGiveRadGrid.CurrentPageIndex;

                var data = elements.Skip(pageSize * pageNum)
                  .Take(pageSize);

                MaterialsToGiveRadGrid.DataSource = elements.ToArray();
            }
        }

        protected void SearchMaterialClick(object sender, EventArgs e)
        {
            MaterialsToGiveRadGrid.Rebind();
        }

        //protected void AddMaterialToGiveClick_old(object sender, EventArgs e)
        //{
        //    if (IsValid)
        //    {
        //        WarehouseDocumentPosition warehouseDocumentPositionCandidate;
        //        GridItem item = MaterialsToGiveRadGrid.SelectedItems[0];
        //        var cell = item.Cells[INDEX_MATERIAL_ID_IN_GRID];
        //        int materialId = int.Parse(cell.Text);
        //        //var orderNumber = item.Cells[INDEX_ORDERNUMBER_IN_GRID].Text;
        //        int sourceWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

        //        double quantity = double.Parse(QuantityTextBox.Text.Replace(".", ","));
        //        IQueryable<CurrentStackInDocuments> stack;
        //        CurrentStackInDocuments[] materializedStack;

        //        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //        {
        //            warehouseDocumentPositionCandidate = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, 0,
        //                                                                                                     quantity);
        //            stack = from p in warehouseDs.GetCurrentStackInDocuments("", "", sourceWarehouseId)
        //                    where p.MaterialId == materialId // && ((orderNumber == "" && p.OrderNumber == null) || (p.OrderNumber == orderNumber))
        //                    select p;

        //            materializedStack = stack.ToArray();
        //        }

        //        foreach (var t in materializedStack.OrderBy(s => s.PositionId))
        //        {
        //            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //            {
        //                WarehouseDocumentPosition warehouseDocumentPosition;

        //                double used = 0;

        //                var materialMatch = positionsToGive.Where(p => p.position.Material.Id == t.MaterialId && p.WarehousePositionZones.All(pos_zone => pos_zone.Zone.Name == t.ShelfName));
        //                foreach (var match in materialMatch)
        //                {
        //                    var foundShelf = match.WarehousePositionZones.FirstOrDefault(pz => pz.Zone.Id == t.ShelfId);
        //                    if (foundShelf != null)
        //                    {
        //                        used = foundShelf.Quantity;
        //                    }
        //                }


        //                if (warehouseDocumentPositionCandidate.Quantity <= t.Quantity - used)
        //                {

        //                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, t.UnitPrice,
        //                                                                                                    -warehouseDocumentPositionCandidate.Quantity,
        //                                                                                                    t.PositionId, (int)(t.OrderId ?? 0));

        //                    warehouseDocumentPositionCandidate.Quantity = 0;
        //                }
        //                else
        //                {
        //                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, t.UnitPrice,
        //                                                                                                    (double)-t.Quantity, t.PositionId, (int)(t.OrderId ?? 0));

        //                    warehouseDocumentPositionCandidate.Quantity -= (double)t.Quantity;
        //                }

        //                if (warehouseDocumentPosition.Quantity == 0) continue;

        //                if (positionsToGive.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id) == 1)
        //                {
        //                    PositionWithQuantityAndShelfs positionsWithShelfsWhichExists =
        //                        positionsToGive.First(
        //                            p =>
        //                            p.position.Material.Id == warehouseDocumentPosition.Material.Id);

        //                    if (Math.Abs(positionsWithShelfsWhichExists.position.Quantity + warehouseDocumentPosition.Quantity) <= materializedStack.Sum(m => m.Quantity))
        //                    {
        //                        positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
        //                        positionsWithShelfsWhichExists.documentParentIds.Add(t.WarehouseDocumentId);
        //                        positionsWithShelfsWhichExists.positionParentIds.Add(new ChildParentPositionQuantity()
        //                        {
        //                            PositionParentID = t.PositionId,
        //                            Quantity = -warehouseDocumentPosition.Quantity,
        //                            LocationId = t.ShelfId
        //                        });

        //                        var positionWithZone =
        //                            positionsWithShelfsWhichExists.WarehousePositionZones.FirstOrDefault(pswz => pswz.Zone.Id == t.ShelfId);
        //                        if (positionWithZone != null)
        //                        {
        //                            positionWithZone.Quantity += -warehouseDocumentPosition.Quantity;
        //                        }
        //                        else
        //                        {
        //                            Zone zone = warehouseDs.GetShelfByIdDataOnly((int)t.ShelfId);

        //                            positionsWithShelfsWhichExists.WarehousePositionZones.Add(new WarehousePositionZone()
        //                            {
        //                                WarehouseDocumentPosition = positionsWithShelfsWhichExists.position,
        //                                Zone = zone,
        //                                Quantity = -warehouseDocumentPosition.Quantity
        //                            });
        //                        }

        //                    }
        //                    else
        //                    {
        //                        warehouseDocumentPositionCandidate.Quantity += (double)t.Quantity;
        //                    }
        //                }
        //                else
        //                {
        //                    List<WarehousePositionZone> warehousePositionZones = null;

        //                    Zone zone = warehouseDs.GetShelfByIdDataOnly((int)t.ShelfId);

        //                    if (zone != null)
        //                    {
        //                        warehousePositionZones = new List<WarehousePositionZone>();

        //                        warehousePositionZones.Add(new WarehousePositionZone()
        //                        {
        //                            WarehouseDocumentPosition = warehouseDocumentPosition,
        //                            Zone = zone,
        //                            Quantity = -warehouseDocumentPosition.Quantity
        //                        });
        //                    }


        //                    double? availableQuantityInStack = materializedStack.Sum(m => m.Quantity);

        //                    if (availableQuantityInStack == null) throw new ApplicationException("Something wrong with available quantity for Za");

        //                    if (Math.Abs(warehouseDocumentPosition.Quantity) <= availableQuantityInStack)
        //                    {
        //                        positionsToGive.Add(new PositionWithQuantityAndShelfs()
        //                        {
        //                            position = warehouseDocumentPosition,
        //                            documentParentIds = new List<int>() { t.WarehouseDocumentId },
        //                            positionParentIds = new List<ChildParentPositionQuantity>() { new ChildParentPositionQuantity() { PositionParentID = t.PositionId, Quantity = -warehouseDocumentPosition.Quantity, LocationId = t.ShelfId } },
        //                            WarehousePositionZones = warehousePositionZones
        //                        });
        //                    }
        //                    else
        //                    {
        //                        warehouseDocumentPositionCandidate.Quantity += (double)t.Quantity;
        //                    }
        //                }
        //            }

        //            if (warehouseDocumentPositionCandidate.Quantity == 0) break;
        //            if (warehouseDocumentPositionCandidate.Quantity < 0) throw new ApplicationException("Internal application error during ordering material");
        //        }

        //        if (warehouseDocumentPositionCandidate.Quantity > 0)
        //        {
        //            throw new ApplicationException("Not enought material for given position - probably data is inconsistent or a bug in application");
        //        }

        //        Session["GiveMaterial_positionsToGive"] = positionsToGive;

        //        QuantityTextBox.Text = "";
        //        //WarehouseRCB.SelectedIndex = -1;
        //        MaterialsToGiveRadGrid.SelectedIndexes.Clear();
        //        MaterialsToGiveRadGrid.Rebind();
        //        MaterialsWhichWillBeGivenRadGrid.Rebind();
        //    }
        //}

        protected void AddMaterialToGiveClick(object sender, EventArgs e)
        {
            WarehouseDocumentPosition warehouseDocumentPosition;

            if (IsValid)
            {
                GridDataItem item = MaterialsToGiveRadGrid.SelectedItems[0] as GridDataItem;

                string orderNumber = item.GetCellValueAs<string>(ORDERNUMBER_IN_GRID);
                var valuePerUnit = item.GetCellValueAs<decimal>(UNITPRICE_IN_GRID);
                var quantity = decimal.Parse(QuantityTextBox.Text);
                int materialId = item.GetCellValueAs<int>(MATERIAL_ID_IN_GRID);

                using (var warehouseDs = new WarehouseDS())
                {
                    int orderId = 0;
                    Mag.Domain.Model.Order order = orderNumber != "" ? warehouseDs.GetAllOrders().FirstOrDefault(p => p.OrderNumber == orderNumber) : null;
                    if (order != null)
                        orderId = order.Id;

                    warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPositionDataOnly(materialId, valuePerUnit, -quantity, 0, orderId);

                    if (positionsToGive.Count(p => p.position.Material.Id == materialId
                                    && (p.position.Order != null && p.position.Order.OrderNumber == orderNumber)
                                    && p.position.UnitPrice == valuePerUnit) == 1)
                    {
                        PositionWithQuantityAndShelfs positionsWithShelfsWhichExists = positionsToGive.First(p => p.position.Material.Id == materialId
                                                                      && p.position.Order.OrderNumber == orderNumber.ToString()
                                                                      && p.position.UnitPrice == valuePerUnit);

                        positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
                    }
                    else
                    {
                        positionsToGive.Add(new PositionWithQuantityAndShelfs()
                                    {
                                        position = warehouseDocumentPosition,
                                        order = warehouseDocumentPosition.Order,
                                        documentParentIds = new List<int>(),
                                        positionParentIds = new List<ChildParentPositionQuantity>(),
                                        MaterialId = warehouseDocumentPosition.Material.Id,
                                        UnitPrice = warehouseDocumentPosition.UnitPrice,
                                        Quantity = (decimal)warehouseDocumentPosition.Quantity
                                    });
                    }
                }

                Session["GiveMaterial_positionsToGive"] = positionsToGive;

                QuantityTextBox.Text = "";

                MaterialsToGiveRadGrid.SelectedIndexes.Clear();
                MaterialsWhichWillBeGivenRadGrid.Rebind();
                MaterialsToGiveRadGrid.Rebind();

                btnSaveDocument.Enabled = true;
            }
        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int userId;

                if (positionsToGive.Count > 0)
                {
                    int docSubtypeSIMPLE;

                    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId) && int.TryParse(RadComboBox_DocSubtypeSIMPLE.SelectedValue, out docSubtypeSIMPLE))
                    {
                        WarehouseDocument warehouseDocument;

                        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            int sourceWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                            //var positionsQuery = from p in positionsToGive
                            //                     select p.position;

                            //List<WarehouseDocumentPosition> positionsToAdd = new List<WarehouseDocumentPosition>(positionsQuery);
                            lock (typeof(WarehouseDS))
                            {
                                try
                                {
                                    int consumerId = int.Parse(CompanyClientRCB.SelectedValue);

                                    warehouseDocument = warehouseDs.ExternalGive(sourceWarehoseId,
                                                           warehouseDs.GetPersonForSystemUser(
                                                             userId).Id, positionsToGive,
                                                           consumerId,
                                                           inputDate.SelectedDate ??
                                                           DateTime.Now.Date, CheckBox_ExportToSIMPLE.Checked,
                                                           docSubtypeSIMPLE);

                                    positionsToGive = new List<PositionWithQuantityAndShelfs>();
                                    Session["GiveMaterial_positionsToGive"] = positionsToGive;
                                    MaterialsToGiveRadGrid.Rebind();
                                    //MaterialsWhichWillBeGivenRadGrid.Rebind();

                                    DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument,
                                                           WarehouseDocumentTypeEnum.Wz,
                                                           warehouseDocument.Id);

                                    Label_SourceZones.Text = warehouseDs.GetDocumentSourceZonesInfoFormatted(warehouseDocument.Id);

                                    //look below //var name = ""; // warehouseDocument.Warehouse1.Name (this crashes), should use warehouseDs.ContractingParty, but there is no time at the moment
                                    WebTools.AlertMsgAdd("Dokument został zapisany. Materiały zostały wydane");// na <b>" +  name + "</b>");
                                }
                                catch (Exception ex)
                                {
                                    WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                                }
                            }
                        }



                        btnPrintDocument.Enabled = true;
                        btnSaveDocument.Enabled = false;
                        btnAddPosition.Enabled = false;

                        SearchMaterialButton.Enabled = false;
                        MaterialsToGiveRadGrid.Enabled = false;
                        QuantityTextBox.Enabled = false;
                        MaterialsWhichWillBeGivenRadGrid.Enabled = false;
                        CompanyClientRCB.Enabled = false;
                        inputDate.Enabled = false;

                        RadComboBox_DocSubtypeSIMPLE.Enabled = false;

                        //Session["GiveMaterial_positionsToGive"] = null;
                    }
                }
                else
                {
                    WebTools.AlertMsgAdd("Brak pozycji");
                }
            }
        }

        protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = decimal.Parse(QuantityTextBox.Text) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (MaterialsToGiveRadGrid.SelectedItems.Count == 1)
            {
                GridDataItem item = MaterialsToGiveRadGrid.SelectedItems[0] as GridDataItem;
                decimal quantity = decimal.Parse(QuantityTextBox.Text.Replace(".", ","));

                decimal availableQuantity = item.GetCellValueAs<decimal>(QUANTITY_IN_GRID);

                if (quantity > availableQuantity)
                {
                    args.IsValid = false;
                    return;
                }
            }

            args.IsValid = true;
        }

        protected void DocSubtypeSIMPLE_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = int.Parse(RadComboBox_DocSubtypeSIMPLE.SelectedValue) > 0 ? true : false;
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        protected void MaterialsToReceiveRadGrid_DeleteRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                positionsToGive.RemoveAt(e.Item.ItemIndex);
                Session["GiveMaterial_positionsToGive"] = positionsToGive;
                MaterialsWhichWillBeGivenRadGrid.Rebind();

                if (positionsToGive.Count == 0)
                {
                    btnSaveDocument.Enabled = false;
                }
            }
            else
            {
                throw new ApplicationException("Unknown grid command");
            }
        }

        protected void MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (positionsToGive != null)
            {
                var source2 = from p in positionsToGive
                              select
                                new
                                {
                                    Id = 0,
                                    MaterialName = p.position.Material.Name,
                                    IndexSIMPLE = p.position.Material.IndexSIMPLE, //.MaterialSIMPLE.FirstOrDefault(ms => ms.Material.Id == p.position.Material.Id) != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().IndexSIMPLE : null,
                                    NameSIMPLE = p.position.Material.Name, //.MaterialSIMPLE.FirstOrDefault(ms => ms.Material.Id == p.position.Material.Id) != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().NameSIMPLE : null,
                                    Quantity = -p.position.Quantity,
                                    UnitName = p.position.Material.Unit.Name,
                                    UnitPrice = p.position.UnitPrice,
                                    //ShelfName = p.shelf != null ? p.shelf.Name : null
                                };

                MaterialsWhichWillBeGivenRadGrid.DataSource = source2;
            }
        }

        protected void CustomValidator_ContractingPartySimple_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!string.IsNullOrEmpty(CompanyClientRCB.SelectedValue) && int.Parse(CompanyClientRCB.SelectedValue) > 1)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.CompanyClientRCB.Text = "";
        }



        protected void NewDocumentClick(object sender, EventArgs e)
        {
            Session["GiveMaterial_positionsToGive"] = new List<PositionWithQuantityAndShelfs>();
            positionsToGive = (List<PositionWithQuantityAndShelfs>)Session["GiveMaterial_positionsToGive"];

            MaterialsToGiveRadGrid.Rebind();

            this.MaterialsWhichWillBeGivenRadGrid.Rebind();

            QuantityTextBox.Text = "";

            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = false;
            btnAddPosition.Enabled = true;

            SearchMaterialButton.Enabled = true;
            MaterialsToGiveRadGrid.Enabled = true;
            QuantityTextBox.Enabled = true;
            MaterialsWhichWillBeGivenRadGrid.Enabled = true;
            CompanyClientRCB.Enabled = true;
            inputDate.Enabled = true;

            RadComboBox_DocSubtypeSIMPLE.Enabled = true;

            Label_SourceZones.Text = "";
        }


    }
}
