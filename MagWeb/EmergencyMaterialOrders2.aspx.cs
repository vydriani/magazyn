﻿using System;
using System.Linq;
using System.Web.UI;
using Mag.Domain;
using Telerik.Web.UI;

namespace MagWeb
{
  public partial class EmergencyMaterialOrders2 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
      }
    }

    private void ordersRCB_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
      var orderNumber = ordersRCB.SelectedItem != null ? ordersRCB.SelectedItem.Text : null;
      if (!String.IsNullOrEmpty(orderNumber))
      {
        int companyId = (!orderNumber.Contains('M') && !orderNumber.Contains('E'))
                            ? 1 //  Willson
                            : (!orderNumber.Contains('E'))
                                  ? 2   //  Mold
                                  : 3;  //  Metal
        materialsRCB.WebServiceSettings.Method = "GetTelericMaterialsListCompany" + companyId.ToString();
      }
    }

    protected void EmergencyMaterialOrders2Grid_OnNeededDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        int orderId = 0;
        int.TryParse(ordersRCB.SelectedValue, out orderId);

        int materialId = 0;
        int.TryParse(materialsRCB.SelectedValue, out materialId);

        int personId = 0;
        int.TryParse(personsRCB.SelectedValue, out personId);

        string technologistPersonName = technologistPersonsRCB.SelectedItem != null
                                            ? technologistPersonsRCB.SelectedItem.Text
                                            : null;

        if (orderId > 0)
        {
          var warehouseDocumentPositions = warehouseDs
              .GetEmergencyMaterialOrders
              (orderId,
                (materialId > 0
                      ? (int?)materialId
                      : null),
                (personId > 0
                      ? (int?)personId
                      : null),
                technologistPersonName
              )
              .Select((o, i) => new
              {
                Number = i,
                IndexSIMPLE = o.Material.IndexSIMPLE,
                MaterialName = o.Material.Name,
                QuantityAbs = Math.Abs(o.Quantity),
                UnitShortName = o.Material.Unit.ShortName,
                OrderNumber = o.Order.OrderNumber,
                WarehouseDocumentTimeStamp = o.WarehouseDocument.TimeStamp,
                PersonName = o.WarehouseDocument.Person.PersonalId,
                Technologist = o.Order.Productions.First().Technologist
              })
              .OrderByDescending(p => p.Number)//.Id)
              .ToList();

          //DataField="Number"           
          //DataField="Material.IndexSIMPLE"
          //DataField="Material.Name"
          //DataField="QuantityAbs"
          //DataField="Material.Unit.ShortName"
          //DataField="Order.OrderNumber"
          //DataField="WarehouseDocument.TimeStamp"
          //DataField="WarehouseDocument.Person.PersonName"
          //DataField="Order.Productions[0].Technologist"

          //int i = 1;
          //foreach (var warehouseDocumentPosition in warehouseDocumentPositions)
          //{
          //    warehouseDocumentPosition.Number = i;
          //    i++;
          //}

          EmergencyMaterialOrders2Grid.DataSource = warehouseDocumentPositions;
        }
      }
    }

    protected void EmergencyMaterialOrders2Grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
      if (e.Item is GridDataItem)
      {
      }
    }

    protected void Button_ApplyFilterClick(object sender, EventArgs e)
    {
      EmergencyMaterialOrders2Grid.Rebind();
    }

    protected void ButtonReloadClick(object sender, EventArgs e)
    {
      Response.Redirect(Request.RawUrl);
    }
  }
}