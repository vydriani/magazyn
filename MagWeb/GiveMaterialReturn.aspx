﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WarehouseMaster.Master" AutoEventWireup="true" CodeBehind="GiveMaterialReturn.aspx.cs" Inherits="MagWeb.GiveMaterialReturn1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
         <!--
            function RowSelected(sender, args) {
                document.getElementById("<%= QuantityTextBox.ClientID %>").value = args.getDataKeyValue("Quantity");
            }

            function GetFirstDataItemKeyValues() {
                var firstDataItem = $find("<%= MaterialsToGiveRadGrid.MasterTableView.ClientID %>").get_dataItems()[0];
                var keyValues =
                    'Quantity = "' + firstDataItem.getDataKeyValue("Quantity") + '"';
                alert(keyValues);
            }
            -->
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
        <p align="center" style="color: Green; font-size: 18px; font-weight: bold;">
        Zwrot Towaru do Dostawcy
    </p>
    <asp:Panel ID="Panel_EntirePageContent" runat="server">
        <table>
            <tr>
                <td valign="top" style="width: 50%; padding-right: 10px">
                        <table>
                            <tr>
                                <td>
									<asp:Label ID="Label4" runat="server" Text="Nazwa materiału" CssClass="labelSearch"></asp:Label>
                                    <telerik:RadTextBox ID="MaterialToSearchRTB" runat="server">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
									<asp:Label ID="Label5" runat="server" Text="Indeks SIMPLE" CssClass="labelSearch"></asp:Label>
                                    <telerik:RadTextBox ID="IndexSimpleRTB" runat="server">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
									<asp:Label ID="Label1" runat="server" Text="Przypisanie" CssClass="labelSearch"></asp:Label>
                                    <telerik:RadTextBox ID="OrderNumberToSearchRTB" runat="server">
                                    </telerik:RadTextBox>
                                </td>
								<td>
									<asp:Label ID="Label2" runat="server" Text="Wymagane do" CssClass="labelSearch"></asp:Label>
                                    <telerik:RadDatePicker ID="RadDatePicker_DateTo" runat="server">
                                    </telerik:RadDatePicker>
                                </td>
								<td style="text-align: right; vertical-align: bottom;">
									<asp:Button ID="SearchMaterialButton" runat="server" OnClick="SearchMaterialClick"
                                        Text="Wyszukaj materiał" Enabled="True" />
								</td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <telerik:RadGrid ID="MaterialsToGiveRadGrid" runat="server" OnNeedDataSource="MaterialsToGiveRadGrid_OnNeedDataSource">
                                        <GroupingSettings GroupContinuesFormatString=" Pozostałe elementy na następnej stronie."
                                            GroupContinuedFormatString="Pozostałe elementy z poprzedniej strony. " GroupSplitDisplayFormat="{0} z {1} elementów." />
                                        <MasterTableView EnableViewState="true" ClientDataKeyNames="Quantity">
                                            <Columns>
                                                <telerik:GridBoundColumn HeaderText="Lp" UniqueName="Lp" DataField="Lp" />
                                                <telerik:GridBoundColumn HeaderText="Dostawca" UniqueName="ContractingPartyName" DataField="ContractingPartyName" />
                                                <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                <telerik:GridBoundColumn HeaderText="Nazwa" UniqueName="MaterialName" DataField="MaterialName" />
                                                <telerik:GridBoundColumn HeaderText="Ilość do zwrotu" UniqueName="Quantity" DataField="Quantity" />
                                                <telerik:GridBoundColumn HeaderText="J.m." UniqueName="UnitName" DataField="UnitName" />
                                                <telerik:GridBoundColumn HeaderText="Przypisanie" UniqueName="OrderNumber" DataField="OrderNumber" />
                                                <telerik:GridBoundColumn HeaderText="Lokalizacja" UniqueName="ShelfName" DataField="ShelfName" />
                                                <telerik:GridBoundColumn HeaderText="Data wydania zwrotu" UniqueName="TimeRequired" DataField="TimeRequired" />
                                                <telerik:GridBoundColumn HeaderText="" UniqueName="MaterialId" DataField="MaterialId" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="" UniqueName="WarehouseDocumentId" DataField="WarehouseDocumentId" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="" UniqueName="WarehouseDocumentPositionId" DataField="WarehouseDocumentPositionId" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="" UniqueName="WarehouseDocumentPositionKCStateId" DataField="WarehouseDocumentPositionKCStateId" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="" UniqueName="ContractingPartyId" DataField="ContractingPartyId" Display="false" />
                                                <telerik:GridBoundColumn HeaderText="Nr zam. do dost." UniqueName="OrderNumberFromPZ" DataField="OrderNumberFromPZ" Visible="true" />
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Selecting AllowRowSelect="True" />
                                            <ClientEvents OnRowSelected="RowSelected" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                </td>
                <td valign="top" style="width: 50%">
					<div class="inputPanel">
						<table>
							<tr>
								<td style="width: 150px; vertical-align: top">
									<asp:Label ID="Label3" runat="server" Text="Ilość do zwrotu:"></asp:Label>
								</td>
								<td>
									<asp:TextBox ID="QuantityTextBox" runat="server" Enabled="False"></asp:TextBox>
									<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ilość jest wymagana"
										ControlToValidate="QuantityTextBox" ValidationGroup="AddPositionGroup"></asp:RequiredFieldValidator>
									<asp:CustomValidator ID="DataTypeValidator" runat="server" ErrorMessage="Wydawana ilość musi być liczbą dadatnią"
										ValidationGroup="AddPositionGroup" OnServerValidate="DataTypeValidator_ServerValidate"></asp:CustomValidator>
									<asp:CustomValidator ID="QuantityValidator" runat="server" ErrorMessage="Nie można wydać więcej niż jest dostępne"
										ValidationGroup="AddPositionGroup" OnServerValidate="Quantity_ServerValidate"></asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:Button ID="btnAddPosition" runat="server" OnClick="AddMaterialToGiveClick" Text="Przygotuj dokument"
										Enabled="True" ValidationGroup="AddPositionGroup" />
								</td>
							</tr>
						</table>
					</div>


                    <div class="inputPanel">
                       
                                    <telerik:RadGrid ID="MaterialsWhichWillBeGivenRadGrid" runat="server" OnNeedDataSource="MaterialsWhichWillBeGivenRadGrid_OnNeedDataSource"
                                        OnItemCommand="MaterialsToReceiveRadGrid_DeleteRow">
                                        <MasterTableView EnableViewState="true">
                                            <Columns>
                                                <telerik:GridBoundColumn HeaderText="Indeks" UniqueName="IndexSIMPLE" DataField="IndexSIMPLE" />
                                                <telerik:GridBoundColumn HeaderText="Nazwa" UniqueName="MaterialName" DataField="MaterialName" />
                                                <telerik:GridBoundColumn HeaderText="Ilość" UniqueName="Quantity" DataField="Quantity" />
                                                <telerik:GridBoundColumn HeaderText="Jednostka" UniqueName="UnitName" DataField="UnitName" />
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn UniqueName="EditCommandColumn1">
                                                </EditColumn>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                         <table style="margin-top: 20px;">
                            <tr>
                                <td>
                                    <asp:Label ID="labelDate" runat="server" Text="Data dokumentu:"></asp:Label>
                                </td>
                                <td>
									<telerik:RadDatePicker ID="inputDate" runat="server" DateInput-DateFormat="yyyy-MM-dd">
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td>
									Eksportuj do SIMPLE
                                </td>
                                <td >
                                    <asp:CheckBox ID="CheckBox_ExportToSIMPLE" runat="server" Checked="true" Enabled="false" />
										<asp:Label ID="Label_ExportToSIMPLE" runat="server" Text="Eksport do SIMPLE niemożliwy"
                                        ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
								</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 20px;">
									<asp:Button ID="btnSaveDocument" runat="server" OnClick="SaveDocumentClick"
                                        Text="Wydaj" Enabled="False" ValidationGroup="GiveMaterialGroup"
                                        ToolTip="Zapisz dokument" AlternateText="Zapisz dokument" />
                                    <asp:Button ID="btnPrintDocument" runat="server" Text="Drukuj dokument" Enabled="False" />
                                    <asp:Button ID="btnNewDocument" runat="server" Text="Nowy dokument" OnClick="NewDocumentClick" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="Label_SourceZones" runat="server" Text="" Visible="False"></asp:Label>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
