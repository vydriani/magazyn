using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb
{
  public partial class BalanceSheet : FeaturedSystemWebUIPage
  {
    private const int RECEIVE_MATERIAL_INDEX_IN_TABS = 0;
    private const int BALANCE_SHEET_INDEX_IN_TABS = 1;
    private const int INTERNAL_RECEIVE_ORDERED_MATERIAL_INDEX_IN_TABS = 2;
    private const int INTERNAL_RECEIVE_MATERIAL_INDEX_IN_TABS = 3;
    private const int INTERNAL_RECEIVE_GARBAGE_INDEX_IN_TABS = 4;

    private List<PositionWithShelf> positionsWithShelfsToAdd;

    protected void Page_Load(object sender, EventArgs e)
    {
      int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
      int? userId = (int?)Session[SessionData.SessionKey_UserId];
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        string message = "";
        if (currentWarehouseId == null || userId == null ||
            (message = warehouseDs.CheckOperationAvailability(currentWarehouseId, currentWarehouseId, WarehouseDocumentTypeEnum.BO, userId)) != "")
        {
          Session[SessionData.SessionKey_Message] = message;
          PermissionDenied();
          return;
        }
      }

      if (!IsPostBack)
      {
        Session["BSpositionsWithShelfsToAdd"] = null;
      }

      //if(!Page.IsPostBack)
      {
        if (Session["BSpositionsWithShelfsToAdd"] == null)
          positionsWithShelfsToAdd = new List<PositionWithShelf>();
        else
          positionsWithShelfsToAdd = (List<PositionWithShelf>)Session["BSpositionsWithShelfsToAdd"];

        //if (Session["shelfsToStore"] == null)
        //    shelfsToStore = new List<int>();
        //else
        //    shelfsToStore = (List<int>)Session["shelfsToStore"];

      }
      //MaterialsToReceiveRadGrid.DataSource = from p in data
      //                                       select new { RowNumber = p, MaterialName = "ala" };
      //   MaterialsToReceiveRadGrid.MasterTableView.EditMode=
      //            SearchStackPositions.WarehouseId = 4;
      //SearchStackPositions.CurrentStack = warehouseDs.GetMaterials;
      //SearchStackPositions.ShowDateInput = false;
      //SearchStackPositions.ShowOrderNumberInput = false;
      //SearchStackPositions.ShowShelfCombo = true;


      //using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      //{
      //    WarehouseRCB.DataSource = from w in warehouseDs.GetAllWarehouses() select new { Value = w.Id, Text = w.Name };
      //    WarehouseRCB.DataTextField = "Text";
      //    WarehouseRCB.DataValueField = "Value";
      //    WarehouseRCB.DataBind();
      //    WarehouseRCB.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));
      //}


      if (!IsPostBack)
      {
        int materialId;
        int orderId;
        int zoneId;

        if (
            !string.IsNullOrEmpty(Request["o"]) && Request["o"] == "x"
          //&& !string.IsNullOrEmpty(Request["w"]) && int.Parse(Request["w"]) > 0
            && !string.IsNullOrEmpty(Request["material"]) && int.TryParse(Request["material"], out materialId)
            && !string.IsNullOrEmpty(Request["order"]) && int.TryParse(Request["order"], out orderId)
            && !string.IsNullOrEmpty(Request["zone"]) && int.TryParse(Request["zone"], out zoneId)
            && !string.IsNullOrEmpty(Request["status"])
            )
        {
          ValuePerUnit.Enabled = false;
          PanelQuantityOnStacks.Visible = true;

          using (var warehouseDs = new WarehouseDS())
          {
            Material material = warehouseDs.GetMaterialById(materialId);
            Order order = orderId > 0 ? warehouseDs.GetOrderByIdDataOnly(orderId) : null;
            Zone zone = zoneId > 0 ? warehouseDs.GetZoneByIdDataOnly(zoneId) : null;

            IndexSIMPLEToSearchRTB.Text = material.IndexSIMPLE;
            if (order != null)
            {
              RadTextBox_OrderNumberToSearch.Text = order.OrderNumber;
            }

            string zoneName = zone != null ? zone.Name : "";


            ShelfToFind.Text = zoneName;


            var stacksOfMaterial = warehouseDs.GetCurrentStackInDocumentsNew(material.Name, "", (int?)Session[SessionData.SessionKey_CurrentWarehouseId], zoneId > 0 ? zoneId : (int?)null, order == null, false, order != null ? order.OrderNumber : "", 100, true);


            string damageStatus = Request["status"];

            double? quantity = stacksOfMaterial.Where(p => p.DamageStatus == damageStatus).Sum(p => p.Quantity); //p.ShelfName == zoneName && 

            QuantityOnStacks.Text = quantity.ToString();
            QuantityOnStacksUnit.Text = material.Unit.ShortName;

            Quantity.Text = quantity.ToString();

            decimal? unitPrice = stacksOfMaterial.Average(p => p.AverageUnitPrice);

            ValuePerUnit.Text = unitPrice.ToString();
          }
        }
        else
        {
          ValuePerUnit.Enabled = true;
          PanelQuantityOnStacks.Visible = false;
        }
      }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        if (MaterialGrid.MasterTableView.Items.Count == 1)
        {
          MaterialGrid.MasterTableView.Items[0].Selected = true;
        }

        if (ShelfRadGrid.MasterTableView.Items.Count == 1)
        {
          ShelfRadGrid.MasterTableView.Items[0].Selected = true;
        }

        if (OrderRadGrid.MasterTableView.Items.Count == 1)
        {
          OrderRadGrid.MasterTableView.Items[0].Selected = true;
        }

        Quantity.Focus();

        PanelBack.Visible = PanelQuantityOnStacks.Visible;
      }

      this.btnSaveDocument.Visible = this.btnSaveDocument.Enabled;
    }

    protected void Tab_Prerender(object sender, EventArgs e)
    {
      int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
      int? userId = (int?)Session[SessionData.SessionKey_UserId];

      //using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      //{
      //    RadToolBar1.Tabs[RECEIVE_MATERIAL_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
      //                                                                (
      //                                                                    warehouseDs.CheckOperationAvailability(
      //                                                                        null,
      //                                                                        currentWarehouseId.Value,
      //                                                                        WarehouseDocumentTypeEnum.Pz,
      //                                                                        userId, null) == "" ||
      //                                                                    warehouseDs.CheckOperationAvailability(
      //                                                                        null,
      //                                                                        currentWarehouseId.Value,
      //                                                                        WarehouseDocumentTypeEnum.PzKC,
      //                                                                        userId, null) == "" ||
      //                                                                    warehouseDs.CheckOperationAvailability(
      //                                                                        0,
      //                                                                        currentWarehouseId.Value,
      //                                                                        WarehouseDocumentTypeEnum.MMPlus,
      //                                                                        userId, null) == "")
      //                                                               );

      //    RadToolBar1.Tabs[BALANCE_SHEET_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
      //                                                                warehouseDs.CheckOperationAvailability(
      //                                                                    null,
      //                                                                    currentWarehouseId.Value,
      //                                                                    WarehouseDocumentTypeEnum.BO,
      //                                                                    userId, null) == "");

      //    RadToolBar1.Tabs[INTERNAL_RECEIVE_ORDERED_MATERIAL_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
      //                                                                warehouseDs.CheckOperationAvailability(
      //                                                                    0,
      //                                                                    currentWarehouseId.Value,
      //                                                                    WarehouseDocumentTypeEnum.MMPlus,
      //                                                                    userId, null) == "");

      //    RadToolBar1.Tabs[INTERNAL_RECEIVE_MATERIAL_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
      //                                                                warehouseDs.CheckOperationAvailability(
      //                                                                    0,
      //                                                                    currentWarehouseId.Value,
      //                                                                    WarehouseDocumentTypeEnum.MMPlus,
      //                                                                    userId, null) == "");

      //    RadToolBar1.Tabs[INTERNAL_RECEIVE_GARBAGE_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
      //                                                                warehouseDs.CheckOperationAvailability(
      //                                                                    0,
      //                                                                    currentWarehouseId.Value,
      //                                                                    WarehouseDocumentTypeEnum.POPlus,
      //                                                                    userId, null) == "");
      //}

    }

    protected void AddPositionClick(object sender, EventArgs e)
    {
      WarehouseDocumentPosition warehouseDocumentPosition;

      int materialId;

      if (IsValid)
      {
        GridItem item = MaterialGrid.SelectedItems[0];
        var cell = item.Cells[2];
        materialId = int.Parse(cell.Text);

        var valuePerUnit = decimal.Parse(ValuePerUnit.Text.Replace(".", ","));
        var quantity = decimal.Parse(Quantity.Text.Replace(".", ","));
        //int parentWarehouseDocumentPosition = int.Parse(item.Cells[10].Text);

        string shelfName = null;
        int shelfId = -1;

        if (ShelfRadGrid.SelectedItems.Count == 1)
        {
          GridItem shelfItem = ShelfRadGrid.SelectedItems[0];

          shelfId = int.Parse(shelfItem.Cells[2].Text);
          shelfName = shelfItem.Cells[3].Text;
        }

        string orderNumber = null;
        int orderId = -1;

        if (OrderRadGrid.SelectedItems.Count == 1)
        {
          GridItem orderItem = OrderRadGrid.SelectedItems[0];

          orderId = int.Parse(orderItem.Cells[2].Text);
          orderNumber = orderItem.Cells[3].Text;
        }

        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        {
          warehouseDocumentPosition = warehouseDs.CreateWarehouseDocumentPosition(materialId, valuePerUnit, quantity);//.CreateWarehouseDocumentPositionDataOnly(materialId, valuePerUnit, quantity);

          if (orderId > 0)
            warehouseDocumentPosition.Order = warehouseDs.GetOrderByIdDataOnly(orderId);

        }



        PositionWithShelf positionsWithShelfsWhichExists =
            positionsWithShelfsToAdd.FirstOrDefault(
                p =>
                p.position.Material.Id == warehouseDocumentPosition.Material.Id
                &&
                p.position.UnitPrice == warehouseDocumentPosition.UnitPrice
                &&
                (p.position.Order == null && warehouseDocumentPosition.Order == null || (p.position.Order != null && warehouseDocumentPosition.Order != null && p.position.Order.Id == warehouseDocumentPosition.Order.Id)));

        if (positionsWithShelfsWhichExists != null) //positionsWithShelfsToAdd.Count(p => p.position.Material.Id == warehouseDocumentPosition.Material.Id && p.position.UnitPrice == warehouseDocumentPosition.UnitPrice && p.shelf.Id == shelfId) == 1)
        {
          //PositionWithShelf positionsWithShelfsWhichExists =
          //    positionsWithShelfsToAdd.First(
          //        p =>
          //        p.position.Material.Id == warehouseDocumentPosition.Material.Id 
          //        &&
          //        p.position.UnitPrice == warehouseDocumentPosition.UnitPrice
          //        &&
          //        (p.position.Order == null && warehouseDocumentPosition.Order == null ||  (p.position.Order != null && warehouseDocumentPosition.Order != null && p.position.Order.Id == warehouseDocumentPosition.Order.Id)));

          positionsWithShelfsWhichExists.position.Quantity += warehouseDocumentPosition.Quantity;
        }
        else
        {
          positionsWithShelfsToAdd.Add(new PositionWithShelf()
          {
            position = warehouseDocumentPosition,
            shelf = shelfId != -1 ? new Zone() { Id = shelfId, Name = shelfName } : null,
            order = warehouseDocumentPosition.Order
          });
        }

        Session["BSpositionsWithShelfsToAdd"] = positionsWithShelfsToAdd;

        Quantity.Text = "";
        ValuePerUnit.Text = "";
        MaterialGrid.SelectedIndexes.Clear();
        MaterialsToReceiveRadGrid.Rebind();
      }
    }

    protected void SaveDocumentClick(object sender, EventArgs e)
    {
      if (IsValid)
      {
        int userId;
        if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
        {
          using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
          {
            //warehouseDs.ExternalReceive(int.Parse(WarehouseRCB.Items.FindItemByText(WarehouseRCB.Text).Value), userId, position, null);
            int currentWarehoseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

            //var positionsQuery = from p in positionsWithShelfsToAdd
            //                     select p.position;

            //List<WarehouseDocumentPosition> positionsToAdd = new List<WarehouseDocumentPosition>(positionsQuery);

            try
            {
              warehouseDs.OpeningBalance(currentWarehoseId, warehouseDs.GetPersonForSystemUser(userId).Id, positionsWithShelfsToAdd, null, this.CheckBox_AddOnly.Checked);
            }
            catch (Exception ex)
            {
              WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
            }
            //                    warehouseDs.Bind(positionsWithShelfsToAdd);
          }

          positionsWithShelfsToAdd = new List<PositionWithShelf>();
          Session["BSpositionsWithShelfsToAdd"] = positionsWithShelfsToAdd;
          MaterialsToReceiveRadGrid.Rebind();
        }
      }
    }

    protected void SearchMaterialClick(object sender, EventArgs e)
    {
      MaterialGrid.Rebind();
    }

    protected void SearchShelfClick(object sender, EventArgs e)
    {
      ShelfRadGrid.Rebind();
    }

    protected void SearchOrderClick(object sender, EventArgs e)
    {
      OrderRadGrid.Rebind();
    }

    //SearchOrderClick

    protected void ShelfRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        string shelfToSearch = ShelfToFind.Text;
        var shelfs = from p in warehouseDs.GetShelfListFromCache(shelfToSearch)
                     select p;

        ShelfRadGrid.DataSource = shelfs.Take(10).ToArray();
      }
    }

    protected void MaterialsToReceiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      if (positionsWithShelfsToAdd != null)
      {
        int i = 1;
        var source2 = from p in positionsWithShelfsToAdd
                      select
                          new
                          {
                            RowNumber = i++,
                            MaterialName = p.position.Material.Name,
                            IndexSIMPLE = p.position.Material.IndexSIMPLE,//.MaterialSIMPLE.FirstOrDefault(ms => ms.Material.Id == p.position.Material.Id) != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().IndexSIMPLE : null,
                            NameSIMPLE = p.position.Material.Name, //.MaterialSIMPLE.FirstOrDefault(ms => ms.Material.Id == p.position.Material.Id) != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().NameSIMPLE : null,
                            Quantity = p.position.Quantity,
                            UnitName = p.position.Material.Unit.Name,
                            UnitPrice = p.position.UnitPrice,
                            ShelfName = p.shelf.Name,
                            OrderNumber = p.position.Order != null ? p.position.Order.OrderNumber : ""
                          };
        MaterialsToReceiveRadGrid.DataSource = source2;
      }
    }

    protected void OrderRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
      {

        IQueryable<Mag.Domain.Model.Order> filteredOrders = warehouseDS.GetOrdersList(this.RadTextBox_OrderNumberToSearch.Text);


        var allOrdersQuery = filteredOrders.Take(10);

        var allOrdersObjects = allOrdersQuery.ToArray();
        var allOrdersRadComboBoxItemData = from order in allOrdersObjects
                                           orderby order.OrderNumber
                                           select new
                                           {
                                             Number = order.OrderNumber,
                                             Id = order.Id.ToString()
                                           };

        var items = allOrdersRadComboBoxItemData.ToArray();

        OrderRadGrid.DataSource = items;

        //if (items.Count() == 0)
        //{
        //    result.Items = new RadComboBoxItemData[2]
        //                       {
        //                           new RadComboBoxItemData() { Text = "brak wyników1", Value = "1" },
        //                           new RadComboBoxItemData() { Text = "brak wyników2", Value = "2" }
        //                       };
        //}
      }
    }

    //OrderRadGrid_OnNeedDataSource

    protected void MaterialGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
      {
        int companyId = (int)Session[SessionData.SessionKey_CurrentCompanyId];
        //var t=SearchPanel.Items[0].Items[0].Items.Count;
        //string materialToSearch = ((RadTextBox) SearchPanel.FindControl("MaterialToSearchRTB")).Text;
        //string indexSIMPLEToSearch = ((RadTextBox) SearchPanel.FindControl("IndexSIMPLEToSearchRTB")).Text;

        string materialToSearch = MaterialToSearchRTB.Text;
        string indexSIMPLEToSearch = IndexSIMPLEToSearchRTB.Text;
        var materials = from p in warehouseDs.GetMaterials(materialToSearch, indexSIMPLEToSearch, companyId)
                        select
                            new
                            {
                              Id = p.Id,
                              MaterialName = p.MaterialName,
                              IndexSIMPLE = p.IndexSIMPLE,
                              NameSIMPLE = p.NameSIMPLE,
                              UnitName = p.UnitName,
                            };
        MaterialGrid.DataSource = materials.Take(20).ToArray();
      }
    }

    protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = MaterialGrid.SelectedItems.Count == 1 ? true : false; //co jesli juz jest invalid?
    }

    //
    protected void QuantityChangeCustomValidator_ServerValidate_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = !PanelQuantityOnStacks.Visible || double.Parse(Quantity.Text) != double.Parse(QuantityOnStacks.Text);
    }

    protected void DataTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      try
      {
        args.IsValid = double.Parse(ValuePerUnit.Text.Replace(".", ",")) > 0 ? true : false; //double.Parse(Quantity.Text.Replace(".", ",")) > 0
      }
      catch (Exception)
      {
        args.IsValid = false;
      }
    }

    // OnServerValidate="ShelfValidator_ServerValidate"
    protected void ShelfValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = this.ShelfRadGrid.SelectedItems.Count == 1;
    }

    //PositionsValidator_ServerValidate
    protected void PositionsValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = this.positionsWithShelfsToAdd.Count > 0;
    }

    //MaterialValidator_ServerValidate
    protected void MaterialValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
      args.IsValid = this.MaterialGrid.SelectedItems.Count > 0;
    }

    protected void MaterialsToReceiveRadGrid_DeleteRow(object source, GridCommandEventArgs e)
    {
      if (e.CommandName == "DeleteRow")
      {
        positionsWithShelfsToAdd.RemoveAt(e.Item.ItemIndex);
        Session["BSpositionsWithShelfsToAdd"] = positionsWithShelfsToAdd;
        MaterialsToReceiveRadGrid.Rebind();
      }
      else
      {
        throw new ApplicationException("Unknown grid command");
      }
    }
  }
}
