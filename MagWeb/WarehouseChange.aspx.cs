﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class WarehouseChange : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            panelWarehouse.Visible = false;
        }

        protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var dataItem = DocPZGrid.SelectedItems[0] as GridDataItem;
            if (dataItem != null)
            {
                this.DocId.Value = dataItem["Id"].Text;
                var currentCompanyId = (int?)(Session[SessionData.SessionKey_CurrentCompanyId]);

                LoadWarehouses(currentCompanyId.Value);

                panelWarehouse.Visible = true;
            }
        }


        private void LoadWarehouses(int companyId)
        {
            using (var warehouseDs = new WarehouseDS())
            {
                var warehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

                WarehouseRCB.DataSource = warehouseDs.GetAllWarehouses(companyId)
                    .Where(x => x.Id != warehouseId)
                    .Select(w => new { w.Id, w.Name })
                    .ToArray();
                WarehouseRCB.DataTextField = "Name";
                WarehouseRCB.DataValueField = "Id";
                WarehouseRCB.DataBind();
            }

            WarehouseRCB.SelectedIndex = 0;
        }


        protected void SearchMaterialClick(object sender, EventArgs e)
        {

            GetDocs();
        }

        private void GetDocs()
        {
            this.DocId.Value = "0";

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                DateTime? startDate = RadDatePicker1.SelectedDate;
                DateTime? endDate = RadDatePicker2.SelectedDate.Value.AddDays(1);

                var warehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];
                var docNumber = tbOrderNumberPZ.Text;

                IQueryable<WarehouseDocument> allDocs = warehouseDs.WarehouseEntitiesTest
                    .WarehouseDocuments
                    .Include(x => x.Person)
                    .Include(x => x.WarehouseDocumentRelations)
                    .Where(x => x.DocumentTypeId == 1
                        && x.Number != null
                        && x.WarehouseTargetId == warehouseId
                        && !x.WarehouseDocumentRelations.Any()
                        );

                if (startDate != null)
                {
                    allDocs = allDocs.Where(x => x.TimeStamp >= startDate.Value);

                }

                if (endDate != null)
                {
                    allDocs = allDocs.Where(x => x.TimeStamp <= endDate.Value);
                }

                if (!string.IsNullOrEmpty(docNumber))
                {
                    allDocs = allDocs.Where(x => x.Number.Contains(docNumber));
                }


                if (!string.IsNullOrEmpty(cmbAuthor.SelectedValue))
                {
                    int pId = int.Parse(cmbAuthor.SelectedValue);
                    allDocs = allDocs.Where(x => x.AuthorId == pId);
                }

                var docPZ = allDocs
                    .Select(x => new
                    {
                        Id = x.Id,
                        Number = x.Number,
                        TimeStamp = x.TimeStamp,
                        Person = x.Person.FirstName + " " + x.Person.LastName
                    })
                    .OrderByDescending(x => x.Id)
                    .ToList();

                if (docPZ.Count > 0)
                {
                    if (docPZ.Count > 100)
                        docPZ.Take(100);

                    gridPanel.Visible = true;
                    DocPZGrid.DataSource = docPZ;
                    DocPZGrid.Rebind();
                }
                else
                    gridPanel.Visible = false;
            }
        }


        protected void ChangeWarehouse(object sender, EventArgs e)
        { 
            int Id;
            int newTarget;

            if (int.TryParse(this.DocId.Value, out Id) && int.TryParse(WarehouseRCB.SelectedValue,out newTarget))
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var doc = warehouseDs.WarehouseEntitiesTest.WarehouseDocuments.Where(x => x.Id == Id).Single();
                    doc.WarehouseTargetId = newTarget;

                    var docexc = warehouseDs.WarehouseEntitiesTest.WarehouseDataExchangeDocumentNews
                        .Where(x => x.WarehouseDocumentId == Id).FirstOrDefault();
                    if (docexc != null)
                        docexc.WarehouseTargetId = newTarget;

                    warehouseDs.WarehouseEntitiesTest.SaveChanges();
                    GetDocs();
                
                }
            }

        
        }

    }
}