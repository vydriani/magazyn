﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MagWeb.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILoggingService" in both code and config file together.
    [ServiceContract]
    public interface ILoggingService
    {
        [OperationContract]
        LogError Log(string user, string error, string source, string comment, string logLevel);


    }

    [DataContract]
    public class LogError
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Val { get; set; }
    }
}
