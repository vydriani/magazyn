﻿using System;
using System.IO;
using System.ServiceModel.Activation;
using System.Web;
//using Client.Domain.LoggingService;
using MagWeb.Helpers;

namespace MagWeb.Services
{
    /// <summary>
    /// Servis slużacy do logowanie bledow na serwerze.
    /// Korzysta z instancji klasy NLogger
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class LoggingService : ILoggingService
    {
        private NLogger logger = new NLogger();


        private void WriteEx(string ex)
        {
            string path = HttpContext.Current.Server.MapPath("/log");
            StreamWriter fileStream = new StreamWriter(path + "/LOGERROR.txt", true);
            try
            {
                fileStream.WriteLine(ex);
            }
            finally
            {
                fileStream.Close();
            }
        }


        public LogError Log(string user, string error, string source, string comment, string logLevel)
        {
            try
            {
                comment = comment + " (IP:" + HttpContext.Current.Request.UserHostAddress + ")";

                string logError = logger.Log(user, error, source, comment, logLevel);
                var er = new LogError { Name = logError, Val = 0 };
                return er;
            }
            catch (Exception e)
            {
                WriteEx(e.Message);
                return new LogError { Name = e.Message, Val = 0 };
            }

        }
    }
}
