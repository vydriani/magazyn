﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class ManageWarehouseGroups : FeaturedSystemWebUIPage
    {

        private const int WAREHOUSEGROUP_ID_IN_GRID = 2;
        private const int TARGET_WAREHOUSEGROUP_ID_IN_GRID = 2;
        private const int PERMISSION_ID_IN_GRID = 2;
        private const int DOCUMENT_TYPE_ID_IN_GRID = 3;

        protected void Page_Init(object sender, EventArgs e)
        {
            Session["tab_warehousegroup_mgmt"] = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    DocumentTypeRCB.DataSource = from t in warehouseDs.GetAllWarehouseDocumentTypes()
                                                 select new { Value = t.Id, Text = t.ShortType };
                    DocumentTypeRCB.DataTextField = "Text";
                    DocumentTypeRCB.DataValueField = "Value";
                    DocumentTypeRCB.DataBind();
                    DocumentTypeRCB.Items.Insert(0, new RadComboBoxItem("(wybierz typ dokumentu)", "-1"));


                    AllowedStatuses_CheckboxesSetDefault();
                }
            }

            this.Label_StatusMessage.Text = "";
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.TextBox_Name.Text = "";
            this.TextBox_ShortName.Text = "";

            this.TextBox_Name.Enabled = true;
            this.TextBox_ShortName.Enabled = true;
            this.Button_ChangeName.Enabled = true;
            this.Button_ChangeShortName.Enabled = true;

            this.Panel_CreateWarehouse.Visible = true;


            if ((this.WarehouseGroupsGrid.SelectedItems.Count == 1 && this.TargetWarehouseGroupsGrid.SelectedItems.Count == 1) || (this.DocumentTypeGrid.SelectedItems.Count == 1))
            {
                this.Panel_AllowedStatuses.Visible = true;
            }
            else
            {
                this.Panel_AllowedStatuses.Visible = false;
            }

            if (this.DocumentTypeGrid.SelectedItems.Count == 1)
            {
                this.Button_AllowedStatusesUpdate.Visible = true;


                using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
                {

                    // Getting info about allowed statuses:

                    if (DocumentTypeGrid.SelectedItems.Count == 1)
                    {
                        AllowedStatuses_CheckboxesSetDefault();

                        string permIdInDocTypeGrid = DocumentTypeGrid.SelectedItems[0].Cells[PERMISSION_ID_IN_GRID].Text;

                        int permissionId = int.Parse(permIdInDocTypeGrid);


                        var persmissions = from p in warehouseDS.GetAllWarehouseGroupPermissions() // !!!
                                           where p.Id == permissionId
                                           select p;

                        var wp = persmissions.FirstOrDefault();

                        if (wp != null)
                        {
                            this.CheckBox_Package.Checked = wp.MaterialIsPackage;
                            this.CheckBox_Garbage.Checked = wp.PositionIsGarbage;
                            this.CheckBox_ZZ.Checked = wp.PositionIsZZ;
                            this.CheckBox_Damaged.Checked = wp.PositionIsDamaged;
                            this.CheckBox_PartlyDamaged.Checked = wp.PositionIsPartlyDamaged;
                        }
                    }
                }
            }
            else
            {
                this.Button_AllowedStatusesUpdate.Visible = false;
            }
        }

        protected void CompanyRCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            WarehouseGroupsGrid.Rebind();
            TargetWarehouseGroupsGrid.Rebind();
        }

        protected void WarehouseGroupsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {


                //int company = int.Parse(CompanyRCB.SelectedValue); // Company selection from combo box

                var warehouseGroupsCandidatesToConsider = warehouseDs.GetAllWarehouseGroups();  //GetAllWarehouses(company);

                var warehouseGroupsCandidatesToConsiderOrdered = warehouseGroupsCandidatesToConsider.OrderBy(p => p.Name);

                List<Mag.Domain.Model.WarehouseGroup> warehouseGroupsCandidatesToPresent = new List<Mag.Domain.Model.WarehouseGroup>();

                foreach (var warehouseGroup in warehouseGroupsCandidatesToConsiderOrdered)
                {
                    Mag.Domain.Model.WarehouseGroup waitingForWarehouseGroupToPresent = new Mag.Domain.Model.WarehouseGroup();

                    waitingForWarehouseGroupToPresent.Id = warehouseGroup.Id;
                    waitingForWarehouseGroupToPresent.Name = warehouseGroup.Name;
                    waitingForWarehouseGroupToPresent.ShortName = warehouseGroup.ShortName;

                    warehouseGroupsCandidatesToPresent.Add(waitingForWarehouseGroupToPresent);

                }

                //if (company != -1)
                //{
                    Mag.Domain.Model.WarehouseGroup warehouseGroupNull = new Mag.Domain.Model.WarehouseGroup();
                    warehouseGroupNull.Id = -1;
                    warehouseGroupNull.Name = "(nieokreślona grupa)";
                    warehouseGroupNull.ShortName = "(nieokreślona grupa)";
                    warehouseGroupsCandidatesToPresent.Insert(0, warehouseGroupNull);
                //}

                var warehouseGroupsCandidatesToPresentMaterialized = warehouseGroupsCandidatesToPresent.ToArray();

                var warehouseGroups = from p in warehouseGroupsCandidatesToPresentMaterialized
                                     select
                                         new
                                         {
                                             WarehouseGroupName = p.Name,
                                             WarehouseGroupShortName = p.ShortName,
                                             WarehouseGroupID = p.Id
                                         };

                WarehouseGroupsGrid.DataSource = warehouseGroups;
            }
        }

        protected void WarehouseGroupsGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                WarehouseGroupsGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                //TargetWarehousesGrid.SelectedIndexes.Clear();
                //TargetWarehousesGrid.SelectedIndexes.Add(int.Parse(WarehousesGrid.SelectedIndexes[0]));
                DocumentTypeGrid.Rebind();

                AllowedStatuses_CheckboxesSetDefault();
            }
        }

        protected void TargetWarehouseGroupsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                //int company = int.Parse(CompanyRCB.SelectedValue);

                var warehouseGroups = warehouseDs.GetAllWarehouseGroups().ToList(); ;

                //if (company != -1)
                //{
                Mag.Domain.Model.WarehouseGroup warehouseGroupNull = new Mag.Domain.Model.WarehouseGroup();
                warehouseGroupNull.Id = -1;
                warehouseGroupNull.Name = "(nieokreślona grupa)";
                warehouseGroupNull.ShortName = "(nieokreślona grupa)";
                warehouseGroups.Insert(0, warehouseGroupNull);
                //}

                TargetWarehouseGroupsGrid.DataSource = warehouseGroups;
            }
        }

        protected void TargetWarehouseGroupsGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                TargetWarehouseGroupsGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                DocumentTypeGrid.Rebind();

                AllowedStatuses_CheckboxesSetDefault();
            }
        }

        protected void CustomValidatorShowTypes_ServerValidate(object source, ServerValidateEventArgs e)
        {
            if (WarehouseGroupsGrid.SelectedItems.Count != 1)
            {
                CustomValidatorShowTypes.ErrorMessage = "Nie wybrano grupy źródłowej";
                e.IsValid = false;
            }
            else if (TargetWarehouseGroupsGrid.SelectedItems.Count != 1)
            {
                CustomValidatorShowTypes.ErrorMessage = "Nie wybrano grupy docelowej";
                e.IsValid = false;
            }
            else
            {
                CustomValidatorShowTypes.ErrorMessage = "";
                e.IsValid = true;
            }
        }

        protected void Button_ShowTypes_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                DocumentTypeGrid.Rebind();
            }
        }

        protected void DocumentTypeGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int? sourceWarehouseGroupId = -1;
                if (WarehouseGroupsGrid.SelectedItems.Count == 1)
                {
                    sourceWarehouseGroupId = int.Parse(WarehouseGroupsGrid.SelectedItems[0].Cells[WAREHOUSEGROUP_ID_IN_GRID].Text);
                    sourceWarehouseGroupId = sourceWarehouseGroupId != -1 ? sourceWarehouseGroupId : null;
                }

                int? targetWarehouseGroupId = -1;
                if (TargetWarehouseGroupsGrid.SelectedItems.Count == 1)
                {
                    targetWarehouseGroupId = int.Parse(TargetWarehouseGroupsGrid.SelectedItems[0].Cells[TARGET_WAREHOUSEGROUP_ID_IN_GRID].Text);
                    targetWarehouseGroupId = targetWarehouseGroupId != -1 ? targetWarehouseGroupId : null;
                }

                var permissions =
                    from p in warehouseDs.GetWarehouseGroupPermissions(sourceWarehouseGroupId, targetWarehouseGroupId)
                    join t in warehouseDs.GetAllWarehouseDocumentTypes() on p.WarehouseDocumentType.Id equals t.Id
                    select new
                    {
                        PermissionId = p.Id,
                        TypeId = t.Id,
                        ShortType = t.ShortType,
                        Type = t.Type
                    };

                DocumentTypeGrid.DataSource = permissions.OrderBy(p => p.TypeId).ToArray();
            }
        }

        private void AllowedStatuses_CheckboxesSetDefault()
        {
            this.CheckBox_Package.Checked = true;
            this.CheckBox_Garbage.Checked = false;
            this.CheckBox_ZZ.Checked = true;
            this.CheckBox_Damaged.Checked = true;
            this.CheckBox_PartlyDamaged.Checked = true;

            //this.Button_AllowedStatusesUpdate.Visible = false;
        }

        protected void DocumentTypeGrid_ItemCommand(object source, GridCommandEventArgs e)
        {

        }

        protected void CustomValidatorAddPermission_ServerValidate(object source, ServerValidateEventArgs e)
        {
            if (WarehouseGroupsGrid.SelectedItems.Count != 1)
            {
                CustomValidatorAddPermission.ErrorMessage = "Nie wybrano magazynu źródłowego";
                e.IsValid = false;
            }
            else if (TargetWarehouseGroupsGrid.SelectedItems.Count != 1)
            {
                CustomValidatorAddPermission.ErrorMessage = "Nie wybrano magazynu docelowego";
                e.IsValid = false;
            }
            else if (DocumentTypeRCB.SelectedValue == "-1")
            {
                CustomValidatorAddPermission.ErrorMessage = "Nie wybrano typu dokumentu";
                e.IsValid = false;
            }
            else
            {
                CustomValidatorAddPermission.ErrorMessage = "";
                e.IsValid = true;
            }
        }

        protected void Button_AddPermission_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int? sourceWarehouseGroupId = int.Parse(WarehouseGroupsGrid.SelectedItems[0].Cells[WAREHOUSEGROUP_ID_IN_GRID].Text);
                    sourceWarehouseGroupId = sourceWarehouseGroupId != -1 ? sourceWarehouseGroupId : null;

                    int? targetWarehouseGroupId = int.Parse(TargetWarehouseGroupsGrid.SelectedItems[0].Cells[TARGET_WAREHOUSEGROUP_ID_IN_GRID].Text);
                    targetWarehouseGroupId = targetWarehouseGroupId != -1 ? targetWarehouseGroupId : null;

                    int documentTypeId = int.Parse(DocumentTypeRCB.SelectedValue);


                    warehouseDs.AddWarehouseGroupPermission(sourceWarehouseGroupId, targetWarehouseGroupId, documentTypeId, this.CheckBox_Package.Checked, this.CheckBox_ZZ.Checked, this.CheckBox_Garbage.Checked, this.CheckBox_Damaged.Checked, this.CheckBox_PartlyDamaged.Checked);

                    DocumentTypeGrid.Rebind();
                }
            }
        }

        protected void CustomValidatorRemovePermission_ServerValidate(object source, ServerValidateEventArgs e)
        {
            if (DocumentTypeGrid.SelectedItems.Count != 1)
            {
                CustomValidatorRemovePermission.ErrorMessage = "Nie wybrano typu dokumentu";
                e.IsValid = false;
            }
            else
            {
                CustomValidatorRemovePermission.ErrorMessage = "";
                e.IsValid = true;
            }
        }

        protected void Button_RemovePermission_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int permissionId = int.Parse(DocumentTypeGrid.SelectedItems[0].Cells[PERMISSION_ID_IN_GRID].Text);

                    warehouseDs.RemoveWarehouseGroupPermission(permissionId);

                    DocumentTypeGrid.Rebind();
                }
            }
        }

        protected void Button_ChangeName_Click(object sender, EventArgs e)
        {
            int responseCode;

            if (IsValid)
            {
                if (this.WarehouseGroupsGrid.SelectedItems.Count > 0)
                {
                    GridItem item = this.WarehouseGroupsGrid.SelectedItems[0];


                    var cell = item.Cells[WAREHOUSEGROUP_ID_IN_GRID];
                    int warehouseGroupId = int.Parse(cell.Text);

                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        responseCode = warehouseDs.UpdateWarehouseGroupName(this.TextBox_Name.Text, warehouseGroupId); // !!!
                    }


                    if (responseCode > -1)
                    {
                        //this.Label_Message.Text = "Zmiana " + warehouseId.ToString();

                        //WarehousesGrid.Controls.Clear(); // ?
                        WarehouseGroupsGrid.Rebind();
                        TargetWarehouseGroupsGrid.Rebind();
                        DocumentTypeGrid.Rebind();
                    }
                    else
                    {
                        if (responseCode == -1)
                            this.CustomValidator_Name.ErrorMessage = "Błędny identyfikator grupy";
                        if (responseCode == -2)
                            this.CustomValidator_Name.ErrorMessage = "Grupa o podanej nazwie już istnieje";

                        this.CustomValidator_Name.IsValid = false;
                    }
                }
                else
                {
                    this.CustomValidator_Name.ErrorMessage = "Nie dokonano wyboru grupy";
                    this.CustomValidator_Name.IsValid = false;
                }

            }
            else
            {
                this.CustomValidator_Name.ErrorMessage = "Niezidentyfikowany błąd";
                this.CustomValidator_Name.IsValid = false;
            }
        }


        protected void Button_ChangeShortName_Click(object sender, EventArgs e)
        {
            int responseCode;

            if (IsValid)
            {
                if (this.WarehouseGroupsGrid.SelectedItems.Count > 0)
                {
                    GridItem item = this.WarehouseGroupsGrid.SelectedItems[0];


                    var cell = item.Cells[WAREHOUSEGROUP_ID_IN_GRID];
                    int warehouseId = int.Parse(cell.Text);

                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        responseCode = warehouseDs.UpdateWarehouseShortName(this.TextBox_ShortName.Text, warehouseId); // !!!
                    }


                    if (responseCode > -1)
                    {
                        //this.Label_Message.Text = "Zmiana " + warehouseId.ToString();

                        //WarehousesGrid.Controls.Clear(); // ?
                        WarehouseGroupsGrid.Rebind();
                        TargetWarehouseGroupsGrid.Rebind();
                        DocumentTypeGrid.Rebind();
                    }
                    else
                    {
                        if (responseCode == -1)
                            this.CustomValidator_ShortName.ErrorMessage = "Błędny identyfikator grupy";
                        if (responseCode == -2)
                            this.CustomValidator_ShortName.ErrorMessage = "Grupa o podanym skrócie już istnieje";
                        if (responseCode == -3)
                            this.CustomValidator_ShortName.ErrorMessage = "Nazwa skrócona może mieć maksymalnie 7 znaków";

                        this.CustomValidator_ShortName.IsValid = false;
                    }
                }
                else
                {
                    this.CustomValidator_ShortName.ErrorMessage = "Nie dokonano wyboru grupy";
                    this.CustomValidator_ShortName.IsValid = false;
                }

            }
            else
            {
                this.CustomValidator_Name.ErrorMessage = "Niezidentyfikowany błąd";
                this.CustomValidator_Name.IsValid = false;
            }
        }

        protected void Button_AllowedStatusesUpdate_Click(object sender, EventArgs e)
        {
            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {
                int permissionId = int.Parse(DocumentTypeGrid.SelectedItems[0].Cells[PERMISSION_ID_IN_GRID].Text);
                warehouseDS.UpdateWarehouseGroupPermission(permissionId, this.CheckBox_Package.Checked, this.CheckBox_ZZ.Checked, this.CheckBox_Garbage.Checked, this.CheckBox_Damaged.Checked, this.CheckBox_PartlyDamaged.Checked);
            }
        }









        // For creating groups:
        protected void CreateWarehouseGroupButton_Clicked(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int responseCode = -1; // MJZ> kod służący do sprawdzenia wyniku utworzenia magazynu

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {


                    string newWarehouseGroupName = WarehouseGroupNameRCB.Text;
                    string shortName = WarehouseGroupShortNameRCB.Text;

                    //int company = int.Parse(CompanyRCB.Items.FindItemByText(CompanyRCB.Text).Value);
                    //int type = int.Parse(WarehouseTypeRCB.Items.FindItemByText(WarehouseTypeRCB.Text).Value);
                    //int department = int.Parse(DepartmentRCB.Items.FindItemByText(DepartmentRCB.Text).Value);

                    //if (type == (int)WarehouseTypeEnum.WarehouseStandard)
                    //{
                    //    responseCode = warehouseDs.CreateWarehouse(newWarehouseName, shortName, (CompanyEnum)company, int.Parse(Session[SessionData.SessionKey_UserId].ToString()));
                    //}
                    //else
                    //{
                    //    WarehouseTypeEnum warehouseTypeEnum;

                    //    if (type == (int)WarehouseTypeEnum.WarehouseStandard)
                    //    {
                    //        warehouseTypeEnum = WarehouseTypeEnum.WarehouseStandard;
                    //    }
                    //    else if (type == (int)WarehouseTypeEnum.WarehouseStandardWithoutOrdering)
                    //    {
                    //        warehouseTypeEnum = WarehouseTypeEnum.WarehouseStandardWithoutOrdering;
                    //    }
                    //    else if (type == (int)WarehouseTypeEnum.WarehouseDepartmentWithoutOrdering)
                    //    {
                    //        warehouseTypeEnum = WarehouseTypeEnum.WarehouseDepartmentWithoutOrdering;
                    //    }
                    //    else if (type == (int)WarehouseTypeEnum.WarehouseDepartmentWithOrdering)
                    //    {
                    //        warehouseTypeEnum = WarehouseTypeEnum.WarehouseDepartmentWithOrdering;
                    //    }
                    //    else
                    //    {
                    //        throw new NotSupportedException();
                    //    }

                    responseCode = warehouseDs.CreateWarehouseGroup(newWarehouseGroupName, shortName, int.Parse(Session[SessionData.SessionKey_UserId].ToString()));
                    //}
                }
                if (responseCode > -1)
                {
                    this.Label_StatusMessage.Text = "Utworzono grupę o nazwie <b>" + WarehouseGroupNameRCB.Text + "</b> (o nazwie skróconej <b>" + WarehouseGroupShortNameRCB.Text + "</b>)" ;// w firmie <b>" + CompanyRCB.Text + "</b>";
                    this.WarehouseGroupNameRCB.Text = "";
                    this.WarehouseGroupShortNameRCB.Text = "";
                    //this.WarehouseTypeRCB.ClearSelection();
                    //this.DepartmentRCB.ClearSelection();

                    //this.Panel_Permissions.Visible = true;

                    this.WarehouseGroupsGrid.Rebind();
                    this.TargetWarehouseGroupsGrid.Rebind();
                    this.DocumentTypeGrid.Rebind();

                }
                else
                {
                    switch (responseCode)
                    {
                        case -1:
                            // Nazwa już istnieje
                            this.CustomValidator_NameExists.ErrorMessage = "Magazyn o tej nazwie już istnieje";
                            this.CustomValidator_NameExists.IsValid = false;
                            break;
                        case -2:
                            // Skrót już istnieje
                            this.RequiredFieldValidator_ShortName.ErrorMessage = "Magazyn o tej nazwie skróconej już istnieje";
                            this.RequiredFieldValidator_ShortName.IsValid = false;
                            break;
                        case -3:
                            // Skrót nieprawidłowy
                            this.RequiredFieldValidator_ShortName.ErrorMessage = "Nazwa skrócona może mieć maksymalnie 7 znaków";
                            this.RequiredFieldValidator_ShortName.IsValid = false;
                            break;
                        default:
                            // Niesprecyzowany błąd
                            break;

                    }

                }
            }
        }

        //protected void DepartmentRCBCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    int warehouseDepartmentWithoutOrderingEnumValue = (int)WarehouseTypeEnum.WarehouseDepartmentWithoutOrdering;
        //    int warehouseDepartmentWithOrderingEnumValue = (int)WarehouseTypeEnum.WarehouseDepartmentWithOrdering;

        //    if ((int.Parse(WarehouseTypeRCB.Items.FindItemByText(WarehouseTypeRCB.Text).Value) == warehouseDepartmentWithoutOrderingEnumValue || int.Parse(WarehouseTypeRCB.Items.FindItemByText(WarehouseTypeRCB.Text).Value) == warehouseDepartmentWithOrderingEnumValue) &&
        //        int.Parse(DepartmentRCB.Items.FindItemByText(DepartmentRCB.Text).Value) == -1)
        //    {
        //        args.IsValid = false;
        //    }
        //    else
        //    {
        //        args.IsValid = true;
        //    }
        //}











      
    }
}
