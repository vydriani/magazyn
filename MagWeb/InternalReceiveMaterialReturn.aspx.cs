using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using MagWeb.WarehouseDocuments;
using Telerik.Web.UI;

namespace MagWeb
{
    public partial class ReceiveMaterialReturn : FeaturedSystemWebUIPage
    {
        //private const int INTERNAL_GIVE_MATERIAL_RETURN_INDEX_IN_TABS = 0;
        //private const int INTERNAL_RECEIVE_MATERIAL_RETURN_INDEX_IN_TABS = 1;

        private List<WarehouseDocumentPosition> positionsList;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["tab_ReturnMaterial"] = 1;

            int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
            int? userId = (int?)Session[SessionData.SessionKey_UserId];
      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                string message = "";
                if (currentWarehouseId == null || userId == null ||
                    (message = warehouseDs.CheckOperationAvailability(null, currentWarehouseId, WarehouseDocumentTypeEnum.ZwPlus, userId)) != "")
                {
                    Session[SessionData.SessionKey_Message] = message;
					PermissionDenied();
					return;
                }
            }

            if (!IsPostBack)
            {
                Session["ReceiveMaterialReturn_positionsList"] = null;

                inputDate.SelectedDate = DateTime.Now.Date;
            }

            if (Session["ReceiveMaterialReturn_positionsList"] == null)
                positionsList = new List<WarehouseDocumentPosition>();
            else
                positionsList = (List<WarehouseDocumentPosition>)Session["ReceiveMaterialReturn_positionsList"];

      using (var warehouseDs = new WarehouseDS())
            {
                bool isSimpleExportEnabled = warehouseDs.CheckSimpleExportPossibility(currentWarehouseId.Value,
                                                                                      warehouseDs.GetPersonForSystemUser
                                                                                          (userId).Id, 12);
                if (isSimpleExportEnabled)
                {
                    this.CheckBox_ExportToSIMPLE.Enabled = true;
                    this.Label_ExportToSIMPLE.Visible = false;
                }
                else
                {
                    this.CheckBox_ExportToSIMPLE.Checked = false;
                    this.CheckBox_ExportToSIMPLE.Enabled = false;
                    this.Label_ExportToSIMPLE.Visible = true;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        }

        //protected void Tab_Prerender(object sender, EventArgs e)
        //{
        //    int? currentWarehouseId = (int?)Session[SessionData.SessionKey_CurrentWarehouseId];
        //    int? userId = (int?)Session[SessionData.SessionKey_UserId];

    //    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
        //    {
        //        RadToolBar1.Tabs[INTERNAL_GIVE_MATERIAL_RETURN_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
        //                                                                    warehouseDs.CheckOperationAvailability(
        //                                                                        currentWarehouseId.Value,
        //                                                                        0,
        //                                                                        WarehouseDocumentTypeEnum.ZwMinus,
        //                                                                        userId, null) == "");

        //        RadToolBar1.Tabs[INTERNAL_RECEIVE_MATERIAL_RETURN_INDEX_IN_TABS].Enabled = (currentWarehouseId != null && userId != null &&
        //                                                                    warehouseDs.CheckOperationAvailability(
        //                                                                        0,
        //                                                                        currentWarehouseId.Value,
        //                                                                        WarehouseDocumentTypeEnum.ZwPlus,
        //                                                                        userId, null) == "");
        //    }
        //}

        protected void WarehouseDocumentRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int currentWarehouseId = (int)Session[SessionData.SessionKey_CurrentWarehouseId];

      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var documents = from p in warehouseDs.GetToReceiveMaterialReturnDocument(currentWarehouseId, null)
                                select
                                  new
                                  {
                                      Id = p.Id,
                                      WarehouseSource = p.Warehouse.Name,
                                      WarehouseTarget = p.Warehouse1.Name,
                                      DocumentType = p.WarehouseDocumentType.Type,
                                      Author = p.Person.FirstName + " " + p.Person.LastName,
                                      CreationDate = p.TimeStamp,
                                      ExportToSIMPLE = p.ExportToSIMPLE ? "Tak" : "Nie"
                                  };

                documents = documents.OrderByDescending(p => p.Id);

                WarehouseDocumentRadGrid.DataSource = documents.ToArray();
            }
        }

        protected void MaterialsToReceiveRadGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            int selectedDocumentId = -1;

            if (WarehouseDocumentRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = WarehouseDocumentRadGrid.SelectedItems[0];
                var cell = item.Cells[3];
                selectedDocumentId = int.Parse(cell.Text);
            }

      var positionWithShelfAndQuantities = new List<PositionWithShelfAndQuantity>();

      using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                var positions = warehouseDs.GetPositionsForWarehouseDocument(selectedDocumentId);

                foreach (var externalIterator in positions)
                {
          var position = positionsList.FirstOrDefault(p => p.Id == externalIterator.Id);

          if (position != null && position.WarehousePositionZones != null && position.WarehousePositionZones.Any())
                    {
            var usedQuantity = 0M;
            foreach (var internalIterator in position.WarehousePositionZones)
                        {
                            positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                            {
                                position = externalIterator,
                                shelf = internalIterator.Zone,
                quantity = Math.Abs((decimal)internalIterator.Quantity)
                            });
              usedQuantity += Math.Abs((decimal)internalIterator.Quantity);
                        }
            if (usedQuantity < Math.Abs((decimal)externalIterator.Quantity))
                        {
                            positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                            {
                                position = externalIterator,
                                shelf = null,
                quantity = Math.Abs((decimal)externalIterator.Quantity) - usedQuantity
                            });
                        }
                    }
                    else
                    {
                        positionWithShelfAndQuantities.Add(new PositionWithShelfAndQuantity()
                        {
                            position = externalIterator,
                            shelf = null,
              quantity = Math.Abs((decimal)externalIterator.Quantity)
                        });
                    }
                }

                var shelfs = from p in positionWithShelfAndQuantities //warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id))
                             group p by new { Id = p.position.Id, MaterialName = p.position.Material.NameShort, IndexSIMPLE = p.position.Material.IndexSIMPLE, NameSIMPLE = p.position.Material.Name, Quantity = Math.Abs(p.quantity), UnitName = p.position.Material.Unit.Name, ShelfName = p.shelf != null ? p.shelf.Name : null }
                                 into g
                                 orderby g.Key.Id
                                 select
                                     new
                                     {
                                         Id = g.Key.Id, //p.position.Id,
                                         MaterialName = g.Key.MaterialName, //warehouseDs.GetExtendedMaterialName(warehouseDs.GetPositionById(p.position.Id)),//p.position.Material.Name,
                                         IndexSIMPLE = g.Key.IndexSIMPLE, //p.position.Material.MaterialSIMPLE.FirstOrDefault() != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().IndexSIMPLE : null,
                                         NameSIMPLE = g.Key.NameSIMPLE, //p.position.Material.MaterialSIMPLE.FirstOrDefault() != null ? p.position.Material.MaterialSIMPLE.FirstOrDefault().NameSIMPLE : null,
                                         Quantity = g.Sum(p => p.quantity),
                                         UnitName = g.Key.UnitName, //p.position.Material.Unit.Name,
                                         ShelfName = g.Key.ShelfName //p.shelf != null ? p.shelf.Name : null
                                     };

                MaterialsToReceiveRadGrid.DataSource = shelfs.ToArray();
            }
        }

        protected void SaveDocumentClick(object sender, EventArgs e)
        {

            if (IsValid)
            {
                if (positionsList.Count > 0)
                {
                    GridItem item = WarehouseDocumentRadGrid.SelectedItems[0];
                    var cell = item.Cells[3];
                    int orderDocumentId = int.Parse(cell.Text);
                    int userId;

                    if (int.TryParse(Session[SessionData.SessionKey_UserId].ToString(), out userId))
                    {
                        WarehouseDocument warehouseDocument;

            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                        {
                            //warehouseDocument = warehouseDs.InternalReceive(orderDocumentId, positionsList, userId);
                            lock (typeof(WarehouseDS))
                            {
                                try
                                {
                                    warehouseDocument = warehouseDs.ReceiveReturn(orderDocumentId, positionsList, warehouseDs.GetPersonForSystemUser(userId).Id,
                                                                              inputDate.SelectedDate ??
                                                                              DateTime.Now.Date);
                                    
                                    DocumentPrintingUtils.BindDocumentToButton(btnPrintDocument, WarehouseDocumentTypeEnum.ZwPlus,
                                                                   warehouseDocument.Id);
                                }
                                catch (Exception ex)
                                {
                                    
                                    WebTools.AlertMsgAdd(ex, userId); //WebTools.AlertMsgAdd(ex.Message);
                                }
                            }
                        }

                        WarehouseDocumentRadGrid.Enabled = false;
                        MaterialsToReceiveRadGrid.Enabled = false;
                        MaterialsToReceiveRadGrid.SelectedIndexes.Clear();

                        btnPrintDocument.Enabled = true;
                        btnSaveDocument.Enabled = false;

                        MaterialsToReceiveRadGrid.Rebind();
                    }
                }
                else
                {
                    WebTools.AlertMsgAdd("Brak pozycji");
                }
            }
        }

        protected void MaterialCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = WarehouseDocumentRadGrid.SelectedItems.Count == 1 ? true : false;
        }

        protected void WarehouseDocumentRadGrid_SelectRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "SelectRow")
            {
                WarehouseDocumentRadGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                positionsList = new List<WarehouseDocumentPosition>();
                Session["ReceiveMaterialReturn_positionsList"] = positionsList;
                MaterialsToReceiveRadGrid.Rebind();
            }
            else
            {
                throw new ApplicationException("Unknown grid command");
            }
        }

        protected void MaterialsToReceiveRadGrid_SelectRow(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "SelectRow")
            {
                var location = e.Item.Cells[8];
                if (location.Text != "&nbsp;") return;

                e.Item.Selected = true;
                WarehouseDocumentRadGrid.Enabled = false;
                btnSaveDocument.Enabled = false;

                cmbLocalization.Enabled = true;
                btnAssignLocation.Enabled = true;
                btnCancelAssignLocation.Enabled = true;
                Quantity.Enabled = true;
            }
            else
            {
                throw new ApplicationException("Unknown grid command");
            }
        }

        protected void Localization_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Zone zone = null;

      //using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            //{
            //    zone = warehouseDs.GetShelfByNameDataOnly(cmbLocalization.Text);
            //}

            //if (zone == null) args.IsValid = false;
            //else args.IsValid = true;


            if (!string.IsNullOrEmpty(cmbLocalization.SelectedValue))
            {
        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
          zone = warehouseDs.GetShelfByName(cmbLocalization.Text);//.GetShelfByNameDataOnly(cmbLocalization.Text);
                }

                if (zone == null) args.IsValid = false;
                else args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }

        }

        protected void Localization_Required_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int selectedDocumentId = -1;

            if (WarehouseDocumentRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = WarehouseDocumentRadGrid.SelectedItems[0];
                var cell = item.Cells[3];
                selectedDocumentId = int.Parse(cell.Text);


        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var selectedPositions = warehouseDs.GetPositionsForWarehouseDocument(selectedDocumentId);
                    if (selectedPositions.Count() != positionsList.Count())
                    {
                        args.IsValid = false;
                        return;
                    }

                    foreach (var position in positionsList)
                    {
                        WarehouseDocumentPosition warehouseDocumentPosition = selectedPositions.FirstOrDefault(p => p.Id == position.Id);

            double usedQuantity = position.WarehousePositionZones.Sum(z => z.Quantity);

                        if (Math.Abs(warehouseDocumentPosition.Quantity) > usedQuantity)
                        {
                            args.IsValid = false;
                            return;
                        }
                    }
                }
            }

            args.IsValid = true;
        }

        protected void Localization_Quantity_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Zone zone = null;

            int selectedDocumentId = -1;

            if (WarehouseDocumentRadGrid.SelectedItems.Count == 1)
            {
                GridItem item = WarehouseDocumentRadGrid.SelectedItems[0];
                var cell = item.Cells[3];
                selectedDocumentId = int.Parse(cell.Text);


        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    var selectedPositions = warehouseDs.GetPositionsForWarehouseDocument(selectedDocumentId);
                    if (selectedPositions.Count() != positionsList.Count())
                    {
                        args.IsValid = true;
                        return;
                    }

                    foreach (var position in positionsList)
                    {
                        WarehouseDocumentPosition warehouseDocumentPosition = selectedPositions.FirstOrDefault(p => p.Id == position.Id);

            double usedQuantity = position.WarehousePositionZones.Sum(z => z.Quantity);

            if (Math.Abs(warehouseDocumentPosition.Quantity) < usedQuantity + double.Parse(Quantity.Text.Replace(",", ".")))
                        {
                            args.IsValid = false;
                            return;
                        }
                    }
                }
            }

            args.IsValid = true;
        }

        protected void AssignLocationClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int selectedPositionId = -1;

                if (MaterialsToReceiveRadGrid.SelectedItems.Count == 1)
                {
                    GridItem item = MaterialsToReceiveRadGrid.SelectedItems[0];
                    var cell = item.Cells[2];
                    selectedPositionId = int.Parse(cell.Text);
                }

                Zone zone;

        using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
          zone = warehouseDs.GetShelfByName(cmbLocalization.Text);//.GetShelfByNameDataOnly(cmbLocalization.Text);
                }

                var selectedPositions = positionsList.FirstOrDefault(p => p.Id == selectedPositionId);

                if (selectedPositions == null)
                {
                    selectedPositions = new WarehouseDocumentPosition() { Id = selectedPositionId };
                    positionsList.Add(selectedPositions);
                }

        var zoneToUpdate = selectedPositions.WarehousePositionZones.FirstOrDefault(wpz => wpz.Zone.Id == zone.Id);

                if (zoneToUpdate != null)
                {
                    zoneToUpdate.Quantity += double.Parse(Quantity.Text.Replace(".", ","));
                }
                else
                {
          selectedPositions.WarehousePositionZones.Add(new WarehousePositionZone()
                    {
                        Zone = zone,
                        WarehouseDocumentPosition = selectedPositions,
                        Quantity = double.Parse(Quantity.Text.Replace(".", ","))
                    });
                }

                MaterialsToReceiveRadGrid.SelectedIndexes.Clear();
                MaterialsToReceiveRadGrid.Rebind();

                WarehouseDocumentRadGrid.Enabled = true;
                btnSaveDocument.Enabled = true;

                cmbLocalization.Text = "";
                Quantity.Text = "";

                cmbLocalization.Enabled = false;
                btnAssignLocation.Enabled = false;
                btnCancelAssignLocation.Enabled = false;
                Quantity.Enabled = false;
                btnNewDocument.Enabled = true;
            }
        }

        protected void CancelAssignLocationClick(object sender, EventArgs e)
        {
            MaterialsToReceiveRadGrid.SelectedIndexes.Clear();

            WarehouseDocumentRadGrid.Enabled = true;
            btnSaveDocument.Enabled = false;

            cmbLocalization.Text = "";
            Quantity.Text = "";

            cmbLocalization.Enabled = false;
            btnAssignLocation.Enabled = false;
            btnCancelAssignLocation.Enabled = false;
            Quantity.Enabled = false;
            btnNewDocument.Enabled = true;
        }

        protected void NewDocumentClick(object sender, EventArgs e)
        {
            btnPrintDocument.Enabled = false;
            btnSaveDocument.Enabled = false;

            positionsList = new List<WarehouseDocumentPosition>();
            Session["ReceiveMaterialReturn_positionsList"] = positionsList;

            WarehouseDocumentRadGrid.SelectedIndexes.Clear();
            MaterialsToReceiveRadGrid.SelectedIndexes.Clear();

            WarehouseDocumentRadGrid.Enabled = true;
            MaterialsToReceiveRadGrid.Enabled = true;

            WarehouseDocumentRadGrid.Rebind();
            MaterialsToReceiveRadGrid.Rebind();
        }
    }
}
