﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Mag.Domain;
using Mag.Domain.Model;
using Telerik.Web.UI;




namespace MagWeb
{
    public partial class ManageWarehouses : FeaturedSystemWebUIPage
    {

        private const int WAREHOUSE_ID_IN_GRID = 2;
        private const int TARGET_WAREHOUSE_ID_IN_GRID = 2;
        private const int PERMISSION_ID_IN_GRID = 2;
        private const int DOCUMENT_TYPE_ID_IN_GRID = 3;

        private const int MEMBERSHIP_ID_IN_GRID = 2;
        private const int GROUP_ID_IN_GRID = 3;
        private const int GROUP_NAME_IN_GRID = 4;

        //private void InvalidateAllSecurityCaches()
        //{
        //    using(WarehouseDS warehouseDs = new WarehouseDS())
        //    {
        //        warehouseDs.InvalidateCache(null, "WarehouseSecurityConfigMatrix");
        //    }
        //}

        protected void Page_Init(object sender, EventArgs e)
        {
            Session["tab_warehouse_mgmt"] = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    CompanyRCB.DataSource = from w in warehouseDs.GetAllCompanies()
                                            select new {Value = w.Id, Text = w.Name};
                    CompanyRCB.DataTextField = "Text";
                    CompanyRCB.DataValueField = "Value";
                    CompanyRCB.DataBind();
                    CompanyRCB.Items.Insert(0, new RadComboBoxItem("(wybierz firmę)", "-1"));

                    DocumentTypeRCB.DataSource = from t in warehouseDs.GetAllWarehouseDocumentTypes()
                                                 select new {Value = t.Id, Text = t.ShortType};
                    DocumentTypeRCB.DataTextField = "Text";
                    DocumentTypeRCB.DataValueField = "Value";
                    DocumentTypeRCB.DataBind();
                    DocumentTypeRCB.Items.Insert(0, new RadComboBoxItem("(wybierz typ dokumentu)", "-1"));


                    AllowedStatuses_CheckboxesSetDefault();



                    // For warehouse crreation:

                    //CompanyRCB2.DataSource = from w in warehouseDs.GetAllCompanies() select new { Value = w.Id, Text = w.Name };
                    //CompanyRCB2.DataTextField = "Text";
                    //CompanyRCB2.DataValueField = "Value";
                    //CompanyRCB2.DataBind();
                    //CompanyRCB2.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));

                    //////WarehouseTypeRCB.DataSource = from w in warehouseDs.GetAllWarehouseTypes() select new { Value = w.Id, Text = w.Name };
                    //////WarehouseTypeRCB.DataTextField = "Text";
                    //////WarehouseTypeRCB.DataValueField = "Value";
                    //////WarehouseTypeRCB.DataBind();
                    //////WarehouseTypeRCB.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));

                    DepartmentRCB.DataSource = from w in warehouseDs.GetAllDepartments() select new { Value = w.Id, Text = w.Name };
                    DepartmentRCB.DataTextField = "Text";
                    DepartmentRCB.DataValueField = "Value";
                    DepartmentRCB.DataBind();
                    DepartmentRCB.Items.Insert(0, new RadComboBoxItem("(wybierz)", "-1"));


                }

                //this.Panel_AllowedStatuses.Visible = false;
            }

            this.Label_StatusMessage.Text = "";
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.TextBox_Name.Text = "";
            this.TextBox_ShortName.Text = "";

            if(int.Parse(CompanyRCB.SelectedValue) == -1)
            {
                this.TextBox_Name.Enabled = false;
                this.TextBox_ShortName.Enabled = false;
                this.Button_ChangeName.Enabled = false;
                this.Button_ChangeShortName.Enabled = false;

                this.Panel_CreateWarehouse.Visible = false;
            }
            else
            {
                this.TextBox_Name.Enabled = true;
                this.TextBox_ShortName.Enabled = true;
                this.Button_ChangeName.Enabled = true;
                this.Button_ChangeShortName.Enabled = true;

                this.Panel_CreateWarehouse.Visible = true;
            }

            if ((this.WarehousesGrid.SelectedItems.Count == 1 && this.TargetWarehousesGrid.SelectedItems.Count == 1) || (this.DocumentTypeGrid.SelectedItems.Count == 1))
            {
                this.Panel_AllowedStatuses.Visible = true;
            }
            else
            {
                this.Panel_AllowedStatuses.Visible = false;
            }

            if (this.DocumentTypeGrid.SelectedItems.Count == 1)
            {
                this.Button_AllowedStatusesUpdate.Visible = true;


                using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
                {

                    // Getting info about allowed statuses:

                    if (DocumentTypeGrid.SelectedItems.Count == 1)
                    {
                        AllowedStatuses_CheckboxesSetDefault();

                        string permIdInDocTypeGrid = DocumentTypeGrid.SelectedItems[0].Cells[PERMISSION_ID_IN_GRID].Text;

                        int permissionId = int.Parse(permIdInDocTypeGrid);


                        var persmissions = from p in warehouseDS.GetAllWarehousePermissions()
                                           where p.Id == permissionId
                                           select p;

                        var wp = persmissions.FirstOrDefault();

                        if (wp != null)
                        {
                            this.CheckBox_Package.Checked = wp.MaterialIsPackage;
                            this.CheckBox_Garbage.Checked = wp.PositionIsGarbage;
                            this.CheckBox_ZZ.Checked = wp.PositionIsZZ;
                            this.CheckBox_Damaged.Checked = wp.PositionIsDamaged;
                            this.CheckBox_PartlyDamaged.Checked = wp.PositionIsPartlyDamaged;
                        }
                    }
                }
            }
            else
            {
                this.Button_AllowedStatusesUpdate.Visible = false;
            }
        }

        protected void CompanyRCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            WarehousesGrid.Rebind();
            TargetWarehousesGrid.Rebind();
        }

        protected void WarehousesGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {

                
                int company = int.Parse(CompanyRCB.SelectedValue); // Company selection from combo box

                var warehousesCandidatesToConsider = warehouseDs.GetAllWarehouses(company, true, false);

                var warehousesCandidatesToConsiderOrdered = warehousesCandidatesToConsider.OrderBy(p => p.Name);

                List<Mag.Domain.Model.Warehouse> warehousesCandidatesToPresent = new List<Mag.Domain.Model.Warehouse>();

                foreach (var warehouse in warehousesCandidatesToConsiderOrdered)
                {
                    Mag.Domain.Model.Warehouse waitingForWarehouseToPresent = new Mag.Domain.Model.Warehouse();

                    waitingForWarehouseToPresent.Id = warehouse.Id;
                    waitingForWarehouseToPresent.Name = warehouse.Name;
                    waitingForWarehouseToPresent.ShortName = warehouse.ShortName;

                    warehousesCandidatesToPresent.Add(waitingForWarehouseToPresent);

                }

                if (company != -1)
                {
                    Mag.Domain.Model.Warehouse warehouseNull = new Mag.Domain.Model.Warehouse();
                    warehouseNull.Id = -1;
                    warehouseNull.Name = "(nieokreślony magazyn)";
                    warehouseNull.ShortName = "(nieokreślony magazyn)";
                    warehousesCandidatesToPresent.Insert(0, warehouseNull);
                }

                var warehousesCandidatesToPresentMaterialized = warehousesCandidatesToPresent.ToArray();

                var warehouses = from p in warehousesCandidatesToPresentMaterialized
                                 select
                                     new
                                         {
                                             WarehouseName = p.Name,
                                             WarehouseShortName = p.ShortName,
                                             WarehouseID = p.Id
                                         };

                WarehousesGrid.DataSource = warehouses;
            }           
        }

        protected void WarehousesGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                WarehousesGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                //TargetWarehousesGrid.SelectedIndexes.Clear();
                //TargetWarehousesGrid.SelectedIndexes.Add(int.Parse(WarehousesGrid.SelectedIndexes[0]));
                DocumentTypeGrid.Rebind();

                WarehouseGroupMembershipsGrid.Rebind();

                AllowedStatuses_CheckboxesSetDefault();
            }
        }

        protected void TargetWarehousesGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int company = int.Parse(CompanyRCB.SelectedValue);

                var warehouses = warehouseDs.GetAllWarehouses(company, true, false).ToList();

                if (company != -1)
                {
                    Mag.Domain.Model.Warehouse warehouseNull = new Mag.Domain.Model.Warehouse();
                    warehouseNull.Id = -1;
                    warehouseNull.Name = "(nieokreślony magazyn)";
                    warehouseNull.ShortName = "(nieokreślony magazyn)";
                    warehouses.Insert(0, warehouseNull);
                }

                TargetWarehousesGrid.DataSource = warehouses;
            }
        }

        protected void TargetWarehousesGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                TargetWarehousesGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;
                DocumentTypeGrid.Rebind();
                WarehouseGroupMembershipsGrid.Rebind();

                AllowedStatuses_CheckboxesSetDefault();
            }
        }

        protected void CustomValidatorShowTypes_ServerValidate(object source, ServerValidateEventArgs e)
        {
            if (WarehousesGrid.SelectedItems.Count != 1)
            {
                CustomValidatorShowTypes.ErrorMessage = "Nie wybrano magazynu źródłowego";
                e.IsValid = false;
            }
            else if (TargetWarehousesGrid.SelectedItems.Count != 1)
            {
                CustomValidatorShowTypes.ErrorMessage = "Nie wybrano magazynu docelowego";
                e.IsValid = false;
            }
            else
            {
                CustomValidatorShowTypes.ErrorMessage = "";
                e.IsValid = true;
            }
        }

        protected void Button_ShowTypes_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                DocumentTypeGrid.Rebind();
            }
        }

        protected void DocumentTypeGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int? sourceWarehouseId = -1;
                if (WarehousesGrid.SelectedItems.Count == 1)
                {
                    sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
                    sourceWarehouseId = sourceWarehouseId != -1 ? sourceWarehouseId : null;
                }

                int? targetWarehouseId = -1;
                if (TargetWarehousesGrid.SelectedItems.Count == 1)
                {
                    targetWarehouseId = int.Parse(TargetWarehousesGrid.SelectedItems[0].Cells[TARGET_WAREHOUSE_ID_IN_GRID].Text);
                    targetWarehouseId = targetWarehouseId != -1 ? targetWarehouseId : null;
                }

                var warehousePermissions = warehouseDs.GetWarehousePermissions(sourceWarehouseId, targetWarehouseId);
                //var test = warehousePermissions.ToList();
                var warehouseDocumentTypes = warehouseDs.GetAllWarehouseDocumentTypes(); //.ToList();

                var permissions =
                    from p in warehousePermissions
                    join t in warehouseDocumentTypes on p.WarehouseDocumentType.Id equals t.Id
                    select new
                               {
                                   PermissionId = p.Id,
                                   TypeId = t.Id,
                                   ShortType = t.ShortType,
                                   Type = t.Type
                               };

                DocumentTypeGrid.DataSource = permissions.OrderBy(p => p.TypeId).ToArray();
            }
        }

        protected void DocumentTypeGrid_ItemCommand(object source, GridCommandEventArgs e)
        {

        }      

        private void AllowedStatuses_CheckboxesSetDefault()
        {
            this.CheckBox_Package.Checked = true;
            this.CheckBox_Garbage.Checked = false;
            this.CheckBox_ZZ.Checked = true;
            this.CheckBox_Damaged.Checked = true;
            this.CheckBox_PartlyDamaged.Checked = true;

            //this.Button_AllowedStatusesUpdate.Visible = false;
        }

        protected void WarehouseGroupMembershipsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
            {
                int sourceWarehouseId = -1;
                if (WarehousesGrid.SelectedItems.Count == 1)
                {
                    sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
                }
                if (sourceWarehouseId > -1)
                {
                    var groups = from g in warehouseDs.GetAllWarehouseGroups()
                                 select g;

                    List<GroupAndMembership> groupsAndMemberships = new List<GroupAndMembership>();

                    foreach (var group in groups)
                    {
                        GroupAndMembership membership = new GroupAndMembership();

                        WarehouseGroupMembership _membership = warehouseDs.GetWarehouseGroupMembership(sourceWarehouseId,group.Id);

                        if (_membership != null)
                        {
                            membership.MembershipID = _membership.Id;
                        }
                        else
                        {
                            membership.MembershipID = null;
                        }

                        //membership.PermissionID = (_membership != null ? _membership.Id : null);
                        membership.GroupID = group.Id;
                        membership.GroupName = group.Name;
                        membership.IsMember = _membership != null;

                        groupsAndMemberships.Add(membership);
                    }

                    WarehouseGroupMembershipsGrid.DataSource = groupsAndMemberships.OrderBy(m => m.GroupName).ToArray();
                }
            }
        }
        protected void WarehouseGroupMembershipsGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {

                this.DocumentTypeGrid.SelectedIndexes.Clear();
                e.Item.Selected = true;

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int sourceWarehouseId = -1;
                    if (WarehousesGrid.SelectedItems.Count == 1)
                    {
                        sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
                    }



                    int membershipId = -1;
                    string membershipIdStr = e.Item.Cells[MEMBERSHIP_ID_IN_GRID].Text;

                    if (membershipIdStr != "&nbsp;" && membershipIdStr != "")
                    {
                        membershipId = int.Parse(e.Item.Cells[MEMBERSHIP_ID_IN_GRID].Text);
                    }

                    int groupId = -1;
                    groupId = int.Parse(e.Item.Cells[GROUP_ID_IN_GRID].Text);

                    if (membershipId == -1 || warehouseDs.GetWarehouseGroupMembership(membershipId) == null)
                    {
                        // Adding membership
                        warehouseDs.AddWarehouseGroupMembership(sourceWarehouseId, groupId);
                        //warehouseDs.SecurityAddUserPriviledgeForExecution(sourceWarehouseId, targetWarehouseId, int.Parse(e.Item.Cells[DOCUMENT_TYPE_ID_IN_GRID].Text), int.Parse(this.UsersGrid.SelectedItems[0].Cells[USER_ID_IN_GRID].Text));
                    }
                    else
                    {
                        // Removing membership
                        warehouseDs.RemoveWarehouseGroupMembership(membershipId);
                    }
                }
                WarehouseGroupMembershipsGrid.Rebind();
            }
        }

        protected void CustomValidatorAddPermission_ServerValidate(object source, ServerValidateEventArgs e)
        {
            if (WarehousesGrid.SelectedItems.Count != 1)
            {
                CustomValidatorAddPermission.ErrorMessage = "Nie wybrano magazynu źródłowego";
                e.IsValid = false;
            }
            else if (TargetWarehousesGrid.SelectedItems.Count != 1)
            {
                CustomValidatorAddPermission.ErrorMessage = "Nie wybrano magazynu docelowego";
                e.IsValid = false;
            }
            else if (DocumentTypeRCB.SelectedValue == "-1")
            {
                CustomValidatorAddPermission.ErrorMessage = "Nie wybrano typu dokumentu";
                e.IsValid = false;
            }
            else
            {
                CustomValidatorAddPermission.ErrorMessage = "";
                e.IsValid = true;
            }
        }

        protected void Button_AddPermission_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int? sourceWarehouseId = int.Parse(WarehousesGrid.SelectedItems[0].Cells[WAREHOUSE_ID_IN_GRID].Text);
                    sourceWarehouseId = sourceWarehouseId != -1 ? sourceWarehouseId : null;

                    int? targetWarehouseId = int.Parse(TargetWarehousesGrid.SelectedItems[0].Cells[TARGET_WAREHOUSE_ID_IN_GRID].Text);
                    targetWarehouseId = targetWarehouseId != -1 ? targetWarehouseId : null;
                    
                    int documentTypeId = int.Parse(DocumentTypeRCB.SelectedValue);

                    warehouseDs.AddWarehousePermission(sourceWarehouseId, targetWarehouseId, documentTypeId, this.CheckBox_Package.Checked, this.CheckBox_ZZ.Checked, this.CheckBox_Garbage.Checked, this.CheckBox_Damaged.Checked, this.CheckBox_PartlyDamaged.Checked);

                    DocumentTypeGrid.Rebind();
                }
            }
        }

        protected void CustomValidatorRemovePermission_ServerValidate(object source, ServerValidateEventArgs e)
        {
            if (DocumentTypeGrid.SelectedItems.Count != 1)
            {
                CustomValidatorRemovePermission.ErrorMessage = "Nie wybrano typu dokumentu";
                e.IsValid = false;
            }
            else
            {
                CustomValidatorRemovePermission.ErrorMessage = "";
                e.IsValid = true;
            }
        }

        protected void Button_RemovePermission_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {
                    int permissionId = int.Parse(DocumentTypeGrid.SelectedItems[0].Cells[PERMISSION_ID_IN_GRID].Text);

                    warehouseDs.RemoveWarehousePermission(permissionId);

                    DocumentTypeGrid.Rebind();
                }
            }
        }

        protected void Button_ChangeName_Click(object sender, EventArgs e)
        {
            int responseCode;

            if(IsValid)
            {
                if (this.WarehousesGrid.SelectedItems.Count > 0)
                {
                    GridItem item = this.WarehousesGrid.SelectedItems[0];


                    var cell = item.Cells[WAREHOUSE_ID_IN_GRID];
                    int warehouseId = int.Parse(cell.Text);

                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        responseCode = warehouseDs.UpdateWarehouseName(this.TextBox_Name.Text, warehouseId);
                    }
                    

                    if (responseCode > -1)
                    {
                        //this.Label_Message.Text = "Zmiana " + warehouseId.ToString();

                        //WarehousesGrid.Controls.Clear(); // ?
                        WarehousesGrid.Rebind();
                        TargetWarehousesGrid.Rebind();
                        DocumentTypeGrid.Rebind();
                    }
                    else
                    {
                        if (responseCode == -1)
                            this.CustomValidator_Name.ErrorMessage = "Błędny identyfikator magazynu";
                        if (responseCode == -2)
                            this.CustomValidator_Name.ErrorMessage = "Magazyn o podanej nazwie już istnieje";

                        this.CustomValidator_Name.IsValid = false;
                    }
                }
                else
                {
                    this.CustomValidator_Name.ErrorMessage = "Nie dokonano wyboru magazynu";
                    this.CustomValidator_Name.IsValid = false;                    
                }
                
            }
            else
            {
                this.CustomValidator_Name.ErrorMessage = "Niezidentyfikowany błąd";
                this.CustomValidator_Name.IsValid = false;                                    
            }
        }


        protected void Button_ChangeShortName_Click(object sender, EventArgs e)
        {
            int responseCode;

            if (IsValid)
            {
                if (this.WarehousesGrid.SelectedItems.Count > 0)
                {
                    GridItem item = this.WarehousesGrid.SelectedItems[0];


                    var cell = item.Cells[WAREHOUSE_ID_IN_GRID];
                    int warehouseId = int.Parse(cell.Text);

                    using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                    {
                        responseCode = warehouseDs.UpdateWarehouseShortName(this.TextBox_ShortName.Text, warehouseId);
                    }


                    if (responseCode > -1)
                    {
                        //this.Label_Message.Text = "Zmiana " + warehouseId.ToString();

                        //WarehousesGrid.Controls.Clear(); // ?
                        WarehousesGrid.Rebind();
                        TargetWarehousesGrid.Rebind();
                        DocumentTypeGrid.Rebind();
                    }
                    else
                    {
                        if (responseCode == -1)
                            this.CustomValidator_ShortName.ErrorMessage = "Błędny identyfikator magazynu";
                        if (responseCode == -2)
                            this.CustomValidator_ShortName.ErrorMessage = "Magazyn o podanym skrócie już istnieje";
                        if (responseCode == -3)
                            this.CustomValidator_ShortName.ErrorMessage = "Nazwa skrócona może mieć maksymalnie 7 znaków";

                        this.CustomValidator_ShortName.IsValid = false;
                    }
                }
                else
                {
                    this.CustomValidator_ShortName.ErrorMessage = "Nie dokonano wyboru magazynu";
                    this.CustomValidator_ShortName.IsValid = false;
                }

            }
            else
            {
                this.CustomValidator_Name.ErrorMessage = "Niezidentyfikowany błąd";
                this.CustomValidator_Name.IsValid = false;
            }
        }

        protected void Button_AllowedStatusesUpdate_Click(object sender, EventArgs e)
        {
            using (WarehouseDS warehouseDS = new WarehouseDS(SessionData.ClientData))
            {
                int permissionId = int.Parse(DocumentTypeGrid.SelectedItems[0].Cells[PERMISSION_ID_IN_GRID].Text);
                warehouseDS.UpdateWarehousePermission(permissionId, this.CheckBox_Package.Checked, this.CheckBox_ZZ.Checked, this.CheckBox_Garbage.Checked, this.CheckBox_Damaged.Checked, this.CheckBox_PartlyDamaged.Checked);
            }
        }



        //protected void WarehouseGroupMembershipsGrid_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        //{

        //}





        // For creating warehouses:
        protected void CreateWarehouseButton_Clicked(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int responseCode = -1; // MJZ> kod służący do sprawdzenia wyniku utworzenia magazynu

                using (var warehouseDs = new WarehouseDS(SessionData.ClientData))
                {


                    string newWarehouseName = WarehouseNameRCB.Text;
                    string shortName = WarehouseShortNameRCB.Text;

                    int company = int.Parse(CompanyRCB.Items.FindItemByText(CompanyRCB.Text).Value);
                    int type = (int)WarehouseTypeEnum.WarehouseStandardWithoutRequirements; ////// int.Parse(WarehouseTypeRCB.Items.FindItemByText(WarehouseTypeRCB.Text).Value);
                    int department = int.Parse(DepartmentRCB.Items.FindItemByText(DepartmentRCB.Text).Value);

                    if (type == (int)WarehouseTypeEnum.WarehouseStandardWithoutRequirements)
                    {
                        responseCode = warehouseDs.CreateWarehouse(newWarehouseName, shortName, (CompanyEnum)company, warehouseDs.GetPersonForSystemUser(int.Parse(Session[SessionData.SessionKey_UserId].ToString())).Id, department);
                    }
                    else
                    {
                        //WarehouseTypeEnum warehouseTypeEnum;

                        //if (type == (int)WarehouseTypeEnum.WarehouseStandard)
                        //{
                        //    warehouseTypeEnum = WarehouseTypeEnum.WarehouseStandard;
                        //}
                        //else if (type == (int)WarehouseTypeEnum.WarehouseStandardWithoutOrdering)
                        //{
                        //    warehouseTypeEnum = WarehouseTypeEnum.WarehouseStandardWithoutOrdering;
                        //}
                        //else if (type == (int)WarehouseTypeEnum.WarehouseDepartmentWithoutOrdering)
                        //{
                        //    warehouseTypeEnum = WarehouseTypeEnum.WarehouseDepartmentWithoutOrdering;
                        //}
                        //else if (type == (int)WarehouseTypeEnum.WarehouseDepartmentWithOrdering)
                        //{
                        //    warehouseTypeEnum = WarehouseTypeEnum.WarehouseDepartmentWithOrdering;
                        //}
                        //else
                        //{
                        //    throw new NotSupportedException();
                        //}

                        //responseCode = warehouseDs.CreateWarehouse(newWarehouseName, shortName, (CompanyEnum)company, int.Parse(Session[SessionData.SessionKey_UserId].ToString()),
                        //                            warehouseTypeEnum, department);
                    }
                }
                if (responseCode > -1)
                {
                    this.Label_StatusMessage.Text = "Utworzono magazyn o nazwie <b>" + WarehouseNameRCB.Text + "</b> (o nazwie skróconej <b>" + WarehouseShortNameRCB.Text + "</b>) w firmie <b>" + CompanyRCB.Text + "</b>";
                    this.WarehouseNameRCB.Text = "";
                    this.WarehouseShortNameRCB.Text = "";
                    //this.WarehouseTypeRCB.ClearSelection();
                    this.DepartmentRCB.ClearSelection();

                    //this.Panel_Permissions.Visible = true;

                    this.WarehousesGrid.Rebind();
                    this.TargetWarehousesGrid.Rebind();
                    this.DocumentTypeGrid.Rebind();

                }
                else
                {
                    switch (responseCode)
                    {
                        case -1:
                            // Nazwa już istnieje
                            this.CustomValidator_NameExists.ErrorMessage = "Magazyn o tej nazwie już istnieje";
                            this.CustomValidator_NameExists.IsValid = false;
                            break;
                        case -2:
                            // Skrót już istnieje
                            this.RequiredFieldValidator_ShortName.ErrorMessage = "Magazyn o tej nazwie skróconej już istnieje";
                            this.RequiredFieldValidator_ShortName.IsValid = false;
                            break;
                        case -3:
                            // Skrót nieprawidłowy
                            this.RequiredFieldValidator_ShortName.ErrorMessage = "Nazwa skrócona może mieć maksymalnie 7 znaków";
                            this.RequiredFieldValidator_ShortName.IsValid = false;
                            break;
                        default:
                            // Niesprecyzowany błąd
                            break;

                    }

                }
            }
        }

        protected void DepartmentRCBCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //int warehouseDepartmentWithoutOrderingEnumValue = (int)WarehouseTypeEnum.WarehouseDepartmentWithoutOrdering;
            //int warehouseDepartmentWithOrderingEnumValue = (int)WarehouseTypeEnum.WarehouseDepartmentWithOrdering;

            //if ((int.Parse(WarehouseTypeRCB.Items.FindItemByText(WarehouseTypeRCB.Text).Value) == warehouseDepartmentWithoutOrderingEnumValue || int.Parse(WarehouseTypeRCB.Items.FindItemByText(WarehouseTypeRCB.Text).Value) == warehouseDepartmentWithOrderingEnumValue) &&
            //    int.Parse(DepartmentRCB.Items.FindItemByText(DepartmentRCB.Text).Value) == -1)
            //{
            //    args.IsValid = false;
            //}
            //else
            //{
            //    args.IsValid = true;
            //}

            if (int.Parse(DepartmentRCB.Items.FindItemByText(DepartmentRCB.Text).Value) == -1)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }











      
    }
}
